pace-client
===========
Microsoft Excel Add-in for the OPEN PACE planning application.


Copyright
==========
Copyright (c) 2017-2019 Contributors to Open Pace and others.

The OPEN PACE product suite is free software: you can redistribute it and/or
modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or (at your
option) any later version.
 
This software is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
License for more details.
 
You should have received a copy of the GNU General Public License along with
this  software. If not, see <http://www.gnu.org/licenses/>.


System Requirements
===================
1) Visual Studio 2012 or 2013 (.Net 4)

2) Microsoft Excel 2007, or 2010 (32 or 64 bit).  Excel 2013 will NOT work.  You can have both versions of Excel installed, but the solution will not load if ONLY Excel 2013 is found.

3) DevExpress 13.1.5 (http://www.devexpress.com/Support/WhatsNew/DXperience/files/13.1.5.xml)

4) Visual Studio 2010 Tools for Office Runtime (http://www.microsoft.com/en-us/download/details.aspx?id=39290)

5) InstallShield 2013 or later
