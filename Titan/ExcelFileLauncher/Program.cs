#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace ExcelFileLauncher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(String[] args)
        {
            try
            {
                Process proc = new Process();

                if (args.Length > 0)
                {
                    if (!File.Exists(args[0]))
                    {
                        throw new FileNotFoundException(String.Format("File: {0} not found", new String[] { args[0] }));
                    }
                }
                else 
                {
                    throw new Exception("No Excel workbook was specified in the command line.");
                }

                proc.StartInfo.FileName = "excel.exe";
                proc.StartInfo.Arguments = @"""" + args[0] + @"""";
                proc.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"Open Pace Client", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}