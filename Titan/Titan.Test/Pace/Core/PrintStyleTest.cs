﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using NUnit.Framework;
using Titan.Pace.Application.Core;
using Titan.PafService;

namespace Titan.Test
{
    public class PrintStyleTest
    {
        private printStyle _globalStyle;

        [TestFixtureSetUp]
        public void Init()
        {

            _globalStyle = new printStyle();

            _globalStyle.adjustTo = true;
            _globalStyle.alignWithPageMargin = true;
            _globalStyle.blackAndWhite = false;

            _globalStyle.bottom = 1;
            _globalStyle.cellErrorsAs = "displayed";
            _globalStyle.centerHorizontally = true;

            _globalStyle.centerVertically = false;
            _globalStyle.colsToRepeatAtLeft = "Pace Column Headings";
            _globalStyle.comment = "Test Case Comment";

            _globalStyle.defaultStyle = false;
            _globalStyle.diffFirstPage = true;
            _globalStyle.diffOddAndEvenPages = true;

            _globalStyle.downThenOver = false;
            _globalStyle.draftQuality = false;
            _globalStyle.entireView = false;

            _globalStyle.firstPageNumber = "Auto";
            _globalStyle.fitTo = true;
            _globalStyle.footer = 1;

            _globalStyle.footerText = "Footer Text";
            _globalStyle.GUID = Guid.NewGuid().ToString();
            _globalStyle.gridlines = false;

            _globalStyle.header = 2;
            _globalStyle.headerText = "Test Header";
            _globalStyle.landscape = false;

            _globalStyle.left = 3;
            _globalStyle.name = "Test Print Style";
            _globalStyle.overThenDown = false;

            _globalStyle.pageTall = 2;
            _globalStyle.pageWide = 1;
            _globalStyle.paperSize = "Legal";

            _globalStyle.percentNormalSize = 100;
            _globalStyle.portrait = false;
            _globalStyle.right = 2;

            _globalStyle.rowAndColHeadings = false;
            _globalStyle.rowsToRepeatAtTop = "Pace Row Headings";
            _globalStyle.scaleWithDocument = false;


            _globalStyle.top = 2;
            _globalStyle.userSelection = false;
            _globalStyle.userSelectionText = String.Empty;

        }

        [Test]
        public void TestPrintStyle()
        {
            IPrintStyle ps = new PrintStyle(_globalStyle);
            IPrintStyle ps1 = new PrintStyle(_globalStyle);
            IPrintStyle badPrintStyle = ps.Clone();
            badPrintStyle.GUID = Guid.NewGuid().ToString();

            Assert.IsTrue(ps.Equals(ps1));

            Assert.IsFalse(ps.Equals(badPrintStyle));
        }
    }
}
