﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using NUnit.Framework;
using Titan.Pace.Base.Data;
using Titan.Palladium.DataStructures;

namespace Titan.Test
{
    [TestFixture]
    class PerformanceTestCoordinates
    {
        private readonly string[] _axis = new[] { "Measures", "PlanType", "Time", "Version", "Years", "Product", "Location" };

        private readonly string[] _coord = new[] { "SLS_DLR", "ClassChn", "WK27", "WP", "FY2006", "CLS608-10", "StoreTotal" };

        private readonly string[] _coord2 = new[] { "SLS_DLR", "ClassChn", "WK28", "WP", "FY2006", "CLS608-10", "StoreTotal" };

        public string[] GetRandomStringArray()
        {
            List<string> str = new List<string>(7);
            for (int i = 0; i < 7; i++)
            {
                str.Add(GetRandomString());
            }

            return str.ToArray();
        }


        public string GetRandomString()
        {
            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789qwertyuiop[]\asdfghjkl;'zxcvbnm,./!@#$%^&*()_+-=:<>?";
            var random = new Random(Guid.NewGuid().GetHashCode());
            var result = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

        public int GetRandomInt()
        {
            var random = new Random(Guid.NewGuid().GetHashCode());

            return random.Next(int.MinValue, int.MaxValue);
        }

        [Test]
        public void TestCreateIntegerHashSetMemoryTest()
        {
            Stopwatch sw = Stopwatch.StartNew();

            long originalByteCount = GC.GetTotalMemory(true);
            long originalMegByteCount = originalByteCount / 1048576;
            Trace.WriteLine("Begin Memory: " + originalMegByteCount.ToString("N0") + " MB");

            HashSet<int> tempList = new HashSet<int>();
            for (int i = 0; i < 2840; i++)
            {
                tempList.Add(GetRandomInt());
            }
            Trace.WriteLine("HashSet size: " + tempList.Count.ToString("N0"));
            long endByteCount = GC.GetTotalMemory(true);
            long endMegByteCount = endByteCount / 1048576;
            Trace.WriteLine("End Memory: " + endMegByteCount.ToString("N0") + " MB");
            sw.Stop();
            Trace.WriteLine("TestCreateIntegerHashSetMemoryTest: " + sw.ElapsedMilliseconds);
            Assert.LessOrEqual(tempList.Count, 2840);
        }
       
        [Test]
        public void TestCreateCoordinateHashSetMemoryTest()
        {
            Stopwatch sw = Stopwatch.StartNew();

            long originalByteCount = GC.GetTotalMemory(true);
            long originalMegByteCount = originalByteCount / 1048576;
            Trace.WriteLine("Begin Memory: " + originalMegByteCount.ToString("N0") + " MB");

            HashSet<Coordinate> tempList = new HashSet<Coordinate>();
            for (int i = 0; i < 1700; i++)
            {
                tempList.Add(new Coordinate(GetRandomStringArray()));
            }
            Trace.WriteLine("HashSet size: " + tempList.Count.ToString("N0"));
            long endByteCount = GC.GetTotalMemory(true);
            long endMegByteCount = endByteCount / 1048576;
            Trace.WriteLine("End Memory: " + endMegByteCount.ToString("N0") + " MB");
            sw.Stop();
            Trace.WriteLine("TestCreateCoordinateHashSetMemoryTest: " + sw.ElapsedMilliseconds);
            Assert.AreEqual(1700, tempList.Count);
        }

        [Test]
        public void TestCreateCoordinateListMemoryTest()
        {
            Stopwatch sw = Stopwatch.StartNew();

            long originalByteCount = GC.GetTotalMemory(true);
            long originalMegByteCount = originalByteCount / 1048576;
            Trace.WriteLine("Begin Memory: " + originalMegByteCount.ToString("N0") + " MB");

            List<Coordinate> tempList = new List<Coordinate>();
            for (int i = 0; i < 17000; i++)
            {
                tempList.Add(new Coordinate(GetRandomStringArray()));
            }
            Trace.WriteLine("HashSet size: " + tempList.Count.ToString("N0"));
            long endByteCount = GC.GetTotalMemory(true);
            long endMegByteCount = endByteCount / 1048576;
            Trace.WriteLine("End Memory: " + endMegByteCount.ToString("N0") + " MB");
            sw.Stop();
            Trace.WriteLine("TestCreateCoordinateListMemoryTest: " + sw.ElapsedMilliseconds);
            Assert.AreEqual(17000, tempList.Count);
        }


        [Test]
        public void TestCreateCoordinateSetMemoryTest()
        {
            Stopwatch sw = Stopwatch.StartNew();

            long originalByteCount = GC.GetTotalMemory(true);
            long originalMegByteCount = originalByteCount / 1048576;
            Trace.WriteLine("Begin Memory: " + originalMegByteCount.ToString("N0") + " MB");

            CoordinateSet coordinateSet = new CoordinateSet(_axis);
            for (int i = 0; i < 17000; i++)
            {
                coordinateSet.AddCoordinate(GetRandomStringArray());

            }
            long endByteCount = GC.GetTotalMemory(true);
            long endMegByteCount = endByteCount / 1048576;
            Trace.WriteLine("End Memory: " + endMegByteCount.ToString("N0") + " MB");
            sw.Stop();
            Trace.WriteLine("TestCreateCoordinateSetMemoryTest: " + sw.ElapsedMilliseconds);
            Assert.AreEqual(17000, coordinateSet.Count());
        }
    }
}