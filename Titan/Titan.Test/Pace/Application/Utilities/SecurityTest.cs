﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using NUnit.Framework;
using Titan.Pace.Application.Utilities;
using Titan.Pace.DataStructures;
using Titan.Palladium.DataStructures;

namespace Titan.Test
{
    [TestFixture]
    public class SecurityTest 
    {
        [Test]
        public void TestUserNativeMode()
        {
            PafLogonInfo user = new PafLogonInfo("jim", "password", null, null, null);
            PafLogonInfo user2 = Security.EncryptUser(user);

            Assert.AreNotEqual("jim" , user2.UserName);
            Assert.AreNotEqual("password", user2.UserPassword);
            Assert.Null(user2.UserDomain);
            Assert.Null( user2.UserSID);
            Assert.NotNull(user2.IV);
        }

        [Test]
        public void TestUserMixedModeSso()
        {
            string userId = @"corp\kmoos";
            string sid = Security.GetUserSID();
            PafLogonInfo user = new PafLogonInfo(userId, null, null, sid, null);
            PafLogonInfo user2 = Security.EncryptUser(user);

            Assert.AreNotEqual(userId, user2.UserName);
            Assert.AreNotEqual(sid, user2.UserSID);
            Assert.Null(user2.UserPassword);
            Assert.Null(user2.UserDomain);
            Assert.NotNull(user2.IV);
        }

        [Test]
        public void TestUserMixedModeSso1()
        {
            string userId = @"corp\kmoos";
            string userDomain = Security.GetUsersDomain(userId, true);

            PafLogonInfo user = new PafLogonInfo(userId, null, userDomain, null, null);
            PafLogonInfo user2 = Security.EncryptUser(user);

            Assert.AreNotEqual(userId, user2.UserName);
            Assert.AreNotEqual(userDomain, user2.UserDomain);
            Assert.Null(user2.UserPassword);
            Assert.Null(user2.UserSID);
            Assert.NotNull(user2.IV);
        }

        [Test]
        public void TestUserMixedModeNonSso()
        {
            string userId = @"corp\kmoos";
            string password = "password";
            string userDomain = Security.GetUsersDomain(userId, true);

            PafLogonInfo user = new PafLogonInfo(userId, password, userDomain, null, null);
            PafLogonInfo user2 = Security.EncryptUser(user);

            Assert.AreNotEqual(userId, user2.UserName);
            Assert.AreNotEqual(userDomain, user2.UserDomain);
            Assert.AreNotEqual(password, user2.UserPassword);
            Assert.Null(user2.UserSID);
            Assert.NotNull(user2.IV);
        }

        [Test]
        public void TestWindowsUserNameNotNull()
        {
            string userId = Security.GetUserName(false);
            Assert.NotNull(userId);
        }



    }
}
