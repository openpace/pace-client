﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Windows.Forms;
using NUnit.Framework;
using Titan.Pace.Application.Controls.TreeView;
using Titan.Pace.Application.Extensions;
using Titan.Pace.ExcelGridView;
using Titan.Palladium.GridView;
using Titan.PafService;
using Titan.Palladium;

namespace Titan.Test
{

    [TestFixture]
    public class PaceTreeViewTest : BaseTest
    {
        private string[] _dims;
        private SimpleTrees _simpleTrees;
        private List<pafSimpleDimTree> _pafSimpleDimTrees;
        
        [TestFixtureSetUp]
        public void Init()
        {
            _dims = new[] { "ClimateZone", "Indoor.Outdoor", "Location", "Measures", "PlanType", "Product", "Season", "State", "**TIME.HORIZON**", "Time", "Version", "Years" };

            _simpleTrees = new SimpleTrees();
            _pafSimpleDimTrees = new List<pafSimpleDimTree>();

            foreach (string dim in _dims)
            {
                pafSimpleDimTree tree = ObjectExtension.Load<pafSimpleDimTree>(ApplicationDataPath, dim.RemoveInvalidFileChars() + ".xml");

                _pafSimpleDimTrees.Add(tree);

                _simpleTrees.Add(tree, tree.id, true);
            }
        }


        [Test]
        public void TestPaceTreeView()
        {
            foreach(pafSimpleDimTree pafSimpleTree in _pafSimpleDimTrees)
            {
                if (pafSimpleTree.id != "Measures") continue;

                PaceTreeView tree = new PaceTreeView();
                tree.Name = pafSimpleTree.id;
                tree.DimensionName = pafSimpleTree.id;
                tree.DimensionType = PaceTreeNode.PaceTreeDimensionType.Base;
                tree.MultiSelectTree = true;
                tree.CheckBoxes = true;
                tree.DrawMode = TreeViewDrawMode.Normal;
                tree.UnselectableMbrName.Add(pafSimpleTree.id);
                tree.DisallowAncestorSelections = false;
                tree.AllowRootNodeSelection = true;
                tree.BuildTreeControl(_simpleTrees, pafSimpleTree, true, String.Empty);

                Assert.AreNotEqual(0, tree.Nodes.Count);

                List<PaceTreeNode> nodes = tree.Find("Total", true, false, false);

                Assert.AreEqual(12, nodes.Count);

                nodes = tree.Find("Total", true, false, true);

                Assert.IsNull(nodes);

                tree.SelectAllNodes();

                tree.TraverseTreeView();

                Assert.AreEqual(89, tree.CheckedNodes.Count);

                tree.UncheckAllNodes();

                Assert.AreEqual(0, tree.CheckedNodes.Count);

                tree.DisallowAncestorSelections = true;

                tree.SelectAllNodes();

                Assert.AreEqual(1, tree.CheckedNodes.Count);

                tree.UncheckAllNodes();

                Assert.AreEqual(0, tree.CheckedNodes.Count);

            }


        }
    }
}
