﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Linq;
using Titan.Palladium;
using NUnit.Framework;
using Titan.Pace.Application.Compression.PafSimpleCellNotes;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.PafService;

namespace Titan.Test
{
    [TestFixture]
    public class CompressionTestMemberTag : BaseTest
    {
        private const string FileName = "pafViewWithMemberTags.xml";

        [TestFixtureSetUp]
        public void Init()
        {
            PafView = ObjectExtension.Load<pafView>(ApplicationDataPath, FileName);

            PafView.Uncompress();
        }

        [Test]
        public void TestViewSection()
        {
            Assert.AreEqual(PafView.viewSections[0].colTuples.Count(), 10);
            Assert.AreEqual(PafView.viewSections[0].rowTuples.Count(), 14);
            Assert.AreEqual(PafView.name, "Member Tags - Tags on Row and Col by Location");

            Assert.AreEqual(PafView.viewSections[0].pafDataSlice.columnCount, 9);
            Assert.AreEqual(PafView.viewSections[0].pafDataSlice.compressed, false);
        }

        [Test]
        public void TestCellNotes()
        {
            CreateViewState();

            if (PafView.viewSections[0].cellNotes != null && PafView.viewSections[0].cellNotes.Length > 0)
            {
                PafSimpleCellNotes pscn = new PafSimpleCellNotes(PafView.viewSections[0].cellNotes);
                pscn.uncompressData();
            }

            Assert.IsNull(PafView.viewSections[0].cellNotes);

        }
    }
}
