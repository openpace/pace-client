﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using Microsoft.Office.Interop.Excel;
using NUnit.Framework;
using Titan.Pace.Application.Extensions;
using Titan.Palladium;

namespace Titan.Test
{
    public class ExcelExtensionTest
    {
        [Test]
        public void TestPaperSize()
        {
            string paperSize = "Letter";
            Assert.AreEqual(XlPaperSize.xlPaperLetter, paperSize.ToXlPaperSize());

            paperSize = "Tabloid";
            Assert.AreEqual(XlPaperSize.xlPaperTabloid, paperSize.ToXlPaperSize());

            paperSize = "Legal";
            Assert.AreEqual(XlPaperSize.xlPaperLegal, paperSize.ToXlPaperSize());

            paperSize = "Executive";
            Assert.AreEqual(XlPaperSize.xlPaperExecutive, paperSize.ToXlPaperSize());

            paperSize = "A3";
            Assert.AreEqual(XlPaperSize.xlPaperA3, paperSize.ToXlPaperSize());

            paperSize = "A4";
            Assert.AreEqual(XlPaperSize.xlPaperA4, paperSize.ToXlPaperSize());

            paperSize = "B4(JIS)";
            Assert.AreEqual(XlPaperSize.xlPaperB4, paperSize.ToXlPaperSize());

            paperSize = "B5(JIS)";
            Assert.AreEqual(XlPaperSize.xlPaperB5, paperSize.ToXlPaperSize());

            paperSize = "Envelope #10";
            Assert.AreEqual(XlPaperSize.xlPaperB4, paperSize.ToXlPaperSize());

            paperSize = "Envelope Monarch";
            Assert.AreEqual(XlPaperSize.xlPaperEnvelopeMonarch, paperSize.ToXlPaperSize());
        }

        [Test]
        public void TestPageOrientation()
        {
            string paperSize = "Portrait";
            Assert.AreEqual(XlPageOrientation.xlPortrait, paperSize.ToXlPageOrientation());

            paperSize = "Landscape";
            Assert.AreEqual(XlPageOrientation.xlLandscape, paperSize.ToXlPageOrientation());

            paperSize = String.Empty;
            Assert.AreEqual(XlPageOrientation.xlPortrait, paperSize.ToXlPageOrientation());
        }

        [Test]
        public void TestPrintLocation()
        {
            string paperSize = "(None)";
            Assert.AreEqual(XlPrintLocation.xlPrintNoComments, paperSize.ToXlPrintLocation());

            paperSize = "At end of sheet";
            Assert.AreEqual(XlPrintLocation.xlPrintSheetEnd, paperSize.ToXlPrintLocation());

            paperSize = "As displayed on sheet";
            Assert.AreEqual(XlPrintLocation.xlPrintInPlace, paperSize.ToXlPrintLocation());

            paperSize = String.Empty;
            Assert.AreEqual(XlPrintLocation.xlPrintNoComments, paperSize.ToXlPrintLocation());
        }

        [Test]
        public void TestPrintErrors()
        {
            string paperSize = "<blank>";
            Assert.AreEqual(XlPrintErrors.xlPrintErrorsBlank, paperSize.ToXlPrintErrors());

            paperSize = "displayed";
            Assert.AreEqual(XlPrintErrors.xlPrintErrorsDisplayed, paperSize.ToXlPrintErrors());

            paperSize = "--";
            Assert.AreEqual(XlPrintErrors.xlPrintErrorsDash, paperSize.ToXlPrintErrors());

            paperSize = "#N/A";
            Assert.AreEqual(XlPrintErrors.xlPrintErrorsNA, paperSize.ToXlPrintErrors());

            paperSize = String.Empty;
            Assert.AreEqual(XlPrintErrors.xlPrintErrorsDisplayed, paperSize.ToXlPrintErrors());
        }


    }
}
