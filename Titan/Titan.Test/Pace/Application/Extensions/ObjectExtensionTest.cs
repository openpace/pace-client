﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Drawing;
using NUnit.Framework;
using Titan.Pace.Application.Extensions;
using Titan.Palladium;

namespace Titan.Test
{
    [TestFixture]
    class ObjectExtensionTest
    {
        [Test]
        public void TestDouble()
        {
            object d = 5.25;
            Assert.IsTrue(d.IsDouble());
        }

        [Test]
        public void TestGetDouble()
        {
            object d = 5.25;
            Assert.AreEqual(d.GetDouble(true), d);
        }

        [Test]
        public void TestGetDoubleFail()
        {
            object d = "this is a test";
            Assert.IsFalse(d.IsDouble());
        }


        [Test]
        public void TestGetInteger()
        {
            object i = 500;
            Assert.AreEqual(i.GetInteger(true), i);
        }

        [Test]
        public void TestGetIntegerFail()
        {
            object i = 500.5;
            Assert.AreEqual(i.GetInteger(), null);
        }

        [Test]
        public void TestIsInteger()
        {
            object o = 500;
            int result;
            Assert.IsTrue(o.IsInteger(out result));
        }

        [Test]
        public void TestIsNotInteger()
        {
            object o = 500.50;
            int result;
            Assert.IsFalse(o.IsInteger(out result));
        }


        [Test]
        public void TestIsNull()
        {
            object o = null;
            Assert.IsTrue(o.IsNull());
        }

        [Test]
        public void TestIsNotNull()
        {
            object o = 500;
            Assert.IsFalse(o.IsNull());
        }
    }
}
