﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Titan.Pace.Application.Extensions;
using Titan.Palladium;


namespace Titan.Test
{
    [TestFixture]
    public class StringExtensionsTest
    {
        [Test]
        public void TestEqualsIgnoreCase()
        {
            string str1 = "WE ARE THE SAME";
            string str2 = "we are the same";
            Assert.IsTrue(str1.EqualsIgnoreCase(str2));
        }

        [Test]
        public void TestHashCode()
        {
            string str1 = "BOP_U, CorpChn, Q1, WF, FY2006, DIV03, StoreTotal";
            string str2 = "bop_u, CorpChn, Q1, WF, FY2006, DIV03, StoreTotal";
            Assert.AreNotEqual(str1.GetHashCode(), str2.GetHashCode());
        }

        [Test]
        public void TestHashCode2()
        {
            string str1 = "BOP_U, CorpChn, Q1, WF, FY2006, DIV03, StoreTotal";
            string str2 = "BOP_U, CorpChn, Q1, WF, FY2006, DIV03, StoreTotal";
            Assert.AreEqual(str1.GetHashCode(), str2.GetHashCode());
        }

        [Test]
        public void TestConvertToList()
        {
            string test = "the,lazy,brown, fox, jumps , over";
            List<string> convert = test.ConverttoList(",");

            Assert.AreEqual(convert.Count, 6);
        }

        [Test]
        public void TestConvertToListOfLists()
        {
            string test = "null^0^3^3^Clare Tonge/Lucy Ngyuen/Julia Hamer/Claire Telfer (women1)^women1|^0^1^4^2^18 - designer bridge^18|^1^2^5^1^134 - contemporary bridge^134|^1^3^5^1^151 - dvf^151|^1^4^5^1^159 - hugo boss^159|^1^5^5^1^168 - max mara^168|^1^6^5^1^172 - paul smith^172|^1^7^5^1^214 - james perse^214|^1^8^5^1^219 - armani collezioni^219|^1^9^5^1^224 - helmut lang^224|^1^10^5^1^225 - margiela 6^225|^1^11^5^1^239 - the zone^239|^1^12^5^1^245 - ralph lauren black^245|^1^13^5^1^257 - max mara weekend^257|^1^14^5^1^262 - dkny - ready to wear^262|^1^15^5^1^274 - tahari^274|^1^16^5^1^312 - cult designer^312|^1^17^5^1^337 - cheap & chic^337|^1^18^5^1^338 - comme comme^338|^1^19^5^1^369 - m missoni^369|^1^20^5^1^419 - d&g^419|^1^21^5^1^459 - costume nationale^459|^1^22^5^1^477 - nicole farhi^477|^1^23^5^1^482 - barbara bui^482|^1^24^5^1^540 - lucy in disguise^540|^1^25^5^1^756 - tsesay^756|^1^26^5^1^820 - burberry luxe^820|^1^27^5^1^983 - nicole farhi^983|^1^28^5^1^991 - burberry london^991|^0^29^4^2^41 - designerwear^41|^29^30^5^1^161 - alexander mcqueen^161|^29^31^5^1^166 - chloe^166|^29^32^5^1^167 - balenciaga^167|^29^33^5^1^173 - dolce & gabbana^173|^29^34^5^1^174 - burberry^174|^29^35^5^1^190 - pucci^190|^29^36^5^1^192 - stella mccartney^192|^29^37^5^1^194 - temperley^194|^29^38^5^1^196 - celine rtw^196|^29^39^5^1^215 - dries van noten^215|^29^40^5^1^226 - roberto cavalli^226|^29^41^5^1^227 - vivienne westwood^227|^29^42^5^1^228 - halston^228|^29^43^5^1^238 - designer room^238|^29^44^5^1^417 - miu miu^417|^29^45^5^1^420 - givenchy^420|^29^46^5^1^510 - gucci rtw^510|^29^47^5^1^566 - elio ferraro^566|^29^48^5^1^824 - marni^824|^29^49^5^1^872 - chloe designerwear^872|^29^50^5^1^947 - balenciaga dwear^947|^0^51^4^2^50 - classic bridge^50|^51^52^5^1^153 - kenzo^153|^51^53^5^1^154 - marella^154|^51^54^5^1^155 - armani jeans^155|^51^55^5^1^156 - smart casual room^156|^51^56^5^1^157 - special occasion^157|^51^57^5^1^162 - separates^162|^51^58^5^1^163 - occasionwear^163|^51^59^5^1^169 - plus brands^169|^51^60^5^1^175 - georges rech^175|^51^61^5^1^184 - knitwear^184|^51^62^5^1^185 - kenzo^185|^51^63^5^1^189 - tailored colle^189|^51^64^5^1^213 - basler^213|^51^65^5^1^243 - ralph lauren blue^243|^51^66^5^1^259 - evening wear^259|^51^67^5^1^260 - hats^260|^51^68^5^1^296 - dkny pure^296|^51^69^5^1^316 - oska^316|^51^70^5^1^339 - easy dressing^339|^51^71^5^1^422 - mfg^422|^51^72^5^1^487 - day^487|^51^73^5^1^511 - thomas pink woman^511|^51^74^5^1^520 - coast (london)^520|^51^75^5^1^543 - toast^543|^51^76^5^1^623 - jaeger^623|^51^77^5^1^750 - cos^750|^51^78^5^1^780 - comptoir  cotonniers^780|^51^79^5^1^793 - joseph^793|^51^80^5^1^797 - hobbs^797|^51^81^5^1^815 - burberry lifestyle^815|^51^82^5^1^832 - lk bennett rtw^832|^51^83^5^1^869 - fen wright manson^869|^51^84^5^1^929 - magaret howell^929|^51^85^5^1^952 - reiss ladies^952|^51^86^5^1^984 - tse^984|^51^87^5^1^992 - icb^992|^";
            List<List<string>> convert = test.ConverttoListofLists("|^", "^");

            Assert.AreEqual(convert.Count, 88);
        }

        [Test]
        public void TestSplitToList()
        {
            string test = "null^0^3^3^Clare Tonge/Lucy Ngyuen/Julia Hamer/Claire Telfer (women1)^women1|^0^1^4^2^18 - designer bridge^18|^1^2^5^1^134 - contemporary bridge^134|^1^3^5^1^151 - dvf^151|^1^4^5^1^159 - hugo boss^159|^1^5^5^1^168 - max mara^168|^1^6^5^1^172 - paul smith^172|^1^7^5^1^214 - james perse^214|^1^8^5^1^219 - armani collezioni^219|^1^9^5^1^224 - helmut lang^224|^1^10^5^1^225 - margiela 6^225|^1^11^5^1^239 - the zone^239|^1^12^5^1^245 - ralph lauren black^245|^1^13^5^1^257 - max mara weekend^257|^1^14^5^1^262 - dkny - ready to wear^262|^1^15^5^1^274 - tahari^274|^1^16^5^1^312 - cult designer^312|^1^17^5^1^337 - cheap & chic^337|^1^18^5^1^338 - comme comme^338|^1^19^5^1^369 - m missoni^369|^1^20^5^1^419 - d&g^419|^1^21^5^1^459 - costume nationale^459|^1^22^5^1^477 - nicole farhi^477|^1^23^5^1^482 - barbara bui^482|^1^24^5^1^540 - lucy in disguise^540|^1^25^5^1^756 - tsesay^756|^1^26^5^1^820 - burberry luxe^820|^1^27^5^1^983 - nicole farhi^983|^1^28^5^1^991 - burberry london^991|^0^29^4^2^41 - designerwear^41|^29^30^5^1^161 - alexander mcqueen^161|^29^31^5^1^166 - chloe^166|^29^32^5^1^167 - balenciaga^167|^29^33^5^1^173 - dolce & gabbana^173|^29^34^5^1^174 - burberry^174|^29^35^5^1^190 - pucci^190|^29^36^5^1^192 - stella mccartney^192|^29^37^5^1^194 - temperley^194|^29^38^5^1^196 - celine rtw^196|^29^39^5^1^215 - dries van noten^215|^29^40^5^1^226 - roberto cavalli^226|^29^41^5^1^227 - vivienne westwood^227|^29^42^5^1^228 - halston^228|^29^43^5^1^238 - designer room^238|^29^44^5^1^417 - miu miu^417|^29^45^5^1^420 - givenchy^420|^29^46^5^1^510 - gucci rtw^510|^29^47^5^1^566 - elio ferraro^566|^29^48^5^1^824 - marni^824|^29^49^5^1^872 - chloe designerwear^872|^29^50^5^1^947 - balenciaga dwear^947|^0^51^4^2^50 - classic bridge^50|^51^52^5^1^153 - kenzo^153|^51^53^5^1^154 - marella^154|^51^54^5^1^155 - armani jeans^155|^51^55^5^1^156 - smart casual room^156|^51^56^5^1^157 - special occasion^157|^51^57^5^1^162 - separates^162|^51^58^5^1^163 - occasionwear^163|^51^59^5^1^169 - plus brands^169|^51^60^5^1^175 - georges rech^175|^51^61^5^1^184 - knitwear^184|^51^62^5^1^185 - kenzo^185|^51^63^5^1^189 - tailored colle^189|^51^64^5^1^213 - basler^213|^51^65^5^1^243 - ralph lauren blue^243|^51^66^5^1^259 - evening wear^259|^51^67^5^1^260 - hats^260|^51^68^5^1^296 - dkny pure^296|^51^69^5^1^316 - oska^316|^51^70^5^1^339 - easy dressing^339|^51^71^5^1^422 - mfg^422|^51^72^5^1^487 - day^487|^51^73^5^1^511 - thomas pink woman^511|^51^74^5^1^520 - coast (london)^520|^51^75^5^1^543 - toast^543|^51^76^5^1^623 - jaeger^623|^51^77^5^1^750 - cos^750|^51^78^5^1^780 - comptoir  cotonniers^780|^51^79^5^1^793 - joseph^793|^51^80^5^1^797 - hobbs^797|^51^81^5^1^815 - burberry lifestyle^815|^51^82^5^1^832 - lk bennett rtw^832|^51^83^5^1^869 - fen wright manson^869|^51^84^5^1^929 - magaret howell^929|^51^85^5^1^952 - reiss ladies^952|^51^86^5^1^984 - tse^984|^51^87^5^1^992 - icb^992|^";
            
            List<string> convert = test.SplitManual("|^", true, true);
            List<string> convert2 = test.SplitString("|^");
            List<string> convert3 = test.Split(new string[] {"|^"}, StringSplitOptions.RemoveEmptyEntries).ToList();

            Assert.AreEqual(convert.Count, 88);
            Assert.AreEqual(convert2.Count, 88);
            Assert.AreEqual(convert3.Count, 88);

            Assert.AreEqual(convert, convert2);
            Assert.AreEqual(convert, convert3);
            Assert.AreEqual(convert2, convert3);

        }


        [Test]
        public void TestConvertToDictionary()
        {
            string test = "Week58^60|^Week59^61|^H3TD_P14^52|^Week57^59|^P12^43|^H2_P11A^31|^H3_P17B^77|^Week34^15|^Week33^14|^Week32^13|^Week31^12|^Week61^63|^Week60^62|^Week54^55|^Week53^54|^Week35^16|^H2^1|^Week55^56|^Week56^57|^H3^48|^H2TD_P11^2|^H3TD_P16^50|^H2_P10^24|^H3_P15^64|^H2_P11B^30|^H3_P18C^86|^H2_P10A^23|^H2TD_P10^3|^P9^18|^P10^25|^H2_P9^17|^Week68^75|^Week67^74|^Week66^73|^Week40^26|^H2_P11^32|^Week42^28|^Week41^27|^Week43^29|^Week69^76|^H3TD_P15^51|^H3_P16^71|^P7^6|^P8^11|^H3_P16A^70|^Week29^9|^H3_P18A^88|^Week28^8|^Week27^7|^P16^72|^Week78^94|^Week76^92|^Week77^93|^P11^33|^Week75^91|^Week30^10|^H2_P12A^41|^H2TD_P8^5|^Week73^84|^Week74^85|^H3_P17A^78|^H2_P12^42|^Week70^81|^Week71^82|^Week72^83|^P18^90|^Half2^0|^H3TD_P17^49|^P13^53|^P14^58|^H2_P12C^39|^Week64^68|^Week65^69|^P17^80|^Week62^66|^Week63^67|^Week38^21|^Week39^22|^Week36^19|^Week37^20|^Week50^45|^Week51^46|^Week52^47|^H2_P12B^40|^H3_P18^89|^H2TD_P9^4|^Week45^35|^H3_P17^79|^Week44^34|^Week47^37|^Week46^36|^Week49^44|^Week48^38|^P15^65|^H3_P18B^87|^";
            Dictionary<int, string> convert = test.ConverttoDictionary("|^", "^");

            Assert.AreEqual(convert.Count, 95);
        }

        [Test]
        public void TestCreateDelimitedString()
        {
            string[] str = new string[] {"one","two","three","four"};
            string delimiter = ",";

            string s1 = str.CreateDelimitedString(delimiter);

            Assert.AreEqual(s1, "one,two,three,four");
        }

        [Test]
        public void TestSplitStringLeaveBlanks()
        {
            string test = "the,lazy,brown, fox, jumps ,, over";
            string[] split = test.SplitString(",", false);

            Assert.AreEqual(split.Length, 7);
        }

        [Test]
        public void TestSplitStringRemoveBlanks()
        {
            string test = "the,lazy,brown, fox, jumps ,, over";
            string[] split = test.SplitString(",", true);

            Assert.AreEqual(split.Length, 6);
        }

        [Test]
        public void TestInsertString()
        {
            string str1 = "500###;##";
            string str2 = "RA";

            string numericFormat = str1.InsertString(";", str2);

            Assert.AreEqual(numericFormat, "500###RA;##");
        }

        [Test]
        public void TestIsUriValid()
        {
            string str1 = "http://localhost:8080/pace/PafService";

            Assert.IsTrue(str1.IsUrlValid());
        }

        [Test]
        public void TestIsUriNotValid()
        {
            string str1 = "http://localhost:-1/pace/PafService";

            Assert.IsFalse(str1.IsUrlValid());
        }


        [Test]
        public void TestIsUriNotValid2()
        {
            string str1 = "http://:443/pace/PafService";

            Assert.IsFalse(str1.IsUrlValid());
        }

        [Test]
        public void TestIsUriNotValid3()
        {
            string str1 = "badprotocol://localhost/pace/PafService";

            Assert.IsFalse(str1.IsUrlValid());
        }

        [Test]
        public void TestSplitManual()
        {
            string str1 = "1^2^3^4^5^6^7^8^9^10";

            List<string> lst = str1.SplitManual("^", true, true);

            Assert.AreEqual(10, lst.Count);
        }

        [Test]
        public void TestSplitManual2()
        {
            string str1 = "1^2^3^4^5^6^7^8^9^10^";

            List<string> lst = str1.SplitManual("^", true, true);

            Assert.IsFalse(lst[lst.Count - 1].Contains("^"));
        }

    }
}
