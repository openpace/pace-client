﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Linq;
using Titan.Palladium;
using NUnit.Framework;
using Titan.Pace.Application.Compression.PafSimpleCellNotes;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.PafService;

namespace Titan.Test
{
    [TestFixture]
    public class CompressionTest : BaseTest
    {
        private const string FileName = "pafView.xml";

        [TestFixtureSetUp]
        public void Init()
        {
            PafView = ObjectExtension.Load<pafView>(ApplicationDataPath, FileName);
            PafView.Uncompress();
        }

        [Test]
        public void TestViewSection()
        {
            Assert.AreEqual(PafView.viewSections[0].colTuples.Count(), 35);
            Assert.AreEqual(PafView.viewSections[0].rowTuples.Count(), 335);
            Assert.AreEqual(PafView.name, "Inventory Flow");

            Assert.AreEqual(PafView.viewSections[0].pafDataSlice.columnCount, 35);
            Assert.AreEqual(PafView.viewSections[0].pafDataSlice.compressed, true);
        }

        [Test]
        public void TestCellNotes()
        {
            CreateViewState();

            if (PafView.viewSections[0].cellNotes != null && PafView.viewSections[0].cellNotes.Length > 0)
            {
                PafSimpleCellNotes pscn = new PafSimpleCellNotes(PafView.viewSections[0].cellNotes);
                pscn.uncompressData();
            }

            Assert.NotNull(PafView.viewSections[0].cellNotes);

            Assert.AreEqual(PafView.viewSections[0].cellNotes.Length, 3);

            AddCellNotes(PafView.viewSections[0].cellNotes);

            Assert.AreEqual(ViewStateInfo.CellNotes.Count, 3);


            Assert.AreEqual(ViewStateInfo.CellNotesAsArray[0].text, "last updated: jim\n12/19/2012 11:44:57 AM\nroot:\nDPT608,WK29,SLS_DLR");
            Assert.AreEqual(ViewStateInfo.CellNotesAsArray[1].text, "last updated: jim\n12/19/2012 11:44:57 AM\nroot:\nCLS608-10,AUG,SLS_DLR");
            Assert.AreEqual(ViewStateInfo.CellNotesAsArray[2].text, "last updated: jim\n12/19/2012 11:44:57 AM\nroot:\nDIV9,WK27,SLS_DLR");

        }

        private void AddCellNotes(simpleCellNote[] cellNotes)
        {
            ViewStateInfo.SetCellNotes(cellNotes);
        }
    }
}
