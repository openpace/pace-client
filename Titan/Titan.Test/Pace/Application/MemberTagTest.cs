﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Titan.Palladium;
using NUnit.Framework;
using Titan.Pace;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.MemberTag;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Test
{
    [TestFixture]
    public class MemberTagTest : BaseTest
    {
        private const string FileName = "pafViewWithMemberTagsInvalid.xml";

        [TestFixtureSetUp]
        public void Init()
        {
            PafView = ObjectExtension.Load<pafView>(ApplicationDataPath, FileName);

            PafView.Uncompress();

            CreateViewState();
        }


        [Test]
        public void TestMemberTags()
        {
            pafViewSection viewSection = PafView.viewSections[ViewSectionNumber];

            //add the member tag comment names.
            if (PafView.viewSections[ViewSectionNumber].memberTagCommentEntries != null)
            {
                foreach (memberTagCommentEntry commentEntry in PafView.viewSections[ViewSectionNumber].memberTagCommentEntries)
                {
                    if (!String.IsNullOrEmpty(commentEntry.name))
                    {
                        OlapView.MemberTag.MemberTagCommentNames.Add(commentEntry.name);
                    }
                }
            }

            //cache the comment member tag id/psoitions
            if (PafView.viewSections[ViewSectionNumber].rowMemberTagCommentPositions != null &&
                PafView.viewSections[ViewSectionNumber].rowMemberTagCommentPositions.Length > 0)
            {
                foreach (memberTagCommentPosition row in PafView.viewSections[ViewSectionNumber].rowMemberTagCommentPositions)
                {
                    List<string> values;
                    if (OlapView.MemberTag.RowCommentMemberTags.TryGetValue(row.axis, out values))
                    {
                        values.Add(row.name);
                    }
                    else
                    {
                        values = new List<string> {row.name};
                        OlapView.MemberTag.RowCommentMemberTags.Add(row.axis, values);
                    }
                }
            }

            //cache the comment member tag id/psoitions
            if (PafView.viewSections[ViewSectionNumber].colMemberTagCommentPositions != null &&
                PafView.viewSections[ViewSectionNumber].colMemberTagCommentPositions.Length > 0)
            {
                foreach (memberTagCommentPosition col in PafView.viewSections[ViewSectionNumber].colMemberTagCommentPositions)
                {
                    List<string> values;
                    if (OlapView.MemberTag.ColCommentMemberTags.TryGetValue(col.axis, out values))
                    {
                        values.Add(col.name);
                    }
                    else
                    {
                        values = new List<string> {col.name};
                        OlapView.MemberTag.ColCommentMemberTags.Add(col.axis, values);
                    }
                }
            }

            Assert.AreEqual(2, OlapView.MemberTag.RowCommentMemberTags.Count);
            Assert.AreEqual(2, OlapView.MemberTag.MemberTagCommentNames.Count);
            Assert.AreEqual(0, OlapView.MemberTag.ColCommentMemberTags.Count);


            ArrayList[] rowLists = null, colLists = null;
            //List<Tuple> tuples;
            StringBuilder mbrNameHash;

            //Row tuples
            if (viewSection.rowTuples != null)
            {
                for (int i = 0; i < viewSection.rowTuples.Length; i++)
                {
                    if (rowLists == null)
                    {
                        rowLists = new ArrayList[viewSection.rowTuples[i].memberDefs.Length];
                        for (int k = 0; k < rowLists.Length; k++)
                        {
                            rowLists[k] = new ArrayList();
                        }
                    }

                    //Hash together member names into a string
                    mbrNameHash = new StringBuilder();

                    //Row tuple members
                    for (int j = 0; j < viewSection.rowTuples[i].memberDefs.Length; j++)
                    {
                        string dimName = viewSection.rowAxisDims[j];

                        rowLists[j].Add(viewSection.rowTuples[i].memberDefs[j]);

                        //Append mbrNameHash for RowDimHash
                        mbrNameHash.Append(viewSection.rowTuples[i].memberDefs[j]);

                        //Populate dictionary of unique view members
                        if (!OlapView.ViewMembers.ContainsKey(viewSection.rowTuples[i].memberDefs[j]))
                        {
                            OlapView.ViewMembers.Add(viewSection.rowTuples[i].memberDefs[j], "Row");
                        }

                        //Build a dictionary of member tag comments.
                        if (OlapView.MemberTag.RowCommentMemberTags != null &&
                            OlapView.MemberTag.RowCommentMemberTags.Count > 0)
                        {
                            //AddCommentMemberTagItems(viewSection.rowTuples[i], dimName, j,
                            //    OlapView.MemberTag.RowMemberTagCommentValues,
                            //    OlapView.MemberTag.RowCommentMemberTags,
                            //    viewSection.rowAxisDims,
                            //    OlapView);

                            Assert.AreEqual(2, OlapView.MemberTag.RowCommentMemberTags.Count);
                            Assert.AreEqual(2, OlapView.MemberTag.MemberTagCommentNames.Count);
                            Assert.AreEqual(0, OlapView.MemberTag.ColCommentMemberTags.Count);

                        }
                    }
                }
            }

            //Column tuples
            if (viewSection.colTuples != null)
            {
                for (int i = 0; i < viewSection.colTuples.Length; i++)
                {
                    if (colLists == null)
                    {
                        colLists = new ArrayList[viewSection.colTuples[i].memberDefs.Length];
                        for (int k = 0; k < colLists.Length; k++)
                        {
                            colLists[k] = new ArrayList();
                        }
                    }

                    //Hash together member names into a string
                    mbrNameHash = new StringBuilder();

                    //Col tuple members
                    for (int j = 0; j < viewSection.colTuples[i].memberDefs.Length; j++)
                    {
                        string dimName = viewSection.colAxisDims[j];

                        colLists[j].Add(viewSection.colTuples[i].memberDefs[j]);

                        //Append mbrNameHash for ColDimHash
                        mbrNameHash.Append(viewSection.colTuples[i].memberDefs[j]);

                        //Populate dictionary of unique view members
                        if (!OlapView.ViewMembers.ContainsKey(viewSection.colTuples[i].memberDefs[j]))
                        {
                            OlapView.ViewMembers.Add(viewSection.colTuples[i].memberDefs[j], "Col");
                        }

                        //Build a dictionary of member tag comments.
                        if (OlapView.MemberTag.ColCommentMemberTags != null &&
                            OlapView.MemberTag.ColCommentMemberTags.Count > 0)
                        {
                            List<string> value;
                            if (OlapView.MemberTag.ColCommentMemberTags.TryGetValue(dimName, out value))
                            {
                                //AddCommentMemberTagItems(viewSection.colTuples[i], dimName, j,
                                //    OlapView.MemberTag.ColMemberTagCommentValues,
                                //    OlapView.MemberTag.ColCommentMemberTags,
                                //    viewSection.colAxisDims,
                                //    OlapView);

                                Assert.AreEqual(2, OlapView.MemberTag.RowCommentMemberTags.Count);
                                Assert.AreEqual(2, OlapView.MemberTag.MemberTagCommentNames.Count);
                                Assert.AreEqual(0, OlapView.MemberTag.ColCommentMemberTags.Count);
                            }
                        }
                    }
                }
            }


            //Invalid Member Tag Intersections
            if (viewSection.invalidMemberTagIntersectionsLC != null)
            {
                Range InvalidMbrTagRange = new Range();

                foreach (lockedCell lockCell in viewSection.invalidMemberTagIntersectionsLC)
                {
                    if (lockCell != null)
                    {
                        CellAddress CellAddr = new CellAddress(OlapView.StartCell.Row - 1 + lockCell.rowIndex, OlapView.StartCell.Col - 1 + lockCell.colIndex);

                        OlapView.MemberTag.AddInvalidLockedCell(CellAddr);

                        InvalidMbrTagRange.AddContiguousRange(CellAddr);
                    }
                }

                if (InvalidMbrTagRange.ContiguousRanges.Count > 0)
                {
                    OlapView.InValidMemberTagRange = InvalidMbrTagRange;
                }
            }

            String[] memberTagData = OlapView.MemberTag.GetMemberTagData("Dept_Desc", viewSection, ViewAxis.Col);
            Assert.AreEqual(13, memberTagData.Length);


        }

        /// <summary>
        /// Adds comment member tags to the axis collections.
        /// </summary>
        /// <param name="axisTuple">View tuple for the axis.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <param name="j">Member position.</param>
        /// <param name="AxisMemberTagCommentValues">The comment member tag values for the axis.</param>
        /// <param name="AxisCommentMemberTags">Comment member tags for the axis.</param>
        /// <param name="axisDims"></param>
        /// <param name="olapView"></param>
        internal void AddCommentMemberTagItems(viewTuple axisTuple, string dimName, int j,
            CachedSelections<string, Intersection, CommentMemberTagData> AxisMemberTagCommentValues,
            IDictionary<string, List<string>> AxisCommentMemberTags,
            string[] axisDims, OlapView olapView)
        {

            List<string> value;
            memberTagDef[] memberTagDefs = ObjectExtension.Load<memberTagDef[]>(ApplicationDataPath, @"memberTagDef.xml");
            MemberTagInfo mbrTagInfo = new MemberTagInfo(memberTagDefs);


            if (AxisCommentMemberTags.TryGetValue(dimName, out value))
            {
                memberTagDef simpleMbr = mbrTagInfo.GetMemberTag(value[0]);

                Intersection inter = olapView.MemberTag.CreateIntersection(axisDims, axisTuple.memberDefs, value[0], simpleMbr);

                List<CommentMemberTagData> data = AxisMemberTagCommentValues.GetCachedSelections(dimName, inter);

                CommentMemberTagData[] newData = CommentMemberTagData.CreateArrayOfCommentMemberTags(value, axisTuple.memberTagCommentNames, axisTuple.memberTagCommentValues);

                foreach (CommentMemberTagData item in newData)
                {
                    if (data == null || !data.Contains(item))
                    {
                        AxisMemberTagCommentValues.CacheAddSelections(
                            dimName,
                            inter,
                            item,
                            true);
                    }
                }
            }
        }
    }
}
