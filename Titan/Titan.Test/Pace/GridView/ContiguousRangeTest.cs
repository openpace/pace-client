﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using NUnit.Framework;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;

namespace Titan.Test
{
    [TestFixture]
    public class ContiguousRangeTest
    {
        [Test]
        public void HashCodeTest()
        {
            ContiguousRange cr1 = new ContiguousRange("R5C50");
            ContiguousRange cr2 = new ContiguousRange(new CellAddress(5, 25));

            Assert.AreNotEqual(cr1.GetHashCode(), cr2.GetHashCode());
        }

        [Test]
        public void ToStringTest()
        {
            ContiguousRange cr1 = new ContiguousRange("R5C25");
            ContiguousRange cr2 = new ContiguousRange(new CellAddress(5, 25));

            Assert.AreEqual(cr1.ToString(), cr2.ToString());
        }

        [Test]
        public void ToA1AddressTest()
        {
            ContiguousRange cr1 = new ContiguousRange("R5C25");
            ContiguousRange cr2 = new ContiguousRange(new CellAddress(5, 25));

            Assert.AreEqual(cr1.A1Address, cr2.A1Address);
        }

        [Test]
        public void ToR1C1AddressTest()
        {
            ContiguousRange cr1 = new ContiguousRange("R5C25");
            ContiguousRange cr2 = new ContiguousRange(new CellAddress(5, 25));

            Assert.AreEqual(cr1.R1C1Address, cr2.R1C1Address);
        }

        [Test]
        public void ToA1AddressTestRange()
        {
            ContiguousRange cr1 = new ContiguousRange("R2C2:R4C4");
            ContiguousRange cr2 = new ContiguousRange(new CellAddress(2, 2), new CellAddress(4, 4));

            Assert.AreEqual(cr1.A1Address, cr2.A1Address);
        }

        [Test]
        public void ToR1C1AddressTestRange()
        {
            ContiguousRange cr1 = new ContiguousRange("R2C2:R4C4");
            ContiguousRange cr2 = new ContiguousRange(new CellAddress(2, 2), new CellAddress(4,4));

            Assert.AreEqual(cr1.R1C1Address, cr2.R1C1Address);
        }

        [Test]
        public void TestRowCount()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C4");

            Assert.AreEqual(cr.Rows.Count, 3);
        }

        [Test]
        public void TestColumnCount()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C6");

            Assert.AreEqual(cr.Columns.Count, 5);
        }

        [Test]
        public void TestPopulateData()
        {
            ContiguousRange cr = new ContiguousRange("R2C2");
            double d = 5.00;
            cr.PopulateData(d);
            Assert.AreEqual(cr.Cells[0].Value, d);
        }

        [Test]
        public void TestInContiguousRangeCellAddress()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C4");

            Assert.IsTrue(cr.InContiguousRange(new CellAddress(2, 3)));
        }

        [Test]
        public void TestInContiguousRangeCell()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C4");

            Assert.IsTrue(cr.InContiguousRange(new Cell(2, 3)));
        }

        [Test]
        public void TestNotInContiguousRangeCellAddress()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C4");

            Assert.IsFalse(cr.InContiguousRange(new CellAddress(5, 3)));
        }

        [Test]
        public void TestNotInContiguousRangeCell()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C4");

            Assert.IsFalse(cr.InContiguousRange(new Cell(6, 3)));
        }

        [Test]
        public void TestInContiguousRangeRow()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C4");

            Assert.IsTrue(cr.InContiguousRange(2));
        }

        [Test]
        public void TestNotInContiguousRangeRow()
        {
            ContiguousRange cr = new ContiguousRange("R2C2:R4C4");

            Assert.IsFalse(cr.InContiguousRange(5));
        }

        [Test]
        public void TestPointerBugInContigousRangeConstructor()
        {
            ContiguousRange cr = new ContiguousRange(new CellAddress(10, 8));

            cr.CombineRange( new ContiguousRange(new CellAddress(10, 9)));

            Assert.AreEqual(cr.R1C1Address, "R10C8:R10C9");
        }
    }
}
