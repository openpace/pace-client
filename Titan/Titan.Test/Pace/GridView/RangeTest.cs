﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Drawing;
using NUnit.Framework;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;

namespace Titan.Test
{
    [TestFixture]
    public class RangeTest
    {
        [Test]
        public void TestConstructor()
        {
            Range range = new Range(new CellAddress(5, 5));
            Assert.AreEqual(range.CellCount, 1);
        }

        [Test]
        public void TestNotEquals()
        {
            Range range =  new Range(new CellAddress(2,4));
            Range range2 = new Range(new CellAddress(5, 10));

            Assert.IsFalse(range.Equals(range2));
        }

        [Test]
        public void TestEquals()
        {
            Range range = new Range(new CellAddress(2, 4));
            Range range2 = new Range(new CellAddress(2, 4));

            Assert.IsTrue(range.Equals(range2));
        }


        [Test]
        public void TestAddContigRange()
        {
            Range range = new Range(new CellAddress(2, 4));

            range.AddContiguousRange(new ContiguousRange(new CellAddress(3,4)));

            Assert.AreEqual(range.ContiguousRanges.Count, 1);
        }

        [Test]
        public void TestAddNonContigRange()
        {
            Range range = new Range(new CellAddress(2, 4));

            range.AddContiguousRange(new ContiguousRange(new CellAddress(25, 4)));

            Assert.AreEqual(range.ContiguousRanges.Count, 2);
        }

        [Test]
        public void TestAddContigRangeViaCellAddress()
        {
            Range range = new Range(new CellAddress(2, 4));

            range.AddContiguousRange(new CellAddress(3, 4));

            Assert.AreEqual(range.ContiguousRanges.Count, 1);
        }

        [Test]
        public void TestAddNonContigRangeViaCellAddress()
        {
            Range range = new Range(new CellAddress(2, 4));

            range.AddContiguousRange(new CellAddress(10, 4));

            Assert.AreEqual(range.ContiguousRanges.Count, 2);
        }


        [Test]
        public void TestAddCombineRange()
        {
            Range range = new Range(new CellAddress(2, 4));

            ContiguousRange cr = new ContiguousRange(new CellAddress(3, 4));

            range.CombineOrAddContiguousRange(cr);

            Assert.AreEqual(range.ContiguousRanges.Count, 1);
        }

        [Test]
        public void TestAddCombineRangeSideBySide()
        {
            Range range = new Range(new CellAddress(2, 4));

            ContiguousRange cr = new ContiguousRange(new CellAddress(2, 5));

            range.CombineOrAddContiguousRange(cr);

            Assert.AreEqual(range.ContiguousRanges.Count, 1);
        }

        [Test]
        public void TestAddCombineRangeNotContig()
        {
            Range range = new Range();

            range.AddContiguousRange(new CellAddress(2, 4));

            ContiguousRange cr = new ContiguousRange(new CellAddress(5, 4));

            range.CombineOrAddContiguousRange(cr);

            Assert.AreEqual(range.ContiguousRanges.Count, 2);
        }

        [Test]
        public void TestPointerBugInContigousRangeConstructor()
        {
            Range range = new Range();

            range.AddContiguousRange(new CellAddress(10, 8));

            range.AddContiguousRange(new CellAddress(10, 9));

            Assert.AreEqual(range.ContiguousRanges[0].R1C1Address, "R10C8:R10C9");
            Assert.AreEqual(range.ContiguousRanges[0].A1Address, "H10:I10");
        }

        [Test]
        public void TestClone()
        {
            Range r1 = new Range(new CellAddress(5, 10));

            Range r2 = (Range) r1.Clone();

            Assert.AreEqual(r1, r2);
        }

        [Test]
        public void TestHash()
        {
            Range r1 = new Range(new CellAddress(5, 10));

            Range r2 = (Range)r1.Clone();

            Assert.AreNotEqual(r1.GetHashCode(), r2.GetHashCode());
        }
    }
}
