﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using NUnit.Framework;
using Titan.Palladium.GridView;

namespace Titan.Test
{
    [TestFixture]
    public class CellAddressTest
    {
        [Test]
        public void HashCodeTest()
        {
            CellAddress cellAddress = new CellAddress(2, 5);
            CellAddress cellAddress2 = new CellAddress(50, 100);

            Assert.AreNotEqual(cellAddress.GetHashCode(), cellAddress2.GetHashCode());
        }

        [Test]
        public void ToStringNotEqual()
        {
            CellAddress cellAddress = new CellAddress(2, 5);
            CellAddress cellAddress2 = new CellAddress(50, 50);

            Assert.AreNotEqual(cellAddress.ToString(), cellAddress2.ToString());
        }

        [Test]
        public void ToStringEqual()
        {
            CellAddress cellAddress = new CellAddress(2, 5);
            CellAddress cellAddress2 = new CellAddress(2, 5);

            Assert.AreEqual(cellAddress.ToString(), cellAddress2.ToString());
        }

        [Test]
        public void A1AddressNotEqual()
        {
            CellAddress cellAddress = new CellAddress(2, 5);
            CellAddress cellAddress2 = new CellAddress(50, 50);

            Assert.AreNotEqual(cellAddress.A1Address, cellAddress2.A1Address);
        }

        [Test]
        public void R1C1AddressNotEqual()
        {
            CellAddress cellAddress = new CellAddress(2, 5);
            CellAddress cellAddress2 = new CellAddress(50, 5050);

            Assert.AreNotEqual(cellAddress.R1C1Address, cellAddress2.R1C1Address);
        }

        [Test]
        public void A1AddressEqual()
        {
            CellAddress cellAddress = new CellAddress(2, 5);
            CellAddress cellAddress2 = new CellAddress(2, 5);

            Assert.AreEqual(cellAddress.A1Address, cellAddress2.A1Address);
        }

        [Test]
        public void R1C1AddressEqual()
        {
            CellAddress cellAddress = new CellAddress(10, 50);
            CellAddress cellAddress2 = new CellAddress(10, 50);

            Assert.AreEqual(cellAddress.R1C1Address, cellAddress2.R1C1Address);
        }

        [Test]
        public void ColGetTest()
        {
            CellAddress cellAddress = new CellAddress(10, 50);

            Assert.AreEqual(cellAddress.Col, 50);
        }

        [Test]
        public void RowGetTest()
        {
            CellAddress cellAddress = new CellAddress(10, 50);

            Assert.AreEqual(cellAddress.Row, 10);
        }

        [Test]
        public void ColSetTest()
        {
            CellAddress cellAddress = new CellAddress(10, 50);

            cellAddress.Col = 100;

            Assert.AreEqual(cellAddress.Col, 100);
        }

        [Test]
        public void RowSetTest()
        {
            CellAddress cellAddress = new CellAddress(10, 50);

            cellAddress.Row = 500;

            Assert.AreEqual(cellAddress.Row, 500);
        }

        [Test]
        public void TestEquals()
        {
            CellAddress cellAddress1 = new CellAddress();
            cellAddress1.Row = 2;
            cellAddress1.Col = 4;

            CellAddress cellAddress2 = new CellAddress(5,10);

            Assert.IsFalse(cellAddress1.Equals(cellAddress2));
        }

        [Test]
        public void TestCompareToGreatorThanCol()
        {
            CellAddress cellAddress1 = new CellAddress();
            cellAddress1.Row = 5;
            cellAddress1.Col = 10;

            CellAddress cellAddress2 = new CellAddress(2, 4);

            Assert.GreaterOrEqual(cellAddress1.CompareTo(cellAddress2), 1);
        }

        [Test]
        public void TestCompareToGreatorRow()
        {
            CellAddress cellAddress1 = new CellAddress(7, 3);

            CellAddress cellAddress2 = new CellAddress(29, 3);

            Assert.AreEqual(cellAddress2.CompareTo(cellAddress1), 1);
        }

        [Test]
        public void TestCompareToGreatorRow2()
        {
            CellAddress cellAddress1 = new CellAddress(35, 3);

            CellAddress cellAddress2 = new CellAddress(29, 3);

            Assert.AreEqual(cellAddress2.CompareTo(cellAddress1), -1);
        }

        [Test]
        public void TestCompareToGreatorCol()
        {
            CellAddress cellAddress1 = new CellAddress(7, 3);

            CellAddress cellAddress2 = new CellAddress(7, 15);

            Assert.AreEqual(cellAddress2.CompareTo(cellAddress1), 1);
        }

        [Test]
        public void TestCompareToLessRow()
        {
            CellAddress cellAddress1 = new CellAddress(7, 3);

            CellAddress cellAddress2 = new CellAddress(29, 3);

            Assert.AreEqual(cellAddress1.CompareTo(cellAddress2), -1);
        }

        [Test]
        public void TestCompareToLessCol()
        {
            CellAddress cellAddress1 = new CellAddress(7, 3);

            CellAddress cellAddress2 = new CellAddress(7, 15);

            Assert.AreEqual(cellAddress1.CompareTo(cellAddress2), -1);
        }


        [Test]
        public void TestCompareToGreatorThanRow()
        {
            CellAddress cellAddress1 = new CellAddress();
            cellAddress1.Row = 5;
            cellAddress1.Col = 4;

            CellAddress cellAddress2 = new CellAddress(2, 4);

            Assert.GreaterOrEqual(cellAddress1.CompareTo(cellAddress2), 1);
        }


        [Test]
        public void TestCompareToLessThanCol()
        {
            CellAddress cellAddress1 = new CellAddress();
            cellAddress1.Row = 2;
            cellAddress1.Col = 2;

            CellAddress cellAddress2 = new CellAddress(2, 4);

            Assert.LessOrEqual(cellAddress1.CompareTo(cellAddress2), 0);
        }

        [Test]
        public void TestCompareToLessThanRow()
        {
            CellAddress cellAddress1 = new CellAddress();
            cellAddress1.Row = 2;
            cellAddress1.Col = 2;

            CellAddress cellAddress2 = new CellAddress(5, 2);

            Assert.LessOrEqual(cellAddress1.CompareTo(cellAddress2), 0);
        }

        [Test]
        public void TestCompareToEqual()
        {
            CellAddress cellAddress1 = new CellAddress();
            cellAddress1.Row = 2;
            cellAddress1.Col = 2;

            CellAddress cellAddress2 = new CellAddress(2, 2);

            Assert.AreEqual(cellAddress1.CompareTo(cellAddress2), 0);
        }

        [Test]
        public void TestCloneToString()
        {
            CellAddress cellAddress1 = new CellAddress(5, 10);

            CellAddress cellClone = cellAddress1.DeepClone();

            Assert.AreEqual(cellAddress1.ToString(), cellClone.ToString());
        }

        [Test]
        public void TestStringConstructor()
        {
            CellAddress cell = new CellAddress("R5C56");

            Assert.AreEqual(cell.A1Address, "BD5");
            Assert.AreEqual(cell.R1C1Address, "R5C56");
        }

    }
}
