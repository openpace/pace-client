﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using NUnit.Framework;
using Pace.mdb;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.PafService;
using Titan.Palladium;

namespace Titan.Test
{
    [TestFixture]
    public class SimpleTreeTest : BaseTest
    {

        private string[] _dims;

        [TestFixtureSetUp]
        public void Init()
        {
            _dims = new[] { "ClimateZone", "Indoor.Outdoor", "Location", "Measures", "PlanType", "Product", "Season", "State", "**TIME.HORIZON**", "Time", "Version", "Years" };
        }


        [Test]
        public void TestUncompression()
        {
            List<SimpleDimTree> simpleTrees = new List<SimpleDimTree>();

            foreach(string dim in _dims)
            {

                pafSimpleDimTree tree = ObjectExtension.Load<pafSimpleDimTree>(ApplicationDataPath, dim.RemoveInvalidFileChars() + ".xml");

                SimpleDimTree t = new SimpleDimTree(
                    tree.aliasTableNames,
                        tree.traversedMembers,
                        tree.compAliasTableNames,
                        tree.compMemberIndex,
                        tree.compParentChild,
                        tree.compressed,
                        tree.elementDelim,
                        tree.groupDelim,
                        tree.id,
                        tree.memberObjects.BuildArray(),
                        tree.rootKey);

                Assert.AreEqual(dim, t.Id);
                Assert.AreEqual(t.MemberObjects.Length, tree.traversedMembers.Length);
                Assert.AreEqual(t.TraversedMembers.Length, tree.traversedMembers.Length);
                Assert.AreEqual(t.RootKey, tree.rootKey);


                simpleTrees.Add(t);
            }

            Assert.AreEqual(simpleTrees.Count, 12);

        }
    }
}
