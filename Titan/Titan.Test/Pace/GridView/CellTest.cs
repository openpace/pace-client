﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Drawing;
using NUnit.Framework;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;

namespace Titan.Test
{
    [TestFixture]
    public class CellTest
    {
        [Test]
        public void ColumnTest()
        {
            Cell cell = new Cell(5, 10);
            Assert.AreEqual(cell.CellAddress.Col, 10);
        }

        [Test]
        public void RowTest()
        {
            Cell cell = new Cell(5, 10);
            Assert.AreEqual(cell.CellAddress.Row, 5);
        }

        [Test]
        public void ValueTest()
        {
            Cell cell = new Cell(5, 10, 5000);
            Assert.AreEqual(cell.Value, 5000);
        }

        [Test]
        public void FontColorTest()
        {
            Cell cell = new Cell(5, 10, 5000);
            cell.FontColor = Color.DeepPink;
            Assert.AreEqual(cell.FontColor, Color.DeepPink);
        }

        [Test]
        public void EqualsTest()
        {
            Cell cell = new Cell(5, 10, 5000);
            Cell cell2 = new Cell(5, 10, 5000);
            Assert.IsTrue(cell.Equals(cell2));
        }

        [Test]
        public void Equals2Test()
        {
            Cell cell = new Cell(5, 10);
            Cell cell2 = new Cell(5, 10);
            Assert.IsTrue(cell.Equals(cell2));
        }


        [Test]
        public void ContainsTest()
        {
            List<Cell> cells = new List<Cell>();
            cells.Add(new Cell(1, 1));
            cells.Add(new Cell(11, 50));
            cells.Add(new Cell(5, 100));

            Cell cellToCheck = new Cell(11, 50);

            Assert.IsTrue(cells.Contains(cellToCheck));
        }

        [Test]
        public void Contains3Test()
        {
            List<Cell> cells = new List<Cell>();
            cells.Add(new Cell(1, 1, 500));
            cells.Add(new Cell(11, 50, 1));
            cells.Add(new Cell(5, 100, 0));

            Cell cellToCheck = new Cell(11, 50, 1);

            Assert.IsTrue(cells.Contains(cellToCheck));
        }

        [Test]
        public void Contains4Test()
        {
            List<Cell> cells = new List<Cell>();
            cells.Add(new Cell(1, 1, 500));
            cells.Add(new Cell(11, 50, 1));
            cells.Add(new Cell(5, 100, 0));

            Cell cellToCheck = new Cell(11, 50, 100);

            Assert.IsFalse(cells.Contains(cellToCheck));
        }

    }
}
