﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using NUnit.Framework;
using Titan.Pace;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.DataStructures;
using Titan.PafService;
using Titan.Palladium;
using Titan.Palladium.DataStructures;

namespace Titan.Test
{
    [TestFixture]
    public class PafSimpleCoordListTest
    {

        private simpleCoordList _simpleCoordList;
        private const string CompressedData = "0^1^2^3^4^5^6^|^7^8^9^10^11^12^13^";
        private const string CompressedStringTable = "Measures^PlanType^Time^Version^Years^Product^Location^SLS_DLR^ClassChn^WK27^WP^FY2006^CLS608-10^StoreTotal^";

        [TestFixtureSetUp]
        public void Init()
        {
            if (String.IsNullOrEmpty(PafAppConstants.ELEMENT_DELIM))
            {
                PafAppConstants.ELEMENT_DELIM = "^";
            }

            if (String.IsNullOrEmpty(PafAppConstants.GROUP_DELIM))
            {
                PafAppConstants.GROUP_DELIM = "|^";
            }


            _simpleCoordList = new simpleCoordList();
            _simpleCoordList.axis = new[] { "Measures", "PlanType", "Time", "Version", "Years", "Product", "Location" };
            _simpleCoordList.coordinates = new[] { "SLS_DLR", "ClassChn", "WK27", "WP", "FY2006", "CLS608-10", "StoreTotal" };
            _simpleCoordList.compressed = false;
            _simpleCoordList.compressedData = null;
        }

        [Test]
        public void TestConstructor()
        {
            PafSimpleCoordList pafSimpleCoordList = new PafSimpleCoordList(_simpleCoordList);

            Assert.AreEqual(pafSimpleCoordList.GetSimpleCoordList.axis, _simpleCoordList.axis);
            Assert.AreEqual(pafSimpleCoordList.GetSimpleCoordList.coordinates, _simpleCoordList.coordinates);
        }

        [Test]
        public void TestBuildIntersections()
        {
            Intersection[] intersections = _simpleCoordList.ToIntersections();

            Assert.AreEqual(intersections.Length, 1);
            Assert.AreEqual(intersections[0].AxisSequence, _simpleCoordList.axis);
            Assert.AreEqual(intersections[0].Coordinates, _simpleCoordList.coordinates);
        }


        [Test]
        public void TestBuildSimpleCoordList()
        {
            Intersection[] intersections = _simpleCoordList.ToIntersections();

            Assert.AreEqual(intersections.Length, 1);

            simpleCoordList coordList = intersections[0].ToSimpleCoordList();

            Assert.AreEqual(coordList.axis, intersections[0].AxisSequence);
            Assert.AreEqual(coordList.coordinates, intersections[0].Coordinates);
        }

        [Test]
        public void TestBuildSimpleCoordListFromArray()
        {
            Intersection[] intersections = _simpleCoordList.ToIntersections();

            Assert.AreEqual(intersections.Length, 1);

            simpleCoordList coordList = intersections.ToSimpleCoordList(false);

            Assert.AreEqual(coordList.axis, intersections[0].AxisSequence);
            Assert.AreEqual(coordList.coordinates, intersections[0].Coordinates);
        }

        [Test]
        public void TestDelmitArray()
        {
            PafSimpleCoordList simpleCoordList = new PafSimpleCoordList(_simpleCoordList, true);
                       
            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.compressedData, CompressedData);

            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.compressedStringTable, CompressedStringTable);

            simpleCoordList simpleCoord = new simpleCoordList();
            simpleCoord.compressedData = simpleCoordList.GetSimpleCoordList.compressedData;
            simpleCoord.compressedStringTable = simpleCoordList.GetSimpleCoordList.compressedStringTable;

            PafSimpleCoordList simpleCoordList2 = new PafSimpleCoordList(simpleCoord);
            simpleCoordList2.uncompressData();

                

            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.axis, simpleCoordList2.GetSimpleCoordList.axis);
            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.coordinates, simpleCoordList2.GetSimpleCoordList.coordinates);

            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.compressedData, simpleCoordList2.GetSimpleCoordList.compressedData);
            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.compressedStringTable, simpleCoordList2.GetSimpleCoordList.compressedStringTable);


            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.compressedData, CompressedData);
            Assert.AreEqual(simpleCoordList2.GetSimpleCoordList.compressedData, CompressedData);

            Assert.AreEqual(simpleCoordList.GetSimpleCoordList.compressedStringTable, CompressedStringTable);
            Assert.AreEqual(simpleCoordList2.GetSimpleCoordList.compressedStringTable, CompressedStringTable);
        }
    } 
}
