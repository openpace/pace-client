﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Diagnostics;
using NUnit.Framework;
using Titan.Palladium.DataStructures;

namespace Titan.Test
{
    [TestFixture]
    public class IntersectionTest
    {
        private readonly string[] _axis = new[] { "Measures", "PlanType", "Time", "Version", "Years", "Product", "Location" };

        private readonly string[] _coord = new[] { "SLS_DLR", "ClassChn", "WK27", "WP", "FY2006", "CLS608-10", "StoreTotal" };

        private readonly string[] _coord2 = new[] { "SLS_DLR", "ClassChn", "WK28", "WP", "FY2006", "CLS608-10", "StoreTotal" };


        [Test]
        public void TestCreateIntersection()
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<Intersection> tempList = new List<Intersection>();
            for (int i = 0; i < 750000; i++)
            {
                Intersection temp = new Intersection(_axis, _coord);
                tempList.Add(temp);
            }
            sw.Stop();
            Trace.WriteLine("TestCreateIntersection: " + sw.ElapsedMilliseconds);
        }

        [Test]
        public void TestCreateIntersection2()
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<Pace.DataStructures.Old.Intersection> tempList = new List<Pace.DataStructures.Old.Intersection>();
            for (int i = 0; i < 750000; i++)
            {
                Pace.DataStructures.Old.Intersection temp = new Pace.DataStructures.Old.Intersection(_axis, _coord);
                tempList.Add(temp);
            }
            sw.Stop();
            Trace.WriteLine("TestCreateIntersection2: " + sw.ElapsedMilliseconds);
        }

        [Test]
        public void TestCoordinatesAreNull()
        {
            string[] coordNotInit = new[] { "[not intialized]", "[not intialized]", "[not intialized]", "[not intialized]", "[not intialized]", "[not intialized]", "[not intialized]" };

            Intersection inter = new Intersection(_axis);

            Assert.AreEqual(inter.Coordinates, coordNotInit);
        }

        [Test]
        public void TestCoordinatesAndAxisCons()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Assert.AreEqual(inter.AxisSequence, _axis);

            Assert.AreEqual(inter.Coordinates, _coord);
        }

        [Test]
        public void TestCoordinatesAndAxis()
        {
            Intersection inter = new Intersection(_axis);

            inter.SetCoordinates(_coord);

            Assert.AreEqual(inter.AxisSequence, _axis);

            Assert.AreEqual(inter.Coordinates, _coord);
        }

        [Test]
        public void TestAddingCoordinates()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = new Intersection(_axis);

            inter2.SetCoordinates(_coord);

            Assert.AreEqual(inter.AxisSequence, inter2.AxisSequence);

            Assert.AreEqual(inter.Coordinates, inter2.Coordinates);

            Assert.AreEqual(inter, inter2);
        }

        [Test]
        public void TestIntersectionToString()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = (Intersection) inter.Clone();

            Assert.AreEqual(inter.ToString(), inter2.ToString());

        }

        [Test]
        public void TestIntersectionToLongString()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = (Intersection)inter.Clone();

            Assert.AreEqual(inter.ToLongString(), inter2.ToLongString());

        }


        [Test]
        public void TestIntersectionSetCoord()
        {
            Intersection inter = new Intersection(_axis, _coord);

            inter.SetCoordinate("Years", "FY2009");

            Assert.AreEqual(inter.GetCoordinate("Years"), "FY2009");

        }

        [Test]
        public void TestIntersectionContainsCoord()
        {
            Intersection inter2 = new Intersection(_axis);

            inter2.SetCoordinates(_coord);

            Assert.IsTrue(inter2.ContainsCoordinate("WK27"));
        }

        [Test]
        public void TestIntersectionContainsAxis()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Assert.IsTrue(inter.ContainsAxis("Years"));

        }

        [Test]
        public void TestIntersectionUniqueId()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = new Intersection(_axis, _coord);

            Assert.AreEqual(inter.UniqueID, inter2.UniqueID);

        }

        [Test]
        public void TestIntersectionUniqueIdNotEqual()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = new Intersection(_axis, _coord2);

            Assert.AreNotEqual(inter.UniqueID, inter2.UniqueID);

        }

        [Test]
        public void TestIntersectionGetHashCode()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = new Intersection(_axis, _coord);

            Assert.AreEqual(inter.GetHashCode(), inter2.GetHashCode());

        }

        [Test]
        public void TestIntersectionEquals()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = new Intersection(_axis, _coord);

            Assert.IsTrue(inter.Equals(inter2));

        }

        [Test]
        public void TestIntersectionEqualsFail()
        {
            Intersection inter = new Intersection(_axis, _coord);

            Intersection inter2 = new Intersection(_axis, _coord2);

            Assert.IsFalse(inter.Equals(inter2));

        }

    }
}
