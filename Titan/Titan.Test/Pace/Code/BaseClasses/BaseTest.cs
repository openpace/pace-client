﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.IO;
using System.Reflection;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Extensions;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.Rules;
using Titan.PafService;
using Titan.Palladium;
using Titan.Palladium.GridView;

namespace Titan.Test
{
    public abstract class BaseTest
    {
        private const string DataFolder = "Data";
        public const int ViewSectionNumber = 0;
        public int StartRow = GRID_START_ROW;
        private const int GRID_START_ROW = 4;
        protected string ApplicationPath ;
        protected string ApplicationDataPath;
        protected pafView PafView;
        internal OlapView OlapView;
        internal ViewStateInfo ViewStateInfo;
        internal ExcelBuildView ExcelBuildView;

        protected BaseTest()
        {
            string filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            ApplicationPath = Path.GetDirectoryName(filePath);
            ApplicationDataPath = Path.Combine(ApplicationPath, DataFolder);
           
        }

        protected void CreateViewState()
        {
            int i = 1;
            ViewStateInfo = new ViewStateInfo(i.RandomNumber(), PafView.name, null, new Format(), new LockMngr());
            OlapView = ViewStateInfo.GetOlapView();
            ExcelBuildView = new ExcelBuildView(new WorksheetClass(), PafView.name, false, new Format(), false);
        }

    }
}
