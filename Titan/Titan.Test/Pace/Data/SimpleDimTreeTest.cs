﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Data;
using Titan.PafService;
using Titan.Palladium;

namespace Titan.Test
{
    [TestFixture]
    public class SimpleDimTreeTest : BaseTest
    {
        private string[] _dims;
        private SimpleDimTrees _simpleDimTrees;
        

        [TestFixtureSetUp]
        public void Init()
        {
            _dims = new[] { "ClimateZone", "Indoor.Outdoor", "Location", "Measures", "PlanType", "Product", "Season", "State", "**TIME.HORIZON**", "Time", "Version", "Years" };
            string[] baseDims = new[] {  "Location", "Measures", "PlanType", "Product", "**TIME.HORIZON**", "Time", "Version", "Years" };
            string[] attreDims = new[] { "ClimateZone", "Indoor.Outdoor", "Season", "State",  };

            List<pafSimpleDimTree> baseTrees = new List<pafSimpleDimTree>();
            List<pafSimpleDimTree> attrTrees = new List<pafSimpleDimTree>();
            _simpleDimTrees = new SimpleDimTrees();

            foreach (string dim in _dims)
            {
                pafSimpleDimTree tree = ObjectExtension.Load<pafSimpleDimTree>(ApplicationDataPath, dim.RemoveInvalidFileChars() + ".xml");

                if(baseDims.Contains(dim))
                {
                    baseTrees.Add(tree);
                }

                if (attreDims.Contains(dim))
                {
                    attrTrees.Add(tree);
                }

            }

            //Add base dimension
            foreach (pafSimpleDimTree tree in baseTrees)
            {
                _simpleDimTrees.AddTree(tree, true, false, null, false);
            }

            //Add Attribute dimension
            foreach (pafSimpleDimTree tree in attrTrees)
            {
                _simpleDimTrees.AddTree(tree, false, false, null, false);
            }
        }

        [Test]
        public void TestDimensionTableCount()
        {
            Assert.AreEqual(12, _simpleDimTrees.Dimension.Rows.Count);
        }

        [Test]
        public void TestMembersTableCount()
        {
            Assert.AreEqual(1095, _simpleDimTrees.Members.Rows.Count);
        }

        [Test]
        public void TestAliasTableCount()
        {
            Assert.AreEqual(3, _simpleDimTrees.AliasTable.Rows.Count);
        }

        [Test]
        public void TestMemberAliasTableCount()
        {
            Assert.AreEqual(2995, _simpleDimTrees.MemberAlias.Rows.Count);
        }

        [Test]
        public void TestDimensionAliasTableCount()
        {
            Assert.AreEqual(12, _simpleDimTrees.Dimension.Rows.Count);
        }

        [Test]
        public void TestGetFloorMembersProduct()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers("Product", "DIV09");
            Assert.AreEqual(10, temp.Count);
        }

        [Test]
        public void TestGetFloorMembersMeasures()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers("Measures", "Measures");
            Assert.AreEqual(108, temp.Count);
        }
        [Test]
        public void TestGetFloorMembersMeasures2()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers("Measures", "GL");
            Assert.AreEqual(2, temp.Count);
        }
        [Test]
        public void TestGetFloorMembersTime()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers("Time", "S02.53");
            Assert.AreEqual(27, temp.Count);
        }
    }
}
