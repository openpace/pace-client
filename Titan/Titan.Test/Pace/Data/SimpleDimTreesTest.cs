﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Data;
using NUnit.Framework;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Data;
using Titan.Palladium;
using Titan.Palladium.DataStructures;

namespace Titan.Test
{
    [TestFixture]
    public class SimpleDimTreesTest : BaseTest
    {
        private SimpleDimTrees _simpleDimTrees;
        private SimpleDimTrees _simpleDimTreesWithLocks;
        private string[] _dimensionPriority;
        private const string Product = "Product";
        private const string TimeHorizon = "**TIME.HORIZON**";
        private const string Measures = "Measures";
        private const string Time = "Time";
        private const string Years = "Years";
        private const string Version = "Version";
        private const string Location = "Location";



        [TestFixtureSetUp]
        public void Init()
        {
            _simpleDimTrees = ObjectExtension.Load<SimpleDimTrees>(ApplicationDataPath, @"SimpleDimTrees.xml");
            _simpleDimTreesWithLocks = ObjectExtension.Load<SimpleDimTrees>(ApplicationDataPath, @"SimpleDimTreesWithLocks.xml");
            _dimensionPriority = new[] { Measures, "PlanType", Time, Version, Years, Product, Location };
        }

        [Test]
        public void TestDimensionTableCount()
        {
            Assert.AreEqual(13, _simpleDimTrees.Dimension.Rows.Count);
        }

        [Test]
        public void TestMembersTableCount()
        {
            Assert.AreEqual(224, _simpleDimTrees.Members.Rows.Count);
        }

        [Test]
        public void TestAliasTableCount()
        {
            Assert.AreEqual(3, _simpleDimTrees.AliasTable.Rows.Count);
        }

        [Test]
        public void TestMemberAliasTableCount()
        {
            Assert.AreEqual(602, _simpleDimTrees.MemberAlias.Rows.Count);
        }

        [Test]
        public void TestDimensionAliasTableCount()
        {
            Assert.AreEqual(13, _simpleDimTrees.Dimension.Rows.Count);
        }

        [Test]
        public void TestGetFloorMembersProduct()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers(Product, "DIV09");
            Assert.AreEqual(10, temp.Count);
        }

        [Test]
        public void TestGetFloorMembersMeasures()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers(Measures, Measures);
            Assert.AreEqual(83, temp.Count);

            temp = _simpleDimTrees.GetFloorMembers(Measures, "GL");
            Assert.AreEqual(2, temp.Count);
        }

        [Test]
        public void TestGetFloorMembersTime()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers(Time, "S02");
            Assert.AreEqual(26, temp.Count);
        }

        [Test]
        public void TestGetFloorMembersTimeFailure()
        {
            List<SimpleDimTrees.MembersRow> temp = _simpleDimTrees.GetFloorMembers(Time, "S02.53");
            Assert.IsNull(temp);
        }

        [Test]
        public void TestGetFirstLevelZeroMember()
        {
            var temp = _simpleDimTrees.GetFirstLevelZeroMember(Time);
            Assert.AreEqual("WK27", temp);

            temp = _simpleDimTrees.GetFirstLevelZeroMember(Product );
            Assert.AreEqual("CLS608-10", temp);

            temp = _simpleDimTrees.GetFirstLevelZeroMember(Location);
            Assert.IsNull(temp);
        }

        [Test]
        public void TestGetFirstMember()
        {
            long dimKey = _simpleDimTrees.GetDimensionKey(Time);
            int maxMbrLevel = _simpleDimTrees.GetMemberMaxLevel(dimKey, true);

            SimpleDimTrees.MembersRow temp = _simpleDimTrees.GetFirstMember(dimKey, maxMbrLevel);
            Assert.AreEqual(2, temp.GetMembersRows().Length);
        }

        [Test]
        public void TestGetParentMemberName()
        {
            string result = _simpleDimTrees.GetParentMemberName(Time, "WK28");
            Assert.AreEqual("Aug", result);

            result = _simpleDimTrees.GetParentMemberName(Time, "Q3");
            Assert.AreEqual("S02", result);
        }

        [Test]
        public void TestGetChildren()
        {
            long dimKey = _simpleDimTrees.GetDimensionKey(Time);
            var result = _simpleDimTrees.GetChildren(dimKey, "Q3");
            Assert.AreEqual(3, result.Count);

            result = _simpleDimTrees.GetChildren(dimKey, "WK28");
            Assert.AreEqual(0, result.Count);

        }

        [Test]
        public void TestGetMember()
        {
            long dimKey = _simpleDimTrees.GetDimensionKey(Time);
            var result = _simpleDimTrees.GetMember(dimKey, "Q3");
            Assert.AreEqual(2, result.Level);
            var parent = _simpleDimTrees.GetParentMemberName(Time, "Q3");
            Assert.AreEqual(parent, result.MembersRowParent.MemberName);
        }

        [Test]
        public void TestGetFirstMemberProduct()
        {
            long dimKey = _simpleDimTrees.GetDimensionKey(Product);
            int maxMbrLevel = _simpleDimTrees.GetMemberMaxLevel(dimKey, true);

            SimpleDimTrees.MembersRow temp = _simpleDimTrees.GetFirstMember(dimKey, maxMbrLevel);
            Assert.AreEqual(2, temp.GetMembersRows().Length);
        }

        [Test]
        public void TestGetAllMembers()
        {
            var results = _simpleDimTrees.GetAllMembers();
            foreach (var result in results)
            {
                switch(result.Key)
                {
                    case Product:
                        Assert.AreEqual(13, result.Value.Count);
                        break;
                    case Time:
                        Assert.AreEqual(35, result.Value.Count);
                        break;
                    case Years:
                        Assert.AreEqual(1, result.Value.Count);
                        break;
                    case Location:
                        Assert.AreEqual(1, result.Value.Count);
                        break;
                    case Measures:
                        Assert.AreEqual(88, result.Value.Count);
                        break;
                    case Version:
                        Assert.AreEqual(22, result.Value.Count);
                        break;
                }
            }
        }

        [Test]
        public void TestGetFloorMembersNameProduct()
        {
            List<string> temp = _simpleDimTrees.GetFloorMembersName(Product, "DIV09");
            Assert.AreEqual(10, temp.Count);
        }

        [Test]
        public void TestGetDimensionKey()
        {
            long key = _simpleDimTrees.GetDimensionKey(Product, false);
            Assert.AreEqual(2, key);
        }

        [Test]
        public void TestGetDimensionKeyHidden()
        {
            long key = _simpleDimTrees.GetDimensionKey(TimeHorizon, true);
            Assert.AreEqual(8, key);
        }

        [Test]
        public void TestGetDimensionKeyHiddenFailure()
        {
            long key = _simpleDimTrees.GetDimensionKey(TimeHorizon, false);
            Assert.AreEqual(-1, key);
        }

        [Test]
        public void TestGetDimensionKeyFailure()
        {
            long key = _simpleDimTrees.GetDimensionKey("foobar", true);
            Assert.AreEqual(-1, key);
        }

        [Test]
        public void TestGetMemberKey()
        {
            long key = _simpleDimTrees.GetMemberKey("DPT608", Product);
            Assert.AreEqual(37, key);
        }

        [Test]
        public void TestGetDimensionIsHidden()
        {
            bool result = _simpleDimTrees.GetDimensionIsHidden(Product);
            Assert.IsFalse(result);

            result = _simpleDimTrees.GetDimensionIsHidden(TimeHorizon);
            Assert.IsTrue(result);

        }

        [Test]
        public void TestGetOffset()
        {
            string offset = _simpleDimTrees.GetOffset(Time, "WK30", 1);
            Assert.AreEqual(offset, "WK31");

            offset = _simpleDimTrees.GetOffset(Time, "WK30", -1);
            Assert.AreEqual(offset, "WK29");

            offset = _simpleDimTrees.GetOffset(Time, "Q3", 1);
            Assert.AreEqual(offset, "Q4");

            offset = _simpleDimTrees.GetOffset(Time, "Q3", -1);
            Assert.IsNullOrEmpty(offset);

            //Jan.53, WK53

            offset = _simpleDimTreesWithLocks.GetOffset(Time, "WK30", 1);
            Assert.AreEqual(offset, "WK31");

            offset = _simpleDimTreesWithLocks.GetOffset(Time, "WK30", -1);
            Assert.AreEqual(offset, "WK29");

            //=
            offset = _simpleDimTreesWithLocks.GetOffset(Time, "Jan.53", 1);
            Assert.IsNullOrEmpty(offset);

            offset = _simpleDimTreesWithLocks.GetOffset(Time, "Jan.53", -1);
            Assert.AreEqual(offset, "Dec");

            //=
            offset = _simpleDimTreesWithLocks.GetOffset(Time, "WK53", 1);
            Assert.IsNullOrEmpty(offset);

            offset = _simpleDimTreesWithLocks.GetOffset(Time, "WK53", -1);
            Assert.AreEqual(offset, "WK52");
        }

        [Test]
        public void TestGetDimensionNames()
        {
            List<string> result = _simpleDimTrees.GetDimensionNames();
            Assert.AreEqual(12, result.Count);
        }

        [Test]
        public void TestGetDimensionNamesBaseOnly()
        {
            List<string> result = _simpleDimTrees.GetDimensionNames(true);
            Assert.AreEqual(7, result.Count);
        }



        [Test]
        public void TestIsBaseDimension()
        {
            bool isBase = _simpleDimTrees.IsDimensionBase(Product);
            Assert.IsTrue(isBase);

            isBase = _simpleDimTrees.IsDimensionBase("State");
            Assert.IsFalse(isBase);
        }

        [Test]
        public void TestIsDimensionSynthetic()
        {
            bool result = _simpleDimTrees.IsDimensionSynthetic(Years);
            Assert.IsFalse(result);

            result = _simpleDimTreesWithLocks.IsDimensionSynthetic(Years);
            Assert.IsTrue(result);
        }

        [Test]
        public void TestGetDimensionLevelZeroOnly()
        {
            bool result = _simpleDimTrees.GetDimensionLevelZeroOnly(Version);
            Assert.IsTrue(result);

            result = _simpleDimTrees.GetDimensionLevelZeroOnly(Product);
            Assert.IsFalse(result);
        }

        [Test]
        public void TestGetMemberMaxLevel()
        {
            int result = _simpleDimTrees.GetMemberMaxLevel(1, true);
            Assert.AreEqual(3, result);

            result = _simpleDimTrees.GetMemberMaxLevel(1, false);
            Assert.AreEqual(3, result);

            long dimKey = _simpleDimTrees.GetDimensionKey(Version, false);

            result = _simpleDimTrees.GetMemberMaxLevel(dimKey, true);
            Assert.AreEqual(0, result);

            result = _simpleDimTrees.GetMemberMaxLevel(dimKey, false);
            Assert.AreEqual(3, result);
        }

        [Test]
        public void TestGetRoot()
        {
            SimpleDimTrees.MembersRow result = _simpleDimTrees.GetRoot(Product);
            Assert.AreEqual("DIV09", result.MemberName);

            result = _simpleDimTrees.GetRoot(Time);
            Assert.AreEqual("S02", result.MemberName);

            result = _simpleDimTreesWithLocks.GetRoot(Product);
            Assert.AreEqual("TotProd", result.MemberName);

            result = _simpleDimTreesWithLocks.GetRoot(Time);
            Assert.AreEqual("Total.Year.53", result.MemberName);
        }

        [Test]
        public void TestGetSyntheticMembers()
        {
            List<string> result = _simpleDimTrees.GetSyntheticMembers(Years);
            Assert.IsNull(result);

            result = _simpleDimTreesWithLocks.GetSyntheticMembers(Years);
            Assert.AreEqual(2, result.Count);

            result = _simpleDimTrees.GetAliasTables(Product, false);
            Assert.AreEqual(3, result.Count);
        }

        [Test]
        public void TestGetLockIds()
        {
            List<long> result = _simpleDimTreesWithLocks.GetLockIds(false);
            Assert.AreEqual(22, result.Count);
        }

        [Test]
        public void TestToTreeListDataTable()
        {
            DataTable result = _simpleDimTrees.ToTreeListDataTable(Product);
            Assert.AreEqual(13, result.Rows.Count);

            result = _simpleDimTrees.ToTreeListDataTable(Time);
            Assert.AreEqual(35, result.Rows.Count);

            result = _simpleDimTrees.ToTreeListDataTable(Years);
            Assert.AreEqual(1, result.Rows.Count);

            result = _simpleDimTrees.ToTreeListDataTable(Measures);
            Assert.AreEqual(88, result.Rows.Count);

            result = _simpleDimTreesWithLocks.ToTreeListDataTable(Product);
            Assert.AreEqual(693, result.Rows.Count);

            result = _simpleDimTreesWithLocks.ToTreeListDataTable(Time);
            Assert.AreEqual(72, result.Rows.Count);

            result = _simpleDimTreesWithLocks.ToTreeListDataTable(Years);
            Assert.AreEqual(3, result.Rows.Count);

            result = _simpleDimTreesWithLocks.ToTreeListDataTable(Measures);
            Assert.AreEqual(114, result.Rows.Count);
        }

        [Test]
        public void TestToGridDataTable()
        {
            DataTable result = _simpleDimTreesWithLocks.ToGridDataTable(Product);
            Assert.AreEqual(26, result.Rows.Count);

            result = _simpleDimTreesWithLocks.ToGridDataTable(Time);
            Assert.AreEqual(26, result.Rows.Count);

            result = _simpleDimTreesWithLocks.ToGridDataTable(Years);
            Assert.AreEqual(26, result.Rows.Count);

            result = _simpleDimTreesWithLocks.ToGridDataTable(Measures);
            Assert.AreEqual(26, result.Rows.Count);
        }


        [Test]
        public void TestFindIntersection()
        {
            string[] interToTest = new [] { Measures, "CorpChn", "Q3", "WP", "FY2006", "DIV09", "Store1" };
            Intersection result = _simpleDimTreesWithLocks.GetIntersection(1, _dimensionPriority);
            Assert.AreEqual(new Intersection(_dimensionPriority, interToTest), result);
        }
    }
}