#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Rules;
using Titan.Palladium.DataStructures;

namespace Titan.Palladium.Rules
{
    /// <summary>
    /// Object to hold the list of intersections contained in the Protection Manager.
    /// </summary>
    [Serializable]
    public class ProtectionMngrListTracker
    {
        /// <summary>
        /// List of protected cell intersections (sent from the server).
        /// </summary>
        private List<Intersection> _ProtectedCells;

        /// <summary>
        /// List of changed cells intersections.
        /// </summary>
        private List<Intersection> _Changes;

        /// <summary>
        /// List of user locked intersections.
        /// </summary>
        private List<Intersection> _UserLocks;

        /// <summary>
        /// List of changes passed to middle tier.
        /// </summary>
        private List<Intersection> _UnLockedChanges;

        /// <summary>
        /// List of replicate existing cells to pass to the mid tier.
        /// </summary>
        private List<Intersection> _ReplicateExistingCells;

        /// <summary>
        /// List of replicate all cells to pass to the mid tier.
        /// </summary>
        private List<Intersection> _ReplicateAllCells;

        /// <summary>
        /// List of lift existing cells to pass to the mid tier.
        /// </summary>
        private List<Intersection> _LiftExistingCells;

        /// <summary>
        /// List of lift all cells to pass to the mid tier.
        /// </summary>
        private List<Intersection> _LiftAllCells;

        /// <summary>
        /// List of session locks intersections.
        /// </summary>
        private List<Intersection> _SessionLockCells;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ProtectionMngrListTracker()
        { }

        /// <summary>
        /// Overloaded Constructor.
        /// </summary>
        /// <param name="protectedCells">List of protected cell intersections (sent from the server).</param>
        /// <param name="changes">List of changed cells intersections.</param>
        /// <param name="userLocks">List of user locked intersections.</param>
        /// <param name="unlockedChanges">List of changes passed to middle tier.</param>
        /// <param name="replicateAllCells">List of replicate existing cells to pass to the mid tier.</param>
        /// <param name="replicateExistingCells">List of replicate all cells to pass to the mid tier</param>
        /// <param name="liftAllCells">List of lift existing cells to pass to the mid tier.</param>
        /// <param name="liftExistingCells">List of lift all cells to pass to the mid tier</param>
        /// <param name="sessionLocks">List of session locks</param>
        public ProtectionMngrListTracker(List<Intersection> protectedCells,
            List<Intersection> changes, List<Intersection> userLocks, List<Intersection> unlockedChanges,
            List<Intersection> replicateAllCells, List<Intersection> replicateExistingCells,
            List<Intersection> liftAllCells, List<Intersection> liftExistingCells, List<Intersection> sessionLocks)
        {
            _ProtectedCells = protectedCells;
            _Changes = changes;
            _UserLocks = userLocks;
            _UnLockedChanges = unlockedChanges;
            _ReplicateAllCells = replicateAllCells;
            _ReplicateExistingCells = replicateExistingCells;
            _LiftAllCells = liftAllCells;
            _LiftExistingCells = liftExistingCells;
            _SessionLockCells = sessionLocks;
        }

        internal void UpdateLists(ProtectionMngr protectionMngr)
        {
            _ProtectedCells = protectionMngr.ProtectedCellsSet.ToIntersections();
            _Changes = protectionMngr.Changes.ToList();
            _UserLocks = protectionMngr.UserLocks.ToList();
            _UnLockedChanges = protectionMngr.UnLockedChanges.ToList();
            _ReplicateAllCells = protectionMngr.Replication.AllocateAllCells.ToList();
            _ReplicateExistingCells = protectionMngr.Replication.AllocateExistingCells.ToList();
            _LiftAllCells = protectionMngr.Lift.AllocateAllCells.ToList();
            _LiftExistingCells = protectionMngr.Lift.AllocateExistingCells.ToList();
            _SessionLockCells = protectionMngr.GlobalLocks.AllSessionLocks().ToIntersections();
        }

        /// <summary>
        /// Get/set list of protected cell intersections (sent from the server).
        /// </summary>
        public List<Intersection> ProtectedCells
        {
            get
            {
                if (_ProtectedCells != null)
                    return _ProtectedCells;
                else
                    return null;
            }
            set { _ProtectedCells = value; }
        }

        /// <summary>
        /// Get/set list of changed cells intersections.
        /// </summary>
        public List<Intersection> Changes
        {
            get
            {
                if (_Changes != null)
                    return _Changes;
                else
                    return null;
            }
            set { _Changes = value; }
        }

        /// <summary>
        /// Get/set list of user locked intersections.
        /// </summary>
        public List<Intersection> UserLocks
        {
            get
            {
                if (_UserLocks != null)
                    return _UserLocks;
                else
                    return null;
            }
            set { _UserLocks = value; }
        }

        /// <summary>
        /// Get/set list of changes passed to middle tier.
        /// </summary>
        public List<Intersection> UnLockedChanges
        {
            get
            {
                if (_UnLockedChanges != null)
                    return _UnLockedChanges;
                else
                    return null;
            }
            set { _UnLockedChanges = value; }
        }

        /// <summary>
        /// Gets/sets list of replicate existing cells to pass to the mid tier.
        /// </summary>
        public List<Intersection> ReplicateExistingCells
        {
            get 
            {
                if (_ReplicateExistingCells != null)
                {
                    return _ReplicateExistingCells;
                }
                else
                {
                    return null;
                }
            }
            set { _ReplicateExistingCells = value; }
        }

        /// <summary>
        /// Gets/sets list of replicate all cells to pass to the mid tier.
        /// </summary>
        public List<Intersection> ReplicateAllCells
        {
            get 
            {
                if (_ReplicateAllCells != null)
                {
                    return _ReplicateAllCells;
                }
                else 
                {
                    return null;
                }
            }
            set { _ReplicateAllCells = value; }
        }

        /// <summary>
        /// Gets/sets list of lift existing cells to pass to the mid tier.
        /// </summary>
        public List<Intersection> LiftExistingCells
        {
            get
            {
                if (_LiftExistingCells != null)
                {
                    return _LiftExistingCells;
                }
                else
                {
                    return null;
                }
            }
            set { _LiftExistingCells = value; }
        }

        /// <summary>
        /// Gets/sets list of lift all cells to pass to the mid tier.
        /// </summary>
        public List<Intersection> LiftAllCells
        {
            get
            {
                if (_LiftAllCells != null)
                {
                    return _LiftAllCells;
                }
                else
                {
                    return null;
                }
            }
            set { _LiftAllCells = value; }
        }

        /// <summary>
        /// Gets/sets list of session lock cells to pass to the mid tier.
        /// </summary>
        public List<Intersection> SessionLockCells
        {
            get { return _SessionLockCells; }
            set { _SessionLockCells = value; }
        }
    }
}