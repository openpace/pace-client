#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Text;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Palladium.TestHarness
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TestSequenceResults
    {
        /// <summary>
        /// Top left position of the results on the grid.
        /// </summary>
        public CellAddress TopLeft;

        /// <summary>
        /// The bottom right position of the results on the grid.
        /// </summary>
        public CellAddress BottomRight;

        /// <summary>
        /// Results of the test.
        /// </summary>
        public object[,] ResultSet;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TestSequenceResults()
        { 
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="topLeft">Top left position of the results on the grid.</param>
        /// <param name="bottomRight">The bottom right position of the results on the grid.</param>
        /// <param name="resultSet">Results of the test.</param>
        public TestSequenceResults(CellAddress topLeft, CellAddress bottomRight, object[,] resultSet)
        {
            TopLeft = topLeft;
            BottomRight = bottomRight;
            ResultSet = resultSet;
        }
    }
}
