#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Reflection;
using System.Xml.Serialization;

namespace Titan.Palladium.TestHarness
{
    /// <summary>
    /// 
    /// </summary>
    [XmlRootAttribute("TestHarness", IsNullable = false)]
    [Serializable]
    public class TestHarness : ICollection
    {
        private List<RecordedTest> recordedTests = new List<RecordedTest>();
        private Version _Version;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TestHarness()
        {
            _Version = Assembly.GetExecutingAssembly().GetName().Version;
        }

        /// <summary>
        /// Gets/sets an item.
        /// </summary>
        /// <param name="index">Index of the item to return.</param>
        /// <returns></returns>
        public RecordedTest this[int index]
        {
            get { return recordedTests[index]; }
        }

        /// <summary>
        /// Version of the recorded test format.
        /// </summary>
        public Version Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        /// <summary>
        /// Copies the array from one array to another.
        /// </summary>
        /// <param name="a">Array to copy.</param>
        /// <param name="index">Position to start copying array.</param>
        public void CopyTo(Array a, int index)
        {
            RecordedTest[] b = new RecordedTest[recordedTests.Count];
            recordedTests.CopyTo(b, index);
            b.CopyTo(a, index);
        }

        /// <summary>
        /// Gets the number of elements.
        /// </summary>
        public int Count
        {
            get { return recordedTests.Count; }
        }

        /// <summary>
        /// 
        /// </summary>
        public object SyncRoot
        {
            get { return this; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsSynchronized
        {
            get { return false; }
        }

        /// <summary>
        /// Returns an enumerator used to iterate through the TestHarness.
        /// </summary>
        /// <returns>An enumerator.</returns>
        public IEnumerator GetEnumerator()
        {
            return recordedTests.GetEnumerator();
        }

        /// <summary>
        /// Adds an element to the end of the list.
        /// </summary>
        /// <param name="recordedTest">The element to add.</param>
        public void Add(RecordedTest recordedTest)
        {
            recordedTests.Add(recordedTest);
        }
        
        /// <summary>
        /// Adds a group of elements to the end of the list. 
        /// </summary>
        /// <param name="recordedTest">The group of elements to add.</param>
        public void AddRange(IEnumerable recordedTest)
        {
            foreach (RecordedTest test in recordedTest)
                recordedTests.Add(test);
        }
    }
}