#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Office.Interop.Excel;
using Titan.Pace;
using Titan.Pace.Application.Win32;
using Titan.Pace.Base.Data;
using Titan.Pace.ExcelGridView.Utility;
using Excel = Microsoft.Office.Interop.Excel;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Palladium.TestHarness
{
    internal class TestHarnessProtMngr
    {
        private List<string> _ProtErrLog;

        /// <summary>
        /// Constructor.
        /// </summary>
        public TestHarnessProtMngr()
        {
            _ProtErrLog = new List<string>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="recordedTestName">The name of the recorded test.</param>
        /// <param name="ts">The currently running test sequence.</param>
        /// <param name="cellNumber">The sequence number of the cell change that was just made.</param>
        /// <param name="testRunChanges">Protection manager change list from the currently running test run.</param>
        /// <param name="testRunProtectedCells">Protection manager protected list from the currently running test run.</param>
        /// <param name="testRunUserLocked">Protection manager user locked list from the currently running test run.</param>
        /// <param name="testRunUnlockedChanges">Protection manager unlocked changes list from the currently running test run.</param>
        /// <param name="testReplicateAll">Protection manager change list from the currently running test run.</param>
        /// <param name="testReplicateExisting">Protection manager protected list from the currently running test run.</param>
        /// <param name="testLiftAll">Protection manager user locked list from the currently running test run.</param>
        /// <param name="testLiftExisting">Protection manager unlocked changes list from the currently running test run.</param>
        /// <returns>true if no error occured, false if an error was detected.</returns>
        public bool ValidateProtectionManagerLists(string recordedTestName, TestSequence ts,
            int cellNumber, HashSet<Intersection> testRunChanges, HashSet<Intersection> testRunProtectedCells,
            HashSet<Intersection> testRunUserLocked, HashSet<Intersection> testRunUnlockedChanges,
            HashSet<Intersection> testReplicateAll, HashSet<Intersection> testReplicateExisting,
            HashSet<Intersection> testLiftAll, HashSet<Intersection> testLiftExisting)
        {
            Stopwatch sw = Stopwatch.StartNew();
            try
            {
                bool returnValue = true;
                //List of intersections that were incorrectly protected in the test run.
                HashSet<Intersection> notFoundInTestRun = new HashSet<Intersection>();
                //List of intersection that were not protected in the test run.
                HashSet<Intersection> notFoundInOriginalRun = new HashSet<Intersection>();

                //Protected Cells
                HashSet<Intersection> protectedCells = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.ProtectedCells != null)
                {
                    protectedCells = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.ProtectedCells);
                }
                //Changes
                HashSet<Intersection> changes = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.Changes != null)
                {
                    changes = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.Changes);
                }
                //userLocks
                HashSet<Intersection> userLocks = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.UserLocks != null)
                {
                    userLocks = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.UserLocks);
                }
                //Unlocked Cells
                HashSet<Intersection> unlockedChanges = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.UnLockedChanges != null)
                {
                    unlockedChanges = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.UnLockedChanges);
                }
                //Replicate All
                HashSet<Intersection> replicateAll = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.ReplicateAllCells != null)
                {
                    replicateAll = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.ReplicateAllCells);
                }
                //Replicate Existing
                HashSet<Intersection> replicateExisting = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.ReplicateExistingCells != null)
                {
                    replicateExisting = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.ReplicateExistingCells);
                }
                //Lift All
                HashSet<Intersection> liftAll = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.LiftAllCells != null)
                {
                    liftAll = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.LiftAllCells);
                }
                //Lift Existing
                HashSet<Intersection> liftExisting = new HashSet<Intersection>();
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.LiftExistingCells != null)
                {
                    liftExisting = new HashSet<Intersection>(ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker.LiftExistingCells);
                }



                //Check to make sure the protection list exist.  If they don't then return true.
                if (ts.ChangedCellInfoList[cellNumber].ProtMngrListTracker == null)
                {
                    return returnValue;
                }

                //
                if (!CheckIntersectionLists(protectedCells, testRunProtectedCells, ref notFoundInTestRun, ref notFoundInOriginalRun))
                {
                    //Log the test name and sequence number.
                    WriteLineToLog(
                        String.Format("***Error occured in test \"{0}\", sequence number \"{1}\"***",
                        new string[] { recordedTestName, ts.SequenceNumber }));

                    //Log the cell changes and the resulting protection lists.
                    LogProtectionErrors(cellNumber, ts.ChangedCellInfoList, notFoundInTestRun.ToList(), notFoundInOriginalRun.ToList());

                    //Error occured.
                    returnValue = false;
                }

                notFoundInTestRun.Clear();
                notFoundInOriginalRun.Clear();
                if (!CheckIntersectionLists(changes,testRunChanges, ref notFoundInTestRun, ref notFoundInOriginalRun))
                {
                    //Log the cell changes and the resulting protection lists.
                    LogListErrors(notFoundInTestRun.ToList(),notFoundInOriginalRun.ToList(), "Changes");
                    //Error occured.
                    returnValue = false;
                }

                notFoundInTestRun.Clear();
                notFoundInOriginalRun.Clear();
                if (!CheckIntersectionLists(userLocks,testRunUserLocked, ref notFoundInTestRun, ref notFoundInOriginalRun))
                {
                    //Log the cell changes and the resulting protection lists.
                    LogListErrors(notFoundInTestRun.ToList(),notFoundInOriginalRun.ToList(), "User Locked");
                    //Error occured.
                    returnValue = false;
                }

                notFoundInTestRun.Clear();
                notFoundInOriginalRun.Clear();
                if (!CheckIntersectionLists(unlockedChanges,testRunUnlockedChanges, ref notFoundInTestRun, ref notFoundInOriginalRun))
                {
                    //Log the cell changes and the resulting protection lists.
                    LogListErrors(notFoundInTestRun.ToList(),notFoundInOriginalRun.ToList(), "Unlocked Changes");
                    //Error occured.
                    returnValue = false;
                }

                //notFoundInTestRun.Clear();
                //notFoundInOriginalRun.Clear();
                //if (!CheckIntersectionLists(replicateAll, testReplicateAll, ref notFoundInTestRun, ref notFoundInOriginalRun))
                //{
                //    //Log the cell changes and the resulting protection lists.
                //    LogListErrors(notFoundInTestRun.ToList(), notFoundInOriginalRun.ToList(), "Replicate All");
                //    //Error occured.
                //    returnValue = false;
                //}

                //notFoundInTestRun.Clear();
                //notFoundInOriginalRun.Clear();
                //if (!CheckIntersectionLists(replicateExisting, testReplicateExisting, ref notFoundInTestRun, ref notFoundInOriginalRun))
                //{
                //    //Log the cell changes and the resulting protection lists.
                //    LogListErrors(notFoundInTestRun.ToList(), notFoundInOriginalRun.ToList(), "Replicate Existing");
                //    //Error occured.
                //    returnValue = false;
                //}

                notFoundInTestRun.Clear();
                notFoundInOriginalRun.Clear();
                if (!CheckIntersectionLists(liftAll, testLiftAll, ref notFoundInTestRun, ref notFoundInOriginalRun))
                {
                    //Log the cell changes and the resulting protection lists.
                    LogListErrors(notFoundInTestRun.ToList(), notFoundInOriginalRun.ToList(), "Lift All");
                    //Error occured.
                    returnValue = false;
                }

                notFoundInTestRun.Clear();
                notFoundInOriginalRun.Clear();
                if (!CheckIntersectionLists(liftExisting, testLiftExisting, ref notFoundInTestRun, ref notFoundInOriginalRun))
                {
                    //Log the cell changes and the resulting protection lists.
                    LogListErrors(notFoundInTestRun.ToList(), notFoundInOriginalRun.ToList(), "Lift Existing");
                    //Error occured.
                    returnValue = false;
                }

                sw.Stop();
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("ValidateProtectionManagerLists execution time: " + sw.ElapsedMilliseconds.ToString("N0") + " (ms)");

                return returnValue;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Writes the cell changes and protection errors to an output file.
        /// </summary>
        /// <param name="errorPosition">The step(cell change number) where the error occured.</param>
        /// <param name="cellChanges">The list of cell changes to be made.</param>
        /// <param name="protectedIncorrectly">List of intersections that were incorrectly protected</param>
        /// <param name="notProtected">List of intersection that were not protected.</param>
        private void LogProtectionErrors(int errorPosition,  List<ChangedCellInfo> cellChanges,
            List<Intersection> notProtected, List<Intersection> protectedIncorrectly)
        {
            for (int i = 0; i <= errorPosition; i++)
            {
                ChangedCellInfo cell = cellChanges[i];

                if (i == errorPosition)
                {
                    OutputToLog("Error occured on the following step:", new string[] {});
                }

                if (cell.ChangeType == Change.Locked)
                {
                    if (cell.CellAddress != null)
                    {
                        OutputToLog(
                            "User Locked Cell: {0}!R[{1}]C[{2}].",
                            new string[] 
                            {
                                cell.SheetName, 
                                cell.CellAddress.Row.ToString(), 
                                cell.CellAddress.Col.ToString()
                            });
                    }
                    else
                    {
                        OutputToLog(
                            "User locking cells: {0} - {1} on sheet: {2}.",
                            new string[] 
                            { 
                                cell.CellAdressRange.TopLeft.ToString(), 
                                cell.CellAdressRange.BottomRight.ToString(),
                                cell.SheetName
                            });
                    }
                }
                else if (cell.ChangeType == Change.PasteShape)
                {
                    if (cell.CellAddress != null)
                    {
                        OutputToLog(
                            "Set Cell (Paste Shape): {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                            new string[]
                            {
                                cell.SheetName, 
                                cell.CellAddress.Row.ToString(), 
                                cell.CellAddress.Col.ToString(), 
                                cell.CellValue.ToString()
                            });
                    }
                    else
                    {
                        string[] str = PafApp.GetTestHarnessManager().ConvertTwoDimObjectArrayToStringArray(
                            cell.CellValue);

                        OutputToLog(
                            "Setting range (Paste Shape): {0} - {1} on sheet: {2} - to values: {3}",
                            new string[] 
                            { 
                                cell.CellAdressRange.TopLeft.ToString(), 
                                cell.CellAdressRange.BottomRight.ToString(),
                                cell.SheetName,
                                String.Join(", ", str)
                            });
                    }
                }
                else if (cell.ChangeType == Change.SessionLocked)
                {
                    if (cell.SessionLocks != null)
                    {
                        OutputToLog(
                            "Session Locked Cell(s): Sheet: {0}, Locks: {1}",
                            new string[]
                            {
                                cell.SheetName, 
                                cell.SessionLocks.ToString(), 
                            });
                    }
                }
                else
                {
                    if (cell.CellAddress != null)
                    {
                        OutputToLog(
                            "Set Cell: {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                            new string[]
                            {
                                cell.SheetName,
                                cell.CellAddress.Row.ToString(),
                                cell.CellAddress.Col.ToString(),
                                cell.CellValue.ToString()
                            });
                    }
                    else
                    {
                        string[] str = PafApp.GetTestHarnessManager().ConvertTwoDimObjectArrayToStringArray(
                            cell.CellValue);

                        OutputToLog(
                            "Setting range: {0} - {1} on sheet: {2} - to values: {3}",
                            new string[] 
                            { 
                                cell.CellAdressRange.TopLeft.ToString(), 
                                cell.CellAdressRange.BottomRight.ToString(),
                                cell.SheetName,
                                String.Join(", ", str)
                            });
                    }
                }
            }

            string sheetName = cellChanges[errorPosition].Sheet;
            List<string> temp = new List<string>();
            for (int i = 0; i < notProtected.Count; i++)
            {
                List<CellAddress> ca = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(notProtected[i]);
                if (ca != null)
                {
                    foreach(CellAddress c in ca)
                    {
                        temp.Add(c.ToString());
                    }
                }
                else
                {
                    temp.Add(String.Format("{0}-{1}", new string[] { "Offscreen", notProtected[i].ToString() }));
                }
            }
            if (temp.Count > 0)
            {
                string[] temp2 = new string[temp.Count];
                temp.CopyTo(temp2);

                WriteLineToLog("Protection Manger did not protect cells/intersection(s): ");
                WriteLineToLog(String.Join(",", temp2));
            }

            temp.Clear();
            for (int i = 0; i < protectedIncorrectly.Count; i++)
            {
                List<CellAddress> ca = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(protectedIncorrectly[i]);
                if (ca != null)
                {
                    foreach (CellAddress c in ca)
                    {
                        temp.Add(c.ToString());
                    }
                }
                else
                {
                    temp.Add(String.Format("{0}-{1}", new string[] {"Offscreen", protectedIncorrectly[i].ToString()}));
                }
            }
            if (temp.Count > 0)
            {
                string[] temp2 = new string[temp.Count];
                temp.CopyTo(temp2);

                WriteLineToLog("Protection Manger has additionally protected the following cells/intersection(s): ");
                WriteLineToLog(String.Join(",", temp2));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="notFoundInTestRun">List of intersections that were in the original run, 
        /// but not in the test run.</param>
        /// <param name="notFoundInOriginalRun">List of intersections that were in the test run, 
        /// but not in the original run.</param>
        /// <param name="listName">Name of the list being printed.</param>
        private void LogListErrors(IList<Intersection> notFoundInTestRun, IList<Intersection> notFoundInOriginalRun, string listName)
        {

            string[] temp = new string[notFoundInOriginalRun.Count];
            for (int i = 0; i < temp.Length; i++)
            {
                List<CellAddress> ca = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(notFoundInOriginalRun[i]);
                if (ca != null)
                {
                    temp[i] = ca[0].ToString();
                }
                else
                {
                    temp[i] = String.Empty;
                }

            }
            if (temp.Length > 0)
            {
                OutputToLog(
                    "Protection Manger list \"{0}\", is missing cell(s): ", 
                    new string[] {listName});

                WriteLineToLog(String.Join(",", temp));
            }

            temp = new string[notFoundInTestRun.Count];
            for (int i = 0; i < temp.Length; i++)
            {
                List<CellAddress> ca = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(notFoundInTestRun[i]);
                if (ca != null)
                {
                    temp[i] = ca[0].ToString();
                }
                else
                {
                    temp[i] = String.Empty;
                }
            }
            if (temp.Length > 0)
            {
                OutputToLog(
                    "Protection Manger list \"{0}\", has extra cell(s): ",
                    new string[] { listName });

                WriteLineToLog(String.Join(",", temp));
            }
        }

        /// <summary>
        /// Write a format string to the output file.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <param name="data">Data array.</param>
        private void OutputToLog(string format, string[] data)
        {
            try
            {
                _ProtErrLog.Add(String.Format(format, data));
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        /// <summary>
        /// Writes a line to the output log.
        /// </summary>
        /// <param name="line">Text to write.</param>
        private void WriteLineToLog(string line)
        {
            try
            {
                if(! String.IsNullOrEmpty(line))
                {
                    if (line.Length > 500)
                    {
                        _ProtErrLog.Add(line.Substring(0, 500));
                        WriteLineToLog(line.Substring(499));
                    }
                    else
                    {
                        _ProtErrLog.Add(line);
                    }
                }

            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wbk">The Excel workbook to output to.</param>
        /// <param name="rt">The recorded test which has the variance.</param>
        /// <param name="ts">The test sequence in the recorded test that has the variance.</param>
        public void OutputLog(Workbook wbk, RecordedTest rt, TestSequence ts)
        {
            object missing = System.Reflection.Missing.Value;
            try
            {
                string sheetName = rt.Name + "_" + ts.SequenceNumber + "_ProtMngr";
                if (_ProtErrLog.Count > 0)
                {
                    OutputLogToSheet(wbk, sheetName);
                }
                _ProtErrLog.Clear();
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                throw;
            }
            finally
            {
                Excel.Application ExcelObj = wbk.Application;
                //Don't display any alerts.
                ExcelObj.DisplayAlerts = false;
                //Close the workbook.
                wbk.Close(true, missing, missing);
                //close the excel object
                ExcelObj.Quit();
                //make sure the excel obj is closed.
                IntPtr objPtr = new IntPtr(ExcelObj.Hwnd);
                Win32Utils.KillProcess(objPtr);
            }
        }

        /// <summary>
        /// Outputs an log to an Excel sheet.
        /// </summary>
        /// <param name="wbk">The Excel workbook to output to.</param>
        /// <param name="sheetName">The name of the worksheet to output the array to.</param>
        private void OutputLogToSheet(Workbook wbk, string sheetName)
        {
            object missing = System.Reflection.Missing.Value;
            Worksheet sheet;
            string[,] objArray = new string[_ProtErrLog.Count, 1];
            try
            {
                int i = 0;
                foreach (string str in _ProtErrLog)
                {
                    objArray[i,0] = str;
                    i++;
                }
                
                if (!PafApp.GetGridApp().DoesSheetExist(wbk, sheetName))
                {
                    sheet = PafApp.GetGridApp().AddNewWorksheet(wbk, sheetName, true, true);
                }
                else
                {
                    sheet = ((Excel.Worksheet)wbk.Worksheets[sheetName]);
                }

                sheet.Cells.Clear();
                Excel.Range Rng = null;
                Rng = PafApp.GetTestHarnessManager().GetRange(new ContiguousRange(
                    new CellAddress(1, 1),
                    new CellAddress(objArray.GetLength(0), objArray.GetLength(1))),
                    sheet);
                Rng.Value2 = objArray;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                throw;
            }
        }

        /// <summary>
        /// Compare the original list to the list that results from the test run.
        /// </summary>
        /// <param name="originalRun">The original list.</param>
        /// <param name="testRun">The test run list.</param>
        /// <param name="notFoundInTestRun"></param>
        /// <param name="notFoundInOriginalRun"></param>
        /// <returns>true, if the lists are equal; false if the lists are not equal.</returns>
        private bool CheckIntersectionLists(HashSet<Intersection> originalRun, HashSet<Intersection> testRun,
            ref HashSet<Intersection> notFoundInTestRun, ref HashSet<Intersection> notFoundInOriginalRun)
        {
            if (notFoundInTestRun == null)
                notFoundInTestRun = new HashSet<Intersection>();

            if (notFoundInOriginalRun == null)
                notFoundInOriginalRun = new HashSet<Intersection>();

            bool returnValue = true;
            try
            {
                //If they are both null, then we have nothing to compare so they are fine.
                if (originalRun == null && testRun == null)
                    return true;

                //If one is null and the other is not null then they can't match.
                if (originalRun == null && testRun != null)
                {
                    notFoundInOriginalRun.UnionWith(testRun);
                    return false;
                }

                //If one is null and the other is not null then they can't match.
                if (originalRun != null && testRun == null)
                {
                    notFoundInTestRun.UnionWith(originalRun);
                    return false;
                }

                //Overloaded compare operator on Intersection handles comparison.
                foreach (Intersection item in testRun)
                {
                    if (!originalRun.Contains(item))
                    {
                        //Extra
                        notFoundInOriginalRun.Add(item);
                        if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Intersection list sizes are not equal.");
                        returnValue = false;
                    }
                }

                //Overloaded compare operator on Intersection handles comparison.
                foreach (Intersection item in originalRun)
                {
                    if (!testRun.Contains(item))
                    {
                        //Missing
                        notFoundInTestRun.Add(item);
                        if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Intersection list sizes are not equal.");
                        returnValue = false;
                    }
                }

                return returnValue;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                throw;
            }
        }
    }
}
