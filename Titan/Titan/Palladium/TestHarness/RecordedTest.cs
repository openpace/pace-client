#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Palladium.TestHarness
{
    /// <summary>
    /// Collection of TestSequences.
    /// </summary>
    [Serializable]
    public class RecordedTest
    {
        private string _Name;
        private string _Description;
        private bool _Reload;
        private string _Role;
        private string _Season;
        private bool _RoleFilterEnabled;
        private pafDimSpec[] _RoleFilterSelections;
        private bool _InvalidIntersection;
        private bool _FilteredSubtotals;
        private List<TestSequence> _TestSequences = new List<TestSequence>();

        /// <summary>
        /// Name of the RecordedTest.
        /// </summary>
        public string Name
        {
            get { return  _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Description of the recorded test.
        /// </summary>
        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }

        /// <summary>
        /// Reload data cache before running the test.
        /// </summary>
        public bool Reload
        {
            get { return _Reload; }
            set { _Reload = value; }
        }

        /// <summary>
        /// User's role.
        /// </summary>
        public string Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        /// <summary>
        /// User's season.
        /// </summary>
        public string season
        {
            get { return _Season; }
            set { _Season = value; }
        }

        /// <summary>
        /// List of the TestSequences.
        /// </summary>
        public List<TestSequence> TestSequences
        {
            get { return _TestSequences; }
            set { _TestSequences = value; }
        }

        /// <summary>
        /// Role fileter status for this role/process.
        /// </summary>
        public bool RoleFilterEnabled
        {
            get { return _RoleFilterEnabled; }
            set { _RoleFilterEnabled = value; }
        }

        /// <summary>
        /// Role fileter selections for this role filter, null if role filter enabled is false.
        /// </summary>
        public pafDimSpec[] RoleFilterSelections
        {
            get { return _RoleFilterSelections; }
            set { _RoleFilterSelections = value; }
        }

        /// <summary>
        /// Return invalid intersections flag.
        /// </summary>
        public bool InvalidIntersection
        {
            get { return _InvalidIntersection; }
            set { _InvalidIntersection = value; }
        }

        /// <summary>
        /// Return filtered subtotals flag.
        /// </summary>
        public bool FilteredSubtotals
        {
            get { return _FilteredSubtotals; }
            set { _FilteredSubtotals = value; }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public RecordedTest()
        { 
        }


        /// <summary>
        /// Creates a new RecordedTest.
        /// </summary>
        /// <param name="name">Name of the RecordedTest.</param>
        /// <param name="description">Description of the test.</param>
        /// <param name="reload">Reload data cache before running the test.</param>
        /// <param name="role">User's role.</param>
        /// <param name="season">User's season.</param>
        /// <param name="item">List of the TestSequences.</param>
        public RecordedTest(string name, string description, bool reload, string role,
            string season, TestSequence item)
        {
            _Name = name;
            _Description = description;
            _Reload = reload;
            _Role = role;
            _Season = season;
            _RoleFilterEnabled = false;
            _RoleFilterSelections = null;
            _TestSequences.Add(item);
        }

        /// <summary>
        /// Creates a new RecordedTest.
        /// </summary>
        /// <param name="name">Name of the RecordedTest.</param>
        /// <param name="description">Description of the test.</param>
        /// <param name="reload">Reload data cache before running the test.</param>
        /// <param name="role">User's role.</param>
        /// <param name="season">User's season.</param>
        /// <param name="item">List of the TestSequences.</param>
        /// <param name="roleFilterEnabled">Role fileter status for this role/process.</param>
        /// <param name="roleFilterSelections">Role fileter selections for this role filter, null if role filter enabled is false.</param>
        /// <param name="invalidIntersections">invalid intersections flag.</param>
        /// <param name="filteredSubtotals">Filtered Subtotals Flag</param>
        public RecordedTest(string name, string description, bool reload, string role,
            string season, TestSequence item, bool roleFilterEnabled, pafDimSpec[] roleFilterSelections,
            bool invalidIntersections, bool filteredSubtotals)
        {
            _Name = name;
            _Description = description;
            _Reload = reload;
            _Role = role;
            _Season = season;
            _RoleFilterEnabled = roleFilterEnabled;
            _RoleFilterSelections = roleFilterSelections;
            _InvalidIntersection = invalidIntersections;
            _FilteredSubtotals = filteredSubtotals;
            _TestSequences.Add(item);
        }

        /// <summary>
        /// Creates a new RecordedTest.
        /// </summary>
        /// <param name="name">Name of the RecordedTest.</param>
        /// <param name="description">Description of the test.</param>
        /// <param name="reload">Reload data cache before running the test.</param>
        /// <param name="role">User's role.</param>
        /// <param name="season">User's season.</param>
        /// <param name="invalidIntersections">invalid intersections flag.</param>
        /// <param name="filteredSubtotals">Filtered Subtotals Flag</param>
        public RecordedTest(string name, string description, bool reload, string role, string season, bool invalidIntersections, bool filteredSubtotals)
        {
            _Name = name;
            _Description = description;
            _Reload = reload;
            _Role = role;
            _Season = season;
            _RoleFilterEnabled = false;
            _RoleFilterSelections = null;
            _InvalidIntersection = invalidIntersections;
            _FilteredSubtotals = filteredSubtotals;
        }

        /// <summary>
        /// Creates a new RecordedTest.
        /// </summary>
        /// <param name="name">Name of the RecordedTest.</param>
        /// <param name="description">Description of the test.</param>
        /// <param name="reload">Reload data cache before running the test.</param>
        /// <param name="role">User's role.</param>
        /// <param name="season">User's season.</param>
        /// <param name="roleFilterEnabled">Role fileter status for this role/process.</param>
        /// <param name="roleFilterSelections">Role fileter selections for this role filter, null if role filter enabled is false.</param>
        /// <param name="invalidIntersections">invalid intersections flag.</param>
        /// <param name="filteredSubtotals">Filtered Subtotals Flag</param>
        public RecordedTest(string name, string description, bool reload, string role,
            string season, bool roleFilterEnabled, pafDimSpec[] roleFilterSelections,
            bool invalidIntersections, bool filteredSubtotals)
        {
            _Name = name;
            _Description = description;
            _Reload = reload;
            _Role = role;
            _Season = season;
            _RoleFilterEnabled = roleFilterEnabled;
            _RoleFilterSelections = roleFilterSelections;
            _InvalidIntersection = invalidIntersections;
            _FilteredSubtotals = filteredSubtotals;
        }

        /// <summary>
        /// Returns an enumerator used to iterate through the RecordedTest.
        /// </summary>
        /// <returns>An enumerator.</returns>
        public IEnumerator GetEnumerator()
        {
            return _TestSequences.GetEnumerator();
        }
    }
}