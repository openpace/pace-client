#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;

namespace Titan.Palladium.TestHarness
{
    /// <summary>
    /// A grouping of cell changes, terminated by a calculation.
    /// </summary>
    [Serializable]
    public class TestSequence
    {
        private string _SequenceNumber;
        private string _Parent;
        private string _ViewName;
        private string _RuleSetName;
        private bool _IsDynamic;
        private bool _RowsSuppressed;
        private bool _ColumnsSuppressed; 
        private pafUserSelection[] _pafUserSelections;
        private List<ChangedCellInfo> _ChangedCellInfoList =
            new List<ChangedCellInfo>();
        private TestSequenceBeginningState _TestSequenceBeginningState;
        private TestSequenceResults _TestSequenceResults;


        /// <summary>
        /// Number of this TestSequence within the RecordedTest.
        /// </summary>
        public string SequenceNumber
        {
            get { return _SequenceNumber; }
            set { _SequenceNumber = value; }
        }

        /// <summary>
        /// Name of the parent.
        /// </summary>
        public string Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }

        /// <summary>
        /// Name of the view.
        /// </summary>
        public string ViewName
        {
            get { return _ViewName; }
            set { _ViewName = value; }
        }

        /// <summary>
        /// The rule set to be used during the evaluation.
        /// </summary>
        public string RuleSetName
        {
            get { return _RuleSetName; }
            set { _RuleSetName = value; }
        }

        /// <summary>
        /// Is the view a dynamic view (one that requires user selections).
        /// </summary>
        public bool IsDynamic
        {
            get { return _IsDynamic; }
            set { _IsDynamic = value; }
        }

        /// <summary>
        /// The user selections required for the dynamic view.
        /// </summary>
        public pafUserSelection[] PafUserSelections
        {
            get { return _pafUserSelections; }
            set { _pafUserSelections = value; }
        } 

        /// <summary>
        /// List of changes made to the grid.
        /// </summary>
        public List<ChangedCellInfo> ChangedCellInfoList
        {
            get { return _ChangedCellInfoList; }
            set { _ChangedCellInfoList = value; }
        }

        /// <summary>
        /// Results of the test.
        /// </summary>
        public TestSequenceResults TestSequenceResults
        {
            get { return _TestSequenceResults; }
            set { _TestSequenceResults = value; }
        }

        /// <summary>
        /// Beginning state of the protection manager and evaluation.
        /// </summary>
        public TestSequenceBeginningState TestSequenceBeginningState
        {
            get { return _TestSequenceBeginningState; }
            set { _TestSequenceBeginningState = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool RowsSuppressed
        {
            get { return _RowsSuppressed; }
            set { _RowsSuppressed = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool ColumnsSuppressed
        {
            get { return _ColumnsSuppressed; }
            set { _ColumnsSuppressed = value; }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TestSequence()
        {
        }

        ///// <summary>
        ///// Create a new TestSequence.
        ///// </summary>
        ///// <param name="sequenceNum">Number of this TestSequence within the RecordedTest.</param>
        ///// <param name="parent">The name of the parent </param>
        ///// <param name="viewName">Name of the view in this test.</param>
        ///// <param name="cellInfo">List of changes made to grid.</param>
        ///// <param name="resultSet">Results of the test.</param>
        ///// <param name="isDynamic">Is the view dynamic.</param>
        ///// <param name="pafUserSelections">Array of PafUserSelections if the view is dynamic.</param>
        ///// <param name="ruleSetName">The name of the rule set to be used.</param>
        ///// <param name="rowsSuppressed">Suppress the rows on the view.</param>
        ///// <param name="columnsSuppressed">Suppress the columns on the view.</param>
        //public TestSequence(string sequenceNum, string parent, string viewName, 
        //    ChangedCellInfo cellInfo, TestSequenceResults resultSet, bool isDynamic,
        //    pafUserSelection[] pafUserSelections, string ruleSetName, 
        //    bool rowsSuppressed, bool columnsSuppressed)
        //{
        //    _SequenceNumber = sequenceNum;
        //    _Parent = parent;
        //    _ViewName = viewName;
        //    _TestSequenceResults = resultSet;
        //    _ChangedCellInfoList.Add(cellInfo);
        //    _IsDynamic = isDynamic;
        //    _pafUserSelections = pafUserSelections;
        //    _RuleSetName = ruleSetName;
        //    _RowsSuppressed = rowsSuppressed;
        //    _ColumnsSuppressed = columnsSuppressed;
        //}

        /// <summary>
        /// Adds a group of elements to the end of the list.
        /// </summary>
        /// <param name="items">The group of elements to add.</param>
        public void AddRange(IEnumerable items)
        {
            foreach (ChangedCellInfo item in items)
                _ChangedCellInfoList.Add(item);
        }
    }
}