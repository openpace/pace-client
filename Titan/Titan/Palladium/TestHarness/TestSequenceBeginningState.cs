#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Text;
using Titan.Pace.Base.Data;
using Titan.Palladium.DataStructures;

namespace Titan.Palladium.TestHarness
{
    /// <summary>
    /// Object to hold the beginning Protection Manager state.
    /// </summary>
    [Serializable]
    public class TestSequenceBeginningState
    {
        /// <summary>
        /// List System Locked intersections (sent from the server).
        /// </summary>
        private Dictionary<Intersection, Intersection> _SystemLocks;

        /// <summary>
        /// List of protected cell intersections (sent from the server).
        /// </summary>
        private List<Intersection> _ProtectedCells;

        /// <summary>
        /// Get/set list of system locked intersections (sent from the server).
        /// </summary>
        public Dictionary<Intersection, Intersection> SystemLocks
        {
            get
            {
                if (_SystemLocks != null)
                    return _SystemLocks;
                else
                    return null;
            }
            set { _SystemLocks = value; }
        }

        /// <summary>
        /// Get/set list of protected cell intersections (sent from the server).
        /// </summary>
        public List<Intersection> ProtectedCells
        {
            get
            {
                if (_ProtectedCells != null)
                    return _ProtectedCells;
                else
                    return null;
            }
            set { _ProtectedCells = value; }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TestSequenceBeginningState()
        { 
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="systemLocks">List of system locked intersections (sent from the server).</param>
        /// <param name="protectedCells">List of protected cell intersections (sent from the server).</param>
        public TestSequenceBeginningState(Dictionary<Intersection, Intersection> systemLocks,
            List<Intersection> protectedCells)
        {
            _SystemLocks = systemLocks;
            _ProtectedCells = protectedCells;
        }
    }
}