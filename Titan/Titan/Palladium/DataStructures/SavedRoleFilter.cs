﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Titan.Pace.DataStructures
{
    [Serializable]
    [XmlRoot("SavedFilterSelections")]
    public class SavedFilterSelections
    {
        [XmlElement("IgnoreAllDimensions")]
        public bool IgnoreAllDimensions { get; set; }

        [XmlElement("IgnoredDimensions")]
        public HashSet<String> DimensionsToIgnore { get; set; }

        [XmlElement("SavedSelections")]
        public SerialDictionary<string, List<string>> SavedSelections { get; set; }

        [XmlIgnore]
        public Dictionary<string, List<string>> SavedSelectionsDictionary;

        public SavedFilterSelections()
        {
            DimensionsToIgnore = new HashSet<string>();
            SavedSelections = new SerialDictionary<string, List<string>>();
            SavedSelectionsDictionary = new Dictionary<string, List<string>>();
        }

        public SavedFilterSelections(SerialDictionary<string, List<string>> savedSelections, HashSet<string> dimsToSave)
        {
            SavedSelections = savedSelections;
            DimensionsToIgnore = dimsToSave;
        }

        /// <summary>
        /// Gets the values for the associated key.
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<string> Values(string key)
        {
            List<string> tmp;

            SavedSelectionsDictionary.TryGetValue(key, out tmp);

            return tmp;
        }

        /// <summary>
        /// Returns true if the dimension is in the list to ignore.
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public bool DimensionIgnored(string dimension)
        {
            return DimensionsToIgnore.Contains(dimension);
        }

        /// <summary>
        /// Adds a new Key/Value pair
        /// </summary>
        /// <param name="key"></param>
        /// <param name="values"></param>
        /// <param name="clearExisting">If true existing selections will be removed.</param>
        public void AddKeyValue(string key, List<string> values, bool clearExisting = false)
        {
            if (clearExisting)
            {
                RemoveKeyValue(key);
            }
            List<string> tmp;
            if (SavedSelectionsDictionary.TryGetValue(key, out tmp))
            {
                tmp.AddRange(values);
                SavedSelectionsDictionary[key] = tmp;
            }
            else
            {
                SavedSelectionsDictionary.Add(key, values);
            }

        }

        /// <summary>
        /// Adds an ignored dimension to the internal list.
        /// </summary>
        /// <param name="dimension"></param>
        public void AddIgnoredDimension(string dimension)
        {
            DimensionsToIgnore.Add(dimension);
        }

        /// <summary>
        /// Removes a saved selection
        /// </summary>
        /// <param name="key"></param>
        public void RemoveKeyValue(string key)
        {
            if (key == null) return;
            SavedSelectionsDictionary.Remove(key);
        }

        /// <summary>
        /// Removed an ignored dimension
        /// </summary>
        /// <param name="dimension"></param>
        public void RemoveIgnoredDimension(string dimension)
        {
            DimensionsToIgnore.Remove(dimension);
        }

        /// <summary>
        /// Clears all the cached selections.
        /// </summary>
        public void ClearAllKeyValues()
        {
            SavedSelectionsDictionary.Clear();
        }

        public void ClearAllIgnoredDimensions()
        {
            DimensionsToIgnore.Clear();
        }

        public void PostDeserialize()
        {
            SavedSelectionsDictionary = SavedSelections.DictionaryTyped;
            SavedSelections.Dictionary.Clear();
        }

        public void PreSerialize()
        {
            SavedSelections = new SerialDictionary<string, List<string>>(SavedSelectionsDictionary);
        }
    }
}
