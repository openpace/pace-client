#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Titan.PafService;
using Titan.Palladium;
using Titan.Palladium.Application.Compression;

namespace Titan.Pace.DataStructures
{
    /// <summary>
    /// 
    /// </summary>
    internal class PafSimpleCoordList : IPafCompressedObj
    {
        private simpleCoordList _SimpleCoordList;


        /// <summary>
        /// Default constructor.
        /// </summary>
        public PafSimpleCoordList(simpleCoordList simpleCoordList)
            : this(simpleCoordList, false)
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PafSimpleCoordList(simpleCoordList simpleCoordList, bool compress)
        {
            _SimpleCoordList = new simpleCoordList();
            _SimpleCoordList.axis = simpleCoordList.axis;
            _SimpleCoordList.coordinates = simpleCoordList.coordinates;
            _SimpleCoordList.compressedData = simpleCoordList.compressedData;
            _SimpleCoordList.compressed = simpleCoordList.compressed;
            _SimpleCoordList.compressedStringTable = simpleCoordList.compressedStringTable;
            if (compress)
            {
                compressData();
            }
        }

        /// <summary>
        /// Gets the simple coord list that is created.
        /// </summary>
        public simpleCoordList GetSimpleCoordList
        {
            get { return _SimpleCoordList; }
        }

        /// <summary>
        /// Calculates the number of Coordinates in the SimpleCoordList.
        /// </summary>
        /// <returns></returns>
        public int NumberOfCoordinates()
        {
            if (_SimpleCoordList.coordinates.Length == 0 || _SimpleCoordList.axis.Length == 0) return 0;
            return _SimpleCoordList.coordinates.Length/_SimpleCoordList.axis.Length;
        }


        #region IPafCompressedObj Members

        /// <summary>
        /// Gets the compressed status of the simple coord list.
        /// </summary>
        /// <returns>true if compressed, false if not.</returns>
        public bool isCompressed()
        {
            return _SimpleCoordList.compressed;
        }

        /// <summary>
        /// Set the compressed status of the simple coord list.
        /// </summary>
        /// <param name="isCompressed"></param>
        public void setCompressed(bool isCompressed)
        {
            _SimpleCoordList.compressed = isCompressed;
        }

        /// <summary>
        /// compress the simple coord list.
        /// </summary>
        public void compressData()
        {
            Stopwatch sw = Stopwatch.StartNew();
            //if not null or length isn't 0
            if (_SimpleCoordList.axis != null && _SimpleCoordList.axis.Length != 0 &&
                    _SimpleCoordList.coordinates != null && _SimpleCoordList.coordinates.Length != 0)
            {

                // initialization
                StringBuilder dataBuffer = new StringBuilder(1000);
                StringBuilder memberNameBuffer = new StringBuilder(1000);
                HashSet<string> memberNameLookup = new HashSet<string>();
                Dictionary<string, int> surrogateKeyLookup = new Dictionary<string, int>();

                // add dimensions
                for (int i = 0; i < _SimpleCoordList.axis.Length; i++)
                {

                    string dim = _SimpleCoordList.axis[i];
                    int key = GenerateSurrogateKey(dim, memberNameLookup, surrogateKeyLookup);
                    dataBuffer.Append(key).Append(PafAppConstants.ELEMENT_DELIM);

                }
                dataBuffer.Append(PafAppConstants.GROUP_DELIM);

                // add coordinates
                for (int i = 0; i < _SimpleCoordList.coordinates.Length; i++)
                {

                    string coord = _SimpleCoordList.coordinates[i];
                    int key = GenerateSurrogateKey(coord, memberNameLookup, surrogateKeyLookup);
                    dataBuffer.Append(key).Append(PafAppConstants.ELEMENT_DELIM);
                }
                _SimpleCoordList.compressedData = dataBuffer.ToString();


                // compress member name lookup
                //Iterator<String> iterator = memberNameLookup.GetEnumerator();
                //while (iterator.hasNext())
                foreach (string memberName in memberNameLookup)
                {
                    memberNameBuffer.Append(memberName).Append(PafAppConstants.ELEMENT_DELIM);
                }
                _SimpleCoordList.compressedStringTable = memberNameBuffer.ToString();


                // final housekeeping
                _SimpleCoordList.axis = null;
                _SimpleCoordList.coordinates = null;
                _SimpleCoordList.compressed = true;
            }
            sw.Stop();
            PafApp.GetLogger().InfoFormat("compressData runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// compress the simple coord list.
        /// </summary>
        public void compressData1()
        {
            try
            {
                if (_SimpleCoordList == null || _SimpleCoordList.axis.Length == 0 || _SimpleCoordList.coordinates.Length == 0)
                {
                    return;
                }
            }
            catch (Exception)
            {
                return;
            }

            _SimpleCoordList.compressedData = delimitArray(_SimpleCoordList, PafAppConstants.ELEMENT_DELIM, PafAppConstants.GROUP_DELIM);
            setCompressed(true);
        }

        /// <summary>
        /// uncompress the simple coord list.
        /// </summary>
        public void uncompressData1()
        {
            //null out the axis and coordniates.
            _SimpleCoordList.axis = null;
            _SimpleCoordList.coordinates = null;

            //if parms are not null
            if (_SimpleCoordList.compressedData != null)
            {
                //get a list of simple coord list attributes { axis, coordinates }
                int pos = _SimpleCoordList.compressedData.IndexOf(PafAppConstants.GROUP_DELIM, StringComparison.Ordinal);
                string s1 = _SimpleCoordList.compressedData.Substring(0, pos);
                string s2 = _SimpleCoordList.compressedData.Substring(pos + PafAppConstants.GROUP_DELIM.Length);

                //if both string are not null
                if (!String.IsNullOrEmpty(s1) && !String.IsNullOrEmpty(s2))
                {
                    //get axis list
                    List<String> axisList = s1.ConverttoList(PafAppConstants.ELEMENT_DELIM);

                    //if not null, set axis with axis list
                    if (axisList != null)
                    {
                        _SimpleCoordList.axis = axisList.ToArray();
                    }

                    //get coord list
                    List<String> coordList = s2.ConverttoList(PafAppConstants.ELEMENT_DELIM);

                    //if not null, set coordiantes
                    if (coordList != null)
                    {
                        _SimpleCoordList.coordinates = coordList.ToArray();
                    }
                }
                setCompressed(false);
            }
        }

        /// <summary>
        /// uncompress the simple coord list.
        /// </summary>
        public void uncompressData()
        {
            Stopwatch sw = Stopwatch.StartNew();
            do
            {
                if (_SimpleCoordList.compressed)
                {

                    // Parse the compressed member names into a list. Exit if they are null or empty
                    List<String> memberNameLookup = _SimpleCoordList.compressedStringTable.ConverttoList(PafAppConstants.ELEMENT_DELIM);
                    if (memberNameLookup == null)
                        continue;


                    // Split the compressed coordinate list into an axis list and a coordinate list. Exit if either of these lists
                    // are null.
                    List<String> splitCoordList = _SimpleCoordList.compressedData.ConverttoList(PafAppConstants.GROUP_DELIM);
                    if (splitCoordList == null || splitCoordList.Count != 2)
                        continue;


                    // Uncompress the axis and list. Exit if null or empty.
                    List<String> axisList = splitCoordList[0].ConverttoList(PafAppConstants.ELEMENT_DELIM);
                    if (axisList == null || axisList.Count == 0)
                        continue;

                    // Convert the surrogate keys in the axis list to their corresponding member names.
                    _SimpleCoordList.axis = ResolveSurrogateKeys(axisList, memberNameLookup);


                    // Uncompress the coordinate list. Exit if null or empty.
                    List<String> coordList = splitCoordList[1].ConverttoList(PafAppConstants.ELEMENT_DELIM);
                    // if not null, set coordinates
                    if (coordList == null || coordList.Count == 0)
                        continue;

                    // Convert the surrogate keys in the coordinate list to their corresponding member names.
                    _SimpleCoordList.coordinates = ResolveSurrogateKeys(coordList, memberNameLookup);

                    // All done!
                    _SimpleCoordList.compressedData = null;
                    _SimpleCoordList.compressedStringTable = null;
                    _SimpleCoordList.compressed = false;

                }
            }
            while (false);

            sw.Stop();
            PafApp.GetLogger().InfoFormat("uncompressData runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
        }

        public static int GenerateSurrogateKey(String stringValue, HashSet<string> stringLookup, Dictionary<string, int> surrogateKeyLookup)
        {

            int surrogateKey;

            // Check if a surrogate key has already been generated
            if (stringLookup.Contains(stringValue))
            {
                // Existing string, just lookup its surrogate key
                surrogateKey = surrogateKeyLookup[stringValue];
            }
            else
            {
                // Else create a new surrogate key and update lookup collections
                surrogateKey = stringLookup.Count;
                stringLookup.Add(stringValue);
                surrogateKeyLookup.Add(stringValue, surrogateKey);
            }

            return surrogateKey;
        }

        private static List<string> ResolveSurrogateKeys1(IEnumerable<string> surrogateKeyList, IList<string> stringLookup)
        {
            return surrogateKeyList.Select(axis => Convert.ToInt32((string) axis)).Select(surrogateKey => stringLookup[surrogateKey]).ToList();
        }

        private static string[] ResolveSurrogateKeys(IList<string> surrogateKeyList, IList<string> stringLookup)
        {
            string[] resolvedKeyList = new string[surrogateKeyList.Count];
            int i = 0;
		    foreach (string axis in surrogateKeyList) 
            {
			    int surrogateKey = Convert.ToInt32(axis);
			    //resolvedKeyList.Add(stringValue);
                resolvedKeyList[i] = stringLookup[surrogateKey];
                i++;
            }
    		
		    // Return converted keys
		    return resolvedKeyList;
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="coordList">Coord list to compress</param>
        ///// <param name="elementDelim">element delimiter.</param>
        ///// <param name="groupDelim">group deliemter.</param>
        ///// <returns>a compressed string.</returns>
        //private string delimitArray2(simpleCoordList coordList, string elementDelim, string groupDelim)
        //{
        //    int dimNum = coordList.axis.Length;

        //    using (var ms = new MemoryStream())
        //    {
        //        var sw = new StreamWriter(ms);


        //        for (int i = 0; i < dimNum; i++)
        //        {
        //            sw.Write(coordList.axis[i]);
        //            sw.Write(elementDelim);
        //        }
        //        sw.Write(groupDelim);


        //        foreach (string t in coordList.coordinates)
        //        {
        //            sw.Write(t);
        //            sw.Write(elementDelim);
        //            sw.Flush();
        //        }


        //        ms.Position = 0;
        //        var sr = new StreamReader(ms);
        //        return sr.ReadToEnd();
        //    }
        //}


        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="coordList">Coord list to compress</param>
        ///// <param name="elementDelim">element delimiter.</param>
        ///// <param name="groupDelim">group deliemter.</param>
        ///// <returns>a compressed string.</returns>
        //private string delimitArray1(simpleCoordList coordList, string elementDelim, string groupDelim)
        //{
        //    int dimNum = coordList.axis.Length;

        //    using (var writer = new StringWriter())
        //    {
        //        for (int i = 0; i < dimNum; i++)
        //        {
        //            writer.Write(coordList.axis[i]);
        //            writer.Write(elementDelim);
        //        }
        //        writer.Write(groupDelim);


        //        for (int i = 0; i < coordList.coordinates.Length; i++)
        //        {
        //            writer.Write(coordList.coordinates[i]);
        //            writer.Write(elementDelim);
        //        }

        //        return writer.ToString();
        //    }
        //}


        /// <summary>
        /// 
        /// </summary>
        /// <param name="coordList">Coord list to compress</param>
        /// <param name="elementDelim">element delimiter.</param>
        /// <param name="groupDelim">group deliemter.</param>
        /// <returns>a compressed string.</returns>
        private string delimitArray(simpleCoordList coordList, string elementDelim, string groupDelim)
        {
            Stopwatch sw = Stopwatch.StartNew();

            int dimNum = coordList.axis.Length;

            int lenght = 0;
            for (int i = 0; i < dimNum; i++)
            {
                lenght += coordList.axis[i].Length;
                lenght += elementDelim.Length;
            }
            lenght += elementDelim.Length;


            for (int i = 0; i < coordList.coordinates.Length; i++)
            {
                lenght += coordList.coordinates[i].Length;
                lenght += elementDelim.Length;
            }

            StringBuilder sb = new StringBuilder(lenght + 250);


            for (int i = 0; i < dimNum; i++)
            {
                sb.Append(coordList.axis[i]);
                sb.Append(elementDelim);
            }
            sb.Append(groupDelim);


            for (int i = 0; i < coordList.coordinates.Length; i++)
            {
                sb.Append(coordList.coordinates[i]);
                sb.Append(elementDelim);
            }


            sw.Stop();
            PafApp.GetLogger().Info("delimitArray, runtime: " + sw.ElapsedMilliseconds.ToString("N0") + " (ms)");

            return sb.ToString();
        }
        #endregion
    }
}