#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Pace.DataStructures 
{
    /// <summary>
    /// 
    /// </summary>
    public class TupleSet : System.IComparable<TupleSet>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public TupleSet()
        {
            IsPartOfSortCriteria = false;
            Order = 0;
            InvalidMbrTagIntersectionsLockedCell = null;
            InvalidAttrIntersectionsLockedCell = null;
            ForwardPlannableLockedCells = null;
            NonPlannableLockedCells = null;
            IsBlank = false;
            Tuple = null;
            MemberTagData = null;
            CachedData = null;
            Data = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public List<object> Data { get; set; }

        /// <summary>
        /// Data.
        /// </summary>
        public List<double?> CachedData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> MemberTagData { get; set; }

        /// <summary>
        /// 
        /// </summary>
        internal viewTuple Tuple { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsBlank { get; set; }

        /// <summary>
        /// List of forward non plannable locked cells
        /// </summary>
        public List<lockedCell> NonPlannableLockedCells { get; set; }

        /// <summary>
        /// List of forward plannable locked cells
        /// </summary>
        public List<lockedCell> ForwardPlannableLockedCells { get; set; }

        /// <summary>
        /// Invalid attribute intersection locked cells.
        /// </summary>
        public List<lockedCell> InvalidAttrIntersectionsLockedCell { get; set; }

        /// <summary>
        /// Invalid member tag intersections locked cells.
        /// </summary>
        public List<lockedCell> InvalidMbrTagIntersectionsLockedCell { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// Gets/sets the status of the tuples sort status in the sort.
        /// true if the tuple is part of the sort criteria, false if not.
        /// </summary>
        public bool IsPartOfSortCriteria { get; set; }

        /// <summary>
        /// Overidden CompareTo.
        /// </summary>
        /// <param name="tupleSet"></param>
        /// <returns></returns>
        public int CompareTo(TupleSet tupleSet)
        {
            int result = Order.CompareTo(tupleSet.Order);

            return result;
        }
	}
}