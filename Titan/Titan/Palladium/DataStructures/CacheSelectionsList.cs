#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace Titan.Pace.DataStructures
{
    /// <summary>
    /// This is an extension of CacheSelections but with an additional key at the beginning of the 
    /// structure.  This is a dictionay of a dictionary.
    /// </summary>
    /// <typeparam name="K">First key.</typeparam>
    /// <typeparam name="V">Key of the Dictionary.</typeparam>
    /// <typeparam name="X">Value of the dictionary(Inserted into a list).</typeparam>
    [Obsolete("Do not use this class in any new code.")]
    internal class CacheSelectionsList<K, V, X> : CachedSelections<K, V, X>
    {
        private readonly Dictionary<K, Dictionary<V, List<X>>> _DynamicUserSelections;
        private readonly IEqualityComparer<V> EqualityComparer;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CacheSelectionsList()
        {
            _DynamicUserSelections = new Dictionary<K, Dictionary<V, List<X>>>();
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        public CacheSelectionsList(IEqualityComparer<V> equalityComparer)
        {
            _DynamicUserSelections = new Dictionary<K, Dictionary<V, List<X>>>();
            EqualityComparer = equalityComparer;
            
        }

        /// <summary>
        /// Get a list of X values
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <returns>A list x values, or null if no selections have been made.</returns>
        public override List<X> GetCachedSelections(K key, V value)
        {
            if (_DynamicUserSelections.ContainsKey(key))
            {
                //Get the value dictionary
                Dictionary<V, List<X>> dimTreeNodes = _DynamicUserSelections[key];
                //See if the value is in the dictionary.
                List<X> ret;
                if (dimTreeNodes.TryGetValue(value, out ret))
                {
                    return ret;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get a list of X values
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <returns>A list x values, or null if no selections have been made.</returns>
        public override X GetCachedSelection(K key, V value)
        {
            List<X> lst = GetCachedSelections(key, value);
            if(lst != null && lst.Count > 0)
            {
                return lst[0];
            }
            else
            {
                return default(X);
            }
        }

        /// <summary>
        /// Cache an array of values.  This will replace any existing values with the new values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="value">Value.</param>
        public void CacheSelection(K key, Dictionary<V, List<X>> value)
        {
            _DynamicUserSelections.Add(key, value);
        }

        /// <summary>
        /// Cache an array of values.  This will replace any existing values with the new values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="value">Value.</param>
        public override void CacheSelection(K key, V key2, X value)
        {
            CacheSelection(key, key2, new X[] {value});
        }

        /// <summary>
        /// Cache an array of values.  This will replace any existing values with the new values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="values">Array of values</param>
        public override void CacheSelection(K key, V key2, X[] values)
        {
            //See if we have the view in the dictionary.
            if (_DynamicUserSelections.ContainsKey(key))
            {
                //Get the value dictionary
                Dictionary<V, List<X>> dimTreeNodes = _DynamicUserSelections[key];
                //See if the value is in the dictionary.
                if (dimTreeNodes.ContainsKey(key2))
                {
                    List<X> userSel = dimTreeNodes[key2];
                    userSel.Clear();
                    userSel.AddRange(values);
                    dimTreeNodes[key2] = userSel;
                }
                //Dimension is not in the dictionary, so add it.
                else
                {
                    //Create the node list.
                    List<X> tree = new List<X>(values);
                    //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                    dimTreeNodes.Add(key2, tree);
                }
            }
            //No view so we have to create the view and value dictioanry.
            else
            {
                //Create the node list.
                List<X> tree = new List<X>(values);
                //Build the (dimensionName, nodeList) dictionary.
                Dictionary<V, List<X>> dim;
                if (EqualityComparer != null)
                {
                    dim = new Dictionary<V, List<X>>(EqualityComparer);
                }
                else
                {
                    dim = new Dictionary<V, List<X>>();
                }
                //Add the items to the dictionary.
                dim.Add(key2, tree);
                //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                _DynamicUserSelections.Add(key, dim);
            }
        }

        /// <summary>
        /// Cache or add to the array of values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="value">Value</param>
        /// <param name="allowDuplicates">Allow duplicate value to be entered into the list.</param>
        public override void CacheAddSelections(K key, V key2, X value, bool allowDuplicates)
        {
            CacheAddSelections(key, key2, new X[]{value}, allowDuplicates);
        }

        /// <summary>
        /// Cache or add to the array of values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="values">Array of values</param>
        /// <param name="allowDuplicates">Allow duplicate value to be entered into the list.</param>
        public override void CacheAddSelections(K key, V key2, X[] values, bool allowDuplicates)
        {
            if (values != null)
            {
                //See if we have the view in the dictionary.
                if (_DynamicUserSelections.ContainsKey(key))
                {
                    //Get the value dictionary
                    Dictionary<V, List<X>> dimTreeNodes = _DynamicUserSelections[key];
                    //See if the value is in the dictionary.
                    if (dimTreeNodes.ContainsKey(key2))
                    {
                        List<X> userSel = dimTreeNodes[key2];
                        if (allowDuplicates)
                        {
                            userSel.AddRange(values);
                            //dimTreeNodes[key2] = userSel;
                        }
                        else
                        {
                            foreach(X v in values)
                            {
                                int pos = userSel.IndexOf(v);
                                if (pos > -1)
                                {
                                    userSel.RemoveAt(pos);
                                    userSel.Insert(pos, v);
                                }
                                else
                                {
                                    userSel.Add(v);
                                }
                            }
                        }
                        dimTreeNodes[key2] = userSel;
                    }
                        //Dimension is not in the dictionary, so add it.
                    else
                    {
                        //Create the node list.
                        List<X> tree = new List<X>(values);
                        //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                        dimTreeNodes.Add(key2, tree);
                    }
                }
                    //No view so we have to create the view and value dictioanry.
                else
                {
                    //Create the node list.
                    List<X> tree = new List<X>(values);
                    //Build the (dimensionName, nodeList) dictionary.
                    Dictionary<V, List<X>> dim;
                    if (EqualityComparer != null)
                    {
                        dim = new Dictionary<V, List<X>>(EqualityComparer);
                    }
                    else
                    {
                        dim = new Dictionary<V, List<X>>();
                    }
                    //Add the items to the dictionary.
                    dim.Add(key2, tree);
                    //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                    _DynamicUserSelections.Add(key, dim);
                }
            }
        }

        /// <summary>
        /// Remove the key/value if it exists.
        /// </summary>
        /// <param name="key"></param>
        public override void RemoveSelection(K key)
        {
            //See if we have the view in the dictionary.
            if (_DynamicUserSelections.ContainsKey(key))
            {
                _DynamicUserSelections.Remove(key);
            }
        }

        /// <summary>
        /// Remove the key/value if it exists.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        public override void RemoveSelection(K key, V key2)
        {
            //See if we have the view in the dictionary.
            if (ContainsKey2(key, key2))
            {
                //Get the value dictionary
                Dictionary<V, List<X>> dimTreeNodes = _DynamicUserSelections[key];
                List<X> userSel = dimTreeNodes[key2];
                userSel.Clear();
            }
        }

        /// <summary>
        /// Gets the key collection
        /// </summary>
        public Dictionary<K, Dictionary<V, List<X>>>.KeyCollection Keys
        {
            get { return _DynamicUserSelections.Keys; }
        }

        /// <summary>
        /// Gets the value collection
        /// </summary>
        /// <param name="key">Key.</param>
        public Dictionary<V, List<X>> Values(K key)
        {
            if (_DynamicUserSelections.ContainsKey(key))
            {
                return _DynamicUserSelections[key];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Clear out the selections.
        /// </summary>
        public override void Clear()
        {
            if (_DynamicUserSelections != null)
            {
                _DynamicUserSelections.Clear();
            }
        }

        /// <summary>
        /// Number of values in the first key.
        /// </summary>
        public override int Count()
        {

            if( _DynamicUserSelections != null)
            {
                return _DynamicUserSelections.Count;
            }
            else
            {
                return 0;
            }
            
        }


        /// <summary>
        /// Number of values in the second key.
        /// </summary>
        public int Count2
        {
            get
            {
                if (_DynamicUserSelections != null)
                {
                    return _DynamicUserSelections.Count;
                }
                else
                {
                    return 0;
                }
            }
        }

        /// <summary>
        /// Contains the key
        /// </summary>
        /// <param name="key">Key to search for.</param>
        /// <returns>true if the key if found, false if not.</returns>
        public override bool ContainsKey(K key)
        {
            if(_DynamicUserSelections.ContainsKey(key))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Contains the key2
        /// </summary>
        /// <param name="key">Main key.</param>
        /// <param name="key2">Key to search for.</param>
        /// <returns>true if the key if found, false if not.</returns>
        public override bool ContainsKey2(K key, V key2)
        {
            //See if we have the view in the dictionary.
            if (_DynamicUserSelections.ContainsKey(key))
            {
                //Get the value dictionary
                Dictionary<V, List<X>> dimTreeNodes = _DynamicUserSelections[key];
                //See if the value is in the dictionary.
                if (dimTreeNodes.ContainsKey(key2))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            //Dimension is not in the dictionary, returtn false;
            return false;
        }
    }
}
