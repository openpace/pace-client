﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Titan.Pace.DataStructures
{
    [Serializable]
    [XmlRoot("Tuple")]
    public class TripleTupleList<T1, T2, TV> : IEquatable<TripleTupleList<T1, T2, TV>>
    {
        /// <summary>
        /// Tuple Item 1
        /// </summary>
        [XmlElement("Item1")]
        public T1 Item1 { get; set; }

        /// <summary>
        /// Tuple Item 2
        /// </summary>
        [XmlElement("Item2")]
        public T2 Item2 { get; set; }

        /// <summary>
        /// Tuple Item Three (List)
        /// </summary>
        [XmlArray("Values")]
        [XmlArrayItem(ElementName = "Value")]
        public List<TV> Values { get; set; }

        public TripleTupleList()
        {
        }

        public TripleTupleList(T1 item1, T2 item2, List<TV> values)
        {
            Item1 = item1;
            Item2 = item2;
            Values = values;
        }

        public bool Equals(TripleTupleList<T1, T2, TV> other)
        {
            if (!Item1.Equals(other.Item1)) return false;
            if (!Item2.Equals(other.Item2)) return false;
            if (Values.Count != other.Values.Count) return false;
            if(!Values.SequenceEqual(other.Values)) return false;
            return true;
        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            result = prime * result + ((Item1.Equals(null)) ? 0 : Item1.GetHashCode());
            result = prime * result + ((Item2.Equals(null)) ? 0 : Item2.GetHashCode());
            result = prime * result + ((Values.Equals(null)) ? 0 : Values.GetHashCode());
            return result;
        }

        public override bool Equals(object obj)
        {
            if (obj is TripleTupleList<T1, T2, TV>)
            {
                return Equals((TripleTupleList<T1, T2, TV>)obj);
            }
            return false;
        }

        public static List<TripleTupleList<T1, T2, TV>> ConvertToSerialStructure(Dictionary<Tuple<T1, T2>, List<TV>> baseType)
        {
            return baseType.Select(kvp => new TripleTupleList<T1, T2, TV>(kvp.Key.Item1, kvp.Key.Item2, kvp.Value)).ToList();
        }

        public static Dictionary<Tuple<T1, T2>, List<TV>> ConvertFromSerialStructure(List<TripleTupleList<T1, T2, TV>> baseType)
        {
            return baseType.ToDictionary(kvp => new Tuple<T1, T2>(kvp.Item1, kvp.Item2), kvp => kvp.Values);
        }
    }
}
