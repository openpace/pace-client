﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using Titan.Pace;
using Titan.Pace.Application.Extensions;

namespace Titan.Palladium.DataStructures
{
    /// <summary>
    /// Summary description for Tuple.
    /// </summary>
    [Serializable]
    public class Intersection : ICloneable, IEquatable<Intersection>
    {
        /// <summary>
        /// Array of Dimensions
        /// </summary>
        public string[] AxisSequence { get; private set; }

        /// <summary>
        /// Array of Coordinates.
        /// </summary>
        public string[] Coordinates { get; private set; }

        /// <summary>
        /// Coordinate value for all values not specified.
        /// </summary>
        [NonSerialized]
        public const string NotIntialized = "[not intialized]";

        /// <summary>
        /// Private variable for hash code.  This keeps the hash code from being regeneraged over and over again.
        /// </summary>
        [NonSerialized]
	    private int _hashCode = 0;

        /// <summary>
        ///  Do not remove or rename this dead variable.  It is required for deserialization of older test harness tests.
        /// </summary>
        //[NonSerialized]
        //private int _uniqueId = 0;

        /// <summary>
        /// Do not remove or rename this dead variable.  It is required for deserialization of older test harness tests.
        /// </summary>
        [OptionalField]
        private string[,] _Coordinates;

        /// <summary>
        ///  Do not remove or rename this dead variable.  It is required for deserialization of older test harness tests.
        /// </summary>
        [OptionalField]
        private string[] _AxisSeq;

        /// <summary>
        /// Private variable for the Unique String (comma delimited string of coordinates)
        /// </summary>
        [NonSerialized]
        private string _uniqueString;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dimensions">Array of dimensions.</param>
        public Intersection(IEnumerable<string> dimensions)
            : this(dimensions.ToArray())
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dimensions">Array of dimensions.</param>
        public Intersection(string[] dimensions) 
        {
		    //AxisSequence = dimensions;
            AxisSequence = new string[dimensions.Length];
            Array.Copy(dimensions, AxisSequence, dimensions.Length);
		    Coordinates = new string[dimensions.Length];
            for (int i = 0; i < Coordinates.Length; i++ )
            {
                Coordinates[i] = NotIntialized;
            }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dimensions">Array of dimensions.</param>
        /// <param name="coordinates">Array of coordinates</param>
        public Intersection(IEnumerable<string> dimensions, IEnumerable<string> coordinates) 
            :this(dimensions.ToArray(), coordinates.ToArray())
        {
	    }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="dimensions">Array of dimensions.</param>
        /// <param name="coordinates">Array of coordinates</param>
        public Intersection(string[] dimensions, string[] coordinates)
        {
            //Coordinates = coordinates;
            Coordinates = new string[dimensions.Length];
            Array.Copy(coordinates, Coordinates, coordinates.Length);
            //AxisSequence = dimensions;
            AxisSequence = new string[dimensions.Length];
            Array.Copy(dimensions, AxisSequence, dimensions.Length);
        }


        /// <summary>
        /// Searches a intersection for a axis.
        /// </summary>
        /// <param name="axis">Axis to search for.</param>
        /// <returns>true if found, false if not.</returns>
        public bool ContainsAxis(string axis)
        {
            if (String.IsNullOrEmpty(axis) || AxisSequence == null || AxisSequence.Length == 0)
            {
                return false;
            }

            return AxisSequence.Any(ax => ax.Equals(axis));
        }

        /// <summary>
        /// Searches a intersection for a coordinate.
        /// </summary>
        /// <param name="coordinate">Coordinate to search for.</param>
        /// <returns>true if found, false if not.</returns>
        public bool ContainsCoordinate(string coordinate)
        {
            if (String.IsNullOrEmpty(coordinate))
            {
                return false;
            }
            if (Coordinates == null || Coordinates.Length == 0)
            {
                return false;
            }

            return Coordinates.Contains(coordinate);
        }

        /// <summary>
        /// Returns the dimension member for a given dimension 
        /// </summary>
        /// <param name="dimension">dimension</param>
        /// <returns>dimension member</returns>
        public string GetCoordinate(string dimension)
        {
            int i = 0;
            foreach (string dim in AxisSequence) 
            {
                if (dim.Equals(dimension))
                {
                    return Coordinates[i];
                }
                i++;
		    }
		    return null;
        }

        /// <summary>
        /// Sets the array of coordinates.
        /// </summary>
        /// <param name="values"></param>
        public void SetCoordinates(string[] values)
        {
            Coordinates = values;

            _hashCode = 0;
            _uniqueString = null;
            //_uniqueId = 0;
        }

        /// <summary>
        /// Sets a coordinate for a single dimension.
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="value"></param>
        public void SetCoordinate(string dimension, string value)
        {
            if (Coordinates == null)
            {
                Coordinates = new string[AxisSequence.Length];
            }

            int i = 0;
            foreach (string dim in AxisSequence)
            {
                if (dim.EqualsIgnoreCase(dimension))
                {
                    Coordinates[i] = value;
                    _hashCode = 0;
                    _uniqueString = null;
                    //_uniqueId = 0;
                    return;
                }
                i++;
            }
            throw new ArgumentException(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.Class.Intersection.CoordinateError"));
        }


        /// <summary>
        /// Sets a coordinate for a single dimension, if the dimension does not exist, then it is added to the sequence.
        /// Use SetCoordinate if not adding a new dimension, becuase this method does Array resizing which will be slow if a lot of calls are made.
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="value"></param>
        public void AddCoordinate(string dimension, string value)
        {
            try
            {
                SetCoordinate(dimension, value);
            }
            catch (ArgumentException)
            {
                string[] axisTemp = AxisSequence;
                Array.Resize(ref axisTemp, axisTemp.Length + 1);
                axisTemp[axisTemp.Length - 1] = dimension;
                AxisSequence = axisTemp;

                string[] coorTemp = Coordinates;
                Array.Resize(ref coorTemp, coorTemp.Length + 1);
                coorTemp[coorTemp.Length - 1] = NotIntialized;
                Coordinates = coorTemp;

                _hashCode = 0;
                _uniqueString = null;

                SetCoordinate(dimension, value);
            }
        }

        

        /// <summary>
        /// Converts the object to a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ToShortString();
        }

        /// <summary>
        /// Converts the object to a long string.
        /// </summary>
        /// <returns></returns>
        public string ToLongString()
        {
            string s = "(";
            int card = Coordinates.GetLength(0);

            for (int i = 0; i < card; i++)
            {
                s += AxisSequence[i] + ":" + Coordinates[i] + ", ";
            }

            s = s.TrimEnd(new char[2] { ' ', ',' });
            return s + ")";
        }

        /// <summary>
        /// Converts the Coordinates to a comma delimited string.
        /// </summary>
        /// <returns></returns>
        public string UniqueString
        {
            get
            {
                if (String.IsNullOrEmpty(_uniqueString))
                {
                    _uniqueString = String.Join(", ", Coordinates);
                }
                return _uniqueString;
            }
        }

        /// <summary>
        /// Converts the object to a short string.
        /// </summary>
        /// <returns></returns>
        public string ToShortString()
        {
            return String.Format("{0}{1}{2}", new object[] { "{", UniqueString, "}" });
        }



        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Intersection)) return false;

            Intersection temp = (Intersection)obj;

            return Equals(temp);
        }

        /// <summary>
        /// Compares an intersection to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public bool Equals(Intersection other)
        {
            if (other == null) return false;

            return UniqueString == other.UniqueString;
            //return UniqueID == other.UniqueID;
        }


        /// <summary>
        /// Return the UniqueId or the HashCode.
        /// </summary>
        public int UniqueID
        {
            get
            {
                //if (_uniqueId != 0) return _uniqueId;

                //_uniqueId = ToString().GetHashCode();

                //return _uniqueId;
                return UniqueString.GetHashCode();
            }
        }

        /// <summary>
        /// Return the UniqueId or the HashCode.
        /// </summary>
        public int UniqueID2
        {
            get
            {
                //if (_uniqueId != 0) return _uniqueId;

                //_uniqueId = ToString().GetHashCode();
    
                //return _uniqueId;
                return GetHashCode();
            }
        }

        /// <summary>
        /// Return the hashcode of the Intersection object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        [DebuggerHidden]
        public override int GetHashCode()
        {
            if (_hashCode != 0) return _hashCode;

            _hashCode = 17;
            
            foreach (string t in Coordinates)
            {
                _hashCode = 37 * _hashCode + t.GetHashCode();
            }
            return _hashCode;
        }

        /// <summary>
        /// Creates a deep clone of the Intersection object
        /// </summary>
        /// <returns></returns>
        public object Clone()
        {
            return DeepClone();
        }

        /// <summary>
        /// Creates a deep clone of the Intersection object
        /// </summary>
        /// <returns></returns>
        public Intersection DeepClone()
        {
            return new Intersection(AxisSequence, Coordinates);
        }

        /// <summary>
        /// Due to the changes in the Intersection object (Changed implementation from 2D array to two 1D arrays).
        /// This will map old Intersections into the new Intersetion format.
        /// </summary>
        /// <param name="sc"></param>
        [OnDeserialized]
        private void CopyOldTwoDimArrayToNewSingleDimArray(StreamingContext sc)
        {
            if (_AxisSeq == null || _AxisSeq.Length == 0)
            {
                if (_Coordinates == null || _Coordinates.GetLength(0) == 0) return;

                //Copy the Axis & Coordinates from the old 2D to 1D array
                Coordinates = new string[_Coordinates.GetLength(0)];
                AxisSequence = new string[Coordinates.Length];
                for (int i = 0; i < _Coordinates.GetLength(0); i++)
                {
                    Coordinates[i] = _Coordinates[i, 1];
                    AxisSequence[i] = _Coordinates[i, 0];
                }
            }
            else
            {
                //Copy the _AxisSeq old 2D array to the new 1D array
                AxisSequence = new string[_AxisSeq.Length];
                Array.Copy(_AxisSeq, AxisSequence, _AxisSeq.Length);

                //Copy the Coordinates from the old 2D to 1D array
                Coordinates = new string[_AxisSeq.Length];
                for (int i = 0; i < _AxisSeq.Length; i++)
                {
                    Coordinates[i] = _Coordinates[i, 1];
                }
            }
        }
    }
}
