#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace Titan.Pace.DataStructures
{
    internal class SortSelection : IEquatable<SortSelection>
    {
        private List<bool> _SortDirectionSelections = null;
        private List<string> _SortOnColumnSelections = null;
        private bool _AreColumnHeadersSelected = false;
        private List<int> _SortPositions = null;

        public SortSelection(List<int> sortPositions)
        {
            _SortPositions = sortPositions;
        }

        public List<int> SortPositions
        {
            get { return _SortPositions; }
        }

        /// <summary>
        /// List of up to 3 Ascending/Descending sorting selections made by the user.
        /// </summary>
        public List<bool> SortDirectionSelections
        {
            get
            {
                if (_SortDirectionSelections == null)
                {
                    _SortDirectionSelections = new List<bool>();
                }
                return _SortDirectionSelections;
            }
            set { _SortDirectionSelections = value; }
        }

        /// <summary>
        /// List of up to 3 columns to sort on selected by the user
        /// </summary>
        public List<string> SortOnColumnSelections
        {
            get
            {
                if (_SortOnColumnSelections == null)
                {
                    _SortOnColumnSelections = new List<string>();
                }
                return _SortOnColumnSelections;
            }
            set { _SortOnColumnSelections = value; }
        }

        /// <summary>
        /// Indicates whether or not the user selected Column Headers to sort on.
        /// </summary>
        public bool AreColumnHeadersSelected
        {
            get { return _AreColumnHeadersSelected; }
            set { _AreColumnHeadersSelected = value; }
        }

        public bool ContainsPosition(int position)
        {
            if (this.SortPositions.Contains(position))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="temp">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public bool Equals(SortSelection temp)
        {
            if (this.SortPositions.Count != temp.SortPositions.Count)
            {
                return false;
            }
            else
            {
                //this.SortPositions.Sort();
                //temp.SortPositions.Sort();

                for (int i = 0; i < this.SortPositions.Count; i++)
                {
                    if (this.SortPositions[0] != temp.SortPositions[0])
                    {
                        return false;
                    }
                }

                return true;
            }
        }

        /// <summary>
        /// Return the hashcode of the object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        public override int GetHashCode()
        {
            return _SortPositions.GetHashCode();
        }
    }
}
