#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace Titan.Pace.DataStructures
{
    /// <typeparam name="K">First key.</typeparam>
    /// <typeparam name="V">Key of the Dictionary.</typeparam>
    /// <typeparam name="X">Value of the dictionary.</typeparam>
    [Obsolete("Do not use this class in any new code.")]
    internal abstract class CachedSelections<K, V, X>
    {
        public abstract List<X> GetCachedSelections(K key, V value);
        public abstract X GetCachedSelection(K key, V value);
        public abstract void CacheSelection(K key, V key2, X[] values);
        public abstract void CacheSelection(K key, V key2, X value);
        public abstract void CacheAddSelections(K key, V key2, X[] values, bool allowDuplicates);
        public abstract void CacheAddSelections(K key, V key2, X value, bool allowDuplicates);
        public abstract void RemoveSelection(K key);
        public abstract void RemoveSelection(K key, V key2);
        public abstract void Clear();
        public abstract int Count();
        public abstract bool ContainsKey(K key);
        public abstract bool ContainsKey2(K key, V key2);
    }
}
