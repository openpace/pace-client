﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace Titan.Pace.DataStructures
{
    [Serializable]
    [XmlRoot("SerialDictionary")]
    public class SerialDictionary<TK, TV> 
    {
        public SerialDictionary()
        {
        }

        public SerialDictionary(Dictionary<TK, TV> data)
        {
            DictionaryTyped = data;
        }

        [XmlArray("Tuples")]
        [XmlArrayItem(ElementName = "Tuple")]
        public List<SerialTuple<TK, TV>> Dictionary { get; set; }

        [XmlIgnore]
        public Dictionary<TK, TV> DictionaryTyped
        {
            get { return Dictionary.ToDictionary(x => x.Item1, x => x.Item2); }
            set { Dictionary = value.Select(x => new SerialTuple<TK, TV>(x.Key, x.Value)).ToList(); }

        }

    }
}
