#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

namespace Titan.Pace.DataStructures
{
    internal class SortBy
    {
        private int _Position = 0;
        private bool _Ascending = true;


        public SortBy(int position, bool ascending)
        {
            _Position = position;
            _Ascending = ascending;
        }

        public int Position
        {
            get { return _Position; }
            set { _Position = value; }
        }

        public bool Ascending
        {
            get { return _Ascending; }
            set { _Ascending = value; }
        }



    }
}
