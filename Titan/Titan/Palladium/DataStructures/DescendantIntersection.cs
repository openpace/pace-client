﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.Pace.Base.Data;

namespace Titan.Pace.DataStructures
{
    internal class DescendantIntersection
    {
        /// <summary>
        /// The Parent intersection.
        /// </summary>
        public CoordinateSet Parent { get; set; }

        /// <summary>
        /// List of descendants of the specified parent intersection
        /// </summary>
        public CoordinateSet Descendants { get; set; }

        /// <summary>
        /// Coordinates contain attribute dimensions.
        /// </summary>
        public bool HasAttributes { get; set; }

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public DescendantIntersection()
        {
            Parent = new CoordinateSet();
            Descendants = new CoordinateSet();
        }

        /// <summary>
        /// Overloaded Constructor.
        /// </summary>
        /// <param name="parent"></param>
        public DescendantIntersection(CoordinateSet parent)
        {
            Parent = parent;
            Descendants = new CoordinateSet();
        }

        /// <summary>
        /// Overloaded Constructor.
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="descendants"></param>
        public DescendantIntersection(CoordinateSet parent, CoordinateSet descendants)
        {
            Parent = parent;
            Descendants = descendants;
        }
    }
}
