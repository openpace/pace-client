#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.PafService;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// Class to manage the views(PafViewTreeView) from the pafPlanSessionResponse.
    /// </summary>
    internal class ViewTreeView
    {
        /// <summary>
        /// All of the views available to the current user.
        /// </summary>
        private readonly Dictionary<string, pafViewTreeItem> _views;
        /// <summary>
        /// View grouping specifications.
        /// </summary>
        private readonly Dictionary<string, List<GroupingSpec>> _viewGroupingSpecs;
        /// <summary>
        /// keyed by view name, dimension name, table name(default, test, etc...)
        /// </summary>
        private readonly CacheSelectionsList<string, string, string> _cachedAliasTableNames;
        /// <summary>
        /// keyed by view name, dimension name, row format - (ALIAS_MAPPING_FORMAT_ALIAS,
        /// ALIAS_MAPPING_FORMAT_MEMBER, or String.Empty)
        /// </summary>
        private readonly CacheSelectionsList<string, string, string> _cachedPrimaryRowColFormat;
        /// <summary>
        /// keyed by view name, dimension name, row format - (ALIAS_MAPPING_FORMAT_ALIAS,
        /// ALIAS_MAPPING_FORMAT_MEMBER, or String.Empty)
        /// </summary>
        private readonly CacheSelectionsList<string, string, string> _cachedAdditionalRowColFormat;

        /// <summary>
        /// Constructor.
        /// </summary>
        public ViewTreeView(pafViewTreeItem[] viewTreeItems)
        {
            _views = new Dictionary<string, pafViewTreeItem>();
            _cachedAliasTableNames = new CacheSelectionsList<string, string, string>();
            _cachedPrimaryRowColFormat = new CacheSelectionsList<string, string, string>();
            _cachedAdditionalRowColFormat = new CacheSelectionsList<string, string, string>();
            _viewGroupingSpecs = new Dictionary<string, List<GroupingSpec>>(10);
            BuildViewDictionary(viewTreeItems);
        }

        #region Private Members

        /// <summary>
        /// Build the dictionary of views.
        /// </summary>
        /// <param name="views">Array of PafViewTreeViews to build the dictionary from.</param>
        private void BuildViewDictionary(IEnumerable<pafViewTreeItem> views)
        {
            if (views != null)
            {
                foreach (pafViewTreeItem view in views)
                {
                    if (view != null)
                    {
                        if (!view.group)
                        {
                            if (!_views.ContainsKey(view.label))
                            {
                                _views.Add(view.label, view);
                                CacheAliasMappingInformation(view);
                            }
                        }
                        else
                        {
                            BuildViewDictionary(view.items);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Cache the alias mapping information.
        /// </summary>
        /// <param name="viewTreeItem">The view tree item to cache.</param>
        public void CacheAliasMappingInformation(pafViewTreeItem viewTreeItem)
        {
            if (viewTreeItem == null || viewTreeItem.aliasMappings == null) return;
            foreach (aliasMapping am in viewTreeItem.aliasMappings.Where(am => am != null))
            {
                _cachedAliasTableNames.CacheSelection(viewTreeItem.label, am.dimName, new [] {am.aliasTableName});
                _cachedPrimaryRowColFormat.CacheSelection(viewTreeItem.label, am.dimName, new [] {am.primaryRowColumnFormat});
                _cachedAdditionalRowColFormat.CacheSelection(viewTreeItem.label, am.dimName, new [] {am.additionalRowColumnFormat});
            }
        }

        #endregion Private Members

        #region Public Members

        /// <summary>
        /// Reload the dictionary of views.
        /// </summary>
        public void ReloadViewDictionary(pafViewTreeItem[] viewTreeItems)
        {
            if (_views != null)
                _views.Clear();

            if (_cachedAliasTableNames != null)
                _cachedAliasTableNames.Clear();

            if(_cachedPrimaryRowColFormat != null)
                _cachedPrimaryRowColFormat.Clear();

            if(_cachedAdditionalRowColFormat != null)
                _cachedAdditionalRowColFormat.Clear();

            if (_viewGroupingSpecs != null) _viewGroupingSpecs.Clear();

            BuildViewDictionary(viewTreeItems);
        }

        /// <summary>
        /// Gets the array of views from the pafPlanSessionResponse.
        /// </summary>
        /// <returns>An array of PafViewTreeView's</returns>
        public pafViewTreeItem[] GetBaseViews()
        {
            return PafApp.GetPafPlanSessionResponse().viewTreeItems;
        }

        /// <summary>
        /// Searches the PafViewTreeView dictionary for a specified view.
        /// </summary>
        /// <param name="name">The name of the view to return the PafViewTreeView for.</param>
        /// <returns>A PafViewTreeView.</returns>
        public pafViewTreeItem GetView(string name)
        {
            //If found return it, if not return null.
            if (name == null)
            {
                return null;
            }
            pafViewTreeItem item;
            return _views.TryGetValue(name, out item) ? item : null;
        }

        /// <summary>
        /// Get the UserSelection for a specific view.
        /// </summary>
        /// <param name="viewName">The name of the view to return the user selections.</param>
        /// <returns>An array of pafUserSelection if the view is dynamic, null if the view is static.</returns>
        public pafUserSelection[] GetUserSelections(string viewName)
        {
            if (viewName == null)
            {
                return null;
            }
            //Create a temp object.
            pafViewTreeItem pafViewTree = GetView(viewName);

            //Make sure there are some selections, if so return them, else return null.
            return pafViewTree.userSelections;
        }

        /// <summary>
        /// Updates the cache with an updated pafUserSelection specification.
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="userSelections"></param>
        public void SetUserSelection(string viewName, pafUserSelection[] userSelections)
        {
            if (viewName == null)
            {
                return;
            }
            //Create a temp object.
            pafViewTreeItem pafViewTree = GetView(viewName);
            if (pafViewTree == null) return;
            pafViewTree.userSelections = userSelections;
        }

        /// <summary>
        /// Finds the view, then searches it for a certain dimension.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>The pafUserSelection if the view/dim conbinations exists, null if it does not.</returns>
        public pafUserSelection GetUserSelection(string viewName, string dimName)
        {
            //Now loop thru the userSelections for the view.
            if (GetView(viewName).userSelections != null)
            {
                foreach (pafUserSelection userSel in GetView(viewName).userSelections)
                {
                    //If the userSel is null, then there are not user sel, so return false,
                    if (userSel == null)
                    {
                        return null;
                    }
                    //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                    if (userSel.dimension.EqualsIgnoreCase(dimName))
                    {
                        return userSel;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Finds the view, then searches it for a certain dimension.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <param name="userSelId">Unique user selection id.</param>
        /// <returns>The pafUserSelection if the view/dim conbinations exists, null if it does not.</returns>
        public pafUserSelection GetUserSelection(string viewName, string dimName, string userSelId)
        {
            //Now loop thru the userSelections for the view.
            if (GetView(viewName).userSelections != null)
            {
                foreach (pafUserSelection userSel in GetView(viewName).userSelections)
                {
                    //If the userSel is null, then there are not user sel, so return false,
                    if (userSel == null)
                    {
                        return null;
                    }
                    //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                    if (userSel.dimension.EqualsIgnoreCase(dimName) && userSel.id.EqualsIgnoreCase(userSelId))
                    {
                        return userSel;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Finds the view, then searches it for a certain dimension.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <param name="userSelId">Unique user selection id.</param>
        /// <returns>The pafSimpleDimTree if the view/dim conbinations exists, null if it does not.</returns>
        public HashSet<string> GetUserSelectionSpecification(string viewName, string dimName, string userSelId)
        {
            //Now loop thru the userSelections for the view.
            if (GetView(viewName).userSelections != null)
            {
                foreach (pafUserSelection userSel in GetView(viewName).userSelections)
                {
                    //If the userSel is null, then there are not user sel, so return false,
                    if (userSel == null)
                    {
                        return null;
                    }
                    //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                    if (userSel.dimension.EqualsIgnoreCase(dimName) && userSel.id.EqualsIgnoreCase(userSelId) 
                        && userSel.specification != null && userSel.specification.expressionList != null)
                    {
                        return userSel.specification.expressionList.ToHashSet();
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Returns the default member selections for a view selection.
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="dimName"></param>
        /// <param name="userSelId"></param>
        /// <returns></returns>
        public HashSet<string> GetUserSelectionDefaultSelections(string viewName, string dimName, string userSelId)
        {
            //Now loop thru the userSelections for the view.
            if (GetView(viewName).userSelections != null)
            {
                foreach (pafUserSelection userSel in GetView(viewName).userSelections)
                {
                    //If the userSel is null, then there are not user sel, so return false,
                    if (userSel == null)
                    {
                        return null;
                    }
                    //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                    if (userSel.dimension.EqualsIgnoreCase(dimName) && userSel.id.EqualsIgnoreCase(userSelId)
                        && userSel.defaultMembers != null && userSel.defaultMembers.expressionList != null)
                    {
                        return userSel.defaultMembers.expressionList.ToHashSet();
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// Finds the view, then searches it for a certain dimension and returns the prompt string.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <param name="userSelId">Unique user selection id.</param>
        /// <returns>The prompt string if one exists, if not then the dimName value is returned.</returns>
        public string GetUserSelectionPromptString(string viewName, string dimName, string userSelId)
        {
            //Now loop thru the userSelections for the view.
            foreach (pafUserSelection userSel in GetView(viewName).userSelections)
            {
                //If the userSel is null, then there are not user sel, so return false,
                if (userSel == null)
                {
                    return null;
                }
                //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                if (userSel.dimension.EqualsIgnoreCase(dimName) && userSel.id.EqualsIgnoreCase(userSelId))
                {
                    return !String.IsNullOrEmpty(userSel.promptString) ? userSel.promptString : dimName;
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// Finds the view, then searches it for a certain dimension and returns the display string.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <param name="userSelId">Unique user selection id.</param>
        /// <returns>The prompt string if one exists, if not then the dimName value is returned.</returns>
        public string GetUserSelectionDisplayString(string viewName, string dimName, string userSelId)
        {
            //Now loop thru the userSelections for the view.
            foreach (pafUserSelection userSel in GetView(viewName).userSelections)
            {
                //If the userSel is null, then there are not user sel, so return false,
                if (userSel == null)
                {
                    return null;
                }
                //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                if (userSel.dimension.EqualsIgnoreCase(dimName) && userSel.id.EqualsIgnoreCase(userSelId))
                {
                    return userSel.displayString;
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns></returns>
        public ViewAxis GetDimensionAxis(string viewName, string dimName)
        {
            //Now loop thru the userSelections for the view.
            foreach (pafUserSelection userSel in GetView(viewName).userSelections)
            {
                //If the userSel is null, then there are not user sel, so return false,
                if (userSel == null)
                {
                    return ViewAxis.Unknown;
                }
                //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                if (userSel.dimension.EqualsIgnoreCase(dimName))
                {
                    return userSel.pafAxis.ToAxis();
                }
            }
            return ViewAxis.Unknown;
        }

        /// <summary>
        /// Searchs a PafUserSelecton for a certain dimension and returns those user selections.
        /// </summary>
        /// <param name="pafUserSelections">A pafUserSelection for a specific view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>The pafUserSelection if one exists for the dim name, null if it does not.</returns>
        public pafUserSelection GetUserSelection(pafUserSelection[] pafUserSelections, string dimName)
        {
            //Now loop thru the userSelections for the view.
            foreach (pafUserSelection userSel in pafUserSelections)
            {
                //If the userSel is null, then there are not user sel, so return false,
                if (userSel == null)
                {
                    return null;
                }
                //If the userSelection dim name == the dimName parm, then there are user sels, so return true.
                if (userSel.dimension.EqualsIgnoreCase(dimName))
                {
                    return userSel;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the alias table value.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public string GetAliasTableName(string viewName, string dimName)
        {
            List<string> item = _cachedAliasTableNames.GetCachedSelections(viewName, dimName);
            return item != null ? item[0] : String.Empty;
        }

        /// <summary>
        /// Gets the alias table value.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimensions"></param>
        /// <param name="aliasTableNames"></param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public void GetAliasTableName(string viewName, 
            out List<string> dimensions, out List<string> aliasTableNames)
        {
            Dictionary<string, List<string>> item = _cachedAliasTableNames.Values(viewName);
            dimensions = new List<string>();
            aliasTableNames = new List<string>();
            if (item != null)
            {
                foreach(KeyValuePair<string, List<string>> kvp in item)
                {
                    dimensions.Add(kvp.Key);
                    aliasTableNames.Add(kvp.Value[0]);
                }
            }
        }

        /// <summary>
        /// Gets the Primary row column format.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public string GetPrimaryRowColumnFormat(string viewName, string dimName)
        {
            List<string> item = _cachedPrimaryRowColFormat.GetCachedSelections(viewName, dimName);
            return item != null ? item[0] : String.Empty;
        }

        /// <summary>
        /// Gets the Primary row column format.
        /// </summary>
        /// <param name="aliasMappings">Array of alias mappings.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public string GetPrimaryRowColumnFormat(aliasMapping[] aliasMappings, string dimName)
        {
            foreach (aliasMapping am in aliasMappings.Where(am => am.dimName == dimName))
            {
                return am.primaryRowColumnFormat;
            }
            return String.Empty;
        }

        /// <summary>
        /// Gets the Secondary row column format.
        /// </summary>
        /// <param name="aliasMappings">Array of alias mappings.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public string GetSecondaryRowColumnFormat(aliasMapping[] aliasMappings, string dimName)
        {
            foreach (aliasMapping am in aliasMappings.Where(am => am.dimName == dimName))
            {
                return am.additionalRowColumnFormat;
            }
            return String.Empty;
        }


        /// <summary>
        /// Gets Dictionary of dimension/format for the current view
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimensions"></param>
        /// <param name="primaryRowColumnFormat"></param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public void GetPrimaryRowColumnFormat(string viewName,
            out List<string> dimensions, out List<string> primaryRowColumnFormat)
        {

            Dictionary<string, List<string>> item = _cachedPrimaryRowColFormat.Values(viewName);
            dimensions = new List<string>();
            primaryRowColumnFormat = new List<string>();
            if (item != null)
            {
                foreach (KeyValuePair<string, List<string>> kvp in item)
                {
                    dimensions.Add(kvp.Key);
                    primaryRowColumnFormat.Add(kvp.Value[0]);
                }
            }
        }

        /// <summary>
        /// Gets Dictionary of dimension/format for the current view
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimensions">Name of the view.</param>
        /// <param name="rowColumnFormat">Name of the view.</param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public void GetRowColumnFormat(string viewName,
            out List<string> dimensions, out List<string> rowColumnFormat)
        {
            Dictionary<string, List<string>> item = _cachedAdditionalRowColFormat.Values(viewName);
            dimensions = new List<string>();
            rowColumnFormat = new List<string>();
            if (item != null)
            {
                foreach (KeyValuePair<string, List<string>> kvp in item)
                {
                    dimensions.Add(kvp.Key);
                    rowColumnFormat.Add(kvp.Value[0]);
                }
            }
        }

        /// <summary>
        /// Gets the additional row column format.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>the alias table value, or an empty string if none exists.</returns>
        public string GetRowColumnFormat(string viewName, string dimName)
        {
            List<string> item = _cachedAdditionalRowColFormat.GetCachedSelections(viewName, dimName);
            return item != null ? item[0] : String.Empty;
        }

        /// <summary>
        /// Gets the count of the additional row aliases.
        /// </summary>
        /// <param name="viewName">Name of the view</param>
        /// <param name="dimName">Dim name</param>
        /// <returns></returns>
        public int GetAdditionalRowColumnFormat(string viewName, string[] dimName)
        {
            return dimName.Aggregate(0, (current, dim) => current + GetAdditionalRowColumnFormat(viewName, dim));
        }

        /// <summary>
        /// Gets the count of the additional row aliases.
        /// </summary>
        /// <param name="viewName">Name of the view</param>
        /// <param name="dimName">Dim name</param>
        /// <returns></returns>
        public int GetAdditionalRowColumnFormat(string viewName, string dimName)
        {
            string primaryformat = GetRowColumnFormat(viewName, dimName);

            return String.IsNullOrEmpty(primaryformat) ? 0 : 1;
        }

        /// <summary>
        /// Gets the aliases values.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Dimension name.</param>
        /// <param name="aliasTable">Alias table.</param>
        /// <param name="mbrName">Name of the memeber.</param>
        /// <param name="primaryValue">Variable to hold the primary value.</param>
        /// <param name="additionalValue">Variable to hold the additional value.</param>
        public void GetAliasesValues(string viewName, string dimName, string aliasTable, string mbrName,
            out string primaryValue, out string additionalValue)
        {
            primaryValue = String.Empty;
            additionalValue = String.Empty;
            SimpleDimTrees dimTrees = PafApp.SimpleDimTrees;

            string primRowFormat = PafApp.GetViewTreeView().GetPrimaryRowColumnFormat(viewName, dimName);
            string addRowFormat = PafApp.GetViewTreeView().GetRowColumnFormat(viewName, dimName);

            switch (primRowFormat.ToLower())
            {
                case PafAppConstants.ALIAS_MAPPING_FORMAT_ALIAS:
                    //primaryValue = PafApp.GetGridApp().GetSimpleTrees().GetAlias(dimName, mbrName, aliasTable);
                    primaryValue = dimTrees.GetMemberAliasValue(dimName, mbrName, aliasTable);
                    break;
                case PafAppConstants.ALIAS_MAPPING_FORMAT_MEMBER:
                    //TTN-1185
                    //primaryValue = mbrName;
                    var psdm = dimTrees.GetMember(dimName, mbrName);
                    //pafSimpleDimMember psdm = PafApp.GetGridApp().GetSimpleTrees().GetMember(dimName, mbrName);
                    primaryValue = psdm != null ? psdm.MemberName : null;
                    break;
            }

            switch (addRowFormat.ToLower())
            {
                case PafAppConstants.ALIAS_MAPPING_FORMAT_ALIAS:
                    //additionalValue = PafApp.GetGridApp().GetSimpleTrees().GetAlias(dimName, mbrName, aliasTable);
                    additionalValue = dimTrees.GetMemberAliasValue(dimName, mbrName, aliasTable);
                    break;
                case PafAppConstants.ALIAS_MAPPING_FORMAT_MEMBER:
                    var psdm = dimTrees.GetMember(dimName, mbrName);
                    ////TTN-1185
                    //pafSimpleDimMember psdm = PafApp.GetGridApp().GetSimpleTrees().GetMember(dimName, mbrName);
                    additionalValue = psdm != null ? psdm.MemberName : null;
                    break;
            }
        }

        /// <summary>
        /// Gets the aliases values.
        /// </summary>
        /// <param name="aliasMapping">Name of the view.</param>
        /// <param name="dimName">Dimension name.</param>
        /// <param name="aliasTable">Alias table.</param>
        /// <param name="mbrName">Name of the memeber.</param>
        /// <param name="primaryValue">Variable to hold the primary value.</param>
        /// <param name="additionalValue">Variable to hold the additional value.</param>
        public void GetAliasesValues(aliasMapping[] aliasMapping, string dimName, string aliasTable, string mbrName,
            out string primaryValue, out string additionalValue)
        {
            primaryValue = String.Empty;
            additionalValue = String.Empty;
            SimpleDimTrees dimTrees = PafApp.SimpleDimTrees;

            string primRowFormat = PafApp.GetViewTreeView().GetPrimaryRowColumnFormat(aliasMapping, dimName);
            string addRowFormat = PafApp.GetViewTreeView().GetSecondaryRowColumnFormat(aliasMapping, dimName);

            switch (primRowFormat.ToLower())
            {
                case PafAppConstants.ALIAS_MAPPING_FORMAT_ALIAS:
                    //primaryValue = PafApp.GetGridApp().GetSimpleTrees().GetAlias(dimName, mbrName, aliasTable);
                    primaryValue = dimTrees.GetMemberAliasValue(dimName, mbrName, aliasTable);
                    break;
                case PafAppConstants.ALIAS_MAPPING_FORMAT_MEMBER:
                    //TTN-1185
                    //primaryValue = mbrName;
                    var psdm = dimTrees.GetMember(dimName, mbrName);
                    //pafSimpleDimMember psdm = PafApp.GetGridApp().GetSimpleTrees().GetMember(dimName, mbrName);
                    primaryValue = psdm != null ? psdm.MemberName : null;
                    break;
            }

            switch (addRowFormat.ToLower())
            {
                case PafAppConstants.ALIAS_MAPPING_FORMAT_ALIAS:
                    //additionalValue = PafApp.GetGridApp().GetSimpleTrees().GetAlias(dimName, mbrName, aliasTable);
                    additionalValue = dimTrees.GetMemberAliasValue(dimName, mbrName, aliasTable);
                    break;
                case PafAppConstants.ALIAS_MAPPING_FORMAT_MEMBER:
                    //TTN-1185
                    //additionalValue = mbrName;
                    var psdm = dimTrees.GetMember(dimName, mbrName);
                    //pafSimpleDimMember psdm = PafApp.GetGridApp().GetSimpleTrees().GetMember(dimName, mbrName);
                    additionalValue = psdm != null ? psdm.MemberName : null;
                    break;
            }
        }



        /// <summary>
        /// Checks a view/dimension combination to see if it allows multiple selections.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>True if the view has dynamic selections, false if not.</returns>
        public bool DoesViewDimHaveDynamicSelections(string viewName, string dimName)
        {

            //Check to see if the view has dynamic selections.
            if (! DoesViewHaveDynamicSelections(viewName))
                return false;

            pafUserSelection userSel = GetUserSelection(viewName, dimName);

            //If the userSel is null, then there are not user sel, so return false,
            return userSel != null;
        }

        /// <summary>
        /// Checks a view to see if it allows multiple selections.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns>True if the view has dynamic selections, false if not.</returns>
        public bool DoesViewHaveDynamicSelections(string viewName)
        {
            pafViewTreeItem pafViewTree = GetView(viewName);

            //The following checks have been added to make sure no bad values are sent back to the caller.
            //If the pafViewTree is null, then the dim does not have dynamic sels.
            if (pafViewTree == null)
            {
                return false;
            }

            //If the pafViewTree.userSelections is null, then the dim does not have dynamic sels.
            if (pafViewTree.userSelections == null)
            {
                return false;
            }

            //If the lenght of pafViewTree.userSelections == 0, then the dim does not have dynamic sels.
            if (pafViewTree.userSelections.Length == 0)
            {
                return false;
            }

            //Sometimes the array has one item, and it's null - so if it is then the view is not dynamic.
            if (pafViewTree.userSelections[0] == null)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Checks to see if a view allows multiple dynamic selections.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <param name="userSelId">Unique user selection id.</param>
        /// <returns>True if the view allows multiple selections within a dimension, false if not.</returns>
        public bool DoesViewDimAllowMultipleSelections(string viewName, string dimName, string userSelId)
        {
            pafUserSelection userSel = GetUserSelection(viewName, dimName, userSelId);

            //If the userSel is null, then there are not user sel, so return false,
            return userSel != null && userSel.multiples;
        }

        /// <summary>
        /// Gets the row/column suppressed settings.
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns>An array of true or false (Row is 0, Column is 1) if the view exists, false if the view name is null or does not exist.</returns>
        public bool[] GetViewRowColumnSupressZeroEnabled(string viewName)
        {
            pafViewTreeItem pafView = GetView(viewName);
            bool[] settings = new bool[2];

            if (pafView != null)
            {
                if (pafView.suppressZeroSettings != null && pafView.suppressZeroSettings.Length > 0)
                {
                    settings[0] = pafView.suppressZeroSettings[0].rowsSuppressed;
                    settings[1] = pafView.suppressZeroSettings[0].columnsSuppressed;
                }
            }
            return settings;
        }

        /// <summary>
        /// Gets the suppress zero enabled setting for the view.
        /// </summary>
        /// <param name="viewName">Name of the view to the get the setting for.</param>
        /// <returns>true or false if the view exists, false if the view name is null or does not exist.</returns>
        public bool GetViewSuppressZeroEnabled(string viewName)
        {
            bool setting = false;
            if (viewName != null)
            {
                pafViewTreeItem pafView = GetView(viewName);

                if (pafView != null)
                {
                    if (pafView.suppressZeroSettings != null && pafView.suppressZeroSettings.Length > 0)
                    {
                        setting = pafView.suppressZeroSettings[0].enabled;
                    }
                }
            }
            return setting;
        }

        /// <summary>
        /// Gets the suppress zero visible setting for the view.
        /// </summary>
        /// <param name="viewName">Name of the view to the get the setting for.</param>
        /// <returns>true or false if the view exists, false if the view name is null or does not exist.</returns>
        public bool GetViewSuppressZeroVisible(string viewName)
        {
            bool setting = false;
            if (viewName != null)
            {
                pafViewTreeItem pafView = GetView(viewName);

                if (pafView != null)
                {
                    if (pafView.suppressZeroSettings != null && pafView.suppressZeroSettings.Length > 0)
                    {
                        setting = pafView.suppressZeroSettings[0].visible;
                    }
                }
            }
            return setting;
        }

        /// <summary>
        /// Adds a grouping spec to the cache.
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="specs"></param>
        public void SetGroupingSpec(string viewName, groupingSpec[] specs)
        {
            List<GroupingSpec> s = null;
            if (specs != null)
            {
                s = new List<GroupingSpec>(specs.Length);
                s.AddRange(specs.Select(spec => new GroupingSpec(spec)));
            }
            if (s == null) return;
            GroupingSpec row = null;
            GroupingSpec col = null;
            foreach (GroupingSpec spec in s)
            {
                switch (spec.ViewAxis)
                {
                    case ViewAxis.Col:
                        col = spec;
                        break;
                    case ViewAxis.Row:
                        row = spec;
                        break;
                }
            }
            SetGroupingSpec(viewName, row, col);
        }

        /// <summary>
        /// Adds a grouping spec to the cache.
        /// </summary>
        /// <param name="viewName"></param>
        /// <param name="rowSpec"></param>
        /// <param name="colSpec"></param>
        public void SetGroupingSpec(string viewName, GroupingSpec rowSpec, GroupingSpec colSpec)
        {
            List<GroupingSpec> specs;
            
            if (_viewGroupingSpecs.TryGetValue(viewName, out specs))
            {
                foreach (GroupingSpec s in specs)
                {
                    if (s == null) continue;
                    switch (s.ViewAxis)
                    {
                        case ViewAxis.Col:
                            if (colSpec == null) continue;
                            colSpec.ApplyGroups = s.ApplyGroups;
                            break;
                        case ViewAxis.Row:
                            if (rowSpec == null) continue;
                            rowSpec.ApplyGroups = s.ApplyGroups;
                            break;
                    }
                }
                _viewGroupingSpecs[viewName] = new List<GroupingSpec>(2) { rowSpec, colSpec };
            }
            else
            {
                _viewGroupingSpecs.Add(viewName, new List<GroupingSpec>(2) { rowSpec, colSpec });
            }
        }

        /// <summary>
        /// Returns the grouping specification for a view.
        /// </summary>
        /// <param name="viewName"></param>
        /// <returns></returns>
        public List<GroupingSpec> GetViewGroupingSpecs(string viewName)
        {
            List<GroupingSpec> temp;
            _viewGroupingSpecs.TryGetValue(viewName, out temp);

            return temp;
        }

        /// <summary>
        /// Returns the grouping specification status for a view.
        /// </summary>
        /// <param name="viewName"></param>
        /// <returns>boolean {row, column}</returns>
        public bool[] GetViewGroupingSpecStatus(string viewName)
        {
            List<GroupingSpec> specs;
            bool[] status = { true, true };
            if (_viewGroupingSpecs.TryGetValue(viewName, out specs))
            {
                foreach (var spec in specs)
                {
                    int idx = 0;
                    if (spec != null)
                    {
                        switch (spec.ViewAxis)
                        {
                            case ViewAxis.Col:
                                idx = 1;
                                break;
                        }
                        status[idx] = spec.ApplyGroups;
                    }
                }
            }

            return status;
        }

        /// <summary>
        /// Returns the grouping specification for a view.
        /// </summary>
        /// <param name="viewAxis"></param>
        /// <param name="viewName"></param>
        /// <returns></returns>
        public GroupingSpec GetViewGroupingSpec(ViewAxis viewAxis, string viewName)
        {
            var groupingSpec = GetViewGroupingSpecs(viewName);
            return groupingSpec == null ? null : groupingSpec.Where(spec => spec != null).FirstOrDefault(spec => spec.ViewAxis == viewAxis);
        }

        #endregion Public Members
    }
}