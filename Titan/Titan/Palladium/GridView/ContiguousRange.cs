﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Titan.Pace.ExcelGridView.Utility;

namespace Titan.Palladium.GridView
{
    /// <summary>
    /// Contiguous range object.
    /// </summary>
    [Serializable]
    public class ContiguousRange : ICloneable, IEquatable<ContiguousRange>, IComparable<ContiguousRange>
    {
        private CellAddress _TopLeft;
        private CellAddress _BottomRight;
        private List<Cell> _cells = null;
        private List<int> _cols = null;
        private List<int> _rows = null;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="address">R1C1 style address (the constructor will try and parse)</param>
        public ContiguousRange(string address)
        {
            string[] addresses = address.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
            if (addresses.Length > 2)
            {
                throw new ArgumentException("Invalid address specified for the range: " + address);
            }

            if (addresses.Length == 1)
            {
                CellAddress c1 = new CellAddress(addresses[0]);
                _TopLeft = c1;
                _BottomRight = c1;
            }
            else
            {
                _TopLeft = new CellAddress(addresses[0]);
                _BottomRight = new CellAddress(addresses[1]);
            }
        }

        /// <summary>
        /// Constructor.  Creates a new ContiguousRange.
        /// </summary>
        /// <param name="c1">Begining(TopLeft) address.</param>
        /// <param name="c2">Ending(BottomRight) address.</param>
        public ContiguousRange(CellAddress c1, CellAddress c2)
        {
            _TopLeft = c1;
            _BottomRight = c2;
        }

        /// <summary>
        /// Constructor.  Creates a new ContiguousRange.
        /// </summary>
        /// <param name="c">Starting cell address.</param>
        public ContiguousRange(CellAddress c)
        {
            _TopLeft = c;
            _BottomRight = c.DeepClone();
        }

        /// <summary>
        /// Get/set the CellAddress of the TopLeft portion of the ContiguousRange.
        /// </summary>
        public CellAddress TopLeft
        {
            [DebuggerHidden]
            get { return _TopLeft; }
            [DebuggerHidden]
            set { _TopLeft = value; }
        }

        /// <summary>
        /// Get/set the CellAddress of the BottomRight portion of the ContiguousRange.
        /// </summary>
        public CellAddress BottomRight
        {
            [DebuggerHidden]
            get { return _BottomRight; }
            [DebuggerHidden]
            set { _BottomRight = value; }
        }

        /// <summary>
        /// Populates the cell in the ContiguousRange with data (ContiguousRange only).
        /// </summary>
        /// <param name="data">Double variable</param>
        public void PopulateData(double data)
        {
            //object[,] array = new object[2,2];
            //array[1, 1] = data;
            PopulateData((object)data);
        }

        /// <summary>
        /// Populates the cell in the ContiguousRange with data (ContiguousRange only).
        /// </summary>
        /// <param name="data">Double variable</param>
        public void PopulateData(object data)
        {
            object[,] array = new object[2, 2];
            array[1, 1] = data;
            PopulateData(array);
        }

        /// <summary>
        /// Populates the cells in the ContiguousRange with data.
        /// </summary>
        /// <param name="data">Object array that contains the data.</param>
        public void PopulateData(object[,] data)
        {
            _cells = new List<Cell>();

            int r = 1;
            int c = 1;

            for (int i = TopLeft.Row; i <= BottomRight.Row; i++)
            {
                for (int j = TopLeft.Col; j <= BottomRight.Col; j++)
                {
                    _cells.Add(new Cell(i, j, data[r, c]));
                }
                c++;
                if (c > Columns.Count)
                {
                    c = 1;
                    r++;
                }
            }
        }

        /// <summary>
        /// Gets a list of cell containted in the ContigousRange.
        /// </summary>
        public List<Cell> Cells
        {
            get
            {
                if (_cells == null)
                {
                    _cells = new List<Cell>();

                    for (int i = TopLeft.Row; i <= BottomRight.Row; i++)
                    {
                        for (int j = TopLeft.Col; j <= BottomRight.Col; j++)
                        {
                            _cells.Add(new Cell(i, j));
                        }
                    }
                }
                return _cells;
            }
        }

        /// <summary>
        /// Returns a list of Excel rows in the contiguous range
        /// </summary>
        public List<int> Rows
        {
            get
            {
                if (_rows == null)
                {
                    _rows = new List<int>();

                    for (int i = TopLeft.Row; i <= BottomRight.Row; i++)
                    {
                        _rows.Add(i);
                    }
                }
                return _rows;
            }
        }

        /// <summary>
        /// Returns a list of Excel columns in the contiguous range
        /// </summary>
        public List<int> Columns
        {
            get
            {
                if (_cols == null)
                {
                    _cols = new List<int>();

                    for (int i = TopLeft.Col; i <= BottomRight.Col; i++)
                    {
                        _cols.Add(i);
                    }
                }
                return _cols;
            }
        }

        /// <summary>
        /// Compares a CellAddress to the ContigouousRange to see if they are contiguous.
        /// </summary>
        /// <param name="cellAddress">CellAddress to compare.</param>
        /// <returns>true if it is contigouous, false if not.</returns>
        public bool InContiguousRange(CellAddress cellAddress)
        {
            if (cellAddress.Row < _TopLeft.Row ||
                cellAddress.Row > _BottomRight.Row ||
                cellAddress.Col < _TopLeft.Col ||
                cellAddress.Col > _BottomRight.Col)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Compares a Cell to the ContigouousRange to see if they are contiguous.
        /// </summary>
        /// <param name="cell">Cell to compare.</param>
        /// <returns>true if it is contigouous, false if not.</returns>
        public bool InContiguousRange(Cell cell)
        {
            if (cell.CellAddress.Row < _TopLeft.Row ||
                cell.CellAddress.Row > _BottomRight.Row ||
                cell.CellAddress.Col < _TopLeft.Col ||
                cell.CellAddress.Col > _BottomRight.Col)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="contiguousRange"></param>
        /// <returns></returns>
        public bool CombineRange(ContiguousRange contiguousRange)
        {
            //DateTime st = DateTime.Now;
            int pos = IsContigousRange(contiguousRange);

            if(pos < 0)
            {
                //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("CombineRange: -1, " + (DateTime.Now - st).TotalMilliseconds + " (ms)");
                return false;
            }
            if(pos == 1)
            {
                BottomRight.Col = contiguousRange.BottomRight.Col;
                //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("CombineRange: 1, " + (DateTime.Now - st).TotalMilliseconds + " (ms)");
                return true;
            }
            //if(pos == 2)
            //{
            //    TopLeft.Col = contiguousRange.TopLeft.Col;
            //    //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("CombineRange: 2, " + (DateTime.Now - st).TotalMilliseconds + " (ms)");
            //    return true;
            //}
            if(pos == 3)
            {
                BottomRight.Row = contiguousRange.BottomRight.Row;
                //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("CombineRange: 3, " + (DateTime.Now - st).TotalMilliseconds + " (ms)");
                return true;
            }
            //if(pos == 4)
            //{
            //    TopLeft.Row = contiguousRange.TopLeft.Row;
            //    //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("CombineRange: 4, " + (DateTime.Now - st).TotalMilliseconds + " (ms)");
            //    return true;
            //}

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("CombineRange: -1, " + (DateTime.Now - st).TotalMilliseconds + " (ms)");
            return false;
        }

        /// <summary>
        /// Is one ContiguousRange contigous to another 
        /// </summary>
        /// <param name="contiguousRange">ContiguousRange to compare</param>
        /// <returns>1 if the range is on the right, 3 if the botton.  -1 if they are not contiguous.</returns>
        /// <remarks>TTN-2098 Refactor to variables/use locals over properties</remarks>
        public int IsContigousRange(ContiguousRange contiguousRange)
        {
            CellAddress compareTopLeft = contiguousRange._TopLeft;
            CellAddress compareBottomRight = contiguousRange._BottomRight;

            bool topLeftRow = _TopLeft.Row == compareTopLeft.Row;
            bool bottomRightRow = _BottomRight.Row == compareBottomRight.Row;

            //Side by side (on Right)
            if (topLeftRow && bottomRightRow && _BottomRight.Col == (compareTopLeft.Col - 1))
            {
                return 1;
            }

            bool topLeftCol = _TopLeft.Col == compareTopLeft.Col;
            bool bottomRightCol = _BottomRight.Col == compareBottomRight.Col;
            //Stacked (Bottom).
            if (topLeftCol && bottomRightCol && _BottomRight.Row == (compareTopLeft.Row - 1))
            {
                return 3;
            }

            return -1;
        }

        /// <summary>
        /// Is a CellAddress contigous to a ContiguousRange 
        /// </summary>
        /// <param name="cellAddress">cellAddress to compare</param>
        /// <returns>1 if the range is on the right, 3 if the botton.  -1 if they are not contiguous.</returns>
        /// <remarks>TTN-2098 Refactor to variables/use locals over properties</remarks>
        public int IsContigousRange(CellAddress cellAddress)
        {
            int compareCol = cellAddress.Col;
            int compareRow = cellAddress.Row;

            bool topLeftRow = _TopLeft.Row == compareRow;
            bool bottomRightRow = _BottomRight.Row == compareRow;

            //Side by side (on Right)
            if (topLeftRow && bottomRightRow && _BottomRight.Col == (compareCol - 1))
            {
                return 1;
            }

            bool topLeftCol = TopLeft.Col == compareCol;
            bool bottomRightCol = BottomRight.Col == compareCol;
            //Stacked (Bottom).
            if (topLeftCol && bottomRightCol && _BottomRight.Row == (compareRow - 1))
            {
                return 3;
            }

            return -1;
        }

        /// <summary>
        /// Determines if a given row position is within the ContiguousRange.
        /// </summary>
        /// <param name="row">The row position</param>
        /// <returns></returns>
        public bool InContiguousRange(int row)
        {
            if (row < _TopLeft.Row ||
                row > _BottomRight.Row)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Get a shallow copy of the ContiguousRange.
        /// </summary>
        /// <returns>A shallow copy of the ContiguousRange.</returns>
        public Object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Implementation of "IEquatable".  
        /// </summary>
        /// <param name="temp">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public bool Equals(ContiguousRange temp)
        {
            if (!temp._TopLeft.Equals(TopLeft) || !temp._BottomRight.Equals(BottomRight))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Compares one ContiguousRange to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public int CompareTo(ContiguousRange other)
        {
            return this == other ? 0 : _TopLeft.CompareTo(other._TopLeft);
        }

        /// <summary>
        /// Gets the R1C1 style address for the cell address.
        /// </summary>
        /// <returns></returns>
        public string R1C1Address
        {
            get
            {
                if (_TopLeft.Equals(_BottomRight))
                {
                    return _TopLeft.R1C1Address;
                }

                //return _TopLeft.R1C1Address + ":" + _BottomRight.R1C1Address;
                return String.Format("{0}:{1}", _TopLeft.R1C1Address, _BottomRight.R1C1Address);
            }
        }

        /// <summary>
        /// Gets the A1 style address for the range.
        /// </summary>
        /// <returns></returns>
        public string A1Address
        {
            get
            {
                if (_TopLeft.Equals(_BottomRight))
                {
                    return _TopLeft.A1Address;
                }
                //return _TopLeft.A1Address + ":" + _BottomRight.A1Address;
                return String.Format("{0}:{1}", _TopLeft.A1Address, _BottomRight.A1Address);
            }
        }

        

        /// <summary>
        /// Returns a System.String representation of the ContiguousRange.
        /// </summary>
        /// <returns>Returns a System.String representation of the ContiguousRange.</returns>
        public override string ToString()
        {
            //return "(" + _TopLeft.Row + "," + _TopLeft.Col + "):(" + _BottomRight.Row + "," + _BottomRight.Col + ")";
            return String.Format("{0}:{1}", _TopLeft, BottomRight);
        }

        /// <summary>
        /// Return the hashcode of the object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        public override int GetHashCode()
        {
            return TopLeft.GetHashCode() / 2 + TopLeft.GetHashCode() / 2;
        }
    }
}
