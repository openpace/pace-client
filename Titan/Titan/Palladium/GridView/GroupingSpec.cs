﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Resources;
using System.Windows.Forms;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Forms;
using Titan.PafService;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// Class to hold a grouping (Excel) specification.
    /// </summary>
    internal class GroupingSpec
    {
        /// <summary>
        /// Name of the dimension
        /// </summary>
        public string DimensionName { get; set; }

        /// <summary>
        /// Specifies the Row or Column (number) in which the spec is located.
        /// </summary>
        public int Range { get; set; }

        /// <summary>
        /// Axis on which the grouping will be set.
        /// </summary>
        public ViewAxis ViewAxis { get; set; }

        /// <summary>
        /// Generation or Level where the grouping will be expanded.
        /// </summary>
        public int? DefaultLevel { get; set; }

        /// <summary>
        /// Where to look for and apply the grouping (Level, Gen, Bottom)
        /// </summary>
        public LevelGenerationType Type { get; set; }

        /// <summary>
        /// Map of Levels to Excel Group numbers.
        /// Level, Excel Group #
        /// </summary>
        public IDictionary<int, int> ExcelGroupLevelMap { get; set; }

        /// <summary>
        /// Map of Generations to Excel Group numbers.
        /// Level, Excel Group #
        /// </summary>
        public IDictionary<int, int> ExcelGroupGenerationMap { get; set; }

        /// <summary>
        /// Dictionary &lt;MemberName, List of decendants&gt;
        /// </summary>
        public IDictionary<string, List<string>> MbrDescendants { get; set; }

        /// <summary>
        /// Dictionary &lt;MemberName, List of cell addresses&gt;
        /// </summary>
        public IDictionary<string, List<CellAddress>> MbrAddresses { get; set; }

        /// <summary>
        /// Dictionary &lt;Address, Tuple Parent First&gt;
        /// </summary>
        public IDictionary<CellAddress, bool> MbrAddressParentFirst { get; set; }

        /// <summary>
        /// Dictionary &lt;MemberName, Member Level&gt; 
        /// </summary>
        public IDictionary<string, int> MemberLevels { get; set; }

        /// <summary>
        /// Address of blanks
        /// </summary>
        public List<CellAddress> Blanks { get; set; }

        /// <summary>
        /// List of levels used.
        /// </summary>
        public HashSet<int> Levels { get; set; }

        /// <summary>
        /// List of Generations used.
        /// </summary>
        public HashSet<int> Generations { get; set; }

        /// <summary>
        /// Flag to specify if members were found and grouped.
        /// </summary>
        public bool? GroupsApplied { get; set; }

        /// <summary>
        /// Global flag to apply member groups.  
        /// </summary>
        public bool ApplyGroups { get; set; }

        /// <summary>
        /// Set of Parent First flags.
        /// </summary>
        public HashSet<bool> ParentFirst { get; set; }

        /// <summary>
        /// If Parents are listed before children, then the Excel group needs to be flipped.
        /// </summary>
        public bool IsParentFirst { get; private set; }

        /// <summary>
        /// Lowest level of all the members.
        /// </summary>
        public int LowestLevel { get; set; }

        /// <summary>
        /// Highest level of all the members
        /// </summary>
        public int HighestLevel { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string FirstMember { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int FirstLevel { get; set; }

        /// <summary>
        /// Get/set the level of the Excel outline.
        /// </summary>
        public int MaxOutlineLevel { get; set; }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public GroupingSpec()
        {
            ExcelGroupGenerationMap = new Dictionary<int, int>(7);
            ExcelGroupLevelMap = new Dictionary<int, int>(7);
            Blanks = new List<CellAddress>(15);
            Levels = new HashSet<int>();
            Generations = new HashSet<int>();
            ParentFirst = new HashSet<bool>();
            GroupsApplied = null;
            IsParentFirst = false;
            ApplyGroups = true;
        }

        /// <summary>
        /// Constructor to create a GroupingSpec from a groupingSpec
        /// </summary>
        /// <param name="spec"></param>
        public GroupingSpec(groupingSpec spec):this()
        {
            if (spec == null) return;
            DimensionName = spec.dimension;
            ViewAxis = !spec.axisSpecified ? ViewAxis.Unknown : spec.axis.ParseEnum<ViewAxis>();
            if (spec.levelSpecified)
            {
                DefaultLevel = spec.level;
            }
            else
            {
                DefaultLevel = null;
            }

            Type = !spec.typeSpecified ? LevelGenerationType.Bottom : spec.type.ToLevelGenerationType();
        }

        /// <summary>
        /// Overloaded Constructor
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="viewAxis"></param>
        /// <param name="type"></param>
        /// <param name="level"></param>
        public GroupingSpec(string dimensionName, ViewAxis viewAxis, LevelGenerationType type, int? level = null)
            : this()
        {
            DimensionName = dimensionName;
            ViewAxis = viewAxis;
            DefaultLevel = level;
            Type = type;
        }

        /// <summary>
        /// Create the collections (to the exact size)
        /// </summary>
        /// <param name="size"></param>
        internal void InitializeMemberDictionaries(int size)
        {
            MbrDescendants = new Dictionary<string, List<string>>(size);
            MemberLevels = new Dictionary<string, int>(size);
            MbrAddresses = new Dictionary<string, List<CellAddress>>(size);
            MbrAddressParentFirst = new Dictionary<CellAddress, bool>(size);
        }

        /// <summary>
        /// Build the maps of Excel Levels to Level/Generation
        /// </summary>
        internal void BuildExcelGroupMaps(bool clearMemberList = false)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            if (!GroupsApplied.GetValueOrDefault()) return;
            
            LowestLevel = Levels.Min();
            HighestLevel = Levels.Max();

            //TTN-2320
            if (ParentFirst.Count > 1)
            {
                //Log it
                PafApp.GetLogger().Info("Multiple parent first values found.  The groupings may not be correct.");

                ResourceManager manager = PafApp.GetLocalization().GetResourceManager();

                string message = manager.GetString("Application.Exception.MultiParentFirstInGroup");

                HelpRequestor c = new HelpRequestor(message,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Warning,
                    manager.GetString("Application.HelpFile.BaseUrl"),
                    manager.GetString("Application.HelpFile.MultiParentFirstInGroup"),
                    true);

                c.ShowMessage();
                c.Close();
                

            }
            if (ParentFirst.Count == 1)
            {
                IsParentFirst = ParentFirst.ElementAt(0);
            }

            //TTN-2320 Removed.
            //if (FirstLevel == HighestLevel)
            //{
            //    ParentFirst = true;
            //}

            //Build the Excel Mapping Lists
            List<int> l = Levels.OrderByDescending(x => x).ToList();
            ExcelGroupLevelMap.Clear();
            int i = 1;
            foreach (int lv in l)
            {
                ExcelGroupLevelMap.Add(lv, i++);
            }
            MaxOutlineLevel = ExcelGroupLevelMap.Select(x => x.Value).Max();


            List<int> g = Generations.OrderBy(x => x).ToList();
            ExcelGroupGenerationMap.Clear();
            i = 1;
            foreach (int lv in g)
            {
                ExcelGroupGenerationMap.Add(lv, i++);
            }

            //Clear the builder lists to save some mem space.
            if (clearMemberList)
            {
                MbrDescendants.Clear();
                MemberLevels.Clear();
                MbrAddresses.Clear();
            }

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("BuildExcelGroupMaps, Axis: {0}, Dimension: {1}, Runtime: {2} (ms)", ViewAxis, DimensionName, startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Get the Excel grouping level, using the Type of grouping spec.
        /// </summary>
        /// <returns></returns>
        internal int? GetExcelGroupingExpansionLevel()
        {
            int? xlLevel = null;

            switch (Type)
            {
                case LevelGenerationType.Level:
                    if (DefaultLevel.HasValue)
                    {
                        int i;
                        if (ExcelGroupLevelMap.TryGetValue(DefaultLevel.Value, out i))
                        {
                            xlLevel = i;
                        }
                    }
                    break;
                case LevelGenerationType.Generation:
                    if (DefaultLevel.HasValue)
                    {
                        int i;
                        if (ExcelGroupGenerationMap.TryGetValue(DefaultLevel.Value, out i))
                        {
                            xlLevel = i;
                        }
                    }
                    break;
                case LevelGenerationType.Bottom:
                    if (ExcelGroupLevelMap != null && ExcelGroupLevelMap.Keys.Count > 0)
                    {
                        var min = ExcelGroupLevelMap.Keys.ToList().Min();
                        xlLevel = ExcelGroupLevelMap[min];
                    }
                    break;
            }
            return xlLevel;
        }
    }
}