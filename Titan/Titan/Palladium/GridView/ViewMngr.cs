#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Titan.Pace.DataStructures;
using Titan.Pace.Rules;
using Titan.PafService;
using Titan.Palladium.GridView;
using Titan.Properties;

namespace Titan.Pace.ExcelGridView
{
    internal class ViewMngr : MarshalByRefObject
    {
        private Dictionary<int, ViewStateInfo> _Views =
            new Dictionary<int, ViewStateInfo>();
        private ViewStateInfo _CurrentView;
        private bool _Sorting = false;
        private bool _ShowSortForm = false;

        public event ViewMngr_ViewNotPlannable ViewNotPlannable;
        public delegate void ViewMngr_ViewNotPlannable(object sender, ViewPlannableStatusChangeEventArgs EventArgs);
        public event ViewMngr_ViewPlannable ViewIsPlannable;
        public delegate void ViewMngr_ViewPlannable(object sender, ViewPlannableStatusChangeEventArgs EventArgs);
        public event ViewMngr_ViewRemoved ViewRemoved;
        public delegate void ViewMngr_ViewRemoved(object sender, string viewName);

        /// <summary>
        /// Constructor.
        /// </summary>
        [DebuggerHidden]
        public ViewMngr()
        {
            if (GlobalLockMngr == null)
            {
                GlobalLockMngr = new LockMngr();
            }
        }

        /// <summary>
        /// Returns the currently active ViewStateInfo object.
        /// </summary>
        public ViewStateInfo CurrentView
        {
            [DebuggerHidden] 
            get { return _CurrentView;}
            [DebuggerHidden]
            set { _CurrentView = value; }
        }

        /// <summary>
        /// Global lock manager (for session locks)
        /// </summary>
        public LockMngr GlobalLockMngr { get; private set; }

        /// <summary>
        /// Returns a dictiony of ViewStateInfo objects.
        /// </summary>
        public Dictionary<int, ViewStateInfo> Views
        {
            [DebuggerHidden]
            get { return _Views; }
        }

        /// <summary>
        /// Returns a GridInterface from the dictionary.
        /// </summary>
        /// 
        /// <returns>A GridInterface.</returns>
        public IGridInterface CurrentGrid
        {
            [DebuggerHidden]
            get
            {
                if (CurrentView != null)
                {
                    return CurrentView.GetGrid();
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Gets the Olap view.
        /// </summary>
        /// <returns>The olap view object.</returns>
        public OlapView CurrentOlapView
        {
            [DebuggerHidden]
            get
            {
                if (CurrentView != null)
                {
                    return CurrentView.GetOlapView();
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// Update the GlobalLockMngr in all the ProtectionMngrs
        /// </summary>
        public void UpdateGlobalLockMngrs()
        {
            foreach (KeyValuePair<int, ViewStateInfo> kvp in _Views)
            {
                if (!kvp.Value.HasAttributes)
                {
                    kvp.Value.SetGlobalLockMng(GlobalLockMngr);
                }
            }
        }

        /// <summary>
        /// Gets the olap view object for a given sheet hash
        /// </summary>
        /// <param name="hashCode"></param>
        /// <returns></returns>
        [DebuggerHidden]
        public OlapView GetOlapView(int hashCode)
        {
            if (ViewExists(hashCode))
            {
                return Views[hashCode].GetOlapView();
            }

            return null;
        }

        /// <summary>
        /// Gets the view object for a given sheet hash
        /// </summary>
        /// <param name="hashCode"></param>
        /// <returns></returns>
        [DebuggerHidden]
        public ViewStateInfo GetView(int hashCode)
        {
            ViewStateInfo viewStateInfo = null;
            return Views.TryGetValue(hashCode, out viewStateInfo) ? viewStateInfo : null;
        }

        /// <summary>
        /// Gets the protections manager for a worksheet.
        /// </summary>
        /// <returns>ProtectionsManager.</returns>
        public ProtectionMngr CurrentProtectionMngr
        {
            [DebuggerHidden]
            get
            {
                if (CurrentView != null)
                {
                    return CurrentView.GetProtectionMngr();
                }
                else
                {
                    return null;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns>An object the node that correspondes to this view sheet.</returns>
        public object CurrentViewNode
        {
            [DebuggerHidden]
            get
            {
                if (CurrentView != null)
                {
                    return CurrentView.ViewNode;
                }
                else
                {
                    return null;
                }
            }
            [DebuggerHidden]
            set { CurrentView.ViewNode = value; }
        }

        /// <summary>
        /// Searches the views for a view that is currently being planned.
        /// </summary>
        /// <returns>The name of the worksheet that is being planned.</returns>
        public string PlannableViewName
        {
            get
            {
                foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
                {
                    if (kv.Value.GetProtectionMngr().CalculationsPending())
                    {
                        return kv.Value.ViewName;
                    }
                }
                return "";
            }
        }

        /// <summary>
        /// Returns the hash code of the view that is being planned.
        /// </summary>
        /// <returns>The hash code of the view that is being planned.</returns>
        public int PlannableViewHashCode
        {
            get
            {
                foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
                {
                    if (kv.Value.GetProtectionMngr().CalculationsPending())
                    {
                        return kv.Value.HashCode;
                    }
                }
                return 0;
            }
        }

        public bool Sorting
        {
            get { return _Sorting; }
            set { _Sorting = value; }
        }

        /// <summary>
        /// Get hashtable of registered grids.
        /// </summary>
        public Hashtable Grids
        {
            get
            {
                Hashtable ht = new Hashtable(Views.Count);
                foreach (ViewStateInfo view in Views.Values)
                {
                    ht.Add(view.HashCode, view.GetGrid());
                }
                return ht;
            }
        }

        public bool ShowSortForm
        {
            get { return _ShowSortForm; }
            set { _ShowSortForm = value; }
        }

        public void SetCurrentView(int hashCode)
        {
            ViewStateInfo viewState;
            if (Views.TryGetValue(hashCode, out viewState))
            {
                CurrentView = viewState;
            }
        }

        /// <summary>
        /// Registers the grid in the internal structure.
        /// </summary>
        /// <param name="hashCode">Hash code of the worksheet.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="interfaceImplementer">The GridInterface to register.</param>
        /// <param name="defaultFormat">The default format to be used to fill in blank values.</param>
        /// <param name="attributeView">Is the view an attribute view.</param>
        public void RegisterGrid(int hashCode, string viewName, IGridInterface interfaceImplementer, Format defaultFormat, bool attributeView)
        {
            if (!Views.ContainsKey(hashCode))
            {
                ViewStateInfo viewInfo = null;
                if (!attributeView)
                {
                    viewInfo = new ViewStateInfo(
                                            hashCode,
                                            viewName,
                                            interfaceImplementer,
                                            defaultFormat,
                                            GlobalLockMngr) {HasAttributes = false};
                }
                else
                {
                    viewInfo = new  ViewStateInfo(
                                           hashCode,
                                           viewName,
                                           interfaceImplementer,
                                           defaultFormat,
                                           null) {HasAttributes = true};
                }

                Views.Add(hashCode, viewInfo);

                CurrentView = Views[hashCode];
                CurrentView.ViewPlannableStatusChanged += new ViewStateInfo.ViewStateInfo_ViewPlannableStatusChange(CurrentView_ViewPlannableStatusChanged);
            }
            else
            {
                //clear out the dictionary of server formats.
                CurrentView.DefaultFormats.Clear();
                CurrentView.HasAttributes = attributeView;
            }
        }

        ///// <summary>
        ///// Registers the grid in the internal structure.
        ///// </summary>
        ///// <param name="hashCode">Hash code of the worksheet.</param>
        ///// <param name="viewName">Name of the view.</param>
        ///// <param name="interfaceImplementer">The GridInterface to register.</param>
        ///// <param name="defaultFormat">The default format to be used to fill in blank values.</param>
        //public void RegisterGrid(int hashCode, string viewName, GridInterface interfaceImplementer, Format defaultFormat)
        //{
        //    RegisterGrid(hashCode, viewName, interfaceImplementer, defaultFormat, null);
        //}


        void CurrentView_ViewPlannableStatusChanged(object sender,
            ViewPlannableStatusChangeEventArgs viewPlannableStatusChangeEventArgs)
        {
            if (!viewPlannableStatusChangeEventArgs.NewPlannableStatus && ViewNotPlannable == null)
                return;

            if (viewPlannableStatusChangeEventArgs.NewPlannableStatus && ViewIsPlannable == null)
                return;

            if (viewPlannableStatusChangeEventArgs.ProtectionMngr != null)
            {
                if (viewPlannableStatusChangeEventArgs.HashCode != 0)
                {
                    if (viewPlannableStatusChangeEventArgs.NewPlannableStatus)
                    {
                        ViewIsPlannable(this, viewPlannableStatusChangeEventArgs);
                    }
                    else
                    {
                        ViewNotPlannable(this, viewPlannableStatusChangeEventArgs);
                    }
                }
            }
        }

        /// <summary>
        /// Removes a view from the view manager.
        /// </summary>
        /// <param name="hashCode">The name of the grid to remove.</param>
        /// <returns>the hash code of view that was removed was the current view, zero if the view that 
        /// was removed was not the current view or no view was removed.</returns>
        public int RemoveView(int hashCode)
        {
            int currView = 0;

            if (Views.ContainsKey(hashCode))
            {
                //Save the view name.
                string viewName = Views[hashCode].ViewName;

                //Remove the view from the dictionary.
                Views.Remove(hashCode);

                //TTN-1040
                //the current view has been removed so reset the protection mngr.
                if(_CurrentView.HashCode == hashCode)
                {
                    currView = hashCode;
                    _CurrentView.GetProtectionMngr().ClearProtection();
                }

                //Fire the event if someone is listening.
                if (ViewRemoved != null)
                    ViewRemoved(this, viewName);
            }
            return currView;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hashCode"></param>
        /// <param name="resetProtectionMngr"></param>
        [DebuggerHidden]
        public void ResetView(int hashCode, bool resetProtectionMngr)
        {
            if (Views.ContainsKey(hashCode))
            {
                Views[hashCode].ResetOlapView();

                if (resetProtectionMngr)
                {
                    Views[hashCode].ResetProtectionMngr();
                }
            }
        }

        /// <summary>
        /// Checks to see if the grid exists.
        /// </summary>
        /// <param name="hashCode">Hash code of the worksheet.</param>
        /// <returns>True if the grid exists, false if not or an error occurs.</returns>
        [DebuggerHidden]
        public bool ViewExists(int hashCode)
        {
            try
            {
                if (!Views.ContainsKey(hashCode))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Makes all of the grids in the view manager writeable.
        /// </summary>
        public void MakeAllProtMngPlanable()
        {
            foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
            {
                kv.Value.Plannable = true;
            }
        }

        /// <summary>
        /// Makes all of the member tag in the view manager clean.
        /// </summary>
        public void MakeAllMbrTagsClean()
        {
            foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
            {
                kv.Value.DirtyMbrTags = true;
            }
        }


        /// <summary>
        /// Makes all of the member tag formatting manager dirty except for the specified view.
        /// </summary>
        /// <param name="notDirty">The view that will NOT be marked as Dirty.</param>
        public void MakeAllMemberTagFormattingMngrDirtyExcept(int notDirty)
        {
            foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
            {
                if (!kv.Key.Equals(notDirty))
                {
                    kv.Value.DirtyMbrTagFormatting = true;
                }
                else
                {
                    kv.Value.DirtyMbrTagFormatting = false;
                }
            }
        }

        /// <summary>
        /// Makes all of the member tags dirty except for the specified view.
        /// </summary>
        /// <param name="notDirty">The view that will NOT be marked as Dirty.</param>
        public void MakeAllMemberTagsDirtyExcept(int notDirty)
        {
            foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
            {
                if (!kv.Key.Equals(notDirty))
                {
                    kv.Value.DirtyMbrTags = true;
                }
                else
                {
                    kv.Value.DirtyMbrTags = false;
                }
            }
        }

        /// <summary>
        /// Makes all of the grids in the view manager dirty except for the specified view.
        /// </summary>
        /// <param name="notDirty">The view that will NOT be marked as Dirty.</param>
        public void MakeAllProtMngrDirtyExcept(int notDirty)
        {
            foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
            {
                if (!kv.Key.Equals(notDirty))
                {
                    kv.Value.ClearProtection();
                    kv.Value.Dirty = true;
                }
                else
                {
                    kv.Value.Dirty = false;
                }
            }
        }

        /// <summary>
        /// Makes all of the grids in the view manager read only except for the specified view.
        /// </summary>
        /// <param name="notReadOnly">The view that will NOT be marked as ReadOnly.</param>
        public void MakeAllProtMngrNotPlanableExcept(int notReadOnly)
        {
            foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
            {
                if (!kv.Key.Equals(notReadOnly))
                {
                    kv.Value.ClearProtection();
                    kv.Value.Plannable = false;
                }
                else
                {
                    kv.Value.Plannable = true;
                }
            }
        }

        /// <summary>
        /// Removes any deleted grids from the internal list.
        /// </summary>
        /// <param name="listOfSheets">List of current worksheet hashes.</param>
        /// <returns>a list a int's, if the list contains a number > 0 the current view/view sheet was deleted.</returns>
        public List<int> RemoveDeletedGrids(List<int> listOfSheets)
        {
            //create a new list to hold the results.
            List<int> results = new List<int>(listOfSheets.Count);

            //Have to iterate backwords so we delete from the back.  If we delete
            //from the front, you can get an enumeration error.

            //Copy the keys to an array so we can loop backwords.
            int[] tempArr = new int[Views.Keys.Count];
            Views.Keys.CopyTo(tempArr, 0);

            //Loop and delete if the grid no longer exists.
            for (int i = tempArr.Length - 1; i >= 0; i--)
            {
                if (!listOfSheets.Contains(tempArr[i]))
                {
                    results.Add(RemoveView(tempArr[i]));
                }
            }
            return results;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hashCode">Hash code of the worksheet.</param>
        /// <param name="node">The object associated with this view.</param>
        public void SetViewNode(int hashCode, object node)
        {
            if (Views.ContainsKey(hashCode))
                Views[hashCode].ViewNode = node;
        }

        /// <summary>
        /// Applies formatting to the view.
        /// </summary>
        public void ApplyFormatting(bool resetProtectionMngrFormatting)
        {
            PafApp.GetFormattingMngr().ApplyFormatting(CurrentView, resetProtectionMngrFormatting);
        }

        /// <summary>
        /// Applies session lock protected formatting to the view.
        /// </summary>
        public void ApplySessionLockProtectedFormats(bool applyProtectionFormatting)
        {
            PafApp.GetFormattingMngr().ApplySessionLockProtectedFormats(applyProtectionFormatting);
        }

        /// <summary>
        /// Apply formatting to the data ranges for all PAFBlank members
        /// </summary>
        public void ApplyBlankFormatting()
        {
            PafApp.GetFormattingMngr().ApplyBlankFormatting(CurrentView);
        }

        /// <summary>
        /// Sets all the styles for a worksheet.
        /// </summary>
        public void ResetMemberTagFormatChanges(ViewStateInfo currentView)
        {
            PafApp.GetFormattingMngr().ResetMemberTagFormatChanges(currentView);
        }

        /// <summary>
        /// Sets all the styles for a worksheet.
        /// </summary>
        public void ResetFormatChanges()
        {
            PafApp.GetFormattingMngr().ResetFormatChanges(CurrentView);
        }

        /// <summary>
        /// Restore the member tag changed cell formatting.
        /// </summary>
        public void RestoreMemberTagChangedCellFormatting()
        {
            PafApp.GetFormattingMngr().RestoreMemberTagChangedCellFormatting();
        }

        /// <summary>
        /// Returns whether or not any Protection Mngr has pending calculations.
        /// </summary>
        public bool AreAnyCalculationsPending()
        {
            foreach (KeyValuePair<int, ViewStateInfo> kv in Views)
            {
                if (kv.Value.GetProtectionMngr().CalculationsPending())
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        public void Sort()
        {   
            //Get the cached view...
            pafView currentPafView= PafApp.GetServiceMngr().GetCurrentPafView(CurrentView.ViewName);

            //Populate the dictionary of tuplesets with row metadata and data
            SortedDictionary<int, TupleSet> TupleSets = PopulateTupleSetsUsingGrid(currentPafView);

            //check for single cell view ^

            //Sort the selected rows by the SortOn columns in the Sort form
            SortOnSelectedColumns(TupleSets);

            //Update the pafView from the Tuplesets dictionary after the sort
            double?[] cachedDataArray;
            UpdatePafView(currentPafView, TupleSets, out cachedDataArray);

            //Rebuild the view section
            PafApp.GetEventManager().RefreshView(CurrentView.ViewName, false);

            //Populate the Data Slice data with the cached data - for the Refresh functionality
            PafApp.GetServiceMngr().GetCurrentPafView(CurrentView.ViewName).viewSections[0].pafDataSlice.data = cachedDataArray;

            //Store the current sort order to be used later
            CurrentOlapView.CurrentSortOrder.Clear();
            foreach(KeyValuePair<int, TupleSet> kv in TupleSets)
            {
                CurrentOlapView.CurrentSortOrder.Add(kv.Value.Order);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void SortPafView(pafView pafView, List<int> currentSortOrder)
        {
            //Populate the dictionary of tuplesets with row metadata and data
            Dictionary<int, TupleSet> TupleSets = PopulateTupleSetsUsingDataSlice(pafView);
            SortedDictionary<int, TupleSet> newTupleSets = new SortedDictionary<int, TupleSet>();

            //Sort based on the current sort order
            int i = 1;
            foreach (int position in currentSortOrder)
            {
                newTupleSets[i++] = TupleSets[position];
            }

            UpdatePafView(pafView, newTupleSets);
        }

        /// <summary>
        /// 
        /// </summary>
        public void SortPafDataSlice(pafDataSlice dataSlice, List<int> currentSortOrder)
        {
            //Get the cached pafView RowTuples & ColTuples
            viewTuple[] rowTuples = PafApp.GetServiceMngr().GetCurrentRowTuples(CurrentView.ViewName);
            viewTuple[] colTuples = PafApp.GetServiceMngr().GetCurrentColTuples(CurrentView.ViewName);

            //Populate the dictionary of tuplesets with row metadata and data
            Dictionary<int, TupleSet> TupleSets = PopulateTupleSetsUsingDataSlice(rowTuples, colTuples, dataSlice);
            Dictionary<int, TupleSet> newTupleSets = new Dictionary<int, TupleSet>();

            //Sort based on the current sort order
            int i = 1;
            foreach (int position in currentSortOrder)
            {
                newTupleSets[i++] = TupleSets[position];
            }

            //Build data slice
            int j = 0;
            double?[] dataArray = new double?[dataSlice.data.Length];
            foreach (KeyValuePair<int, TupleSet> kv in newTupleSets)
            {
                if (kv.Value.IsBlank != true)
                {
                    if (kv.Value.Data != null)
                    {
                        foreach (double dataValue in kv.Value.Data)
                        {
                            dataArray[j++] = dataValue;
                        }
                    }
                }
            }

            dataSlice.data = dataArray;

        }

        /// <summary>
        /// This method is used to build a dictionary of TupleSets that contain only data.
        /// </summary>
        /// <param name="rowTuples">pafView row ViewTuples in the original server order.</param>
        /// <param name="colTuples">pafView col ViewTuples in the original server order.</param>
        /// <param name="dataSlice">pafView pafDataSlice in the original server order.</param>
        /// <returns></returns>
        private Dictionary<int, TupleSet> PopulateTupleSetsUsingDataSlice(viewTuple[] rowTuples, viewTuple[] colTuples, pafDataSlice dataSlice)
        {
            Dictionary<int, TupleSet> tupleSets = new Dictionary<int, TupleSet>();  //1 based array
            pafView pafView = PafApp.GetServiceMngr().GetCurrentPafView(CurrentView.ViewName);
            string blankMember = Settings.Default.Blank;
            int mbrTagCnt = 0;
            int k = 0;

            for (int r = 1; r <= rowTuples.Length; r++)
            {
                TupleSet tupleSet = new TupleSet();
                if (rowTuples[r - 1].memberTag)
                {
                    List<string> mbrTagData = new List<string>();
                    mbrTagData.Add(PafAppConstants.INVALID_MEMBER_TAG_CELL);
                    tupleSet.MemberTagData = mbrTagData;
                    tupleSet.IsPartOfSortCriteria = IsRowPartOfSortSel(CurrentOlapView, r);
                }
                else if (rowTuples[r - 1].memberDefs[rowTuples[r - 1].memberDefs.Length - 1].ToUpper() != blankMember)
                {
                    List<string> mbrTagData = new List<string>();
                    AddMemberTagDataToList(mbrTagCnt, ref mbrTagData, pafView.viewSections[0]);
                    tupleSet.MemberTagData = mbrTagData;
                    tupleSet.IsPartOfSortCriteria = IsRowPartOfSortSel(CurrentOlapView, r);
                    mbrTagCnt++;

                    //Get a List of data values for each column in a row
                    List<object> colData = new List<object>();

                    for (int c = 1; c <= colTuples.Length; c++)
                    {
                        if (colTuples[c - 1].memberDefs[colTuples[c - 1].memberDefs.Length - 1].ToUpper()
                            != blankMember && !colTuples[c - 1].memberTag)
                        {
                            colData.Add(dataSlice.data[k++]);
                        }
                    }

                    //Add the data for each row to the TupleSet
                    tupleSet.Data = colData;
                }
                else
                {
                    tupleSet.IsBlank = true;
                    mbrTagCnt++;
                }

                //Add the TupleSet to the dictionary
                tupleSets.Add(r, tupleSet);
            }

            return tupleSets;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pafView"></param>
        /// <returns></returns>
        private Dictionary<int, TupleSet> PopulateTupleSetsUsingDataSlice(pafView pafView)
        {
            Dictionary<int, TupleSet> tupleSets = new Dictionary<int, TupleSet>();  //1 based array
            Dictionary<int, List<lockedCell>> nonPlannableLockedCellsbyRow; //1 based array
            Dictionary<int, List<lockedCell>> forwardPlannableLockedCellsbyRow; //1 based array
            Dictionary<int, List<lockedCell>> invalidAttrIntersectionsLockedCellbyRow; //1 based array
            Dictionary<int, List<lockedCell>> invalidMbrTagIntersectionsLockedCellbyRow; //1 based array
            viewTuple[] colTuples = pafView.viewSections[0].colTuples;
            viewTuple[] rowTuples = pafView.viewSections[0].rowTuples;
            pafDataSlice dataSlice = pafView.viewSections[0].pafDataSlice;
            int mbrTagCnt = 0;
            string blankMember = Settings.Default.Blank;

            PopulateNonPlannableDictionaries(pafView, 
                out nonPlannableLockedCellsbyRow, 
                out forwardPlannableLockedCellsbyRow,
                out invalidAttrIntersectionsLockedCellbyRow,
                out invalidMbrTagIntersectionsLockedCellbyRow);

            int k = 0;

            for (int r = 1; r <= rowTuples.Length; r++)
            {

                TupleSet tupleSet = new TupleSet();
                //Get a List of data values for each column in a row
                List<object> colData = new List<object>();

                if (rowTuples[r - 1].memberTag)
                {
                    List<string> mbrTagData = new List<string>();
                    mbrTagData.Add(PafAppConstants.INVALID_MEMBER_TAG_CELL);
                    tupleSet.MemberTagData = mbrTagData;
                    tupleSet.IsPartOfSortCriteria = IsRowPartOfSortSel(CurrentOlapView, r);
                }
                else if (rowTuples[r - 1].memberDefs[rowTuples[r - 1].memberDefs.Length - 1].ToUpper() !=
                         blankMember)
                {
                    List<string> mbrTagData = new List<string>();
                    AddMemberTagDataToList(mbrTagCnt, ref mbrTagData, pafView.viewSections[0]);
                    tupleSet.MemberTagData = mbrTagData;
                    tupleSet.IsPartOfSortCriteria = IsRowPartOfSortSel(CurrentOlapView, r);
                    mbrTagCnt++;

                    for (int c = 1; c <= colTuples.Length; c++)
                    {
                        if (colTuples[c - 1].memberDefs[colTuples[c - 1].memberDefs.Length - 1].ToUpper() !=
                                 blankMember && !colTuples[c - 1].memberTag)
                        {
                            colData.Add(dataSlice.data[k++]);
                        }
                    }
                    //Add the data for each row to the TupleSet
                    tupleSet.Data = colData;
                }
                else
                {
                    tupleSet.IsBlank = true;
                    mbrTagCnt++;
                }
            
                //Add the row metadata
                tupleSet.Tuple = rowTuples[r - 1];
                

                //Add the row order
                tupleSet.Order = (int)tupleSet.Tuple.order;

                //Add the Non Plannable Locked Cells (1 based)
                if (nonPlannableLockedCellsbyRow.ContainsKey(r))
                {
                    tupleSet.NonPlannableLockedCells = nonPlannableLockedCellsbyRow[r];
                }

                //Add the Forward Plannable Locked Cells (1 based)
                if (forwardPlannableLockedCellsbyRow.ContainsKey(r))
                {
                    tupleSet.ForwardPlannableLockedCells = forwardPlannableLockedCellsbyRow[r];
                }

                //Add the Invalid Attr Inter Locked Cells
                if (invalidAttrIntersectionsLockedCellbyRow.ContainsKey(r))
                {
                    tupleSet.InvalidAttrIntersectionsLockedCell = invalidAttrIntersectionsLockedCellbyRow[r];
                }

                //add the invalid member tag locked cells.
                if (invalidMbrTagIntersectionsLockedCellbyRow.ContainsKey(r))
                {
                    tupleSet.InvalidMbrTagIntersectionsLockedCell = invalidMbrTagIntersectionsLockedCellbyRow[r];
                }

                //Add the TupleSet to the dictionary
                tupleSets.Add(r, tupleSet);
            }
            return tupleSets;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private SortedDictionary<int, TupleSet> PopulateTupleSetsUsingGrid(pafView pafView)
        {
            
            object[,] arrayS;
            OlapView olapView = CurrentOlapView;
            ContiguousRange dataRange = olapView.DataRange;
            SortedDictionary<int, TupleSet> tupleSets = new SortedDictionary<int, TupleSet>();  //1 based array
            Dictionary<int, List<lockedCell>> nonPlannableLockedCellsbyRow; //1 based array
            Dictionary<int, List<lockedCell>> forwardPlannableLockedCellsbyRow; //1 based array
            Dictionary<int, List<lockedCell>> invalidAttrIntersectionsLockedCellbyRow;
            Dictionary<int, List<lockedCell>> invalidMbrTagIntersectionsLockedCellbyRow;
            int k = 0;
            int rowLength;

            int colLength = (dataRange.BottomRight.Col - dataRange.TopLeft.Col + 1);
            if (dataRange.Cells.Count == 1)
            {
                //TTN-1998
                double d = olapView.Grid.GetValueDouble(dataRange.TopLeft.Row, dataRange.TopLeft.Col);
                arrayS = new object[2, 2];
                //use a one based array, becuase of Excel - Hence the extra item in the array.
                arrayS[1, 1] = d;
                rowLength = 1;
            }
            else
            {
                arrayS = olapView.Grid.GetValueObjectArray(dataRange);
                rowLength = (arrayS.Length / colLength);

            }

            viewTuple[] rowTuples = pafView.viewSections[0].rowTuples;

            PopulateNonPlannableDictionaries(pafView,
                out nonPlannableLockedCellsbyRow, 
                out forwardPlannableLockedCellsbyRow,
                out invalidAttrIntersectionsLockedCellbyRow,
                out invalidMbrTagIntersectionsLockedCellbyRow);

            //Get the DataSlice that has been cached to use for a refresh
            pafDataSlice cachedDataSlice = pafView.viewSections[0].pafDataSlice;

            for (int r = 1; r <= rowLength; r++)
            {
                TupleSet tupleSet = new TupleSet();

                //Get a List of data values for each column in a row
                List<object> colData = new List<object>();

                //Get a List of cached data values for each column in a row
                List<double?> cachedColData = new List<double?>();

                //col member tag data
                List<string> mbrTagData = new List<string>();

                //Identify blank rows
                if (CurrentOlapView.RowBlanks.Contains(r - 1))
                {
                    tupleSet.IsBlank = true;
                }

                for (int c = 1; c <= colLength; c++)
                {
                    if (olapView.MemberTag.RowMbrTagBlanks.ContainsKey(r - 1))
                    {
                        memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(CurrentOlapView.MemberTag.RowMbrTagBlanks[r - 1]);
                        AddMemberTagDataToList(olapView, dataRange.TopLeft.Col + c - 1, dataRange.TopLeft.Row + r - 1, c, r, arrayS, tagDef, ref mbrTagData);
                    }
                    //There is no data for blank columns
                    else if (!olapView.ColBlanks.Contains(c - 1))
                    {
                        if (arrayS[r, c] != null)
                        {
                            colData.Add(arrayS[r, c]);
                        }
                        else
                        {
                            colData.Add(Double.PositiveInfinity);
                        }

                        if (!tupleSet.IsBlank)
                        {
                            cachedColData.Add(cachedDataSlice.data[k++]);
                        }
                    }
                    else if (olapView.MemberTag.ColMbrTagBlanks.ContainsKey(c - 1))
                    {
                        memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(CurrentOlapView.MemberTag.ColMbrTagBlanks[c - 1]);
                        AddMemberTagDataToList(olapView, dataRange.TopLeft.Col + c - 1, dataRange.TopLeft.Row + r - 1, c, r, arrayS, tagDef, ref mbrTagData);
                    }
                    else if (olapView.MemberTag.RowMbrTagBlanks.ContainsKey(c - 1))
                    {
                        memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(CurrentOlapView.MemberTag.RowMbrTagBlanks[c - 1]);
                        AddMemberTagDataToList(olapView, dataRange.TopLeft.Col + c - 1, dataRange.TopLeft.Row + r - 1, c, r, arrayS, tagDef, ref mbrTagData);
                    }
                }
        
                //Add the Data from the screen and the cached data from the mid-tier to the TupleSet
                tupleSet.Data = colData;
                tupleSet.CachedData = cachedColData;
                tupleSet.MemberTagData = mbrTagData;
                tupleSet.IsPartOfSortCriteria = IsRowPartOfSortSel(olapView, r);
                
                //Add the row metadata
                tupleSet.Tuple = rowTuples[r - 1];

                //Add the row order
                tupleSet.Order = (int) tupleSet.Tuple.order;

                //Add the Non Plannable Locked Cells
                if (nonPlannableLockedCellsbyRow.ContainsKey(r))
                {
                    tupleSet.NonPlannableLockedCells = nonPlannableLockedCellsbyRow[r];
                }

                //Add the Forward Plannable Locked Cells
                if (forwardPlannableLockedCellsbyRow.ContainsKey(r))
                {
                    tupleSet.ForwardPlannableLockedCells = forwardPlannableLockedCellsbyRow[r];
                }

                //Add the Invalid Attr Inter Locked Cells
                if (invalidAttrIntersectionsLockedCellbyRow.ContainsKey(r))
                {
                    tupleSet.InvalidAttrIntersectionsLockedCell = invalidAttrIntersectionsLockedCellbyRow[r];
                }

                //add the invalid member tag locked cells.
                if(invalidMbrTagIntersectionsLockedCellbyRow.ContainsKey(r))
                {
                    tupleSet.InvalidMbrTagIntersectionsLockedCell = invalidMbrTagIntersectionsLockedCellbyRow[r];
                }

                //Add the TupleSet to the dictionary
                tupleSets.Add(r, tupleSet);
            }

            return tupleSets;
        }

        /// <summary>
        /// Add member tags to the member tag data list.
        /// </summary>
        /// <param name="olapView">Current olap view.</param>
        /// <param name="curCol">Current col.</param>
        /// <param name="curRow">Current row.</param>
        /// <param name="c">Column index.</param>
        /// <param name="r">Row index.</param>
        /// <param name="arrayS">Array holding the data.</param>
        /// <param name="tagDef">Member tag data def.</param>
        /// <param name="mbrTagData">List to hold the member tag data (ref parm).</param>
        private static void AddMemberTagDataToList(OlapView olapView, int curCol, int curRow, 
            int c, int r, object[,] arrayS, memberTagDef tagDef, ref List<string> mbrTagData)
        {
            Cell topMergedCell;
            CellAddress ca = new CellAddress(curRow, curCol);
            if (!olapView.MemberTag.IsIntersectionInvalid(ca))
            {
                //get the merged status of the cell address.
                bool isMerged = olapView.Grid.IsMerged(new Range(ca), out topMergedCell);
                //get the value from the array.
                object value = arrayS[r, c];
                //check the merged cell value and replace the value if necessary.
                if (isMerged && topMergedCell != null && topMergedCell.Value != null)
                {
                    value = topMergedCell.Value;
                }
                else
                {
                    //if the member tag type if a formula, then we have to go back to the grid, and
                    //get extract the formula - since its not text.
                    switch (tagDef.type)
                    {
                        case memberTagType.FORMULA:
                            value = olapView.Grid.GetValueFormula(curRow, curCol);
                            break;
                    }
                }
                //put the values in the list, making sure we add blank values just like the server
                //does.
                if (value != null)
                {
                    mbrTagData.Add(value.ToString());
                }
                else
                {
                    mbrTagData.Add(String.Empty);
                }
            }
            else
            {
                mbrTagData.Add(PafAppConstants.INVALID_MEMBER_TAG_CELL);
            }
        }

        /// <summary>
        /// Add member tags to the member tag data list.
        /// </summary>
        /// <param name="curRow">Current row.</param>
        /// <param name="mbrTagData">List to hold the member tag data (ref parm).</param>
        /// <param name="viewSection">Current view section.</param>
        private static void AddMemberTagDataToList(int curRow,
            ref List<string> mbrTagData, pafViewSection viewSection)
        {
            if (viewSection.colMemberTagData == null)
            {
                return;
            }

            foreach (memberTagViewSectionData data in viewSection.colMemberTagData)
            {
                mbrTagData.Add(data.memberTagValues[curRow]);
            }

            if(mbrTagData.Count == 0)
            {
                mbrTagData.Add(null);
            }

        }

        /// <summary>
        /// Checks to see if the row tuple is part of the sort selections.
        /// </summary>
        /// <param name="olapView">Olap view.</param>
        /// <param name="row">Row number.</param>
        /// <returns>true if the tuple is part of the selection, false if not.</returns>
        private static bool IsRowPartOfSortSel(OlapView olapView, int row)
        {
            foreach(SortSelection ss in olapView.SortSelections)
            {
                if(ss.SortPositions.Contains(row))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Populates the dictionaries of locked cells.
        /// </summary>
        /// <param name="pafView"></param>
        /// <param name="nonPlannableLockedCellsbyRow"></param>
        /// <param name="forwardPlannableLockedCellsbyRow"></param>
        /// <param name="invalidAttrIntersectionsLockedCellbyRow"></param>
        /// <param name="invalidMbrTagIntersectionsLockedCellbyRow"></param>
        private void PopulateNonPlannableDictionaries(pafView pafView, out Dictionary<int, List<lockedCell>> nonPlannableLockedCellsbyRow, 
            out Dictionary<int, List<lockedCell>> forwardPlannableLockedCellsbyRow,
            out Dictionary<int, List<lockedCell>> invalidAttrIntersectionsLockedCellbyRow,
            out Dictionary<int, List<lockedCell>> invalidMbrTagIntersectionsLockedCellbyRow)
        {   
            lockedCell[] nonPlannableLockedCells = pafView.viewSections[0].notPlannableLockedCells;
            lockedCell[] forwardPlannableLockedCells = pafView.viewSections[0].forwardPlannableLockedCell;
            lockedCell[] invalidAttrIntersectionsLockedCell = pafView.viewSections[0].invalidAttrIntersectionsLC;
            lockedCell[] invalidMbrTagIntersectionsLockedCell = pafView.viewSections[0].invalidMemberTagIntersectionsLC;

            nonPlannableLockedCellsbyRow = new Dictionary<int, List<lockedCell>>(); //1 based array
            forwardPlannableLockedCellsbyRow = new Dictionary<int, List<lockedCell>>(); //1 based array
            invalidAttrIntersectionsLockedCellbyRow = new Dictionary<int, List<lockedCell>>(); //1 based array
            invalidMbrTagIntersectionsLockedCellbyRow = new Dictionary<int, List<lockedCell>>(); //1 based array

            //Populate a dictionary of Lists of non plannable locked cells.  There is a list for each unique row index.
            if (nonPlannableLockedCells != null)
            {
                foreach (lockedCell lockedCell in nonPlannableLockedCells)
                {
                    if (lockedCell != null)
                    {
                        if (!nonPlannableLockedCellsbyRow.ContainsKey(lockedCell.rowIndex))
                        {
                            nonPlannableLockedCellsbyRow.Add(lockedCell.rowIndex, new List<lockedCell>());
                        }
                        nonPlannableLockedCellsbyRow[lockedCell.rowIndex].Add(lockedCell);
                    }
                }
            }

            //Populate a dictionary of Lists of forward plannable locked cells.  There is a list for each unique row index.
            if (forwardPlannableLockedCells != null)
            {
                foreach (lockedCell lockedCell in forwardPlannableLockedCells)
                {
                    if (lockedCell != null)
                    {
                        if (!forwardPlannableLockedCellsbyRow.ContainsKey(lockedCell.rowIndex))
                        {
                            forwardPlannableLockedCellsbyRow.Add(lockedCell.rowIndex, new List<lockedCell>());
                        }
                        forwardPlannableLockedCellsbyRow[lockedCell.rowIndex].Add(lockedCell);
                    }
                }
            }

            //Populate a dictionary of Lists of forward plannable locked cells.  There is a list for each unique row index.
            if (invalidAttrIntersectionsLockedCell != null)
            {
                foreach (lockedCell lockedCell in invalidAttrIntersectionsLockedCell)
                {
                    if (lockedCell != null)
                    {
                        if (!invalidAttrIntersectionsLockedCellbyRow.ContainsKey(lockedCell.rowIndex))
                        {
                            invalidAttrIntersectionsLockedCellbyRow.Add(lockedCell.rowIndex, new List<lockedCell>());
                        }
                        invalidAttrIntersectionsLockedCellbyRow[lockedCell.rowIndex].Add(lockedCell);
                    }
                }
            }

            //Populate a dictionary of Lists of forward plannable locked cells.  There is a list for each unique row index.
            if (invalidMbrTagIntersectionsLockedCell != null)
            {
                foreach (lockedCell lockedCell in invalidMbrTagIntersectionsLockedCell)
                {
                    if (lockedCell != null)
                    {
                        if (!invalidMbrTagIntersectionsLockedCellbyRow.ContainsKey(lockedCell.rowIndex))
                        {
                            invalidMbrTagIntersectionsLockedCellbyRow.Add(lockedCell.rowIndex, new List<lockedCell>());
                        }
                        invalidMbrTagIntersectionsLockedCellbyRow[lockedCell.rowIndex].Add(lockedCell);
                    }
                }
            }

        }

        /// <summary>
        /// Made Public for Unit Testing purposes only
        /// </summary>
        /// <param name="TupleSets"></param>
        public void SortOnSelectedColumns(SortedDictionary<int, TupleSet> TupleSets)
        {
            List<SortCriteria> SortedRows = new List<SortCriteria>();
            foreach (int position in CurrentOlapView.SortPositions)
            {
                if (CurrentOlapView.SortByList.Count > 0)
                {
                    SortCriteria sortCriteria = new SortCriteria();
                    sortCriteria.Position = position;
                    sortCriteria.TupleSet = TupleSets[position];
                    //sortCriteria.SortOn1 = (double)TupleSets[position].Data[CurrentOlapView.SortByList[0].Position];
                    if (TupleSets[position].Data != null && TupleSets[position].Data.Count > 0)
                    {
                        sortCriteria.SortOn1 = (double)TupleSets[position].Data[CurrentOlapView.SortByList[0].Position];
                    }
                    else
                    {
                        sortCriteria.SortOn1 = TupleSets[position].MemberTagData[CurrentOlapView.SortByList[0].Position];
                    }
                    sortCriteria.Ascending1 = CurrentOlapView.SortByList[0].Ascending;
                    if (CurrentOlapView.SortByList.Count > 1)
                    {
                        //sortCriteria.SortOn2 = (double)TupleSets[position].Data[CurrentOlapView.SortByList[1].Position];
                        if (TupleSets[position].Data != null && TupleSets[position].Data.Count > 0)
                        {
                            sortCriteria.SortOn2 = (double)TupleSets[position].Data[CurrentOlapView.SortByList[1].Position];
                        }
                        else
                        {
                            sortCriteria.SortOn2 = TupleSets[position].MemberTagData[CurrentOlapView.SortByList[1].Position];
                        }
                        sortCriteria.Ascending2 = CurrentOlapView.SortByList[1].Ascending;

                        if (CurrentOlapView.SortByList.Count > 2)
                        {
                            //sortCriteria.SortOn3 = (double)TupleSets[position].Data[CurrentOlapView.SortByList[2].Position];
                            if (TupleSets[position].Data != null && TupleSets[position].Data.Count > 0)
                            {
                                sortCriteria.SortOn3 = (double)TupleSets[position].Data[CurrentOlapView.SortByList[2].Position];
                            }
                            else
                            {
                                sortCriteria.SortOn3 = TupleSets[position].MemberTagData[CurrentOlapView.SortByList[2].Position];
                            }
                            sortCriteria.Ascending3 = CurrentOlapView.SortByList[2].Ascending;
                        }
                    }
                    SortedRows.Add(sortCriteria);
                }
                
            }

            //List of rows/column positions that will be sorted
            List<int> SortedPositions = new List<int>();

            //Remove the TupleSets that will be sorted
            foreach (SortCriteria sortedRow in SortedRows)
            {
                TupleSets.Remove(sortedRow.Position);
                SortedPositions.Add(sortedRow.Position);
            }

            //Sort those TupleSets
            SortedRows.Sort();

            //Place the sorted TupleSets back into the Larger TupleSets dictionary
            int i = 0;
            foreach (SortCriteria sortedRow in SortedRows)
            {
                //if (sortedRow.TupleSet.IsBlank == false)
                //{
                    TupleSets.Add(SortedPositions[i++], sortedRow.TupleSet);
                //}
            }
        }

        private void UpdatePafView(pafView pafView, SortedDictionary<int, TupleSet> TupleSets)
        {
            double?[] cachedDataArray;

            UpdatePafView(pafView, TupleSets, out cachedDataArray);
        }

        

        /// <summary>
        /// Update the paf view data with the sorted data.
        /// </summary>
        /// <param name="pafView"></param>
        /// <param name="TupleSets"></param>
        /// <param name="cachedData"></param>
        private void UpdatePafView(pafView pafView, ICollection<KeyValuePair<int, TupleSet>> TupleSets, 
            out double?[] cachedData)
        {
            //Start key for the TupleSets
            List<double?> dataArray = new List<double?>();
            List<double?> cachedDataArray = new List<double?>();
            List<int> invalidTupleSets = new List<int>();
            List<int> rowMemberTags = new List<int>();
            viewTuple[] viewTuples = new viewTuple[TupleSets.Count];
            string[,] mbrTagData;
            int i = 0;  //increments with each cached data value

            List<lockedCell> nonPlannableLockedCell = new List<lockedCell>();
            List<lockedCell> forwardPlannableLockedCell = new List<lockedCell>();
            List<lockedCell> invalidAttrIntersectionsLockedCell = new List<lockedCell>();
            List<lockedCell> invalidMbrTagIntersectionsLockedCell = new List<lockedCell>();
            int k = 1; //1 based row counter for locked cells
            int c = 0;
            


            //TODO modify this to include the invalid member tag data rows.
            foreach (KeyValuePair<int, TupleSet> kv in TupleSets)
            {
                //Row Member Tag.
                if (kv.Value.MemberTagData != null && kv.Value.MemberTagData.Contains(PafAppConstants.INVALID_MEMBER_TAG_CELL))
                {
                    invalidTupleSets.Add(kv.Key);
                }
                else 
                {
                    i++;

                }
            }

            //if the col mbr tag count is == 0 then no col so set the collection to null.
            if (CurrentOlapView.MemberTag.ColMbrTagBlanks.Count != 0)
            {
                mbrTagData = new string[CurrentOlapView.MemberTag.ColMbrTagBlanks.Count, i];
            }
            else
            {
                mbrTagData = null;
            }
            i = 0;

            foreach (KeyValuePair<int, TupleSet> kv in TupleSets)
            {
                if (!kv.Value.IsBlank)
                {
                    if (kv.Value.Data != null)
                    {
                        foreach (double dataValue in kv.Value.Data)
                        {
                            dataArray.Add(dataValue);
                        }
                    }

                    if (kv.Value.CachedData != null)
                    {
                        foreach (double cachedDataValue in kv.Value.CachedData)
                        {
                            cachedDataArray.Add(cachedDataValue);
                        }
                    }
                    //this only allows the repopulation of member tag values that were part of
                    //the actual sort, we just ignore the values that were not part of the sort.
                    if (kv.Value.MemberTagData != null && mbrTagData != null &&
                        !invalidTupleSets.Contains(kv.Key) && kv.Value.IsPartOfSortCriteria)
                    {
                        int r = 0;
                        foreach (string mbrTagValue in kv.Value.MemberTagData)
                        {
                            mbrTagData[r, c] = mbrTagValue;
                            r++;
                        }
                        c++;
                    }
                    else if (!invalidTupleSets.Contains(kv.Key))
                    //TTN-1245 - modify the else statement to check to see if the key exists in the invalidTupleSets
                    {
                        c++;
                    }
                }
                else
                {
                    //this only allows the repopulation of member tag values that were part of
                    //the actual sort, we just ignore the values that were not part of the sort.
                    if (kv.Value.MemberTagData != null && mbrTagData != null &&
                        !invalidTupleSets.Contains(kv.Key) && kv.Value.IsPartOfSortCriteria)
                    {
                        int r = 0;
                        foreach (string mbrTagValue in kv.Value.MemberTagData)
                        {
                            mbrTagData[r, c] = mbrTagValue;
                            r++;
                        }
                        c++;
                    }
                    else if (!invalidTupleSets.Contains(kv.Key))
                    //TTN-1245 - modify the else statement to check to see if the key exists in the invalidTupleSets
                    {
                        c++;
                    }
                }

                viewTuples[i++] = kv.Value.Tuple;


                //Locked Cells
                if (kv.Value.NonPlannableLockedCells != null)
                {
                    foreach (lockedCell lockedCell in kv.Value.NonPlannableLockedCells)
                    {
                        lockedCell.rowIndex = k;
                        nonPlannableLockedCell.Add(lockedCell);
                    }
                }

                if (kv.Value.ForwardPlannableLockedCells != null)
                {
                    foreach (lockedCell lockedCell in kv.Value.ForwardPlannableLockedCells)
                    {
                        lockedCell.rowIndex = k;
                        forwardPlannableLockedCell.Add(lockedCell);
                    }
                }

                if (kv.Value.InvalidAttrIntersectionsLockedCell != null)
                {
                    foreach (lockedCell lockedCell in kv.Value.InvalidAttrIntersectionsLockedCell)
                    {
                        lockedCell.rowIndex = k;
                        invalidAttrIntersectionsLockedCell.Add(lockedCell);
                    }
                }

                if (kv.Value.InvalidMbrTagIntersectionsLockedCell != null)
                {
                    foreach (lockedCell lockedCell in kv.Value.InvalidMbrTagIntersectionsLockedCell)
                    {
                        lockedCell.rowIndex = k;
                        invalidMbrTagIntersectionsLockedCell.Add(lockedCell);
                    }
                }

                k++;
            }

            //Update the data slice
            pafDataSlice dataSlice = new pafDataSlice();
            dataSlice.columnCount = pafView.viewSections[0].colTuples.Length - CurrentOlapView.ColBlanks.Count;
            dataSlice.data = dataArray.ToArray();
            pafView.viewSections[0].pafDataSlice = dataSlice;

            //Update the Tuple metadata
            pafView.viewSections[0].rowTuples = viewTuples;

            //do not need to anything with row member tag data because it's not ever sorted across
            //so, it will be just repopulated when the view is rerendered.
            //pafView.viewSections[0].rowMemberTagData = rowMemberTagData;

            //update the member tag data (if any)
            if (mbrTagData != null && mbrTagData.Length > 0)
            {
                 for (i = 0; i < mbrTagData.GetLength(0); i++)
                 {
                     for (int j = 0; j < mbrTagData.GetLength(1); j++)
                    {
                        if (mbrTagData[i, j] != null && !mbrTagData[i, j].Equals(PafAppConstants.INVALID_MEMBER_TAG_CELL))
                        {
                            pafView.viewSections[0].colMemberTagData[i].memberTagValues[j] = mbrTagData[i, j];
                        }
                    }
                }
            }

            //Update the Locked Cells
            pafView.viewSections[0].notPlannableLockedCells = nonPlannableLockedCell.ToArray();
            pafView.viewSections[0].forwardPlannableLockedCell = forwardPlannableLockedCell.ToArray();
            pafView.viewSections[0].invalidAttrIntersectionsLC = invalidAttrIntersectionsLockedCell.ToArray();
            pafView.viewSections[0].invalidMemberTagIntersectionsLC = invalidMbrTagIntersectionsLockedCell.ToArray();

            cachedData = cachedDataArray.ToArray();
        }

        public string UnSortedDataSlice(out int colCount)
        {
            ContiguousRange dataRange = CurrentOlapView.DataRange;
            int rowLength;
            int colLength;
            object[,] arrayS = CurrentOlapView.Grid.GetValueObjectArray(dataRange);  //1 based
            List<TupleSet> tupleSets = new List<TupleSet>(); 
            viewTuple[] rowTuples = PafApp.GetServiceMngr().GetCurrentPafView(PafApp.GetViewMngr().CurrentView.ViewName).viewSections[0].rowTuples;
            
            colLength = (dataRange.BottomRight.Col - dataRange.TopLeft.Col + 1);
            rowLength = (arrayS.Length / colLength);
            colCount = colLength - CurrentOlapView.ColBlanks.Count;

            for (int i = 1; i <= rowLength; i++)
            {
                TupleSet tupleSet = new TupleSet();

                //Get a List of data values for each column in a row
                List<object> colData = new List<object>();

                //Identify blank rows
                if (!CurrentOlapView.RowBlanks.Contains(i - 1))
                {
                    for (int j = 1; j <= colLength; j++)
                    {
                        if (!CurrentOlapView.ColBlanks.Contains(j - 1))
                        {
                            if (arrayS[i, j] != null)
                            {
                                colData.Add(arrayS[i, j]);
                            }
                            else
                            {
                                colData.Add(0.0);
                            }
                        }
                    }

                    //Add the Data from the screen and the cached data from the mid-tier to the TupleSet
                    tupleSet.Data = colData;

                    //Add the row order
                    tupleSet.Order = (int)rowTuples[i - 1].order;

                    //Add the TupleSet to the dictionary
                    tupleSets.Add(tupleSet);
                }
            }

            tupleSets.Sort();

            StringBuilder getData = new StringBuilder();
            foreach (TupleSet ts in tupleSets)
            {
                foreach (double dataValue in ts.Data)
                {
                    byte[] byteArray = BitConverter.GetBytes(dataValue);
                    getData.Append(Convert.ToBase64String(byteArray));
                }
            }

            return getData.ToString();

        }
    }
}
