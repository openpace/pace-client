#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Titan.Pace.Application.Extensions;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView
{
    internal class SimpleCellNoteMngr : CellNoteMngr
    {
        #region Public Methods

        /// <summary>
        /// Creates a new simple cell note manager.
        /// </summary>
        public SimpleCellNoteMngr() : base()
        {
        }

        /// <summary>
        /// Signals if the client cache is dirty and need to be written back to the server.
        /// </summary>
        /// <returns>true if the client cache is dirty, false if not.</returns>
        public override bool IsClientCacheDirty()
        {
            if (_ClientCache.CellNoteDeletes != null && 
                _ClientCache.CellNoteInsert != null &&
                _ClientCache.CellNoteUpdates != null)
            {
                if (_ClientCache.CellNoteDeletes.Count > 0 || 
                    _ClientCache.CellNoteInsert.Count > 0 ||
                    _ClientCache.CellNoteUpdates.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Clears out the client cell note cache.
        /// </summary>
        public override void ClearCache()
        {
            _ClientCache.ClientCellNoteCache.Clear();
        }

        /// <summary>
        /// Clears out the client delete, insert, and update buckets.
        /// </summary>
        public override void ClearBuckets()
        {
            _ClientCache.CellNoteDeletes.Clear();
            _ClientCache.CellNoteInsert.Clear();
            _ClientCache.CellNoteUpdates.Clear();
            _DuplicateIntersections.Clear();
            _Intersections.Clear();
        }

        /// <summary>
        /// Gets the client cell note cache.
        /// </summary>
        public override Dictionary<Intersection, simpleCellNote> ClientCache
        {
            get { return _ClientCache.ClientCellNoteCache; }
        }

        /// <summary>
        /// Gets the cache of SimpleCellNotes that need to be inserted in the database.
        /// </summary>
        public override simpleCellNote[] GetCellNoteInsertBucketForInsert()
        {
            simpleCellNote[] arr = new simpleCellNote[_ClientCache.CellNoteInsert.Count];
            int i = 0;
            foreach (KeyValuePair<Intersection, simpleCellNote> kvp in _ClientCache.CellNoteInsert)
            {
                UpdateCellNoteInformation(kvp);
                arr[i] = CreateCellNote(kvp.Key, kvp.Value);
                //arr[i] = kvp.Value;
                i++;
            }
            return arr;
        }

        /// <summary>
        /// Gets the cache of SimpleCellNotes that need to be updated into the database.
        /// </summary>
        public override simpleCellNote[] GetCellNoteInsertBucketForUpdate()
        {
            simpleCellNote[] arr = new simpleCellNote[_ClientCache.CellNoteUpdates.Count];
            int i = 0;
            foreach (KeyValuePair<Intersection, simpleCellNote> kvp in _ClientCache.CellNoteUpdates)
            {
                UpdateCellNoteInformation(kvp);
                arr[i] = CreateCellNote(kvp.Key, kvp.Value);
                //arr[i] = kvp.Value;
                i++;
            }
            return arr;
        }

        /// <summary>
        /// Gets the simpleCoordList of addresses that need to be deleted from the database.
        /// </summary>
        public override simpleCoordList GetCellNoteInsertBucketForDelete()
        {
            simpleCoordList scl = null;
            if (_ClientCache.CellNoteDeletes.Count > 0)
            {
                Intersection[] arr = new Intersection[_ClientCache.CellNoteDeletes.Count];

                _ClientCache.CellNoteDeletes.CopyTo(arr, 0);
                if (arr.Length > 0)
                {
                    scl = arr.ToSimpleCoordList(false);
                }
            }
            return scl;
        }

        /// <summary>
        /// Takes the target cell address intersection and finds any cell addresses with the same intersection.
        /// The specified source address is the most updated address, so this function will handle 
        /// updating or deleting the necessary cell notes in the duplicate intersection/cell addresses.
        /// </summary>
        /// <param name="cellAddress">The target cell to copy.</param>
        /// <param name="currentView">The associated view.</param>
        public override void CopyAndDeleteCellNotes(CellAddress cellAddress, ViewStateInfo currentView)
        {
            Intersection intersection = FindIntersection(cellAddress, currentView);
            if(intersection != null)
            {
                CopyAndDeleteCellNotes(intersection, cellAddress, currentView);
            }
        }

        /// <summary>
        /// Copies necessary cell notes to duplicate intersections.  Then performs then updates the client 
        /// cache with the inserts, updates, deletes.
        /// </summary>
        /// <param name="currentView">The associated view.</param>
        public override void CopyAndWriteAllUpdatesInsertsDeletsToClientCache(ViewStateInfo currentView)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            if (currentView == null)
            {
                return;
            }

            try
            {
                var cellNotes = currentView.GetOlapView().Grid.GetCellNotes().Keys;
                foreach (var cell in cellNotes)
                {
                    CopyCellNotesToDuplicateCells(cell, currentView);
                }

                Dictionary<CellAddress, simpleCellNote> newCellNotes =
                    currentView.GetOlapView().Grid.GetCellNotes();

                WriteUpdatesToClientCache(
                    newCellNotes,
                    currentView);

                WriteDeletesToClientCache(
                    currentView.CellNotes,
                    newCellNotes,
                    currentView);
            }
            catch(COMException)
            {
                //cannot get cell notes off gird.
                //TTN-955
                return;
            }
            catch(Exception)
            {
                return;
            }
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("CopyAndWriteAllUpdatesInsertsDeletsToClientCache runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Removed the cached intersections.
        /// </summary>
        /// <param name="viewName">Name of the view to removed the cached intersecions for.</param>
        public override void ClearCachedIntersections(string viewName)
        {
            //Fix TTN-980
            if(_DuplicateIntersections.ContainsKey(viewName))
            {
                _DuplicateIntersections.RemoveSelection(viewName);
            }
            if (_Intersections.ContainsKey(viewName))
            {
                _Intersections.RemoveSelection(viewName);
            }
        }

        /// <summary>
        /// Renders the notes on the view.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public override void RenderNotes(ViewStateInfo currentView)
        {
            if(currentView ==  null)
            {
                return;
            }

            try 
            {
                //now we have to convert these intersections to ca's
                Dictionary<Intersection, simpleCellNote> sheetIntCache;

                //get the client cache dictionary 
                Dictionary<Range, simpleCellNote> sheetCache =
                    GetNotesForView(
                        currentView,
                        out sheetIntCache);

                //clear the cell notes.
                ClearCellNotes(currentView);

                //write the notes to the grid
                currentView.GetOlapView().Grid.SetCellNotes(sheetCache, false, false);

                //set the client cache.
                currentView.CellNotes = sheetIntCache;
            }
            catch(COMException)
            {
                return;
            }
            catch(Exception)
            {
                return;
            }
        }


        /// <summary>
        /// Renders the notes on the view.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public override void UpdateNotes(ViewStateInfo currentView)
        {
            if (currentView == null)
            {
                return;
            }

            try
            {
                //now we have to convert these intersections to ca's
                Dictionary<Intersection, simpleCellNote> sheetIntCache;

                //get the client cache dictionary 
                Dictionary<Range, simpleCellNote> sheetCache =
                    GetNotesForView(
                        currentView,
                        out sheetIntCache);

                //get the deleted notes (if any)
                List<Range> deletes = new List<Range>();
                foreach(Intersection inter in _ClientCache.CellNoteDeletes)
                {
                    List<CellAddress> ca = FindIntersectionAddress2(inter, currentView);
                    if (ca != null)
                    {
                        foreach (CellAddress c in ca)
                        {
                            deletes.Add(new Range(c));
                        }
                    }
                }

                //delete the deleted notes.
                if (deletes.Count > 0)
                {
                    currentView.GetOlapView().Grid.DeleteCellNotes(deletes);
                }

                //write the notes to the grid
                currentView.GetOlapView().Grid.SetCellNotes(sheetCache, false, true);

                //set the client cache.
                currentView.CellNotes = sheetIntCache;
            }
            catch (COMException)
            {
                return;
            }
            catch (Exception)
            {
                return;
            }
        }


        /// <summary>
        /// Renders the notes on the view.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public override void ClearCellNotes(ViewStateInfo currentView)
        {
            if (currentView == null)
            {
                return;
            }

            try
            {
                currentView.GetOlapView().Grid.DeleteAllCellNotes(currentView.GetOlapView().DataRange);
            }
            catch(COMException)
            {
                return;
            }
            catch(Exception)
            {
                return;
            }

        }


        /// <summary>
        /// Updates the client cache from an array of simple cell notes.
        /// </summary>
        /// <param name="simpleNotes">Array of simple cell notes.</param>
        public override void UpdateClientCacheFromServerCache(simpleCellNote[] simpleNotes)
        {
            if(simpleNotes != null && simpleNotes.Length > 0)
            {
                foreach(simpleCellNote note in simpleNotes)
                {
                    if(note != null)
                    {
                        Intersection intersection = new Intersection(
                            note.simpleCoordList.axis, note.simpleCoordList.coordinates);

                        simpleCellNote cn = GetNoteFromCache(intersection);
                        if (cn == null)
                        {
                            if (!_ClientCache.CellNoteDeletes.Contains(intersection))
                            {
                                CacheNote(intersection, note);
                            }
                        }
                    }
                }
            }
        }

        

        /// <summary>
        /// Determines if the cell address is in a valid location for a cell note on the view.
        /// </summary>
        /// <param name="cellAddress">Currently active cell.</param>
        /// <param name="currentView">The associated view.</param>
        public override bool isCellAddressValidForCellNote(CellAddress cellAddress, ViewStateInfo currentView)
        {
            bool enableButtons = false;

            try
            {
                //If the sheet is not plannable(read only) then don't show the buttons.
                if (!currentView.Plannable)
                {
                    enableButtons = false;
                }
                    //If the view does not have a data range, disable the buttons fix
                else if (currentView.GetOlapView().StartCell == null)
                {
                    enableButtons = false;
                }
                    //if the user right clicks in the data range on the view
                else if (currentView.GetOlapView().DataRange.InContiguousRange(cellAddress) && !currentView.ReadOnly)
                {
                    Intersection intersection = FindIntersection(cellAddress, currentView);

                    //the activecell is not a blank
                    Cell adjustedCell = PafApp.GetViewMngr().CurrentOlapView.AdjustCellAddress(cellAddress);

                    if (!currentView.GetOlapView().RowBlanks.Contains(adjustedCell.CellAddress.Row) &&
                        !currentView.GetOlapView().ColBlanks.Contains(adjustedCell.CellAddress.Col))
                    {
                        //the activecell is not locked, protected, or read-only
                        if ((!currentView.GetProtectionMngr().UserLocks.Contains(intersection))
                            && (!currentView.GetProtectionMngr().SystemLocks.Contains(intersection.UniqueID))
                            && (!currentView.GetProtectionMngr().ProtectedCells.Contains(intersection.UniqueID)))
                        {
                            enableButtons = true;
                        }
                    }
                }
            }
            catch(Exception)
            {
                enableButtons = false;
            }
            return enableButtons;
        }

        #endregion Public Methods
    }
}