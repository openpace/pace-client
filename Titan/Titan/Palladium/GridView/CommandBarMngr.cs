#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Titan.Pace.Application.Win32;
using Titan.PafService;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView
{
    internal class CommandBarMngr
    {
        private bool _ShouldBuildRuleSetComboBox = false;

        //private ICustomAction _addinUtilities;


        #region Public Methods

        //public void LoadAutomationObject()
        //{
        //    if(_addinUtilities == null)
        //    {
        //        object addInProgId = "Titan.Addin";
        //        foreach (COMAddIn c in Globals.ThisWorkbook.Application.COMAddIns)
        //        {
        //            Debug.Print(c.ProgId);
        //        }
        //        _addinUtilities = (ICustomAction)Globals.ThisWorkbook.Application.COMAddIns.Item(ref addInProgId).Object;
        //    }
        //}

        /// <summary>
        /// Determines if Lock/unlock buttons should be enabled/disabled depending on the current cell.
        /// </summary>
        public void EnableLockAndPasteShapeButtons()
        {

            RibbonBarButtonStatus buttonStatus = Globals.ThisWorkbook.Ribbon.ButtonStatus;
            PafApp.GetGridApp().EnableLockButtons(buttonStatus.LockEnabled);
            PafApp.GetGridApp().EnableSessionLockButtons(true);
            PafApp.GetGridApp().EnablePasteShapeButtons(buttonStatus.PasteShapeEnabled());
        }

        /// <summary>
        /// Determines cell notes buttons should be enabled/disabled depending on the current cell.
        /// </summary>
        /// <param name="cell">Currently active cell.</param>
        /// <param name="buttons">The list of command bar buttons to enable/disable.</param>
        public void EnableCellNotesButtons(Cell cell, List<CommandBarControl> buttons)
        {
            bool enableButtons = PafApp.GetCellNoteMngr().isCellAddressValidForCellNote(
                cell.CellAddress, 
                PafApp.GetViewMngr().CurrentView);

            PafApp.GetGridApp().EnableCellNotesButtons(enableButtons, buttons);
        }

        /// <summary>
        /// Determines cell notes buttons should be enabled/disabled depending on the current cell.
        /// </summary>
        /// <param name="cell">Currently active cell.</param>
        public bool EnableCellNotesButtonsRibbon(Cell cell)
        {
            return PafApp.GetCellNoteMngr().isCellAddressValidForCellNote(
                cell.CellAddress,
                PafApp.GetViewMngr().CurrentView);


        }

        public void EnableReplicateButtons()
        {
            RibbonBarButtonStatus buttonStatus = Globals.ThisWorkbook.Ribbon.ButtonStatus;
            PafApp.GetGridApp().EnableReplicateButtons(buttonStatus.EnableReplicateButton(), buttonStatus.EnableUnreplicateButton());
            PafApp.GetGridApp().EnableLiftButtons(buttonStatus.EnableLiftButton(), buttonStatus.EnableUnliftButton());
        }

        /// <summary>
        /// Determines if lock/unlock buttons (on the right click menu) should be enabled/disabled depending on the current worksheet and cell.
        /// </summary>
        /// <param name="hashCode">Name of the currently active worksheet.</param>
        public void ContextMenuHandler(int hashCode)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            //Added to fix TTN-257 (If the grid does not exist, then we must reset the right click menu)
            if (PafApp.GetViewMngr().ViewExists(hashCode))
            {
                ShowRightClickMenuButtons(true);
            }
            else
            {
                ShowRightClickMenuButtons(false);
            }
            startTime.Stop();
            PafApp.GetLogger().Info("Memory Usage: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            PafApp.GetLogger().InfoFormat("Complete ContextMenuHandler, runtime: {0} ms", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Show or hide the Role Filter toolbar bar and menubar burrons.
        /// </summary>
        public void ShowRoleFilterToolbarButton()
        {
            PafApp.GetGridApp().ShowRoleFilterToolbarButton();
        }

        /// <summary>
        /// Show or hide the Role Filter toolbar bar and menubar burrons.
        /// </summary>
        public void EnableDisableRoleFilterToolbarButton()
        {
            PafApp.GetGridApp().EnableRoleFilterToolbarButton();
        }

        /// <summary>
        /// Adds the Titan buttons to the Excel sheet right click menu.
        /// </summary>
        public void AddButtonsToRightClickMenu()
        {
            //Add the Lock buttons to the right-click menu
            PafApp.GetGridApp().AddControlstoRightClickMenu();
        }

        /// <summary>
        /// Resets (removes) the Titan buttons from the Excel sheet right click menu.
        /// </summary>
        public void ResetRightClickMenu()
        {
            PafApp.GetGridApp().ResetRightClickMenu();
        }

        public void ShowRightClickMenuButtons(bool showButons)
        {
            PafApp.GetGridApp().ShowRightClickMenuButtons(showButons);
        }

        /// <summary>
        /// Creates the Excel ToolBar.
        /// </summary>
        public void SetToolbarEnabledStatus(bool toolbarStatus)
        {
            PafApp.GetGridApp().SetToolbarStatus(toolbarStatus);
        }

        /// <summary>
        /// Creates the Excel ToolBar.
        /// </summary>
        public void CreateToolbars()
        {
            PafApp.GetGridApp().BuildToolbar();
        }

        /// <summary>
        /// Creates the Excel ToolBar.
        /// </summary>
        public void CreateTestHarnessToolbar()
        {
            PafApp.GetGridApp().BuildTestHarnessToolBarButtons();
        }

        /// <summary>
        /// Creates the Excel MenuBar item.
        /// </summary>
        public void CreateMenu()
        {
            PafApp.GetGridApp().BuildMenu(
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.Name"), "");
        }

        /// <summary>
        /// Deletes the Excel MenuBar item.
        /// </summary>
        public void DeleteMenu()
        {
            PafApp.GetGridApp().DeleteMenus(
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.Name"));
        }

        /// <summary>
        /// Disables all the buttons that are not avaiable when on a non view sheet.
        /// </summary>
        public void NonViewSheetToolbarSetup()
        {
            try
            {
                //Disable the refresh button.
                PafApp.GetGridApp().EnableViewSheetOnlyButtons(false);
                //When changing sheets, we may have to disable some of the toolbar buttons.
                PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();
                //Disable the lock button.
                PafApp.GetGridApp().EnableLockButtons(false);
                //Disable the session button.
                PafApp.GetGridApp().EnableSessionLockButtons(false);
                //Disable the Paste Shape Button.
                PafApp.GetGridApp().EnablePasteShapeButtons(false);
            }
            catch 
            {
            }
        }

        public void ViewSheetToolbarSetup(Cell cell, String sheetName)
        {
            try
            {
                //Hide Essbase AddIn
                PafApp.GetGridApp().ShowHideEssbaseAddIn(false);
                //Hide Hyprion Smart View AddIn
                PafApp.GetGridApp().ShowHideHSVAddIn(false);
                //Show the application bars.
                PafApp.GetCommandBarMngr().ShowCustomApplicationBars(true, true, true);
                //enable the refresh button.
                PafApp.GetGridApp().EnableViewSheetOnlyButtons(true);
                //When changing sheets, we may have to disable some of the toolbar buttons.
                PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();
                //Fix the lock and pasteshape buttons - Fix (TTN-313).
                PafApp.GetCommandBarMngr().EnableLockAndPasteShapeButtons();
            }
            catch
            {
            }
        }

        /// <summary>
        /// Disables the custom class plan menu.
        /// </summary>
        public void DisableCustomCommandMenus()
        {

            if (PafApp.GetPafPlanSessionResponse() != null && 
                PafApp.GetPafPlanSessionResponse().customMenuDefs != null)
            {
                foreach (customMenuDef menuDef in PafApp.GetPafPlanSessionResponse().customMenuDefs)
                {
                    //Skip if no menu definition
                    if (menuDef != null)
                    {
                        PafApp.GetGridApp().EnableMenuButton(menuDef.caption, false);
                    }
                }
            }
        }

        /// <summary>
        /// Enables the custom class plan menu.
        /// </summary>
        /// <param name="viewName">Name of the current view.</param>
        public void EnableCustomCommandMenus(string viewName)
        {
            if (PafApp.GetPafPlanSessionResponse() != null && PafApp.GetPafPlanSessionResponse().customMenuDefs != null)
            {
                foreach (customMenuDef menuDef in PafApp.GetPafPlanSessionResponse().customMenuDefs)
                {
                    //Skip if no menu definition
                    if (menuDef != null)
                    {
                        //If View Filters exist they enable/disable custom menus based on the filters
                        if (menuDef.viewFilter != null && menuDef.viewFilter[0] != null)
                        {
                            bool isFound = false;
                            foreach (string view in menuDef.viewFilter)
                            {
                                if (view.Trim() == viewName.Trim())
                                {
                                    isFound = true;
                                    break;
                                }
                            }
                            PafApp.GetGridApp().EnableMenuButton(menuDef.caption, isFound);
                        }
                        //Enable on all views if no filters exist
                        else
                        {
                            PafApp.GetGridApp().EnableMenuButton(menuDef.caption, true);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Enable or disable the sort button
        /// </summary>
        public void EnableSortMenuButton(bool enabled)
        {
            PafApp.GetGridApp().EnableMenuButton(PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Sort"), enabled);
        }

        /// <summary>
        /// Enable or disable the create assortment button
        /// </summary>
        public void EnableCreateAssortmentMenuButton(bool enabled)
        {
            PafApp.GetGridApp().EnableMenuButton(PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.CreateAssortment"), enabled);
        }

        /// <summary>
        /// Enable or disable the options button
        /// </summary>
        public void EnableOptionsMenuButton(bool enabled)
        {
            PafApp.GetGridApp().EnableMenuButton(PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Options"), enabled);
        }

        /// <summary>
        /// Enable or disable the roles button
        /// </summary>
        public void EnableRolesMenuandToolbarButtons(bool enabled)
        {
            PafApp.GetGridApp().EnableRolesButtons(enabled);

        }

        /// <summary>
        /// Shows/Hides the task pane
        /// </summary>
        /// <param name="showTaskPane">true shows the task pane/false hides it</param>
        public void SetTaskPaneStatus(bool showTaskPane)
        {
            PafApp.GetGridApp().SetTaskPaneStatus(showTaskPane);
        }

        /// <summary>
        /// Enable or disable the change password button
        /// </summary>
        /// <param name="enabled">Enable or disable the button.
        /// If the user is a domain/LDAP user this parm is ignored and the button is disabled.
        /// </param>
        public void EnableChangePasswordMenuButton(bool enabled)
        {
            PafApp.GetGridApp().EnableChangePasswordButton(enabled);

        }

        /// <summary>
        /// Enable or disalbe the hyperlink button.
        /// </summary>
        /// <param name="cell">Currently selected cell.</param>
        /// <param name="buttons">Buttons to enable/disable.</param>
        public void EnableHyperlinkButton(Cell cell, List<CommandBarControl> buttons)
        {
            OlapView ov = PafApp.GetViewMngr().CurrentOlapView;
            memberTagDef tagDef;
            bool flag = false;
            if(ov.MemberTag.IsMemberTag(cell.CellAddress, out tagDef))
            {
                switch(tagDef.type)
                {
                    case memberTagType.HYPERLINK:
                        flag = true;
                        break;
                }
            }
            PafApp.GetGridApp().EnableHyperlinkButton(flag, buttons);
        }

        /// <summary>
        /// Enable or disalbe the hyperlink button.
        /// </summary>
        /// <param name="cell">Currently selected cell.</param>
        public bool EnableHyperlinkButtonRibbon(Cell cell)
        {
            OlapView ov = PafApp.GetViewMngr().CurrentOlapView;
            memberTagDef tagDef;
            bool flag = false;
            if (ov.MemberTag.IsMemberTag(cell.CellAddress, out tagDef))
            {
                switch (tagDef.type)
                {
                    case memberTagType.HYPERLINK:
                        flag = true;
                        break;
                }
            }
            return flag;
        }

        /// <summary>
        /// Enable or disalbe the hyperlink button.
        /// </summary>
        /// <param name="cell">Currently selected cell.</param>
        /// <param name="buttons">Buttons to enable/disable.</param>
        public void EnableFormulaButton(Cell cell, List<CommandBarControl> buttons)
        {
            OlapView ov = PafApp.GetViewMngr().CurrentOlapView;
            memberTagDef tagDef;
            bool flag = false;
            if (ov.MemberTag.IsMemberTag(cell.CellAddress, out tagDef))
            {
                switch (tagDef.type)
                {
                    case memberTagType.FORMULA:
                        flag = true;
                        break;
                }
            }
            PafApp.GetGridApp().EnableHyperlinkButton(flag, buttons);
        }

        /// <summary>
        /// Resest the suppress zero buttons to the default, if needed.
        /// </summary>
        /// <remarks>TTN-1277</remarks>
        public void ResetSuppressZeroButtons()
        {
            try
            {
                string viewName = PafApp.GetViewMngr().CurrentView.ViewName;
                if (!String.IsNullOrEmpty(viewName))
                {
                    PafApp.GetGridApp().ActionsPane.SetSuppressZeroSettings(viewName);
                    PafApp.GetGridApp().ActionsPane.SetViewPlannableOptions(viewName);
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        /// <summary>
        /// Enables or disables the save, commit, undo buttons and change ruleset combobox depending on the actions 
        /// available.
        /// </summary>
        public void EnableMenuandToolbarButtons()
        {
            bool areCalculationsPending = false;
            bool areAnyCalculationsPending = false;

            //Fails if the active sheet is a non-plannable worksheet.
            try
            {
                PafApp.GetGridApp().EnableTestHarnessToolbarButtons(
                    PafApp.GetPafPlanSessionResponse() != null ? true: false);

                areCalculationsPending = PafApp.GetViewMngr().CurrentProtectionMngr.CalculationsPending();
                areAnyCalculationsPending = PafApp.GetViewMngr().AreAnyCalculationsPending();

            }
            catch (ArgumentException)
            {
                areCalculationsPending = false;
                areAnyCalculationsPending = true;
            }
            catch (Exception) { }

            PafApp.GetGridApp().EnableCommandBarControls(
                areCalculationsPending,
                areAnyCalculationsPending);
        }

        /// <summary>
        /// Indicates whether or not the RuleSet ComboBox should be built
        /// </summary>
        public bool ShouldRuleSetComboBoxBeBuilt
        {
            get { return _ShouldBuildRuleSetComboBox; }
            set { _ShouldBuildRuleSetComboBox = value; }
        }

        /// <summary>
        /// Controls the display of the application toolbar, menubar, and task pane
        /// </summary>
        /// <param name="showToolBar">Shows the Application toolbar</param>
        /// <param name="showMenuBar">Shows the Application menu</param>
        /// <param name="showTaskPane">Shows the task pane</param>
        public void ShowCustomApplicationBars(bool showToolBar, bool showMenuBar, bool showTaskPane)
        {
            PafApp.GetGridApp().EnableApplicationCommandBars(showToolBar, showMenuBar);
            PafApp.GetGridApp().ShowHideTaskPane(showTaskPane);
        }


        /// <summary>
        /// Build the ruleset combo box and add it to the Palladium toolbar.
        /// </summary>
        public void BuildRuleSetComboBox()
        {
            //I couldn't find a way to detect the current "before" position so I hard coded it.
            PafApp.GetGridApp().BuildRuleSetComboBox(10);
        }

        /// <summary>
        /// Builds the actions pane and the accomping toolbars.
        /// </summary>
        public void BuildActionsPane()
        {
            PafApp.GetGridApp().BuildActionsPane();
            PafApp.GetCommandBarMngr().EnableRolesMenuandToolbarButtons(true);
            PafApp.GetCommandBarMngr().EnableOptionsMenuButton(true);
            PafApp.GetCommandBarMngr().EnableChangePasswordMenuButton(true);
            PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();
            PafApp.GetGridApp().RoleSelector.BuildCustomMenuDefs();
        }

        /// <summary>
        /// 
        /// </summary>
        public void CustomMenuHandler(string descriptionText, string viewName)
        {
            customMenuDef menuDef = PafApp.CustomMenuDefs[descriptionText];
            bool isFirstTime = true;


              

            if (MessageBox.Show(menuDef.confirmationMessage,
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question) == DialogResult.Yes)
            {

                // this menu command requires the uow be saved first.
                if (menuDef.autoSaveUow)
                {
                    PafApp.GetEventManager().SaveData(true);
                }


                List<string> parameterKeys = new List<string>();
                List<string> parameterValues = new List<string>();


                parameterKeys.Add("MENUINPUT.ActiveView");
                parameterValues.Add(viewName);

                //string[] parameterKeys = new string[] { "MENUINPUT.ActiveView", "ACTIONPARM.WEEK1" };
                //string[] parameterValues = new string[] { viewName, "foobar" };
                

                List<string> key = new List<string>();
                List<string> value = new List<string>();

                //if(_addinUtilities != null)
                //{
                //    _addinUtilities.SetMenuKey(menuDef.key);
                //    _addinUtilities.SetDimensionTrees(PafApp.GetGridApp().ActionsPane.ApiDimTrees);
                //    _addinUtilities.SimpleTrees = PafApp.GetGridApp().ActionsPane.PaceSimpleTrees;
                //    _addinUtilities.SetSimpleTrees(PafApp.GetGridApp().ActionsPane.PaceSimpleTrees);

                //    List<string> dimension;
                //    List<string> aliasTableName;
                //    PafApp.GetViewTreeView().GetAliasTableName(viewName, out dimension , out aliasTableName);
                //    _addinUtilities.AliasTableNames = aliasTableName.ToArray();
                //    _addinUtilities.AliasTableNamesDims = dimension.ToArray();

                //    List<string> prcDimension;
                //    List<string> primaryRowColumnFormat;
                //    PafApp.GetViewTreeView().GetPrimaryRowColumnFormat(viewName, out prcDimension, out primaryRowColumnFormat);
                //    _addinUtilities.PrimaryRowColumnFormat = primaryRowColumnFormat.ToArray();
                //    _addinUtilities.PrimaryRowColumnFormatDims = prcDimension.ToArray();

                //    List<string> rcDimension;
                //    List<string> rowColumnFormat;
                //    PafApp.GetViewTreeView().GetRowColumnFormat(viewName, out rcDimension, out rowColumnFormat);
                //    _addinUtilities.RowColumnFormat = rowColumnFormat.ToArray();
                //    _addinUtilities.RowColumnFormatDims = rcDimension.ToArray();


                //    foreach(string str in menuDef.customActionDefs[0].actionNamedParameters)
                //    {
                //        //if (!str.ToLower().Equals("@token")) continue;
                //        List<string> lst = str.ConverttoList("=");
                //        string[] arr = lst.ToArray();
                //        if (lst.Count >= 1 && arr[1].ToLower().Equals("@token"))
                //        {
                //            key.Add(arr[0]);
                //            value.Add(arr[1]);
                //        }
                //    }

                //    if(key.Count >= 1)
                //    {
                //        _addinUtilities.TokenKeys =  key.ToArray();
                //    }

                //    if(key.Count >= 2)
                //    {
                //        _addinUtilities.TokenValues = value.ToArray();
                //    }

                //    Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;

                //    _addinUtilities.Fire();

                //    Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;

                //    if(_addinUtilities.TokenKeys != null)
                //    {
                //        foreach(string s in _addinUtilities.TokenKeys)
                //        {
                //            parameterKeys.Add("ACTIONPARM." + s);
                //        }
                //    }

                //    if (_addinUtilities.TokenValues != null)
                //    {
                //        parameterValues.AddRange(_addinUtilities.TokenValues);
                //    }
                //}

                customCommandResult[] results = PafApp.RunCustomCommand(menuDef.key, parameterKeys.ToArray(), parameterValues.ToArray());

                if (results != null)
                {
                    StringBuilder resultMessage = new StringBuilder();
                    resultMessage.AppendLine(menuDef.terminationMessage);

                    foreach (customCommandResult result in results)
                    {
                        if (result.returnMessage != null)
                        {
                            if (isFirstTime)
                            {
                                resultMessage.AppendLine("");
                                resultMessage.AppendLine(PafApp.GetLocalization().GetResourceManager().
                                    GetString("Application.MessageBox.CustomMenuWarning"));

                                isFirstTime = false;
                            }
                            resultMessage.AppendLine(result.returnMessage);
                        }
                    }

                    MessageBox.Show(resultMessage.ToString(),
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


                // this menu command requires the users datacache to be refreshed.
                if (menuDef.autoRefreshUow)
                {
                    if (menuDef.autoRefreshVersionFilter != null)
                    {
                        PafApp.GetSaveWorkMngr().UpdateDataCache(viewName, menuDef.autoRefreshVersionFilter);
                        PafApp.GetEventManager().RefreshView(viewName, true);
                        PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(PafApp.GetViewMngr().CurrentView.HashCode);
                    }
                    else
                    {
                        //TTN-1019
                        PafApp.GetSaveWorkMngr().ReloadDataCache(viewName);
                        PafApp.GetEventManager().RefreshView(viewName, true);
                        PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(PafApp.GetViewMngr().CurrentView.HashCode);
                    }
                }

            }
        }
        #endregion Public Methods

    }
}
