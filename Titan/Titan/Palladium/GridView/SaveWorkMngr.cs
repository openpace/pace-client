#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using Titan.PafService;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// Tracks wheather or not changes have been made to the data cache.  Also supplies
    /// method to save data cache changes back to Essbase.
    /// </summary>
    internal class SaveWorkMngr
    {

        private bool dataWaiting;

        /// <summary>
        /// Constructor.
        /// </summary>
        public SaveWorkMngr()
        {
            dataWaiting = false;
        }

        /// <summary>
        /// Signals wheather or not the data cache is dirty.
        /// </summary>
        public bool DataWaitingToBeSaved
        {
            get { return dataWaiting; }
            set { dataWaiting = value; }
        }

        /// <summary>
        /// Saves work from the datacache back to Essbase.
        /// </summary>
        public void SaveWork()
        {
            
            pafCommandResponse commandReponse = null;
            Stopwatch startTime = Stopwatch.StartNew();
            try
            {
                saveWorkRequest saveWorkReq = new saveWorkRequest();
                saveWorkReq.clientId = PafApp.ClientId;
                saveWorkReq.sessionToken = PafApp.GetPafAuthResponse().securityToken.sessionToken;

                commandReponse = PafApp.GetPafService().saveWork(saveWorkReq);

                if (! commandReponse.successful)
                {
                    MethodAccessException er = new MethodAccessException();
                    throw er;
                }
                //log runtime.
                PafApp.GetLogger().InfoFormat("saveWork (svc call) runtime:  {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

                string viewName = PafApp.GetViewMngr().CurrentView.ViewName;

                Stopwatch startTime2 = Stopwatch.StartNew();

                // execute any autorun commands
                string[] parameterKeys = new string[] { "ActiveView" };
                string[] parameterValues = new string[] { viewName };

                if (PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig.autoRunOnSaveMenuItemNames != null) {
                    foreach (String menuCmdName in PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig.autoRunOnSaveMenuItemNames) {
                        if (menuCmdName != null) {
                            customCommandResult[] results = PafApp.RunCustomCommand(menuCmdName, parameterKeys, parameterValues);
                            if (results == null) break;
                        }
                    }
                }
                startTime2.Stop();
                PafApp.GetLogger().InfoFormat("saveWork (autorun command) runtime:  {0} (ms)", startTime2.ElapsedMilliseconds.ToString("N0"));

                this.dataWaiting = false;
            }
            catch (MethodAccessException ex)
            {
                PafApp.MessageBox().Show(commandReponse.message, PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"), ex.StackTrace);
                return;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return;
            }
            finally
            {
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("saveWork (total process) runtime:  {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }
        }


        /// <summary>
        /// Gets a fresh DataCache from the Essbase database.
        /// </summary>
        /// <returns>A pafDataSlice if successful, null if a failure occurs.</returns>
        public pafDataSlice UpdateDataCache(string viewName, string[] versionFilter)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            try
            {
                PafServiceProviderService pafsvc = null;

                pafUpdateDatacacheRequest updateRequest = new pafUpdateDatacacheRequest();
                updateRequest.clientId = PafApp.ClientId;
                updateRequest.sessionToken = PafApp.GetPafAuthResponse().securityToken.sessionToken;
                updateRequest.viewName = viewName;
                updateRequest.versionFilter = versionFilter;

                pafsvc = PafApp.GetPafService();

                pafDataSlice respSlice = pafsvc.updateDatacache(updateRequest);

                if (respSlice != null)
                {
                    dataWaiting = false;
                    return respSlice;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
            finally
            {
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("updateDatacache runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// Gets a fresh DataCache from the Essbase database.
        /// </summary>
        /// <returns>A pafDataSlice if successful, null if a failure occurs.</returns>
        public pafDataSlice ReloadDataCache(string viewName)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            try
            {
                PafService.PafServiceProviderService pafsvc = null;

                pafViewRequest reloadRequest = new pafViewRequest();
                reloadRequest.clientId = PafApp.ClientId;
                reloadRequest.sessionToken = PafApp.GetPafAuthResponse().securityToken.sessionToken;
                reloadRequest.viewName = viewName;

                pafsvc = PafApp.GetPafService();

                pafDataSlice respSlice = pafsvc.reloadDatacache(reloadRequest);

                if (respSlice != null)
                {
                    dataWaiting = false;
                    return respSlice;
                }
                else
                    return null;

            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
            finally
            {
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("ReloadDataCache runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }
        }
    }
}
