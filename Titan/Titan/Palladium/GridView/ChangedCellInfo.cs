#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.Pace;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.DataStructures;
using Titan.Palladium.Rules;

namespace Titan.Palladium.GridView
{
    /// <summary>
    /// Enum for type of cell change.
    /// </summary>
    /// <remarks>Do not move this enum.  It will cause serialization to break.
    /// Do not change the order of these items.
    /// </remarks>
    public enum Change
    {
        /// <summary>
        /// The user initiated the cell change.
        /// </summary>
        UserChanged,
        /// <summary>
        /// The cell was locked by the user.
        /// </summary>
        Locked,
        /// <summary>
        /// The program protected the cell.
        /// </summary>
        Protected,
        /// <summary>
        /// The user entered this cell using the PasteShape feature.
        /// </summary>
        PasteShape,
        /// <summary>
        /// The user selects to replicate all intersections on the server
        /// </summary>
        ReplicateAll,
        /// <summary>
        /// The user selects to replicate only existing intersections on the server.
        /// </summary>
        ReplicateExisting,
        /// <summary>
        /// The user selects to list all intersections on the server
        /// </summary>
        LiftAll,
        /// <summary>
        /// The user selects to lift only existing intersections on the server.
        /// </summary>
        LiftExisting,
        /// <summary>
        /// Session lock(s) created by the user.
        /// </summary>
        SessionLocked
    }
    /// <summary>
    /// Class to hold information about changed cells.
    /// </summary>
    [Serializable]
    public class ChangedCellInfo : ICloneable, IEquatable<ChangedCellInfo>
    {
        /// <summary>
        /// Private variable to hold the sheet's hash code.
        /// </summary>
        [NonSerialized]
        private string _Sheet;

        /// <summary>
        /// Private variable to hold the actual sheet name.
        /// </summary>
        private string _SheetName;

        /// <summary>
        /// Private variable to hold the cell address.
        /// </summary>
        private CellAddress _CellAddress;

        /// <summary>
        /// Holds a range of cell addresses.
        /// </summary>
        private ContiguousRange _CellAddressRange;

        /// <summary>
        /// Private variable to hold the cell style.
        /// </summary>
        [NonSerialized]
        private Format _CellFormat;

        /// <summary>
        /// Private variable to hold the cell value.
        /// </summary>
        private Object _CellValue;

        /// <summary>
        /// Private variable identifying the origin of the change
        /// </summary>
        private Change _ChangeType;

        /// <summary>
        /// The intersection of the changed Cell
        /// </summary>
        private Intersection _Intersection;

        /// <summary>
        /// Private variable to hold the protection manager lists.
        /// </summary>
        private ProtectionMngrListTracker _ProtMngrListTracker;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangedCellInfo()
        {
        }

        /// <summary>
        /// Overloaded constructor that takes class properties as parameters.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides(hash code).</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="cellAddress">Address of the cell.</param>
        /// <param name="cellValue">Value of the cell.</param>
        /// <param name="changeType">The type of change that occured.</param>
        public ChangedCellInfo(string sheet, string sheetName, CellAddress cellAddress, Object cellValue, Change changeType)
        {
            try
            {
                _Sheet = sheet;
                _SheetName = sheetName;
                _CellAddress = cellAddress;
                _CellValue = cellValue;
                _ChangeType = changeType;
                _Intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cellAddress);

                GetCellFormat();
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Overloaded constructor that takes class properties as parameters.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides(hash code).</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="cellAddress">Address of the cell.</param>
        /// <param name="intersection">Intersection of the changed cell.</param>
        /// <param name="cellValue">Value of the cell.</param>
        /// <param name="changeType">The type of change that occured.</param>
        /// <param name="format">Format object for the cell.</param>
        public ChangedCellInfo(string sheet, string sheetName, CellAddress cellAddress, Intersection intersection, Object cellValue, Change changeType, Format format)
        {

            try
            {
                _Sheet = sheet;
                _SheetName = sheetName;
                _CellAddress = cellAddress;
                _CellValue = cellValue;
                _ChangeType = changeType;
                _Intersection = intersection;

                _CellFormat = format;
            }
            catch (Exception)
            { }

        }

        /// <summary>
        /// Overloaded constructor that takes class properties as parameters.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides(hash code).</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="cellAddress">Address of the cell.</param>
        /// <param name="cellValue">Value of the cell.</param>
        /// <param name="changeType">The type of change that occured.</param>
        /// <param name="protectedCells">The list of cells that are protected on the sheet after the change has been applied.</param>
        /// <param name="changes">The list of cell that have been changed on the sheet after the change has been applied.</param>
        /// <param name="userLocks">The list of user locks.</param>
        /// <param name="unlockedChanges">The list of unlocked changed.</param>
        /// <param name="replicateAllCells">The list of replicate all cells.</param>
        /// <param name="replicateExistingCells">The list of replicate existing cells.</param>
        /// <param name="liftAllCells">List of lift existing cells to pass to the mid tier.</param>
        /// <param name="liftExistingCells">List of lift all cells to pass to the mid tier</param>
        public ChangedCellInfo(string sheet, string sheetName, CellAddress cellAddress, Object cellValue,
            Change changeType, List<Intersection> protectedCells, List<Intersection> changes,
            List<Intersection> userLocks, List<Intersection> unlockedChanges,
            List<Intersection> replicateAllCells, List<Intersection> replicateExistingCells,
            List<Intersection> liftAllCells, List<Intersection> liftExistingCells)
        {
            try
            {
                _Sheet = sheet;
                _SheetName = sheetName;
                _CellAddress = cellAddress;
                _CellValue = cellValue;
                _ChangeType = changeType;
                _Intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cellAddress);
                _ProtMngrListTracker = new ProtectionMngrListTracker(
                    protectedCells, changes, userLocks, unlockedChanges, replicateAllCells, replicateExistingCells, liftAllCells, liftExistingCells, null);

                GetCellFormat();
            }
            catch (Exception)
            { }
        }


        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides(hash code).</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="changeType">The type of change that occured.</param>
        /// <param name="protectedCells">The list of cells that are protected on the sheet after the change has been applied.</param>
        /// <param name="changes">The list of cell that have been changed on the sheet after the change has been applied.</param>
        /// <param name="userLocks">The list of user locks.</param>
        /// <param name="unlockedChanges">The list of unlocked changed.</param>
        /// <param name="replicateAllCells">The list of replicate all cells.</param>
        /// <param name="replicateExistingCells">The list of replicate existing cells.</param>
        /// <param name="liftAllCells">List of lift existing cells to pass to the mid tier.</param>
        /// <param name="liftExistingCells">List of lift all cells to pass to the mid tier</param>
        /// <param name="sessionLocks">Session locked intersections.</param>
        public ChangedCellInfo(string sheet, string sheetName, Change changeType, List<Intersection> protectedCells,
            List<Intersection> changes, List<Intersection> userLocks, List<Intersection> unlockedChanges,
            List<Intersection> replicateAllCells, List<Intersection> replicateExistingCells,
            List<Intersection> liftAllCells, List<Intersection> liftExistingCells, List<Intersection> sessionLocks)
        {
            try
            {
                _Sheet = sheet;
                _SheetName = sheetName;
                _ChangeType = changeType;
                SessionLocks = sessionLocks;
                _ProtMngrListTracker = new ProtectionMngrListTracker(
                    protectedCells, changes, userLocks, unlockedChanges, replicateAllCells, replicateExistingCells, liftAllCells, liftExistingCells, sessionLocks);

                GetCellFormat();
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides(hash code).</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="cellAddresRange">Dictionary of cell addresses, where the value is the value in the dictionary.</param>
        /// <param name="cellValues">A two dimensional array holding the value of the cells in cellAddressRange.</param>
        /// <param name="changeType">The type of change that occured.</param>
        /// <param name="protectedCells">The list of cells that are protected on the sheet after the change has been applied.</param>
        /// <param name="changes">The list of cell that have been changed on the sheet after the change has been applied.</param>
        /// <param name="userLocks">The list of user locks.</param>
        /// <param name="unlockedChanges">The list of unlocked changed.</param>
        /// <param name="replicateAllCells">The list of replicate all cells.</param>
        /// <param name="replicateExistingCells">The list of replicate existing cells.</param>
        /// <param name="liftAllCells">List of lift existing cells to pass to the mid tier.</param>
        /// <param name="liftExistingCells">List of lift all cells to pass to the mid tier</param>
        public ChangedCellInfo(string sheet, string sheetName, ContiguousRange cellAddresRange, Object cellValues,
            Change changeType, List<Intersection> protectedCells, List<Intersection> changes,
            List<Intersection> userLocks, List<Intersection> unlockedChanges,
            List<Intersection> replicateAllCells, List<Intersection> replicateExistingCells,
            List<Intersection> liftAllCells, List<Intersection> liftExistingCells)
        {
            try
            {
                _Sheet = sheet;
                _SheetName = sheetName;
                _CellAddressRange = cellAddresRange;
                _CellValue = cellValues;
                _ChangeType = changeType;
                _ProtMngrListTracker = new ProtectionMngrListTracker(
                    protectedCells, changes, userLocks, unlockedChanges, replicateAllCells, replicateExistingCells, liftAllCells, liftExistingCells, null);

                GetCellFormat();
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Overloaded constructor that takes class properties as parameters.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides.</param>
        /// <param name="cellAddresRange">Address of the cell.</param>
        /// <param name="changeType">The type of cell change.</param>
        public ChangedCellInfo(string sheet, ContiguousRange cellAddresRange, Change changeType)
        {
            _Sheet = sheet;
            _SheetName = "";
            _CellAddressRange = cellAddresRange;
            _CellValue = 0;
            _ChangeType = changeType;

            GetCellFormat();
        }

        /// <summary>
        /// Overloaded constructor that takes class properties as parameters.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides.</param>
        /// <param name="cellAddress">Address of the cell.</param>
        /// <param name="changeType">The type of cell change.</param>
        public ChangedCellInfo(string sheet, CellAddress cellAddress, Change changeType)
        {
            _Sheet = sheet;
            _SheetName = "";
            _CellAddress = cellAddress;
            _CellValue = 0;
            _ChangeType = changeType;
            _Intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cellAddress);

            GetCellFormat();
        }


        /// <summary>
        /// Overloaded constructor that takes class properties as parameters.
        /// </summary>
        /// <param name="sheet">Sheet where the cell resides.</param>
        /// <param name="cellAddress">Address of the cell.</param>
        /// <param name="changeType">The type of cell change.</param>
        /// <param name="format">Format object for the cell.</param>
        public ChangedCellInfo(string sheet, CellAddress cellAddress, Change changeType, Format format)
        {
            _Sheet = sheet;
            _SheetName = "";
            _CellAddress = cellAddress;
            _CellValue = 0;
            _ChangeType = changeType;
            _Intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cellAddress);

            _CellFormat = format;
        }

        /// <summary>
        /// Returns a reference to this instance of ChangedCellInfo.
        /// </summary>
        /// <returns>Returns a reference to this instance of ChangedCellInfo.</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Identifies what originated the change
        /// </summary>
        public Change ChangeType
        {
            get { return _ChangeType; }
            set { _ChangeType = value; }
        }

        /// <summary>
        /// Intersection of the changed cell
        /// </summary>
        public Intersection Intersection
        {
            get { return _Intersection; }
            set { _Intersection = value; }
        }

        /// <summary>
        /// Hash code of the sheet where the cell resides.
        /// </summary>
        public string Sheet
        {
            get { return _Sheet; }
            set { _Sheet = value; }
        }

        /// <summary>
        /// Name of the sheet where the cell resides.
        /// </summary>
        public string SheetName
        {
            get { return _SheetName; }
            set { _SheetName = value; }
        }

        /// <summary>
        /// Address of the cell.
        /// </summary>
        public CellAddress CellAddress
        {
            get { return _CellAddress; }
            set { _CellAddress = value; }
        }
        
        /// <summary>
        /// The Excel.Style of the cell.
        /// </summary>
        public Format CellFormat
        {
            get { return _CellFormat; }
            set { _CellFormat = value; }
        }
        
        /// <summary>
        /// Value of the cell.
        /// </summary>
        public Object CellValue
        {
            get { return _CellValue; }
            set { _CellValue = value; }
        }

        /// <summary>
        /// Parent level session locks.
        /// </summary>
        public List<Intersection> SessionLocks { get; set; }

        /// <summary>
        /// Get/set the protection manager lists.
        /// </summary>
        public ProtectionMngrListTracker ProtMngrListTracker
        {
            get 
            {
                if (_ProtMngrListTracker != null)
                    return _ProtMngrListTracker;
                else
                    return null;
            }
            set { _ProtMngrListTracker = value; }
        }

        /// <summary>
        /// Get/set a dictionary of cell addresses, where the value is the value in the dictionary.
        /// </summary>
        public ContiguousRange CellAdressRange
        {
            get
            {
                if (_CellAddressRange != null)
                    return _CellAddressRange;
                else
                    return null;
            }
            set { _CellAddressRange = value; }
        }

        /// <summary>
        /// Implementation of "IEquatable".  
        /// </summary>
        /// <remarks>
        /// Without this implementation, when you use ChangedCellInfo as a key in a hash structure,
        /// the comparsion would always return false because of the way the default comparsion works.
        /// </remarks>
        /// <param name="changedCell">The cell that you want to compare.</param>
        /// <returns>True if the objects are equal, false if they are not.</returns>
        public bool Equals(ChangedCellInfo changedCell)
        {
            if (changedCell.Sheet.Equals(_Sheet) &&
                changedCell.CellAddress.Row == _CellAddress.Row &&
                changedCell.CellAddress.Col == _CellAddress.Col && 
                changedCell.CellFormat.ToString().Equals(_CellFormat.ToString()) &&
                changedCell.CellValue.ToString().Equals(_CellValue.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the hash code for the object.
        /// </summary>
        /// <returns>Returns the hash code for the object.</returns>
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }

        /// <summary>
        /// Returns a System.String representation of the CellAddress.
        /// </summary>
        /// <returns>Returns a System.String representation of the ChangedCellInfo.</returns>
        public override string ToString()
        {
            return "(" + _Sheet + "," + _CellAddress.Row + "," + CellAddress.Col + "," + 
                _CellFormat.ToString() + "," + _CellValue.ToString() + ")";
        }

        private void GetCellFormat()
        {
            _CellFormat = new Format();

            string bgFillColor = string.Empty;
            string unlocked = string.Empty;
            string numericFormat = string.Empty;

            if (_CellAddress != null)
            {
                PafApp.GetViewMngr().CurrentGrid.GetCellFormatInformation(_CellAddress.Row, _CellAddress.Col, ref bgFillColor, ref unlocked, ref numericFormat);
            }
            else
            {
                PafApp.GetViewMngr().CurrentGrid.GetCellFormatInformation(_CellAddressRange.TopLeft.Row, _CellAddressRange.TopLeft.Col, ref bgFillColor, ref unlocked, ref numericFormat);
            }

            _CellFormat.BgHexFillColor = bgFillColor;
            _CellFormat.Plannable = unlocked;
            _CellFormat.NumberFormat = numericFormat;

            //old code

            ////Create the format object to be passed to the change manager.
            ////_CellFormat.Alignment = PafApp.GetViewMngr().GetGrid(_Sheet).GetHorizontalAlignment(_CellAddress.Row, _CellAddress.Col);
            //_CellFormat.BgHexFillColor = PafApp.GetViewMngr().CurrentGrid.GetBgFillColor(_CellAddress.Row, _CellAddress.Col);
            ////_CellFormat.Bold = PafApp.GetViewMngr().GetGrid(_Sheet).IsBold(_CellAddress.Row, _CellAddress.Col);
            ////_CellFormat.FontColor = PafApp.GetViewMngr().GetGrid(_Sheet).GetFontColor(_CellAddress.Row, _CellAddress.Col);
            ////_CellFormat.FontName = PafApp.GetViewMngr().GetGrid(_Sheet).GetFontName(_CellAddress.Row, _CellAddress.Col);
            ////_CellFormat.Italics = PafApp.GetViewMngr().GetGrid(_Sheet).IsItalicised(_CellAddress.Row, _CellAddress.Col);
            ////_CellFormat.Size = PafApp.GetViewMngr().GetGrid(_Sheet).GetFontSize(_CellAddress.Row, _CellAddress.Col);
            ////_CellFormat.StrikeOut = PafApp.GetViewMngr().GetGrid(_Sheet).isStrikedOut(_CellAddress.Row, _CellAddress.Col);
            ////_CellFormat.UnderLine = PafApp.GetViewMngr().GetGrid(_Sheet).isUnderlined(_CellAddress.Row, _CellAddress.Col);
            //_CellFormat.Plannable = PafApp.GetViewMngr().CurrentGrid.isUnLocked(_CellAddress.Row, _CellAddress.Col);
            //_CellFormat.NumberFormat = PafApp.GetViewMngr().CurrentGrid.GetNumericFormat(_CellAddress.Row, _CellAddress.Col);
        }
    }
}