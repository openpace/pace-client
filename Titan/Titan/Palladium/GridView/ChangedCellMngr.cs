#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.Pace.Application.Controls;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// 
    /// </summary>
    internal class ChangedCellMngr
    {
        /// <summary>
        /// We have a dictionary to track all the sheets, so that is our key for the dictionary.
        /// The value associated to the sheet is another dictionary of type, 
        /// Dictionary(CellAddress, Stack(ChangedCellInfo)).  This allows us to have the addres as the key
        /// of this dictionary(for fast access to members), and a list of changed cells.  
        /// See remarks as to why we have a list of ChangedCellInfo.
        /// </summary>
        /// <remarks>
        /// Using a list in this structure so that in the future, 
        /// we can track multiple changes to a cell.  This would allow us to have undo functionality at
        /// a cell level.
        /// </remarks>
        private Dictionary<string, SortedDictionary<CellAddress, Stack<ChangedCellInfo>>> _SheetCellTracker;
        private List<string> _DefaultBgColors;
        private ColorUtilities cu;

        #region Constructors

        /// <summary>
        /// Default constructor.
        /// </summary>
        public ChangedCellMngr()
        {
            _SheetCellTracker = new Dictionary<string, SortedDictionary<CellAddress, Stack<ChangedCellInfo>>>();
            _DefaultBgColors = new List<string>();
            cu = new ColorUtilities();
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="sheetName">The unique sheet to add to the dictionary.</param>
        public ChangedCellMngr(string sheetName)
        {
            _SheetCellTracker = new Dictionary<string, SortedDictionary<CellAddress, Stack<ChangedCellInfo>>>();
            AddSheet(sheetName);
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="sheetNames">The unique array of sheets to add to the dictionary.</param>
        public ChangedCellMngr(string[] sheetNames)
        {
            _SheetCellTracker = new Dictionary<string, SortedDictionary<CellAddress, Stack<ChangedCellInfo>>>();

            foreach (string str in sheetNames)
                AddSheet(str);
        }

        #endregion Constructors

        #region Private Methods

        /// <summary>
        /// Load/reload the default color list.
        /// </summary>
        public void ReloadDefaultColors()
        {
            _DefaultBgColors.Clear();
            _DefaultBgColors.Add(cu.ColorToHexString(PafApp.GetPafAppSettingsHelper().GetForwardPlannableProtectedColor()));
            _DefaultBgColors.Add(cu.ColorToHexString(PafApp.GetPafAppSettingsHelper().GetNonPlannableProtectedColor()));
            _DefaultBgColors.Add(cu.ColorToHexString(PafApp.GetPafAppSettingsHelper().GetProtectedColor()));
            _DefaultBgColors.Add(cu.ColorToHexString(PafApp.GetPafAppSettingsHelper().GetSystemLockColor()));
            _DefaultBgColors.Add(cu.ColorToHexString(PafApp.GetPafAppSettingsHelper().GetUserLockColor()));
        }

        /// <summary>
        /// Add a changed cell to a sheet dictionary.
        /// </summary>
        /// <param name="cell">The changed cell to add.</param>
        /// <param name="sheetDct">The sheet dictionary to add the cell to.</param>
        private void AddCell(ChangedCellInfo cell,
            SortedDictionary<CellAddress, Stack<ChangedCellInfo>> sheetDct)
        {
            //cell does not exist, log it.
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("User Changed Cell: {0}!R[{1}]C[{2}] from initial value to: {3}",
                new string[] { cell.Sheet, cell.CellAddress.Row.ToString(), 
                    cell.CellAddress.Col.ToString(), cell.CellValue.ToString() });

            //Add the new CellAddress, Stack combo, to the dictionary.
            if (! sheetDct.ContainsKey(cell.CellAddress))
                sheetDct.Add(cell.CellAddress, new Stack<ChangedCellInfo>());

            //Add the object to the stack.
            sheetDct[cell.CellAddress].Push((ChangedCellInfo)cell.Clone());
        }

        /// <summary>
        /// Gets a dictionary with the formats as the key of the dictionary, the value of the \
        /// dictionary is a empty list.
        /// </summary>
        /// <param name="sheetDictionary">The sheet dictionary to extract the formats from.</param>
        /// <returns>A dictionary with the formats as the key, and a blank list of ranges.</returns>
        private Dictionary<Format, List<Range>> CreateDictionaryOfFormats(
            SortedDictionary<CellAddress, Stack<ChangedCellInfo>> sheetDictionary)
        {
            try
            {
                Dictionary<Format, List<Range>> formats = new Dictionary<Format, List<Range>>();

                foreach (KeyValuePair<CellAddress, Stack<ChangedCellInfo>> kvp in sheetDictionary)
                {
                    Format fmt = kvp.Value.Peek().CellFormat;

                    //checks the format to see if the bgfill color matches one of our "colors"
                    //if so it replaces it with the original 
                    GetOriginalCellFormat(kvp.Key, fmt);

                    if (!formats.ContainsKey(fmt))
                    {
                        formats.Add(fmt, new List<Range>());
                    }
                }
                return formats;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the origianl cell format.
        /// </summary>
        /// <param name="ca">Cells address.</param>
        /// <param name="format">Format to copy the origianl format into.</param>
        private void GetOriginalCellFormat(CellAddress ca, Format format)
        {
            //do a null check, and make sure the current bgfill color is a protected color.
            if (!String.IsNullOrEmpty(format.BgHexFillColor.ToLower()) && _DefaultBgColors.Contains(format.BgHexFillColor.ToLower()))
            {
                //get the origianl server format, from the format manager.
                Format tempFormat = PafApp.GetFormattingMngr().GetDefaultCellFormat(ca);
                if (tempFormat != null)
                {
                    format.CopyAll(tempFormat);
                }
            }
        }

        #endregion Private Methods

        #region Public Methods

        /// <summary>
        /// Get/set the SheetCellTracker dictinary.
        /// </summary>
        public Dictionary<string, SortedDictionary<CellAddress, Stack<ChangedCellInfo>>> SheetCellTracker
        {
            get { return _SheetCellTracker; }

            set { _SheetCellTracker = value; }
        }

        /// <summary>
        /// Add a new unique sheet id to the dictionary.
        /// </summary>
        /// <param name="sheetId">The id of the sheet to add.</param>
        public void AddSheet(string sheetId)
        {
            if (_SheetCellTracker != null)
            {
                if (!_SheetCellTracker.ContainsKey(sheetId))
                    _SheetCellTracker.Add(sheetId, new SortedDictionary<CellAddress, Stack<ChangedCellInfo>>());
            }
        }

        /// <summary>
        /// Add a changed cell to the dictionary tracking structure.
        /// </summary>
        /// <param name="sheetId">The name of the sheet to retrieve the changed cell information.</param>
        /// <param name="cell">The cell to add to the sheet dictionary.</param>
        public void AddCell(string sheetId, ChangedCellInfo cell)
        {
            if (!String.IsNullOrEmpty(sheetId))
            {
                if (_SheetCellTracker.ContainsKey(sheetId))
                {
                    //The dictionary exists, so we just have to add the member.

                    //Get inner dictionary.
                    SortedDictionary<CellAddress, Stack<ChangedCellInfo>> dct =
                        _SheetCellTracker[sheetId];

                    //See if the cell exists
                    if (dct.ContainsKey(cell.CellAddress))
                    {
                        //cell already exists, so change it.
                        ChangeCell(cell, dct, true);
                    }
                    else
                    {
                        //cell does not exist, so add it.
                        AddCell(cell, dct);
                    }
                }
                else
                {
                    //The dictionary does not exist, so we have to create one.

                    //Add the sheet to the SheetTracker
                    AddSheet(sheetId);

                    //Get inner dictionary.
                    SortedDictionary<CellAddress, Stack<ChangedCellInfo>> dct =
                        _SheetCellTracker[sheetId];

                    //cell does not exist, so add it.
                    AddCell(cell, dct);
                }
            }
        }

        /// <summary>
        /// Add a cell to the stack, when the celladdress already exists in the dictionary..
        /// </summary>
        /// <param name="newCell">The new cell to add.</param>
        /// <param name="sheetDct">The sheet dictionary to add the cells to.</param>
        /// <param name="copyLastCellStyle">Copy the style from the cell at the top of the stack
        /// to the new cell.</param>
        public void ChangeCell(ChangedCellInfo newCell, SortedDictionary<CellAddress, 
            Stack<ChangedCellInfo>> sheetDct, bool copyLastCellStyle)
        {
            //cell already exists.
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("User Changed Cell: {0}!R[{1}]C[{2}] - From value: " + 
                "{3}, to value: {4}", new string[] {newCell.Sheet, newCell.CellAddress.Row.ToString(), 
                    newCell.CellAddress.Col.ToString(), sheetDct[newCell.CellAddress].Peek().CellValue.ToString(),
                    newCell.CellValue.ToString()});

            //Get the old style
            if(copyLastCellStyle)
                newCell.CellFormat = sheetDct[newCell.CellAddress].Peek().CellFormat;

            //Add the cell to the dictionary.
            AddCell(newCell, sheetDct);
        }

        /// <summary>
        /// Get a dictionary of formats and ranges for a particular sheet id.
        /// </summary>
        /// <param name="sheetId">The name of the sheet to retrieve the changed cell information.</param>
        /// <returns>A dictionary with the format as the key and the value is a list of ranges.</returns>
        public Dictionary<Format, List<Range>> GetChangedCells(string sheetId)
        {
            if (!String.IsNullOrEmpty(sheetId))
            {
                if (_SheetCellTracker.ContainsKey(sheetId))
                {
                    //Get inner dictionary.
                    SortedDictionary<CellAddress, Stack<ChangedCellInfo>> dct =
                        _SheetCellTracker[sheetId];

                    //Get a dictionary of unique formats.
                    Dictionary<Format, List<Range>> formats =
                        CreateDictionaryOfFormats(dct);

                    if (formats != null)
                    {
                        foreach (KeyValuePair<CellAddress, Stack<ChangedCellInfo>> kvp in dct)
                        {
                            List<CellAddress> cellAddresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(kvp.Value.Peek().Intersection);

                            if (cellAddresses != null)
                            {
                                foreach (CellAddress ca in cellAddresses)
                                {
                                    //grabs the top format off the top of the stack, and associates it
                                    //with the cell address.
                                    formats[kvp.Value.Peek().CellFormat].Add(new Range(ca));
                                }
                            }
                        }
                    }
                    return formats;
                }
            }
            return null;
        }


        

        /// <summary>
        /// Removes all elements from the ChangedCellMngr structure.
        /// </summary>
        public void Clear() 
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Cleared changed cell mngr.");
            _SheetCellTracker.Clear();
        }

        /// <summary>
        /// Removes all elements from the ChangedCellMngr structure for a particular sheet.
        /// </summary>
        /// <param name="sheetName">The name of the sheet.</param>
        public void ClearChangedCells(string sheetName)
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Cleared changed cell mngr for sheet: " + sheetName);
            if(_SheetCellTracker.ContainsKey(sheetName))
                _SheetCellTracker.Remove(sheetName);
        }


        #endregion Public Methods
    }

}