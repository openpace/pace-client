#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Forms;
using Titan.Pace.Base.Data;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Palladium.GridView.MemberTag;
using Titan.Properties;
using Tuple = Titan.Pace.DataStructures.Tuple;

namespace Titan.Pace.ExcelGridView
{   
    /// <summary>
	/// Summary description for OlapView.
	/// </summary>
    internal class OlapView
	{
        private const int HEADER_START_ROW = 1;
        private Dictionary<string, List<string>> _rowDims;
        private Dictionary<string, List<int>> _rowDimHash;
        private Dictionary<string, List<string>> _colDims;
        private Dictionary<string, List<int>> _colDimHash;
        private Dictionary<string, string> _pageDims;
        private List<string> _headers;
        private Dictionary<string, string> _viewMembers;
        private IGridInterface _grid;
        private ContiguousRange _dataRange;
        private ContiguousRange _rowRange;
        private ContiguousRange _colRange;
        private ContiguousRange _pageRange;
        private ContiguousRange _headerRange;
        
        private pafView _pafView;
        private HashSet<int> _rowBlanks;
        private HashSet<int> _colBlanks;
        private Dictionary<string, List<Range>> _viewHeaderStyles;
        private Dictionary<TupleFormat, List<Tuple>> _headerStyles;
        private Dictionary<TupleFormat, List<Tuple>> _dataColStyles;
        private Dictionary<TupleFormat, List<Tuple>> _dataRowStyles;
        private Dictionary<TupleConditionalFormat, List<Tuple>> _dataColCondStyles;
        private Dictionary<TupleConditionalFormat, List<Tuple>> _dataRowCondStyles;
        private Dictionary<int, bool> _rowParentFirst;
        private Dictionary<int, bool> _colParentFirst;
        private Dictionary<string, List<Tuple>> _numericRowFormats ;
        private Dictionary<string, List<Tuple>> _numericColFormats ;
        private List<PafSimpleCoordList> _inValidReplicationRange;
        private Dictionary<ViewAxis, List<ContiguousRange>> _mergedCellRanges;
        private Dictionary<float, List<int>> _rowHeights;
        private Dictionary<float, List<int>> _colWidths;
        private Dictionary<int, int?[]> _rowSymetricTupleGroups;
        private Dictionary<int, int?[]> _colSymetricTupleGroups;
        private List<SortSelection> _sortSelections;
        private List<SortBy> _sortByList;
        private List<int> _sortPositions;
        private List<int> _currentSortOrder;
        private Range _sortRange;
        private MemberTags _memberTag;
        private CellAddress _freezePaneCell;
        private readonly HashSet<int> _invalidExcelGridNumbers = new HashSet<int>();

        private readonly Dictionary<Intersection, List<CellAddress>> _intersectionAddressHash = new Dictionary<Intersection, List<CellAddress>>();
        private readonly Dictionary<CellAddress, Intersection> _addressIntersectionHash = new Dictionary<CellAddress, Intersection>();

       

        /// <summary>
        /// Interface to the Grid API
        /// </summary>
        internal IGridInterface Grid
        {
            [DebuggerHidden]
            get { return _grid; }
            set { _grid = value; }
        }

        public SimpleDimTrees SimpleDimTrees
        {
            get { return PafApp.SimpleDimTrees; }
        }

        /// <summary>
        /// The Upper leftmost cell on the View
        /// </summary>
        public CellAddress StartCell { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public ViewAxis PrimaryFormattingAxis { get; set; }

        /// <summary>
        /// Row grouping specification.
        /// </summary>
        public GroupingSpec RowGroupSpec { get; set; }

        /// <summary>
        /// Column grouping specification.
        /// </summary>
        public GroupingSpec ColGroupSpec { get; set; }

        public string ConditionalFormat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> ViewMembers
        {
            get { return _viewMembers ?? (_viewMembers = new Dictionary<string, string>()); }
        }

        /// <summary>
        /// Dictionary of the row dimensions.
        /// </summary>
        public Dictionary<string, List<string>> RowDims
        {
            get { return _rowDims ?? (_rowDims = new Dictionary<string, List<string>>()); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<int>> RowDimHash
        {
            get { return _rowDimHash ?? (_rowDimHash = new Dictionary<string, List<int>>()); }
        }

        /// <summary>
        /// Dictionary of the column dimensions.
        /// </summary>
        public Dictionary<string, List<string>> ColDims
        {
            get { return _colDims ?? (_colDims = new Dictionary<string, List<string>>()); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<int>> ColDimHash
        {
            get { return _colDimHash ?? (_colDimHash = new Dictionary<string, List<int>>()); }
        }

        /// <summary>
        /// Dictionary of the page dimensions.
        /// </summary>
        public Dictionary<string, string> PageDims
        {
            get { return _pageDims ?? (_pageDims = new Dictionary<string, string>()); }
        }

        /// <summary>
        /// List of the row headers.
        /// </summary>
        public List<string> Headers
        {
            get { return _headers ?? (_headers = new List<string>()); }
        }

        /// <summary>
        /// List of row blanks.
        /// </summary>
        public HashSet<int> RowBlanks
        {
            get { return _rowBlanks ?? (_rowBlanks = new HashSet<int>()); }

            set { _rowBlanks = value; }
        }

        /// <summary>
        /// List of column blanks.
        /// </summary>
        public HashSet<int> ColBlanks
        {
            get { return _colBlanks ?? (_colBlanks = new HashSet<int>()); }

            set { _colBlanks = value; }
        }

        /// <summary>
        /// The range of the sheet where the data is located.
        /// </summary>
        public ContiguousRange DataRange
        {
            get
            {
                if (_dataRange == null)
                {
                    if (_rowDims.Count > 0 && _colDims.Count > 0)
                    {
                        CellAddress topLeft = new CellAddress(RowOffset, ColOffset);

                        List<string> colMbrList = _colDims[GetFirstColDimension()];
                        List<string> rowMbrList = _rowDims[GetFirstRowDimension()];

                        //modified for aliases
                        int curRow = RowOffset + rowMbrList.Count - 1;
                        
                        //modified for aliases
                        int curCol = ColOffset + colMbrList.Count - 1;
                        CellAddress bottomRight = new CellAddress(curRow, curCol);

                        _dataRange = new ContiguousRange(topLeft, bottomRight);
                    }
                }   
                return _dataRange;
            }
        }
       
        /// <summary>
        /// The range of the sheet where the row headers are located.
        /// </summary>
        public ContiguousRange RowRange
        {
            get
            {
                if (_rowRange == null)
                {
                    List<string> rowMbrList;

                    //modified for aliases;
                    int curRow = RowOffset;
                    int curCol = StartCell.Col;

                    CellAddress topLeft = new CellAddress(curRow, curCol);

                    string firstRowDim = GetFirstRowDimension();
                    if (!String.IsNullOrEmpty(firstRowDim))
                    {
                        rowMbrList = _rowDims[firstRowDim];
                    }
                    else
                    {
                        rowMbrList = new List<string>();
                    }
                    //RowMbrList = _RowDims[GetFirstRowDimension()];

                    //modified for aliases, and suppress zeros
                    if (rowMbrList != null && rowMbrList.Count == 0)
                    {
                        curRow = RowOffset + rowMbrList.Count;
                    }
                    else if (rowMbrList != null && rowMbrList.Count > 0)
                    {
                        curRow = RowOffset + rowMbrList.Count - 1;
                    }
                    //CurRow = StartCell.Row + _ColDims.Count + RowMbrList.Count - 1 + _ColAliases;
                    
                    //modified for row aliases, and suppress zeros
                    if (_rowDims != null && _rowDims.Count == 0)
                    {
                        curCol = ColOffset;
                    }
                    else if (_rowDims != null && _rowDims.Count > 0)
                    {
                        curCol = ColOffset - 1;
                    }
                    
                    CellAddress bottomRight = new CellAddress(curRow, curCol);

                    _rowRange = new ContiguousRange(topLeft, bottomRight);
                }
                return _rowRange;
            }
        }

        /// <summary>
        /// The range of the sheet where the row headers are located.
        /// </summary>
        public ContiguousRange ColRange
        {
            get
            {
                if (_colRange == null)
                {
                    List<string> colMbrList;

                    int curRow = StartCell.Row;

                    //modified for aliases
                    int curCol = ColOffset;
                    CellAddress topLeft = new CellAddress(curRow, curCol);

                    string firstColDim = GetFirstColDimension();
                    if (!String.IsNullOrEmpty(firstColDim))
                    {
                        colMbrList = _colDims[firstColDim];
                    }
                    else
                    {
                        colMbrList = new List<string>();
                    }
                    //ColMbrList = _ColDims[GetFirstColDimension()];


                    //modified for aliases, and suppress zeros
                    if (colMbrList != null && colMbrList.Count == 0)
                    {
                        curCol = ColOffset + colMbrList.Count;
                    }
                    else if (colMbrList != null && colMbrList.Count > 0)
                    {
                        curCol = ColOffset + colMbrList.Count - 1;
                    }
                    //CurCol = StartCell.Col + _RowDims.Count + ColMbrList.Count - 1 + _RowAliases;

                    //modified for aliases, and suppress zeros
                    if (_colDims != null && _colDims.Count == 0)
                    {
                        curRow = RowOffset;
                    }
                    else if (_colDims != null && _colDims.Count > 0)
                    {
                        curRow = RowOffset - 1;
                    }

                    CellAddress bottomRight = new CellAddress(curRow, curCol);

                    _colRange = new ContiguousRange(topLeft, bottomRight);
                }
                return _colRange;
            }
        }

        /// <summary>
        /// The range of the sheet where the page members are located.
        /// </summary>
        public ContiguousRange PageRange
        {
            get
            {
                if (_pageRange == null)
                {
                    int curRow = StartCell.Row - 1;
                    //modified for aliases
                    int curCol = ColOffset;
                    CellAddress topLeft = new CellAddress(curRow, curCol);

                    curRow = StartCell.Row - 1;
                    //modified for aliases
                    curCol = ColOffset +_pageDims.Count - 1;
                    CellAddress bottomRight = new CellAddress(curRow, curCol);

                    _pageRange = new ContiguousRange(topLeft, bottomRight);
                }
                return _pageRange;
            }
        }

        /// <summary>
        /// The range of the sheet where the headers are located.
        /// </summary>
        public ContiguousRange HeaderRange
        {
            get
            {
                if (_headerRange == null)
                {
                    int curRow = HEADER_START_ROW;
                    int curCol = StartCell.Col;
                    CellAddress topLeft = new CellAddress(curRow, curCol);

                    curRow = Headers.Count;
                    curCol = StartCell.Col;
                    CellAddress bottomRight = new CellAddress(curRow, curCol);

                    _headerRange = new ContiguousRange(topLeft, bottomRight);
                }
                return _headerRange;
            }
        }

        /// <summary>
        /// Number of dimensions.
        /// </summary>
        public int DimensionCount
        {
            get
            {
                int row = 0, page = 0, col = 0;
                if(ColDims != null)
                {
                    col = ColDims.Count;
                }
                if(RowDims != null)
                {
                    row = RowDims.Count;
                }
                if(PageDims != null)
                {
                    page = PageDims.Count;
                }
                return (page + row + col);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<Range>> ViewHeaderStyles
        {
            get { return _viewHeaderStyles ?? (_viewHeaderStyles = new Dictionary<string, List<Range>>()); }
        }

        /// <summary>
        /// Styles and tuple for header rows/cols.
        /// </summary>
        public Dictionary<TupleFormat, List<Tuple>> HeaderStyles
        {
            get { return _headerStyles ?? (_headerStyles = new Dictionary<TupleFormat, List<Tuple>>()); }
        }

        /// <summary>
        /// Styles and tuples for data columns.
        /// </summary>
        public Dictionary<TupleFormat, List<Tuple>> DataColStyles
        {
            get { return _dataColStyles ?? (_dataColStyles = new Dictionary<TupleFormat, List<Tuple>>()); }
        }

        /// <summary>
        /// Styles and tuple for data rows.
        /// </summary>
        public Dictionary<TupleFormat, List<Tuple>> DataRowStyles
        {
            get { return _dataRowStyles ?? (_dataRowStyles = new Dictionary<TupleFormat, List<Tuple>>()); }
        }

        /// <summary>
        /// Hashmap of tuples that are valid for conditional formatting.
        /// </summary>
        public HashSet<string> SecondaryConditionalTuples { set; get; }
        

        /// <summary>
        /// Conditional styles and tuple for data rows.
        /// </summary>
        public Dictionary<TupleConditionalFormat, List<Tuple>> DataColConditionalStyles
        {
            get { return _dataColCondStyles ?? (_dataColCondStyles = new Dictionary<TupleConditionalFormat, List<Tuple>>()); }
        }

        /// <summary>
        /// Conditional styles and tuple for data cols.
        /// </summary>
        public Dictionary<TupleConditionalFormat, List<Tuple>> DataRowConditionalStyles
        {
            get { return _dataRowCondStyles ?? (_dataRowCondStyles = new Dictionary<TupleConditionalFormat, List<Tuple>>()); }
        }

        /// <summary>
        /// Col tuples parent first.
        /// </summary>
        public Dictionary<int, bool> RowParentFirst
        {
            get { return _rowParentFirst ?? (_rowParentFirst = new Dictionary<int, bool>()); }
        }

        /// <summary>
        /// Row tuples parent first.
        /// </summary>
        public Dictionary<int, bool> ColParentFirst
        {
            get { return _colParentFirst ?? (_colParentFirst = new Dictionary<int, bool>()); }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<Tuple>> NumericRowFormats
        {
            get { return _numericRowFormats ?? (_numericRowFormats = new Dictionary<string, List<Tuple>>()); }

            set
            {
                _numericRowFormats = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, List<Tuple>> NumericColFormats
        {
            get { return _numericColFormats ?? (_numericColFormats = new Dictionary<string, List<Tuple>>()); }

            set
            {
                _numericColFormats = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Range ForwardPlannableRange { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        public Range NonPlannableRange { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Range InValidAttributeRange { get; set; }

        /// <summary>
        /// Invalid replication range.
        /// </summary>
        public List<PafSimpleCoordList> InValidReplicationRange
        {
            get { return _inValidReplicationRange ?? (_inValidReplicationRange = new List<PafSimpleCoordList>()); }

            set { _inValidReplicationRange = value; }
        }

        /// <summary>
        /// Invalid member tag ranges, this is where a member tag row and column intersect.
        /// </summary>
        public Range InValidMemberTagRange { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Range SingletonRuleRange { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Range BOHProtectedRange { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Range SessionLockProtectedRange { get; set; }

        
        /// <summary>
        /// 
        /// </summary>
        public Dictionary<ViewAxis, List<ContiguousRange>> MergedCellRanges
        {
            get { return _mergedCellRanges ?? (_mergedCellRanges = new Dictionary<ViewAxis, List<ContiguousRange>>()); }
        }

        /// <summary>
        /// Row heights in view
        /// </summary>
        public Dictionary<float, List<int>> RowHeights
        {
            get { return _rowHeights ?? (_rowHeights = new Dictionary<float, List<int>>()); }
        }

        /// <summary>
        /// Column widths in view
        /// </summary>
        public Dictionary<float, List<int>> ColWidths
        {
            get { return _colWidths ?? (_colWidths = new Dictionary<float, List<int>>()); }
        }

        /// <summary>
        /// A list of Row positions that symetric tuple groups begin
        /// </summary>
        public Dictionary<int, int?[]> RowSymetricTupleGroups
        {
            get { return _rowSymetricTupleGroups ?? (_rowSymetricTupleGroups = new Dictionary<int, int?[]>()); }
        }

        /// <summary>
        /// A list of col positions that symetric tuple groups begin
        /// </summary>
        public Dictionary<int, int?[]> ColSymetricTupleGroups
        {
            get { return _colSymetricTupleGroups ?? (_colSymetricTupleGroups = new Dictionary<int, int?[]>()); }
        }

        internal List<SortSelection> SortSelections
        {
            get { return _sortSelections ?? (_sortSelections = new List<SortSelection>()); }

            set { _sortSelections = value; }
        }

        /// <summary>
        /// Specifies if a sort criteria has been specified by the server.
        /// </summary>
        public bool ServerSortSpecified { get; set; }

        /// <summary> 
        /// Specified if a user has overridden the server sort.
        /// </summary>
        public bool ServerSortOverridden { get; set; }

        /// <summary>
        /// Dictionary of Tuples to sort by.  The Boolean value determines whether or not the sort is ascending.
        /// </summary>
        public List<SortBy> SortByList
        {
            get { return _sortByList ?? (_sortByList = new List<SortBy>()); }

            set { _sortByList = value; }
        }

        /// <summary>
        /// List of positions to sort on
        /// </summary>
        public List<int> SortPositions
        {
            get { return _sortPositions ?? (_sortPositions = new List<int>()); }

            set { _sortPositions = value; }
        }

        /// <summary>
        /// Indicates that the view has been sorted.
        /// </summary>
        public bool IsSorted { get; set; }

        /// <summary>
        /// Indicates that the view has been suppressed.
        /// </summary>
        public bool IsSuppressed { get; set; }

        /// <summary>
        /// The most recent sort order for the view section rows.
        /// </summary>
        public List<int> CurrentSortOrder
        {
            get { return _currentSortOrder ?? (_currentSortOrder = new List<int>()); }

            set { _currentSortOrder = value; }
        }

        /// <summary>
        /// The range that has been selected by the user to be sorted.
        /// </summary>
        public Range SortRange
        {
            get { return _sortRange ?? (_sortRange = new Range()); }

            set { _sortRange = value; }
        }

        /// <summary>
        /// The TopLeft Anchor Position for the Window
        /// </summary>
        public Cell TopLeftWindowAnchor { get; set; }

        /// <summary>
        /// The View Section Dimensions in Priority Order
        /// </summary>
        public string[] DimensionPriority { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool IsEmpty { get; set; }

        /// <summary>
        /// The number of row aliases in the view section.
        /// This values will not be populated until view has been rendered or render has been called.
        /// </summary>
        public int RowAliases { get; private set; }

        /// <summary>
        /// The number of column aliases in the view section.
        /// This values will not be populated until view has been rendered or render has been called.
        /// </summary>
        public int ColAliases { get; private set; }

        /// <summary>
        /// Gets the number of indexes that cells must be adjust for.  
        /// This is due to column headers, page information, and aliases
        /// </summary>
        public int RowOffset
        {
            get
            { 
                return StartCell.Row + _colDims.Count + ColAliases;
            }
        }

        /// <summary>
        /// Gets the number of indexes that cells must be adjust for.  
        /// This is due to row headers, page information, and aliases
        /// </summary>
        public int ColOffset
        {
            get
            {
                return StartCell.Col + _rowDims.Count + RowAliases;
            }
        }

        /// <summary>
        /// Cached member tag variable info.
        /// </summary>
        public MemberTags MemberTag
        {
            [DebuggerHidden]
            get { return _memberTag ?? (_memberTag = new MemberTags(this)); }
        }

        /// <summary>
        /// True if the view section has attribute dimensions.
        /// </summary>
        [DebuggerHidden]
        public bool HasAttributes { [DebuggerHidden]get; [DebuggerHidden]set; }

        /// <summary>
        /// The freeze pane cell as set by the user / server.
        /// </summary>
        public CellAddress FreezePaneCell
        {
            get { return _freezePaneCell; }
            //Adjust the col/row with the proper offsets.
            set
            {
                int row = value.Row + RowOffset;
                //if (row == 0)
                //{
                //    row = RowOffset;
                //}
                int col = value.Col + ColOffset;
                //if (col == 0)
                //{
                //    col = ColOffset;
                //}
                _freezePaneCell = new CellAddress(row, col);
            }
        }


        public OlapView()
        {
            ColAliases = 0;
            RowAliases = 0;
            IsEmpty = false;
            IsSuppressed = false;
            IsSorted = false;
            BOHProtectedRange = null;
            SessionLockProtectedRange = null;
            SingletonRuleRange = null;
            InValidMemberTagRange = null;
            InValidAttributeRange = null;
            NonPlannableRange = null;
            ForwardPlannableRange = null;
            PrimaryFormattingAxis = ViewAxis.Unknown;
            StartCell = null;
            RowGroupSpec = null;
            ColGroupSpec = null;
            ConditionalFormat = null;
            _freezePaneCell = null;
            SecondaryConditionalTuples = new HashSet<string>();
            _invalidExcelGridNumbers.Add((int)PafAppConstants.ExcelGridErrEnum.ErrDiv0);
            _invalidExcelGridNumbers.Add((int)PafAppConstants.ExcelGridErrEnum.ErrNA);
            _invalidExcelGridNumbers.Add((int)PafAppConstants.ExcelGridErrEnum.ErrName);
            _invalidExcelGridNumbers.Add((int)PafAppConstants.ExcelGridErrEnum.ErrNull);
            _invalidExcelGridNumbers.Add((int)PafAppConstants.ExcelGridErrEnum.ErrRef);
            _invalidExcelGridNumbers.Add((int)PafAppConstants.ExcelGridErrEnum.ErrValue);
        }

        /// <summary>
        /// Render the page members, page headers, and data into the grid.
        /// </summary>
        /// <param name="grid">Grid where the data will be placed.</param>
        /// <param name="pafView">The view that contains the information to be rendered.</param>
        /// <param name="showPageMembers">Shows or hides the page members.</param>
        /// <param name="setRowGroups">Set the Excel Row Groups</param>
        /// <param name="setColumnGroups">Set the Excel Column Groups</param>
        public bool Render(IGridInterface grid, pafView pafView, bool showPageMembers, bool setRowGroups = true, bool setColumnGroups = true)
        {
            _grid = grid;
            _pafView = pafView;
            string invalidMember = Render(showPageMembers, _pafView.name, _pafView.viewSections[0], setRowGroups, setColumnGroups);
            if(invalidMember.Length > 0)
            {
                PafApp.MessageBox().Show(
                    PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.Exception.InvalidMembers"),
                    PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.Title"),
                     invalidMember, 
                     SystemIcons.Error,
                     frmMessageBox.frmMessageBoxButtons.OK);

                throw new Exception(String.Empty);
            }
            RenderPageHeaders();

            bool hasData = PopulateData(_pafView.viewSections[0].pafDataSlice);
            //only contine if we have data to populate from the server...
            if (hasData)
            {
                //Clear out the member tag columns.
                ClearMbrTagData();
                PopulateMbrTags(_pafView.viewSections[0]);
                OverlayChangedMemberTags(_pafView.viewSections[0]);
            }
            return hasData;
        }

        /// <summary>
        /// Sets the PafViewHeaders on the grid.
        /// </summary>
        private void RenderPageHeaders()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            //If there are not headers then just exit.
            if (Headers.Count <= 0)
                return;

            string[,] headerArray;
            int r = 0, c = 0;

            headerArray = new string[Headers.Count, c + 1];

            foreach(string header in Headers)
            {
                headerArray[r, c] = header;
                r++;
            }

            //03/03/2008 Moved To Server
            ////Get Page Headers
            //foreach (string de in Headers)
            //{
            //    //output the individual headers
            //    if (de.Contains("@"))
            //    {
            //        string tempHeader = DetokenizeDimensionHeader(de, pafView.name);
            //        tempHeader = DetokenizeUserSelectionHeader(tempHeader, pafView.name);
            //        headerArray[r, c] = tempHeader;
            //    }
            //    else
            //        headerArray[r, c] = de;

            //    r++;
            //}

            //Set the array to the grid.
            _grid.SetValueTextArray(HeaderRange, headerArray);

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("RenderPageHeaders, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }
        
        ///// <summary>
        ///// Detokenizes the @DIMNAME syntax from the header.
        ///// </summary>
        ///// <param name="header">The header to detokenize.</param>
        ///// <param name="viewName">The name of the view the user is accessing.</param>
        ///// <returns>A string with token(s) replaced by the value.</returns>
        ///// <remarks>These are going to be rendered on the server per Alan.</remarks>
        //private string DetokenizeDimensionHeader(string header, string viewName)
        //{
        //    try
        //    {
        //        string newHeader = header.Clone().ToString();
        //        foreach (KeyValuePair<string, string> kv in _PageDims)
        //        {
        //            //Look for the @DIMNAME syntax.
        //            string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(viewName, kv.Key);
        //            string member;
        //            string aMember;

        //            //get the alias values...
        //            PafApp.GetViewTreeView().GetAliasesValues(viewName, kv.Key, aliasTable, kv.Value, out member, out aMember);
        //            //get the position of the token.
        //            int pos = newHeader.IndexOf(TOKEN_START + kv.Key, StringComparison.CurrentCultureIgnoreCase);
        //            //do the replacement/concat if the token exists.
        //            if (pos >= 0)
        //            {
        //                newHeader = String.Concat(newHeader.Substring(0, pos), member, 
        //                    newHeader.Substring(pos + (TOKEN_START + kv.Key).Length));
        //            }
        //        }
        //        return newHeader;
        //    }
        //    catch
        //    {
        //        return header;
        //    }
        //}

        ///// <summary>
        ///// Detokenizes the @USER_SEL(xx) syntax from the header.
        ///// </summary>
        ///// <param name="header">The header to detokenize.</param>
        ///// <param name="viewName">The name of the view the user is accessing.</param>
        ///// <returns>A string with token(s) replaced by the value.</returns>
        ///// <remarks>These are going to be rendered on the server per Alan.</remarks>
        //private string DetokenizeUserSelectionHeader(string header, string viewName)
        //{
        //    try
        //    {
        //        string newHeader = header.Clone().ToString();

        //        if (!PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName))
        //            return header;

        //        //Now look for the @USER_SEL() syntax.
        //        if (newHeader.ToUpper().Contains(TOKEN_START + USER_SEL_SYNTAX + "("))
        //        {
        //            pafUserSelection[] userSelections = PafApp.GetGridApp().ActionsPane.GetCachedDyanmicPafUserSelections(viewName);
        //            if (userSelections != null)
        //            {
        //                foreach (pafUserSelection userSel in userSelections)
        //                {
        //                    int pos = newHeader.IndexOf(TOKEN_START + USER_SEL_SYNTAX + "(" + userSel.id + ")",
        //                        StringComparison.CurrentCultureIgnoreCase);

        //                    if (pos >= 0)
        //                    {
        //                        //get the alias table
        //                        string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(viewName, userSel.dimension);
        //                        //create a new array.
        //                        string[] members = new string[userSel.values.Length];
        //                        //get the alias or member values.
        //                        for (int i = 0; i < userSel.values.Length; i++)
        //                        {
        //                            string member;
        //                            string aMember;
        //                            PafApp.GetViewTreeView().GetAliasesValues(viewName, userSel.dimension, aliasTable, userSel.values[i], out member, out aMember);
        //                            members[i] = member;
        //                        }
        //                        //concanate the aliase/member values into the header...
        //                        newHeader = String.Concat(
        //                            newHeader.Substring(0, pos),
        //                            String.Join(", ", members),
        //                            newHeader.Substring(pos + (TOKEN_START + USER_SEL_SYNTAX + "(" + userSel.id + ")").Length));
        //                    }
        //                }
        //            }
        //        }
        //        return newHeader;
        //    }
        //    catch
        //    {
        //        return header;
        //    }
        //}

        /// <summary>
        /// Render the Row, Col, and Page Headers on the grid
        /// </summary>
        /// <param name="showPageMembers">Show the page dimension(s) across the top of the column headers.</param>
		///<param name="viewName">Name of the view.</param>
		/// <param name="viewSection">View Section Associated with the view.</param>
        /// <param name="setRowGroups">Set the Excel Row Groups</param>
        /// <param name="setColumnGroups">Set the Excel Column Groups</param>
        private string Render(bool showPageMembers, string viewName, pafViewSection viewSection, bool setRowGroups = true, bool setColumnGroups = true)
		{
            Stopwatch startTime = Stopwatch.StartNew();
            int r, c=0;
            int mbrCount;
            int dimCount;
            string[,] memberArray;
            string member, pMember = String.Empty;
            string prevMember;
            List<string> invalidMembers = new List<string>();
            bool mergedCellFlag;
            bool hasCommMbrTag;
            bool mbrIsMbrTag;
            int curRow = 0;
            int curCol = 0;
            CellAddress topLeft = null;
            int?[] prevSymTupleGroup;
            bool differentSymGroup;
            int adjTupleGroupLength;
            string blankMember = Settings.Default.Blank;

            //need to get primary Row count.
            if (RowDims.Count > 0)
            {
                string[] rDims = new string[RowDims.Count];
                RowDims.Keys.CopyTo(rDims, 0);
                RowAliases = PafApp.GetViewTreeView().GetAdditionalRowColumnFormat(viewName, rDims);
            }
            //need to get primary Col count.
            if (ColDims.Count > 0)
            {
                string[] cDims = new string[ColDims.Count];
                ColDims.Keys.CopyTo(cDims, 0);
                ColAliases = PafApp.GetViewTreeView().GetAdditionalRowColumnFormat(viewName, cDims);
            }

            #region PageMembers
            // Output Page Headers
            if (showPageMembers)
            {
                if (PageDims.Count > 0)
                {
                    memberArray = new string[1, PageDims.Count];

                    foreach (KeyValuePair<string, string> kv in PageDims)
                    {
                        string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(viewName, kv.Key);

                        // output the individual members for the page
                        //string mbrAlias = PafApp.GetGridApp().GetSimpleTrees().GetAlias(kv.Key, kv.Value, aliasTable);

                        PafApp.GetViewTreeView().GetAliasesValues(viewName, kv.Key, aliasTable, kv.Value, out member, out pMember);

                        if (member != null)
                        {
                            memberArray[0, c] = member;
                        }
                        else
                        {
                            memberArray[0, c] = kv.Value;
                        }

                        c++;
                    }

                    //Populate Page Range
                    _grid.SetValueTextArray(PageRange, memberArray);
                }
            }
            #endregion PageMembers

            //TTN-
            //Remove any existing groupings
            _grid.ClearGroupings();

            if (ColDims.Count > 0)
            {
                // Output Column Headers for each dimension in columns dimensions
                // Initialize starting offsets, columns must start inset by the total # of row dim
                mbrCount = GetAxisMemberCount(ViewAxis.Col);
                dimCount = ColDims.Count;
                //add the alias columns.
                dimCount += ColAliases;
                memberArray = new string[dimCount, mbrCount];
                prevMember = "";
                prevSymTupleGroup = null;
                List<string> mbrTagMbrsToIgr = new List<string>();
                memberTagDef tag = null;
                string colGroupDim = String.Empty;
                r = 0; c = 0; mergedCellFlag = false;

                int groupMbrCount = 0;
                if (ColDims.ContainsKey(colGroupDim))
                {
                    groupMbrCount = ColDims[colGroupDim].Distinct().Count();
                }

                if (ColGroupSpec != null)
                {
                    colGroupDim = ColGroupSpec.DimensionName;
                    ColGroupSpec.InitializeMemberDictionaries(groupMbrCount);
                    ColGroupSpec.ApplyGroups = setColumnGroups;
                }
               

                foreach (string dim in ColDims.Keys)
                {
                    //get the number of additional alias cols.
                    int dimAliasCount = PafApp.GetViewTreeView().GetAdditionalRowColumnFormat(viewName, dim);
                    //get the alias table.
                    string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(viewName, dim);
                    if(String.IsNullOrEmpty(aliasTable))
                    {
                        aliasTable = PafAppConstants.ALIAS_DEFAULT_TABLE_NAME;
                    }

                    // output the individual members for that column
                    foreach (string mbrName in ColDims[dim])
                    {
                        if (!String.IsNullOrWhiteSpace(colGroupDim) && dim.Equals(colGroupDim) && !mbrName.EqualsIgnoreCase(blankMember))
                        {
                            BuildMemberGroups(ColGroupSpec, mbrName, ColParentFirst[c], ColRange.TopLeft.Row + r, ColRange.TopLeft.Col + c);
                            ColGroupSpec.Range = ColRange.TopLeft.Row + r;
                        }
                        else if (!String.IsNullOrWhiteSpace(colGroupDim) && dim.Equals(colGroupDim) && mbrName.EqualsIgnoreCase(blankMember))
                        {
                            ColGroupSpec.Blanks.Add(new CellAddress(ColRange.TopLeft.Row + r, ColRange.TopLeft.Col + c));
                        }

                        List<string> commentMbrTag;
                        hasCommMbrTag = MemberTag.ColCommentMemberTags.TryGetValue(dim, out commentMbrTag);
                        mbrIsMbrTag = false;

                        //Populate blanks with empty strings
                        if (mbrName.ToUpper() != blankMember)
                        {
                            PafApp.GetViewTreeView().GetAliasesValues(viewName, dim, aliasTable, mbrName, out member, out pMember);
                            if (member == null)
                            {
                                tag = PafApp.GetMemberTagInfo().GetMemberTag(mbrName);
                                if(tag != null)
                                {
                                    member = tag.label;
                                    mbrIsMbrTag = true;
                                }
                                else
                                {
                                    if (!invalidMembers.Contains(mbrName))
                                    {
                                        invalidMembers.Add(mbrName);
                                    }
                                }
                            }
                            else if (member == String.Empty)
                            {
                                PafApp.GetViewTreeView().GetAliasesValues(viewSection.aliasMappings, dim, aliasTable, mbrName, out member, out pMember);
                            }
                        }
                        else
                        {
                            member = String.Empty;
                            pMember = String.Empty;
                        }

                        //Determine if the current member and the previous member are in the same symetric tuple group. 
                        //Symetric tuple groups are defined on the server.
                        differentSymGroup = false;
                        if (prevSymTupleGroup != null)
                        {
                            if (ColSymetricTupleGroups.ContainsKey(r))
                            {
                                if (ColSymetricTupleGroups[r].Length > 1)
                                {
                                    adjTupleGroupLength = ColSymetricTupleGroups[r].Length - 2;
                                }
                                else
                                {
                                    adjTupleGroupLength = ColSymetricTupleGroups[r].Length - 1;
                                }

                                for (int k = 0; k <= adjTupleGroupLength; k++)
                                {
                                    if (ColSymetricTupleGroups[c][k] != prevSymTupleGroup[k])
                                    {
                                        differentSymGroup = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //Identicle members in the same symetric tuple group will be merged for presentation
                        //purposes
                        if (member == prevMember && differentSymGroup == false)
                        {
                            memberArray[r, c] = String.Empty;
                            if (dimAliasCount > 0)
                            {
                                memberArray[r + 1, c] = String.Empty;
                            }

                            //Get the Cell Address for the first merged cell.
                            if (mergedCellFlag == false)
                            {
                                mergedCellFlag = true;

                                curRow = ColRange.TopLeft.Row + r;
                                curCol = ColRange.TopLeft.Col + c - 1;
                                topLeft = new CellAddress(curRow, curCol);
                            }
                        }
                        else
                        {
                            memberArray[r, c] = member;
                            if (dimAliasCount > 0)
                            {
                                memberArray[r + 1, c] = pMember;
                            }
                            //if the member is a member tag then we must do some special merging.
                            if (mbrIsMbrTag)
                            {
                                if (mbrTagMbrsToIgr.Contains(member) && r > 0)
                                {
                                    memberArray[r, c] = String.Empty;
                                }
                                else
                                {
                                    mbrTagMbrsToIgr.Add(member);
                                    MergeMemberTag(ViewAxis.Col, tag, MemberTag.ColMbrTagSymetricTupleGroups);
                                }
                            }
                            //add one for the original member/alias...
                            MergeColCells(ref mergedCellFlag, r, c, topLeft, dimAliasCount +1);
                            CacheTupleValues(mbrName, r, c, ColRange, dimAliasCount, ViewAxis.Col);
                            if (hasCommMbrTag)
                            {
                                if (commentMbrTag != null && commentMbrTag.Count > 0)
                                {
                                    foreach (string mbrTag in commentMbrTag)
                                    {
                                        MemberTag.AddCellNotes(dim, mbrTag, mbrName, ViewAxis.Col, r, c, true);
                                    }
                                }
                            }
                        }

                        //Store the previous member for comparison purposes
                        prevMember = member;
                        //Store the previous Symetric Tuple Group for comparison purposes
                        if (ColSymetricTupleGroups.ContainsKey(c))
                        {
                            prevSymTupleGroup = ColSymetricTupleGroups[c];
                        }
                        else
                        {
                            prevSymTupleGroup = null;
                        }

                        c++;
                    }

                    //add one for the original member/alias...
                    MergeColCells(ref mergedCellFlag, r, c, topLeft, dimAliasCount +1);
                    

                    //Reset the the previous Symetric Tuple Group to null for each Dimension
                    prevSymTupleGroup = null;
                    c = 0;
                    if (dimAliasCount > 0)
                    {
                        r = r + 1 + dimAliasCount;
                    }
                    else
                    {
                        r++;
                    }
                }

                //TTN-804
                BuildMemberGroups(ViewAxis.Col);

                //Populate Column Range
                _grid.SetValueTextArray(ColRange, memberArray);
            }

            if (RowDims.Count > 0)
            {
                // Output Row Headers for each dimension in row dimensions
                // Initialize starting offsets, rows must start inset by the total # of col dim
                mbrCount = GetAxisMemberCount(ViewAxis.Row);
                dimCount = RowDims.Count;
                //add the alias columns.
                dimCount += RowAliases;
                memberArray = new string[mbrCount,dimCount];
                prevMember = "";
                prevSymTupleGroup = null;
                List<string> mbrTagMbrsToIgr = new List<string>();
                memberTagDef tag = null;
                string rowGroupDim = String.Empty;

                r = 0; c = 0; mergedCellFlag = false;

                int groupMbrCount = 0;
                if (RowDims.ContainsKey(rowGroupDim))
                {
                    groupMbrCount = RowDims[rowGroupDim].Distinct().Count();
                }


                if (RowGroupSpec != null)
                {
                    rowGroupDim = RowGroupSpec.DimensionName;
                    RowGroupSpec.InitializeMemberDictionaries(groupMbrCount);
                    RowGroupSpec.ApplyGroups = setRowGroups;
                }

                foreach (string dim in RowDims.Keys)
                {
                    //get the number of additional alias cols.
                    int dimAliasCount = PafApp.GetViewTreeView().GetAdditionalRowColumnFormat(viewName, dim);
                    //get the alias table.
                    string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(viewName, dim);
                    if(String.IsNullOrEmpty(aliasTable))
                    {
                        aliasTable = PafAppConstants.ALIAS_DEFAULT_TABLE_NAME;
                    }

                    // output the individual members for that column
                    foreach (string mbrName in RowDims[dim])
                    {
                        if (!String.IsNullOrWhiteSpace(rowGroupDim) && dim.Equals(rowGroupDim) && !mbrName.EqualsIgnoreCase(blankMember))
                        {
                            BuildMemberGroups(RowGroupSpec, mbrName, RowParentFirst[c], RowRange.TopLeft.Row + r, RowRange.TopLeft.Col + c);
                            RowGroupSpec.Range = RowRange.TopLeft.Col + c;
                        }
                        else if (!String.IsNullOrWhiteSpace(rowGroupDim) && dim.Equals(rowGroupDim) && mbrName.EqualsIgnoreCase(blankMember))
                        {
                            RowGroupSpec.Blanks.Add(new CellAddress(RowRange.TopLeft.Row + r, RowRange.TopLeft.Col + c));
                        }

                        List<string> commentMbrTag;
                        hasCommMbrTag = MemberTag.RowCommentMemberTags.TryGetValue(dim, out commentMbrTag);
                        mbrIsMbrTag = false;

                        //Populate blanks with empty strings
                        if (mbrName.ToUpper() != blankMember)
                        {
                            
                            PafApp.GetViewTreeView().GetAliasesValues(viewName, dim, aliasTable, mbrName, out member, out pMember);
                            if (member == null)
                            {
                                tag = PafApp.GetMemberTagInfo().GetMemberTag(mbrName);
                                if (tag != null)
                                {
                                    member = tag.label;
                                    mbrIsMbrTag = true;
                                }
                                else
                                {
                                    if (!invalidMembers.Contains(mbrName))
                                    {
                                        invalidMembers.Add(mbrName);
                                    }
                                }
                            }
                            else if (member == String.Empty)
                            {
                                PafApp.GetViewTreeView().GetAliasesValues(viewSection.aliasMappings, dim, aliasTable, mbrName, out member, out pMember);
                            }
                        }
                        else
                        {
                            member = String.Empty;
                            pMember = String.Empty;
                        }

                        //Determine if the current member and the previous member are in the same symetric tuple group. 
                        //Symetric tuple groups are defined on the server.
                        differentSymGroup = false;
                        if (prevSymTupleGroup != null)
                        {
                            if (RowSymetricTupleGroups.ContainsKey(r))
                            {
                                if (RowSymetricTupleGroups[r].Length > 1)
                                {
                                    adjTupleGroupLength = RowSymetricTupleGroups[r].Length - 2;
                                }
                                else
                                {
                                    adjTupleGroupLength = RowSymetricTupleGroups[r].Length - 1;
                                }

                                for (int k = 0; k <= adjTupleGroupLength; k++)
                                {
                                    if (RowSymetricTupleGroups[r][k] != prevSymTupleGroup[k])
                                    {
                                        differentSymGroup = true;
                                        break;
                                    }
                                }
                            }
                        }

                        //Identicle members in the same symetric tuple group will be merged for presentation
                        //purposes
                        if (member == prevMember && !differentSymGroup)
                        {
                            memberArray[r, c] = String.Empty;
                            if (dimAliasCount > 0)
                            {
                                memberArray[r, c + 1] = String.Empty;
                            }
                            
                            //Get the Cell Address for the first merged cell.
                            if (mergedCellFlag == false)
                            {
                                mergedCellFlag = true;

                                curRow = RowRange.TopLeft.Row + r - 1;
                                curCol = RowRange.TopLeft.Col + c;
                                topLeft = new CellAddress(curRow, curCol);
                            }
                        }
                        else
                        {
                            memberArray[r, c] = member;
                            if (dimAliasCount > 0)
                            {
                                memberArray[r, c + 1] = pMember;
                            }
                            //if the member is a member tag then we must do some special merging.
                            if (mbrIsMbrTag)
                            {
                                if (mbrTagMbrsToIgr.Contains(member) && c > 0)
                                {
                                    memberArray[r, c] = String.Empty;
                                }
                                else
                                {
                                    mbrTagMbrsToIgr.Add(member);
                                    MergeMemberTag(ViewAxis.Row, tag, MemberTag.RowMbrTagSymetricTupleGroups);
                                }
                            }
                            //add one for the original member/alias...
                            MergeRowCells(ref mergedCellFlag, r, c, topLeft, dimAliasCount + 1);
                            CacheTupleValues(mbrName, r , c , RowRange, dimAliasCount, ViewAxis.Row);
                            if (hasCommMbrTag)
                            {
                                if (commentMbrTag != null && commentMbrTag.Count > 0)
                                {
                                    foreach (string mbrTag in commentMbrTag)
                                    {
                                        MemberTag.AddCellNotes(dim, mbrTag, mbrName, ViewAxis.Row, r, c, true);
                                    }
                                }
                            }
                        }

                        //Store the previous member for comparison purposes
                        prevMember = member;
                        //Store the previous Symetric Tuple Group for comparison purposes
                        if (RowSymetricTupleGroups.ContainsKey(r))
                        {
                            prevSymTupleGroup = RowSymetricTupleGroups[r];
                        }
                        else
                        {
                            prevSymTupleGroup = null;
                        }

                        r++;
                    }

                    //add one for the original member/alias...
                    MergeRowCells(ref mergedCellFlag, r, c, topLeft, dimAliasCount + 1);

                    //Reset the the previous Symetric Tuple Group to null for each Dimension
                    prevSymTupleGroup = null;
                    r = 0;

                    if(dimAliasCount > 0)
                    {
                        c = c + 1 + dimAliasCount;
                    }
                    else
                    {
                        c++;
                    }
                }

                //TTN-804
                BuildMemberGroups(ViewAxis.Row);

                //Populate Row Range
                _grid.SetValueTextArray(RowRange, memberArray);
            }

            MemberTag.ColBorderCommentProcessed.Clear();
            MemberTag.RowBorderCommentProcessed.Clear();

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("Render, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

            if(invalidMembers.Count > 0)
            {
                return String.Join(PafAppConstants.CRLF, invalidMembers.ToArray()).Trim();
            }
            else
            {
                return String.Empty;
            }
		}


        private void BuildMemberGroups(GroupingSpec spec, string mbrName, bool parentFirst, int row, int column)
        {
            string dimension = spec.DimensionName;
            if (String.IsNullOrWhiteSpace(spec.FirstMember))
            {
                spec.FirstMember = mbrName;
                spec.FirstLevel = SimpleDimTrees.GetMemberLevel(dimension, mbrName);
            }
            if (!spec.MbrDescendants.ContainsKey(mbrName))
            {
                var allMembers = SimpleDimTrees.GetDescendants(dimension, mbrName);
                if (allMembers.Count > 0)
                {
                    var level = SimpleDimTrees.GetMemberLevel(dimension, mbrName);
                    spec.Levels.Add(level);
                    if (!spec.MemberLevels.ContainsKey(mbrName))
                    {
                        spec.MemberLevels.Add(mbrName, level);
                    }
                    var generation = SimpleDimTrees.GetMemberGeneration(dimension, mbrName);
                    spec.Generations.Add(generation);
                    spec.MbrDescendants.Add(mbrName, allMembers);
                }
                else
                {
                    var level = SimpleDimTrees.GetMemberLevel(dimension, mbrName);
                    spec.Levels.Add(level);
                    var generation = SimpleDimTrees.GetMemberGeneration(dimension, mbrName);
                    spec.Generations.Add(generation);
                }
            }
            List<CellAddress> addr;
            CellAddress cell = new CellAddress(row, column);
            if (spec.MbrAddresses.TryGetValue(mbrName, out addr))
            {
                addr.Add(cell);
                spec.MbrAddresses[mbrName] = addr;
            }
            else
            {
                addr = new List<CellAddress> {cell};
                spec.MbrAddresses.Add(mbrName, addr);
            }

            if (!spec.MbrAddressParentFirst.ContainsKey(cell))
            {
                spec.MbrAddressParentFirst.Add(cell, parentFirst);
            }

        }

        /// <summary>
        /// Clears the member groups for the specified axis.
        /// </summary>
        /// <param name="viewAxis"></param>
        public void ClearMemberGroups(ViewAxis viewAxis)
        {
            if (DataRange == null) return;
            Stopwatch startTime = Stopwatch.StartNew();
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    if (ColGroupSpec != null)
                    {
                        _grid.ClearGroupings(ViewAxis.Col, Convert.ToInt32(ColGroupSpec.MaxOutlineLevel));
                    }
                    break;
                case ViewAxis.Row:
                    if (RowGroupSpec != null)
                    {
                        _grid.ClearGroupings(ViewAxis.Row, Convert.ToInt32(RowGroupSpec.MaxOutlineLevel));
                    }
                    break;
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ClearMemberGroups, Axis: {0}, Runtime: {1} (ms)", viewAxis, startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Applies Excel Groups for the specified axis.
        /// </summary>
        /// <param name="viewAxis"></param>
        /// <exception cref="ArgumentException"></exception>
        public void BuildMemberGroups(ViewAxis viewAxis)
        {
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    BuildMemberGroups(ViewAxis.Col, ColGroupSpec);
                    break;
                case ViewAxis.Row:
                    BuildMemberGroups(ViewAxis.Row, RowGroupSpec);
                    break;
            }
        }

        /// <summary>
        /// Gets the grouping spec for the specified axis.
        /// </summary>
        /// <param name="viewAxis"></param>
        /// <returns></returns>
        public GroupingSpec GetGroupingSpec(ViewAxis viewAxis)
        {
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    return ColGroupSpec;
                case ViewAxis.Row:
                    return RowGroupSpec;
            }
            return null;
        }

        private void BuildMemberGroups(ViewAxis viewAxis, GroupingSpec spec)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            
            HashSet<bool> parentFirst = new HashSet<bool>();

            if (spec == null) return;
            spec.GroupsApplied = false;
            if (!spec.ApplyGroups) return;

            if (spec.Levels.Max() > 8)
            {
                spec.GroupsApplied = false;
                PafApp.GetLogger().Error("Excel only allows grouping up to 8 levels.  No groupings will be applied to the view.");
                return;
            }

            if (spec.MbrAddresses != null && spec.MbrAddresses.Count > 0)
            {
                //Need to build from bottom up, so get the lowest level members
                var members = spec.MemberLevels.OrderBy(x => x.Value).ToList();
                //foreach (var rowMbr in groupMembers)
                foreach (var member in members)
                {
                    List<string> rowMbr;
                    if (!spec.MbrDescendants.TryGetValue(member.Key, out rowMbr)) continue;

                    Range rng = new Range();
                    foreach (var mbr in rowMbr)
                    {
                        List<CellAddress> temp;
                        if (!spec.MbrAddresses.TryGetValue(mbr, out temp)) continue;
                        foreach (var t in temp)
                        {
                            bool pf;
                            spec.MbrAddressParentFirst.TryGetValue(t, out pf);
                            spec.ParentFirst.Add(pf);
                            rng.AddContiguousRange(t);
                        }
                    }
                    foreach (var range in rng.ContiguousRanges)
                    {
                        switch (viewAxis)
                        {
                            case ViewAxis.Col:
                                spec.GroupsApplied = true;
                                _grid.SetColumnGrouping(range);
                                break;
                            case ViewAxis.Row:
                                spec.GroupsApplied = true;
                                _grid.SetRowGrouping(range);
                                break;
                        }
                    }
                }
                if (spec.GroupsApplied.GetValueOrDefault())
                {
                    spec.BuildExcelGroupMaps();
                    _grid.SetOutlineSummaryLocation(viewAxis, spec.IsParentFirst);
                }
                else
                {
                    PafApp.GetLogger().WarnFormat("No members found suitable for grouping on axis: {0}, dimension: {1}", viewAxis, spec.DimensionName);
                }
            }

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyMemberGroups, Axis: {0}, Dimension: {1}, Runtime: {2} (ms)", viewAxis, spec.DimensionName, startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Merge member tag tuples borders.
        /// </summary>
        /// <param name="viewAxis">axis to merge.</param>
        /// <param name="tagDef">Member tag def.</param>
        /// <param name="tuplesToMerge">tuples to merge.</param>
        public void MergeMemberTag(ViewAxis viewAxis, memberTagDef tagDef, CachedDictionary<string, int, int> tuplesToMerge)
        {
            if(tuplesToMerge == null)
            {
                return;
            }

            Dictionary<string, Dictionary<int, int>>.KeyCollection mbrTags = tuplesToMerge.Keys;

            foreach (string mbrTag in mbrTags)
            {
                if (mbrTag.Equals(tagDef.name, StringComparison.CurrentCultureIgnoreCase))
                {
                    Dictionary<int, int> tuple = tuplesToMerge.Values(mbrTag);

                    if (tuple != null && tuple.Count > 0)
                    {
                        foreach (KeyValuePair<int, int> kvp in tuple)
                        {
                            int curRow;
                            int curCol;
                            CellAddress TopLeft = null;
                            CellAddress BottomRight = null;
                            switch (viewAxis)
                            {
                                case ViewAxis.Col:
                                    curRow = ColRange.TopLeft.Row;
                                    curCol = ColRange.TopLeft.Col + kvp.Key;

                                    TopLeft = new CellAddress(curRow, curCol);
                                    //create a new top left to use so that the alias merging works correctly.
                                    BottomRight = new CellAddress(curRow + kvp.Value - 1, curCol);

                                    break;
                                case ViewAxis.Row:
                                    curRow = RowRange.TopLeft.Row + kvp.Key;
                                    curCol = RowRange.TopLeft.Col;

                                    TopLeft = new CellAddress(curRow, curCol);
                                    //create a new top left to use so that the alias merging works correctly.
                                    BottomRight = new CellAddress(curRow, curCol + kvp.Value - 1);

                                    break;
                            }

                            //Build a Contiguous Range to store the Merged range.
                            ContiguousRange mergedCellRange = new ContiguousRange(TopLeft, BottomRight);
                            if (!MergedCellRanges.ContainsKey(viewAxis))
                            {
                                List<ContiguousRange> contigRanges = new List<ContiguousRange>();
                                MergedCellRanges.Add(viewAxis, contigRanges);
                            }
                            if (!MergedCellRanges[viewAxis].Contains(mergedCellRange))
                            {
                                MergedCellRanges[viewAxis].Add(mergedCellRange);
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Cache the values of the tuple.
        /// </summary>
        /// <param name="mbrName">String member to cache.</param>
        /// <param name="row">Current row.</param>
        /// <param name="col">Current col.</param>
        /// <param name="range">Current range</param>
        /// <param name="dimAliasCount">Number of aliases.</param>
        /// <param name="viewAxis">Axis the member is on.</param>
        private void CacheTupleValues(string mbrName, int row, int col, ContiguousRange range,
            int dimAliasCount, ViewAxis viewAxis)
        {
            if (!String.IsNullOrEmpty(mbrName) && range != null && range.Cells.Count > 0)
            {
                int CurRow = range.TopLeft.Row + row;
                int CurCol = range.TopLeft.Col + col;

                switch (viewAxis)
                {
                    case ViewAxis.Col:
                    {
                        CurRow = CurRow + dimAliasCount;
                        CellAddress ca = new CellAddress(CurRow, CurCol);
                        MemberTag.ColTupleBorderValues.AddOrUpdate(ca, mbrName);
                        break;
                    }
                    case ViewAxis.Row:
                    {
                        CurCol = CurCol + dimAliasCount;
                        CellAddress ca = new CellAddress(CurRow, CurCol);
                        MemberTag.RowTupleBorderValues.AddOrUpdate(ca, mbrName);
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Pull data from the cells in the grid to a Base64 string
        /// </summary>
        /// <param name="colCount"></param>
        /// <returns></returns>
        public string GetGridDataBase64(out int colCount)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            ContiguousRange dataRange = DataRange;
            StringBuilder getData = new StringBuilder();
            int colLength;

            object[,] arrayS = null;

            if(dataRange.Cells.Count == 1)
            {
                //TTN-1204
                double d = _grid.GetValueDouble(dataRange.TopLeft.Row, dataRange.TopLeft.Col);
                arrayS = new object[2,2];
                //use a one based array, becuase of Excel - Hence the extra item in the array.
                arrayS[1, 1] = d;
            }
            else
            {
                arrayS = _grid.GetValueObjectArray(dataRange);
            }

            colLength = (dataRange.BottomRight.Col - dataRange.TopLeft.Col + 1);
            colCount = colLength - _colBlanks.Count;

            for (int i = 1; i <= arrayS.Length / colLength; i++)
            {
                //Skip blank rows and columns
                if (!_rowBlanks.Contains(i - 1))
                {
                    for (int j = 1; j <= colLength; j++)
                    {
                        if (!_colBlanks.Contains(j - 1))
                        {
                            try
                            {

                                double d = 0.0;
                                if (!arrayS[i, j].IsNull())
                                {
                                    d = arrayS[i, j].GetDouble();
                                }

                                //this will throw an error for null's in the DS.
                                //double d = Double.Parse(arrayS[i, j].ToString());

                                //TTN-1198 - filter out any excel #DIV/0 etc...
                                int result;
                                if(arrayS[i, j].IsInteger(out result))
                                {
                                    if (!IsGridNumberValid(result))
                                    {
                                        d = 0.0;
                                    }
                                }

                                byte[] byteArray = BitConverter.GetBytes(d);
                                getData.Append(Convert.ToBase64String(byteArray));
                            }
                            catch
                            {
                                byte[] byteArray = BitConverter.GetBytes(0.0);
                                getData.Append(Convert.ToBase64String(byteArray));
                            }
                        }
                    }
                }
            }

            //Debug.WriteLine("GetGridDataBase64 time: " + diff.TotalSeconds + " (s)");
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("GetGridDataBase64, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

            return getData.ToString();
        }


        /// <summary>
        /// Validtes that an invalid number has not be placed into the grid.
        /// </summary>
        /// <param name="gridNumberValue">Grid number object.</param>
        /// <returns></returns>
        /// <remarks>Refarkas to someplace else.</remarks>
        public bool IsGridNumberValid(int gridNumberValue)
        {
            return !_invalidExcelGridNumbers.Contains(gridNumberValue);
        }

        ///// <summary>
        ///// Validtes that an invalid number has not be placed into the grid.
        ///// </summary>
        ///// <param name="gridNumberValue">Grid number object.</param>
        ///// <returns></returns>
        ///// <remarks>Refarkas to someplace else.</remarks>
        //public bool IsGridNumberValid(int gridNumberValue)
        //{
        //    bool ret = true;

        //    switch (gridNumberValue)
        //    {
        //        case (int)PafAppConstants.ExcelGridErrEnum.ErrDiv0:
        //            ret = false;
        //            break;
        //        case (int)PafAppConstants.ExcelGridErrEnum.ErrNA:
        //            ret = false;
        //            break;
        //        case (int)PafAppConstants.ExcelGridErrEnum.ErrName:
        //            ret = false;
        //            break;
        //        case (int)PafAppConstants.ExcelGridErrEnum.ErrNull:
        //            ret = false;
        //            break;
        //        case (int)PafAppConstants.ExcelGridErrEnum.ErrRef:
        //            ret = false;
        //            break;
        //        case (int)PafAppConstants.ExcelGridErrEnum.ErrValue:
        //            ret = false;
        //            break;
        //    }
        //    return ret;
        //}

        /// <summary>
        /// Populates the data in the grid.
        /// </summary>
        /// <param name="dataSlice">The source data to place in the grid.</param>
        /// <remarks>modified for aliases.</remarks>
        public bool PopulateData(pafDataSlice dataSlice)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            bool isBlankRow = false;
            bool isBlankCol = false;
            long rows = 0;
            long cols = 0;
            double[,] dataArray;
            bool ret = true;
            //modified for aliases.
            //int curRow = StartCell.Row + _ColDims.Count + 1 + _ColAliases;
            //int curCol = StartCell.Col + _RowDims.Count + 1 + _RowAliases;
            int blankRows = RowBlanks.Count;
            int blankCols = ColBlanks.Count;
            int i = 0;

            if (dataSlice == null || dataSlice.data == null)
            {
                _grid.SetValueText(StartCell.Row + _colDims.Count, 
                    StartCell.Col + _rowDims.Count, 
                    PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Exception.Class.OlapView.NoData"));

                return false;
            }

            dataArray = new double[(dataSlice.data.Length / dataSlice.columnCount) + blankRows, dataSlice.columnCount + blankCols];

            //Range range = new Range();

            while (i < dataSlice.data.Length)
            {
                if (RowBlanks.Contains((int) rows))
                {
                    for (int col = 0; col < dataSlice.columnCount + blankCols; col++)
                    {
                        //dataArray[rows, col] = null;
                        cols++; //increment columns
                    }
                    isBlankRow = true;                    
                }

                //the column blank has already been added if the row was also blank
                if (isBlankRow == false && ColBlanks.Contains((int)cols))
                {
                    //dataArray[rows, cols] = null;
                    isBlankCol = true;
                }

                //if no row/col blanks then add the data value to the data array
                if (!isBlankRow && !isBlankCol)
                {
                    ////uncomment this for null/blanks on the view.
                    //if (dataSlice.data[i].HasValue && !dataSlice.data[i].Value.IsZero())
                    //{
                    //    dataArray[rows, cols] = dataSlice.data[i].Value;

                    //} 
                    //dataArray[rows, cols] = (double) dataSlice.data.GetValue(i); //this is the old method, replaced with a new get that does not have a cast.
                    dataArray[rows, cols] = dataSlice.data[i].GetValueOrDefault();
                    //increment if not a blank row/col to pull next dataslice data value
                    i++;
                }

                //range.AddContiguousRange(new CellAddress(curRow, curCol));

                if (cols == dataSlice.columnCount - 1 + blankCols || isBlankRow)
                {
                    cols = 0;
                    //curCol = StartCell.Col + _RowDims.Count + 1;
                    rows++; //increment rows
                    //curRow++;
                }
                else
                {
                    cols++; //increment columns
                    //curCol++;
                }

                isBlankRow = false;
                isBlankCol = false;
            }

            //_Grid.SetValueObject(this.DataRange, dataArray);
            _grid.SetValueDoubleArray(this.DataRange, dataArray);

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("PopulateData, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

            return ret;
        }

        /// <summary>
        /// Populates all the member tag data in the grid.
        /// </summary>
        /// <param name="pafViewSection">pafViewSection that contains the member tag data.</param>
        public void PopulateMbrTags(pafViewSection pafViewSection)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            //populate mbr tags.
            foreach (KeyValuePair<int, string> kvp in MemberTag.ColMbrTagBlanks)
            {
                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(kvp.Value);
                PopulateMbrTagData(kvp.Key, pafViewSection, ViewAxis.Col, tagDef);
            }

            //populate mbr tags.
            foreach (KeyValuePair<int, string> kvp in MemberTag.RowMbrTagBlanks)
            {
                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(kvp.Value);
                PopulateMbrTagData(kvp.Key, pafViewSection, ViewAxis.Row, tagDef);
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("PopulateMbrTags, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Populates the member tag data in the grid.
        /// </summary>
        /// <param name="blankLocation">Location where the blank row or column was placed.</param>
        /// <param name="pafViewSection">pafViewSection that contains the member tag data.</param>
        /// <param name="viewAxis">Axis to populate the data on.</param>
        /// <param name="mbrTagDef">Member tag defination.</param>
        private void PopulateMbrTagData(int blankLocation, pafViewSection pafViewSection, 
            ViewAxis viewAxis, memberTagDef mbrTagDef)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            string[,] arrayToPaste = null;
            object[,] objArrayToPaste = null;

            //name of the member tag.
            string mbrTagName = mbrTagDef.name;

            //dataArray = new double[(dataSlice.data.Length / dataSlice.columnCount) + blankRows,
            //dataSlice.columnCount + blankCols];

            //get the data off the view section
            string[] data = MemberTag.GetMemberTagData(mbrTagName, pafViewSection, viewAxis, true);
            object[] data2 = MemberTag.GetMemberTagData(mbrTagName, pafViewSection, viewAxis, true);


            //if not was returned from the view section, return.
            if (data == null || data2 == null)
            {
                return;
            }

            //set the base cur row and col.
            int curRow = DataRange.TopLeft.Row;
            int curCol = DataRange.TopLeft.Col;

            //create the range for the paste array, this can be different depending on which
            //axis the member tag is on.
            //also adjust for the blank locations.
            switch (viewAxis)
            {
                case ViewAxis.Col:
                {
                    curCol = DataRange.TopLeft.Col + blankLocation;
                    int invalidLckCls = MemberTag.GetInvalidLockCellsLength(ViewAxis.Col, curCol);
                    arrayToPaste = new string[data.Length + invalidLckCls, 1];
                    objArrayToPaste = new object[data.Length + invalidLckCls, 1];
                    break;
                }
                case ViewAxis.Row:
                {
                    curRow = DataRange.TopLeft.Row + blankLocation;
                    int invalidLckCls = MemberTag.GetInvalidLockCellsLength(ViewAxis.Row, curRow);
                    arrayToPaste = new string[1, data.Length + invalidLckCls];
                    objArrayToPaste = new object[1, data.Length + invalidLckCls];
                    break;
                }
            }

            //create the top left cell in the paste range.
            ContiguousRange range = new ContiguousRange(new CellAddress(curRow, curCol));

            //if not was returned from the view section, return.
            if (arrayToPaste == null || objArrayToPaste == null)
            {
                return;
            }

            int r = 0;
            int c = 0;
            int count = 0;
            //foreach (object s in data2)
            for(int i = 0; i < arrayToPaste.Length; i++)
            {
                CellAddress ca = new CellAddress(r + curRow, c + curCol);
                if (MemberTag.IsIntersectionInvalid(ca))
                {
                    arrayToPaste[r, c] = String.Empty;
                    objArrayToPaste[r, c] = String.Empty;
                }
                else
                {
                    arrayToPaste[r, c] = data2[count].ToString();
                    objArrayToPaste[r, c] = data2[count];
                    count++;
                }

                //arrayToPaste[r, c] = s.ToString();
                //objArrayToPaste[r, c] = s;
                switch (viewAxis)
                {
                    case ViewAxis.Col:
                    {
                        r++;
                        break;
                    }
                    case ViewAxis.Row:
                    {
                        c++;
                        break;
                    }
                }
            }

            //set the Range.BottomRight.
            switch (viewAxis)
            {
                case ViewAxis.Col:
                {
                    range.BottomRight = new CellAddress(curRow + arrayToPaste.Length - 1, curCol);
                    break;
                }
                case ViewAxis.Row:
                {
                    range.BottomRight = new CellAddress(curRow, curCol + arrayToPaste.Length - 1);
                    break;
                }
            }

            bool hasNonNullValues = arrayToPaste.ContainsNonNullValues();

            //set the text (Hyperlinks are added below).
            if (hasNonNullValues)
            {
                switch (mbrTagDef.type)
                {
                    case memberTagType.TEXT:
                        _grid.SetValueTextArray(range, arrayToPaste);
                        //_Grid.SetValueObject(range, objArrayToPaste);
                        break;
                    case memberTagType.FORMULA:
                        _grid.SetValueObject(range, objArrayToPaste);
                        break;
                }
            }

            List<Cell> rng = new List<Cell>(); 
            r = 0;
            c = 0;
            foreach(Cell cell in range.Cells)
            {
                if (MemberTag.IsIntersectionInvalid(cell.CellAddress))
                {
                    cell.Value = String.Empty;
                    rng.Add(cell);
                }
                else
                {
                    if (!String.IsNullOrEmpty(arrayToPaste[r, c]))
                    {
                        //add the hyperlinks.
                        if (mbrTagDef.type == memberTagType.HYPERLINK)
                        {
                            _grid.SetHyperlinks(new Range(cell.CellAddress),
                                arrayToPaste[r, c], String.Empty, String.Empty, arrayToPaste[r, c]);
                        }
                        cell.Value = arrayToPaste[r, c];
                        rng.Add(cell);
                    }
                    else
                    {
                        rng.Add(cell);
                    }
                }

                 switch (viewAxis)
                 {
                     case ViewAxis.Col:
                     {
                         r++;
                         break;
                     }
                     case ViewAxis.Row:
                     {
                         c++;
                         break;
                     }
                 }
            }

            //cache the ranges of the member tag cells.
            if (MemberTag.MemberTagRanges.ContainsKey2(mbrTagName, blankLocation))
            {
                MemberTag.MemberTagRanges.RemoveSelection(mbrTagName, blankLocation);
            }
            else
            {
                MemberTag.MemberTagRanges.CacheAddSelections(mbrTagName, blankLocation, new ContiguousRange[] { range }, true);
                if (MemberTag.BuildMemberTagIntersections(mbrTagName, rng))
                {
                    PafApp.GetViewMngr().MakeAllMemberTagsDirtyExcept(0);
                }
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("PopulateMbrTagData, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Populates the changed member tag data in the grid.
        /// </summary>
        /// <param name="pafViewSection">pafViewSection that contains the member tag data.</param>
        public void OverlayChangedMemberTags(pafViewSection pafViewSection)
        {
            string mbrTagName;
            Dictionary<Intersection, string> values;

            //TTN-1137, the view only has comment member tags.
            if(MemberTag.ColMbrTagBlanks.Count == 0)
            {
                foreach(KeyValuePair<string, List<string>> kvp in MemberTag.ColCommentMemberTags)
                {
                    foreach (string comment in kvp.Value)
                    {
                        memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(comment);
                        mbrTagName = tagDef.name;

                        //server modified member tags.
                        values = PafApp.GetMbrTagMngr().MbrTagCache.ServerModifiedMemberTags(mbrTagName);
                        OverlayChangedMemberTags(tagDef, values);

                        //changed values.
                        values = PafApp.GetMbrTagMngr().MbrTagCache.GetAllValues(mbrTagName);
                        OverlayChangedMemberTags(tagDef, values);
                    }
                }
            }
            else //the view has expanded and maybe comment member tags.
            {
                //populate mbr tags.
                foreach (KeyValuePair<int, string> kvp in MemberTag.ColMbrTagBlanks)
                {
                    memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(kvp.Value);
                    mbrTagName = tagDef.name;

                    //server modified member tags.
                    values = PafApp.GetMbrTagMngr().MbrTagCache.ServerModifiedMemberTags(mbrTagName);
                    OverlayChangedMemberTags(tagDef, values);

                    //changed values.
                    values = PafApp.GetMbrTagMngr().MbrTagCache.GetAllValues(mbrTagName);
                    OverlayChangedMemberTags(tagDef, values);
                }
            }


            //TTN-1137, the view only has comment member tags.
            if (MemberTag.RowMbrTagBlanks.Count == 0)
            {
                foreach (KeyValuePair<string, List<string>> kvp in MemberTag.RowCommentMemberTags)
                {
                    foreach (string comment in kvp.Value)
                    {
                        memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(comment);
                        mbrTagName = tagDef.name;

                        //server modified member tags.
                        values = PafApp.GetMbrTagMngr().MbrTagCache.ServerModifiedMemberTags(mbrTagName);
                        OverlayChangedMemberTags(tagDef, values);

                        //changed values.
                        values = PafApp.GetMbrTagMngr().MbrTagCache.GetAllValues(mbrTagName);
                        OverlayChangedMemberTags(tagDef, values);
                    }
                }
            }
            else //the view has expanded and maybe comment member tags.
            {
                //populate mbr tags.
                foreach (KeyValuePair<int, string> kvp in MemberTag.RowMbrTagBlanks)
                {
                    memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(kvp.Value);
                    mbrTagName = tagDef.name;

                    //server modified member tags.
                    values = PafApp.GetMbrTagMngr().MbrTagCache.ServerModifiedMemberTags(mbrTagName);
                    OverlayChangedMemberTags(tagDef, values);

                    //changed values.
                    values = PafApp.GetMbrTagMngr().MbrTagCache.GetAllValues(mbrTagName);
                    OverlayChangedMemberTags(tagDef, values);
                }
            }
        }

        /// <summary>
        /// Populates the changed member tag data in the grid.
        /// </summary>
        /// <param name="tagDef">Member tag defination.</param>
        /// <param name="values">Values to update.</param>
        private void OverlayChangedMemberTags(memberTagDef tagDef, ICollection<KeyValuePair<Intersection, string>> values)
        {
            string mbrTagName = tagDef.name;
            //Dictionary<Intersection, string> values = PafApp.GetMbrTagMngr().MbrTagCache.GetAllValues(mbrTagName);

            if (values != null && values.Count > 0)
            {
                foreach (KeyValuePair<Intersection, string> kvp in values)
                {
                    List<CellAddress> ca = MemberTag.FindMemberTagIntersectionAddress(kvp.Key, mbrTagName);
                    if (ca != null && ca.Count > 0)
                    {
                        List<Range> range = new List<Range>();
                        foreach (CellAddress cell in ca)
                        {
                            //add a new hyperlink
                            switch (tagDef.type)
                            {
                                case memberTagType.HYPERLINK:
                                    _grid.SetHyperlinks(new Range(cell), kvp.Value, String.Empty, String.Empty, kvp.Value);
                                    break;
                            }
                            range.Add(new Range(cell));
                        }
                        //add the other stuff.
                        switch(tagDef.type)
                        {
                            case memberTagType.FORMULA:
                                _grid.SetValueObject(range, kvp.Value);
                                break;
                            case memberTagType.TEXT:
                                _grid.SetValueTextRange(range, kvp.Value);
                                break;
                        }
                    }
                    if (MemberTag.ColCommentMemberTags.Count > 0)
                    {
                        MemberTag.AddUpdateDeleteCommentMemberTags(mbrTagName, kvp.Key, kvp.Value.Trim());
                    }
                    if (MemberTag.RowCommentMemberTags.Count > 0)
                    {
                        MemberTag.AddUpdateDeleteCommentMemberTags(mbrTagName, kvp.Key, kvp.Value.Trim());
                    }
                }
            }
        }

        /// <summary>
        /// Clears out the current sort data.
        /// </summary>
        public void ClearSortData()
        {
            if (SortSelections != null)
            {
                SortSelections.Clear();
            }

            if (SortByList != null)
            {
                SortByList.Clear();
            }

            if (SortPositions != null)
            {
                SortPositions.Clear();
            }

            IsSorted = false;

            if (CurrentSortOrder != null)
            {
                CurrentSortOrder.Clear();
            }

            SortRange = null;
        }

        /// <summary>
        /// Clear all the member tag data.
        /// </summary>
        public void ClearMbrTagData()
        {
            List<Range> ranges = new List<Range>();
            string blankMember = Settings.Default.Blank;

            //Row Blanks
            foreach (int position in _rowBlanks)
            {
                if (MemberTag.RowMbrTagBlanks.ContainsKey(position))
                {
                    Tuple memberTuple = new Tuple(position, ViewAxis.Row, blankMember);
                    Range memberRange = FindTupleDataRange(memberTuple, ViewAxis.Row);
                    ranges.Add(memberRange);
                }
            }

            //Column Blanks
            foreach (int position in _colBlanks)
            {
                if (MemberTag.ColMbrTagBlanks.ContainsKey(position))
                {
                    Tuple memberTuple = new Tuple(position, ViewAxis.Col, blankMember);
                    Range memberRange = FindTupleDataRange(memberTuple, ViewAxis.Col);
                    ranges.Add(memberRange);
                }
            }

            //Format the data ranges for the Blanks
            if (ranges.Count > 0)
            {
                Grid.SetBlankFormats(ranges, false);
            }
        }

        /// <summary>
        /// This method will return the Range for a given tuple
        /// </summary>
        /// <param name="tuple"></param>
        /// <returns>A range object.</returns>
        /// <remarks>updated for aliases.</remarks>
        public Range FindTupleMemberRange(Tuple tuple)
        {
            int curRow;
            int curCol;
            Range range1 = new Range();

            if (tuple.ViewAxis == ViewAxis.Row)
            {
                //modified for aliases
                //CurRow = this.StartCell.Row + _ColDims.Count + tuple.Position;
                curRow = StartCell.Row + _colDims.Count + tuple.Position + ColAliases;

                curCol = StartCell.Col;
                CellAddress topLeft = new CellAddress(curRow, curCol);

                //modified for aliases
                //CurRow = this.StartCell.Row + _ColDims.Count + tuple.Position;
                curRow = StartCell.Row + _colDims.Count + tuple.Position + ColAliases;

                //modified for aliases
                //CurCol = this.StartCell.Col + _RowDims.Count - 1;
                curCol = StartCell.Col + _rowDims.Count - 1 + RowAliases;

                CellAddress bottomRight = new CellAddress(curRow, curCol);

                ContiguousRange contigRange = new ContiguousRange(topLeft, bottomRight);
                range1.AddContiguousRange(contigRange);
            }
            //Find the location of a column tuple
            else if (tuple.ViewAxis == ViewAxis.Col)
            {
                curRow = StartCell.Row;

                //modified for aliases
                //CurCol = this.StartCell.Col + _RowDims.Count + tuple.Position;
                curCol = StartCell.Col + _rowDims.Count + tuple.Position + RowAliases;
                CellAddress topLeft = new CellAddress(curRow, curCol);

                //modified for aliases
                //CurRow = this.StartCell.Row + _ColDims.Count - 1;
                curRow = StartCell.Row + _colDims.Count - 1 + ColAliases;

                //modified for aliases
                //CurCol = this.StartCell.Col + _RowDims.Count + tuple.Position;
                curCol = StartCell.Col + _rowDims.Count + tuple.Position + RowAliases;
                CellAddress bottomRight = new CellAddress(curRow, curCol);

                ContiguousRange contigRange = new ContiguousRange(topLeft, bottomRight);
                range1.AddContiguousRange(contigRange);
            }

            return range1;
        }

        /// <summary>
        /// Gets the Dimension assoctiated to a member name.
        /// </summary>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public string GetMemberDimName(string memberName)
        {
            foreach (KeyValuePair<string, List<string>> dim in _colDims.Where(dim => dim.Value.Contains(memberName)))
            {
                return dim.Key;
            }

            return (from dim in _rowDims where dim.Value.Contains(memberName) select dim.Key).FirstOrDefault();
        }

        /// <summary>
        /// Gets the header member name for the Cell selection.
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public string GetHeaderMemberName(Cell cell)
        {
            String value = String.Empty;
            if(_rowRange.Cells.Contains(cell))
            {
                if (MemberTag.RowTupleBorderValues.TryGetValue(cell.CellAddress, out value))
                {
                    return value;
                }
            }
            else if(_colRange.Cells.Contains(cell))
            {
                if(MemberTag.ColTupleBorderValues.TryGetValue(cell.CellAddress, out value))
                {
                    return value;
                }
            }

            return value;
        }

        /// <summary>
        /// This method will return the Data Range for a given tuple.
        /// </summary>
        /// <param name="tuple">The tuple - must have position property set</param>
        /// <returns></returns>
        public Range FindTupleDataRange(Tuple tuple)
        {
            ViewAxis tupleAxis = FindMemberAxis(tuple.Members[0]);
            return FindTupleDataRange(tuple, tupleAxis);
        }

        /// <summary>
        /// This method will return the Data Range for a given tuple.
        /// </summary>
        /// <param name="tuple">The tuple - must have position property set</param>
        /// <param name="tupleAxis">Identifies whether the Tuple is on the Row or Column.  Used for PAFBlanks</param>
        /// <returns>A range object.</returns>
        public Range FindTupleDataRange(Tuple tuple, ViewAxis tupleAxis)
        {
            //DateTime startTime = DateTime.Now;
            int curRow;
            int curCol;
            Range range1 = new Range();
            
            //Find the location of a row tuple
            if (tupleAxis == ViewAxis.Row)
            {
                curRow = RowOffset + tuple.Position;
                curCol = ColOffset;
                CellAddress topLeft = new CellAddress(curRow, curCol);

                string firstColDim = GetFirstColDimension();
                List<string> colMbrList;
                if (!String.IsNullOrEmpty(firstColDim))
                {
                    colMbrList = _colDims[firstColDim];
                }
                else
                {
                    colMbrList = new List<string>();
                }

                //modified for aliases, and suppress zeros
                if (colMbrList != null && colMbrList.Count == 0)
                {
                    curCol = ColOffset + colMbrList.Count;
                }
                else if (colMbrList != null && colMbrList.Count > 0)
                {
                    curCol = ColOffset + colMbrList.Count - 1;
                }


                CellAddress bottomRight = new CellAddress(curRow, curCol);

                ContiguousRange contigRange = new ContiguousRange(topLeft, bottomRight);
                range1.AddContiguousRange(contigRange);
            }
            //Find the location of a column tuple
            else if (tupleAxis == ViewAxis.Col)
            {
                curRow = RowOffset;
                curCol = ColOffset + tuple.Position;
                CellAddress topLeft = new CellAddress(curRow, curCol);


                string firstRowDim = GetFirstRowDimension();
                List<string> rowMbrList;
                if (!String.IsNullOrEmpty(firstRowDim))
                {
                    rowMbrList = _rowDims[firstRowDim];
                }
                else
                {
                    rowMbrList = new List<string>();
                }

                //modified for aliases, and suppress zeros
                if (rowMbrList != null && rowMbrList.Count == 0)
                {
                    curRow = RowOffset + rowMbrList.Count;
                }
                else if (rowMbrList != null && rowMbrList.Count > 0)
                {
                    curRow = RowOffset + rowMbrList.Count - 1;
                }

                curCol = ColOffset + tuple.Position;
                CellAddress bottomRight = new CellAddress(curRow, curCol);

                ContiguousRange contigRange = new ContiguousRange(topLeft, bottomRight);
                range1.AddContiguousRange(contigRange);
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("FindTupleDataRange, runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            return range1;
        }

        /// <summary>
        /// This method will return the Data Range for a given conditional tuple.
        /// </summary>
        /// <param name="tuple">The tuple - must have position property set</param>
        /// <param name="tupleAxis">Identifies whether the Tuple is on the Row or Column.  Used for PAFBlanks</param>
        /// <param name="format"></param>
        /// <returns>A range object.</returns>
        public Range FindTupleDataRangeCondFormat(ViewAxis tupleAxis, Tuple tuple, conditionalFormat format)
        {
            //DateTime startTime = DateTime.Now;
            int curRow;
            int curCol;
            Range range = new Range();

            //Find the location of a row tuple
            if (tupleAxis == ViewAxis.Row)
            {
                curRow = RowOffset + tuple.Position;
                curCol = ColOffset;
                CellAddress topLeft = new CellAddress(curRow, curCol);

                if (format.secondaryDimensions != null && SecondaryConditionalTuples.Count > 0)
                {
                    int colMbrCnt = GetAxisMemberCount(ViewAxis.Col);

                    Range colRange = new Range();
                    for (int i = 0; i < colMbrCnt; i++)
                    {
                        int mbrPos = i;
                        string member = String.Concat(ColDims.Keys.Select(c => ColDims[c][mbrPos]));

                        if (SecondaryConditionalTuples.Contains(member))
                        {
                            colRange.AddContiguousRange(new CellAddress(curRow, ColOffset + mbrPos));
                        }
                    }

                    range.AddContiguousRanges(colRange.ContiguousRanges);
                }
                else
                {
                    range.AddContiguousRange(new ContiguousRange(topLeft, new CellAddress(curRow, ColOffset + GetAxisMemberCount(ViewAxis.Col) - 1)));
                }
            }
            //Find the location of a column tuple
            else if (tupleAxis == ViewAxis.Col)
            {
                curRow = RowOffset;
                curCol = ColOffset + tuple.Position;
                CellAddress topLeft = new CellAddress(curRow, curCol);

                if (format.secondaryDimensions != null && SecondaryConditionalTuples.Count > 0)
                {
                    int colMbrCnt = GetAxisMemberCount(ViewAxis.Row);

                    Range rowRange = new Range();
                    for (int i = 0; i < colMbrCnt; i++)
                    {
                        int mbrPos = i;
                        string member = String.Concat(RowDims.Keys.Select(r => RowDims[r][mbrPos]));

                        if (SecondaryConditionalTuples.Contains(member))
                        {
                            rowRange.AddContiguousRange(new CellAddress(RowOffset + mbrPos, curCol));
                        }
                    }

                    range.AddContiguousRanges(rowRange.ContiguousRanges);
                }
                else
                {
                    range.AddContiguousRange(new ContiguousRange(topLeft, new CellAddress(RowOffset + GetAxisMemberCount(ViewAxis.Row) - 1, curCol)));
                }
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("FindTupleDataRangeCondFormat, runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            return range;
        }

        /// <summary>
        /// Expands or collapses the grouping of a cell.
        /// </summary>
        /// <param name="viewAxis"></param>
        /// <param name="cell"></param>
        public bool ExpandCellGroup(ViewAxis viewAxis, CellAddress cell)
        {
            if (cell == null) return false;
            Stopwatch stopwatch = Stopwatch.StartNew();
            switch (viewAxis)
            {
                case ViewAxis.Row:
                    if (RowGroupSpec != null && RowGroupSpec.Range == cell.Col)
                    {
                        return _grid.ExpandRowGrouping(cell);
                    }
                    break;
                case ViewAxis.Col:
                    if (ColGroupSpec != null && ColGroupSpec.Range == cell.Row)
                    {
                        return _grid.ExpandColumnGrouping(cell);
                    }
                    break;
            }
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ExpandCellGroup, Axis: {0}, Cell: {1}, Runtime: {2} (ms)", viewAxis, cell, stopwatch.ElapsedMilliseconds.ToString("N0"));
            return false;
        }

        public void ExpandGroups(bool applyRowGroups = true, bool applyColumnGroups = true)
        {
            if (applyRowGroups && RowGroupSpec != null && RowGroupSpec.GroupsApplied.GetValueOrDefault())
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                int? row = RowGroupSpec.GetExcelGroupingExpansionLevel();
                if (!row.HasValue)
                {
                    PafApp.GetLogger().WarnFormat("Axis: Row, Type: {0}, Number: {1} not found.  All row groups will be expanded. ", RowGroupSpec.Type, RowGroupSpec.DefaultLevel);
                }
                else
                {
                    List<int> excelLevels = RowGroupSpec.ExcelGroupLevelMap.Values.OrderByDescending(x => x).ToList();
                    foreach (int level in excelLevels)
                    {
                        _grid.SetGroupingLevels(level, null);
                    }
                    _grid.SetGroupingLevels(row, null);
                }
                stopwatch.Stop();
                PafApp.GetLogger().InfoFormat("ExpandGroups, Axis: {0}, Dimension: {1}, Runtime: {2} (ms)", RowGroupSpec.ApplyGroups, RowGroupSpec.DimensionName, stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
            if (applyColumnGroups && ColGroupSpec != null && ColGroupSpec.GroupsApplied.GetValueOrDefault())
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                int? col = ColGroupSpec.GetExcelGroupingExpansionLevel();
                if (!col.HasValue)
                {
                    PafApp.GetLogger().WarnFormat("Axis: Column, Type: {0}, Number: {1} not found.  All column groups will be expanded. ", ColGroupSpec.Type, ColGroupSpec.DefaultLevel);
                }
                else
                {
                    List<int> excelLevels = ColGroupSpec.ExcelGroupLevelMap.Values.OrderByDescending(x => x).ToList();
                    foreach (int level in excelLevels)
                    {
                        _grid.SetGroupingLevels(null, level);
                    }
                    _grid.SetGroupingLevels(null, col);
                }
                stopwatch.Stop();
                PafApp.GetLogger().InfoFormat("ExpandGroups, Axis: {0}, Dimension: {1}, Runtime: {2} (ms)", ColGroupSpec.ApplyGroups, ColGroupSpec.DimensionName, stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mbrName"></param>
        /// <returns></returns>
        public List<Intersection> FindMemberIntersections(string mbrName)
        {
            List<Intersection> intersections = new List<Intersection>();
            List<int> indices = new List<int>();
            List<string> mbrList;
            ViewAxis viewAxis = ViewAxis.Unknown;
            bool blankFlag = false;
            string blankMember = Settings.Default.Blank;

            foreach (string dimNameKey in _colDims.Keys)
            {
                mbrList = _colDims[dimNameKey];
                if (mbrList.Contains(mbrName))
                {
                    for (int i = 0; i < mbrList.Count; i++)
                    {
                        if(mbrList[i].Equals(mbrName))
                        {
                            indices.Add(i);
                        }
                    }
                    viewAxis = ViewAxis.Col;
                }
            }

            if (viewAxis != ViewAxis.Col)
            {
                foreach (string dimNameKey in _rowDims.Keys)
                {
                    mbrList = _rowDims[dimNameKey];
                    if (mbrList.Contains(mbrName))
                    {
                        for (int i = 0; i < mbrList.Count; i++)
                        {
                            if(mbrList[i].Equals(mbrName))
                            {
                                indices.Add(i);
                            }
                        }
                        viewAxis = ViewAxis.Row;
                    }
                }
            }

            if (indices.Count > 0)
            {
                for (int i = 0; i < indices.Count; i++)
                {
                    if(viewAxis == ViewAxis.Col)
                    {
                        for (int j = 0; j < _rowDims[GetFirstRowDimension()].Count; j++)
                        {
                            Intersection intersection = new Intersection(DimensionPriority);

                            foreach (string rowDimNameKey in _rowDims.Keys)
                            {
                                mbrList = _rowDims[rowDimNameKey];
                                if (mbrList[j].ToUpper() == blankMember ||
                                    MemberTag.RowMbrTagBlanks.ContainsValue(mbrList[j]))
                                {
                                    blankFlag = true;
                                    break;
                                }
                                intersection.SetCoordinate(rowDimNameKey, mbrList[j]);
                            }

                            if (blankFlag)
                            {
                                blankFlag = false;
                                continue;
                            }

                            foreach (string colDimNamekey in _colDims.Keys)
                            {
                                mbrList = _colDims[colDimNamekey];
                                if (mbrList[indices[i]].ToUpper() == blankMember ||
                                    MemberTag.ColMbrTagBlanks.ContainsValue(mbrList[indices[i]]))
                                {
                                    blankFlag = true;
                                    break;
                                }
                                intersection.SetCoordinate(colDimNamekey, mbrList[indices[i]]);
                            }

                            if (blankFlag)
                            {
                                blankFlag = false;
                                continue;
                            }

                            foreach (string pageDimNamekey in _pageDims.Keys)
                            {
                                intersection.SetCoordinate(pageDimNamekey, _pageDims[pageDimNamekey]);
                            }

                            intersections.Add(intersection);
                        }
                    }
                    else if (viewAxis == ViewAxis.Row)
                    {
                        for (int j = 0; j < _colDims[GetFirstColDimension()].Count; j++)
                        {
                            Intersection intersection = new Intersection(DimensionPriority);

                            foreach (string colDimNamekey in _colDims.Keys)
                            {
                                mbrList = _colDims[colDimNamekey];
                                if (mbrList[j].ToUpper() == blankMember ||
                                    MemberTag.ColMbrTagBlanks.ContainsValue(mbrList[j]))
                                {
                                    blankFlag = true;
                                    break;
                                }
                                intersection.SetCoordinate(colDimNamekey, mbrList[j]);
                            }

                            if (blankFlag)
                            {
                                blankFlag = false;
                                continue;
                            }

                            foreach (string rowDimNameKey in _rowDims.Keys)
                            {
                                mbrList = _rowDims[rowDimNameKey];
                                if (mbrList[indices[i]].ToUpper() == blankMember ||
                                    MemberTag.RowMbrTagBlanks.ContainsValue(mbrList[indices[i]]))
                                {
                                    blankFlag = true;
                                    break;
                                }
                                intersection.SetCoordinate(rowDimNameKey, mbrList[indices[i]]);
                            }

                            if (blankFlag)
                            {
                                blankFlag = false;
                                continue;
                            }

                            foreach (string pageDimNamekey in _pageDims.Keys)
                            {
                                intersection.SetCoordinate(pageDimNamekey, _pageDims[pageDimNamekey]);
                            }

                            intersections.Add(intersection);
                        }
                    }
                }
            }
            return intersections;
        }

        /// <summary>
        /// Gets the axis of a specified dimension.
        /// </summary>
        /// <param name="dimName"></param>
        /// <returns></returns>
        public ViewAxis FindDimensionAxis(string dimName)
        {
            if (RowDims.ContainsKey(dimName))
            {
                return ViewAxis.Row;
            }
            if (ColDims.ContainsKey(dimName))
            {
                return ViewAxis.Col;
            }
            if (PageDims.ContainsKey(dimName))
            {
                return ViewAxis.Page;
            }
            return ViewAxis.Unknown;
        }

        /// <summary>
        /// Finds the Axis from a name of a member.
        /// </summary>
        /// <param name="mbrName">name of the member.</param>
        /// <returns>the axis</returns>
        public ViewAxis FindMemberAxis(string mbrName)
        {
            if (ViewMembers.ContainsKey(mbrName))
            {
                switch (ViewMembers[mbrName])
                {
                    case "Row":
                        return ViewAxis.Row;
                    case "Col":
                        return ViewAxis.Col;
                    case "Page":
                        return ViewAxis.Page;   
                }
            }

            return ViewAxis.Unknown;
        }

        /// <summary>
        /// Finds an cell address from a cell address.
        /// </summary>
        /// <param name="inter">Intersection to find.</param>
        /// <returns>A cellAddress.</returns>
        /// <remarks>Updated for Aliases</remarks>
        public List<CellAddress> FindIntersectionAddress(Intersection inter)
        {
            //
            if (_pageDims == null || _rowDims == null || _colDims == null)
            {
                return null;
            }

            if (inter.AxisSequence.Length != 
                (_pageDims.Keys.Count + _rowDims.Keys.Count + _colDims.Keys.Count))
            {
                return null;
            }
            else
            {
                return FindIntersectionAddress2(inter);
            }
        }

        /// <summary>
        /// Finds an cell address from a cell address.
        /// </summary>
        /// <param name="inter">Intersection to find.</param>
        /// <returns>A cellAddress.</returns>
        /// <remarks>Updated for Aliases</remarks>
        private List<CellAddress> FindIntersectionAddress2(Intersection inter)
        {
            //List<CellAddress> val;
            //if(_intersectionAddressHash.TryGetValue(inter, out val))
            //{
            //    return val;
            //}

            if(inter == null || inter.AxisSequence.Length == 0 || inter.Coordinates.Length == 0)
            {
                return null;
            }

            //its a member tag.
            if (inter.AxisSequence.Length < DimensionCount)
            {
                string memberTagName = PafApp.GetMemberTagInfo().GetMemberTagName(inter.AxisSequence);
                if(memberTagName != String.Empty)
                {
                    return MemberTag.FindMemberTagIntersectionAddress(inter, memberTagName);
                }
                else
                {
                    return null;
                }
            }

            List<string> interDims = new List<string>();
            if(inter.AxisSequence.Length > 0)
            {
                interDims.AddRange(inter.AxisSequence);
            }

            //Page - verify the page members
            bool pageFound = true;
            foreach (string pageDim in _pageDims.Keys)
            {
                if (interDims.Contains(pageDim))
                {
                    string mbrName = inter.GetCoordinate(pageDim);
                    string pageMember = _pageDims[pageDim];

                    if (mbrName != pageMember)
                    {
                        pageFound = false;
                        break;
                    }
                }
            }

            if (!pageFound) return null;

            //Rows
            //Hash together member names into a string
            StringBuilder mbrNameHash = new StringBuilder();
            bool rowFound = false;
            foreach (string rowDim in _rowDims.Keys)
            {
                if (interDims.Contains(rowDim))
                {
                    mbrNameHash.Append(inter.GetCoordinate(rowDim));
                }
            }

            //Build list of positions where the Tuple occurs
            List<int> rowPositions = new List<int>();
            if (RowDimHash.ContainsKey(mbrNameHash.ToString()))
            {
                foreach (int position in RowDimHash[mbrNameHash.ToString()])
                {
                    //add 1 because the MemberList function is 0 based, but the grid location is 1 based
                    rowPositions.Add(position + 1);
                }
                rowFound = true;
            }

            if (!rowFound) return null;

            //Columns
            //Hash together member names into a string
            mbrNameHash = new StringBuilder();
            bool colFound = false;
            foreach (string colDim in _colDims.Keys)
            {
                if (interDims.Contains(colDim))
                {
                    mbrNameHash.Append(inter.GetCoordinate(colDim));
                }
            }

            //Build list of positions where the Tuple occurs
            List<int> colPositions = new List<int>();
            if (ColDimHash.ContainsKey(mbrNameHash.ToString()))
            {
                foreach (int position in ColDimHash[mbrNameHash.ToString()])
                {
                    //add 1 because the MemberList function is 0 based, but the grid location is 1 based
                    colPositions.Add(position + 1);
                }
                colFound = true;
            }

            if (!colFound) return null;

            List<CellAddress> cellAddresses = new List<CellAddress>(rowPositions.Count * colPositions.Count);
            int r = StartCell.Row + _colDims.Count - 1 + ColAliases;
            int c = StartCell.Col + _rowDims.Count - 1 + RowAliases;
            foreach (int rowPosition in rowPositions)
            {
                int rPos = rowPosition + r;
                foreach (int colPosition in colPositions)
                {
                    cellAddresses.Add(new CellAddress(rPos, c + colPosition));
                }
            }

            //_intersectionAddressHash.Add(inter, cellAddresses);

            return cellAddresses;
        }

        /// <summary>
        /// Finds an intersection from a cell address.
        /// </summary>
        /// <param name="addr">The cell address to find.</param>
        /// <returns>An intersection object.</returns>
        /// <remarks>Updated for aliases.</remarks>
		public Intersection FindIntersection(CellAddress addr)
		{
            memberTagDef tagDef;
            if (MemberTag.IsMemberTag(addr, out tagDef))
            {
                return MemberTag.FindMemberTagIntersection(addr);
            }

            //Intersection val;
            //if (_addressIntersectionHash.TryGetValue(addr, out val))
            //{
            //    return val;
            //}

            int rowMbrPos = addr.Row - StartCell.Row - ColDims.Count - ColAliases;
            if (rowMbrPos < 0) return null;

            int colMbrPos = addr.Col - StartCell.Col - RowDims.Count - RowAliases;
            if (colMbrPos < 0) return null;

            Intersection intersection = new Intersection(DimensionPriority);
            int intersectionAxisSequenceLength = intersection.AxisSequence.Length;


            foreach (string rowDim in _rowDims.Keys)
            {
                intersection.SetCoordinate(rowDim, _rowDims[rowDim][rowMbrPos]);
            }

			foreach (string colDim in _colDims.Keys)
			{
                intersection.SetCoordinate(colDim, _colDims[colDim][colMbrPos]);
			}

            //Add Page Members
            for (int j = 0; j < intersectionAxisSequenceLength; j++)
            {
                foreach (string key in _pageDims.Keys)
                {
                    if (intersection.AxisSequence[j] == key)
                    {
                        //intersection.SetCoordinate(key, _pageDims[key]);
                        intersection.Coordinates[j] = _pageDims[key];
                        break;
                    }
                }
            }

            //_addressIntersectionHash.Add(addr, intersection);

            return intersection;
		}

        /// <summary>
        /// Adjusts a cell address for start location, alias, and suppress zero
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public Cell AdjustCellAddress(CellAddress cell)
        {
            return new Cell(cell.Row - RowOffset, cell.Col - ColOffset);
        }

        /// <summary>
        /// Adjusts a cell address for start location, alias, and suppress zero
        /// </summary>
        /// <param name="cell"></param>
        /// <returns></returns>
        public Cell AdjustCellAddress(Cell cell)
        {
            return AdjustCellAddress(cell.CellAddress);
        }

        /// <summary>
        /// Finds an intersection from a cell address.
        /// </summary>
        /// <param name="addr">The cell address to find.</param>
        /// <returns>An intersection object.</returns>
        /// <remarks>Updated for aliases.</remarks>
        public Coordinate FindCoordinate(CellAddress addr, out string[] dimensions)
        {
            memberTagDef tagDef;
            List<string> dims = new List<string>(DimensionPriority);
            dimensions = dims.ToArray();

            if (MemberTag.IsMemberTag(addr, out tagDef))
            {
                return new Coordinate((MemberTag.FindMemberTagIntersection(addr)).Coordinates);
            }

            Coordinate intersection = new Coordinate(DimensionPriority.Length);
            
            int startCellRow = StartCell.Row;
            int startCellCol = StartCell.Col;

            int colDimsCount = ColDims.Count; //added for performance based on profiler
            int rowDimsCount = RowDims.Count; //added for performance based on profiler
            int intersectionAxisSequenceLength = DimensionPriority.Length; //added for performance based on profiler

            foreach (string rowDim in _rowDims.Keys)
            {
                intersection.SetCoordinate(dims.IndexOf(rowDim), _rowDims[rowDim][(addr.Row - startCellRow - colDimsCount - ColAliases)]);
            }

            foreach (string colDim in _colDims.Keys)
            {
                intersection.SetCoordinate(dims.IndexOf(colDim), _colDims[colDim][(addr.Col - startCellCol - rowDimsCount - RowAliases)]);
            }

            //Add Page Members
            for (int j = 0; j < intersectionAxisSequenceLength; j++)
            {
                foreach (string key in _pageDims.Keys)
                {
                    if (dims[j] == key)
                    {
                        intersection.SetCoordinate(dims.IndexOf(key), _pageDims[key]);
                        break;
                    }
                }
            }

            return intersection;
        }

        public Coordinate FindCoordinate2(CellAddress addr, out string[] dimensions)
        {
            Intersection temp = FindIntersection(addr);

            dimensions = temp.AxisSequence;
            return new Coordinate(temp.Coordinates);
        }
    
        /// <summary>
        /// Get the first column dimension listed.
        /// </summary>
        /// <returns>The string value representing the first column dimension.</returns>
        /// <remarks>
        /// I assume that each Dimension on an Axis has the same number of members. 
        /// I take the first Dimension arraylist for a dimension and get the count.
        /// </remarks>
        private string GetFirstColDimension()
        {
            if (_colDims.Count > 0)
            {
                IDictionaryEnumerator colEnum = _colDims.GetEnumerator();
                colEnum.MoveNext();
                return colEnum.Key.ToString();
            }
            return null;
        }


        /// <summary>
        /// Get the first row dimension listed.
        /// </summary>
        /// <returns>The string value representing the first row dimension.</returns>
        /// <remarks>
        /// I assume that each Dimension on an Axis has the same number of members.
        /// I take the first Dimension arraylist for a dimension and get the count.
        /// </remarks>
        private string GetFirstRowDimension()
        {
            if (_rowDims.Count > 0)
            {
                IDictionaryEnumerator rowEnum = _rowDims.GetEnumerator();
                rowEnum.MoveNext();
                return rowEnum.Key.ToString();
            }
            return String.Empty;
        }

        /// <summary>
        /// Gets the number of members for the specified axis.
        /// </summary>
        /// <param name="viewAxis"></param>
        /// <returns></returns>
        private int GetAxisMemberCount(ViewAxis viewAxis)
        {
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    return ColDims[GetFirstColDimension()].Count;
                case ViewAxis.Row:
                    return RowDims[GetFirstRowDimension()].Count;
                case ViewAxis.Page:
                    return PageDims.Count;
            }
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mergedCellFlag"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="TopLeft">The cell in the top left position.</param>
        /// <param name="countOfAlisMembers">The number of alias rows.</param>
        private void MergeColCells(ref bool mergedCellFlag, int row, int col,
            CellAddress TopLeft, int countOfAlisMembers)
        {
            //DateTime startTime = DateTime.Now;
            //Get the Cell Address for the last merged cell.
            if (mergedCellFlag)
            {
                mergedCellFlag = false;

                for (int k = 0; k < countOfAlisMembers; k++)
                {
                    int curRow = ColRange.TopLeft.Row + row + k;
                    int curCol = ColRange.TopLeft.Col + col - 1;
                    CellAddress bottomRight = new CellAddress(curRow, curCol);
                    //create a new top left to use so that the alias merging works correctly.
                    //CellAddress newTopLeft = new CellAddress(TopLeft.Row, TopLeft.Col + k);
                    //TTN-1184
                    CellAddress newTopLeft = new CellAddress(TopLeft.Row + k, TopLeft.Col);

                    //Build a Contiguous Range to store the Merged range.
                    ContiguousRange mergedCellRange = new ContiguousRange(newTopLeft, bottomRight);
                    if (!MergedCellRanges.ContainsKey(ViewAxis.Col))
                    {
                        List<ContiguousRange> contigRanges = new List<ContiguousRange>();
                        MergedCellRanges.Add(ViewAxis.Col, contigRanges);
                    }
                    MergedCellRanges[ViewAxis.Col].Add(mergedCellRange);
                }
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("MergeColCells, runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mergedCellFlag"></param>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="TopLeft">The cell in the top left position.</param>
        /// <param name="countOfAlisMembers">The number of alias rows.</param>
        private void MergeRowCells(ref bool mergedCellFlag, int row, int col, CellAddress TopLeft,
            int countOfAlisMembers)
        {
            //DateTime startTime = DateTime.Now;
            //Get the Cell Address for the last merged cell.
            if (mergedCellFlag)
            {
                mergedCellFlag = false;

                for(int k = 0; k < countOfAlisMembers; k++)
                {
                    int curRow = RowRange.TopLeft.Row + row - 1;
                    int curCol = RowRange.TopLeft.Col + col + k;
                    CellAddress bottomRight = new CellAddress(curRow, curCol);
                    //create a new top left to use so that the alias merging works correctly.
                    CellAddress newTopLeft = new CellAddress(TopLeft.Row, TopLeft.Col + k);

                    //Build a Contiguous Range to store the Merged range.
                    ContiguousRange mergedCellRange = new ContiguousRange(newTopLeft, bottomRight);
                    if (!MergedCellRanges.ContainsKey(ViewAxis.Row))
                    {
                        List<ContiguousRange> contigRanges = new List<ContiguousRange>();
                        MergedCellRanges.Add(ViewAxis.Row, contigRanges);
                    }
                    MergedCellRanges[ViewAxis.Row].Add(mergedCellRange);
                }
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("MergeRowCells, runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
        }
	}
}
