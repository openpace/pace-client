#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Titan.Pace;
using Titan.Pace.Application.Core;
using Titan.Pace.Application.Win32;
using Titan.Pace.ExcelGridView.Utility;

namespace Titan.Palladium.GridView
{
    /// <summary>
    /// The row and col intersection for a cell
    /// </summary>
    [Serializable]
    public class CellAddress : IDeepCloneable<CellAddress>, IEquatable<CellAddress>, IComparable<CellAddress>
    {
        private int _Row;
        private int _Col;
        private static readonly ColumnConverter Converter = new ColumnConverter();

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CellAddress()
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        [DebuggerHidden]
        public CellAddress(int row, int col)
        {
            _Row = row;
            _Col = col;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="address">R1C1 style address (the constructor will try and parse)</param>
        [DebuggerHidden]
        public CellAddress(string address)
        {
            int rPos = address.IndexOf('R');
            int cPos = address.IndexOf('C');

            if (rPos < cPos)
            {
                _Row = int.Parse(address.Substring(rPos + 1, cPos - 1));
                _Col = int.Parse(address.Substring(cPos + 1));
            }
            else
            {
                _Col = int.Parse(address.Substring(cPos + 1, rPos - 1));
                _Row = int.Parse(address.Substring(rPos + 1));
            }
        }

        /// <summary>
        /// Row index.
        /// </summary>
		public int Row
		{
            [DebuggerHidden]
			get { return _Row; }
            [DebuggerHidden]
            set { _Row = value; }
		}

        /// <summary>
        /// Column index.
        /// </summary>
		public int Col
		{
            [DebuggerHidden]
			get { return _Col; }
            [DebuggerHidden]
            set { _Col = value;}
		}

        /// <summary>
        /// Returns the column number as the associated Excel column letter.
        /// </summary>
        public string ColAsLetter
        {
            get { return Converter.GetColumnAlphabet(_Col); }
        }

        /// <summary>
        /// Implementation of "IEquatable".  
        /// </summary>
        /// <remarks>
        /// Without this implementation, when you use CellAddress as a key in a hash structure,
        /// the comparsion would always return false because of the way the default comparsion works.
        /// </remarks>
        /// <param name="cell">The cell that you want to compare.</param>
        /// <returns>True if the objects are equal, false if they are not.</returns>
        public bool Equals(CellAddress cell)
        {
            return cell.Row == _Row && cell.Col == _Col;
        }

        /// <summary>
        /// Implementation of "IComparable"
        /// </summary>
        /// <remarks>
        /// Without the implementation, the cells are not sorted correctly in a SortedList or SortDictionary.
        /// </remarks>
        /// <param name="cell">The cell to compare to the instance.</param>
        /// <returns>0 if equal, -1 if the instance is less than the cell, 1 if the instance
        /// is greater than the cell.</returns>
        public int CompareTo(CellAddress cell)
        {
            int rc;

            if (_Row < cell.Row)
            {
                rc = -1;
            }
            else if (_Row == cell.Row)
            {
                if (_Col < cell.Col)
                {
                    rc = -1;
                }
                else if (_Col == cell.Col)
                {
                    rc = 0;
                }
                else
                {
                    rc = 1;
                }
            }
            else
            {
                rc = 1;
            }

            return rc;
        }

        /// <summary>
        /// Implementation of "IComparable"
        /// </summary>
        /// <remarks>
        /// Without the implementation, the cells are not sorted correctly in a SortedList or SortDictionary.
        /// </remarks>
        /// <param name="cell">The cell to compare to the instance.</param>
        /// <returns>0 if equal, -1 if the instance is less than the cell, 1 if the instance
        /// is greater than the cell.</returns>
        public int CompareTo50(CellAddress cell)
        {
            //They are equal
            if (_Row == cell.Row && _Col == cell.Col) return 0;
            //Instance is less than.
            if (_Col < cell.Col) return -1;
            if (_Col > cell.Col) return 1;

            if (_Row < cell.Row) return -1;

            return 1;
        }

        /// <summary>
        /// Gets the R1C1 style address for the cell address.
        /// </summary>
        /// <returns></returns>
        public string R1C1Address
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder("R");
                return stringBuilder.Append(_Row).Append("C").Append(_Col).ToString();
            }
        }

        /// <summary>
        /// Gets the A1 style address for the cell address.
        /// </summary>
        /// <returns></returns>
        public string A1Address
        {
            get
            {
                StringBuilder stringBuilder = new StringBuilder(Converter.GetColumnAlphabet(_Col));
                return stringBuilder.Append(_Row).ToString();
            }
        }

        /// <summary>
        /// Converts this CellAddress to a new single cell ContiguousRange
        /// </summary>
        public ContiguousRange ToContiguousRange()
        {
            return new ContiguousRange(this);
        }

        /// <summary>
        /// Is a CellAddress contigous to a CellAddress 
        /// </summary>
        /// <param name="cellAddress">cellAddress to compare</param>
        /// <returns>1 if the range is on the right, 2 if the left, 3 if the botton, 4 if top.  -1 if they are not contiguous.</returns>
        public int IsContigous(CellAddress cellAddress)
        {
            bool topLeftRow = _Row == cellAddress.Row;
            bool bottomRightRow = _Row == cellAddress.Row;

            //Side by side (on Right)
            if (topLeftRow && bottomRightRow && _Col == (cellAddress.Col - 1))
            {
                return 1;
            }

            bool topLeftCol = _Col == cellAddress.Col;
            bool bottomRightCol = _Col == cellAddress.Col;
            //Stacked (Bottom).
            if (topLeftCol && bottomRightCol && _Row == (cellAddress.Row - 1))
            {
                return 3;
            }

            return -1;
        }

        ///// <summary>
        ///// Creates a range from a list of CellAddress
        ///// </summary>
        ///// <param name="cellAddresses"></param>
        ///// <returns></returns>
        //public static Range GetRangeOld(List<CellAddress> cellAddresses)
        //{
        //    Stopwatch stopwatch = Stopwatch.StartNew();
        //    PafApp.GetLogger().InfoFormat("Processing: {0} cells", new object[] { cellAddresses.Count.ToString("N0") });
        //    Range range = new Range();

        //    if (cellAddresses.Count == 0) return range;

        //    CellAddress lastCell = new CellAddress(cellAddresses[0].Row, cellAddresses[0].Col);
        //    ContiguousRange cr = new ContiguousRange(lastCell, lastCell.DeepClone());
        //    for (int i = 1; i < cellAddresses.Count; i++)
        //    {
        //        CellAddress lockCell = cellAddresses[i];
        //        if (lockCell == null) continue;

        //        CellAddress cellAddr = new CellAddress(lockCell.Row, lockCell.Col);
        //        int value = lastCell.IsContigous(cellAddr);
        //        //If the last and cureent cell are contigous,
        //        //then chceck to see if the current cell is contigous with the current contigous range.
        //        //if not roll off the range, and set it to null, so a new range is created.
        //        if (value > 0 && cr != null)
        //        {
        //            int pos = cr.IsContigousRange(cellAddr);
        //            if (pos == -1)
        //            {
        //                range.AddContiguousRange(cr);
        //                cr = null;
        //            }
        //        }
        //        //If the last and current range are contigous (1 or 3) then just adjust the BottomRight value of the 
        //        //current ContigousRange (create it if it does not exist).
        //        //If the value is not 1 or 3, then roll off the range and set it to null.
        //        switch (value)
        //        {
        //            case 1:
        //                if (cr == null) cr = new ContiguousRange(lastCell, lastCell.DeepClone());
        //                cr.BottomRight.Col = cellAddr.Col;
        //                break;
        //            case 3:
        //                if (cr == null) cr = new ContiguousRange(lastCell, lastCell.DeepClone());
        //                cr.BottomRight.Row = cellAddr.Row;
        //                break;
        //            default:
        //                if (cr == null) cr = new ContiguousRange(lastCell, lastCell.DeepClone());
        //                range.AddContiguousRange(cr);
        //                cr = null;
        //                break;
        //        }
        //        lastCell = cellAddr;
        //        //Don't forget the last cell.
        //        if (i == cellAddresses.Count - 1)
        //        {
        //            if (cr == null)
        //            {
        //                range.AddContiguousRange(new ContiguousRange(lastCell, lastCell.DeepClone()));
        //            }
        //            else
        //            {
        //                if (!cr.InContiguousRange(lastCell))
        //                {
        //                    cr = new ContiguousRange(lastCell, lastCell.DeepClone());
        //                }
        //            }
        //        }
        //    }
        //    if (cr != null)
        //    {
        //        range.AddContiguousRange(cr);
        //    }
        //    stopwatch.Stop();
        //    PafApp.GetLogger().InfoFormat("GetRangeOld runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        //    PafApp.GetLogger().InfoFormat("GetRangeOld cell count: {0}", range.CellCount.ToString("N0"));
        //    PafApp.GetLogger().InfoFormat("GetRangeOld contig range count: {0}", range.ContiguousRanges.Count.ToString("N0"));

        //    return range;
        //}

        /// <summary>
        /// Creates a range from a list of CellAddress
        /// </summary>
        /// <param name="cellAddresses"></param>
        /// <returns></returns>
        public static Range GetRange(List<CellAddress> cellAddresses)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            Range range = new Range();
            if (cellAddresses.Count == 0) return range;
            PafApp.GetLogger().InfoFormat("Processing: {0} cells", new object[] { cellAddresses.Count.ToString("N0") });

            CellAddress firstCell = new CellAddress(cellAddresses[0].Row, cellAddresses[0].Col);
            ContiguousRange cr = new ContiguousRange(firstCell, firstCell.DeepClone());
            for (int i = 1; i < cellAddresses.Count; i++)
            {
                CellAddress cell = cellAddresses[i];
                if (!cr.CombineRange(cell.ToContiguousRange()))
                {
                    range.AddContiguousRange(cr);
                    cr = new ContiguousRange(cell, cell.DeepClone());
                }
            }
            //Don't forget the last ContigRange
            if (cr.Cells.Count > 0)
            {
                range.AddContiguousRange(cr);
            }

            stopwatch.Stop();

            PafApp.GetLogger().InfoFormat("GetRange runtime: {0} (ms)",  stopwatch.ElapsedMilliseconds.ToString("N0"));
            PafApp.GetLogger().InfoFormat("GetRange cell count: {0}", range.CellCount.ToString("N0"));
            PafApp.GetLogger().InfoFormat("GetRange ContiguousRange count: {0}", range.ContiguousRanges.Count.ToString("N0"));
            return range;
        }

        /// <summary>
        /// Gets the hash code for the object.
        /// </summary>
        /// <returns>Returns the hash code for the object.</returns>
        public override int GetHashCode()
        {
            //int hash = 17;
            //hash = 29 * hash + Row.GetHashCode();
            //hash = 29 * hash + Col.GetHashCode();
            //return hash;
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Returns a System.String representation of the CellAddress.
        /// </summary>
        /// <returns>Returns a System.String representation of the CellAddress.</returns>
		public override string ToString()
		{
            return String.Format("({0},{1})", _Row, _Col);
		}

        /// <summary>
        /// Overloaded deep clone.
        /// </summary>
        /// <returns>A deep copied CellAddress.</returns>
        public CellAddress DeepClone()
        {
            return new CellAddress(_Row, _Col);
        }
    }

    internal class ColumnConverter
    {
        private static readonly string[] ColName = new string[PafAppConstants.EXCEL12_COLUMN_LIMIT + 1];
        
        [DebuggerHidden]
        public ColumnConverter()
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Pre array init: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            for (int index = 0; index < ColName.GetUpperBound(0); ++index)
            {
                ColName[index] = ExcelColumnFromNumber(index);
            }
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Post array init: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");

        }
        
        [DebuggerHidden]
        private static string ExcelColumnFromNumber(int column)
        {
            string columnString = "";
            decimal columnNumber = column;
            while (columnNumber > 0)
            {
                decimal currentLetterNumber = (columnNumber - 1) % 26;
                char currentLetter = (char)(currentLetterNumber + 65);
                columnString = currentLetter + columnString;
                columnNumber = (columnNumber - (currentLetterNumber + 1)) / 26;
            }

            return columnString;
        }

        [DebuggerHidden]
        public string GetColumnAlphabet(int i)
        {
            return ColName[i];
        }
    }
}
