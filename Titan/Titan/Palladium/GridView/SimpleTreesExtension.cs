﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Pace.ExcelGridView
{
    [Obsolete("Replaced by SimpleDimTrees", false)]
    internal static class SimpleTreesExtension
    {
        /// <summary>
        /// Get the alias values for a set of alias tables.
        /// </summary>
        /// <param name="simpleTrees">Titan Simple Tree</param>
        /// <param name="pafTree">Server Simple Tree</param>
        /// <param name="member">Member to find.</param>
        /// <param name="aliasTables">Alias table to get the vaules for.</param>
        /// <returns>A Dictionary of strings</returns>
        public static Dictionary<string, string> GetAliasValues(this SimpleTrees simpleTrees, pafSimpleDimTree pafTree, string member, IEnumerable<string> aliasTables)
        {
            Dictionary<string, string> aliasValues = new Dictionary<string, string>();

            foreach (string aliasTable in aliasTables)
            {
                string aliasValue = simpleTrees.GetAlias(pafTree.id, member, aliasTable);
                if (!String.IsNullOrEmpty(aliasValue))
                {
                    aliasValues.Add(aliasTable, aliasValue);
                }
            }

            return aliasValues;
        }
    }
}
