﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
//using Pace.comm;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using Titan.Pace;
using Titan.Pace.ExcelGridView;
using Titan.Palladium;
using Titan.Palladium.GridView;

namespace Titan
{
    public partial class ThisWorkbook
    {
        /// <summary>
        /// 
        /// </summary>
        public PaceRibbon Ribbon { get; set; }
        //internal ICustomAction addinUtilities;
        private void ThisWorkbook_Startup(object sender, System.EventArgs e)
        {
            try
            {
                //Themes here:
                //http://documentation.devexpress.com/#WindowsForms/CustomDocument2534
                DevExpress.UserSkins.BonusSkins.Register();
                UserLookAndFeel.Default.UseDefaultLookAndFeel = true;
                UserLookAndFeel.Default.ParentLookAndFeel = null;
                UserLookAndFeel.Default.SkinName = SkinManager.Default.GetValidSkinName(Properties.Settings.Default.Theme);
                //UserLookAndFeel.Default.SkinName = "Office 2010 Silver";
                //UserLookAndFeel.Default.SkinName = "Metropolis";
                //UserLookAndFeel.Default.SkinName = "Seven Classic";
                //UserLookAndFeel.Default.SkinName = "Valentine";
                UserLookAndFeel.Default.Style = LookAndFeelStyle.Skin;
                UserLookAndFeel.Default.UseWindowsXPTheme = false;
                PafApp.GetLogger().Info("Using skin: " + UserLookAndFeel.Default.SkinName);
                PafApp.GetEventManager().ThisWorkbook_Startup(sender, e, this);
                //object addInProgId = "Titan.Addin";
                //addinUtilities = (ICustomAction)Application.COMAddIns.Item(ref addInProgId).Object;
                //RemotingServer.Start();
            }
            catch (System.Threading.ThreadAbortException)
            {

            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(
                    ex.Message,
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                    System.Windows.Forms.MessageBoxButtons.OK,
                    System.Windows.Forms.MessageBoxIcon.Error);
                return;
            }
        }

        protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
        {
            Ribbon = new PaceRibbon();
            return Ribbon;
        }

   #region VSTO Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InternalStartup()
        {
            this.Startup += new System.EventHandler(this.ThisWorkbook_Startup);
        }        
        #endregion
    }
  
}