#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web.Services.Protocols;
using System.Windows.Forms;
using Titan.Pace.Application;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Exceptions;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Application.Forms;
using Titan.Pace.Application.Utilities;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.MemberTag;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.Localization;
using Titan.Pace.TestHarness;
using Titan.PafService;
using Titan.Palladium.GridView;
using Titan.Palladium.TestHarness;
using Titan.Properties;

namespace Titan.Pace
{
    /// <summary>
    /// Static PafApp class, contains static members for use by all classes.
    /// </summary>
    internal class PafApp
    {
        private static Stopwatch _stopwatch;
        private static PafServiceProviderService _pafService;
        private static pafAuthResponse _pafAuthResponse;
        private static pafPlanSessionResponse _pafPlanSessionResponse;
        private static pafPopulateRoleFilterResponse _pafPopulateRoleFilterResponse;
        private static pafGetFilteredUOWSizeResponse  _pafGetFilteredUowSizeResponse;
        private static ViewMngr _viewMngr;
        private static CommandBarMngr _commandBarMngr;
        private static ViewTreeView _viewTreeView;
        private static MemberTagInfo _memberTagInfo;
        private static ExcelAppImpl _gridApp;
        private static ExcelEventMgr _eventMgr;
        private static PafServiceMngr _serviceMngr;
        private static FormattingMngr _formattingMngr;
        private static TestHarnessMgr _testHarnessMgr;
        private static frmMessageBox _messageBox;
        private static PafAppSettingsHelpers _pafAppSettingsHelper;
        private static CellNoteMngr _cellNoteMngr;
        private static MemberTagMngr _mbrTagMngr;
        private static AppLocalization _localization;
        private static SaveWorkMngr _saveWorkMngr;
        private static Dictionary<string, pafStyle> _globalStyles;
        private static Dictionary<string, abstractPaceConditionalStyle> _conditionalStyles;
        private static Dictionary<string, conditionalFormat> _conditionalFormats;
        private static Dictionary<string, customMenuDef> _customMenuDefs;
        private static Logging _logger;
        private static ChangedCellMngr _changedCellsMngr;
        private static Queue<ChangedCellInfo> _testHarnessChangedCellsMngr;
        private static UowCalculator _uow;
        private static bool _showMessagePrompts = false;
        private static Dictionary<string, PafServer> _pafServerMap;
        private static List<DescendantIntersection> _descendantIntersections;
        //Start Async Plan Session Event
        public static event PafServiceOnStartPlanSessionCompleted GetOnStartPlanSessionCompleted;
        public delegate void PafServiceOnStartPlanSessionCompleted(object sender, GetOnStartPlanSessionCompletedEventArgs eventArgs);
        //Event for when a PafServerice Async Error Occurs.
        public static event PafServiceAsyncError AsyncOperationsError;
        public delegate void PafServiceAsyncError(object sender, PafWebserviceErrorEventArgs eventArgs);
        //Event for when an Async call can no longer be canceled
        public static event PafServiceAsyncCannotCancel AsyncCannotCancel;
        public delegate void PafServiceAsyncCannotCancel(object sender);
        //Event for when an async call can be canceled.
        public static event PafServiceAsyncCanCancel AsyncCanCancel;
        public delegate void PafServiceAsyncCanCancel(object sender);
        public static event PafServiceGetDescendantsCompleted GetDescendantsCompleted;
        public delegate void PafServiceGetDescendantsCompleted(object sender, GetDescendantsCompletedEventArgs eventArgs);
        public static event PafServiceGetDescendantsError GetDescendantsError;
        public delegate void PafServiceGetDescendantsError(object sender, PafWebserviceErrorEventArgs eventArgs);
        public static event PafServiceWebserviceCall PafServiceCall;
        public delegate void PafServiceWebserviceCall(object sender, PafWebserviceCallEventArgs eventArgs);

        public PafApp()
        {
            RuleSetName = String.Empty;
            ClientId = String.Empty;
            Platform = String.Empty;
            ServerVersion = String.Empty;
            DataSourceId = String.Empty;
            ApplicationId = String.Empty;
            ClientPasswordResetEnabled = false;
            MinNewPasswordLength = null;
            MaxNewPasswordLength = null;
            SessionToken = String.Empty;
            MdbProps = null;
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Clear()
        {
            _viewMngr = null;
            //_ProtectionMngr = null;
            //_StartUp = true;
        }

        /// <summary>
        /// Starts the communication between the client and the server.
        /// </summary>
        public static void Init()
        {
            Init(false);
        }

        /// <summary>
        /// Starts the communication between the client and the server.
        /// </summary>
        /// <param name="forceInit">If true, the initilization will be done reguardless if already exists in map</param>
        public static void Init(bool forceInit)
        {

            //reset flag to false
            ClientPasswordResetEnabled = false;

            //if server map is null
            if (_pafServerMap == null)
            {
                //create a new MAP
                _pafServerMap = new Dictionary<string, PafServer>(StringComparer.CurrentCultureIgnoreCase);

            }

            //get last url
            string url = Settings.Default.Titan_PafService_PafService;
            string urlAddOn = GetLocalization().GetResourceManager().GetString("Application.Controls.UserLogon.UrlLiveAdddOn");

            PafServer pafServer = null;
            PafServer tempServer;
            bool sessionActive = false;

            //TTN-1160
            //TTN-1479 (removed foreInit check, so sessionActive always gets set)
            //if(forceInit)
            //{
                _pafServerMap.TryGetValue(url, out tempServer);
                if(tempServer != null)
                {
                    sessionActive = tempServer.SessionActive;
                }
            //}

            //TTN-1160
            if (forceInit && sessionActive && _pafServerMap.ContainsKey(url))
            {
                pafServer = _pafServerMap[url];
            }
            //if force init or server map doesn't contain key or if the value object is null
            //TTN-1479  added check for sessionActive == false, if so then we reinit()
            else if ( forceInit || !_pafServerMap.ContainsKey(url) || _pafServerMap[url] == null || !sessionActive)
            {
                //if force init but the server map already has entry, remove it
                if (forceInit && _pafServerMap.ContainsKey(url))
                {
                    _pafServerMap.Remove(url);
                }
                else if (!sessionActive)
                {
                    _pafServerMap.Remove(url);
                }

                //TTN-733
                //Verify url is running
                if (IsUrlLive(url + urlAddOn))
                {
                    Stopwatch startTime = Stopwatch.StartNew();
                    PafServiceProviderService pafsvc = new PafServiceProviderService();
                    SetConnectionSettings(pafsvc);

                    clientInitRequest initReq = new clientInitRequest();
                    initReq.clientType = GetGridApp().Version;
                    initReq.clientVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                    initReq.ipAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();// System.Net.IPAddress.Broadcast.ToString();

                    GetLogger().InfoFormat("Init Request: {0} ", new object[] { url });

                    //request a client init.
                    pafServerAck ack = pafsvc.clientInit(initReq);

                    startTime.Stop();
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("clientInitRequest runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));


                    pafServer = new PafServer();

                    pafServer.PafService = pafsvc;
                    pafServer.ClientId = ack.clientId;
                    pafServer.ServerVersion = ack.serverVersion;
                    pafServer.Platform = ack.platform;
                    pafServer.ClientPasswordResetEnabled = ack.clientPasswordResetEnabled;
                    pafServer.MinNewPasswordLength = ack.minPassowordLength;
                    pafServer.MaxNewPasswordLength = ack.maxPassowordLength;
                    pafServer.Url = url;
                    pafServer.ApplicationId = ack.applicationId;
                    pafServer.DataSourceId = ack.dataSourceId;
                    pafServer.ClientUpgradeUrl = ack.clientUpgradeUrl;
                    pafServer.ClientUpgradeRequired = ack.clientUpgradeRequired;
                    pafServer.AuthMode = ack.authMode;

                    //add entry to map
                    _pafServerMap.Add(url, pafServer);

                    //log info
                    GetLogger().InfoFormat("URL: {0}", url);

                    GetLogger().InfoFormat("Client ID: {0}", ack.clientId);

                    GetLogger().InfoFormat("Platform: {0}", ack.platform);

                    GetLogger().InfoFormat("Server Version: {0}", ack.serverVersion);

                    GetLogger().InfoFormat("Machine Name: {0}", Dns.GetHostName());

                    GetLogger().InfoFormat("Host Name: {0}", Dns.GetHostEntry(Dns.GetHostName()).HostName);

                    GetLogger().InfoFormat("User Name: {0}", WindowsIdentity.GetCurrent().Name);

                    GetLogger().InfoFormat("Auth Mode: {0}", ack.authMode);
                    
                }
                else
                {
                    GetLogger().WarnFormat("Could not find URL(IsUrlAlive): {0} ", new object[]{ url });
                    //if can not connect to url, throw exception
                    throw new Exception(GetLocalization().GetResourceManager().GetString("Application.Exception.CouldNotConnectToServer"));
                }
            }
            else
            {
                pafServer = _pafServerMap[url];
            }
            if (pafServer != null)
            {
                ClientPasswordResetEnabled = pafServer.ClientPasswordResetEnabled;
            }        
        }

        /// <summary>
        /// Authenticates the user with the PafServer
        /// </summary>
        /// <param name="username">The userid to be sent to the server.</param>
        /// <param name="passwordHash">The password to be sent to the server (no longer used).</param>
        /// <param name="domain">The users domain (used for LDAP)</param>
        /// <param name="IV">The IV (used for Encription)</param>
        /// <param name="password">The password to be sent to the server.</param>
        /// <param name="sid">The users sid (used for LDAP).</param>
        public static void SendClientAuth(string username, string passwordHash, string domain, 
            string IV, string password, string sid)
        {
            _stopwatch = Stopwatch.StartNew();
            string url = String.Empty;

            try
            {               
                Init();

                //get last url
                url = Settings.Default.Titan_PafService_PafService;

                PafServiceCall.Invoke(null, new PafWebserviceCallEventArgs(ClientId, SessionToken, ServiceCallType.ClientAuth));

                //TTN-1561
                SimpleDimTrees = null;

                //if map contains key and value is not null
                if (_pafServerMap.ContainsKey(url) && _pafServerMap[url] != null )
                {
                    //get paf server from map
                    PafServer pafServer = _pafServerMap[url];


                    // check version
                    if (pafServer.ClientUpgradeRequired)
                    {
                        UpgradeRequired(pafServer.ClientUpgradeUrl);
                        return;
                    }

                    pafAuthRequest initAuth = new pafAuthRequest();
                    initAuth.username = username;
                    //initAuth.passwordHash = passwordHash;
                    initAuth.clientId = pafServer.ClientId;
                    initAuth.domain = domain;
                    initAuth.IV = IV;
                    initAuth.password = password;
                    initAuth.sid = sid;

                    //get cached paf service
                    PafServiceProviderService pafsvc = pafServer.PafService;
                    SetConnectionSettings(pafsvc);

                    GetLogger().InfoFormat("SendingAuthentication. CliendId: {0}", new object[] { pafServer.ClientId });

                    pafAuthResponse auth = new pafAuthResponse();

                    try
                    {
                        auth = pafsvc.clientAuth(initAuth);
                    }
                    catch (SoapException)
                    {

                        try
                        {

                            //Begin TTN-659
                            //get clean init from server then try again
                            Init(true);

                            pafServer = _pafServerMap[url];

                            //if new server entry exists, get new client id then auth again.
                            if (pafServer != null)
                            {

                                // check version
                                if (pafServer.ClientUpgradeRequired)
                                {
                                    // redirect to client url, and exit
                                    UpgradeRequired(pafServer.ClientUpgradeUrl);
                                    return;
                                }

                                initAuth.clientId = pafServer.ClientId;

                                auth = pafsvc.clientAuth(initAuth);
                            }
                            //End TTN-659
                        }
                        catch (SoapException) 
                        {
                            throw;
                        }
                    }
                    catch (Exception)
                    {
                        GetLogger().InfoFormat("SendClientAuth, Exception.  PafServer: {0}", new object[] { url });
                        throw;
                    }


                    if (auth != null)
                    {
                        //if security token is valid
                        if (auth.securityToken.valid)
                        {
                            //get result from auth response
                            _pafAuthResponse = auth;

                            

                            GetLogger().Info("Session Token: " + auth.securityToken.sessionToken);
                            GetLogger().Info("User Name: " + auth.securityToken.userName);

                            //The rest of program will use these attributes now that we've authenticated.
                            if (pafServer != null) _pafService = pafServer.PafService;
                            if (pafServer != null) ClientId = pafServer.ClientId;
                            if (pafServer != null) ServerVersion = pafServer.ServerVersion;
                            if (pafServer != null) Platform = pafServer.Platform;
                            if (pafServer != null) MinNewPasswordLength = pafServer.MinNewPasswordLength;
                            if (pafServer != null) MaxNewPasswordLength = pafServer.MaxNewPasswordLength;
                            if (pafServer != null) ApplicationId = pafServer.ApplicationId;
                            if (pafServer != null) DataSourceId = pafServer.DataSourceId;
                            //if (pafServer != null) _authMode = pafServer.AuthMode;
                            if (pafServer != null) SessionToken = auth.securityToken.sessionToken;
                            //ttn-981
                            if (pafServer != null) pafServer.SessionToken = auth.securityToken.sessionToken;

                            //if user is flaged to change password, throw change password exception
                            if (_pafAuthResponse.changePassword)
                            {
                                GetLogger().Info("Change Password Exception for user " + auth.securityToken.userName);
                                throw new ChangePasswordException();
                            }
                        }
                        else
                        {
                            _pafAuthResponse = auth;

                            string message = String.Empty;
                            //TTN-1056, 1947
                            //If the response is null, then return a generic error message.
                            if (_pafAuthResponse == null || _pafAuthResponse.securityToken == null || String.IsNullOrEmpty(_pafAuthResponse.securityToken.sessionToken))
                            {
                                message = GetLocalization().GetResourceManager().GetString("Application.Exception.InvalidSecurityToken");
                            }
                            else
                            {
                                message = auth.securityToken.sessionToken;
                            }
                            throw new SecurityException(message);
                        }
                    }
                }
            }
            catch (Exception)
            {
                GetLogger().InfoFormat("SendClientAuth, Exception.  PafServer: {0}", new object[] { url });
                throw;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("pafAuthRequest(SendClientAuth) runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        
        }

        /// <summary>
        /// Set the webserivce connection settings.
        /// </summary>
        /// <param name="pafsvc"></param>
        private static void SetConnectionSettings(PafServiceProviderService pafsvc)
        {
            string url = pafsvc.Url;
            //set the Proxy Settings.
            if (Settings.Default.UseProxySettings)
            {
                GetLogger().InfoFormat("Setting Proxy for URL: {0} ", new object[] { url });
                IWebProxy iwp20 = WebRequest.DefaultWebProxy;
                if (iwp20 != null)
                {
                    pafsvc.Proxy = iwp20;
                }
            }
            else
            {
                GetLogger().InfoFormat("Setting URL: {0} Proxy to null.", new object[] { url });
                pafsvc.Proxy = null;
            }
            
            //Create a cookie container, so the client is aware of any cookies.
            if (pafsvc.CookieContainer == null)
            {
                pafsvc.CookieContainer = new CookieContainer();
            }

            //Set the Timeout.
            pafsvc.Timeout = Settings.Default.ClientTimeout;
            GetLogger().InfoFormat("Setting Timeout for URL: {0}, to {1} ", new object[] { url, Settings.Default.ClientTimeout.ToString("N0") });

        }

        /// <summary>
        /// Changes users password.
        /// </summary>
        /// <param name="username">The userid to be sent to the server.</param>
        /// <param name="oldPassword">Users old password.</param>
        /// <param name="newPassword">Users new password.</param>
        public static void ChangePasswordRequest(string username, string oldPassword, string newPassword) 
        {

             GetLogger().Info("Changing password for user: " + username);

             string url = Settings.Default.Titan_PafService_PafService;

            //ensure url is valid
             if (_pafServerMap.ContainsKey(url) && _pafServerMap[url] != null)
             {
                 //get server from map
                 PafServer pafServer = _pafServerMap[url];

                 pafClientChangePasswordRequest changePassword = new pafClientChangePasswordRequest();

                 changePassword.pafUserDef = new pafUserDef();
                 changePassword.pafUserDef.userName = username;
                 changePassword.pafUserDef.password = oldPassword;
                 changePassword.clientId = pafServer.ClientId;
                 changePassword.newPassword = newPassword;

                 PafServiceProviderService pafsvc = pafServer.PafService;
                 SetConnectionSettings(pafsvc);

                 pafClientSecurityResponse changePasswordResponse;

                 //try to change users password
                 try
                 {
                     changePasswordResponse = pafsvc.changePafUserPassword(changePassword);
                 }
                 catch (SoapException)
                 {
                     throw;
                 }
                 catch (Exception)
                 {
                     throw;
                 }

                 //if respons is not null
                 if (changePasswordResponse != null)
                 {


                     //see if password change was successful
                     bool passwordChanged = changePasswordResponse.successful;

                     //if password change was not successful, throw exception
                     if (! passwordChanged)
                     {

                         throw new Exception(GetLocalization().GetResourceManager().GetString("Application.Exception.ChangePasswordProblem"));

                     }
                                          
                 }
                 
             }

        }

        /// <summary>
        /// Resets users password on server the PafServer
        /// </summary>
        /// <param name="username">The userid to be sent to the server.</param>
        public static void RequestPasswordReset(string username)
        {
            try
            {
                //init's client
                Init();

                string url = Settings.Default.Titan_PafService_PafService;

                if (_pafServerMap.ContainsKey(url) && _pafServerMap[url] != null)
                {
                    PafServer pafServer = _pafServerMap[url];

                    //new reset paf user password
                    //create client security request
                    pafClientSecurityRequest resetPassword = new pafClientSecurityRequest();

                    //create new paf user
                    resetPassword.pafUserDef = new pafUserDef();

                    //set username on paf user
                    resetPassword.pafUserDef.userName = username;

                    //set client id
                    resetPassword.clientId = pafServer.ClientId;

                    //get instance of paf service
                    PafServiceProviderService pafsvc = pafServer.PafService;
                    SetConnectionSettings(pafsvc);

                    try
                    {
                        //call reset paf user password on webservice 
                        pafClientSecurityPasswordResetResponse responseResult = pafsvc.resetPafUserPassword(resetPassword);


                            //if not null
                            if (responseResult != null)
                            {

                                //get properties
                                bool isInvalidUsername = responseResult.invalidUserName;
                                bool isInvalidEmailAddress = responseResult.invalidEmailAddress;
                                bool isSuccessful = responseResult.successful;
                                string emailAddress = responseResult.userEmailAddress;

                                //if invalid user name, inform client user
                                if (isInvalidUsername)
                                {

                                    throw new Exception(String.Format(GetLocalization().GetResourceManager().GetString("Application.MessageBox.InvalidUser"), username));

                                }
                                //if invalid email, inform client user
                                else if (isInvalidEmailAddress)
                                {
                                    throw new Exception(GetLocalization().GetResourceManager().GetString("Application.MessageBox.NoEmailAddress"));

                                }
                                //if successful, inform client user
                                else if (isSuccessful)
                                {

                                    string message = String.Format(GetLocalization().GetResourceManager().GetString("Application.MessageBox.SuccessfulPasswordReset"), emailAddress);
                                    System.Windows.Forms.MessageBox.Show(message, GetLocalization().GetResourceManager().GetString("Application.Title"), MessageBoxButtons.OK, MessageBoxIcon.Information);

                                }

                            }


                    }
                    catch (SoapException)
                    {
                        throw;
                    }
                    catch (Exception)
                    {
                        throw;
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Get a Planning Session Response (which contain PafTrees)
        /// </summary>
        /// <param name="role">The users role.</param>
        /// <param name="seasonId">The season/process selected by the user.</param>
        /// <param name="invalidInxSuppSel">Invalid Intersection Suppression Selected</param>
        /// <param name="filteredSubtotals"></param>
        public static void CreatePlanningSessionAsync(string role, string seasonId, bool invalidInxSuppSel, bool filteredSubtotals)
        {
            _stopwatch = Stopwatch.StartNew();

            PafServiceCall.Invoke(null, new PafWebserviceCallEventArgs(ClientId, SessionToken, ServiceCallType.PlanningSession));

            SimpleDimTrees = null;
            TempSeasonId = seasonId;
            try
            {
                pafPlanSessionRequest planSession = new pafPlanSessionRequest
                {
                    clientId = ClientId,
                    isInvalidIntersectionSuppressionSelected = invalidInxSuppSel,
                    filteredSubtotalsSelected = filteredSubtotals,
                    selectedRole = role,
                    seasonId = seasonId,
                    sessionToken = SessionToken,
                    compressResponse = true
                };

                PafServiceProviderService pafsvc = GetPafService();

                pafsvc.startPlanSessionCompleted -= pafsvc_onStartPlanSessionCompleted;
                pafsvc.startPlanSessionCompleted += pafsvc_onStartPlanSessionCompleted;
                AsyncCanCancel.Invoke(null);
                pafsvc.startPlanSessionAsync(planSession);

            }
            catch (SoapException)
            {
                _pafPlanSessionResponse = null;
                SeasonId = null;
                throw;
            }
            catch (Exception ex)
            {
                _pafPlanSessionResponse = null;
                SeasonId = null;
                MessageBox().Show(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("startPlanSession runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
                GetLogger().Info("==========================================");
            }
        }

        public static void CancelAsyncWebCall()
        {
            PafServiceProviderService pafsvc = GetPafService();
            pafsvc.Abort();
        }

        /// <summary>
        /// Get a Planning Session Response (which contain PafTrees)
        /// </summary>
        /// <param name="role">The users role.</param>
        /// <param name="seasonId">The season/process selected by the user.</param>
        /// <param name="invalidInxSuppSel">Invalid Intersection Suppression Selected</param>
        /// <param name="filteredSubtotals"></param>
        public static void CreatePlanningSession(string role, string seasonId, bool invalidInxSuppSel, bool filteredSubtotals)
        {
            _stopwatch = Stopwatch.StartNew();

            PafServiceCall.Invoke(null,new PafWebserviceCallEventArgs(ClientId, SessionToken,ServiceCallType.PlanningSession));

            SimpleDimTrees = null;

            try
            {
                pafPlanSessionRequest planSession = new pafPlanSessionRequest
                {
                    clientId = ClientId,
                    isInvalidIntersectionSuppressionSelected = invalidInxSuppSel,
                    filteredSubtotalsSelected = filteredSubtotals,
                    selectedRole = role,
                    seasonId = seasonId,
                    sessionToken = SessionToken,
                    compressResponse = true
                };

                PafServiceProviderService pafsvc = GetPafService();

                pafPlanSessionResponse resp = pafsvc.startPlanSession(planSession);

                if (resp != null)
                {
                    resp.dimTrees.Uncompress();
                    _pafPlanSessionResponse = resp;
                    SeasonId = seasonId;
                    //(TTN-401) - Reset the data cache flag, since we just got a new cache it can't be dirty.
                    GetSaveWorkMngr().DataWaitingToBeSaved = false;
                    //Set the default ruleset name
                    RuleSetName = _pafPlanSessionResponse.defaultRuleSetName;
                    CacheBlock = _pafPlanSessionResponse.clientCacheBlock;
                    Globals.ThisWorkbook.Ribbon.RuleSetSet = false;
                    //this code serializes the trees to a file.
                    //foreach (pafSimpleDimTree tree in resp.dimTrees)
                    //{
                    //    tree.Save(@"c:\", tree.id.RemoveInvalidFileChars() + ".xml");
                    //}
                    MdbProps = GetServiceMngr().GetPafMdbProps();
                    //Create the SimpleDimTrees from the response object.
                    SimpleDimTrees = resp.ToSimpleDimTrees(_pafAuthResponse, MdbProps);
                    //SimpleDimTrees.Save("c:\\", "dimtree.xml");
                }
            }
            catch (SoapException)
            {
                _pafPlanSessionResponse = null;
                SeasonId = null;
                throw;
            }
            catch (Exception ex)
            {
                _pafPlanSessionResponse = null;
                SeasonId = null;
                MessageBox().Show(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("startPlanSession runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
                GetLogger().Info("==========================================");
            }
        }

        /// <summary>
        /// Populate the role filter.
        /// </summary>
        /// <param name="role">The users role.</param>
        /// <param name="seasonId">The season/process selected by the user.</param>
        /// <param name="invalidInxSuppSel">Invalid Intersection Suppression Selected</param>
        /// <param name="filteredSubtotalsSelected">Filtered Subtotals Selected </param>
        public static void PopulateRoleFilter(string role, string seasonId, bool invalidInxSuppSel, bool filteredSubtotalsSelected)
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                pafPlanSessionRequest roleFilter = new pafPlanSessionRequest();
                roleFilter.clientId = ClientId;
                roleFilter.selectedRole = role;
                roleFilter.seasonId = seasonId;
                roleFilter.isInvalidIntersectionSuppressionSelected = invalidInxSuppSel;
                roleFilter.filteredSubtotalsSelected = filteredSubtotalsSelected;
                roleFilter.sessionToken = SessionToken;
                roleFilter.compressResponse = true;

                pafPopulateRoleFilterResponse resp = pafsvc.populateRoleFilters(roleFilter);

                if (resp != null)
                {
                    resp.dimTrees.Uncompress();
                    _pafPopulateRoleFilterResponse = resp;
                }
            }
            catch (SoapException)
            {
                _pafPopulateRoleFilterResponse = null;
                throw;
            }
            catch (Exception ex)
            {
                _pafPopulateRoleFilterResponse = null;
                MessageBox().Show(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("populateRoleFilters runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// Get the filtered Uow size.
        /// </summary>
        /// <param name="role">The users role.</param>
        /// <param name="seasonId">The season/process selected by the user.</param>
        /// <param name="invalidInxSuppSel">Invalid Intersection Suppression Selected</param>
        /// <param name="filteredSubtotalsSelected">Filtered subtotals.</param>
        /// <param name="pafUserSelections">Paf user selections.</param>
        public static void GetFilteredUowSize(string role, string seasonId,
            bool invalidInxSuppSel, bool filteredSubtotalsSelected, pafDimSpec[] pafUserSelections)
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                PafService.PafServiceProviderService pafsvc = GetPafService();

                pafGetFilteredUOWSizeRequest filteredUOWSize = new pafGetFilteredUOWSizeRequest();
                filteredUOWSize.clientId = ClientId;
                filteredUOWSize.isInvalidIntersectionSuppressionSelected = invalidInxSuppSel;
                filteredUOWSize.filteredSubtotalsSelected = filteredSubtotalsSelected;
                filteredUOWSize.pafUserSelections = pafUserSelections;
                filteredUOWSize.selectedRole = role;
                filteredUOWSize.seasonId = seasonId;
                filteredUOWSize.sessionToken = SessionToken;

                pafGetFilteredUOWSizeResponse resp = pafsvc.getFilteredUOWSize(filteredUOWSize);

                if (resp != null)
                {
                    _pafGetFilteredUowSizeResponse = resp;
                }
            }
            catch (SoapException)
            {
                _pafGetFilteredUowSizeResponse = null;
                throw;
            }
            catch (Exception ex)
            {
                _pafGetFilteredUowSizeResponse = null;
                MessageBox().Show(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("getFilteredUowSize runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        public static void GetReinitClientState()
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                pafRequest reinitClientState = new pafRequest();
                reinitClientState.clientId = ClientId;
                reinitClientState.sessionToken = SessionToken;

                pafsvc.reinitializeClientState(reinitClientState);

            }
            catch (SoapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                MessageBox().Show(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("reinitializeClientState runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// Is the session(client id) active/present on the server.
        /// </summary>
        /// <param name="clientId">Client id.</param>
        /// <returns>true if the client id present, false if not.</returns>
        /// <remarks>TTN-1160</remarks>
        public static bool IsSessionActive(string clientId)
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                pafRequest sessionActive = new pafRequest();
                sessionActive.clientId = clientId;
                sessionActive.sessionToken = null;

                pafSuccessResponse psr = pafsvc.isSessionActive(sessionActive);

                if(psr != null)
                {
                    return psr.success;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                GetLogger().Error(ex);
                return false;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("IsSessionActive runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// Logoff the server, but do not remove the client id.
        /// </summary>
        /// <remarks>ttn-981</remarks>
        public static void LogoffServer()
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                // If a session exists
                if (_pafAuthResponse != null && _pafAuthResponse.securityToken != null &&
                    _pafAuthResponse.securityToken.sessionToken != null)
                {
                    PafServiceProviderService pafsvc = GetPafService();

                    pafRequest logoffSession = new pafRequest();
                    logoffSession.clientId = ClientId;
                    logoffSession.sessionToken = SessionToken;

                    pafsvc.logoff(logoffSession);


                    SeasonId = String.Empty;
                    GetSaveWorkMngr().DataWaitingToBeSaved = false;
                    RuleSetName = String.Empty;
                    _pafPlanSessionResponse = null;
                    CacheBlock = null;
                    _memberTagInfo = null;
                    //TTN-1575
                    _globalStyles = null;
                    _conditionalStyles = null;
                    _conditionalFormats = null;
                }
            }
            catch (Exception ex)
            {
                GetLogger().Warn("Logoff server failed: " + ex);
                //MessageBox().Show(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("logoff runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// Ends all the planning sessions in the pafServerMap.
        /// </summary>
        /// <remarks>ttn-981</remarks>
        public static void EndPlanningSession()
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                // If a session exists
                if (_pafAuthResponse != null && _pafAuthResponse.securityToken != null &&
                    _pafAuthResponse.securityToken.sessionToken != null)
                {
                    if (_pafServerMap != null && _pafServerMap.Count > 0)
                    {
                        foreach (KeyValuePair<string, PafServer> kvp in _pafServerMap)
                        {

                            PafServiceProviderService pafsvc = kvp.Value.PafService;
                            SetConnectionSettings(pafsvc);

                            pafRequest planSession = new pafRequest();
                            planSession.clientId = kvp.Value.ClientId;
                            planSession.sessionToken = kvp.Value.SessionToken;

                            try
                            {
                                //pafsvc.endPlanningSession(planSession);
                                //TTN-2359
                                pafsvc.endPlanningSessionAsync(planSession);
                            }
                            catch
                            {
                                //igrnore...
                            }
                        }

                    }

                    _pafServerMap.Clear();


                    SeasonId = String.Empty;
                    GetSaveWorkMngr().DataWaitingToBeSaved = false;
                    RuleSetName = String.Empty;
                    _pafPlanSessionResponse = null;
                    CacheBlock = null;
                    //TTN-1575
                    _globalStyles = null;
                    _conditionalStyles = null;
                    _conditionalFormats = null;
                    if (_pafService != null)
                    {
                        _pafService.Dispose();
                        _pafService = null;
                    }
                }
            }
            catch (SoapException)
            {
                //TODO This error occurs when the call never seems to reach the server, and the SOAP 
                //layer throws an error.  As a workaround we are just going to toss the error.
                //Do nothing it's a server error.
            }
            catch (Exception ex)
            {
                MessageBox().Show(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("EndPlanningSession runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// Execute a Custom Command
        /// </summary>
        public static customCommandResult[] RunCustomCommand(string menuCommandID, string[] keys, string[] values)
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                pafCustomCommandRequest runCustomCmd = new pafCustomCommandRequest();
                runCustomCmd.clientId = ClientId;
                runCustomCmd.sessionToken = SessionToken;
                runCustomCmd.menuCommandKey = menuCommandID;
                runCustomCmd.parameterKeys = keys;
                runCustomCmd.parameterValues = values;

                customCommandResult[] resp;
                resp = pafsvc.runCustomCommand(runCustomCmd);

                return resp;
            }
            catch (Exception ex)
            {
                MessageBox().Show(ex);
                return null;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("RunCustomCommand runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        public static void GetCacheBlockAsync()
        {
            _stopwatch = Stopwatch.StartNew();
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                pafClientCacheRequest request = new pafClientCacheRequest();
                request.clientId = ClientId;
                request.sessionToken = SessionToken;

                pafsvc.clientCacheRequestCompleted -= pafsvc_clientCacheRequestCompleted;
                pafsvc.clientCacheRequestCompleted += pafsvc_clientCacheRequestCompleted;
                _logger.Info("clientCacheRequest sent");
                pafsvc.clientCacheRequestAsync(request);

            }
            catch (Exception ex)
            {
                GetLogger().Error(ex);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("GetCacheBlockAsync-Start runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }



        public static List<DescendantIntersection> GetDescendantsSync(List<DescendantIntersection> descendantIntersections)
        {
            _stopwatch = Stopwatch.StartNew();

            if (descendantIntersections == null) return null;

            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                paceDescendantsRequest descendantsRequest = new paceDescendantsRequest();
                descendantsRequest.clientId = ClientId;
                descendantsRequest.sessionCells = descendantIntersections.ParentsToList().ToSimpleCoordListCollection().ToArray();
                descendantsRequest.sessionToken = SessionToken;

                paceDescendantsResponse descendantsResponse = pafsvc.getDescendants(descendantsRequest);

                simpleCoordList[] result = descendantsResponse.sessionIntersections;


                if (result != null && 
                    result.Length > 0)
                {
                    int i = 0;
                    foreach (simpleCoordList scl in result)
                    {
                        if (scl == null) continue;
                        if (descendantIntersections.Count <= i)
                        {
                            descendantIntersections.Add(new DescendantIntersection());
                        }
                        descendantIntersections[i].Descendants = scl.ToCoordinateSet();
                        i++;
                    }
                }

                return descendantIntersections;

            }
            catch (Exception ex)
            {
                GetLogger().Error(ex);
                return null;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("GetDescendants runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }
        public static void GetDescendants(List<DescendantIntersection> descendantIntersections)
        {
            _stopwatch = Stopwatch.StartNew();

            if (descendantIntersections == null) return;

            try
            {
                _descendantIntersections = descendantIntersections;

                PafServiceProviderService pafsvc = GetPafService();

                paceDescendantsRequest descendantsRequest = new paceDescendantsRequest();
                descendantsRequest.clientId = ClientId;
                descendantsRequest.sessionCells = _descendantIntersections.ParentsToList().ToSimpleCoordListCollection().ToArray();
                descendantsRequest.sessionToken = SessionToken;

                pafsvc.getDescendantsCompleted -= pafsvc_getDescendantsCompleted;
                pafsvc.getDescendantsCompleted += pafsvc_getDescendantsCompleted;
                pafsvc.getDescendantsAsync(descendantsRequest);


            }
            catch (Exception ex)
            {
                GetLogger().Error(ex);
                return;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("GetDescendants runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        private static void pafsvc_onStartPlanSessionCompleted(object sender, startPlanSessionCompletedEventArgs e)
        {

            AsyncCannotCancel.Invoke(null);

            Stopwatch sw = Stopwatch.StartNew();
            GetLogger().Info("onStartPlanSessionCompleted returned");
            try
            {
                if (e.Error != null)
                {
                    AsyncOperationsError.Invoke(null, new PafWebserviceErrorEventArgs(false, e.Error.Message, e.Error));
                    _pafPlanSessionResponse = null;
                    SeasonId = null;
                    MessageBox().Show(e.Error);
                }
                else if (e.Error == null && e.Result != null)
                {
                    var resp = e.Result;
                    resp.dimTrees.Uncompress();
                    _pafPlanSessionResponse = resp;
                    SeasonId = TempSeasonId;
                    //(TTN-401) - Reset the data cache flag, since we just got a new cache it can't be dirty.
                    GetSaveWorkMngr().DataWaitingToBeSaved = false;
                    //Set the default ruleset name
                    RuleSetName = _pafPlanSessionResponse.defaultRuleSetName;
                    CacheBlock = _pafPlanSessionResponse.clientCacheBlock;
                    Globals.ThisWorkbook.Ribbon.RuleSetSet = false;
                    //this code serializes the trees to a file.
                    //foreach (pafSimpleDimTree tree in resp.dimTrees)
                    //{
                    //    tree.Save(@"c:\", tree.id.RemoveInvalidFileChars() + ".xml");
                    //}
                    MdbProps = GetServiceMngr().GetPafMdbProps();
                    //Create the SimpleDimTrees from the response object.
                    SimpleDimTrees = resp.ToSimpleDimTrees(_pafAuthResponse, MdbProps);
                    //SimpleDimTrees.Save("c:\\", "dimtree.xml");

                    GetOnStartPlanSessionCompleted.Invoke(null, new GetOnStartPlanSessionCompletedEventArgs(SessionToken, ClientId));
                }
                sw.Stop();
                GetLogger().InfoFormat("onStartPlanSessionCompleted, runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));
            }
            catch (SoapException)
            {
                _pafPlanSessionResponse = null;
                SeasonId = null;
                throw;
            }
            catch (Exception ex)
            {
                _pafPlanSessionResponse = null;
                SeasonId = null;
                MessageBox().Show(ex);
            }
        }

        private static void pafsvc_clientCacheRequestCompleted(object sender, clientCacheRequestCompletedEventArgs e)
        {
            Stopwatch sw = Stopwatch.StartNew();
            GetLogger().Info("clientCacheRequest returned");
            try
            {
                if (e.Error == null && e.Result != null)
                {
                    pafClientCacheBlock cacheBlock = e.Result;
                    Parallel.Invoke(() => BuildConditionalFormats(true, cacheBlock), () => BuildConditionalStyles(true, cacheBlock), () => BuildGlobalStyles(true, cacheBlock));
                    sw.Stop();
                    //CacheBlock = cacheBlock;
                    GetLogger().InfoFormat("Rebuilding formatting dictionaries, runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));
                }
            }
            catch (Exception ex)
            {
                GetLogger().Error(ex);
            }
            finally
            {
                //sw.Stop();
                //GetLogger().InfoFormat("pafsvc_clientCacheRequestCompleted runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
            }
        }

        private static void pafsvc_getDescendantsCompleted(object sender, getDescendantsCompletedEventArgs e)
        {
            try
            {
                if (e.Error != null)
                {
                    GetDescendantsError.Invoke(null, new PafWebserviceErrorEventArgs(false, e.Error.Message, e.Error));
                }
                else
                {
                    simpleCoordList[] result = e.Result.sessionIntersections;


                    if (result != null && result.Length > 0)
                    {
                        int i = 0;

                        foreach (simpleCoordList scl in result)
                        {
                            if (scl == null) continue;
                            if (_descendantIntersections.Count <= i)
                            {
                                _descendantIntersections.Add(new DescendantIntersection());
                            }
                            _descendantIntersections[i].Descendants = scl.ToCoordinateSet();
                            i++;
                        }
                    }
                    e = null;
                    GetDescendantsCompleted.Invoke(null, new GetDescendantsCompletedEventArgs(SessionToken, ClientId, _descendantIntersections));
                }

            }
            catch (Exception ex)
            {
                //GetLogger().Error(ex);
                throw;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("pafsvc_getDescendantsCompleted runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        public static clusteredResultSetResponse GetClusteredResult(string label, pafDimSpec time, pafDimSpec years, pafDimSpec measures, pafDimSpec version, 
            int numOfClusters, int maxIterations, string ruleSet, clusterResultSetRequestEntry[] customClusterMap = null)
        {
            try
            {
                _stopwatch = Stopwatch.StartNew();
                clusterResultSetRequest cr = new clusterResultSetRequest();
                cr.clientId = ClientId;
                cr.sessionToken = SessionToken;
                cr.timeDimSpec = time;
                cr.yearsDimSpec = years;
                cr.measuresDimSpec = measures;
                cr.versionDimSpec = version;
                cr.numOfClusters = numOfClusters;
                cr.maxIterations = maxIterations;
                cr.ruleSetName = ruleSet;
                cr.label = label;
                cr.customClusterMap = customClusterMap;
                var result = GetPafService().getClusteredResult(cr);

                result.level0Data.Uncompress();
                result.aggregateData.Uncompress();


                return result;

            }
            catch (Exception ex)
            {
                GetLogger().Error(ex);
                return null;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("GetClusteredResult runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        public static paceResultSetResponse GetFilteredResultSetResponse(IEnumerable<pafDimSpec> attribSpecs, pafDimSpec memberDimSpec, int level)
        {
            try
            {
                _stopwatch = Stopwatch.StartNew();

                paceQueryRequest pqr = new paceQueryRequest();
                if (attribSpecs != null)
                {
                    pqr.attributes = attribSpecs.ToArray();
                }
                pqr.members = memberDimSpec;
                pqr.level = level;
                pqr.clientId = ClientId;
                pqr.sessionToken = SessionToken;


                return GetPafService().getFilteredResultSetResponse(pqr);

            }
            catch (Exception ex)
            {
                GetLogger().Error(ex);
                return null;
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("GetFilteredResultSetResponse runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }


        public static createAsstResponse CreateAssortment()
        {
            try
            {
                _stopwatch = Stopwatch.StartNew();

                createAsstRequest asstReq = new createAsstRequest();
                asstReq.sessionToken = SessionToken;
                asstReq.clientId = ClientId;

                return GetPafService().createAssortment(asstReq);
            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("CreateAssortment runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        public static pafResponse SaveClusteredResultSet(string label, string locationDim, IEnumerable<string> dimToCluster,
            string productDim, IEnumerable<string> dimToMeasure, IEnumerable<string> measures, IEnumerable<string> time, 
            IEnumerable<string> version, IEnumerable<string> years,IEnumerable<clusteredResultSetSaveRequestEntry> clusters, 
             stringRow header)
        {
            try
            {
                _stopwatch = Stopwatch.StartNew();

                clusteredResultSetSaveRequest cr = new clusteredResultSetSaveRequest();
                cr.clientId = ClientId;
                cr.sessionToken = SessionToken;
                cr.clusters = clusters.ToArray();
                cr.assortment = label;
                cr.locationDim = locationDim;
                cr.dimToCluster = dimToCluster.ToArray();
                //cr.data = data.ToArray();
                cr.productDim = productDim;
                cr.dimToMeasure = dimToMeasure.ToArray();
                cr.header = header;
                cr.measures = measures.ToArray();
                cr.time = time.ToArray();
                cr.version = version.ToArray();
                cr.years = years.ToArray();

                return  GetPafService().saveClusteredResultSet(cr);

            }
            finally
            {
                _stopwatch.Stop();
                GetLogger().InfoFormat("SaveClusteredResultSet runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }

        public static void UndoCalculation()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            try
            {
                undoDcChangesRequest request = new undoDcChangesRequest();
                request.clientId = ClientId;
                request.sessionToken = GetPafAuthResponse().securityToken.sessionToken;

                PafServiceProviderService pafsvc = GetPafService();

                pafResponse pafResponse = pafsvc.undoDataCacheChanges(request);

                if (pafResponse == null)
                {
                    GetLogger().Warn("The server did not return a response: undoDataCacheChanges");
                    return;
                }


                if (pafResponse.responseCode != 0)
                {
                    if (!String.IsNullOrWhiteSpace(pafResponse.responseMsg))
                    {
                        throw new Exception(pafResponse.responseMsg);
                    }
                    throw new Exception("The server retunred a non-zero response code, however no error message was returned from the server.");
                }
                if (pafResponse.responseCode == 0)
                {
                    if (!String.IsNullOrWhiteSpace(pafResponse.responseMsg))
                    {
                        GetLogger().Info(pafResponse.responseMsg);
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox().Show(ex);
            }
            finally
            {
                startTime.Stop();
                GetLogger().InfoFormat("UndoCalculation runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }
        }

        /// <summary>
        /// Returns the ClientId set by the PafServer.
        /// </summary>
        public static string ClientId { get; private set; }

        /// <summary>
        /// Returns the SessionToken set by the PafServer.
        /// </summary>
        public static string SessionToken { get; private set; }

        /// <summary>
        /// Gets the application id for the current project.
        /// </summary>
        public static string ApplicationId { get; private set; }

        /// <summary>
        /// Gets the data source id for the current project.
        /// </summary>
        public static string DataSourceId { get; private set; }

        /// <summary>
        /// Returns the platform version.
        /// </summary>
        public static string Platform { get; private set; }

        /// <summary>
        /// Returns the version of the PafServer.
        /// </summary>
        public static string ServerVersion { get; private set; }

        /// <summary>
        /// Returns if client password reset is enabled or not
        /// </summary>
        public static bool ClientPasswordResetEnabled{ get; private set; }

        /// <summary>
        /// Returns the minimum length for a new password
        /// </summary>
        public static int? MinNewPasswordLength{ get; private set; }

        /// <summary>
        /// Returns the minimum length for a new password
        /// </summary>
        public static int? MaxNewPasswordLength { get; private set; }

        /// <summary>
        /// Gets the SeasonId string.
        /// </summary>
        /// <returns>A string containing the SeasonId selected by the user.</returns>
        public static string SeasonId { get; private set; }

        /// <summary>
        /// Gets the SeasonId string.
        /// </summary>
        /// <returns>A string containing the SeasonId selected by the user.</returns>
        private static string TempSeasonId { get; set; }

        /// <summary>
        /// Is an undo available to the system.
        /// </summary>
        public static bool UndoAvailable { get; set; }

        /// <summary>
        /// Returns the name of the planning version
        /// </summary>
        public static string PlanningVersion
        {
            get { return GetPafPlanSessionResponse().planningVersion; }
        }

        /// <summary>
        /// Returns the name of the Measure root
        /// </summary>
        public static string MeasureRoot
        {
            get { return CacheBlock.mdbDef.measureRoot; }
        }

        /// <summary>
        /// Returns the name of the Version Dimension
        /// </summary>
        public static string VersionDimension 
        {
            get { return CacheBlock.mdbDef.versionDim; }
        }

        /// <summary>
        /// Returns the name of the Measure Dimension
        /// </summary>
        public static string MeasureDimension
        {
            get { return CacheBlock.mdbDef.measureDim; }
        }

        /// <summary>
        /// Returns the name of the Time Dimension
        /// </summary>
        public static string TimeDimension
        {
            get { return CacheBlock.mdbDef.timeDim; }
        }

        /// <summary>
        /// Returns the name of the Years Dimension
        /// </summary>
        public static string YearsDimension
        {
            get { return CacheBlock.mdbDef.yearDim; }
        }

        /// <summary>
        /// Returns the name of the Plan Type Dimension
        /// </summary>
        public static string PlanTypeDimension
        {
            get { return CacheBlock.mdbDef.planTypeDim; }
        }

        /// <summary>
        /// Returns true/false if the user is a LDAP (non native) user
        /// </summary>
        public static bool IsLdapUser
        {
            get
            {
                if (GetGridApp() != null && GetGridApp().LogonInformation != null)
                {
                    return GetGridApp().LogonInformation.DomainUser;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns true/false if the user is a testing user (pafAuthResponse.securityToken.admin)
        /// </summary>
        public static bool IsTestingUser
        {
            get
            {
                if (GetPafAuthResponse() != null && GetPafAuthResponse().securityToken != null)
                {
                    return GetPafAuthResponse().securityToken.admin;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns the Calculations Pending status of the applciation.
        /// </summary>
        public static bool AreCalculationsPending
        {
            get
            {
                if (GetViewMngr() != null && GetViewMngr().CurrentProtectionMngr != null)
                {
                    return GetViewMngr().CurrentProtectionMngr.CalculationsPending();
                }
                return false;
            }
        }

        /// <summary>
        /// Returns true if the role is built and the user has a Planning Session
        /// </summary>
        public static bool RoleBuilt
        {
            get { return SimpleDimTrees != null; }
        }

        /// <summary>
        /// Returns current auth mode.
        /// </summary>
        public static authMode GetAuthMode(string url)
        {
            try
            {
                PafServer server;
                if (_pafServerMap == null)
                {
                    Init();
                }

                if (_pafServerMap.TryGetValue(url, out server))
                {
                    return server.AuthMode;
                }
                //fix issue where "login as different user shows up with an invalid server"
                return authMode.nativeMode;
            }
            catch (Exception)
            {
                return authMode.nativeMode;
            }
        }

        /// <summary>
        /// Returns/sets the current Measures Ruleset.
        /// </summary>
        public static string RuleSetName { get; set; }

        /// <summary>
        /// Gets the CacheBlock object.
        /// </summary>
        /// <returns>A pafClientCacheBlock object.</returns>
        public static pafClientCacheBlock CacheBlock { get; set; }

        /// <summary>
        /// Gets the PafMdbProps
        /// </summary>
        public static pafMdbProps MdbProps { get; set; }

        /// <summary>
        /// Get the GlobalStyles.
        /// </summary>
        /// <returns>A dictionary containing the GlobalStyles</returns>
        public static Dictionary<string, pafStyle> GetGlobalStyles()
        {
            if (_globalStyles == null)
            {
                BuildGlobalStyles(false, CacheBlock);
            }

            return _globalStyles;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void BuildGlobalStyles(bool reset = true, pafClientCacheBlock cacheBlock = null)
        {
            if (reset) _globalStyles = null;
            if (_globalStyles == null && cacheBlock != null && cacheBlock.globalStyles != null)
            {
                _globalStyles = new Dictionary<string, pafStyle>(cacheBlock.globalStyles.Length);

                foreach (pafStyle pafStyle in cacheBlock.globalStyles)
                {
                    if (_globalStyles.ContainsKey(pafStyle.name.ToLower()))
                    {
                        _globalStyles.Remove(pafStyle.name.ToLower());
                    }
                    _globalStyles.Add(pafStyle.name.ToLower(), pafStyle);
                }
            }
        }

        /// <summary>
        /// Gets a conditional style by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static abstractPaceConditionalStyle GetConditionalStyle(string name)
        {
            _conditionalStyles = GetConditionalStyles();
            if (_conditionalStyles == null) return null;
            abstractPaceConditionalStyle result;
            _conditionalStyles.TryGetValue(name, out result);
            return result;
        }

        /// <summary>
        /// Gets the ConditionalStyles
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, abstractPaceConditionalStyle> GetConditionalStyles()
        {
            if (_conditionalStyles == null)
            {
                BuildConditionalStyles(false, CacheBlock);
            }
            return _conditionalStyles;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void BuildConditionalStyles(bool reset = true, pafClientCacheBlock cacheBlock = null)
        {
            if (reset) _conditionalStyles = null;
            if (_conditionalStyles == null && cacheBlock != null)
            {
                _conditionalStyles = new Dictionary<string, abstractPaceConditionalStyle>(25);

                if (cacheBlock.iconStyleConditionalStyles != null)
                {
                    foreach (var style in cacheBlock.iconStyleConditionalStyles)
                    {
                        if (_conditionalStyles.ContainsKey(style.name))
                        {
                            _conditionalStyles.Remove(style.name);
                        }
                        _conditionalStyles.Add(style.name, style);
                    }
                }

                if (cacheBlock.dataBarConditionalStyles != null)
                {
                    foreach (var style in cacheBlock.dataBarConditionalStyles)
                    {
                        if (_conditionalStyles.ContainsKey(style.name))
                        {
                            _conditionalStyles.Remove(style.name);
                        }
                        _conditionalStyles.Add(style.name, style);
                    }
                }

                if (cacheBlock.colorScaleConditionalStyles != null)
                {
                    foreach (var style in cacheBlock.colorScaleConditionalStyles)
                    {
                        if (_conditionalStyles.ContainsKey(style.name))
                        {
                            _conditionalStyles.Remove(style.name);
                        }
                        _conditionalStyles.Add(style.name, style);
                    }
                }
            }
        }

        private static bool? _clusteringEnabled;

        public static bool ClusteringEnabled
        {
            get
            {
                if (_clusteringEnabled == null)
                {
                    try
                    {
                        string file = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
                        FileInfo fi = new FileInfo(file);
                        if (fi.DirectoryName != null && File.Exists(Path.Combine(fi.DirectoryName, "cluster.enable")))
                        {
                            _clusteringEnabled = true;
                        }
                        else
                        {
                            _clusteringEnabled = false;
                        }
                        return _clusteringEnabled.Value;
                    }
                    catch (Exception)
                    {
                        _clusteringEnabled = false;
                        return _clusteringEnabled.Value;
                    }
                }
                return _clusteringEnabled.Value;
            }
        }


        /// <summary>
        /// Gets a conditional format by name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static conditionalFormat GetConditionalFormat(string name)
        {
            _conditionalFormats = GetConditionalFormats();
            if (_conditionalFormats == null) return null;
            conditionalFormat result;
            _conditionalFormats.TryGetValue(name, out result);
            return result;
        }

        /// <summary>
        /// Gets the conditional formats
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, conditionalFormat> GetConditionalFormats()
        {
            if (_conditionalFormats == null)
            {
                BuildConditionalFormats(false, CacheBlock);
            }
            return _conditionalFormats;
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void BuildConditionalFormats(bool reset = true, pafClientCacheBlock cacheBlock = null)
        {
            if (reset) _conditionalFormats = null;
            if (_conditionalFormats == null && cacheBlock != null && cacheBlock.conditionalFormats != null)
            {
                _conditionalFormats = new Dictionary<string, conditionalFormat>(cacheBlock.conditionalFormats.Count());

                foreach (var style in cacheBlock.conditionalFormats)
                {
                    if (_conditionalFormats.ContainsKey(style.name))
                    {
                        _conditionalFormats.Remove(style.name);
                    }
                    _conditionalFormats.Add(style.name, style);
                }
            }
        }

        /// <summary>
        /// Returns a dictionary Custom Menu Defs
        /// </summary>
        /// <returns>A dictionary of CustomMenuDefs keyed by the customMenuDef key</returns>
        public static Dictionary<string, customMenuDef> CustomMenuDefs
        {
            get
            {
                if (_customMenuDefs == null)
                {
                    _customMenuDefs = new Dictionary<string, customMenuDef>();
                    foreach (customMenuDef menuDef in GetPafPlanSessionResponse().customMenuDefs)
                    {

                        _customMenuDefs.Add(menuDef.key, menuDef);
                    }
                }
                return _customMenuDefs;
            }

            set
            {
                _customMenuDefs = value;
            }
        }

        /// <summary>
        /// Gets the ChangedCellMngr(an object to track changed cell within the application)
        /// </summary>
        /// <returns>The ChangedCellMngr object.</returns>
        [DebuggerHidden]
        public static ChangedCellMngr GetChangedCellMngr()
        {
            return _changedCellsMngr ?? (_changedCellsMngr = new ChangedCellMngr());
        }

        /// <summary>
        /// Gets the ChangedCellMngr(an object to track changed cell within the application)
        /// </summary>
        /// <returns>The ChangedCellMngr object.</returns>
        [DebuggerHidden]
        public static Queue<ChangedCellInfo> GetTestHarnessChangedCellMngr()
        {
            return _testHarnessChangedCellsMngr ?? (_testHarnessChangedCellsMngr = new Queue<ChangedCellInfo>());
        }

        /// <summary>
        /// Gets the PafService object.
        /// </summary>
        /// <returns>The PafService object created in the init(). </returns>
        public static PafServiceProviderService GetPafService()
        {
            if (_pafService == null)
            {
                _pafService = new PafServiceProviderService();
                SetConnectionSettings(_pafService);
            }
            return _pafService;
        }

        /// <summary>
        /// Get the ViewTreeView.
        /// </summary>
        /// <returns>ViewTreeView Object</returns>
        [DebuggerHidden]
        public static ViewTreeView GetViewTreeView()
        {
            //If the tree view exists return it.
            if (_viewTreeView != null) return _viewTreeView;
            //Get a session response.
            var sessionResponse = GetPafPlanSessionResponse();
            //If the items are null, then we can't create so return null
            if (sessionResponse == null) return null;
            if (sessionResponse.viewTreeItems == null) return null;
            //We have all the necessary components, so create and return.
            _viewTreeView = new ViewTreeView(GetPafPlanSessionResponse().viewTreeItems);
            return _viewTreeView;


            //return _viewTreeView ?? (_viewTreeView = new ViewTreeView(GetPafPlanSessionResponse().viewTreeItems));
        }

        /// <summary>
        /// Get the MemberTagInfo object.
        /// </summary>
        /// <returns>MemberTagInfo Object</returns>
        [DebuggerHidden]
        public static MemberTagInfo GetMemberTagInfo()
        {
            return _memberTagInfo ?? (_memberTagInfo = new MemberTagInfo(CacheBlock.memberTagDefs));
            //_CacheBlock.memberTagDefs.Save<memberTagDef[]>(@"c:\", @"memberTagDef.xml");
            //_CacheBlock.Save<pafClientCacheBlock>(@"c:\", @"pafClientCacheBlock.xml");
        }

        /// <summary>
        /// Gets the global pafPlanSessionResponse.
        /// </summary>
        /// <returns>pafAuthResponse</returns>
        public static pafPlanSessionResponse GetPafPlanSessionResponse()
        {
            return _pafPlanSessionResponse;
        }

        /// <summary>
        /// Gets the global pafPopulateRoleFilterResponse
        /// </summary>
        /// <returns>pafPopulateRoleFilterResponse</returns>
        public static pafPopulateRoleFilterResponse GetPafPopulateRoleFilterResponse()
        {
            return _pafPopulateRoleFilterResponse;
        }

        /// <summary>
        /// Gets the global pafGetFilteredUOWSizeResponse
        /// </summary>
        /// <returns>pafGetFilteredUOWSizeResponse</returns>
        public static pafGetFilteredUOWSizeResponse GetPafGetFilteredUOWSizeResponse()
        {
            return _pafGetFilteredUowSizeResponse;
        }

        /// <summary>
        /// Gets the global PafAppSettings
        /// </summary>
        /// <returns>PafAppSettings</returns>
        public static appSettings GetPafAppSettings()
        {
            return _pafAuthResponse != null ? _pafAuthResponse.appSettings : null;
        }

        /// <summary>
        /// Gets the paf apps settings helper object.
        /// </summary>
        /// <returns>PafAppSettingsHelpers</returns>
        [DebuggerHidden]
        public static PafAppSettingsHelpers GetPafAppSettingsHelper()
        {
            return _pafAppSettingsHelper ?? (_pafAppSettingsHelper = new PafAppSettingsHelpers());
        }

        /// <summary>
        /// Gets the global pafAuthResponse, if one does not exist then it is created.
        /// </summary>
        /// <returns>pafAuthResponse</returns>
        public static pafAuthResponse GetPafAuthResponse()
        {
            return _pafAuthResponse ?? (_pafAuthResponse = new pafAuthResponse());
        }

        /// <summary>
        /// Public method to get the SaveWorkMngr object.
        /// </summary>
        /// <returns>A SaveWorkMngr object.</returns>
        [DebuggerHidden]
        public static SaveWorkMngr GetSaveWorkMngr()
        {
            return _saveWorkMngr ?? (_saveWorkMngr = new SaveWorkMngr());
        }

        /// <summary>
        /// Public method to get the Formatting manager object.
        /// </summary>
        /// <returns>FormattingMngr object.</returns>
        [DebuggerHidden]
        public static FormattingMngr GetFormattingMngr()
        {
            return _formattingMngr ?? (_formattingMngr = new FormattingMngr());
        }

        /// <summary>
        /// Public method to get the cell note cache.
        /// </summary>
        /// <returns>Cell note manager.</returns>
        [DebuggerHidden]
        public static CellNoteMngr GetCellNoteMngr()
        {
            return _cellNoteMngr ?? (_cellNoteMngr = new SimpleCellNoteMngr());
        }

        /// <summary>
        /// Public method to get the member tag mngr.
        /// </summary>
        /// <returns>Member tag cache.</returns>
        [DebuggerHidden]
        public static MemberTagMngr GetMbrTagMngr()
        {
            return _mbrTagMngr ?? (_mbrTagMngr = new MemberTagMngr());
        }

        /// <summary>
        /// Get the logger object.
        /// </summary>
        /// <returns>A Logging object for logging purposes.</returns>
        [DebuggerHidden]
        public static Logging GetLogger()
        {
            //Get the caller and pass it to the logger, so it shows up in the log.
            StackTrace st = new StackTrace();
            StackFrame fr = st.GetFrame(1);
            MethodBase m = fr.GetMethod();

            _logger = new Logging(m.DeclaringType.FullName + "." + m.Name);

            if (_logger == null)
            {
                _logger = new Logging(MethodBase.GetCurrentMethod().DeclaringType.ToString());
            }

            return _logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static UowCalculator GetUowCalculator()
        {
            return _uow ?? (_uow = new UowCalculator());
        }

        /// <summary>
        /// Public method to get the custom Titan message box.
        /// </summary>
        /// <returns>Titan.Pace.Application.Forms.MessageBox1</returns>
        public static frmMessageBox MessageBox()
        {
            return _messageBox ?? (_messageBox = new frmMessageBox());
        }

        /// <summary>
        /// Get the Localization object.
        /// </summary>
        /// <returns>An AppLocalization  object.</returns>
        [DebuggerHidden]
        public static AppLocalization GetLocalization()
        {
            return _localization ?? (_localization = new AppLocalization());
        }

        /// <summary>
        /// Gets the ViewMngr object.
        /// </summary>
        /// <returns>A ViewMngr object.</returns>
        [DebuggerHidden]
        public static ViewMngr GetViewMngr()
        {
            return _viewMngr ?? (_viewMngr = new ViewMngr());
        }

        /// <summary>
        /// Gets the CommandBarMngr object.
        /// </summary>
        /// <returns>A CommandBarMngr object.</returns>
        [DebuggerHidden]
        public static  CommandBarMngr GetCommandBarMngr()
        {
            return _commandBarMngr ?? (_commandBarMngr = new CommandBarMngr());
        }

        /// <summary>
        /// Gets the ExcelEventMgr object.
        /// </summary>
        /// <returns>A ExcelEventMgr object.</returns>
        [DebuggerHidden]
        public static ExcelEventMgr GetEventManager()
        {
            return _eventMgr ?? (_eventMgr = new ExcelEventMgr());
        }

        /// <summary>
        /// Gets a PafServiceMngr(which handles all the calls into the PafServiceLayer).
        /// </summary>
        /// <returns>A PafServiceMngr.</returns>
        [DebuggerHidden]
        public static PafServiceMngr GetServiceMngr()
        {
            return _serviceMngr ?? (_serviceMngr = new PafServiceMngr());
        }

        /// <summary>
        /// Gets the ExcelTestHarnessEventMgr object.
        /// </summary>
        /// <returns>A ExcelTestHarnessEventMgr object.</returns>
        [DebuggerHidden]
        public static TestHarnessMgr GetTestHarnessManager()
        {
            return _testHarnessMgr ?? (_testHarnessMgr = new TestHarnessMgr());
        }

        /// <summary>
        /// Get/Set if the application is in a StartUp state.
        /// </summary>
        public static bool StartUp { get; set; }

        /// <summary>
        /// Gets /set the new SimpleDimTrees object.
        /// This is a typed data set object to replace (SimpleTrees)
        /// </summary>
        public static SimpleDimTrees SimpleDimTrees { get; set; }

        /// <summary>
        /// Display message prompts.
        /// </summary>
        public static bool TestHarnessRunning
        {
            get { return _showMessagePrompts; }
            set { _showMessagePrompts = value; }
        }

        /// <summary>
        /// Get the AppInterface(ExcelAppImpl) object.
        /// </summary>
        /// <returns>An AppInterface object.</returns>
        [DebuggerHidden]
        public static IAppInterface GetGridApp()
        {
            return _gridApp ?? (_gridApp = new ExcelAppImpl());
        }

        /// <summary>
        /// Checks to see if the URL is valid and running.
        /// TTN-733
        /// </summary>
        /// <param name="url">The url path to check.  If url is null, will return false by default.</param>
        /// <returns>if url is online, value will be true otherwise false</returns>
        public static bool IsUrlLive(string url)
        {
            //Make this one based and not 0 so the math works out just fine.
            int count = 1;
            int timeout = Settings.Default.ServerURLTimeout;
            int retryAttempts = Settings.Default.ServerURLRetryAttempts;
            Uri pace = new Uri(url);
            GetLogger().InfoFormat("Host: {0}", pace.Host);
            GetLogger().InfoFormat("Port: {0}", pace.Port);
            GetLogger().InfoFormat("Schema: {0}", pace.Scheme);
            GetLogger().InfoFormat("Local Path: {0}", pace.LocalPath);

            //create a uribuilder
            UriBuilder servletUri = null;
            //Parse the local path to the first entry
            string[] localPath = pace.LocalPath.Split(new [] {"/"}, StringSplitOptions.RemoveEmptyEntries);
            //Something happened during the parse, so go back to the wsdl
            if (localPath.Length > 0)
            {
                //take the first entry in the local path and comine with the servlet name
                string newLocalPath = String.Concat(localPath[0], @"/", PafAppConstants.PACE_SERVLET_LOCAL_NAME);
                //Create the URI using the builder.
                servletUri = new UriBuilder(pace.Scheme, pace.Host, pace.Port, newLocalPath);
            }
            else
            {
                //Something happened so just use the old url
                servletUri = new UriBuilder(pace);   
            }

            //Will attempt to connect retryAttempts, doubling the timeout before each.
            do
            {
                int newTimeout = (timeout*count);
                if (IsUrlLivePrivate(servletUri.Uri, newTimeout))
                {
                    return true;
                }
                count++;
                GetLogger().InfoFormat("IsUrlLive failed, attempting reconnect with doubled timeout.");
            } while (count <= retryAttempts);
            GetLogger().InfoFormat("Cannot find server after: {0} attempts.  Giving up.", new object[] { (retryAttempts).ToString("N0") });
            return false;
        }

        /// <summary>
        /// Checks to see if the URL is valid and running.
        /// TTN-733
        /// </summary>
        /// <param name="uri">The url path to check.  If url is null, will return false by default.</param>
        /// <param name="timeout">Connection timeout.</param>
        /// <returns>if url is online, value will be true otherwise false</returns>
        public static bool IsUrlLivePrivate(Uri uri, int timeout = 3000)
        {
            bool urlIsLive = false;
            //if url is not null
            if (uri != null)
            {
                GetLogger().InfoFormat("Checking URL: {0}, with Timeout: {1} ms", new object[] { uri.ToString() , timeout.ToString("N0")});
                //create a webrequest
                WebRequest webRequest = WebRequest.Create(uri);

                //set the time
                webRequest.Credentials = CredentialCache.DefaultCredentials;
                webRequest.Method = "GET";
                webRequest.Timeout = timeout;


                //Try to connect to url.  If url isn't live, a WebException will be thrown.
                Stopwatch stopwatch = Stopwatch.StartNew();
                try
                {
                    //try to get response
                    using (webRequest.GetResponse())
                    {
                        urlIsLive = true;
                    }
                    stopwatch.Stop();
                    GetLogger().InfoFormat("IsUrlLive success. runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
                }
                catch (WebException e)
                {
                    stopwatch.Stop();
                    GetLogger().WarnFormat("IsUrlLive failed. message: {0}", new object[] { e.Message });
                    GetLogger().InfoFormat("IsUrlLive failed. runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
                }
            }

            //return result
            return urlIsLive;
        }
  
        private static void UpgradeRequired(String upgradeUrl)
        {

            string message = String.Format(GetLocalization().GetResourceManager().GetString("Application.MessageBox.UpgradeRequired"));
            DialogResult response = System.Windows.Forms.MessageBox.Show(message, GetLocalization().GetResourceManager().GetString("Application.Title"), MessageBoxButtons.OKCancel, MessageBoxIcon.Exclamation);

            if (response == DialogResult.OK)
            {
                // redirect to client url, and exit
                ProcessStartInfo procFormsBuilderStartInfo = new ProcessStartInfo
                                                                 {
                                                                     FileName = upgradeUrl,
                                                                     WindowStyle = ProcessWindowStyle.Maximized
                                                                 };
                Process procFormsBuilder = new Process {StartInfo = procFormsBuilderStartInfo};
                procFormsBuilder.Start();
                LogoffServer();
                EndPlanningSession();
                throw new OutOfDateClientException(GetLocalization().GetResourceManager().GetString("Application.Exception.OutOfDateClient"));
            }
            else
            {
                // cleanup and don't login
                LogoffServer();
                EndPlanningSession();
                throw new OutOfDateClientException(GetLocalization().GetResourceManager().GetString("Application.Exception.OutOfDateClient"));
            }
        }
    }
}