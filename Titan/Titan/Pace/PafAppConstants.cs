#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Drawing;
using System.IO;
using Titan.Properties;

namespace Titan.Pace
{
    internal static class PafAppConstants
    {
        // ReSharper disable InconsistentNaming

        /// <summary>
        /// Local name for the PafServer init servlet.
        /// </summary>
        public const string PACE_SERVLET_LOCAL_NAME = "PafInitServlet";

        /// <summary>
        /// Excel 11 and eairler file extension.
        /// </summary>
        public const string EXCEL_FILE_EXTENSION = ".xls";

        /// <summary>
        /// Excel 12 and later file extension.
        /// </summary>
        public const string EXCEL12_FILE_EXTENSION = ".xlsx";

        /// <summary>
        /// Titan Test Harness File Extension
        /// </summary>
        public const string TTH_FILE_EXTENSION = ".tth";

        /// <summary>
        /// Titan Test Harness batch file extension
        /// </summary>
        public const string BTTH_FILE_EXTENSION = ".btth";

        /// <summary>
        /// Our hidden view sheet property value.
        /// </summary>
        public const string OUR_VIEW_PROP_VALUE = "ViewName";

        /// <summary>
        /// Sheet password.
        /// </summary>
        public const string SHEET_PASSWORD = "adg";

        /// <summary>
        /// Max lenght of of charcters allowed in an Excel sheet name.
        /// </summary>
        public const int MAX_EXCEL_SHEET_NAME_LEN = 31;

        /// <summary>
        /// Test harness startup file name.
        /// </summary>
        public const string TEST_HARNESS_STARTUP_FILE_NAME = "TestHarnessStartup.xml";

        /// <summary>
        /// Excel 11 max column limit.
        /// </summary>
        public const int EXCEL11_COLUMN_LIMIT = 256;

        /// <summary>
        /// Excel 11 max row limit.
        /// </summary>
        public const int EXCEL11_ROW_LIMIT = 65536;

        /// <summary>
        /// Excel 12, 14 & 15 (2007, 2010, 2013) max column limit.
        /// </summary>
        public const int EXCEL12_COLUMN_LIMIT = 16384;

        /// <summary>
        /// Excel 12, 14 & 15 (2007, 2010, 2013) max row limit.
        /// </summary>
        public const int EXCEL12_ROW_LIMIT = 1048576;

        /// <summary>
        /// Excel 12, 14 & 15 (2007, 2010, 2013) max cell limit.
        /// </summary>
        public const long EXCEL12_MAX_CELL_COUNT = 17179869184;

        /// <summary>
        /// Max length of a formula in Excel 12, 14, & 15
        /// </summary>
        public const int EXCEL_FORMULA_LENGTH = 8192;

        /// <summary>
        /// Excel max number of non-contigous ranges.
        /// </summary>
        public const int EXCEL_MAX_NON_CONTIG_RANGE = 1023;

        /// <summary>
        /// Compressed element delimiter.
        /// </summary>
        public const string ELEMENT_DELIM_DEFAULT = "^";
        public static string ELEMENT_DELIM = "";

        /// <summary>
        /// Compressed group delimiter.
        /// </summary>
        public const string GROUP_DELIM_DEFAULT = "|^";
        public static string GROUP_DELIM = "";

        /// <summary>
        /// Carrige return + line feed.
        /// </summary>
        public const string CRLF = CR + NL;
        /// <summary>
        /// New line character.
        /// </summary>
        public const string NL = "\n";
        /// <summary>
        /// Carrarige retur.
        /// </summary>
        public const string CR = "\r";
        /// <summary>
        /// Tab.
        /// </summary>
        public const string TAB = "\t";

        /// <summary>
        /// A blank character.
        /// </summary>
        public const char BLANK_CHAR = ' ';

        /// <summary>
        /// Default width for the cell note.
        /// </summary>
        public const int DEFAULT_CELL_NOTE_WIDTH = 207;

        /// <summary>
        /// Default height for the cell note.
        /// </summary>
        public const int DEFAULT_CELL_NOTE_HEIGHT = 77;

        /// <summary>
        /// Default value for a intersection coordinate that has not been initalized.
        /// </summary>
        public const string INTERSECTION_COORDINATE_NOT_INIT  = "[not intialized]";

        /// <summary>
        /// Place holder for and invalid member tag cell, used for sorting.
        /// </summary>
        public const string INVALID_MEMBER_TAG_CELL = "_INVALID_MT_CELL_";

        /// <summary>
        /// Default year for the Time Horizon dimension
        /// </summary>
	    public const string TIME_HORIZON_DEFAULT_YEAR = "**YEAR.NA**";
	    
        /// <summary>
        /// Name of the Time Horizon dimension
        /// </summary>
        public const string TIME_HORIZON_DIM = "**TIME.HORIZON**";
	    
        /// <summary>
        /// Default delimiter for a member in the Time Horizon dimension
        /// Period Delimiter Time
        /// </summary>
        public const string TIME_HORIZON_MBR_DELIM = "||";

        /// <summary>
        /// Lenght of TIME_HORIZON_MBR_DELIM
        /// </summary>
        public static int TIME_HORIZON_MBR_DELIM_LEN = TIME_HORIZON_MBR_DELIM.Length;

        /// <summary>
        /// Member name for a PafBlank
        /// </summary>
        public static string PAF_BLANK = Settings.Default.Blank;

        /// <summary>
        /// 
        /// </summary>
        public const string SERVER_LOGON_SPEC = "Steak Dinner";

        /// <summary>
        /// File name for saved role filter selections
        /// </summary>
        public const string ROLE_FILTER_FILE_NAME = "rem_role_filter.xml";

        /// <summary>
        /// File name of saved user selector selections
        /// </summary>
        public const string USER_SEL_FILE_NAME = "rem_user_sel.xml";

        /// <summary>
        /// Open Pace sub folder.
        /// </summary>
        private const string OPEN_PACE_FOLDER = "Open Pace";

        /// <summary>
        /// Pace Client subfolder
        /// </summary>
        private const string PACE_CLIENT_FOLDER = "Pace Client";

        /// <summary>
        /// Sub folder to store settings files
        /// </summary>
        private const string SETTINGS_FOLDER = "settings";

        /// <summary>
        /// Gets the full path to the settings folder.
        /// </summary>
        public static string SETTINGS_PATH = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), OPEN_PACE_FOLDER, PACE_CLIENT_FOLDER, SETTINGS_FOLDER);

        /// <summary>
        /// Replicate format leading symbols.
        /// </summary>
        public static string REPLICATE_LEADING_SYMBOLS = 
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.Symbols.Leading.Cells");

        /// <summary>
        /// Replicate all format string (string to use in the grid).
        /// </summary>
        public static string REPLICATE_ALL_FORMAT_SUFFIX = 
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.All.Format.Suffix");
        
        /// <summary>
        /// Replicate existing format string (string to use in the grid).
        /// </summary>
        public static string REPLICATE_EXISTING_FORMAT_SUFFIX =
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.Existing.Format.Suffix");

        /// <summary>
        /// Replicate regex string (handles both r and ra).
        /// </summary>
        public static string REPLICATE_REGEX_STRING =
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.Regex.String");

        /// <summary>
        /// Replicate regex string (handles only ra)
        /// </summary>
        public static string REPLICATE_REGEX_STRING_RA =
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.Regex.String.RA");

        /// <summary>
        /// Replicate regex string (handles only r)
        /// </summary>
        public static string REPLICATE_REGEX_STRING_R =
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.Regex.String.R");

        /// <summary>
        /// Replicate regex string (handles both r or ra).
        /// </summary>
        public static string REPLICATE_REGEX_STRING_R_OR_A =
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.Regex.String.R.or.RA");

        /// <summary>
        /// Lift format leading symbols.
        /// </summary>
        public static string LIFT_LEADING_SYMBOLS = 
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Lift.Symbols.Leading.Cells");

        /// <summary>
        /// Lift all format string (string to use in the grid).
        /// </summary>
        public static string LIFT_ALL_FORMAT_SUFFIX = PafApp.GetLocalization().GetResourceManager().GetString("Application.Lift.All.Format.Suffix");

        /// <summary>
        /// Lift existing format string (string to use in the grid).
        /// </summary>
        public static string LIFT_EXISTING_FORMAT_SUFFIX = PafApp.GetLocalization().GetResourceManager().GetString("Application.Lift.Existing.Format.Suffix");

        /// <summary>
        /// Lift regex string (handles both l and la).
        /// </summary>
        public static string LIFT_REGEX_STRING = PafApp.GetLocalization().GetResourceManager().GetString("Application.Lift.Regex.String");

        /// <summary>
        /// Lift regex string (handles only la)
        /// </summary>
        public static string LIFT_REGEX_STRING_LA = PafApp.GetLocalization().GetResourceManager().GetString("Application.Lift.Regex.String.LA");

        /// <summary>
        /// Lift regex string (handles only l)
        /// </summary>
        public static string LIFT_REGEX_STRING_L = PafApp.GetLocalization().GetResourceManager().GetString("Application.Lift.Regex.String.L");

        /// <summary>
        /// Lift regex string (handles both l or la).
        /// </summary>
        public static string LIFT_REGEX_STRING_L_OR_LA = PafApp.GetLocalization().GetResourceManager().GetString("Application.Lift.Regex.String.L.or.LA");

        /// <summary>
        /// Default Essbase alias table name.
        /// </summary>
        public static string ALIAS_DEFAULT_TABLE_NAME = 
            PafApp.GetLocalization().GetResourceManager().GetString("Application.Essbase.Default.AliasTable");

        /// <summary>
        /// ASTERISK
        /// </summary>
        public static string ASTERISK =
            PafApp.GetLocalization().GetResourceManager().GetString("Application.General.Asterisk");

        /// <summary>
        /// Foward slash.
        /// </summary>
        public static string FOWARD_SLASH =
            PafApp.GetLocalization().GetResourceManager().GetString("Application.General.FowardSlash");

        /// <summary>
        /// Alias mapping format member for the defaul alias table.
        /// </summary>
        public const string ALIAS_MAPPING_FORMAT_ALIAS = "alias";

        /// <summary>
        /// Alias mapping format member, this is a server constant.
        /// </summary>
        public const string ALIAS_MAPPING_FORMAT_MEMBER = "member";

        /// <summary>
        /// Member tag type of text, this is a server constant.
        /// </summary>
        public const string MEMBER_TAG_TYPE_TEXT = "TX";

        /// <summary>
        /// Member tag type of formula, this is a server constant.
        /// </summary>
        public const string MEMBER_TAG_TYPE_HYPERLINK = "HY";

        /// <summary>
        /// Member tag type of formula, this is a server constant.
        /// </summary>
        public const string MEMBER_TAG_TYPE_FORMULA = "FO";

        /// <summary>
        /// String constant for 'UOW'
        /// </summary>
        public const string UOW_STRING = "UOW";

        public enum ExcelGridErrEnum
        {
            ErrDiv0 = -2146826281,
            ErrNA = -2146826246,
            ErrName = -2146826259,
            ErrNull = -2146826288,
            ErrNum = -2146826252,
            ErrRef = -2146826265,
            ErrValue = -2146826273
        } ;

        //default colors, only used if non are sent from server.
        public static Color FORWARD_PLANNABLE_PROTECTED_COLOR = Color.Coral;
        public static Color NON_PLANNABLE_PROTECTED_COLOR = Color.Teal;
        public static Color PROTECTED_COLOR = Color.Orange;
        public static Color SYSTEM_LOCK_COLOR = Color.Yellow;
        public static Color USER_LOCK_COLOR = Color.Aqua;
        public static Color MEMBER_TAG_COLOR = Color.LightBlue;
        //Excel color constants.
        public static int xlColorIndexNone = -4142;
        public static int xlColorIndexAutomatic = -4105;

        // ReSharper restore InconsistentNaming
    }
}
