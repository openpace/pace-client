﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Microsoft.Office.Interop.Excel;
using Titan.PafService;

namespace Titan.Pace
{
    internal enum MeasureType
    {
        /// <summary>
        /// Constant for TIMEBALFIRST
        /// </summary>
        TimeBalFirst,
        /// <summary>
        /// Constant for TIMEBALLAST
        /// </summary>
        TimeBalLast,
        /// <summary>
        /// Constant for FORWARDPLANNABLE
        /// </summary>
        ForwardPlannable,
        /// <summary>
        /// Constant for Aggregate Mesaure Type "AGGREGATE"
        /// </summary>
        Aggregate,
        /// <summary>
        /// Constant for RECALC
        /// </summary>
        Recalc,
        /// <summary>
        /// 
        /// </summary>
        None
    };

    internal enum FunctionType
    {
        /// <summary>
        /// Constant for @NEXT
        /// </summary>
        AtNext,
        /// <summary>
        /// Constant for @PREV
        /// </summary>
        AtPrev,
        /// <summary>
        /// 
        /// </summary>
        None
    };

    internal enum ServiceCallType
    {
        ClientAuth,
        PlanningSession,
        Role,
        SeasonProcess
    };

    /// <summary>
    /// Type of replication that's in the cell.
    /// </summary>
    internal enum ReplicationType
    {
        /// <summary>
        /// Replicat to all intersections
        /// </summary>
        ReplicateAll, 
        /// <summary>
        /// Replicate only to existing intersections.
        /// </summary>
        ReplicateExisting, 
        /// <summary>
        /// No replication exists in the current intersection.
        /// </summary>
        None
    };

    /// <summary>
    /// Type of Lift that's in the cell.
    /// </summary>
    internal enum LiftType
    {
        /// <summary>
        /// Lift to all intersections
        /// </summary>
        LiftAll,
        /// <summary>
        /// Lift only to existing intersections.
        /// </summary>
        LiftExisting,
        /// <summary>
        /// No Lift exists in the current intersection.
        /// </summary>
        None
    };

    /// <summary>
    /// Position of item on the view.
    /// </summary>
    internal enum ViewAxis
    {
        /// <summary>
        /// Column
        /// </summary>
        Col = 1,

        /// <summary>
        /// Row
        /// </summary>
        Row = 0,

        /// <summary>
        /// Page
        /// </summary>
        Page = 2,

        /// <summary>
        /// Unknon, Nothing, Blank, You get the idea.
        /// </summary>
        Unknown = -1
    };

    /// <summary>
    /// Type of lift that's in the cell.
    /// </summary>
    internal enum AllocationType
    {
        /// <summary>
        /// Replicat to all intersections
        /// </summary>
        All,
        /// <summary>
        /// Replicate only to existing intersections.
        /// </summary>
        Existing, 
        /// <summary>
        /// No Lift exists in the current intersection.
        /// </summary>
        None
    };

    internal enum ConditionValueTypes
    {
        None = -1,
        Number = 0,
        LowestValue = 1,
        HighestValue = 2,
        Percent = 3,
        Formula = 4,
        Percentile = 5,
    };

    /// <summary>
    /// 
    /// </summary>
    internal enum ConditionalStyleType
    {
        ColorScale,
        IconSet,
        DataBar,
        None
    };

    internal enum ColorScaleType
    {
        TwoColor,
        ThreeColor
    };

    internal enum DataBarCriteriaType
    {
        Automatic,
        Lowest_Value,
        Number,
        Percent,
        Percentile
    };

    internal enum IconStyleType
    {
        Style1,
        Style2,
        Style3
    };

    internal enum IconStyleCriteriaNumType { Three, Four, Five };
    internal enum IconStyleCriteriaType { Number, Percent, Percentile };

    internal enum DataBarAppearanceFillType { Gradient, Solid };
    internal enum DataBarAppearanceBorderType { None, Solid };
    internal enum DataBarAppearanceBarDirectionType { Context, Right_to_Left, Left_to_Right };

    /// <summary>
    /// Dimension information type (Generation or Level)
    /// </summary>
    public enum DimensionInfoType
    {
        Level,
        Generation
    };

    /// <summary>
    /// 
    /// </summary>
    internal enum LevelGenerationType
    {
        Level,
        Generation,
        Bottom,
        Unknown
    };

    /// <summary>
    /// 
    /// </summary>
    internal enum SavedCleared
    {
        Saved,
        Cleared,
        Updated
    };

    /// <remarks/>
    public static class EnumExtensions
    {
        internal static LevelGenerationType ToLevelGenerationType(this levelGenType type)
        {
            switch (type)
            {
                case levelGenType.GEN:
                    return LevelGenerationType.Generation;
                case levelGenType.LEVEL:
                    return LevelGenerationType.Level;
            }
            return LevelGenerationType.Unknown;
        }

        internal static ReplicationType ToReplicationType(this AllocationType type)
        {
            switch (type)
            {
                case AllocationType.All:
                    return ReplicationType.ReplicateAll;
                case AllocationType.Existing:
                    return ReplicationType.ReplicateExisting;
                default:
                    return ReplicationType.None;
            }
        }

        internal static AllocationType ToAllocationType(this ReplicationType type)
        {
            switch (type)
            {
                case ReplicationType.ReplicateAll:
                    return AllocationType.All;
                case ReplicationType.ReplicateExisting:
                    return AllocationType.Existing;
                default:
                    return AllocationType.None;
            }
        }

        internal static LiftType ToLiftType(this AllocationType type)
        {
            switch (type)
            {
                case AllocationType.All:
                    return LiftType.LiftAll;
                case AllocationType.Existing:
                    return LiftType.LiftExisting;
                default:
                    return LiftType.None;
            }
        }

        internal static AllocationType ToAllocationType(this LiftType type)
        {
            switch (type)
            {
                case LiftType.LiftAll:
                    return AllocationType.All;
                case LiftType.LiftExisting:
                    return AllocationType.Existing;
                default:
                    return AllocationType.None;
            }
        }

        internal static XlConditionValueTypes ToXlConditionValueTypes(this ConditionValueTypes type)
        {
            switch (type)
            {
                case ConditionValueTypes.Formula:
                    return XlConditionValueTypes.xlConditionValueFormula;
                case ConditionValueTypes.HighestValue:
                    return XlConditionValueTypes.xlConditionValueHighestValue;
                case ConditionValueTypes.LowestValue:
                    return XlConditionValueTypes.xlConditionValueLowestValue;
                case ConditionValueTypes.None:
                    return XlConditionValueTypes.xlConditionValueNone;
                case ConditionValueTypes.Number:
                    return XlConditionValueTypes.xlConditionValueNumber;
                case ConditionValueTypes.Percent:
                    return XlConditionValueTypes.xlConditionValuePercent;
                case ConditionValueTypes.Percentile:
                    return XlConditionValueTypes.xlConditionValuePercentile;
                default:
                    return XlConditionValueTypes.xlConditionValueNone;
            }
        }

        
    }
}