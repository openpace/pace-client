﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Base.Data
{
    /// <summary>
    /// An alternative to the Intersection object.
    /// This object has a top level Dimension array and 
    /// a set of Coordinate objects.
    /// </summary>
    internal class CoordinateSet
    {
        /// <summary>
        /// Dimension(Axis) of the Coordinates
        /// </summary>
        public string[] Dimensions { get; set; }

        /// <summary>
        /// Set of Coordinates
        /// </summary>
        private HashSet<Coordinate> Coordinates { get; set; }

        /// <summary>
        /// Default Constructor.  Inits an empty set of Coordinates, does not init the Dimensions array.
        /// </summary>
        public CoordinateSet()
        {
            Coordinates = new HashSet<Coordinate>();
        }

        /// <summary>
        /// Constructor.  Inits and empty Set of Coordinates.
        /// </summary>
        /// <param name="dimensions">Dimensions</param>
        public CoordinateSet(string[] dimensions)
        {
            Dimensions = new string[dimensions.Length];
            Array.Copy(dimensions, Dimensions, dimensions.Length);
            Coordinates = new HashSet<Coordinate>();
        }

        /// <summary>
        /// Constructor.  Inits and empty Set of Coordinates.
        /// </summary>
        /// <param name="dimensions">Dimensions</param>
        public CoordinateSet(IEnumerable<String> dimensions)
            : this(dimensions.ToArray())
        {
        }


        //public CoordinateSet(CoordinateSet coordinateSet)
        //{
        //    Dimensions = coordinateSet.Dimensions;
        //    Coordinates = coordinateSet.Coordinates;
        //}

        /// <summary>
        /// Creates and adds a new Coordinte to the set.
        /// </summary>
        /// <param name="coordinates"></param>
        public void AddCoordinate(string[] coordinates)
        {
            Coordinates.Add(new Coordinate(coordinates));
        }

        /// <summary>
        /// Adds a new Coordinate to the set.
        /// </summary>
        /// <param name="intersection"></param>
        public void AddCoordinate(Intersection intersection)
        {
            if (Dimensions == null)
            {
                Dimensions = new string[intersection.AxisSequence.Length];
                Array.Copy((Array) intersection.AxisSequence, (Array) Dimensions, (int) intersection.AxisSequence.Length);
            }
            Coordinates.Add(new Coordinate(intersection.Coordinates));
        }

        /// <summary>
        /// Adds a new Coordinate to the set.
        /// </summary>
        /// <param name="coordinates"></param>
        public void AddCoordinate(Coordinate coordinates)
        {
            Coordinates.Add(coordinates);
        }

        /// <summary>
        /// Adds an Collection of Coordinates to the set.
        /// </summary>
        /// <param name="coordinates"></param>
        public void AddCoordinates(IEnumerable<Coordinate> coordinates)
        {
            Coordinates.UnionWith(coordinates);
        }

        /// <summary>
        /// Removes a Coordinate from the set.
        /// </summary>
        /// <param name="item"></param>
        public void RemoveCoordinates(Coordinate item)
        {
            Coordinates.Remove(item);
        }

        /// <summary>
        /// Returns true/false if a Coordinate exists in the set.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool ContainsCoordinate(Coordinate item)
        {
            return Coordinates.Contains(item);
        }

        /// <summary>
        /// Returns true/false if an Intersection exists in the set.
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool ContainsIntersection(Intersection item)
        {
            return ContainsCoordinate(new Coordinate(item.Coordinates));
        }

        /// <summary>
        /// Gets a List of Coordinates.
        /// </summary>
        /// <returns></returns>
        public List<Coordinate> GetCoordinates()
        {
            return Coordinates.ToList();
        }

        /// <summary>
        /// Gets a count of Coordinates.
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return Coordinates.Count;
        }

        /// <summary>
        /// Converts the CoordinateSet to a List of Intersections.
        /// </summary>
        /// <returns></returns>
        public List<Intersection> ToIntersections()
        {
            List<Intersection> temp = new List<Intersection>(Coordinates.Count);
            temp.AddRange(Coordinates.Select(c => new Intersection(Dimensions, c.Coordinates)));
            return temp;
        }

        //public List<Coordinate> GetDuplicateItems()
        //{
        //    return Coordinates.GroupBy(x => x).Where(x => x.Count() > 1).Select(x => x.Key).ToList();
        //}
    }
}