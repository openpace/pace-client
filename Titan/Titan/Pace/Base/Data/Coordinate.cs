﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Titan.Pace.Application.Extensions;

namespace Titan.Pace.Base.Data
{
    internal class Coordinate : ICloneable, IEquatable<Coordinate>
    {
        /// <summary>
        /// String array of Coordinates.
        /// </summary>
        public string[] Coordinates { get; set; }

        /// <summary>
        /// Constructor.  Inits the array of Coordinates.
        /// </summary>
        /// <param name="coordinate"></param>
        public Coordinate(string[] coordinate)
        {
            Coordinates = new string[coordinate.Length];
            Array.Copy(coordinate, Coordinates, coordinate.Length);
        }

        /// <summary>
        /// Constructor.  Inits the array of Coordinates.
        /// </summary>
        /// <param name="coordinate"></param>
        public Coordinate(IEnumerable<String> coordinate)
            : this(coordinate.ToArray())
        {
        }

        /// <summary>
        /// Constructor.  Inits an empty string array of Coordinates.
        /// </summary>
        /// <param name="arraySize"></param>
        public Coordinate(int arraySize)
        {
            Coordinates = new string[arraySize];
        }

        /// <summary>
        /// Returns the dimension member for a given dimension 
        /// </summary>
        /// <param name="axis">dimension</param>
        /// <returns>dimension member</returns>
        public string GetCoordinate(int axis)
        {
            if (axis < Coordinates.Length && axis > -1)
                return Coordinates[axis];

            return null;

        }

        /// <summary>
        /// Sets a string coordinate in the coordinate array.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="coordinate"></param>
        public void SetCoordinate(int axis, string coordinate)
        {
            Coordinates[axis] = coordinate;
        }


        /// <summary>
        /// Sets the string array of coordinates.
        /// </summary>
        /// <param name="coordinates"></param>
        public void SetCoordinates(string[] coordinates)
        {
            Coordinates = new string[coordinates.Length];
            Array.Copy(coordinates, Coordinates, coordinates.Length);
        }

        /// <summary>
        /// Sets the array of coordinates.
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="value"></param>
        public void SetCoordinates(int axis, String value)
        {
            if (Coordinates == null)
            {
                throw new ArgumentException("Can't set coordinate if [coordinates] property isn't initialized");
            }

            if (axis >= Coordinates.Length || axis < 0)
            {
                throw new ArgumentException("Invalid axis of [" + axis + "] passed to setCoordinate(axis, value)");
            }

            Coordinates[axis] = value;
        }


        /// <summary>
        /// Converts the object to a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return String.Format("{0}{1}{2}", new object[] { "{", UniqueString, "}" });
        }

        /// <summary>
        /// Converts the Coordinates to a comma delimited string.
        /// </summary>
        /// <returns></returns>
        public string UniqueString
        {
            get
            {
                return String.Join(", ", Coordinates);
            }
        }

        /// <summary>
        /// Return the UniqueId or the HashCode.
        /// </summary>
        public int UniqueId
        {
            get
            {
                return UniqueString.GetHashCode();
            }
        }

        /// <summary>
        /// Creates a deep clone of the Coordinates object 
        /// </summary>
        /// <returns></returns>
        [DebuggerHidden]
        public object Clone()
        {
            return DeepClone();
        }

        /// <summary>
        /// Creates a deep clone of the Coordinates object
        /// </summary>
        /// <returns></returns>
        [DebuggerHidden]
        public Coordinate DeepClone()
        {
            return new Coordinate(Coordinates);
        }

        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        [DebuggerHidden]
        public override bool Equals(object obj)
        {
            if (!(obj is Coordinate)) return false;

            Coordinate temp = (Coordinate)obj;

            return Equals(temp);
        }

        /// <summary>
        /// Return the hashcode of the Intersection object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        [DebuggerHidden]
        public override int GetHashCode()
        {
            return Coordinates.Aggregate(17, (current, t) => 37 * current + t.GetHashCode());
        }

        /// <summary>
        /// Compares an Coordinates to another.
        /// </summary>
        /// <param name="other"></param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public bool Equals(Coordinate other)
        {
            return Coordinates.ArrayEqual(other.Coordinates);
        }
    }
}