﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Base.Data
{
    internal class CoordinateSets
    {
        private readonly List<CoordinateSet> _coordinateSets;
        private readonly Dictionary<string, int> _keys = new Dictionary<string, int>(5);

        public CoordinateSets()
        {
            _coordinateSets = new List<CoordinateSet>(5);
        }

        public void AddCoordinate(string[] dimensions, string[] coordinates)
        {
            string key = String.Join(",", dimensions);

            int val;
            
            if (!_keys.TryGetValue(key, out val))
            {
                //Add a new coordinate set.
                _coordinateSets.Add(new CoordinateSet(dimensions));
                _keys.Add(key, _coordinateSets.Count - 1);
                val = _coordinateSets.Count - 1;
            }
            //Then get it.
            _coordinateSets[val].AddCoordinate(coordinates);
        }

        public void AddCoordinate(string[] dimensions, IEnumerable<Coordinate> coordinates)
        {
            string key = String.Join(",", dimensions);

            int val;

            if (!_keys.TryGetValue(key, out val))
            {
                //Add a new coordinate set.
                _coordinateSets.Add(new CoordinateSet(dimensions));
                _keys.Add(key, _coordinateSets.Count - 1);
                val = _coordinateSets.Count - 1;
            }
            //Then get it.
            _coordinateSets[val].AddCoordinates(coordinates);
        }


        public List<Intersection> AsIntersections()
        {
            return null;
        }
    }
}
