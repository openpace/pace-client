#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Resources;
using System.Threading;
using Titan.Properties;

namespace Titan.Pace.Localization
{
    internal class AppLocalization
    {
        private ResourceManager _ResourceManager;

        /// <summary>
        /// Constructor.
        /// </summary>
        [DebuggerHidden]
        public AppLocalization()
        {
            _ResourceManager = new ResourceManager("Titan.Pace.Localization.Titan", 
                Assembly.GetExecutingAssembly());
        }

        /// <summary>
        /// Public method to get Resource Manager.
        /// </summary>
        /// <returns>The PafApp global Resource Manager</returns>
        [DebuggerHidden]
        public ResourceManager GetResourceManager()
        {
            if (_ResourceManager == null)
            {
                _ResourceManager =
                    new ResourceManager("Titan.Pace.Localization.Titan",
                    Assembly.GetExecutingAssembly());
            }
            Thread.CurrentThread.CurrentUICulture = GetCurrentCulture();

            return _ResourceManager;
        }

        /// <summary>
        /// Gets the culture information.
        /// </summary>
        /// <returns>The current culture setting of the machine.</returns>
        [DebuggerHidden]
        public CultureInfo GetCurrentCulture()
        {
            if (_ResourceManager.GetResourceSet(new CultureInfo(Settings.Default.Language), true, true) == null)
                return new CultureInfo("en-US");
            else
                return new CultureInfo(Settings.Default.Language);
        }
    }
}
