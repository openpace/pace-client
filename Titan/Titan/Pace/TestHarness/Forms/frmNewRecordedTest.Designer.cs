namespace Titan.Pace.TestHarness.Forms
{
    /// <summary>
    /// 
    /// </summary>
    partial class frmNewRecordedTest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.lblTestName = new System.Windows.Forms.Label();
            this.txtTestName = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.butNo = new System.Windows.Forms.RadioButton();
            this.butYes = new System.Windows.Forms.RadioButton();
            this.lblRole = new System.Windows.Forms.Label();
            this.lblSeason = new System.Windows.Forms.Label();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOk = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtTestDescription = new System.Windows.Forms.TextBox();
            this.lblTestDescription = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTestName
            // 
            this.lblTestName.AutoSize = true;
            this.lblTestName.Location = new System.Drawing.Point(6, 19);
            this.lblTestName.Name = "lblTestName";
            this.lblTestName.Size = new System.Drawing.Size(13, 13);
            this.lblTestName.TabIndex = 0;
            this.lblTestName.Text = "_";
            // 
            // txtTestName
            // 
            this.txtTestName.Location = new System.Drawing.Point(5, 35);
            this.txtTestName.MaxLength = 20;
            this.txtTestName.Name = "txtTestName";
            this.txtTestName.Size = new System.Drawing.Size(201, 20);
            this.txtTestName.TabIndex = 0;
            this.txtTestName.Validating += new System.ComponentModel.CancelEventHandler(this.txtTestName_Validating);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.butNo);
            this.groupBox1.Controls.Add(this.butYes);
            this.groupBox1.Location = new System.Drawing.Point(5, 132);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(201, 66);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // butNo
            // 
            this.butNo.AutoSize = true;
            this.butNo.Checked = true;
            this.butNo.Location = new System.Drawing.Point(6, 38);
            this.butNo.Name = "butNo";
            this.butNo.Size = new System.Drawing.Size(14, 13);
            this.butNo.TabIndex = 2;
            this.butNo.TabStop = true;
            this.butNo.UseVisualStyleBackColor = true;
            // 
            // butYes
            // 
            this.butYes.AutoSize = true;
            this.butYes.Location = new System.Drawing.Point(6, 19);
            this.butYes.Name = "butYes";
            this.butYes.Size = new System.Drawing.Size(14, 13);
            this.butYes.TabIndex = 3;
            this.butYes.TabStop = true;
            this.butYes.UseVisualStyleBackColor = true;
            // 
            // lblRole
            // 
            this.lblRole.AutoSize = true;
            this.lblRole.Location = new System.Drawing.Point(6, 211);
            this.lblRole.Name = "lblRole";
            this.lblRole.Size = new System.Drawing.Size(13, 13);
            this.lblRole.TabIndex = 2;
            this.lblRole.Text = "_";
            // 
            // lblSeason
            // 
            this.lblSeason.AutoSize = true;
            this.lblSeason.Location = new System.Drawing.Point(6, 234);
            this.lblSeason.Name = "lblSeason";
            this.lblSeason.Size = new System.Drawing.Size(13, 13);
            this.lblSeason.TabIndex = 3;
            this.lblSeason.Text = "_";
            // 
            // cmdCancel
            // 
            this.cmdCancel.CausesValidation = false;
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(138, 270);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdOk
            // 
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(57, 270);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 4;
            this.cmdOk.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtTestDescription);
            this.groupBox2.Controls.Add(this.lblSeason);
            this.groupBox2.Controls.Add(this.txtTestName);
            this.groupBox2.Controls.Add(this.groupBox1);
            this.groupBox2.Controls.Add(this.lblRole);
            this.groupBox2.Controls.Add(this.lblTestName);
            this.groupBox2.Controls.Add(this.lblTestDescription);
            this.groupBox2.Location = new System.Drawing.Point(2, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(211, 261);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            // 
            // txtTestDescription
            // 
            this.txtTestDescription.Location = new System.Drawing.Point(5, 81);
            this.txtTestDescription.MaxLength = 255;
            this.txtTestDescription.Multiline = true;
            this.txtTestDescription.Name = "txtTestDescription";
            this.txtTestDescription.Size = new System.Drawing.Size(201, 45);
            this.txtTestDescription.TabIndex = 1;
            // 
            // lblTestDescription
            // 
            this.lblTestDescription.AutoSize = true;
            this.lblTestDescription.Location = new System.Drawing.Point(6, 65);
            this.lblTestDescription.Name = "lblTestDescription";
            this.lblTestDescription.Size = new System.Drawing.Size(13, 13);
            this.lblTestDescription.TabIndex = 5;
            this.lblTestDescription.Text = "_";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // frmNewRecordedTest
            // 
            this.AcceptButton = this.cmdOk;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(220, 295);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.cmdCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmNewRecordedTest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblTestName;
        private System.Windows.Forms.TextBox txtTestName;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblSeason;
        private System.Windows.Forms.Label lblRole;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.RadioButton butNo;
        private System.Windows.Forms.RadioButton butYes;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label lblTestDescription;
        private System.Windows.Forms.TextBox txtTestDescription;
    }
}