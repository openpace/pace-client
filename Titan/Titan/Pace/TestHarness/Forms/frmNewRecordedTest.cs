#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace Titan.Pace.TestHarness.Forms
{
    internal partial class frmNewRecordedTest : Form
    {
        private string _Role;
        private string _Season;

        /// <summary>
        /// Current role for the user.
        /// </summary>
        public string Role
        {
            get { return lblRole.Text; }
            set 
            { 
                _Role = value;
                lblRole.Text = PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.ActionsPane.Header.RoleLabel") + " " + value;
            }
        }
        
        /// <summary>
        /// Current season for the user.
        /// </summary>
        public string SeasonProcess
        {
            get { return lblSeason.Text; }
            set 
            {
                _Season = value;
                lblSeason.Text = PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.ActionsPane.Header.SeasonLabel") +  " " + value;
            }
        }
        
        /// <summary>
        /// Unique name for the recorded test.
        /// </summary>
        public string TestName
        {
            get 
            { 
                return txtTestName.Text; 
            }
            set
            {
                txtTestName.Text = value; 

            }
        }

        /// <summary>
        /// Description for the recorded test.
        /// </summary>
        public string TestDescription
        {
            get { return txtTestDescription.Text; }
            set { txtTestDescription.Text = value; }
        }
        
        /// <summary>
        /// Reload the datacache before running the test.
        /// </summary>
        public bool Reload
        {
            get 
            {
                if (butNo.Checked)
                    return false;
                else
                    return true;
            }
            set
            {
                if (! value)
                    butNo.Checked = true;
                else
                    butYes.Checked = true;
            }
        }

        /// <summary>
        /// COnstructor.
        /// </summary>
        public frmNewRecordedTest()
        {
            InitializeComponent();
            loadUi();
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="testName">Unique name for the recorded test.</param>
        /// <param name="reload">Reload the datacache before running the test.</param>
        /// <param name="role">Current role for the user.</param>
        /// <param name="season">Current season for the user.</param>
        public frmNewRecordedTest(string testName, bool reload, string role, string season)
        {
            InitializeComponent();
            loadUi();
            TestName = testName;
            Reload = reload;
            Role = role;
            SeasonProcess = season;
        }

        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                //Get the labels from the localized .resx file.
                Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                butNo.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.No");
                butYes.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Yes");
                Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Forms.frmNewRecordedTest.Caption");
                groupBox1.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Forms.frmNewRecordedTest.RefreshDataCache");
                groupBox2.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Forms.frmNewRecordedTest.TestParms");
                lblTestName.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Forms.frmNewRecordedTest.TestName");
                lblTestDescription.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Forms.frmNewRecordedTest.TestDescription");
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Event handler for the test name validate call.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTestName_Validating(object sender, CancelEventArgs e)
        {
            errorProvider1.Clear();

            Match match = Regex.Match(txtTestName.Text, @"[:/[\\\?\*\]]");

            if (match.Length > 0 || txtTestName.Text.Equals(""))
            {

                errorProvider1.SetError(lblTestName,
                    String.Format(
                        PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.TestHarness.Forms.frmNewRecordedTest.Exception.InvalidCharInTestName"),
                        match.Value));
                e.Cancel = true;
            }
        }
    }
}