#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Titan.Pace.TestHarness.Binary
{
    /// <summary>
    /// Binary file deserializer class.
    /// </summary>
    public class BinaryDeserializer
    {
        /// <summary>
        /// Deserializes a file into a TestHarness object. 
        /// </summary>
        /// <param name="pathFileName">Path and file name to the file to be deseralized.</param>
        /// <returns>A TestHarness object, or null if an error occured or the file was of an invalid type.</returns>
        /// <exception cref="System.IO.FileNotFoundException"></exception>
        /// <exception cref="System.Runtime.Serialization.SerializationException"></exception>
        /// <exception cref="System.Exception"></exception>
        public static Palladium.TestHarness.TestHarness Deserialize(string pathFileName)
        {
            FileStream fStream = null;
            try
            {
                Palladium.TestHarness.TestHarness p = new Palladium.TestHarness.TestHarness();
                BinaryFormatter bf = new BinaryFormatter();

                if (!File.Exists(pathFileName))
                    throw new FileNotFoundException();

                fStream = new FileStream(pathFileName, FileMode.OpenOrCreate, FileAccess.Read);

                p = (Palladium.TestHarness.TestHarness)bf.Deserialize(fStream);

                return p;
            }
            catch (System.Runtime.Serialization.SerializationException ex)
            {
                PafApp.GetLogger().Error(ex);
                Exception exp = ex.InnerException;
                while (exp != null)
                {
                    PafApp.GetLogger().Error(exp);
                    exp = exp.InnerException;
                }
                throw;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                Exception exp = ex.InnerException;
                while (exp != null)
                {
                    PafApp.GetLogger().Error(exp);
                    exp = exp.InnerException;
                }
                throw;
            }
            finally
            {
                if (fStream != null)
                    fStream.Close();
            }
        }
    }
}
