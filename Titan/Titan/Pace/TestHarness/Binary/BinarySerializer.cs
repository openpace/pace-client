#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Titan.Pace.TestHarness.Binary
{
    /// <summary>
    /// Binary file serializer class.
    /// </summary>
    public class BinarySerializer
    {
        /// <summary>
        /// Serializes a TestHarness object to a binary file.
        /// </summary>
        /// <param name="t">The TestHarness object to serialize.</param>
        /// <param name="pathFileName">Path and file name where the object will be serialzed.</param>
        public static void Serialize(Palladium.TestHarness.TestHarness t, string pathFileName)
        {
            FileStream fStream = null;
            try
            {
                BinaryFormatter bf = new BinaryFormatter();

                System.IO.File.Delete(pathFileName);

                fStream = new FileStream(pathFileName, FileMode.CreateNew);

                bf.Serialize(fStream, t);

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                Exception exp = ex.InnerException;
                while (exp != null)
                {
                    PafApp.GetLogger().Error(exp);
                    exp = exp.InnerException;
                }
                throw;
            }
            finally
            {
                if (fStream != null)
                    fStream.Close();
            }
        }
    }
}
