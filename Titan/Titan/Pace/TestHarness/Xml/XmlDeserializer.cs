#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.IO;

namespace Titan.Pace.TestHarness.Xml
{
    /// <summary>
    /// Titan Test Harness XML Deserialixer.
    /// </summary>
    public class XmlDeserializer
    {
        /// <summary>
        /// Deserialize a file into an object.
        /// </summary>
        /// <param name="objectType">The type of object to deserialize the file into.</param>
        /// <param name="pathFileName">The path to file to </param>
        /// <returns>An object of type objectType, or null.</returns>
        public static object Deserialize(Type objectType, string pathFileName)
        {
            FileStream fStream = null;
            try
            {
                object p = new object();

                System.Xml.Serialization.XmlSerializer x =
                    new System.Xml.Serialization.XmlSerializer(objectType);

                if (!File.Exists(pathFileName))
                    throw new FileNotFoundException();

                fStream = new FileStream(pathFileName, FileMode.Open);

                p = x.Deserialize(fStream);

                return p;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);

                PafApp.GetLogger().Error(ex);
                Exception exp = ex.InnerException;
                while (exp != null)
                {
                    PafApp.GetLogger().Error(exp);
                    exp = exp.InnerException;
                }
                return null;
            }
            finally
            {
                if (fStream != null)
                    fStream.Close();
            }
        }
    }
}