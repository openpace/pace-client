#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.IO;

namespace Titan.Pace.TestHarness.Xml
{
    /// <summary>
    /// 
    /// </summary>
    public class XmlSerializer
    {
        /// <summary>
        /// Serialize an object to a xml file.
        /// </summary>
        /// <param name="p">The object to serialize.</param>
        /// <param name="objectType">The type of object.</param>
        /// <param name="pathFileName">Path of the file to serialize into (if the file already
        /// exists, it is overwritten).</param>
        public static void Serialize(object p, Type objectType, string pathFileName)
        {
            StreamWriter xWriter = null;
            try
            {
                System.Xml.Serialization.XmlSerializer x =
                    new System.Xml.Serialization.XmlSerializer(objectType);

                System.IO.File.Delete(pathFileName);

                xWriter = new StreamWriter(pathFileName);

                x.Serialize(Console.Out, p);
                x.Serialize(xWriter, p);

                Console.WriteLine();
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);

                PafApp.GetLogger().Error(ex);
                Exception exp = ex.InnerException;
                while (exp != null)
                {
                    PafApp.GetLogger().Error(exp);
                    exp = exp.InnerException;
                }
            }
            finally
            {
                if (xWriter != null)
                    xWriter.Close();
            }
        }
    }
}