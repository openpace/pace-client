#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.TestHarness.Startup 
{
    /// <summary>
    /// Test harness file information object.
    /// </summary>
    [Serializable]
	public class TestHarnessFileInfo
    {
        private bool _UseDefault;
        private string _UserId;
        private string _Url;
        private string _Password;
        private string _Role;
        private string _Process;
        private string _TestHarnessFile;

        /// <summary>
        /// Use the default information.
        /// </summary>
        public bool UseDefault
        {
            get { return _UseDefault; }
            set { _UseDefault = value; }
        }

        /// <summary>
        /// User id for the server.
        /// </summary>
        public string UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }

        /// <summary>
        /// Password for the server.
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        /// <summary>
        /// Role for the server.
        /// </summary>
        public string Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        /// <summary>
        /// Process for the server.
        /// </summary>
        public string Process
        {
            get { return _Process; }
            set { _Process = value; }
        }

        /// <summary>
        /// URL of the server.
        /// </summary>
        public string Url
        {
            get { return _Url; }
            set { _Url = value; }
        }

        /// <summary>
        /// Path to the test harness file.
        /// </summary>
        public string TestHarnessFile
        {
            get { return _TestHarnessFile; }
            set { _TestHarnessFile = value; }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public TestHarnessFileInfo()
        { 
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="userId">User id for the server.</param>
        /// <param name="password">Password for the server.</param>
        /// <param name="role">Role for the server.</param>
        /// <param name="process">Process for the server.</param>
        /// <param name="url">URL for the server.</param>
        /// <param name="useDefualt">Use the default user id, password, etc.</param>
        /// <param name="harnessFile">Path the the Test Harness file.</param>
        public TestHarnessFileInfo(string userId, string password,
            string role, string process, string url, bool useDefualt, string harnessFile)
        {
            _UserId = userId;
            _Password = password;
            _Url = url;
            _Role = role;
            _Process = process;
            _UseDefault = useDefualt;
            _TestHarnessFile = harnessFile;
        }
	}
}