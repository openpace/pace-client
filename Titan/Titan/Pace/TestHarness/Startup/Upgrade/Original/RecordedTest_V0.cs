#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using Titan.Palladium.TestHarness;

namespace Titan.Pace.TestHarness.Startup.Upgrade.Original
{
    /// <summary>
    /// Collection of TestSequences.
    /// </summary>
    [Serializable]
    class RecordedTest_V0
    {
        private string _Name;
        private bool _Reload;
        private string _Role;
        private string _Season;
        private List<TestSequence> _TestSequences = new List<TestSequence>();

        /// <summary>
        /// Name of the RecordedTest.
        /// </summary>
        public string Name
        {
            get { return  _Name; }
            set { _Name = value; }
        }

        /// <summary>
        /// Reload data cache before running the test.
        /// </summary>
        public bool Reload
        {
            get { return _Reload; }
            set { _Reload = value; }
        }

        /// <summary>
        /// User's role.
        /// </summary>
        public string Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        /// <summary>
        /// User's season.
        /// </summary>
        public string season
        {
            get { return _Season; }
            set { _Season = value; }
        }

        /// <summary>
        /// List of the TestSequences.
        /// </summary>
        public List<TestSequence> TestSequences
        {
            get { return _TestSequences; }
            set { _TestSequences = value; }
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public RecordedTest_V0()
        { 
        }


        /// <summary>
        /// Creates a new RecordedTest.
        /// </summary>
        /// <param name="name">Name of the RecordedTest.</param>
        /// <param name="reload">Reload data cache before running the test.</param>
        /// <param name="role">User's role.</param>
        /// <param name="season">User's season.</param>
        /// <param name="item">List of the TestSequences.</param>
        public RecordedTest_V0(string name, bool reload, string role,
            string season, TestSequence item)
        {
            _Name = name;
            _Reload = reload;
            _Role = role;
            _Season = season;
            _TestSequences.Add(item);
        }

        /// <summary>
        /// Creates a new RecordedTest.
        /// </summary>
        /// <param name="name">Name of the RecordedTest.</param>
        /// <param name="reload">Reload data cache before running the test.</param>
        /// <param name="role">User's role.</param>
        /// <param name="season">User's season.</param>
        public RecordedTest_V0(string name, bool reload, string role,
            string season)
        {
            _Name = name;
            _Reload = reload;
            _Role = role;
            _Season = season;
        }

        /// <summary>
        /// Returns an enumerator used to iterate through the RecordedTest.
        /// </summary>
        /// <returns>An enumerator.</returns>
        public IEnumerator GetEnumerator()
        {
            return _TestSequences.GetEnumerator();
        }
    }
}
