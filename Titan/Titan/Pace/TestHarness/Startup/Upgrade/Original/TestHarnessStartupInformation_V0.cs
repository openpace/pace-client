#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;
using Titan.Pace.TestHarness.Xml;

namespace Titan.Pace.TestHarness.Startup.Upgrade.Original
{
    /// <summary>
    /// 
    /// </summary>
    //[Serializable]
    public class TestHarnessStartupInformation
    {
        private string _UserId;
        private string _Password;
        private string _Url;
        private string _Role;
        private string _Process;
        private List<string> _TestHarnessFiles;

        /// <summary>
        /// 
        /// </summary>
        public string UserId
        {
            get { return _UserId; }
            set { _UserId = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string Password
        {
            get { return _Password; }
            set { _Password = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string URL
        {
            get { return _Url; }
            set { _Url = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string Role
        {
            get { return _Role; }
            set { _Role = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public string Process
        {
            get { return _Process; }
            set { _Process = value; }
        }
        
        /// <summary>
        /// 
        /// </summary>
        public List<string> TestHarnessFiles
        {
            get { return _TestHarnessFiles; }
            set { _TestHarnessFiles = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public TestHarnessStartupInformation()
        { }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="password"></param>
        /// <param name="url"></param>
        /// <param name="role"></param>
        /// <param name="process"></param>
        /// <param name="testHarnessFiles"></param>
        public TestHarnessStartupInformation(string userId, string password, string url, string role,
            string process, List<string> testHarnessFiles)
        {
            _UserId = userId;
            _Password = password;
            _Url = url;
            _Role = role;
            _Process = process;
            _TestHarnessFiles = testHarnessFiles;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public static class UpgradeTestHarnessStartupInformation_Original
    {
        /// <summary>
        /// Upgrades only old startup file to the new version.
        /// </summary>
        /// <param name="fileName">File to be updated.</param>
        /// <returns>An updated TestHarnessStartupInformation object.</returns>
        public static Startup.TestHarnessStartupInformation
            Update(string fileName)
        {
            TestHarnessStartupInformation thsiOld = null;
            Startup.TestHarnessStartupInformation thsi =
                new Startup.TestHarnessStartupInformation();

            try
            {
                thsiOld = (TestHarnessStartupInformation) XmlDeserializer.Deserialize(
                    Type.GetType("Titan.Pace.TestHarness.Startup.Upgrade.Original.TestHarnessStartupInformation", true),
                    fileName);

                thsi.Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
                thsi.RunHarnessTests = true;
                thsi.RunUnitTests = false;
                thsi.DefaultUserId = thsiOld.UserId;
                thsi.DefaultPassword = thsiOld.Password;
                thsi.DefaultRole = thsiOld.Role;
                thsi.DefaultProcess = thsiOld.Process;
                thsi.DefaultURL = thsiOld.URL;
                thsi.TestHarnessFiles = new List<TestHarnessFileInfo>();

                foreach (string file in thsiOld.TestHarnessFiles)
                {
                    thsi.TestHarnessFiles.Add(
                        new TestHarnessFileInfo(
                            null,
                            null,
                            null,
                            null,
                            null,
                            true,
                            file));
                }

                XmlSerializer.Serialize(
                    thsi,
                    Type.GetType("Titan.Pace.TestHarness.Startup.TestHarnessStartupInformation", true),
                    fileName);

                return thsi;
            }
            catch (Exception)
            {
                throw;
            }

        }
    }
}

