#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace Titan.Pace.TestHarness.Startup
{
    /// <summary>
    /// 
    /// </summary>
    [Serializable]
    public class TestHarnessStartupInformation
    {
        private string _Version;
        private bool _RunHarnessTests;
        private string _DefaultUserId;
        private string _DefaultPassword;
        private string _DefaultUrl;
        private string _DefaultRole;
        private string _DefaultProcess;
        private bool _RunUnitTests;
        private List<TestHarnessFileInfo> _TestHarnessFiles;
        
        /// <summary>
        /// Version of the recorded test harness startup file.
        /// </summary>
        public string Version
        {
            get { return _Version; }
            set { _Version = value; }
        }

        /// <summary>
        /// Enable/disable the automatic test harness.
        /// </summary>
        public bool RunHarnessTests
        {
            get { return _RunHarnessTests; }
            set { _RunHarnessTests = value; }
        }

        /// <summary>
        /// Enable/disable the automatic unit testing.
        /// </summary>
        public bool RunUnitTests
        {
            get { return _RunUnitTests; }
            set { _RunUnitTests = value; }
        }

        /// <summary>
        /// Default server user id.
        /// </summary>
        public string DefaultUserId
        {
            get { return _DefaultUserId; }
            set { _DefaultUserId = value; }
        }

		/// <summary>
		/// Default server password.
		/// </summary>
        public string DefaultPassword 
        {
            get { return _DefaultPassword; }
            set { _DefaultPassword = value; }
        }

		/// <summary>
		/// Default server role.
		/// </summary>
        public string DefaultRole
        {
            get { return _DefaultRole; }
            set { _DefaultRole = value; }
        }

		/// <summary>
		/// Default server process.
		/// </summary>
        public string DefaultProcess
        {
            get { return _DefaultProcess; }
            set { _DefaultProcess = value; }
        }

        /// <summary>
		/// Default server url.
		/// </summary>
        public string DefaultURL
        {
            get { return _DefaultUrl; }
            set { _DefaultUrl = value; }
        }
		
        /// <summary>
		/// List of test harness files.
		/// </summary>
        public List<TestHarnessFileInfo> TestHarnessFiles
        {
            get { return _TestHarnessFiles; }
            set { _TestHarnessFiles = value; }
        }

        /// <summary>
        /// Defualt constructor.
        /// </summary>
        public TestHarnessStartupInformation()
        {
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="userId">Default user id.</param>
        /// <param name="password">Default password.</param>
        /// <param name="role">Default role.</param>
        /// <param name="process">Default process.</param>
        /// <param name="url">Default url.</param>
        /// <param name="runHarnessTests">Run test harness tests.</param>
        /// <param name="runUnitTests">Run automated unit tests.</param>
        public TestHarnessStartupInformation(string userId, string password, string role,
            string process, string url, bool runHarnessTests, bool runUnitTests)
        {
            _DefaultUserId = userId;
            _DefaultPassword = password;
            _DefaultRole = role;
            _DefaultProcess = process;
            _DefaultUrl = url;
            _RunHarnessTests = runHarnessTests;
            _RunUnitTests = runUnitTests;
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="userId">Default user id.</param>
        /// <param name="password">Default password.</param>
        /// <param name="role">Default role.</param>
        /// <param name="process">Default process.</param>
        /// <param name="url">Default url.</param>
        /// <param name="runHarnessTests">Run test harness tests.</param>
        /// <param name="runUnitTests">Run automated unit tests.</param>
        /// <param name="testHarnessFiles">List of TestHarnessFileInfo.</param>
        public TestHarnessStartupInformation(string userId, string password, string role,
            string process, string url, bool runHarnessTests, bool runUnitTests,
            List<TestHarnessFileInfo> testHarnessFiles)
        {
            _DefaultUserId = userId;
            _DefaultPassword = password;            
            _DefaultRole = role;
            _DefaultProcess = process;
            _DefaultUrl = url;
            _RunHarnessTests = runHarnessTests;
            _RunUnitTests = runUnitTests;
            _TestHarnessFiles = testHarnessFiles;
        }

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="userId">Default user id.</param>
        /// <param name="password">Default password.</param>
        /// <param name="role">Default role.</param>
        /// <param name="process">Default process.</param>
        /// <param name="url">Default url.</param>
        /// <param name="runHarnessTests">Run test harness tests.</param>
        /// <param name="runUnitTests">Run automated unit tests.</param>
        /// <param name="testHarnessFile">TestHarnessFileInfo to add to list.</param>
        public TestHarnessStartupInformation(string userId, string password, string role,
            string process, string url, bool runHarnessTests, bool runUnitTests,
            TestHarnessFileInfo testHarnessFile)
        {
            _DefaultUserId = userId;
            _DefaultPassword = password;
            _DefaultRole = role;
            _DefaultProcess = process;
            _DefaultUrl = url;
            _RunHarnessTests = runHarnessTests;
            _RunUnitTests = runUnitTests;
            _TestHarnessFiles.Add(testHarnessFile);
        }		
	}
}