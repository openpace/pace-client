#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using Titan.Pace.Application.Exceptions;
using Titan.Pace.Application.Utilities;
using Titan.Pace.DataStructures;
using Titan.Properties;

namespace Titan.Pace.TestHarness.Startup
{
    internal class StartupFileAutomationMngr
    {
        private readonly TestHarnessStartupInformation _TestHarnessInfo;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="thsi">TestHarnessStartupInformation information.</param>
        public StartupFileAutomationMngr(TestHarnessStartupInformation thsi)
        {
            _TestHarnessInfo = thsi;
        }

        /// <summary>
        /// Run all the test scrips in the test harness startup file.
        /// </summary>
        public void run()
        {
            PafApp.TestHarnessRunning = true;
            try
            {
                //Check the xml to see if the tests are to be run, if not return.
                if (!_TestHarnessInfo.RunHarnessTests)
                {
                    return;
                }
                else
                {
                    PafApp.GetTestHarnessManager().SuccessfullTests = 0;
                    PafApp.GetTestHarnessManager().ProtectionErrors = 0;
                    PafApp.GetTestHarnessManager().VarianceErrors = 0;
                    PafApp.GetTestHarnessManager().ExceptionErrors = 0;
                    PafApp.GetTestHarnessManager().OtherErrors = false;
                    PafApp.GetLogger().Warn("Begining test harness file run....");
                }

                foreach (TestHarnessFileInfo file in _TestHarnessInfo.TestHarnessFiles)
                {
                    const int maxTries = 3;
                    int tries = 0;
                    bool runLoop = true;
                    do
                    {
                        try
                        {
                            if (file.UseDefault)
                            {
                                SetLogonInformation(_TestHarnessInfo.DefaultUserId, _TestHarnessInfo.DefaultPassword,
                                                    _TestHarnessInfo.DefaultURL, _TestHarnessInfo.DefaultRole,
                                                    _TestHarnessInfo.DefaultProcess);
                                runLoop = false;
                            }
                            else
                            {
                                SetLogonInformation(file.UserId, file.Password, file.Url, file.Role, file.Process);
                                runLoop = false;
                            }
                        }
                        catch (Exception ex)
                        {
                            PafApp.GetLogger()
                                  .WarnFormat("Login exception occured.  Attempting another login in 30 (s)");
                            System.Threading.Thread.Sleep(30000);
                            tries++;
                            if (tries == maxTries)
                            {
                                PafApp.GetLogger()
                                      .Error("Attempted login: " + maxTries +
                                             ", times. Giving up, original exception thrown.");
                                throw new CannotConnectToServerException(ex.Message, ex);
                            }
                        }
                    } while (runLoop);

                    FileInfo testFile = new FileInfo(file.TestHarnessFile);
                    if (testFile.Extension.Equals(PafAppConstants.TTH_FILE_EXTENSION))
                    {
                        PafApp.GetTestHarnessManager().ProcessTthFile(file.TestHarnessFile, false, true);
                    }
                    else
                    {
                        List<string> testNames =
                            PafApp.GetTestHarnessManager().ProcessBtthFile(file.TestHarnessFile, false, true, false);
                    }
                }
                PafApp.GetLogger().WarnFormat(
                    "Test harness run complete. Test results: Successful Tests: {0},  Protection Errors: {1}. Variance Errors: {2}. Exception Errors: {3}.",
                    new String[]
                        {
                            PafApp.GetTestHarnessManager().SuccessfullTests.ToString(),
                            PafApp.GetTestHarnessManager().ProtectionErrors.ToString(),
                            PafApp.GetTestHarnessManager().VarianceErrors.ToString(),
                            PafApp.GetTestHarnessManager().ExceptionErrors.ToString()
                        });
            }
            catch (CannotConnectToServerException)
            {
                throw;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                PafApp.TestHarnessRunning = false;
            }
        }

        /// <summary>
        /// Cleans up any old views, etc.
        /// </summary>
        private static void CleanUp()
        {
            ////KRM added to try and fix memory leak.
            //Remove the cached user selections.
            PafApp.GetGridApp().ActionsPane.ClearCachedPafUserSelections();
            PafApp.GetGridApp().ActionsPane.ClearCachedUserSelections();
            PafApp.GetGridApp().ActionsPane.ClearActionsPane();
            //remove all the view sheets.
            PafApp.GetGridApp().RemoveViewSheets();
            //clear out any cell notes...
            PafApp.GetCellNoteMngr().ClearBuckets();
            PafApp.GetCellNoteMngr().ClearCache();
            //TTN-880
            PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = false;
        }

        /// <summary>
        /// Sets the user logon, role, process information.
        /// </summary>
        /// <param name="userid">User id required to logon to the server.</param>
        /// <param name="password">Password required to logon to the server.</param>
        /// <param name="url">URL of the server.</param>
        /// <param name="role">Role to run the test script under.</param>
        /// <param name="process">Process to run the test script under.</param>
        private void SetLogonInformation(string userid, string password, string url,string role, string process)
        {
            Console.WriteLine("Logging on server using information: url: {0}, userid: {1}, Role: {2}, Process: {3}", new [] {url, userid, role, process});
            CleanUp();
            PafApp.GetGridApp().LogonInformation.UserName = userid;
            PafApp.GetGridApp().LogonInformation.UserPassword = password;
            PafApp.GetGridApp().LogonInformation.URL = url;
            //Set the web service URL
            Settings.Default.Titan_PafService_PafService = url;
            //Logoff the Server if already logged on
            PafApp.EndPlanningSession();
            //create a server logon info object.
            PafLogonInfo user = new PafLogonInfo(userid, password, null, null, null);
            //encrypt the user information.
            user = Security.EncryptUser(user);
            //Login to server.
            PafApp.SendClientAuth(user.UserName, null, user.UserDomain, user.IV, user.UserPassword, user.UserSID);
            //set the role process.
            PafApp.GetGridApp().RoleSelector.UserRole = role;
            PafApp.GetGridApp().RoleSelector.UserSelectedPlanTypeSpecString = process;
            //Create your planning session, send role and process to server.
            
            //=====commented out===
            //PafApp.CreatePlanningSession(role, process, false);
            //PafApp.GetGridApp().RoleSelector.BuildCustomMenuDefs();
            //Rebuild the actions pane.
            //PafApp.GetGridApp().BuildActionsPane();
        }
    }
}
