#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Win32;
using Titan.Palladium.GridView;
using Titan.Palladium.TestHarness;
using Excel = Microsoft.Office.Interop.Excel;

namespace Titan.Pace.TestHarness
{
    internal class TestHarnessEvaluation
    {
        /// <summary>
        /// Cell by cell comparison of the two result arrays.
        /// </summary>
        /// <param name="original">The original array results from the test creation.</param>
        /// <param name="testRun">The results from the test run.</param>
        /// <param name="variance">The result of original - results.</param>
        /// <param name="precision">The number of significant digits to compare.</param>
        /// <returns>true, if the arrays are equal, false if not.</returns>
        public bool ArraysEqual(object[,] original, object[,] testRun, out object[,] variance, int precision)
        {
            bool res = true;

            variance = new object[original.GetLength(0), original.GetLength(1)];

            try
            {
                //Check array sizes - 1st dimension.
                if (original.GetLength(0) != testRun.GetLength(0))
                {
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Array sizes don't match.");
                    variance[original.GetLowerBound(0), original.GetLowerBound(0)] =
                        PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.TestHarness.Exception.ArraySizesDontMatch");
                    return false;
                }
                //Check array sizes - 2nd dimension.
                if (original.GetLength(1) != testRun.GetLength(1))
                {
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Array sizes don't match.");
                    variance[original.GetLowerBound(1), original.GetLowerBound(1)] =
                        PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.TestHarness.Exception.ArraySizesDontMatch");
                    return false;
                }

                //First dimension.
                for (int r = original.GetLowerBound(0); r <= original.GetUpperBound(0); r++)
                {
                    //Second dimension.
                    for (int c = original.GetLowerBound(1); c <= original.GetUpperBound(1); c++)
                    {
                        //Object to hold results.
                        variance[r - 1, c - 1] = new object();

                        //Check to make sure both values are non null.
                        if (original[r, c] != null && testRun[r, c] != null)
                        {
                            //Check to see if both values are numeric.
                            if (original[r, c].IsDouble() && testRun[r, c].IsDouble())
                            {
                                double s = Convert.ToSingle(original[r, c]);
                                double s1 = Convert.ToSingle(testRun[r, c]);

                                //double d = Convert.ToDouble(original[r, c]);
                                //double d1 = Convert.ToDouble(testRun[r, c]);

                                //double d_round = Math.Round(d, precision);
                                //double d1_round = Math.Round(d1, precision);

                                //double diff = d_round - d1_round;

                                double s_round = Math.Round(s, precision);
                                double s1_round = Math.Round(s1, precision);

                                double diff = s_round - s1_round;


                                variance[r - 1, c - 1] = Math.Round(s, precision) - 
                                    Math.Round(s1, precision);


                                //Check to see if the difference is 0.
                                //if (Convert.ToSingle(variance[r - 1, c - 1]) != 0)
                                if (Convert.ToDouble(variance[r - 1, c - 1]) != 0)
                                {
                                    //Difference is not zero, so log it and set the flag to false.
                                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug(String.Format("Error at R{0}C{1}.  Orig value:{2}, Test value:{3}.",
                                        new String[] { r.ToString(), c.ToString(), original[r, c].ToString(), testRun[r, c].ToString() }));

                                    res = false;
                                }
                                else
                                {
                                    variance[r - 1, c - 1] = "N/A";
                                }
                            }
                            else
                            {
                                //Both values are not numeric.
                                //Check for matching string values.
                                if (original[r, c].ToString().Equals(testRun[r, c].ToString()))
                                {
                                    //String match.
                                    variance[r - 1, c - 1] = original[r, c];
                                }
                                else
                                {
                                    //String don't match.
                                    variance[r - 1, c - 1] = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Exception.StringValuesDontMatch");
                                }
                            }
                        }
                        else if (original[r, c] == null && testRun[r, c] != null)
                        {
                            //The original value is null, and the test run is not null.

                            //Lot the error.
                            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug(String.Format("Error at R{0}C{1}.  Orig value:{2}, Test value:{3}.",
                               new String[] { r.ToString(), c.ToString(), "null", testRun[r, c].ToString() }));

                            //Set the flag to false.
                            res = false;

                            //Log a string error to the grid.
                            variance[r - 1, c - 1] = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Exception.OriginalValueIsNull");
                        }
                        else if (original[r, c] != null && (original[r, c].GetDouble()) != 0.0 && testRun[r, c] == null)
                        {
                            //The original value is not null, and the test run is null.
                            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug(String.Format("Error at R{0}C{1}.  Orig value:{2}, Test value:{3}.",
                                new String[] { r.ToString(), c.ToString(), original[r, c].ToString(), "null" }));

                            //Set the flag to false
                            res = false;

                            //Log a string error to the grid.
                            variance[r - 1, c - 1] = PafApp.GetLocalization().GetResourceManager().GetString("Application.TestHarness.Exception.TestValueIsNull");
                        }
                        else
                        {
                            //Both original and testRun are null.
                            variance[r - 1, c - 1] = "";
                        }
                    }
                }
                return res;
            }
            catch (IndexOutOfRangeException ex)
            {
                PafApp.GetLogger().Error(ex);
                return false;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return false;
            }
        }

        /// <summary>
        /// Outputs an object array to an Excel sheet.
        /// </summary>
        /// <param name="wbk">The Excel workbook to output to.</param>
        /// <param name="sheetName">The name of the worksheet to output the array to.</param>
        /// <param name="array">The array to output.</param>
        private void OutputArrayToSheet(Excel.Workbook wbk, string sheetName, object[,] array)
        {
            object missing = System.Reflection.Missing.Value;
            Excel.Worksheet sheet;
            try
            {
                if (!PafApp.GetGridApp().DoesSheetExist(wbk, sheetName))
                {
                    sheet = PafApp.GetGridApp().AddNewWorksheet(wbk, sheetName, true, true);
                }
                else
                {
                    sheet = ((Excel.Worksheet)wbk.Worksheets[sheetName]);
                }

                sheet.Cells.Clear();
                Excel.Range Rng = null;
                Rng = PafApp.GetTestHarnessManager().GetRange(new ContiguousRange(
                    new CellAddress(1, 1),
                    new CellAddress(array.GetLength(0), array.GetLength(1))),
                    sheet);
                Rng.Value2 = array;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        /// <summary>
        /// Outputs the three arrays to three different Excel worksheets.
        /// </summary>
        /// <param name="wbk">The Excel workbook to output to.</param>
        /// <param name="rt">The recorded test which has the variance.</param>
        /// <param name="ts">The test sequence in the recorded test that has the variance.</param>
        /// <param name="testRun">The test run array.</param>
        /// <param name="variance">The variance array.</param>
        public void OutputVariance(Excel.Workbook wbk, RecordedTest rt, TestSequence ts, object[,] testRun, object[,] variance)
        {
            object missing = System.Reflection.Missing.Value;
            try
            {

                string sheetName = rt.Name + "_" + ts.SequenceNumber + "_OrigRun";
                OutputArrayToSheet(wbk, sheetName, ts.TestSequenceResults.ResultSet);


                sheetName = rt.Name + "_" + ts.SequenceNumber + "_TestRun";
                OutputArrayToSheet(wbk, sheetName, testRun);


                sheetName = rt.Name + "_" + ts.SequenceNumber + "_Variance";
                OutputArrayToSheet(wbk, sheetName, variance);

            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                PafApp.GetLogger().Error(ex);
            }
            finally
            {
                Excel.Application ExcelObj = wbk.Application;
                //Don't display any alerts.
                ExcelObj.DisplayAlerts = false;
                //Close the workbook.
                wbk.Close(true, missing, missing);
                //close the excel app
                ExcelObj.Quit();
                //make sure the excel obj is closed.
                IntPtr objPtr = new IntPtr(ExcelObj.Hwnd);
                Win32Utils.KillProcess(objPtr);
            }
        }
    }
}