#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Application.Forms;
using Titan.Pace.Base.Data;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView;
using Titan.Pace.Rules;
using Titan.Pace.TestHarness.Binary;
using Titan.Pace.TestHarness.Forms;
using Titan.Pace.TestHarness.Startup;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Palladium.TestHarness;
using Range=Microsoft.Office.Interop.Excel.Range;
// ReSharper disable All

namespace Titan.Pace.TestHarness
{
    internal class TestHarnessMgr
    {
        private bool _IsRecording;
        private int _RecordedTestSeqNumber;
        private int _TestSeqNumber;
        private Palladium.TestHarness.TestHarness _Th;
        private TestSequenceBeginningState _BeginningState;
        private string _BinaryFileName;
        private static TestHarnessProtMngr _ThProtMngr;
        private static TestHarnessEvaluation _ThEvaluation;
        private enum TestSequenceErrorType { Protection, Variance, Exception, None };
        private int _ProtectionErrors = 0;
        private int _VarianceErrors = 0;
        private int _ExceptionErrors = 0;
        private int _SuccessfullTests = 0;
        private bool _OtherErrors;

        /// <summary>
        /// Default constructor
        /// </summary>
        public TestHarnessMgr()
        {
            _IsRecording = false;
            _RecordedTestSeqNumber = -1;
            _TestSeqNumber = 0;
            _Th = new Palladium.TestHarness.TestHarness();
            _BinaryFileName = "";
            _BeginningState = null;
            _OtherErrors = false;

            _ThEvaluation = new TestHarnessEvaluation();
            PafApp.GetEventManager().UserLockedCell += 
                new ProtectionMngr.ProtectionMngr_UserLockedCell(TestHarnessMgr_UserLockedCell);

            PafApp.GetEventManager().SessionLockedCell +=
                new ProtectionMngr.ProtectionMngr_SessionLockedCell(TestHarnessMgr_SessionLockedCell);

            PafApp.GetEventManager().AddChangedCell += 
                new ProtectionMngr.ProtectionMngr_AddChangedCell(TestHarnessMgr_AddChangedCell);

            PafApp.GetEventManager().AddChangedCells += 
                new ProtectionMngr.ProtectionMngr_AddChangedCells(TestHarnessMgr_AddChangedCells);
        }

        

        /// <summary>
        /// Get/set the recording status of the test harness.
        /// </summary>
        public bool IsRecording
        {
            get { return _IsRecording; }
            set { _IsRecording = value; }
        }

        /// <summary>
        /// Get/set the recording status of the test harness.
        /// </summary>
        public bool IsRunning { set; get; }

        /// <summary>
        /// Get/set the binary file name to serialize the object data.
        /// </summary>
        public string BinaryFileName
        {
            get { return _BinaryFileName; }
            set { _BinaryFileName = value; }
        }

        /// <summary>
        /// The number of exception errors in the current test harness run. 
        /// </summary>
        public int ExceptionErrors
        {
            get { return _ExceptionErrors; }
            set { _ExceptionErrors = value; }
        }

        /// <summary>
        /// The number of variance errors in the current test harness run. 
        /// </summary>
        public int VarianceErrors
        {
            get { return _VarianceErrors; }
            set { _VarianceErrors = value; }
        }

        /// <summary>
        /// The number of protection processing errors in the current test harness run. 
        /// </summary>
        public int ProtectionErrors
        {
            get { return _ProtectionErrors; }
            set { _ProtectionErrors = value; }
        }

        /// <summary>
        /// The number of successfull tests in the current test harness run.
        /// </summary>
        public int SuccessfullTests
        {
            get { return _SuccessfullTests; }
            set { _SuccessfullTests = value; }
        }

        /// <summary>
        /// Have other errors occured during the test harness run.
        /// </summary>
        public bool OtherErrors
        {
            get { return _OtherErrors; }
            set { _OtherErrors = value; }
        }

        /// <summary>
        /// Run all the TestSequences in a RecordedTest.
        /// </summary>
        /// <param name="recordedTestSeq">The RecordedTest to use.</param>
        /// <param name="testSequenceNum">The TestSequence to run.</param>
        /// <param name="showMessages">Display message boxes to the user.</param>
        /// <param name="errorMessage">Error message (if occured)</param>
        private TestSequenceErrorType RunTestSequence(int recordedTestSeq, int testSequenceNum, bool showMessages, out string errorMessage)
        {
            TestSequenceErrorType errorReturnType = TestSequenceErrorType.None;
            errorMessage = String.Empty;
            try
            {
                //Get the recorded test.
                RecordedTest rt = _Th[recordedTestSeq];

                //Get the test sequence.
                TestSequence ts = rt.TestSequences[testSequenceNum];

                bool[] setting = new bool[] {ts.RowsSuppressed, ts.ColumnsSuppressed};

                PafApp.GetGridApp().ActionsPane.StoreSuppressZeroSettings(ts.ViewName, setting);
                PafApp.GetGridApp().ActionsPane.SetSuppressZeroSettings(ts.ViewName);

                //Set the rule set name, if it's not null or empty string.
                if (!String.IsNullOrEmpty(ts.RuleSetName))
                {
                    PafApp.RuleSetName = ts.RuleSetName;
                    PafApp.GetLogger().InfoFormat("Changed RuleSet to: {0}", new string[] { PafApp.RuleSetName });
                }

                ChangedCellInfo sessionLockedCells = ts.ChangedCellInfoList.Find(x => x.ChangeType == Change.SessionLocked);
                if (sessionLockedCells != null)
                {
                    PafApp.GetViewMngr().GlobalLockMngr.Clear();
                }

                //Reload the data cache.
                ReloadDataCache(rt, ts.ViewName, testSequenceNum);

                //If the sheet/view has already been built, then select it.
                if (PafApp.GetGridApp().DoesSheetExist(ts.ViewName))
                {
                    PafApp.GetGridApp().ActivateSheet(ts.ViewName);
                }

                //If the view is dynamic, then set the user selections.
                //Added code to handle pre sorted views.  This flag tell the code below that the user selections have changed
                //so the view has to be reretrieved.
                bool userSelChanged = true;
                if (ts.IsDynamic)
                {
                    pafUserSelection[] pafUserSel = PafApp.GetGridApp().ActionsPane.GetCachedDyanmicPafUserSelections(ts.ViewName);
                    if (pafUserSel != null && !pafUserSel.AreEqual(ts.PafUserSelections))
                    {
                        userSelChanged = false;
                    }

                    PafApp.GetGridApp().ActionsPane.CacheDyanmicPafUserSelections(ts.ViewName, ts.PafUserSelections);
                }

                //Added some code here to handle pre sorted views.  If same view is present and presorted, then
                //don't reselect.
                bool getView = false;
                if (PafApp.GetViewMngr().CurrentView != null && PafApp.GetGridApp().DoesSheetExist(ts.ViewName) &&
                    PafApp.GetViewMngr().CurrentView.ViewName.Equals(ts.ViewName) && userSelChanged)
                {
                    if(!PafApp.GetViewMngr().CurrentView.GetOlapView().ServerSortSpecified)
                    {
                        getView = true;
                    }
                }
                else
                {
                    getView = true;
                }

                if(getView)
                {
                    //Select the view in the test sequence.
                    PafApp.GetEventManager().SelectView(
                        ts.ViewName,
                        (Worksheet)Globals.ThisWorkbook.ActiveSheet,
                        true,
                        null);
                }

                ////Select the view in the test sequence.
                //PafApp.GetEventManager().SelectView(
                //    ts.ViewName,
                //    (Worksheet)Globals.ThisWorkbook.ActiveSheet,
                //    true,
                //    null);

                PafApp.GetGridApp().ScreenUpdating = false;

                //Get the current sheet.
                Worksheet sheet = (Worksheet)Globals.ThisWorkbook.ActiveSheet;

                //Get the hash code of the current sheet.
                string sheetHashCode = sheet.GetHashCode().ToString();

                //Make the changes to the cells.
                Stopwatch sw = Stopwatch.StartNew();
                int count = 0;
                foreach (ChangedCellInfo cell in ts.ChangedCellInfoList)
                {
                    cell.Sheet = sheetHashCode;
                    if (cell.CellAddress != null)
                    {
                        MakeCellChange(cell.ChangeType, sheetHashCode, cell.SheetName, cell.CellAddress, cell.CellValue);
                    }
                    else
                    {
                        if (cell.ChangeType == Change.SessionLocked)
                        {
                            MakeCellChange(cell.ChangeType, sheetHashCode, cell.SheetName, cell.SessionLocks);
                        }
                        else
                        {
                            MakeCellChange(cell.ChangeType, sheetHashCode, cell.SheetName, cell.CellAdressRange, cell.CellValue);
                        }
                    }

                    PafApp.GetGridApp().ScreenUpdating = false;



                    //Check the protection manager
                    bool retVal = _ThProtMngr.ValidateProtectionManagerLists(
                        rt.Name,
                        ts,
                        count,
                        PafApp.GetViewMngr().CurrentProtectionMngr.Changes,
                        PafApp.GetViewMngr().CurrentProtectionMngr.ProtectedCellsSet.ToIntersections().ToHashSet(),
                        PafApp.GetViewMngr().CurrentProtectionMngr.UserLocks,
                        PafApp.GetViewMngr().CurrentProtectionMngr.UnLockedChanges,
                        PafApp.GetViewMngr().CurrentProtectionMngr.Replication.AllocateAllCells.ToHashSet(),
                        PafApp.GetViewMngr().CurrentProtectionMngr.Replication.AllocateExistingCells.ToHashSet(),
                        PafApp.GetViewMngr().CurrentProtectionMngr.Lift.AllocateAllCells.ToHashSet(),
                        PafApp.GetViewMngr().CurrentProtectionMngr.Lift.AllocateExistingCells.ToHashSet());

                    if (!retVal)
                    {
                        //Create the workbook.
                       Workbook wbk = PafApp.GetGridApp().CreateNewWorkBook(
                            Path.GetDirectoryName(_BinaryFileName),
                            Path.GetFileNameWithoutExtension(_BinaryFileName));
                        //log the error
                        errorMessage = String.Format("Protection manager error occured in tth file: {0}, recored test: {1}, sequence # {2}.  Excel workbook: {3} has full details.",new [] {_BinaryFileName, rt.Name, ts.SequenceNumber, wbk.Name});
                        PafApp.GetLogger().ErrorFormat(errorMessage);

                        //Write the log to an Excel workbook.
                        _ThProtMngr.OutputLog(wbk, rt, ts);
                        //set the error return type.
                        errorReturnType = TestSequenceErrorType.Protection;

                    }
                    //continue.
                    count++;
                }
                sw.Stop();
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("RunTestSequence cell changes execution time: " + sw.ElapsedMilliseconds.ToString("N0") + " (ms)");

                //Perform the calculation.
                PafApp.GetEventManager().EvaluateView(sheet);
                if(IsRecording)
                {
                    CommandBarButton button = (CommandBarButton)PafApp.GetGridApp().ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Excel.Menu.Calculate")];
                    bool cancel = false;
                    TestHarnessToolbarEventHandler(button, ref cancel);
                }

                //Get the results from the calculation.
                object[,] testResults = PafApp.GetViewMngr().CurrentGrid.GetValueObjectArray(
                                               new ContiguousRange(new CellAddress(1, 1),
                                               PafApp.GetViewMngr().CurrentOlapView.DataRange.BottomRight));

                //Create an array to hold the variance results.
                object[,] variance;
                //Compare the two arrays
                if (!_ThEvaluation.ArraysEqual(ts.TestSequenceResults.ResultSet, testResults, out variance, Titan.Properties.Settings.Default.TestHarnessPrecision))
                {
                    //Create a new workbook.
                    Workbook wbk = PafApp.GetGridApp().CreateNewWorkBook(
                        Path.GetDirectoryName(_BinaryFileName),
                        Path.GetFileNameWithoutExtension(_BinaryFileName));
                    //log the error
                   errorMessage = String.Format("Test Variance error occured in tth file: {0}, recorded test: {1}, sequence # {2}.  Excel workbook: {3} has full details.",
                        new [] {_BinaryFileName, rt.Name, ts.SequenceNumber, wbk.Name});
                    PafApp.GetLogger().ErrorFormat(errorMessage);
                    //Output to workbook.
                    _ThEvaluation.OutputVariance(wbk, rt, ts, testResults, variance);
                    //set the error type
                    errorReturnType = TestSequenceErrorType.Variance;
                }
                
                return errorReturnType;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                if (showMessages)
                {
                    PafApp.MessageBox().Show(ex);
                }
                else
                {
                    //PafApp.GetLogger().Error(ex);
                    PafApp.GetLogger().ErrorFormat("Exception occured in file: {0}, message: {1}", new[] { _BinaryFileName, ex.Message });
                }
                return TestSequenceErrorType.Exception;
            }
            finally
            {
                PafApp.GetGridApp().ScreenUpdating = true;
            }
        }

        /// <summary>
        /// Enters a value into a sheet.
        /// </summary>
        /// <param name="sheetHashCode">Hash code of the sheet.</param>
        /// <param name="changeType">The type of change to be made.</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="sessionLocks"></param>
        private void MakeCellChange(Change changeType, string sheetHashCode, string sheetName, IEnumerable<Intersection> sessionLocks)
        {
            if (changeType == Change.SessionLocked)
            {
                PafApp.GetLogger().InfoFormat("Session Lock: {0}.", new[] { sheetName });
                ViewMngr viewMngr = PafApp.GetViewMngr();

                List<DescendantIntersection> descendantIntersections = new List<DescendantIntersection>();
                //foreach (CoordinateSet cs in sessionLocks)
                //{
                //    descendantIntersections.Add(new DescendantIntersection(cs));
                //}


                foreach (Intersection t in sessionLocks)
                {
                    CoordinateSet cs = new CoordinateSet(t.AxisSequence);
                    cs.AddCoordinate(t.Coordinates);
                    descendantIntersections.Add(new DescendantIntersection(cs));
                }
                List<DescendantIntersection> allIntersections = PafApp.GetDescendantsSync(descendantIntersections);
                foreach (DescendantIntersection di in allIntersections)
                {
                    viewMngr.GlobalLockMngr.AddUserSessionLocksCoordinateSet(di.Descendants);
                    if (di.Parent != null && di.Parent.Dimensions != null)
                    {
                        viewMngr.GlobalLockMngr.AddUserSessionParentLocksCoordinateSet(di.Parent, di.HasAttributes);
                    }
                }

                viewMngr.CurrentProtectionMngr.AddSessionLocks(viewMngr.GlobalLockMngr, sheetHashCode, sheetName);

            }
        }

        /// <summary>
        /// Enters a value into a sheet.
        /// </summary>
        /// <param name="changeType">The type of change to be made.</param>
        /// <param name="sheetHashCode">Hash code of the sheet.</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="contigRange">Range of cells.</param>
        /// <param name="cellValues">The values to enter.</param>
        private void MakeCellChange(Change changeType, string sheetHashCode, string sheetName,
            ContiguousRange contigRange, Object cellValues)
        {
            
            string[] str = ConvertTwoDimObjectArrayToStringArray(cellValues);
            try
            {
                if (changeType == Change.UserChanged)
                {
                    PafApp.GetLogger().InfoFormat("Setting range: {0} - {1} on sheet: {2} - to values: {3}",
                        new string[] 
                        { 
                            contigRange.TopLeft.ToString(), 
                            contigRange.BottomRight.ToString(),
                            sheetName,
                            String.Join(", ", str)
                        });

                    PafApp.GetViewMngr().CurrentGrid.SetValueObject(
                        contigRange,
                        cellValues);
                }
                else if (changeType == Change.Locked)
                {
                    PafApp.GetLogger().InfoFormat("User locking cells: {0} - {1} on sheet: {2}.",
                        new string[] 
                        { 
                            contigRange.TopLeft.ToString(), 
                            contigRange.BottomRight.ToString(),
                            sheetName
                        });

                    PafApp.GetViewMngr().CurrentProtectionMngr.AddUserLock(
                        contigRange, sheetHashCode, sheetName);

                    PafApp.GetCommandBarMngr().EnableLockAndPasteShapeButtons();
                }
                else if (changeType == Change.PasteShape)
                {
                    PafApp.GetLogger().InfoFormat("Setting range (Paste Shape): {0} - {1} on sheet: {2} - to values: {3}",
                        new string[] 
                        { 
                            contigRange.TopLeft.ToString(), 
                            contigRange.BottomRight.ToString(),
                            sheetName,
                            String.Join(", ", str)
                        });

                    PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape = true;

                    PafApp.GetViewMngr().CurrentGrid.SetValueObject(
                       contigRange,
                       cellValues); 
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally 
            {
                PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape = false;
            }
        }

        /// <summary>
        /// Enters a value into a sheet.
        /// </summary>
        /// <param name="changeType">The type of change to be made.</param>
        /// <param name="sheetHashCode">Hash code of the sheet.</param>
        /// <param name="sheetName">The physical name of the sheet.</param>
        /// <param name="cellAddress">Address of the cell.</param>
        /// <param name="cellValue">The value to enter.</param>
        private void MakeCellChange(Change changeType, string sheetHashCode, string sheetName, CellAddress cellAddress, Object cellValue)
        {
            try
            {
                if (changeType == Change.UserChanged)
                {
                    PafApp.GetLogger().InfoFormat("Set Cell: {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                        new string[] { sheetName, Convert.ToString(cellAddress.Row), Convert.ToString(cellAddress.Col), cellValue.ToString() });


                    PafApp.GetViewMngr().CurrentGrid.SetValueDouble(
                        cellAddress.Row,
                        cellAddress.Col,
                        Convert.ToDouble(cellValue));
                }
                else if (changeType == Change.Locked)
                {
                    PafApp.GetLogger().InfoFormat("User Locked Cell: {0}!R[{1}]C[{2}].",
                        new string[] { sheetName, Convert.ToString(cellAddress.Row), Convert.ToString(cellAddress.Col) });


                    PafApp.GetViewMngr().CurrentProtectionMngr.AddUserLock(
                        new ContiguousRange(cellAddress), sheetHashCode, sheetName);

                    PafApp.GetCommandBarMngr().EnableLockAndPasteShapeButtons();
                }
                else if (changeType == Change.PasteShape)
                {
                    PafApp.GetLogger().InfoFormat("Set Cell (Paste Shape): {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                        new string[] { sheetName, Convert.ToString(cellAddress.Row), Convert.ToString(cellAddress.Col), cellValue.ToString() });


                    PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape = true;

                    PafApp.GetViewMngr().CurrentGrid.SetValueDouble(
                       cellAddress.Row,
                       cellAddress.Col,
                       Convert.ToDouble(cellValue));

                    PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape = false;
                }
                else if (changeType == Change.ReplicateAll)
                {
                    PafApp.GetLogger().InfoFormat("Set Cell (Replicate all: {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                        new string[] { sheetName, Convert.ToString(cellAddress.Row), Convert.ToString(cellAddress.Col), cellValue.ToString() });


                    PafApp.GetViewMngr().CurrentGrid.SetValueDouble(
                      cellAddress.Row,
                      cellAddress.Col,
                      Convert.ToDouble(cellValue));

                    PafApp.GetViewMngr().CurrentProtectionMngr.Replication.ReplicateCell(
                        cellAddress,
                        true,
                        ReplicationType.ReplicateAll);

                    PafApp.GetViewMngr().CurrentProtectionMngr.PruneDependencies();

                }
                else if (changeType == Change.ReplicateExisting)
                {
                    PafApp.GetLogger().InfoFormat("Set Cell (Replication Existing): {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                       new string[] { sheetName, Convert.ToString(cellAddress.Row), Convert.ToString(cellAddress.Col), cellValue.ToString() });

                    PafApp.GetViewMngr().CurrentGrid.SetValueDouble(
                      cellAddress.Row,
                      cellAddress.Col,
                      Convert.ToDouble(cellValue));

                    PafApp.GetViewMngr().CurrentProtectionMngr.Replication.ReplicateCell(
                        cellAddress,
                        true,
                        ReplicationType.ReplicateExisting);

                    PafApp.GetViewMngr().CurrentProtectionMngr.PruneDependencies();

                }
                else if (changeType == Change.LiftAll)
                {
                    PafApp.GetLogger().InfoFormat("Set Cell (Lift all: {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                        new string[] {sheetName, Convert.ToString(cellAddress.Row), Convert.ToString(cellAddress.Col), cellValue.ToString()});

                    PafApp.GetViewMngr().CurrentGrid.SetValueDouble(
                      cellAddress.Row,
                      cellAddress.Col,
                      Convert.ToDouble(cellValue));

                    PafApp.GetViewMngr().CurrentProtectionMngr.Lift.LiftCell(
                        cellAddress,
                        true,
                        LiftType.LiftAll);

                    PafApp.GetViewMngr().CurrentProtectionMngr.PruneDependencies();

                }
                else if (changeType == Change.LiftExisting)
                {
                    PafApp.GetLogger().InfoFormat("Set Cell (Lift Existing): {0}!R[{1}]C[{2}] - To value: " + "{3}.",
                        new string[] { sheetName, Convert.ToString(cellAddress.Row), Convert.ToString(cellAddress.Col), cellValue.ToString() });

                    PafApp.GetViewMngr().CurrentGrid.SetValueDouble(
                      cellAddress.Row,
                      cellAddress.Col,
                      Convert.ToDouble(cellValue));

                    PafApp.GetViewMngr().CurrentProtectionMngr.Lift.LiftCell(
                        cellAddress,
                        true,
                        LiftType.LiftExisting);

                    PafApp.GetViewMngr().CurrentProtectionMngr.PruneDependencies();
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Converts a two dimension object array to a single dimension string array.
        /// </summary>
        /// <param name="array">Object array to convert.</param>
        /// <returns>A one dimensional string array.</returns>
        public string[] ConvertTwoDimObjectArrayToStringArray(Object array)
        {
            object[,] obj = (object[,])array;
            string[] str = new string[obj.Length];
            try
            {
                //Copy the multidimension object array to a single dimension 
                //string array so it can be outputed to the log.
                int i = 0;
                for (int r = obj.GetLowerBound(0); r <= obj.GetUpperBound(0); r++)
                {
                    for (int c = obj.GetLowerBound(1); c <= obj.GetUpperBound(1); c++)
                    {
                        str[i] = obj[r, c].ToString();
                        i++;
                    }
                }
                return str;
            }
            catch (Exception)
            {
                return new string[] { "" };
            }
        }

        /// <summary>
        /// Converts a two dimension object array to a single dimension string array.
        /// </summary>
        /// <param name="array">Object array to convert.</param>
        /// <returns>A one dimensional string array.</returns>
        public object[,] ConvertOneBasedArrayToZeroBased(object[,] array)
        {
            object[,] obj = new object[array.GetUpperBound(0), array.GetUpperBound(0)];
            try
            {
               //First dimension.
                for (int r = array.GetLowerBound(0); r <= array.GetUpperBound(0); r++)
                {
                    //Second dimension.
                    for (int c = array.GetLowerBound(1); c <= array.GetUpperBound(1); c++)
                    {
                        //Object to hold results.
                        obj[r - 1, c - 1] = new object();
                        obj[r - 1, c - 1] = array[r, c];
                    }
                }
                return obj;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Reloads the data cache, if required by the test.
        /// </summary>
        /// <param name="rt">The recorded test.</param>
        /// <param name="viewName">The name of the current view.</param>
        /// <param name="sequenceNumber">The sequence number of the current recorded test.</param>
        private void ReloadDataCache(RecordedTest rt, string viewName, int sequenceNumber)
        {
            try
            {
                //Reload the data cache.
                if (rt.Reload && PafApp.GetSaveWorkMngr().DataWaitingToBeSaved ||
                    !rt.Reload && PafApp.GetSaveWorkMngr().DataWaitingToBeSaved && sequenceNumber == 0)
                {

                    pafDataSlice dataSlice;
                    dataSlice = PafApp.GetSaveWorkMngr().ReloadDataCache(viewName);

                    if (dataSlice != null)
                    {
                        //Reset the dirty flag.
                        PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = false;

                        //Set all the open views to dirty.
                        PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(0);

                        //Make all the views plannable.
                        PafApp.GetViewMngr().MakeAllProtMngPlanable();
                    }

                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Event handler for when user hits the Shift + F9 key combination, 
        /// which is the same as clicking on the calculate button.
        /// </summary>
        /// <param name="sheet">Worksheet where the calculation is occuring.</param>
        public void TestHarnessHotKeyListener_onShiftF9(Worksheet sheet)
        {
            bool cancel = false;

            try
            {

                CommandBarButton but = null;
                foreach (CommandBarControl ctl in PafApp.GetGridApp().ViewToolBar.Controls)
                {
                    if (ctl.Parameter.Equals("t_Calculate"))
                    {
                        but = (CommandBarButton)ctl;
                        break;
                    }
                }
                //Make sure we got it.
                if (but != null)
                {
                    //Now send the button to the handler - just like the user clicked it.
                    TestHarnessToolbarEventHandler(but, ref cancel);
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                PafApp.GetLogger().Error(ex);
            }
        }

        /// <summary>
        /// Eventhandler for the toolbar, menubar, and rightclick menu.
        /// </summary>
        /// <param name="Ctrl">The CommandBarButton that was clicked.</param>
        /// <param name="CancelDefault">Boolean to cancel event.</param>
        public void TestHarnessToolbarEventHandler(CommandBarButton Ctrl, ref bool CancelDefault)
        {
            TestHarnessToolbarEventHandler(Ctrl.Parameter);
        }

        /// <summary>
        /// Eventhandler for the toolbar, menubar, and rightclick menu.
        /// </summary>
        /// <param name="command">The CommandBarButton that was clicked.</param>
        public void TestHarnessToolbarEventHandler(string command)
        {
            try
            {
                if (Globals.ThisWorkbook.ActiveChart == null)
                {
                    //Get the active worksheet.
                    Worksheet sheet =
                            (Worksheet)
                            Globals.ThisWorkbook.Application.ActiveWorkbook.ActiveSheet;

                    string viewName = "";
                    //Get the custom property from the worksheet.
                    object obj = PafApp.GetGridApp().GetCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);
                    //Make sure that the property exists, if so assign it to a var.
                    if (obj != null)
                        viewName = obj.ToString();

                    //Get a command bar object.
                    //CommandBar cb = Ctrl.Parent;

                    switch (command)
                    {
                        case "t_Calculate":
                        case "m_Calculate":
                            if (_IsRecording)
                            {
                                AddNewTestSequence(_RecordedTestSeqNumber, viewName, _BeginningState);

                                //reset the new beginning state
                                _BeginningState = null;

                                if (_BinaryFileName.Equals(""))
                                {
                                    _BinaryFileName = GetSaveAsFileName(_Th[_RecordedTestSeqNumber].Name);
                                }
                                if (!_BinaryFileName.Equals(""))
                                {
                                    BinarySerializer.Serialize(_Th, _BinaryFileName);
                                }
                            }
                            break;
                        case "t_Record":
                            if (!_IsRecording)
                            {
                                //Create a new Recorded Test.
                                frmNewRecordedTest frm = new frmNewRecordedTest(
                                    "",
                                    false,
                                    PafApp.GetGridApp().RoleSelector.UserRole,
                                    PafApp.GetGridApp().RoleSelector.UserSelectedPlanTypeSpecString);

                                //Show the new test dialog box.
                                if (frm.ShowDialog() != DialogResult.OK)
                                {
                                    //The user did not click ok, so cancel.
                                    return;
                                }
                                else
                                {
                                    //Get the paf planner config
                                    pafPlannerConfig ppc = PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig;
                                    //Reset the recorded test seq number. 
                                    _RecordedTestSeqNumber = -1;
                                    //Reset the objects.
                                    _Th = new Palladium.TestHarness.TestHarness();
                                    //Reset the file name.
                                    _BinaryFileName = "";
                                    //Reset the TestSequence number.
                                    _TestSeqNumber = 0;
                                    //Increment the RecordedTest.
                                    _RecordedTestSeqNumber++;
                                    //Add the recorded test to the test harness.
                                    AddNewRecordedTest(
                                        frm.TestName,
                                        frm.TestDescription,
                                        frm.Reload,
                                        frm.Role,
                                        frm.SeasonProcess,
                                        ppc);
                                    //Reset the beginning state
                                    _BeginningState = null;
                                }

                                //set the recording flag to true.
                                _IsRecording = true;
                                Globals.ThisWorkbook.Ribbon.InvalidateAll();

                                //set the button to depressed.
                                //Ctrl.State = MsoButtonState.msoButtonDown;

                                //Change the Icon to a stop button.
                                //Ctrl.FaceId = 2187;

                                //Set the tool tip to recording...
                                //Ctrl.TooltipText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.RecordStop");
                            }
                            else
                            {
                                //Reset the buttons state.
                                //Ctrl.State = MsoButtonState.msoButtonUp;

                                //Reset the tooltip to start recording...
                                //Ctrl.TooltipText =
                                        PafApp.GetLocalization().GetResourceManager().
                                        GetString("Application.Excel.Menu.RecordStart");

                                //Reset the icon to the record button.
                                //Ctrl.FaceId = 184;

                                //Reset the recording flag.
                                _IsRecording = false;
                                Globals.ThisWorkbook.Ribbon.InvalidateAll();

                                //Add the test sequence to the recorded test.
                                AddNewTestSequence(_RecordedTestSeqNumber, viewName, _BeginningState);

                                if (_BinaryFileName.Equals(""))
                                    _BinaryFileName = GetSaveAsFileName(_Th[_RecordedTestSeqNumber].Name);
                                if (!_BinaryFileName.Equals(""))
                                    BinarySerializer.Serialize(_Th, _BinaryFileName);
                            }

                            break;
                        case "t_LoadTest":
                            //Get the file name.

                            IsRecording = false;
                            IsRunning = true;
                            Globals.ThisWorkbook.Ribbon.InvalidateAll();
                            string fileName = GetOpenFileName();

                            //If the user select a file, then continue.
                            if (!fileName.Equals(""))
                            {

                                SuccessfullTests = 0;
                                ProtectionErrors = 0;
                                VarianceErrors = 0;
                                ExceptionErrors = 0;
                                OtherErrors = false;

                                FileInfo file = new FileInfo(fileName);
                                if (file.Extension.Equals(PafAppConstants.TTH_FILE_EXTENSION))
                                {
                                    ProcessTthFile(fileName, true, false, false);
                                }
                                else
                                {
                                    List<string> testNames = ProcessBtthFile(fileName, false, false, true);
                                    OutputTestErrorList(testNames);
                                }
                            }
                            IsRunning = false;
                            break;
                    }
                }
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                Globals.ThisWorkbook.Application.StatusBar = "";
                Globals.ThisWorkbook.Application.StatusBar = false;
            }
            catch (Exception ex)
            {
                IsRunning = false;
                IsRecording = false;
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="EventArgs"></param>
        private void TestHarnessMgr_AddChangedCells(object sender, AddChangedCellsEventArgs EventArgs)
        {
            PafApp.GetTestHarnessChangedCellMngr().Enqueue(
                new ChangedCellInfo(
                    EventArgs.SheetHash,
                    EventArgs.SheetName,
                    EventArgs.Cells,
                    EventArgs.CellValueArray,
                    EventArgs.ChangeType,
                    new List<Intersection>
                        (EventArgs.ListTracker.ProtectedCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.Changes),
                    new List<Intersection>
                        (EventArgs.ListTracker.UserLocks),
                    new List<Intersection>
                        (EventArgs.ListTracker.UnLockedChanges),
                    new List<Intersection>
                        (EventArgs.ListTracker.ReplicateAllCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.ReplicateExistingCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.LiftAllCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.LiftExistingCells)));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="EventArgs"></param>
        private void TestHarnessMgr_AddChangedCell(object sender, AddChangedCellEventArgs EventArgs)
        {
            PafApp.GetTestHarnessChangedCellMngr().Enqueue(
                new ChangedCellInfo(
                    EventArgs.SheetHash,
                    EventArgs.SheetName,
                    EventArgs.Cell.CellAddress,
                    EventArgs.Cell.Value,
                    EventArgs.ChangeType,
                    new List<Intersection>
                        (EventArgs.ListTracker.ProtectedCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.Changes),
                    new List<Intersection>
                        (EventArgs.ListTracker.UserLocks),
                    new List<Intersection>
                        (EventArgs.ListTracker.UnLockedChanges),
                    new List<Intersection>
                        (EventArgs.ListTracker.ReplicateAllCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.ReplicateExistingCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.LiftAllCells),
                    new List<Intersection>
                        (EventArgs.ListTracker.LiftExistingCells)));
        }

        /// <summary>
        /// Event handler for when a user locks a cell(s).
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="EventArgs"></param>
        private void TestHarnessMgr_UserLockedCell(object sender, UserLockedCellEventArgs EventArgs)
        {
            if (!_IsRecording)
                return;

            //if the user right clicks in the data range on the view
            if (PafApp.GetViewMngr().CurrentOlapView.DataRange.InContiguousRange(EventArgs.Cell))
            {
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Sheet: " + EventArgs.SheetName + "(" + EventArgs.SheetHash + ") adding user locked cells to ChangedCellMngr.");

                //Add the changed cells to the ChangedCellMngr
                PafApp.GetTestHarnessChangedCellMngr().Enqueue(
                    new ChangedCellInfo(
                        EventArgs.SheetHash,
                        EventArgs.SheetName,
                        EventArgs.Cell.CellAddress,
                        null,
                        Change.Locked,
                        new List<Intersection>
                            (EventArgs.ListTracker.ProtectedCells),
                        new List<Intersection>
                            (EventArgs.ListTracker.Changes),
                        new List<Intersection>
                            (EventArgs.ListTracker.UserLocks),
                        new List<Intersection>
                            (EventArgs.ListTracker.UnLockedChanges),
                        new List<Intersection>
                            (EventArgs.ListTracker.ReplicateAllCells),
                        new List<Intersection>
                            (EventArgs.ListTracker.ReplicateExistingCells),
                        new List<Intersection>
                            (EventArgs.ListTracker.LiftAllCells),
                        new List<Intersection>
                            (EventArgs.ListTracker.LiftExistingCells)));
            }
        }

        private void TestHarnessMgr_SessionLockedCell(object sender, SessionLockedCellEventArgs EventArgs)
        {
            if (!_IsRecording)
                return;

 
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Sheet: " + EventArgs.SheetName + "(" + EventArgs.SheetHash + ") adding session locked cells to ChangedCellMngr.");

            //Add the changed cells to the ChangedCellMngr
            PafApp.GetTestHarnessChangedCellMngr().Enqueue(
                new ChangedCellInfo(
                    EventArgs.SheetHash,
                    EventArgs.SheetName,
                    Change.SessionLocked,
                    new List<Intersection>(EventArgs.ListTracker.ProtectedCells),
                    new List<Intersection>(EventArgs.ListTracker.Changes),
                    new List<Intersection>(EventArgs.ListTracker.UserLocks),
                    new List<Intersection>(EventArgs.ListTracker.UnLockedChanges),
                    new List<Intersection>(EventArgs.ListTracker.ReplicateAllCells),
                    new List<Intersection>(EventArgs.ListTracker.ReplicateExistingCells),
                    new List<Intersection>(EventArgs.ListTracker.LiftAllCells),
                    new List<Intersection>(EventArgs.ListTracker.LiftExistingCells),
                    new List<Intersection>(EventArgs.ParentIntersections.ToIntersections())));
 

        }

        public void FailBtthFile(List<TestHarnessFileInfo> files)
        {
            foreach (var file in files)
            {
                FailBtthFile(file.TestHarnessFile);
            }

        }

        /// <summary>
        /// Fails all the tth files in a btth
        /// </summary>
        /// <param name="fileName">Name of the .btth file to process.</param>
        private void FailBtthFile(string fileName)
        {
            
            FileInfo file = new FileInfo(fileName);
            string parentFileDir = file.DirectoryName;

            using (StreamReader sr = new StreamReader(fileName))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (!line.Equals(""))
                    {
                        string errorMessage = "Cannot run tth file, check log";
                        FileInfo fileInfo = new FileInfo(line);
                        //Does the file exist?
                        if (File.Exists(fileInfo.FullName))
                        {
                            if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.TTH_FILE_EXTENSION))
                            {
                                FailTthFile(fileInfo.FullName, errorMessage);
                            }
                            else if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.BTTH_FILE_EXTENSION))
                            {
                                FailBtthFile(fileInfo.FullName);
                            }
                        }
                        else
                        {
                            string withParentDir = parentFileDir + Path.DirectorySeparatorChar + line;
                            if (File.Exists(withParentDir))
                            {
                                if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.TTH_FILE_EXTENSION))
                                {
                                    FailTthFile(withParentDir, errorMessage);
                                }
                                else if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.BTTH_FILE_EXTENSION))
                                {
                                    FailBtthFile(withParentDir);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Process all the .tth files in a .btth file.
        /// </summary>
        /// <param name="fileName">Name of the .btth file to process.</param>
        /// <param name="showMessages">Display error messages to the screen.</param>
        /// <param name="closeWorksheets">Close opened worksheets after processing all the test sequences.</param>
        /// <param name="fromGui">Set to true if test was kicked off using the GUI</param>
        public List<string> ProcessBtthFile(string fileName, bool showMessages, bool closeWorksheets, bool fromGui)
        {
            List<string> allFiles = GetListOfFiles(fileName, new List<string>());
            return ProcessTthFiles(allFiles, showMessages, closeWorksheets, fromGui);
        }
        
        /// <summary>
        /// Generates a list of .tth files from a .btth file.  Note that this function will handle nested .btth files
        /// Files that can't be found are not added to the final list.
        /// </summary>
        /// <param name="fileName"></param>
        /// <param name="fileList"></param>
        /// <returns></returns>
        public List<string> GetListOfFiles(string fileName, List<string> fileList)
        {
            {
                var file = new FileInfo(fileName);
                string parentFileDir = file.DirectoryName;

                using (StreamReader sr = new StreamReader(fileName))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        line = line.Trim();
                        if (!line.Equals(""))
                        {

                            FileInfo fileInfo = new FileInfo(line);
                            //Does the file exist?
                            if (File.Exists(fileInfo.FullName))
                            {
                                if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.TTH_FILE_EXTENSION))
                                {
                                    fileList.Add(fileInfo.FullName);
                                }
                                else if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.BTTH_FILE_EXTENSION))
                                {
                                    fileList = GetListOfFiles(fileInfo.FullName, fileList);
                                }
                            }
                            else
                            {
                                string fileWithParentDir = Path.Combine(parentFileDir, line);
                                if (File.Exists(fileWithParentDir))
                                {
                                    if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.TTH_FILE_EXTENSION))
                                    {
                                        fileList.Add(fileWithParentDir);
                                    }
                                    else if (fileInfo.Extension.EqualsIgnoreCase(PafAppConstants.BTTH_FILE_EXTENSION))
                                    {
                                        fileList = GetListOfFiles(fileWithParentDir, fileList);
                                    }
                                }
                                else
                                {
                                    PafApp.GetLogger().Warn("File Does not exist: " + line);
                                    PafApp.GetLogger().Warn("File Does not exist: " + fileWithParentDir);
                                }
                            }
                        }
                    }
                }
                return fileList;
            }
        }

        private void FailTthFile(string fileName, string errorMessage)
        {
            //Create a new test harnesss.
            _Th = new Palladium.TestHarness.TestHarness();
            //log the file name to be processed.
            //PafApp.GetLogger().InfoFormat("Processing tth file: {0}", new string[] {fileName});
            Console.WriteLine("##teamcity[testSuiteStarted name='{0}']", new[] { Path.GetFileName(fileName) });
            //Console.WriteLine("Processing file: {0}", new string[] { fileName });
            //Deserialize the file into an object.
            _Th = BinaryDeserializer.Deserialize(fileName);
            //If the object is not null then run the tests.
            if (_Th != null)
            {
                //Set the binary file name
                _BinaryFileName = fileName;
                for (int i = 0; i < _Th.Count; i++)
                {
                    int testSeqCount = _Th[i].TestSequences.Count;
                    //Loop through the Test sequences.
                    for (int j = 0; j < testSeqCount; j++)
                    {
                        string testName = String.Format("{0}, TS-{1}", Path.GetFileName(_BinaryFileName), j);
                        PafApp.GetLogger().InfoFormat("Starting test: {0}", new object[] {testName});
                        Console.WriteLine("##teamcity[testStarted name='{0}']", new[] {testName});
                        Console.WriteLine("##teamcity[testFailed type='Exception' name='{0}' message='{1}']", new[] {testName, errorMessage});
                        Console.WriteLine("##teamcity[testFinished name='{0}']", new[] {testName});
                    }
                }
                Console.WriteLine("##teamcity[testSuiteFinished name='{0}']", new[] { Path.GetFileName(fileName) });
            }
        }

        /// <summary>
        /// Process all the RecordedTests in a .tth file.
        /// </summary>
        /// <param name="tthFiles">List of .tth files to process</param>
        /// <param name="showMessages">Show any status/error messages in </param>
        /// <param name="closeWorksheets">Close opened worksheets after processing all the test sequences.</param>
        /// <param name="reloadDataCache">If True the data cache will be reset after the test is complete.  Note 
        /// that if the test has reset cache to false then this setting will be ignored.</param>
        /// <returns>Returns a list of the tests that completed with errors.</returns>
        public List<string> ProcessTthFiles(List<string> tthFiles, bool showMessages, bool closeWorksheets, bool reloadDataCache = true)
        {
            List<string> errorTracker = new List<string>();
            int listSize = tthFiles.Count() - 1;
            bool realodDataCache = true;
            for (int i = 0; i < tthFiles.Count(); i++)
            {
                if (!closeWorksheets && i == listSize)
                {
                    reloadDataCache = false;
                }
                errorTracker.AddRange(ProcessTthFile(tthFiles[i], showMessages, closeWorksheets, reloadDataCache));
            }

            return errorTracker;
        }


        /// <summary>
        /// Process all the RecordedTests in a .tth file.
        /// </summary>
        /// <param name="fileName">Name of the .tth file to process.</param>
        /// <param name="showMessages">Show any status/error messages in </param>
        /// <param name="closeWorksheets">Close opened worksheets after processing all the test sequences.</param>
        /// <param name="reloadDataCache">If True the data cache will be reset after the test is complete.  Note 
        /// that if the test has reset cache to false then this setting will be ignored.</param>
        /// <returns>Returns a list of the tests that completed with errors.</returns>
        public List<string> ProcessTthFile(string fileName, bool showMessages, bool closeWorksheets, bool reloadDataCache = true)
        {
            List<string> testNames = new List<string>();

            try
            {
                //Create a new test harnesss.
                _Th = new Palladium.TestHarness.TestHarness();
                //log the file name to be processed.
                //PafApp.GetLogger().InfoFormat("Processing tth file: {0}", new string[] {fileName});
                Console.WriteLine("##teamcity[testSuiteStarted name='{0}']", new [] { Path.GetFileName(fileName) });
                //Console.WriteLine("Processing file: {0}", new string[] { fileName });
                //Deserialize the file into an object.
                _Th = BinaryDeserializer.Deserialize(fileName);
                //If the object is not null then run the tests.
                if (_Th != null)
                {
                    //Set the binary file name
                    _BinaryFileName = fileName;
                    //Delete existing xlxs file.
                    PafApp.GetGridApp().DeleteWorkbook(
                       Path.GetDirectoryName(_BinaryFileName),
                       Path.GetFileNameWithoutExtension(_BinaryFileName));
                    //Create the test harness protection manager object.
                    _ThProtMngr = new TestHarnessProtMngr();


                    Stopwatch sw1 = Stopwatch.StartNew();
                    //Loop through the recorded tests.
                    for (int i = 0; i < _Th.Count; i++)
                    {
                        
                        ////create a role filter);)
                        if(! CreateUOW(
                            PafApp.GetGridApp().LogonInformation.UserName,
                            PafApp.GetGridApp().RoleSelector.UserRole,
                            PafApp.GetGridApp().RoleSelector.UserPlanTypeSpecString,
                            _Th[0]))
                        {
                            //throw new Exception(PafApp.GetLocalization().GetResourceManager().GetString(String.Format("Application.Exception.TestHarnessMgr.CannotCreateUow", new string[] { fileName} )));
                            throw new Exception(String.Format(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.TestHarnessMgr.CannotCreateUow"), new[] { _BinaryFileName }));
                        }

                        //get the count of test sequence.
                        int testSeqCount = _Th[i].TestSequences.Count;
                        //Loop through the Test sequences.
                        for (int j = 0; j < testSeqCount; j++)
                        {
                            string testName = String.Format("{0}, TS-{1}", Path.GetFileName(_BinaryFileName), j);
                            PafApp.GetLogger().InfoFormat("Starting test: {0}", new object[]{testName});
                            Console.WriteLine("##teamcity[testStarted name='{0}']", new[] { testName });
                            Globals.ThisWorkbook.Application.StatusBar = String.Format("Running test: {0}", new object[] {testName});
                            //run the test.
                            string errorMessage = String.Empty;
                            Stopwatch sw = Stopwatch.StartNew();
                            TestSequenceErrorType seqErrType = RunTestSequence(i, j, showMessages, out errorMessage);
                            sw.Stop();
                            PafApp.GetLogger().InfoFormat("{0} execution time: {1} (ms)", new object[] { testName, sw.ElapsedMilliseconds.ToString("N0") });
                            //check for the error the error code.
                            switch(seqErrType)
                            {
                                case TestSequenceErrorType.Exception:
                                    ExceptionErrors++;
                                    Console.WriteLine("##teamcity[testFailed type='Exception' name='{0}' message='{1}']", new[] { testName, errorMessage });
                                    testNames.Add(_Th[i].Name + " - " + _Th[i].TestSequences[j].SequenceNumber);
                                    break;
                                case TestSequenceErrorType.Protection:
                                    ProtectionErrors++;
                                    Console.WriteLine("##teamcity[testFailed type='Protection' name='{0}' message='{1}']", new[] { testName, errorMessage });
                                    testNames.Add(_Th[i].Name + " - " + _Th[i].TestSequences[j].SequenceNumber);
                                    break;
                                case TestSequenceErrorType.Variance:
                                    VarianceErrors++;
                                    Console.WriteLine("##teamcity[testFailed type='Variance' name='{0}' message='{1}']", new[] { testName, errorMessage });
                                    testNames.Add(_Th[i].Name + " - " + _Th[i].TestSequences[j].SequenceNumber);
                                    break;
                                case TestSequenceErrorType.None:
                                    _SuccessfullTests++;
                                    break;
                            }
                            Console.WriteLine("##teamcity[testFinished name='{0}']", new[] { testName });
                        }
                        //Reset any session locks.
                        PafApp.GetViewMngr().GlobalLockMngr.Clear();
                        //Reload the data cache to the default data...
                        if (reloadDataCache)
                        {
                            ReloadDataCache(_Th[0], _Th[0].TestSequences[0].ViewName, 0);
                        }
                        //fix issue where .tth files with multiple seq and
                        //different view have issue running in .btth files
                        if (closeWorksheets)
                        {
                            PafApp.GetGridApp().RemoveViewSheets();
                        }
                    }

                    
                    sw1.Stop();
                    PafApp.GetLogger().Info(Path.GetFileName(_BinaryFileName) + " execution time: " + sw1.ElapsedMilliseconds.ToString("N0") + " (ms)");

                    //Check to if their are any test in the list.  If so throw a message at the user.
                    if (testNames.Count > 0 && showMessages)
                    {
                        OutputTestErrorList(testNames);
                    }
                }

                Console.WriteLine("##teamcity[testSuiteFinished name='{0}']", new[] { Path.GetFileName(fileName) });
                return testNames;
            }
            catch (SerializationException ex)
            {
                Console.WriteLine("##teamcity[testFailed type='Exception' name='{0}' message='Error deserializing file:{0}']", new[] { fileName });
                List<string> lst = new List<string>();
                lst.Add(fileName);
                if (showMessages)
                {
                    PafApp.MessageBox().Show(
                        String.Format("Error deserializing file: {0}", new [] {fileName}),
                        ex.Source,
                        ex.StackTrace,
                        SystemIcons.Error,
                        frmMessageBox.frmMessageBoxButtons.OK);
                }
                else
                {
                    PafApp.GetLogger().ErrorFormat("Error deserializing file: {0}", new [] { fileName });
                }
                Globals.ThisWorkbook.Application.StatusBar = false;
                return lst;
            }
            catch (Exception ex)
            {
                if (showMessages)
                    PafApp.MessageBox().Show(ex);
                else
                    PafApp.GetLogger().Error(ex);

                //Console.WriteLine("##teamcity[testFailed type='Exception' name='{0}' message='{1}']", new[] { fileName, ex.Message });
                PafApp.GetLogger().ErrorFormat("Exception occured in file: {0}, message: {1}", new[] { fileName, ex.Message });
                Globals.ThisWorkbook.Application.StatusBar = false;
                List<string> lst = new List<string>();
                lst.Add(fileName);
                return lst;
            }
        }

        /// <summary>
        /// Output a list of tests that occured while running the test harness.
        /// </summary>
        /// <param name="testNames">The list of tests that failed.</param>
        private void OutputTestErrorList(List<string> testNames)
        {
            if (_OtherErrors)
            {
                testNames.Insert(0, "Other errors may have occured, please check the error log for full details");
            }

            if (testNames.Count > 0)
            {
                MessageBox.Show(
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.TestHarness.Exception.TestsWithErrors"),
                        String.Join(", ", testNames.ToArray())),
                    PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Title"),
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation);
            }
        }

        /// <summary>
        /// Gets a open as file name using the windows open as dialog box.
        /// </summary>
        /// <returns>A string file name, or "" if the user canceled out.</returns>
        private string GetOpenFileName()
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "All Test Harness Files (*.tth, *.btth)|*.tth; *.btth|Test Harness Files (*.tth)|*.tth|Test Harness Batch Files (*.btth)|*.btth";
            open.Multiselect = false;
            open.CheckFileExists = true;
            open.CheckPathExists = true;
            open.ShowHelp = false;
            open.SupportMultiDottedExtensions = true;
            open.ShowReadOnly = false;
            open.Title = "";
            open.ValidateNames = true;
            try
            {
                if (open.ShowDialog() == DialogResult.OK)
                {
                    _BinaryFileName = open.FileName;
                    return _BinaryFileName;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return "";
            }
        }

        /// <summary>
        /// Gets a save as file name using the windows save as dialog box.
        /// </summary>
        /// <param name="suggestion">A default file name to enter in the box.  Use an empty string or null to indicate no default value.</param>
        /// <returns>>A string file name, or "" if the user canceled out.</returns>
        private string GetSaveAsFileName(string suggestion)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.AddExtension = true;
            save.CheckFileExists = false;
            save.CheckPathExists = true;
            save.DefaultExt = PafAppConstants.TTH_FILE_EXTENSION;
            save.Filter = "Test Harness (*.tth)|*.tth";
            save.ShowHelp = false;
            save.SupportMultiDottedExtensions = true;
            save.Title = "";
            save.ValidateNames = true;

            if (! String.IsNullOrEmpty(suggestion))
                save.FileName = suggestion;
            else
                save.FileName = "";

            try
            {
                if (save.ShowDialog() == DialogResult.OK)
                {
                    _BinaryFileName = save.FileName;
                    return _BinaryFileName;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return "";
            }
        }

        /// <summary>
        /// Gets an Excel.Range from a contiguousrange.
        /// </summary>
        /// <param name="ContigRng">The contiguousrange</param>
        /// <param name="sheet">Worksheet where the ranges are located.</param>
        /// <returns>An Excel.Range.</returns>
        public Range GetRange(ContiguousRange ContigRng, Worksheet sheet)
        {
            Range Rng;
            {
                int TopLeftRow;
                TopLeftRow = ContigRng.TopLeft.Row;
                int TopLeftCol = ContigRng.TopLeft.Col;
                int BottomRightRow = ContigRng.BottomRight.Row;
                int BottomLeftCol = ContigRng.BottomRight.Col;

                Range TopLeftRng;
                TopLeftRng = (Range)sheet.Cells[TopLeftRow, TopLeftCol];

                if (TopLeftRow == BottomRightRow && TopLeftCol == BottomLeftCol)
                {
                    Rng = TopLeftRng;
                }
                else
                {
                    Range BottomRightRng;
                    BottomRightRng = (Range)sheet.Cells[BottomRightRow, BottomLeftCol];
                    Rng = sheet.get_Range(TopLeftRng, BottomRightRng);
                }
            }
            return Rng;
        }

        /// <summary>
        /// Adds a new RecordedTest to the current TestHarness.
        /// </summary>
        /// <param name="name">Unique name of the RecordedTest.</param>
        /// <param name="description">Description of the recorded test.</param>
        /// <param name="reload">Reload the data cache before running the test.</param>
        /// <param name="role">User role to use when running the test.</param>
        /// <param name="season">The season/process to use when running the test.</param>
        /// <param name="pafPlannerConfig">The current paf planner config.</param>
        private void AddNewRecordedTest(string name, string description, bool reload, 
            string role, string season, pafPlannerConfig pafPlannerConfig)
        {
            //Adds a new recorded test, to the test harness.
            if (pafPlannerConfig.isUserFilteredUow && pafPlannerConfig.isUserFilteredUow)
            {
                _Th.Add(
                    new RecordedTest(
                        name,
                        description,
                        reload,
                        role,
                        season,
                        true,
                        PafApp.GetGridApp().RoleFilter.PafDimSpecs,
                        PafApp.GetGridApp().RoleFilter.SuppressInvalidInxChk,
                        PafApp.GetGridApp().RoleFilter.FilteredSubtotalsChk));
            }
            else
            {
                _Th.Add(
                    new RecordedTest(
                        name,
                        description,
                        reload,
                        role,
                        season,
                        PafApp.GetGridApp().RoleSelector.SuppressInvalidInx,
                        PafApp.GetGridApp().RoleSelector.FilteredSubtotals));
            }
        }

        /// <summary>
        /// Adds a TestSequence to a RecordedTest.
        /// </summary>
        /// <param name="recordedTestIndex">The index of the RecordedTest to add the TestSequence to.</param>
        /// <param name="viewName">Name of the view associated to the test.</param>
        /// <param name="beginningState"></param>
        private void AddNewTestSequence(int recordedTestIndex, string viewName, TestSequenceBeginningState beginningState)
        {
            //Create a new test sequence.
            TestSequence ts = CreateTestSequence(viewName, beginningState);

            //If the test sequence is not null, then add it to the current recorded test.
            if (ts != null)
            {
                _Th[recordedTestIndex].TestSequences.Add(ts);
            }
        }


        /// <summary>
        /// Creates a role filter uow.
        /// </summary>
        /// <param name="userId">The user to logon to the server.</param>
        /// <param name="role">The current role.</param>
        /// <param name="process">THe current process.</param>
        /// <param name="rt">The recorded test to build the role filter from.</param>
        private bool CreateUOW(string userId, string role, string process, RecordedTest rt)
        {
            bool rebuildActionsPane = false;
            bool ret = true;
            try
            {

                //get the pafPlannerConfig
                pafPlannerConfig ppc = PafApp.GetGridApp().RoleSelector.FindPlannerConfig(role, process);
                

                if(rt.RoleFilterEnabled != ppc.isUserFilteredUow)
                {
                    rt.RoleFilterEnabled = false;
                    rt.InvalidIntersection = ppc.isDataFilteredUow;
                    rt.RoleFilterSelections = null;
                }

                if (rt.RoleFilterEnabled)
                {
                    //Remove the cached user selections.
                    PafApp.GetGridApp().ActionsPane.ClearCachedPafUserSelections();
                    PafApp.GetGridApp().ActionsPane.ClearCachedUserSelections();
                    PafApp.GetGridApp().ActionsPane.ClearActionsPane();
                    //PafApp.GetViewMngr().Views.Clear();
                    //remove all the view sheets.
                    PafApp.GetGridApp().RemoveViewSheets();
                    //clear out any cell notes...
                    PafApp.GetCellNoteMngr().ClearBuckets();
                    PafApp.GetCellNoteMngr().ClearCache();
                    //TTN-880
                    PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = false;
                    

                    PafApp.GetReinitClientState();
                    ppc = PafApp.GetGridApp().RoleSelector.FindPlannerConfig(role, process);
                    PafApp.GetUowCalculator().CalculateUow(
                           null,
                           ppc,
                           userId,
                           role,
                           process,
                           rt);

                    rebuildActionsPane = true;
                }
                else
                {
                    if (PafApp.GetPafPlanSessionResponse() == null || PafApp.GetPafPlanSessionResponse().dimTrees == null)
                    {
                        PafApp.GetUowCalculator().CalculateUow(
                           null,
                           ppc,
                           userId,
                           role,
                           process,
                           rt);

                        rebuildActionsPane = true;
                    }
                }
                return ret;
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return false;
            }
            finally
            {
                if (rebuildActionsPane)
                {
                    PafApp.GetCommandBarMngr().BuildActionsPane();
                }
            }
        }

        /// <summary>
        /// Creates a new test sequence.
        /// </summary>
        /// <param name="viewName">Name of the view associated to the test.</param>
        /// <param name="beginningState"></param>
        /// <returns>A new TestSequence or null if an error occurs.</returns>
        private TestSequence CreateTestSequence(string viewName, TestSequenceBeginningState beginningState)
        {
            if(PafApp.GetTestHarnessChangedCellMngr().Count == 0)
                return null;

            //Create a new test sequence.
            TestSequence ts = new TestSequence();

            //Set the testsequence - sequence number.
            ts.SequenceNumber = _TestSeqNumber.ToString();

            //set the row/column suppressed options.
            bool[] settings = PafApp.GetGridApp().ActionsPane.GetCachedSuppressZeroSelections(viewName);
            if (settings != null && settings.Length >= 2)
            {
                ts.RowsSuppressed = settings[0];
                ts.ColumnsSuppressed = settings[1];
            }
            else
            {
                ts.RowsSuppressed = false;
                ts.ColumnsSuppressed = false;
            }

            //Set the view name.
            ts.ViewName = viewName;

            //Get the name of the rule set being used.
            ts.RuleSetName = PafApp.RuleSetName;

            //Set the test sequence results.
            ts.TestSequenceResults = new TestSequenceResults(new CellAddress(1, 1),
                            PafApp.GetViewMngr().CurrentOlapView.DataRange.BottomRight,
                            PafApp.GetViewMngr().CurrentGrid.GetValueObjectArray(
                                new ContiguousRange(new CellAddress(1, 1),
                                PafApp.GetViewMngr().CurrentOlapView.DataRange.BottomRight)));

            //Check to see if the view is dynamic, if so then set the flag to true and get the user selections.
            if (PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName))
            {
                ts.IsDynamic = true;
                pafUserSelection[] cachedSel = PafApp.GetGridApp().ActionsPane.GetCachedDyanmicPafUserSelections(viewName);
                
                if(cachedSel != null)
                {
                    ts.PafUserSelections = MakeCopyOf(cachedSel);
                }
            }
            else
            {
                ts.IsDynamic = false;
                ts.PafUserSelections = null;
            }

            //Set the beginning state of the test sequence.
            ts.TestSequenceBeginningState = beginningState;

            //If the sequence number is zero then no parent exists.  If not then set it to the previous test.
            if (ts.SequenceNumber.Equals("0"))
                ts.Parent = null;
            else
                ts.Parent = _Th[_RecordedTestSeqNumber].TestSequences[_TestSeqNumber - 1].SequenceNumber;
            
            //Adds the changed cells to the changedcelllist.
            ts.ChangedCellInfoList.AddRange(
                PafApp.GetTestHarnessChangedCellMngr());

            //Clears the test harness changed cell manager.
            PafApp.GetTestHarnessChangedCellMngr().Clear();

            //Increment the test sequence number.
            _TestSeqNumber++;

            return ts;
        }

        /// <summary>
        /// Make a copy of the pafUserSelection array.
        /// </summary>
        /// <param name="userSels">The pafUserSelection array to copy.</param>
        /// <returns>A deep copy of the pafUserSelection array.</returns>
        public pafUserSelection[] MakeCopyOf(pafUserSelection[] userSels)
        {
            if (userSels != null)
            {
                pafUserSelection[] tempSel = new pafUserSelection[userSels.Length];
                for (int i = 0; i < tempSel.Length; i++)
                {
                    tempSel[i] = new pafUserSelection();

                    tempSel[i].dimension = userSels[i].dimension;
                    tempSel[i].id = userSels[i].id;
                    tempSel[i].multiples = userSels[i].multiples;

                    tempSel[i].pafAxis = new pafAxis();
                    tempSel[i].pafAxis.colAxis = userSels[i].pafAxis.colAxis;
                    tempSel[i].pafAxis.pageAxis = userSels[i].pafAxis.pageAxis;
                    tempSel[i].pafAxis.rowAxis = userSels[i].pafAxis.rowAxis;
                    tempSel[i].pafAxis.value = userSels[i].pafAxis.value;

                    tempSel[i].promptString = userSels[i].promptString;

                    tempSel[i].values = new string[userSels[i].values.Length];
                    userSels[i].values.CopyTo(tempSel[i].values, 0);
                }
                return tempSel;
            }
            else
            {
                return null;
            }
        }

    }
}
