#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.PafService;

namespace Titan.Pace.DataStructures
{
    /// <summary>
    /// Used to hold paf server values.  This in java terms would but a POJO but
    /// since we are using microsoft , it's a POMO (Plain Old Microsoft Object)!
    /// </summary>
    class PafServer
    {

        private PafServiceProviderService _PafService;

        private string _ClientId;

        private string _SessionToken;

        private string _Platform;

        private string _ServerVersion;

        private bool _ClientPasswordResetEnabled;

        private int? _MinNewPasswordLength;

        private int? _MaxNewPasswordLength;

        private string _url;

        private string _DataSourceId;

        private string _ApplicationId;

        private string _ClientMinVersion;

        private bool _ClientUpgradeRequired;

        private string _ClientUpgradeUrl;

        private authMode _AuthMode;


        public bool ClientUpgradeRequired
        {
            get { return _ClientUpgradeRequired; }
            set { _ClientUpgradeRequired = value; }
        }

        public string ClientUpgradeUrl
        {
            get { return _ClientUpgradeUrl; }
            set { _ClientUpgradeUrl = value; }
        }
        
        
        public string ClientMinVersion
        {
            get { return _ClientMinVersion; }
            set { _ClientMinVersion = value; }
        }

        public PafServiceProviderService PafService
        {
            get { return _PafService; }
            set { _PafService = value; }
        }

        public string ClientId
        {
            get { return _ClientId; }
            set { _ClientId = value; }
        }

        public string Platform
        {
            get { return _Platform; }
            set { _Platform = value; }
        }

        public string ServerVersion
        {
            get { return _ServerVersion; }
            set { _ServerVersion = value; }
        }

        public bool ClientPasswordResetEnabled
        {
            get { return _ClientPasswordResetEnabled; }
            set { _ClientPasswordResetEnabled = value; }
        }

        public int? MinNewPasswordLength
        {
            get { return _MinNewPasswordLength; }
            set { _MinNewPasswordLength = value; }
        }

        public int? MaxNewPasswordLength
        {
            get { return _MaxNewPasswordLength; }
            set { _MaxNewPasswordLength = value; }
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        public string DataSourceId
        {
            get { return _DataSourceId; }
            set { _DataSourceId = value; }
        }

        public string ApplicationId
        {
            get { return _ApplicationId; }
            set { _ApplicationId = value; }
        }

        public string SessionToken
        {
            get { return _SessionToken; }
            set { _SessionToken = value; }
        }

        public authMode AuthMode
        {
            get { return _AuthMode; }
            set { _AuthMode = value; }
        }

        public bool SessionActive
        {
            get
            {
                return PafApp.IsSessionActive(_ClientId);
            }
        }
    }
}
