#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.DataStructures
{
    class PafLogonInfo: ICloneable
    {
        private string _UserName;
        private string _UserDomain;
        private string _UserPassword;
        private string _UserSID;
        private string _IV;

        /// <summary>
        /// Overloaded constructor.
        /// </summary>
        /// <param name="userName">User name.</param>
        /// <param name="password">Password.</param>
        /// <param name="domain">User domain.</param>
        /// <param name="sid">User SID(token)</param>
        /// <param name="iv">IV used to encrypt the information.</param>
        public PafLogonInfo(string userName, string password, string domain, string sid, string iv)
        {
            _UserName = userName;
            _UserDomain = domain;
            _UserPassword = password;
            _UserSID = sid;
            _IV = iv;
        }

        /// <summary>
        /// User name.
        /// </summary>
        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }

        /// <summary>
        /// Users domain.
        /// </summary>
        public string UserDomain
        {
            get { return _UserDomain; }
            set { _UserDomain = value; }
        }

        /// <summary>
        /// Users password.
        /// </summary>
        public string UserPassword
        {
            get { return _UserPassword; }
            set { _UserPassword = value; }
        }

        /// <summary>
        /// Users SID(Token)
        /// </summary>
        public string UserSID
        {
            get { return _UserSID; }
            set { _UserSID = value; }
        }

        /// <summary>
        /// IV used to encrypt the information.
        /// </summary>
        public string IV
        {
            get { return _IV; }
            set { _IV = value; }
        }



        #region ICloneable Members

        /// <summary>
        /// Get a shallow copy of the PafLogonInfo.
        /// </summary>
        /// <returns>A shallow copy of the PafLogonInfo.</returns>
        public object Clone()
        {
            return MemberwiseClone();
        }

        #endregion

        /// <summary>
        /// Get a deep copy of the PafLogonInfo.
        /// </summary>
        /// <returns>A deep copy of the PafLogonInfo.</returns>
        public PafLogonInfo DeepClone()
        {
           return new PafLogonInfo(_UserName, _UserPassword, _UserDomain, _UserSID, _UserSID);
        }

        
    }
}