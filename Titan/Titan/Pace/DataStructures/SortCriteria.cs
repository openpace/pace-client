#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.DataStructures {


	internal class SortCriteria : IComparable<SortCriteria>
    {
	    public SortCriteria()
        {
            
        }

	    public object SortOn1 { get; set; }

	    public object SortOn2 { get; set; }

	    public object SortOn3 { get; set; }

	    public bool Ascending1 { get; set; }

	    public bool Ascending2 { get; set; }

	    public bool Ascending3 { get; set; }

	    public int Position { get; set; }

	    public TupleSet TupleSet { get; set; }


	    public int CompareTo(SortCriteria sortObject )
        {
            int result = 0;

            //Blank rows must sort to the bottom on both ascending and descending sorts
            if (Ascending1 == false)
            {
                if (SortOn1 == null || SortOn1.Equals(Double.PositiveInfinity))
                {
                    SortOn1 = double.NegativeInfinity;
                }

                if (sortObject.SortOn1 == null || sortObject.SortOn1.Equals(Double.PositiveInfinity))
                {
                    sortObject.SortOn1 = double.NegativeInfinity;
                }
            }


		    //result = this.SortOn1.CompareTo(SortObject.SortOn1);
            if (SortOn1 is string || sortObject.SortOn1 is string)
            {
                result = String.Compare(Convert.ToString(SortOn1), Convert.ToString(sortObject.SortOn1), true, PafApp.GetLocalization().GetCurrentCulture());
            }
            else if (SortOn1 is double)
            {
                result = Convert.ToDouble(SortOn1).CompareTo(sortObject.SortOn1);
            }



            if (result == 0)
            {
                //result = this.SortOn2.CompareTo(SortObject.SortOn2);
                if (SortOn2 is string || sortObject.SortOn2 is string)
                {
                    result = String.Compare(Convert.ToString(SortOn2), Convert.ToString(sortObject.SortOn2), true, PafApp.GetLocalization().GetCurrentCulture());
                }
                else if (SortOn2 is double)
                {
                    result = Convert.ToDouble(SortOn2).CompareTo(sortObject.SortOn2);
                }

                if (result == 0)
                {
                    //result = this.SortOn3.CompareTo(SortObject.SortOn3);
                    if (SortOn3 is string || sortObject.SortOn3 is string)
                    {
                        result = String.Compare(Convert.ToString(SortOn3), Convert.ToString(sortObject.SortOn3), true, PafApp.GetLocalization().GetCurrentCulture());
                    }
                    else if (SortOn3 is double)
                    {
                        result = Convert.ToDouble(SortOn3).CompareTo(sortObject.SortOn3);
                    }


                    if (result == 0)
                    {
                        return Position.CompareTo(sortObject.Position);
                    }
                    else
                    {
                        if (Ascending3)
                        {
                            return result;
                        }
                        else
                        {
                            return result * (-1);
                        }
                    }
                }
                else
                {
                    if (Ascending2)
                    {
                        return result;
                    }
                    else
                    {
                        return result * (-1);
                    }
                }
            }
            else
            {
                if (Ascending1)
                {
                    return result;
                }
                else
                {
                    return result * (-1);
                }
            }
        }
    }
}