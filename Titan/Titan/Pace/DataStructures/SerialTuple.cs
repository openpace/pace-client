﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Xml.Serialization;

namespace Titan.Pace.DataStructures
{
    [Serializable]
    [XmlRoot("Tuple")]
    public class SerialTuple<TK1, TK2> : IEquatable<SerialTuple<TK1, TK2>>
    {
        [XmlElement("Item1")]
        public TK1 Item1 { get; set; }
        
        [XmlElement("Item2")]
        public TK2 Item2 { get; set; }


        public SerialTuple()
        {
        }

        public SerialTuple(TK1 item1, TK2 item2)
        {
            Item1 = item1;
            Item2 = item2;

        }

        public override int GetHashCode()
        {
            const int prime = 31;
            int result = 1;
            result = prime * result + ((Item1.Equals(null)) ? 0 : Item1.GetHashCode());
            result = prime * result + ((Item2.Equals(null)) ? 0 : Item2.GetHashCode());
            return result;
        }

        public bool Equals(SerialTuple<TK1, TK2> other)
        {
            if (!Item1.Equals(other.Item1)) return false;
            if (!Item2.Equals(other.Item2)) return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj is SerialTuple<TK1, TK2>)
            {
                return Equals((SerialTuple<TK1, TK2>)obj);
            }
            return false;
        }


    }
}
