#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Titan.Pace.DataStructures.Old
{
    /// <summary>
    /// Summary description for Tuple.
    /// </summary>
    [Serializable]
    [Obsolete("Do not use this class in any new code.  Use: Titan.Palladium.DataStructures.Intersection")]
    public class Intersection : ICloneable
    {
        private readonly string[,] _Coordinates;
        private const int _Axis = 0;
        private const int _Coord = 1;
        private int _UniqueID = 0;
        private int _Cardinality = 0;
        private bool _HasChanged = false;
        private string[] _AxisSeq = null;
        /// <summary>
        /// Coordinate value for all values not specified.
        /// </summary>
        public const string NotIntialized = "[not intialized]";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="axisInSeq"></param>
        public Intersection(IList<string> axisInSeq)
        {
            _Coordinates = new string[axisInSeq.Count, 2];
            for (int i = 0; i < axisInSeq.Count; i++)
            {
                _Coordinates[i, _Axis] = axisInSeq[i];
                _Coordinates[i, _Coord] = NotIntialized;
            }
            _HasChanged = true;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="axisInSeq"></param>
        /// <param name="coordinates"></param>
        public Intersection(IList<string> axisInSeq, IList<string> coordinates)
        {
            _Coordinates = new string[axisInSeq.Count, 2];
            for (int i = 0; i < axisInSeq.Count; i++)
            {
                _Coordinates[i, _Axis] = axisInSeq[i];
                _Coordinates[i, _Coord] = coordinates[i];
            }
            _HasChanged = true;
        }

        /// <summary>
        /// Creates a deep clone of the Intersection object
        /// </summary>
        /// <returns></returns>
        public Object Clone()
        {
            return new Intersection(AxisSequence, Coordinates);
        }

        /// <summary>
        /// Array of coordinates.
        /// </summary>
        public string[] Coordinates
        {
            get
            {
                string[] coord = new string[Cardinality];

                for (int i = 0; i < coord.Length; i++)
                {
                    coord[i] = _Coordinates[i, _Coord];
                }
                return coord;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        public void SetCoordinates(IEnumerable<string> values)
        {
            int i = 0;
            foreach (string s in values)
            {
                _Coordinates[i, 1] = s;
                i++;
            }

            //for (int i = 0; i < values.Length; i++)
            //{
            //    _Coordinates[i, 1] = values[i];
            //}
            //_UniqueID = this.ToString().GetHashCode();
            _HasChanged = true;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="axis"></param>
        /// <param name="value"></param>
        public void SetCoordinate(string axis, string value)
        {
            int index = GetAxisIndex(axis);
            if (index > -1)
            {
                _Coordinates[index, _Coord] = value;
            }
            else
            {
                throw new ArgumentException(PafApp.GetLocalization().
                    GetResourceManager().GetString("Application.Exception.Class.Intersection.CoordinateError"));
            }
            //_UniqueID = this.ToString().GetHashCode();
            _HasChanged = true;
        }

        /// <summary>
        /// Returns the dimension member for a given dimension 
        /// </summary>
        /// <param name="axis">dimension</param>
        /// <returns>dimension member</returns>
        public string GetCoordinate(string axis)
        {
            int index = GetAxisIndex(axis);
            if (index > -1)
            {
                return _Coordinates[index, _Coord];
            }
            else
            {
                throw new ArgumentException(PafApp.GetLocalization().
                    GetResourceManager().GetString("Application.Exception.Class.Intersection.CoordinateError"));
            }
        }

        /// <summary>
        /// Searches a intersection for a coordinate.
        /// </summary>
        /// <param name="coordinate">Coordinate to search for.</param>
        /// <returns>true if found, false if not.</returns>
        public bool ContainsCoordinate(string coordinate)
        {
            if (String.IsNullOrEmpty(coordinate))
            {
                return false;
            }
            if (AxisSequence == null || AxisSequence.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < AxisSequence.Length; i++)
            {
                if (_Coordinates[i, _Coord].Equals(coordinate))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Searches a intersection for a axis.
        /// </summary>
        /// <param name="axis">Axis to search for.</param>
        /// <returns>true if found, false if not.</returns>
        public bool ContainsAxis(string axis)
        {
            if (String.IsNullOrEmpty(axis))
            {
                return false;
            }
            int index = GetAxisIndex(axis);
            if (index > -1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Converts the object to a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return ToShortString();
        }

        /// <summary>
        /// Converts the object to a long string.
        /// </summary>
        /// <returns></returns>
        public string ToLongString()
        {
            string s = "(";
            int card = Cardinality;

            for (int i = 0; i < card; i++)
            {
                s += _Coordinates[i, _Axis] + ":" + _Coordinates[i, _Coord] + ", ";
            }

            s = s.TrimEnd(new char[2] { ' ', ',' });
            return s + ")";
        }

        /// <summary>
        /// Converts the object to a short string.
        /// </summary>
        /// <returns></returns>
        public string ToShortString()
        {
            StringBuilder s = new StringBuilder();
            int card = Cardinality;

            s.Append("(");
            for (int i = 0; i < card; i++)
            {
                s.Append( _Coordinates[i, _Coord] + ", ");
            }

            return s.ToString().TrimEnd(new char[2] { ' ', ',' }) + ")";
        }

        /// <summary>
        /// The number of Dimensions in the intersection
        /// </summary>
        public int Cardinality
        {
            get 
            {
                if (_Cardinality == 0)
                {
                    _Cardinality = _Coordinates.GetLength(0);
                }
                return _Cardinality;
            } 
        }

        /// <summary>
        /// 
        /// </summary>
        public string[] AxisSequence
        {
            get
            {
                if (_AxisSeq == null)
                {
                    _AxisSeq = new string[Cardinality];

                    for (int i = 0; i < _AxisSeq.Length; i++)
                    {
                        _AxisSeq[i] = _Coordinates[i, _Axis];
                    }
                }
                return _AxisSeq;
            }
        }

        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public override bool Equals(object obj) 
        {
            if (!(obj is Intersection)) return false;

            Intersection temp = (Intersection)obj;

            if (temp.UniqueID != UniqueID)
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Return the hashcode of the object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        [DebuggerHidden]
        public override int GetHashCode()
        {
            int result = 17;
            for (int i = 0; i < _Coordinates.GetLength(0); i++)
            {
                result = 37 * result + _Coordinates[i, _Axis].GetHashCode() + _Coordinates[i, _Coord].GetHashCode();
            }
            return result;           
        }

        /// <summary>
        /// Searches for an axis.
        /// </summary>
        /// <param name="axis">The axis to search for.</param>
        /// <returns>Returns the index of the axis if found, -1 if the axis is not found.</returns>
        private int GetAxisIndex(string axis)
        {
            int card = Cardinality;

            for (int i = 0; i < card; i++)
            {
                if (axis == _Coordinates[i, _Axis])
                {
                    return i;
                }
            }
            return -1;
        }

        /// <summary>
        /// Return the uniqueid.
        /// </summary>
        public int UniqueID
        {
            get
            {
                if (_HasChanged)
                {
                    _UniqueID = ToString().GetHashCode();
                    _HasChanged = false;
                }
                return _UniqueID;
            }
        }
    }
}



