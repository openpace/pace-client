#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace Titan.Pace.DataStructures
{
    internal class Tuple : IEquatable<Tuple>
    {
        /// <summary>
        /// The position that the tuple appears on the row or column
        /// </summary>
        public int Position { get; private set; }

        /// <summary>
        /// Indicates whether the tuple is on the row or column axis
        /// </summary>
        public ViewAxis ViewAxis { get; private set; }

        /// <summary>
        /// Indicates whether the tuple is a member tag or not.
        /// </summary>
        public bool IsMemberTag { get; private set; }

        /// <summary>
        /// Gets the ArrayList of tuple members.
        /// </summary>
        public List<string> Members { get; private set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="members">Array of tuple members.</param>
        public Tuple(params string[] members)
        {
            IsMemberTag = false;
            Position = 0;
            if (Members == null)
            {
                Members = new List<string>(members.Length);
            }
            Members.AddRange(members);
        }

        public Tuple(ViewAxis viewAxis, List<string> members):this(members.ToArray())
        {
            ViewAxis = viewAxis;
        }

        public Tuple(int position, ViewAxis viewAxis, bool memberTag)
        {
            Position = position;
            ViewAxis = viewAxis;
            IsMemberTag = memberTag;
            Members = new List<string>(5);
        }

        public Tuple(int position, ViewAxis viewAxis, params string[] members) : this(members)
        {
            Position = position;
            ViewAxis = viewAxis;
        }

        public Tuple(int position, ViewAxis viewAxis, bool memberTag, params string[] members)
            : this(members)
        {
            Position = position;
            ViewAxis = viewAxis;
            IsMemberTag = memberTag;
        }
        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="temp">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public bool Equals(Tuple temp)
        {
            for ( int i = 0; i < temp.Members.Count; i++)
            {
                if (!Members.Contains(temp.Members[i]))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Return the hashcode of the object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        public override int GetHashCode()
        {
            return Members.GetHashCode();
        }
    }
}