#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace Titan.Pace.DataStructures
{
    /// <summary>
    /// This is an extension of CacheSelections but with an additional key at the beginning of the 
    /// structure.  This is a dictionay of a dictionary.
    /// </summary>
    /// <typeparam name="K">First key.</typeparam>
    /// <typeparam name="V">Key of the Dictionary.</typeparam>
    /// <typeparam name="X">Value of the dictionary.</typeparam>
    [Obsolete("Do not use this class in any new code.")]
    internal class CachedDictionary<K, V, X> : CachedSelections<K, V, X>
    {
        private readonly Dictionary<K, Dictionary<V, X>> _DynamicUserSelections;

        public CachedDictionary()
        {
            _DynamicUserSelections = new Dictionary<K, Dictionary<V, X>>();
        }

        /// <summary>
        /// Get a list of X values
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <returns>A list x values, or null if no selections have been made.</returns>
        public override List<X> GetCachedSelections(K key, V value)
        {
            X item = GetCachedSelection(key, value);
            List<X> items = new List<X>();
            items.Add(item);
            return items;
        }

        /// <summary>
        /// Determins if the object contains a value
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">The second key</param>
        /// <param name="value">Value</param>
        /// <returns>true if found, false if not.</returns>
        public bool TryGetValue(K key, V key2, out X value)
        {
            Dictionary<V, X> ret;
            value = default(X);
            if (_DynamicUserSelections.TryGetValue(key, out ret))
            {
                if (ret.TryGetValue(key2, out value))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get a list of X values
        /// </summary>
        /// <param name="key">key</param>
        /// <param name="value">value</param>
        /// <returns>A list x values, or null if no selections have been made.</returns>
        public override X GetCachedSelection(K key, V value)
        {
            if (_DynamicUserSelections.ContainsKey(key))
            {
                //Get the value dictionary
                Dictionary<V, X> dimTreeNodes = _DynamicUserSelections[key];
                //See if the value is in the dictionary.
                if (dimTreeNodes.ContainsKey(value))
                {
                    return dimTreeNodes[value];
                }
                else
                {
                    return default(X);
                }
            }
            else
            {
                return default(X);
            }
        }

        /// <summary>
        /// Cache an array of values.  This will replace any existing values with the new values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="values">Array of values, only caches the 0 element.</param>
        public override void CacheSelection(K key, V key2, X[] values)
        {
            if (values != null && values.Length > 0 && values[0] != null)
            {
                CacheSelection(key, key2, values[0]);
            }
        }

        /// <summary>
        /// Cache an array of values.  This will replace any existing values with the new values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="value">Array of values</param>
        public override void CacheSelection(K key, V key2, X value)
        {
            //See if we have the view in the dictionary.
            if (_DynamicUserSelections.ContainsKey(key))
            {
                //Get the value dictionary
                Dictionary<V, X> dimTreeNodes = _DynamicUserSelections[key];
                //See if the value is in the dictionary.
                if (dimTreeNodes.ContainsKey(key2))
                {
                    dimTreeNodes[key2] = value;
                }
                //Dimension is not in the dictionary, so add it.
                else
                {
                    //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                    dimTreeNodes.Add(key2, value);
                }
            }
            //No view so we have to create the view and value dictioanry.
            else
            {
                //Build the (dimensionName, nodeList) dictionary.
                Dictionary<V, X> dim = new Dictionary<V, X>();
                //Add the items to the dictionary.
                dim.Add(key2, value);
                //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                _DynamicUserSelections.Add(key, dim);
            }
        }

        /// <summary>
        /// Cache or add to the array of values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="values">Array of values, only caches the 0 element.</param>
        /// <param name="allowDuplicates">Not Applicable</param>
        public override void CacheAddSelections(K key, V key2, X[] values, bool allowDuplicates)
        {
            if (values != null && values.Length > 0 && values[0] != null)
            {
                CacheAddSelections(key, key2, values[0], true);
            }
        }

        /// <summary>
        /// Cache or add to the array of values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        /// <param name="value">Array of values</param>
        /// <param name="allowDuplicates">Not Applicable</param>
        public override void CacheAddSelections(K key, V key2, X value, bool allowDuplicates)
        {
            //See if we have the view in the dictionary.
            if (_DynamicUserSelections.ContainsKey(key))
            {
                //Get the value dictionary
                Dictionary<V, X> dimTreeNodes = _DynamicUserSelections[key];
                //See if the value is in the dictionary.
                if (dimTreeNodes.ContainsKey(key2))
                {
                    dimTreeNodes[key2] = value;
                }
                //Dimension is not in the dictionary, so add it.
                else
                {
                    //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                    dimTreeNodes.Add(key2, value);
                }
            }
            //No view so we have to create the view and value dictioanry.
            else
            {
                //Build the (dimensionName, nodeList) dictionary.
                Dictionary<V, X> dim = new Dictionary<V, X>();
                //Add the items to the dictionary.
                dim.Add(key2, value);
                //Add the (key, (dimensionName, nodeList) dictionary) to the dictionary.
                _DynamicUserSelections.Add(key, dim);
            }
        }

        /// <summary>
        /// Remove the key/value if it exists.
        /// </summary>
        /// <param name="key"></param>
        public override void RemoveSelection(K key)
        {
            //See if we have the view in the dictionary.
            if (_DynamicUserSelections.ContainsKey(key))
            {
                _DynamicUserSelections.Remove(key);
            }
        }

        /// <summary>
        /// Remove the key/value if it exists.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="key2">Key2</param>
        public override void RemoveSelection(K key, V key2)
        {
            //See if we have the view in the dictionary.
            if (ContainsKey2(key, key2))
            {
                //Get the value dictionary
                Dictionary<V, X> dimTreeNodes = _DynamicUserSelections[key];
                dimTreeNodes.Clear();
            }
        }

        /// <summary>
        /// Gets the key collection
        /// </summary>
        public Dictionary<K, Dictionary<V, X>>.KeyCollection Keys
        {
            get
            {
                return _DynamicUserSelections.Keys;
            }
        }

        /// <summary>
        /// Gets the value collection
        /// </summary>
        /// <param name="key">Key.</param>
        public Dictionary<V, X> Values(K key)
        {
            if (_DynamicUserSelections.ContainsKey(key))
            {
                return _DynamicUserSelections[key];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Clear out the selections.
        /// </summary>
        public override void Clear()
        {
            if (_DynamicUserSelections != null)
            {
                _DynamicUserSelections.Clear();
            }
        }

        /// <summary>
        /// Number of values in the first key.
        /// </summary>
        public override int Count()
        {
            if (_DynamicUserSelections != null)
            {
                return _DynamicUserSelections.Count;
            }
            else
            {
                return 0;
            }
        }

        /// <summary>
        /// Contains the key
        /// </summary>
        /// <param name="key">Key to search for.</param>
        /// <returns>true if the key if found, false if not.</returns>
        public override bool ContainsKey(K key)
        {
            if (_DynamicUserSelections.ContainsKey(key))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Contains the key2
        /// </summary>
        /// <param name="key">Main key.</param>
        /// <param name="key2">Key to search for.</param>
        /// <returns>true if the key if found, false if not.</returns>
        public override bool ContainsKey2(K key, V key2)
        {
            //See if we have the view in the dictionary.
            if (_DynamicUserSelections.ContainsKey(key))
            {
                //Get the value dictionary
                Dictionary<V, X> dimTreeNodes = _DynamicUserSelections[key];
                //See if the value is in the dictionary.
                if (dimTreeNodes.ContainsKey(key2))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            //Dimension is not in the dictionary, returtn false;
            return false;
        }
    }
}