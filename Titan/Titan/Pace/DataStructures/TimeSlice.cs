﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using Titan.Pace.Application.Extensions;
using Titan.Palladium;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.DataStructures
{
    internal class TimeSlice : IEquatable<TimeSlice>
    {
        public string Period { get; set; }
        public string Year { get; set; }


        public TimeSlice(Intersection cellIs, string timeDim, string yearDim)
        {
            string period = cellIs.GetCoordinate(timeDim), year = cellIs.GetCoordinate(yearDim);

            Period = period;
            Year = year;
        }

        public TimeSlice(string period, string year)
        {
            Period = period;
            Year = year;
        }

        public TimeSlice(string timeHorizonCoord)
        {
            // Look for period/year delimiter. Throw an error if the delimiter is not found
            // or if it is the last character.
            int pos = TimeSliceExtension.FindTimeHorizonDelim(timeHorizonCoord);

            // Split time horizon coordinate into period and year
            Period = timeHorizonCoord.Substring(pos + PafAppConstants.TIME_HORIZON_MBR_DELIM_LEN);
            Year = timeHorizonCoord.Substring(0, pos);

        }

        /// <summary>
        /// Return the time horizon period coordinate
        /// </summary>
        /// <returns></returns>
        public string GetTimeHorizonPeriod()
        {
            return TimeSliceExtension.BuildTimeHorizonCoord(Period, Year);
        }

        /// <summary>
        /// Compares the Year of one TimeSlice to another.
        /// </summary>
        /// <param name="other">TimeSlice</param>
        /// <returns>True if the year are equal, false if not.</returns>
        public bool YearEqual(TimeSlice other)
        {
            return Year.Equals(other.Year);
        }

        /// <summary>
        /// Compares the Period of one TimeSlice to another.
        /// </summary>
        /// <param name="other">TimeSlice</param>
        /// <returns>True if the Period are equal, false if not.</returns>
        public bool PeriodEqual(TimeSlice other)
        {
            return Period.Equals(other.Period);
        }


        /// <summary>
        /// Return the hashcode of the TimeSlice object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        [DebuggerHidden]
        public override int GetHashCode()
        {
            const int prime = 31;

            int result = 1;
            result = prime * result + ((Period == null) ? 0 : Period.GetHashCode());
            result = prime * result + ((Year == null) ? 0 : Year.GetHashCode());
            return result;
        }

        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is TimeSlice)) return false;

            TimeSlice temp = (TimeSlice)obj;

            return Equals(temp);
        }

        /// <summary>
        /// Compares an TimeSlice to another.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public bool Equals(TimeSlice obj)
        {
            if (obj == null)
                return false;
            TimeSlice other = obj;
            if (Period == null)
            {
                if (other.Period != null)
                {
                    return false;
                }
            }
            else if (!Period.Equals(other.Period))
            {
                return false;
            }
            if (Year == null)
            {
                if (other.Year != null)
                {
                    return false;
                }
            }
            else if (!Year.Equals(other.Year))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Converts the object to a string.
        /// </summary>
        /// <returns></returns>
        [DebuggerHidden]
        public override string ToString()
        {
            return GetTimeHorizonPeriod();
        } 
    }
}