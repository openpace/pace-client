﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Titan.Pace.DataStructures
{
    [Serializable]
    [XmlRoot("MultiKeyDictionary")]
    public class MultiKeyDictionary<T1, T2, TX>
    {
        [XmlIgnore]
        private Dictionary<Tuple<T1, T2>, List<TX>> _dictionary;

        public MultiKeyDictionary()
        {
            _dictionary = new Dictionary<Tuple<T1, T2>, List<TX>>();
        }

        public MultiKeyDictionary(Tuple<T1, T2> key, List<TX> values)
            : this()
        {
            _dictionary.Add(key, values);
        }

        [XmlArray("Tuples")]
        [XmlArrayItem(ElementName = "Tuple")]
        public List<TripleTupleList<T1, T2, TX>> Dictionary
        {
            get { return TripleTupleList<T1, T2, TX>.ConvertToSerialStructure(_dictionary); }
            set { _dictionary = TripleTupleList<T1, T2, TX>.ConvertFromSerialStructure(value); }
        }

        /// <summary>
        /// Clear out the selections.
        /// </summary>
        public void Clear()
        {
            if (_dictionary != null)
            {
                _dictionary.Clear();
            }
        }


        /// <summary>
        /// Cache or add to the array of values.
        /// </summary>
        /// <param name="key">Key</param>
        /// <param name="values">Array of values</param>
        /// <param name="allowDuplicates">Allow duplicate value to be entered into the list.</param>
        public void AddSelections(Tuple<T1, T2> key, IEnumerable<TX> values, bool allowDuplicates = false)
        {
            if (values == null) return;

            //See if we have the view in the dictionary.
            if (_dictionary.ContainsKey(key))
            {
                //Get the value dictionary
                List<TX> dimTreeNodes = _dictionary[key];
                //See if the value is in the dictionary.
                if (dimTreeNodes != null)
                {
                    List<TX> userSel = dimTreeNodes;
                    if (allowDuplicates)
                    {
                        userSel.AddRange(values);
                    }
                    else
                    {
                        foreach (TX v in values)
                        {
                            int pos = userSel.IndexOf(v);
                            if (pos > -1)
                            {
                                userSel.RemoveAt(pos);
                                userSel.Insert(pos, v);
                            }
                            else
                            {
                                userSel.Add(v);
                            }
                        }
                    }
                    _dictionary[key] = userSel;
                }
                //Dimension is not in the dictionary, so add it.
                else
                {
                    _dictionary[key] = new List<TX>(values);
                }
            }
            //No view so we have to create the view and value dictioanry.
            else
            {
                _dictionary.Add(key, new List<TX>(values));
            }
        }

        /// <summary>
        /// Get a list of X values
        /// </summary>
        /// <param name="key">key</param>
        /// <returns>A list x values, or null if no selections have been made.</returns>
        public List<TX> GetValues(Tuple<T1, T2> key)
        {
            return _dictionary.ContainsKey(key) ? _dictionary[key] : null;
        }

        /// <summary>
        /// Remove the value if it exists.
        /// </summary>
        /// <param name="key">Key</param>
        public void RemoveSelection(Tuple<T1, T2> key)
        {
            //See if we have the view in the dictionary.
            if (_dictionary.ContainsKey(key))
            {
                //Get the value dictionary
                List<TX> valueList = _dictionary[key];
                if (valueList != null)
                {
                    valueList.Clear();
                }
            }
        }
    }
}
