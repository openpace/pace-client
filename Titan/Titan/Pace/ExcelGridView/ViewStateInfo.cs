#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Titan.Pace.Application.Core;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.Rules;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView
{
    internal class ViewStateInfo
    {
        private readonly Hashtable _grids = new Hashtable();

        private Dictionary<int, ProtectionMngr> _protectionMngrs;

        private Dictionary<int, OlapView> _olapViews;

        private bool _plannable;

        private Dictionary<Intersection, simpleCellNote> _cellNotes = null;

        private CacheSelectionsList<string, string, Intersection> _mbrTagInterToFormat;

        public event ViewStateInfo_ViewPlannableStatusChange ViewPlannableStatusChanged;

        private LockMngr _globalLockMngr;

        public delegate void ViewStateInfo_ViewPlannableStatusChange(object sender, ViewPlannableStatusChangeEventArgs viewPlannableStatusChangeEventArgs);

        /// <summary>
        /// Create a new ViewStateInfo.
        /// </summary>
        /// <param name="hashCode">Hash code of the view.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="interfaceImplementer">Grid associated with the view.</param>
        /// <param name="defaultFormat">The default format used to fill in blank values.</param>
        /// <param name="lockMngr">The GlobalLockMngr to add to the protection manager.</param>
        public ViewStateInfo(int hashCode, string viewName, IGridInterface interfaceImplementer, Format defaultFormat, LockMngr lockMngr)
        {
            HashCode = hashCode;
            ViewName = viewName;
            ReadOnly = false;
            _grids.Add(0, interfaceImplementer);

            _protectionMngrs = new Dictionary<int, ProtectionMngr>();
            _protectionMngrs.Add(0, new ProtectionMngr());
            _globalLockMngr = lockMngr;
            SetGlobalLockMng(_globalLockMngr);

            _olapViews = new Dictionary<int, OlapView>();
            _olapViews.Add(0, new OlapView());

            DefaultFormats = new Dictionary<int, Format>();
            DefaultFormat = defaultFormat;

            if (ViewNode == null)
            {
                ViewNode = new object();
            }
        }

        /// <summary>
        /// Gets/Sets the view print style.
        /// </summary>
        public IPrintStyle PrintStyle { get; set; }

        /// <summary>
        /// Global lock manager (for session locks)
        /// </summary>
        //public GlobalLockMngr GlobalLockMngr { get; private set; }

        /// <summary>
        /// Get/set the hash code of the view.
        /// </summary>
        public int HashCode { get; set; }

        /// <summary>
        /// Get/set the name of the view.
        /// </summary>
        public string ViewName { get; set; }

        /// <summary>
        /// Get/set the view node.
        /// </summary>
        public object ViewNode { get; set; }

        /// <summary>
        /// True if the view has attributes, false if not.
        /// </summary>
        public bool HasAttributes { get; set; }

        /// <summary>
        /// Gets/sets weather or not the view is stale.  If a view is stale, then it cannot be planned
        /// or updated.
        /// </summary>
        public bool Plannable
        {
            get { return _plannable; }
            set
            {
                _plannable = value;

                if (ViewPlannableStatusChanged != null)
                {
                    if (!value)
                    {
                        ViewPlannableStatusChanged(this, new ViewPlannableStatusChangeEventArgs(HashCode, GetProtectionMngr(), false));
                    }
                    else 
                    {
                        ViewPlannableStatusChanged(this, new ViewPlannableStatusChangeEventArgs(HashCode, GetProtectionMngr(), true));
                    }
                }
            }
        }

        /// <summary>
        /// Gets/sets weather or not the view is dirty.  If a view is dirty, then it must be updated
        /// before it can be planned.
        /// </summary>
        public bool Dirty { get; set; }

        /// <summary>
        /// Gets/sets wether or not the view member Tag formatting is dirty.  If the view formatting is dirty, 
        /// then is must be updated.
        /// </summary>
        public bool DirtyMbrTagFormatting { get; set; }

        /// <summary>
        /// Gets/sets wether or not the view member Tags are dirty.  If the view is dirty, 
        /// then the member tag data must be repainted.
        /// </summary>
        public bool DirtyMbrTags { get; set; }

        /// <summary>
        /// Gets/sets wether or not the view is read only.
        /// </summary>
        public bool ReadOnly { get; set; }

        /// <summary>
        /// Gets/sets wether or not the view has been built.  Returns true if the view has been built, and
        /// the user is refreshing the view.
        /// </summary>
        public bool ViewHasBeenBuilt { get; set; }

        /// <summary>
        /// Gets the default formats for the view.  The key is the hash of the CellAddress.
        /// </summary>
        public Dictionary<int, Format> DefaultFormats { get; set; }

        /// <summary>
        /// Gets the default format used by the view to fill in blank values.
        /// </summary>
        public Format DefaultFormat { get; set; }

        /// <summary>
        /// Clear out all the protection managers.
        /// </summary>
        public void ClearProtection()
        {
            if(_protectionMngrs != null)
            {
                foreach(KeyValuePair<int, ProtectionMngr> kvp in _protectionMngrs)
                {
                    kvp.Value.ClearProtection();
                }
            }
            //////using private var, because this is internal so don't fire the event.
            ////_Plannable = false;
            ////_Dirty = false;  
        }

        /// <summary>
        /// Sets the global lock mngr in the protection manager.
        /// </summary>
        /// <param name="lockMngr"></param>
        [DebuggerHidden]
        public void SetGlobalLockMng(LockMngr lockMngr)
        {
            if (_protectionMngrs.Count > 0)
            {
                _protectionMngrs[0].GlobalLocks = lockMngr;
            }
        }

        /// <summary>
        /// Get the protection manager.
        /// </summary>
        /// <returns>The protection manager associated with this viewstate.</returns>
        /// <remarks>Currently we only support one protectionmanger per view, so this just 
        /// returns the top item in the dictionary.</remarks>
        [DebuggerHidden]
        public ProtectionMngr GetProtectionMngr()
        {
            if (_protectionMngrs.Count > 0)
            {
                //_protectionMngrs[0].GlobalLocks = GlobalLockMngr;
                return _protectionMngrs[0];
            }
            else
            {
                return null;
            }
        }
         
        /// <summary>
        /// Resets (creates) a new protection manager.  
        /// </summary>
        [DebuggerHidden]
        public void ResetProtectionMngr()
        {
            _protectionMngrs = new Dictionary<int, ProtectionMngr>();
            ProtectionMngr temp = new ProtectionMngr {GlobalLocks = _globalLockMngr};
            _protectionMngrs.Add(0, temp);
        }

        /// <summary>
        /// Gets the Olap view associated to the view.
        /// </summary>
        /// <returns>An olap view.</returns>
        /// <remarks>Currently we only support one olap view per view, so this just 
        /// returns the top item in the dictionary.</remarks>
        [DebuggerHidden]
        public OlapView GetOlapView()
        {
            if (_olapViews.Count > 0)
            {
                //The dictionary only holds a single OlapView object with a key of 0
                return _olapViews[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Resets (creates) a new olap view.  
        /// </summary>
        /// pmack
        public void ResetOlapView()
        {
            //Seed the new OlapView object with properties from the current OlapView object
            OlapView olapView = new OlapView
            {
                ServerSortOverridden = GetOlapView().ServerSortOverridden,
                ServerSortSpecified = GetOlapView().ServerSortSpecified,
                IsSorted = GetOlapView().IsSorted,
                CurrentSortOrder = GetOlapView().CurrentSortOrder,
                SortRange = GetOlapView().SortRange,
                TopLeftWindowAnchor = GetOlapView().TopLeftWindowAnchor,
                SortSelections = GetOlapView().SortSelections
            };

            //Create a new dictionary to hold the new OlapView object with a key of 0
            _olapViews = new Dictionary<int, OlapView> {{0, olapView}};
        }

        /// <summary>
        /// Get the grid interface associated to this view.
        /// </summary>
        /// <returns>A GridInterface.</returns>
        /// <remarks>Currently we only support one grid per view, so this just 
        /// returns the top item in the dictionary.</remarks>
        [DebuggerHidden]
        public IGridInterface GetGrid()
        {
            if (_grids.Count > 0)
            {
                return (IGridInterface)_grids[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Sets the cell notes to be rendered to the view.
        /// </summary>
        /// <param name="cellNotes">Cell notes.</param>
        public void SetCellNotes(Dictionary<simpleCoordList, simpleCellNote> cellNotes)
        {
            if (_cellNotes == null)
            {
                _cellNotes = new Dictionary<Intersection, simpleCellNote>();
            }
            else
            {
                _cellNotes.Clear();
            }
            foreach (KeyValuePair<simpleCoordList, simpleCellNote> kvp in cellNotes)
            {
                Intersection intersection = new Intersection(kvp.Key.axis, kvp.Key.coordinates);
                _cellNotes.Add(intersection, kvp.Value);
            }
        }

        /// <summary>
        /// Sets the cell notes to be rendered to the view.
        /// </summary>
        /// <param name="cellNotes">Cell notes.</param>
        public void SetCellNotes(simpleCellNote[] cellNotes)
        {
            if (_cellNotes == null)
            {
                _cellNotes = new Dictionary<Intersection, simpleCellNote>();
            }
            else
            {
                _cellNotes.Clear();
            }
            if (cellNotes != null && cellNotes.Length > 0)
            {
                foreach (simpleCellNote scn in cellNotes)
                {
                    if (scn != null)
                    {
                        Intersection intersection =
                            new Intersection(scn.simpleCoordList.axis, scn.simpleCoordList.coordinates);
                        _cellNotes.Add(intersection, scn);
                    }
                }
            }
        }

        /// <summary>
        /// Cell notes to be rendered to the view.
        /// </summary>
        public Dictionary<Intersection, simpleCellNote> CellNotes
        {

            get
            {
                if (_cellNotes == null)
                {
                    _cellNotes = new Dictionary<Intersection, simpleCellNote>();
                }
                return _cellNotes;
            }
            set { _cellNotes = value; }
        }

        /// <summary>
        /// Gets the cell notes to be renderd to the view as an array.
        /// </summary>
        public simpleCellNote[] CellNotesAsArray
        {

            get
            {
                simpleCellNote[] arr = null;
                if (_cellNotes != null && _cellNotes.Count > 0)
                {
                    arr = new simpleCellNote[_cellNotes.Count];
                    _cellNotes.Values.CopyTo(arr, 0);
                    return arr;
                }
                else
                {
                    return arr;
                }
            }
        }

        ///// <summary>
        ///// List of member tag intersections intersections that need to be returned to the default
        ///// format.
        ///// </summary>
        //public Dictionary<Intersection, string> MbrTagInterToFormat
        //{
        //    get
        //    {
        //        if( _MbrTagInterToFormat == null)
        //        {
        //            _MbrTagInterToFormat = new Dictionary<Intersection, string>();
        //        }
        //        return _MbrTagInterToFormat;
        //    }
        //    set { _MbrTagInterToFormat = value; }
        //}

         /// <summary>
        /// List of member tag intersections intersections that need to be returned to the default
        /// format.
        /// </summary>
        public CacheSelectionsList<string, string, Intersection> MbrTagInterToFormat
        {
            get
            {
                if( _mbrTagInterToFormat == null)
                {
                    _mbrTagInterToFormat = new CacheSelectionsList<string, string, Intersection>();
                }
                return _mbrTagInterToFormat;
            }
            set { _mbrTagInterToFormat = value; }
        }
    }
    #region UserLockedCellEventArgs
    /// <summary>
    /// Custom event argument for when a user locks a cell.
    /// </summary>
    internal class ViewPlannableStatusChangeEventArgs : EventArgs
    {
        public ViewPlannableStatusChangeEventArgs(int hashCode, ProtectionMngr protMngr, bool newPlannableStatus)
        {
            HashCode = hashCode;
            ProtectionMngr = protMngr;
            NewPlannableStatus = newPlannableStatus;
        }

        public int HashCode;
        public ProtectionMngr ProtectionMngr;
        public bool NewPlannableStatus;
    }
    #endregion UserLockedCellEventArgs
}
