#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Core;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Application.Forms;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;
using Border = Titan.Pace.ExcelGridView.Utility.Border;
using ColorScale = Microsoft.Office.Interop.Excel.ColorScale;
using Range = Titan.Pace.ExcelGridView.Utility.Range;
using TextBox = Microsoft.Office.Interop.Excel.TextBox;
using XlBorderWeight=Microsoft.Office.Interop.Excel.XlBorderWeight;
using XlColorIndex=Microsoft.Office.Interop.Excel.XlColorIndex;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// Summary description for Excel.
    /// </summary>
    internal class ExcelGridImpl : IGridInterface
    {
        private readonly Worksheet _sheet;
        private readonly ColorUtilities _cu;

        //you need to do a cast these constants like this...otherwise excel 
        //will mistakenly think that left border is actually top border, etc
        //this is due to the differences in .net and com (hence the cast)
        //FIX TTN-972, for Excel 12.
        /// <summary>
        /// Excel top cell border index constant.
        /// </summary>
        private const XlBordersIndex TOP_BDR_IDX = (XlBordersIndex)Constants.xlTop;
        /// <summary>
        /// Excel bottom cell border index constant.
        /// </summary>
        private const XlBordersIndex BOTTOM_BDR_IDX = (XlBordersIndex)Constants.xlBottom;
        /// <summary>
        /// Excel left cell border index constant.
        /// </summary>
        private const XlBordersIndex LEFT_BDR_IDX = (XlBordersIndex)Constants.xlLeft;
        /// <summary>
        /// Excel right cell border index constant.
        /// </summary>
        private const XlBordersIndex RIGHT_BDR_IDX = (XlBordersIndex)Constants.xlRight;
        /// <summary>
        /// Private member to hold Excel version.
        /// </summary>
        private float _version;

        /// <summary>
        /// Current version of Excel.
        /// </summary>
        public float Version
        {
            get 
            {
                if (_version <= 0)
                {
                    _version = float.Parse(_sheet.Application.Version);
                }
                return _version;
            }
        }

        /// <summary>
        /// Returns the name of the Excel worksheet.
        /// </summary>
        public string Name
        {
            get { return _sheet.Name; }
        }

        /// <summary>
        /// Returns the name of the Excel.Worksheet.
        /// </summary>
        /// <returns>The name of the worksheet.</returns>
        public override string ToString()
        {
            return Name;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sht">The Excel.Worksheet to perform all the actions.</param>
        public ExcelGridImpl(Worksheet sht)
        {
            _sheet = sht;
            _cu = new ColorUtilities();
        }

        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="textArray">Values to update the cells with.</param>
        public void SetValueDoubleArray(ContiguousRange contigRng, double[,] textArray)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(contigRng, _sheet);

            rng.Value2 = textArray;
        }

        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="objectArray">Values to update the cells with.</param>
        public void SetValueObject(ContiguousRange contigRng, Object objectArray)
        {
            Microsoft.Office.Interop.Excel.Range Rng = null;
            Rng = GetRange(contigRng, _sheet);
            Rng.Value2 = objectArray;
        }

        /// <summary>
        /// Update an array of formula cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="objectFormulaArray">Formuals to update the cells with.</param>
        public void SetFormulaArray(ContiguousRange contigRng, Object objectFormulaArray)
        {
            Microsoft.Office.Interop.Excel.Range Rng = null;
            Rng = GetRange(contigRng, _sheet);
            Rng.FormulaArray = objectFormulaArray;
        }

        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="ranges">The range of cells to update.</param>
        /// <param name="objectVal">Values to update the cells with.</param>
        public void SetValueObject(List<Range> ranges, Object objectVal)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);
            rng.Value2 = objectVal;
        }

        /// <summary>
        /// Gets the values of a range of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to retrieve.</param>
        /// <returns>An string array containing the values from the grid.</returns>
        public object[,] GetValueObjectArray(ContiguousRange contigRng)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(contigRng, _sheet);
            object[,] array = (object[,])rng.Cells.Value2;
            return array;
        }

        /// <summary>
        /// Set the string value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <param name="text">String to place into the cell.</param>
        public void SetValueText(int row, int col, string text)
        {
            Microsoft.Office.Interop.Excel.Range rng;
            rng = (Microsoft.Office.Interop.Excel.Range)_sheet.Cells[row, col];

            rng.Value2 = text;
        }

        /// <summary>
        /// Gets the formula value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <returns>The formula of the cell as a string.</returns>
        public object GetValueFormula(int row, int col)
        {
            //Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
            Microsoft.Office.Interop.Excel.Range rng = GetRange(row, col, _sheet);
            return rng.Formula;
        }


        /// <summary>
        /// Set the value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <param name="value">Double to place into the cell.</param>
        public void SetValueDouble(int row, int col, double value)
        {
            //Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
            Microsoft.Office.Interop.Excel.Range rng = GetRange(row, col, _sheet);
            rng.Value2 =  value;
        }

        /// <summary>
        /// Gets the value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <returns>The value of the cell.</returns>
        public double GetValueDouble(int row, int col)
        {
            //Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
            Microsoft.Office.Interop.Excel.Range rng = GetRange(row, col, _sheet);
            return double.Parse(rng.Value2.ToString());
        }

        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="textArray">Values to update the cells with.</param>
        public void SetValueTextArray(ContiguousRange contigRng, string[,] textArray)
        {
            int columnLimit;
            int rowLimit;


            if (Version >= 12)
            {
                columnLimit = PafAppConstants.EXCEL12_COLUMN_LIMIT;
                rowLimit = PafAppConstants.EXCEL12_ROW_LIMIT;
            }
            else
            {
                columnLimit = PafAppConstants.EXCEL11_COLUMN_LIMIT;
                rowLimit = PafAppConstants.EXCEL11_ROW_LIMIT;
            }


            if(contigRng.BottomRight.Col > columnLimit)
            {
                throw new Exception(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.RangeExceedsColumnsLimits"));
            }
            if (contigRng.BottomRight.Row > rowLimit)
            {
                throw new Exception(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.RangeExceedsRowLimits"));
            }

            Microsoft.Office.Interop.Excel.Range rng = GetRange(contigRng, _sheet);

            rng.Value2 = textArray;
        }

        /// <summary>
        /// Update an list of ranges with a single text value.
        /// </summary>
        /// <param name="ranges">The range of cells to update.</param>
        /// <param name="value">Values to update the cells with.</param>
        public void SetValueTextRange(List<Range> ranges, string value)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);
            rng.Value2 = value;
        }

        /// <summary>
        /// Clears out the value of a list of ranges and locks the cells.
        /// </summary>
        /// <param name="ranges">List of ranges to clear.</param>
        /// <param name="lockRange">Lock the range from being updated.</param>
        public void SetBlankFormats(List<Range> ranges, bool lockRange)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            rng.Value2 = "";
            rng.Locked = lockRange;
            //FormatBorders(rng, 0);
        }


        /// <summary>
        /// Set a list of ranges as protected (changes interior color, and locks)
        /// </summary>
        /// <param name="ranges">List of ranges to set as protected.</param>
        /// <param name="color">Name of color that represents a proteced cell.</param>
        public void SetProtectedFormats(List<Range> ranges, Color color)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            rng.Interior.Color = ColorTranslator.ToWin32(color);
            rng.Locked = true;
        }

        public void SetProtectedFormats(Range range, Color color)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

            rng.Interior.Color = ColorTranslator.ToWin32(color);
            rng.Locked = true;
        }

        /// <summary>
        /// Sets the horizontal alignment of a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set alignment.</param>
        /// <param name="alignment">Alignment value name.</param>
        public void SetHorizontalAlignment(List<Range> ranges, string alignment)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting HorizontalAlignment");
            switch (alignment.ToLower())
            {
                case "left":
                    rng.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                    break;
                case "center":
                    rng.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    break;
                case "right":
                    rng.Cells.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
                    break;
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting completed");
        }

        ///// <summary>
        ///// Gets the horizontal alignment for a specific cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>Alignment value name.</returns>
        //public string GetHorizontalAlignment(int row, int col)
        //{
        //    string alignment = "";
        //    Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
        //    Microsoft.Office.Interop.Excel.XlHAlign currentStyle =
        //        (Microsoft.Office.Interop.Excel.XlHAlign)rng.Cells.HorizontalAlignment;

        //    switch (currentStyle)
        //    {
        //        case Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft:
        //            alignment = "Left";
        //            break;
        //        case Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter:
        //            alignment = "Center";
        //            break;
        //        case Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight:
        //            alignment = "Right";
        //            break;
        //        default:
        //            alignment = "";
        //            break;      
        //    }

        //    return alignment;
        //}

        /// <summary>
        /// Set the BgFillColor(interior.color) of a range of cell.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <param name="c">Name of the color.</param>
        public void SetBgFillColor(List<Range> ranges, string c)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting Interior.Color");
            //rng.Interior.Color = ColorTranslator.ToWin32(ColorTranslator.FromHtml(c));
            rng.Interior.Color = _cu.GetWin32Color(c);
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting completed");

        }

        /// <summary>
        /// Set the BgFillColor(interior.color) of a range of cell.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <param name="c">The Color color.</param>
        public void SetBgFillColor(List<Range> ranges, Color c)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);
            rng.Interior.Color = ColorTranslator.ToWin32(c);
        }


        /// <summary>
        /// Set the BgFillColor(interior.color) of a range of cell.
        /// </summary>
        /// <param name="range">The range.</param>
        /// <param name="c">The Color color.</param>
        public void SetBgFillColor(Range range, Color c)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);
            rng.Interior.Color = ColorTranslator.ToWin32(c);

        }

        ///// <summary>
        ///// Set the BgFillColor(interior.color) of a single cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <param name="c">Name of the color.</param>
        //public void SetBgFillColor(int row, int col, Color c)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[(int)row, (int)col];
        //    rng.Interior.Color = ColorTranslator.ToWin32(c);
        //}


        public void GetCellFormatInformation(int row, int col, ref string bgFillColor, ref string isUnlocked, ref string numericFormat)
        {
            //Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
            Microsoft.Office.Interop.Excel.Range rng = GetRange(row, col, _sheet);

            if(bgFillColor != null)
            {
                bgFillColor = _cu.GetHexStrFromWin32Color(int.Parse(rng.Interior.Color.ToString()));
            }

            if(isUnlocked != null && rng.Locked != null)
            {
                isUnlocked = rng.Locked.Equals(true) ? "False" : "True";
            }

            if(numericFormat != null)
            {
                numericFormat = rng.Cells.NumberFormat.ToString();
            }
 
        }

        ///// <summary>
        ///// Set the BgFillColor(interior.color) of a single cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>The name of the color.</returns>
        //public string GetBgFillColor(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
        //    return cu.GetHexStrFromWin32Color(int.Parse(rng.Interior.Color.ToString()));
        //    //return ColorTranslator.FromWin32((int.Parse(rng.Interior.Color.ToString()))).ToArgb().ToString();
            
        //}

        /// <summary>
        /// Set/clear the bold formatting of a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set/clear bold.</param>
        /// <param name="bold">String representation of boolean value.</param>
        public void SetBold(List<Range> ranges, string bold)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting Font.Bold");
            rng.Font.Bold = Boolean.Parse(bold);
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting completed");
        }

        ///// <summary>
        ///// Returns weather or not the cell is bold. 
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>String value representating the boolean value indicating weather or not the cell is bold.</returns>
        //public string IsBold(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng;
        //    rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];

        //    return rng.Cells.Font.Bold.ToString();  
        //}

        /// <summary>
        /// Sets the font color for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to format.</param>
        /// <param name="color">An hex color string.</param>
        public void SetFontColor(List<Range> ranges, string color)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting Font.Color");
            rng.Font.Color = _cu.GetWin32Color(color);
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting completed");
          
        }

        ///// <summary>
        ///// Gets the font color for a single cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>The name of the color.</returns>
        //public string GetFontColor(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng;
        //    rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
        //    return cu.GetHexStrFromWin32Color(int.Parse(rng.Font.Color.ToString()));
        //    //return ColorTranslator.FromWin32((int.Parse(rng.Font.Color.ToString()))).ToArgb().ToString();
        //}

        /// <summary>
        /// Sets the font style for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to to format.</param>
        /// <param name="fontName">Name of font.</param>
        public void SetFontName(List<Range> ranges, string fontName)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting FontStyle");
            rng.Font.FontStyle = fontName;
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting completed");
        }

        ///// <summary>
        ///// Gets the font name for a single cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>The name of the font.</returns>
        //public string GetFontName(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng;
        //    rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];

        //    return rng.Cells.Font.FontStyle.ToString();
        //}

        /// <summary>
        /// Sets the italic status of the list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges</param>
        /// <param name="italics">String representation of boolean value.</param>
        public void SetItalics(List<Range> ranges, string italics)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting Italic");
            rng.Font.Italic = Boolean.Parse(italics);
   
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting completed");
        }

        ///// <summary>
        ///// Gets the italic status of a single cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>"true" if italic, "false" if not italic.</returns>
        //public string IsItalicised(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng;
        //    rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];

        //    return rng.Cells.Font.Bold.ToString();
        //}

        /// <summary>
        /// Sets the italic status of the list of ranges.
        /// </summary>
        /// <param name="range">List of ranges</param>
        /// <param name="topMergedCell">The top cell in the merged cell, this is null if the range
        /// is not merged.</param>
        public bool IsMerged(Range range, out Cell topMergedCell)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

            topMergedCell = null;

            if((bool) rng.Cells.MergeCells)
            {
                topMergedCell = GetTopMergedCell(rng.Cells.MergeArea);
                return true;
            }
            else
            {
                return false;
            }
        }


        /// <summary>
        /// Sets the font size on a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set the font size.</param>
        /// <param name="fontSize">New size of the font.</param>
        public void SetFontSize(List<Range> ranges, double fontSize)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting Font.Size");
            rng.Font.Size = fontSize;
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Painting completed");
        }

        ///// <summary>
        ///// Gets the font size for a single cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>The font size.</returns>
        //public double GetFontSize(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng;
        //    rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];

        //    return (double) rng.Cells.Font.Size;
        //}

        /// <summary>
        /// Sets the strikeout value for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <param name="strikeout">Strikeout value.</param>
        public void SetStrikeout(List<Range> ranges, string strikeout)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            rng.Font.Strikethrough = Boolean.Parse(strikeout);
        }

        ///// <summary>
        ///// Gets the strikeout value for a single cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>The strikeout value.</returns>
        //public string isStrikedOut(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng;
        //    rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];

        //    return rng.Cells.Font.Strikethrough.ToString();
        //}

        /// <summary>
        /// Sets the underline on a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to update.</param>
        /// <param name="underline">String representation of the boolean value.</param>
        public void SetUnderline(List<Range> ranges, string underline)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            switch (Boolean.Parse(underline))
            {
                case true:
                    rng.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                    break;
                case false:
                    rng.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                    break;
            }
        }

        /// <summary>
        /// Sets the double underline on a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to update.</param>
        /// <param name="doubleUnderline">String representation of the boolean value.</param>
        public void SetDoubleUnderline(List<Range> ranges, string doubleUnderline)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            switch (Boolean.Parse(doubleUnderline))
            {
                case true:
                    rng.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                    break;
                case false:
                    rng.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                    break;
            }
        }

        ///// <summary>
        ///// Returns weather or not the cell is underlined.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>"True" if the cell is underlined, "False" if not.</returns>
        //public string isUnderlined(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
        //    Microsoft.Office.Interop.Excel.XlUnderlineStyle currentStyle =
        //        (Microsoft.Office.Interop.Excel.XlUnderlineStyle)rng.Cells.Font.Underline;

        //    if (currentStyle.Equals(Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleSingle))
        //    {
        //        return "True";
        //    }
        //    else
        //    {
        //        return "False";
        //    }
        //}

        /// <summary>
        /// Sets the border to a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to update.</param>
        /// <param name="borders">The size of the border.</param>
        public void SetBorders(List<Range> ranges, int borders)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            FormatBorders(rng, borders);
        }

        /// <summary>
        /// Sets the border to a list of ranges.
        /// </summary>
        /// <param name="range">The contiguous range to set the border.</param>
        /// <param name="borders">The size of the border.</param>
        public void SetBorders(ContiguousRange range, int borders)
        {
            if (range != null)
            {
                Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

                FormatBorders(rng, borders);
            }
        }

        ///// <summary>
        ///// Gets the locked status of a cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        ///// <returns>"True" if the cell is locked, "False" if the cell is unlocked.</returns>
        //public string isUnLocked(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
            
        //    if (rng.Locked.Equals(true))
        //    {
        //        return "False";
        //    }
        //    else
        //    {
        //        return "True";
        //    }
        //}

        ///// <summary>
        ///// Locks a cell.
        ///// </summary>
        ///// <param name="row">Row index.</param>
        ///// <param name="col">Column index.</param>
        //public void Lockcells(int row, int col)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[(int)row, (int)col];
        //    rng.Locked = true;
        //}

        /// <summary>
        /// Locks a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to lock.</param>
        public void Lockcells(List<Range> ranges)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);
            rng.Locked = true;
        }

        /// <summary>
        /// Locks a Contiguous Range.
        /// </summary>
        /// <param name="range">Contiguous Range to lock.</param>
        public void Lockcells(ContiguousRange range)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);
            rng.Locked = true;
        }

        ///// <summary>
        ///// Unlock a contiguous range of cells.
        ///// </summary>
        ///// <param name="r">Contiguous range of cells to unlock.</param>
        //public void UnLockCells(ContiguousRange r)
        //{
        //    Microsoft.Office.Interop.Excel.Range rng = sheet.get_Range(sheet.Cells[(int)r.TopLeft.Row, (int)r.TopLeft.Col], sheet.Cells[(int)r.BottomRight.Row, (int)r.BottomRight.Col]);
        //    rng.Cells.Locked = false;
        //}

        /// <summary>
        /// Unlocks a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to unlock.</param>
        public void UnLockCells(List<Range> ranges)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);
            rng.Locked = false;
        }

        /// <summary>
        /// Sets the numeric format for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to format.</param>
        /// <param name="format">Numeric format to apply.</param>
        public void SetNumericFormat(List<Range> ranges, string format)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(ranges, _sheet);

            rng.NumberFormat = format;
        }

        public object SetRowGrouping(ContiguousRange range, string name = null, int? level = null)
        {
            Microsoft.Office.Interop.Excel.Range rng = _sheet.Rows[string.Format("{0}:{1}", range.TopLeft.Row, range.BottomRight.Row), Type.Missing] as Microsoft.Office.Interop.Excel.Range;

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("{0}:{1}", range.TopLeft.Row, range.BottomRight.Row);

            return SetGrouping(rng, name, level);

        }

        public object SetColumnGrouping(ContiguousRange range, string name = null, int? level = null)
        {
            Microsoft.Office.Interop.Excel.Range rng = _sheet.Columns[string.Format("{0}:{1}", range.TopLeft.ColAsLetter, range.BottomRight.ColAsLetter), Type.Missing] as Microsoft.Office.Interop.Excel.Range;

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("{0}:{1}", range.TopLeft.ColAsLetter, range.BottomRight.ColAsLetter);

            return SetGrouping(rng, name, level);
        }

        public void SetOutlineSummaryLocation(ViewAxis viewAxis, bool parentFirst)
        {
            SetGroupingSummaryLocation(viewAxis, parentFirst);
        }

        public bool ExpandColumnGrouping(CellAddress cellAddress)
        {
            Microsoft.Office.Interop.Excel.Range rng = _sheet.Columns[string.Format("{0}:{1}", cellAddress.ColAsLetter, cellAddress.ColAsLetter), Type.Missing] as Microsoft.Office.Interop.Excel.Range;

            if (rng == null)
            {
                PafApp.GetLogger().Warn("Range is null or empty, cannot set row grouping");
                return false;
            }

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("{0}:{1}", cellAddress.ColAsLetter, cellAddress.ColAsLetter);

            try
            {
                rng.EntireColumn.ShowDetail = !rng.Columns.ShowDetail;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool ExpandRowGrouping(CellAddress cellAddress)
        {
            Microsoft.Office.Interop.Excel.Range rng = _sheet.Rows[string.Format("{0}:{1}", cellAddress.Row, cellAddress.Row), Type.Missing] as Microsoft.Office.Interop.Excel.Range;

            if (rng == null)
            {
                PafApp.GetLogger().Warn("Range is null or empty, cannot set row grouping");
                return false;
            }

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("{0}:{1}", cellAddress.ColAsLetter, cellAddress.ColAsLetter);

            try
            {
                rng.EntireRow.ShowDetail = !rng.EntireRow.ShowDetail;
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public void SetGroupingLevels(int? rowLevel, int? columnLevel)
        {
            if (_sheet == null) return;

            object row = Type.Missing;
            object col = Type.Missing;

            if (rowLevel.HasValue)
            {
                row = rowLevel.Value;
            }

            if (columnLevel.HasValue)
            {
                col = columnLevel.Value;
            }
            
            _sheet.Outline.ShowLevels(row, col);
            
        }


        public void SetConditionalFormatting(Range range, abstractPaceConditionalStyle criteria)
        {
            if (range == null || range.ContiguousRanges.Count == 0)
            {
                PafApp.GetLogger().ErrorFormat("Format range is null, format: {0} will not be applied", criteria.name);
                return;
            }

            if (range.ContiguousRanges.Count > PafAppConstants.EXCEL_MAX_NON_CONTIG_RANGE)
            {

                ResourceManager manager = PafApp.GetLocalization().GetResourceManager();

                string message = String.Format(manager.GetString("Application.Exception.ConfFormatNonContigRange"),
                    new object[] { criteria.name, range.ContiguousRanges.Count.ToString("N0")});

                HelpRequestor c = new HelpRequestor(message, 
                    MessageBoxButtons.OK, 
                    MessageBoxIcon.Warning,
                    manager.GetString("Application.HelpFile.BaseUrl"),
                    manager.GetString("Application.HelpFile.LargeContigRange"), 
                    true);

                c.ShowMessage();
                c.Close();
                

                return;
            }

            Microsoft.Office.Interop.Excel.Range r = GetRange(range, _sheet);

            try
            {
                switch (criteria.GetStyleType())
                {
                    case ConditionalStyleType.ColorScale:
                        SetConditionalFormattingColorScale(r, criteria.ToColorScale());
                        break;
                    case ConditionalStyleType.IconSet:
                        SetConditionalFormattingIcon(r, criteria.ToIconStyle());
                        break;
                    case ConditionalStyleType.DataBar:
                        SetConditionalFormattingDataBars(r, criteria.ToDataBars());
                        break;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                PafApp.GetLogger().InfoFormat("Range cell count: {0}", r.Cells.CountLarge.ToString("N0"));
                PafApp.GetLogger().InfoFormat("Non Contiguous Range count: {0}", r.Areas.Count.ToString("N0"));
                PafApp.GetLogger().InfoFormat("Range Address: {0}", r.get_AddressLocal(false, false));
                int strLen = r.Areas.Cast<Microsoft.Office.Interop.Excel.Range>().Sum(rg => rg.get_AddressLocal(false, false).Length);
                PafApp.GetLogger().InfoFormat("Address length: {0}", strLen.ToString("N0"));
            }
        }


        private void SetConditionalFormattingIcon(Microsoft.Office.Interop.Excel.Range range, iconStyle style)
        {
            if (style == null)
            {
                PafApp.GetLogger().Error("Format range or conditional style is null.");
                return;
            }

            IconSetCondition condition = (IconSetCondition) range.FormatConditions.AddIconSetCondition();
            condition.SetFirstPriority();

            condition.ReverseOrder = false;
            condition.ShowIconOnly = false;

            var iconSets = _sheet.Application.ActiveWorkbook.IconSets;
            switch (style.iconStyle1)
            {
                case iconStyleType.ThreeColorArrows:
                    condition.IconSet = iconSets[XlIconSet.xl3Arrows];
                    break;
                case iconStyleType.FourColorArrows:
                    condition.IconSet = iconSets[XlIconSet.xl4Arrows];
                    break;
                case iconStyleType.FiveColorArrows:
                    condition.IconSet = iconSets[XlIconSet.xl5Arrows];
                    break;
                case iconStyleType.ThreeColorStoplight:
                    condition.IconSet = iconSets[XlIconSet.xl3TrafficLights1];
                    break;
                case iconStyleType.FourColorStoplight:
                    condition.IconSet = iconSets[XlIconSet.xl4TrafficLights];
                    break;
                case iconStyleType.IndicatorCheckBang:
                    condition.IconSet = iconSets[XlIconSet.xl3Symbols2];
                    break;
                case iconStyleType.IndicatorCheckBangCircle:
                    condition.IconSet = iconSets[XlIconSet.xl3Symbols];
                    break;
                case iconStyleType.IndicatorThreeFlags:
                    condition.IconSet = iconSets[XlIconSet.xl3Flags];
                    break;
                default:
                    condition.IconSet = iconSets[0];
                    break;
            }

            int maxIndex = 2;
            if (style.criteriaTwo != null)
            {
                maxIndex = 3;
            }

            if (style.criteriaThree != null)
            {
                maxIndex = 4;
            }

            if (style.criteriaFour != null)
            {
                maxIndex = 5;
            }

            if (style.criteriaFive != null)
            {
                maxIndex = 6;
            }



            if (style.criteriaOne != null)
            {
                SetIconCriteria(condition, style.criteriaOne, maxIndex);
                maxIndex--;
            }

            if (style.criteriaTwo != null)
            {
                SetIconCriteria(condition, style.criteriaTwo, maxIndex);
                maxIndex--;
            }

            if (style.criteriaThree != null)
            {
                SetIconCriteria(condition, style.criteriaThree, maxIndex);
                maxIndex--;
            }

            if (style.criteriaFour != null)
            {
                SetIconCriteria(condition, style.criteriaFour, maxIndex);
                maxIndex--;
            }

            if (style.criteriaFive != null)
            {
                SetIconCriteria(condition, style.criteriaFive, maxIndex);
            }
        }

        private void SetIconCriteria(IconSetCondition condition, iconCriteria criteria, int index)
        {
            IconCriterion iconCriterion = condition.IconCriteria.get_Item(index);
            switch (criteria.criteriaType)
            {
                case criteriaType.Highest:
                    iconCriterion.Type = XlConditionValueTypes.xlConditionValueHighestValue;
                    break;
                case criteriaType.Lowest:
                    iconCriterion.Type = XlConditionValueTypes.xlConditionValueLowestValue;
                    break;
                case criteriaType.Number:
                    iconCriterion.Type = XlConditionValueTypes.xlConditionValueNumber;
                    break;
                case criteriaType.Percent:
                    iconCriterion.Type = XlConditionValueTypes.xlConditionValuePercent;
                    break;
                case criteriaType.Percentile:
                    iconCriterion.Type = XlConditionValueTypes.xlConditionValuePercentile;
                    break;
            }
            iconCriterion.Value = criteria.criteriaValue;

            switch (criteria.criteriaOperator)
            {
                case ">=":
                    //7
                    iconCriterion.Operator = Convert.ToInt32(XlFormatConditionOperator.xlGreaterEqual);
                    break;
                case ">":
                    //5
                    iconCriterion.Operator = Convert.ToInt32(XlFormatConditionOperator.xlGreater);
                    break;
            }

        }

        private void SetConditionalFormattingDataBars(Microsoft.Office.Interop.Excel.Range range, dataBars style)
        {

            if (style == null)
            {
                PafApp.GetLogger().Error("Format range or conditional style is null.");
                return;
            }

            Databar condition = (Databar) range.FormatConditions.AddDatabar();
            condition.ShowValue = true;
            condition.SetFirstPriority();

            //Build the min/max condition values
            SetDataBarCriteriaConditionValue(condition.MinPoint, style.minimum, false);
            SetDataBarCriteriaConditionValue(condition.MaxPoint, style.maximum);

            //Set the Axis position
            condition.AxisPosition = XlDataBarAxisPosition.xlDataBarAxisAutomatic;

            //==Set the appreance object.==

            //Set the Databar fill type
            switch (style.appearance.fillType)
            {
                case dataBarAppearanceFillType.Solid:
                    condition.BarFillType = XlDataBarFillType.xlDataBarFillSolid;
                    break;
                case dataBarAppearanceFillType.Gradient:
                    condition.BarFillType = XlDataBarFillType.xlDataBarFillGradient;
                    break;
            }

            //Set the appreance fill color
            FormatColor barColor = condition.BarColor;
            barColor.Color = _cu.GetWin32Color(style.appearance.fillHexColor);
            barColor.TintAndShade = 0;

            //Set the Databar border type
            switch (style.appearance.borderType)
            {
                case dataBarAppearanceBorderType.Solid:
                    condition.BarBorder.Type = XlDataBarBorderType.xlDataBarBorderSolid;
                    break;
                case dataBarAppearanceBorderType.None:
                    condition.BarBorder.Type = XlDataBarBorderType.xlDataBarBorderNone;
                    break;
            }

            //Set the appreance border hex color
            FormatColor barBorderColor = condition.BarBorder.Color;
            barBorderColor.Color = _cu.GetWin32Color(style.appearance.borderHexColor);
            barBorderColor.TintAndShade = 0;

            //Set the Bar direction
            switch (style.appearance.barDirection)
            {
                case dataBarAppearanceBarDirectionType.Context:
                    condition.Direction = Convert.ToInt32(XlReadingOrder.xlContext);
                    break;
                case dataBarAppearanceBarDirectionType.Left_To_Right:
                    condition.Direction = Convert.ToInt32(XlReadingOrder.xlLTR);
                    break;
                case dataBarAppearanceBarDirectionType.Right_To_Left:
                    condition.Direction = Convert.ToInt32(XlReadingOrder.xlRTL);
                    break;
            }

            //==End Set the appreance object.==


        }

        /// <summary>
        /// Sets the DataBar Type and Value parameters
        /// </summary>
        /// <param name="conditionValue"></param>
        /// <param name="criteria"></param>
        /// <param name="isMax"></param>
        private void SetDataBarCriteriaConditionValue(ConditionValue conditionValue, dataBarCriteria criteria, bool isMax = true)
        {
            switch (criteria.criteriaType)
            {
                case criteriaType.Automatic:
                    conditionValue.Modify(isMax
                        ? XlConditionValueTypes.xlConditionValueAutomaticMax
                        : XlConditionValueTypes.xlConditionValueAutomaticMin);
                    break;
                case criteriaType.Highest:
                    conditionValue.Modify(XlConditionValueTypes.xlConditionValueHighestValue);
                    break;
                case criteriaType.Lowest:
                    conditionValue.Modify(XlConditionValueTypes.xlConditionValueLowestValue);
                    break;
                case criteriaType.Number:
                    conditionValue.Modify(XlConditionValueTypes.xlConditionValueNumber, criteria.criteriaValue);
                    break;
                case criteriaType.Percent:
                    conditionValue.Modify(XlConditionValueTypes.xlConditionValuePercent, criteria.criteriaValue);
                    break;
                case criteriaType.Percentile:
                    conditionValue.Modify(XlConditionValueTypes.xlConditionValuePercentile, criteria.criteriaValue);
                    break;
            }

        }

        private void SetConditionalFormattingColorScale(Microsoft.Office.Interop.Excel.Range range, colorScale style)
        {
            if (style == null)
            {
                PafApp.GetLogger().Error("Format range or conditional style is null.");
                return;
            }

            int index = 2;
            if (style.scaleType == colorScaleColorNumType.Three_Color)
            {
                index = 3;
            }
            ColorScale condition = (ColorScale)range.FormatConditions.AddColorScale(index);
            condition.SetFirstPriority();

            int i = 1;
            if (style.minimum != null)
            {
                SetColorScaleCriteria(condition.ColorScaleCriteria.Item[i], style.minimum, false);
                i++;
            }
            if (style.midpoint != null)
            {
                SetColorScaleCriteria(condition.ColorScaleCriteria.Item[i], style.midpoint, false);
                i++;
            }
            if (style.maximum != null)
            {
                SetColorScaleCriteria(condition.ColorScaleCriteria.Item[i], style.maximum);
            }
        }

        private void SetColorScaleCriteria(ColorScaleCriterion excelColorScale, colorScaleCriteria criteria, bool isMax = true)
        {
            switch (criteria.criteriaType)
            {
                case criteriaType.Automatic:
                    excelColorScale.Type = isMax ? XlConditionValueTypes.xlConditionValueAutomaticMax : XlConditionValueTypes.xlConditionValueAutomaticMin;
                    break;
                case criteriaType.Highest:
                    excelColorScale.Type = XlConditionValueTypes.xlConditionValueHighestValue;
                    break;
                case criteriaType.Lowest:
                    excelColorScale.Type = XlConditionValueTypes.xlConditionValueLowestValue;
                    break;
                case criteriaType.Number:
                    excelColorScale.Type = XlConditionValueTypes.xlConditionValueNumber;
                    excelColorScale.Value = criteria.criteriaValue;
                    break;
                case criteriaType.Percent:
                    excelColorScale.Type = XlConditionValueTypes.xlConditionValuePercent;
                    excelColorScale.Value = criteria.criteriaValue;
                    break;
                case criteriaType.Percentile:
                    excelColorScale.Type = XlConditionValueTypes.xlConditionValuePercentile;
                    excelColorScale.Value = criteria.criteriaValue;
                    break;
            }
            if (String.IsNullOrEmpty(criteria.criteriaHexColor))
            {
                excelColorScale.FormatColor.ColorIndex = XlColorIndex.xlColorIndexNone;
                excelColorScale.FormatColor.TintAndShade = 0;
            }
            else
            {
                excelColorScale.FormatColor.Color = _cu.GetWin32Color(criteria.criteriaHexColor);
                excelColorScale.FormatColor.TintAndShade = 0;
            }
        }

        /// <summary>
        /// Gets the numeric format for a cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <returns>The numeric format for the cell.</returns>
        public string GetNumericFormat(int row, int col)
        {
            //Microsoft.Office.Interop.Excel.Range rng = (Microsoft.Office.Interop.Excel.Range)sheet.Cells[row, col];
            Microsoft.Office.Interop.Excel.Range rng = GetRange(row, col, _sheet);

            return rng.Cells.NumberFormat.ToString();
        }

        /// <summary>
        /// Merge a contiguous range of cells.
        /// </summary>
        /// <param name="r">The range to merge.</param>
        /// <param name="across">True to merge cells in each row of the specified range 
        /// as separate merged cells. The default value is False.</param>
        public void SetMergedCells(ContiguousRange r, bool across)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(r, _sheet);
            rng.Merge(false);
        }

        /// <summary>
        /// Set a style to a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set the styles.</param>
        /// <param name="format">The format to be applied to the list of ranges.</param>
        public void SetStyles(List<Range> ranges, Format format)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            //must be before bold, because if you set this after you set a bold property
            //this will reset the bold property to "false"
            if (format.FontName != null)
            {
                SetFontName(ranges, format.FontName);
            }

            if (format.Alignment != null)
            {
                SetHorizontalAlignment(ranges, format.Alignment);
            }


            if (format.BgHexFillColor != null)
            {
                int colorIdx;
                if (int.TryParse(format.BgHexFillColor, out colorIdx))
                {
                    if (colorIdx == PafAppConstants.xlColorIndexNone)
                    {
                        SetBgFillColor(ranges, Constants.xlNone.ToString());
                    }
                    else
                    {
                        SetBgFillColor(ranges, format.BgHexFillColor);
                    }
                }
                else
                {
                    SetBgFillColor(ranges, format.BgHexFillColor);
                }
            }

            if (format.Bold != null)
            {
                SetBold(ranges, format.Bold);
            }

            if (format.FontHexColor != null)
            {
                SetFontColor(ranges, format.FontHexColor);
            }

            if (format.Italics != null)
            {
                SetItalics(ranges, format.Italics);
            }

            if (format.Size > 0)
            {
                SetFontSize(ranges, format.Size);
            }

            if (format.StrikeOut != null)
            {
                SetStrikeout(ranges, format.StrikeOut);
            }
 
            if (format.UnderLine != null)
            {
                SetUnderline(ranges, format.UnderLine);
            }

            if (format.DoubleUnderline != null)
            {
                SetDoubleUnderline(ranges, format.DoubleUnderline);
            }

            if (format.Plannable != null)
            {
                if (Boolean.Parse(format.Plannable) == true)
                {
                    UnLockCells(ranges);
                }
                else if (Boolean.Parse(format.Plannable) == false)
                {
                    Lockcells(ranges);
                }
            }

            if (format.NumberFormat != null)
            {
                SetNumericFormat(ranges, format.NumberFormat);
            }

            if (format.Borders > 0)
            {
                SetBorders(ranges, format.Borders);
            }
            startTime.Stop();
            //PafApp.GetLogger().InfoFormat("SetStyles: {0}, runtime: {1} (ms)", new object[] { format, startTime.ElapsedMilliseconds.ToString("N0") });
            PafApp.GetLogger().InfoFormat("SetStyles (List, Format), runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Set a style to a list of ranges.
        /// </summary>
        /// <param name="name">Name of the global style.</param>
        /// <param name="ranges">List of ranges to set the styles.</param>
        /// <param name="format">The format to be applied to the list of ranges.</param>
        public void SetStyles(string name, List<Range> ranges, Format format)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            if (ranges == null || ranges.Count == 0)
            {
                PafApp.GetLogger().Info("Ranges is null or empty.");
                return;
            }

            Style style;

            try
            {
                style = Globals.ThisWorkbook.Styles[name];
                PafApp.GetLogger().InfoFormat("Style: '{0}' already exists, will modify.", new object []{name} );
            }
                // Style doesn't exist
            catch
            {
                try
                {
                    PafApp.GetLogger().InfoFormat("Style: '{0}' does not exists.", new object[] { name });
                    style = Globals.ThisWorkbook.Styles.Add(name, Type.Missing);
                }
                catch (Exception ex)
                {
                    PafApp.GetLogger().WarnFormat("Error while adding style: '{0}' exception to follow, no sytle/formatting will be applied.", new object[] { name });
                    PafApp.GetLogger().Warn("Style adding error: " + ex.Message);
                    return;
                }
            }

            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Made it past the catch");


            //must be before bold, because if you set this after you set a bold property
            //this will reset the bold property to "false"
            if (format.FontName != null)
            {
                style.Font.Name = format.FontName;
            }

            //Set Excel style properties
            if (format.Alignment != null)
            {
                try
                {
                    switch (format.Alignment.ToLower())
                    {
                        case "left":
                            style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft;
                            break;
                        case "center":
                            style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                            break;
                        case "right":
                            style.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight;
                            break;
                    }
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting alignment.  No alignment will be set.");
                }
                
            }

            //TTN-1407
            try
            {
                if (format.BgHexFillColor != null)
                {
                    int colorIdx;
                    if (int.TryParse(format.BgHexFillColor, out colorIdx))
                    {
                        if (colorIdx == PafAppConstants.xlColorIndexNone)
                        {
                            style.Interior.ColorIndex = Constants.xlNone;
                        }
                        else
                        {
                            style.Interior.Color = _cu.GetWin32Color(format.BgHexFillColor);
                        }
                    }
                    else
                    {
                        style.Interior.Color = _cu.GetWin32Color(format.BgHexFillColor);
                    }
                }
            }
            catch (Exception)
            {
                PafApp.GetLogger().Warn("Error setting style.Interior.Color.  No color will be set.");
            }



            if (format.Bold != null)
            {
                try
                {
                    style.Font.Bold = Boolean.Parse(format.Bold);
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting bold.  No bold will be set.");
                }
                
            }

            if (format.FontHexColor != null)
            {
                try
                {
                    style.Font.Color = _cu.GetWin32Color(format.FontHexColor);
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting style.Font.Color.  No color will be set.");
                }
            }

            if (format.Italics != null)
            {
                try
                {
                    style.Font.Italic = Boolean.Parse(format.Italics);
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting italic.  No italic will be set.");
                }
               
            }

            if (format.Size > 0)
            {
                try
                {
                    style.Font.Size = format.Size;
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting font size.  No font size will be set.");
                }
            }

            if (format.StrikeOut != null)
            {
                try
                {
                    style.Font.Strikethrough = Boolean.Parse(format.StrikeOut);
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting strike out.  No strikeout will be set."); 
                }
            }

            if (format.UnderLine != null)
            {
                try
                {
                    switch (Boolean.Parse(format.UnderLine))
                    {
                        case true:
                            style.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleSingle;
                            break;
                        case false:
                            style.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                            break;
                    }
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting underline.  No underline will be set.");
                }
            }

            if (format.DoubleUnderline != null)
            {
                try
                {
                    switch (Boolean.Parse(format.DoubleUnderline))
                    {
                        case true:
                            style.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleDouble;
                            break;
                        case false:
                            style.Font.Underline = Microsoft.Office.Interop.Excel.XlUnderlineStyle.xlUnderlineStyleNone;
                            break;
                    }
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting double underline.  No double underline will be set.");
                }
            }

            //Protect cells
            if (format.Plannable != null)
            {
                try
                {
                    style.Locked = !Boolean.Parse(format.Plannable);
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting locked style.  No locked style will be set.");
                }
               
            }

            //Set numeric format
            if (format.NumberFormat != null)
            {
                try
                {
                    style.NumberFormat = format.NumberFormat;
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting numeric format.  No numeric format will be set.");
                }
            }

            //Set borders
            if (format.Borders > 0)
            {
                Border border = new Border(format.Borders);
                try
                {
                    if (border.IsAll() || border.IsLeft())
                    {

                        style.Borders[LEFT_BDR_IDX].LineStyle = XlLineStyle.xlContinuous;
                        style.Borders[LEFT_BDR_IDX].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                        style.Borders[LEFT_BDR_IDX].Weight = XlBorderWeight.xlThin;
                    }

                    if (border.IsAll() || border.IsRight())
                    {
                        style.Borders[RIGHT_BDR_IDX].LineStyle = XlLineStyle.xlContinuous;
                        style.Borders[RIGHT_BDR_IDX].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                        style.Borders[RIGHT_BDR_IDX].Weight = XlBorderWeight.xlThin;
                    }

                    if (border.IsAll() || border.IsTop())
                    {
                        style.Borders[TOP_BDR_IDX].LineStyle = XlLineStyle.xlContinuous;
                        style.Borders[TOP_BDR_IDX].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                        style.Borders[TOP_BDR_IDX].Weight = XlBorderWeight.xlThin;
                    }

                    if (border.IsAll() || border.IsBottom())
                    {
                        style.Borders[BOTTOM_BDR_IDX].LineStyle = XlLineStyle.xlContinuous;
                        style.Borders[BOTTOM_BDR_IDX].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                        style.Borders[BOTTOM_BDR_IDX].Weight = XlBorderWeight.xlThin;
                    }
                }
                catch (Exception)
                {
                    PafApp.GetLogger().Warn("Error setting border.  No Border will be set.");
                }
            }

            //Get range and apply style
            Range tmpRange = null;
            try
            {
                tmpRange = new Range(ranges);
                Microsoft.Office.Interop.Excel.Range rng = GetRange(tmpRange, _sheet);
                if (rng == null)
                {
                    PafApp.GetLogger().Warn("GetRange returned null");
                }
                else
                {
                    rng.Style = name;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().WarnFormat("Error applying style: {0}", new object[]{ex.Message });
                PafApp.GetLogger().WarnFormat("Error applying style: {0}", new object[] { ex.Message });
                StringBuilder sb = new StringBuilder();
                foreach (var cr in ranges.SelectMany(range => range.ContiguousRanges))
                {
                    sb.Append(cr).Append(" | ");
                }
                PafApp.GetLogger().WarnFormat("Cells that errored:  {0}", new object[] { sb.ToString() });
            }
            
            startTime.Stop();
            if (tmpRange == null)
            {
                PafApp.GetLogger().InfoFormat("SetStyles: {0}, runtime: {1} (ms)", new object[] {name, startTime.ElapsedMilliseconds.ToString("N0")});
            }
            else
            {
                PafApp.GetLogger().InfoFormat("SetStyles: {0}, Range: {1} , runtime: {2} (ms)", new object[] { name, tmpRange.ToString(), startTime.ElapsedMilliseconds.ToString("N0") });
                
            }
        }

        /// <summary>
        /// Selects a cell.
        /// </summary>
        /// <param name="cell">The cell to select.</param>
        public void Select(Cell cell)
        {
            ((Microsoft.Office.Interop.Excel.Range)_sheet.Cells[cell.CellAddress.Row, cell.CellAddress.Col]).Select();
        }

        /// <summary>
        /// Selects a range.
        /// </summary>
        /// <param name="range">The cell to select.</param>
        public void Select(Range range)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);
            rng.Select();
        }

        /// <summary>
        /// Get the curretnly active cell.
        /// </summary>
        /// <returns>The currently active cell.</returns>
        public Cell GetActiveCell()
        {
            return new Cell(_sheet.Application.ActiveCell.Row, _sheet.Application.ActiveCell.Column);
        }

        /// <summary>
        /// Positions the Window based on the top left cell displayed in the window
        /// </summary>
        /// <param name="topLeftCell">The top left displayed in the window</param>
        public void SetWindowPosition(Cell topLeftCell)
        {
            _sheet.Application.ActiveWindow.ActivePane.ScrollRow = topLeftCell.CellAddress.Row;
            _sheet.Application.ActiveWindow.ActivePane.ScrollColumn = topLeftCell.CellAddress.Col;
        }


        /// <summary>
        /// Gets the top left cell displayed in the window
        /// </summary>
        public Cell GetWindowPosition()
        {
            Cell topLeftCell = new Cell(_sheet.Application.ActiveWindow.ActivePane.ScrollRow,
                _sheet.Application.ActiveWindow.ActivePane.ScrollColumn);

            return topLeftCell;
        }

        /// <summary>
        /// Returns a Range Object containing continuous ranges for each Area selected
        /// </summary>
        /// <returns>A ContiguougRange of selected cells.</returns>
        public Range GetSelectedCells()
        {
            Range selectedCells = new Range();
            //Check to see that at least 1 Area is selected
            if (_sheet.Application.ActiveWindow.RangeSelection.Areas.Count > 0)
            {
                for (int i = 1; i <= _sheet.Application.ActiveWindow.RangeSelection.Areas.Count; i++)
                {
                    Microsoft.Office.Interop.Excel.Range area = _sheet.Application.ActiveWindow.RangeSelection.Areas.get_Item(i);

                    int row = area.Row;
                    int col = area.Column;
                    CellAddress topLeft = new CellAddress(row, col);

                    row = row + area.Rows.Count - 1;
                    col = col + area.Columns.Count - 1;
                    CellAddress botRight = new CellAddress(row, col);
                    selectedCells.AddContiguousRange(new ContiguousRange(topLeft, botRight));
                }
            }

            return selectedCells;
        }

        /// <summary>
        /// The number of cells in the current range selection
        /// </summary>
        /// <returns></returns>
        public long GetSelectedCellCount()
        {
            Microsoft.Office.Interop.Excel.Range rng = _sheet.Application.ActiveWindow.RangeSelection;
            //TTN-1155
            try
            {
                long rangeCount = Convert.ToInt64(rng.CountLarge);
                if ((bool) rng.Cells.MergeCells)
                {
                    
                    if (Convert.ToInt64(rng.Cells.CountLarge) == rangeCount)
                    {
                        return 1;
                    }
                    else
                    {
                        return rangeCount;
                    }
                }
                else
                {
                    return rangeCount;
                }
            }
            catch(Exception)
            {
                return Convert.ToInt64(rng.CountLarge);
            }
        }

        /// <summary>
        /// Enable/disable the firing of applicaiton events.
        /// </summary>
        /// <param name="flag">true to enable events, false to disable events.</param>
        public void EnableEvents(bool flag)
        {
            try
            {
                if (_sheet != null && _sheet.Application != null) _sheet.Application.EnableEvents = flag;
            }
            catch (Exception)
            {
                //We tried, but oh well.  The code will handle the error.
            }
        }

        /// <summary>
        /// Clears all the contents of the sheet.
        /// </summary>
        public void Clear()
        {
            if (_sheet != null)
            {
                _sheet.Cells.Clear();
                //TTN-629
                _sheet.Rows.RowHeight = 12.75;
                _sheet.Columns.ColumnWidth = 12;
            }
        }

        public void ClearGroupings()
        {
            if (_sheet != null)
            {
                _sheet.Cells.ClearOutline();
            }
        }

        public void ClearGroupings(ViewAxis viewAxis, int level)
        {
            if (_sheet == null) return;
            try
            {
                switch (viewAxis)
                {
                    case ViewAxis.Row:
                        //Microsoft.Office.Interop.Excel.Range rowRange = sheet.Rows[string.Format("{0}:{1}", range.TopLeft.Row, range.BottomRight.Row), Type.Missing] as Microsoft.Office.Interop.Excel.Range;
                        //if (rowRange == null) return;
                        for (int i = 1; i < level; i++)
                        {
                            //rowRange.Rows.Ungroup();
                            _sheet.Rows.Ungroup();
                        }
                        break;
                    case ViewAxis.Col:
                        //Microsoft.Office.Interop.Excel.Range colRange = sheet.Columns[string.Format("{0}:{1}", range.TopLeft.ColAsLetter, range.BottomRight.ColAsLetter), Type.Missing] as Microsoft.Office.Interop.Excel.Range;
                        //if (colRange == null) return;
                        for(int i = 1; i < level; i++)
                        {
                            //colRange.Columns.Ungroup();
                            _sheet.Columns.Ungroup();
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }


        /// <summary>
        /// Clears all the contents of a ContiguousRange.
        /// </summary>
        /// <param name="r">The ContiguousRange to clear.</param>
        public void Clear(ContiguousRange r)
        {
            Microsoft.Office.Interop.Excel.Range changeRange = GetRange(r, _sheet);

            changeRange.Cells.Clear();

            //sheet.Columns.ColumnWidth = 10;
        }

        /// <summary>
        /// Sets the autofit of a particular range.
        /// </summary>
        /// <param name="r">The ContiguousRange to autofit.</param>
        public void AutoFitColumn(ContiguousRange r)
        {
            Microsoft.Office.Interop.Excel.Range autoFitRange = GetRange(r, _sheet);
            autoFitRange.EntireColumn.AutoFit();
        }

        /// <summary>
        /// Sets the autofit of a particular range.
        /// </summary>
        /// <param name="r">The ContiguousRange to autofit.</param>
        public void AutoFitRow(ContiguousRange r)
        {
            Microsoft.Office.Interop.Excel.Range autoFitRange = GetRange(r, _sheet);
            autoFitRange.EntireRow.AutoFit();

        }

        /// <summary>
        /// Gets the address of the range.
        /// </summary>
        /// <param name="r">The ContiguousRange to get the address of.</param>
        /// <returns>Gets the address of the ContiguousRange.</returns>
        public string GetAddress(ContiguousRange r)
        {
            Microsoft.Office.Interop.Excel.Range range = GetRange(r, _sheet);
            
            return range.get_Address(Type.Missing, Type.Missing, XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
        }

        /// <summary>
        /// Shows/hides the column headers.
        /// </summary>
        /// <param name="show">true to show the heading, false to hide the headings.</param>
        public void SetColumnHeader(bool show)
        {
            try
            {
                _sheet.Application.ActiveWindow.DisplayHeadings = show;
            }
            catch (Exception)
            {
            }
            
        }

        /// <summary>
        /// Clears the print area for the current object.
        /// </summary>
        public void ClearPrintRange()
        {
            _sheet.PageSetup.PrintArea = "";
        }

        public void SetPrintRange(ContiguousRange r, IPrintStyle printStyle)
        {

            Stopwatch stopwatch = Stopwatch.StartNew();
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;
            try
            {
                // ReSharper disable RedundantCheckBeforeAssignment
                // ReSharper disable CompareOfFloatsByEqualityOperator
                if (debugEnabled) PafApp.GetLogger().DebugFormat("Setting Print Style: {0}", new object[] { printStyle.Name });

                printStyle.DebugPrintPrintStyle();

                _sheet.PageSetup.PrintArea = r.A1Address;


                //Page Tab   
                _sheet.PageSetup.Orientation = printStyle.Landscape ? XlPageOrientation.xlLandscape : XlPageOrientation.xlPortrait;

                if(printStyle.AdjustTo)
                {
                    if (_sheet.PageSetup.Zoom.GetInteger(false) != printStyle.PercentNormalSize)
                    {
                        _sheet.PageSetup.Zoom = printStyle.PercentNormalSize;
                    }
                }
                else if(printStyle.FitTo)
                {
                    _sheet.PageSetup.Zoom = false;
                    if (_sheet.PageSetup.FitToPagesTall.GetInteger(false) != printStyle.PageTall)
                    {
                        _sheet.PageSetup.FitToPagesTall = printStyle.PageTall;
                    }
                    if (_sheet.PageSetup.FitToPagesWide.GetInteger(false) != printStyle.PageWide)
                    {
                        _sheet.PageSetup.FitToPagesWide = printStyle.PageWide;
                    }
                }


                

                if (!String.IsNullOrEmpty(printStyle.PaperSize))
                {
                    XlPaperSize paperSize = printStyle.PaperSize.ToXlPaperSize();
                    if (_sheet.PageSetup.PaperSize != paperSize)
                    {
                        _sheet.PageSetup.PaperSize = paperSize;
                    }
                }

                int firstPageNum;
                if (printStyle.FirstPageNumber.IsInteger(out firstPageNum))
                {
                    _sheet.PageSetup.FirstPageNumber = firstPageNum;
                }

                //Margins Tab
                double left = _sheet.Application.InchesToPoints(printStyle.Left).Round(0);

                if (_sheet.PageSetup.LeftMargin != left)
                {
                    _sheet.PageSetup.LeftMargin = left;
                }

                double right = _sheet.Application.InchesToPoints(printStyle.Right).Round(0);
                if (_sheet.PageSetup.RightMargin != right)
                {
                    _sheet.PageSetup.RightMargin = right;
                }

                double top = _sheet.Application.InchesToPoints(printStyle.Top).Round(0);
                if (_sheet.PageSetup.TopMargin != top)
                {
                    _sheet.PageSetup.TopMargin = top;
                }


                double bottom = _sheet.Application.InchesToPoints(printStyle.Bottom).Round(0);

                if (_sheet.PageSetup.BottomMargin != bottom)

                {
                    _sheet.PageSetup.BottomMargin = bottom;
                }


                double header = _sheet.Application.InchesToPoints(printStyle.Header).Round(0);
                if (_sheet.PageSetup.HeaderMargin != header)
                {
                    _sheet.PageSetup.HeaderMargin = header;
                }

                double footer = _sheet.Application.InchesToPoints(printStyle.Footer).Round(0);
                if (_sheet.PageSetup.FooterMargin != footer)
                {
                    _sheet.PageSetup.FooterMargin = footer;
                }

                if (_sheet.PageSetup.CenterHorizontally != printStyle.CenterHorizontally)
                {
                    _sheet.PageSetup.CenterHorizontally = printStyle.CenterHorizontally;
                }

                if(_sheet.PageSetup.CenterVertically != printStyle.CenterVertically)
                {
                    _sheet.PageSetup.CenterVertically = printStyle.CenterVertically;
                }

                //Header Footer
                if (!_sheet.PageSetup.CenterHeader.Equals(printStyle.HeaderText))
                {
                    _sheet.PageSetup.CenterHeader = printStyle.HeaderText;
                }

                if (!_sheet.PageSetup.CenterFooter.Equals(printStyle.FooterText))
                {
                    _sheet.PageSetup.CenterFooter = printStyle.FooterText;
                }
                //printStyle.DiffOddAndEvenPages;
                //printStyle.DiffFirstPage;
                //printStyle.ScaleWithDocument;
                //printStyle.AdjustTo;

                //Sheet
                if (!printStyle.RowsToRepeatAtTop.EqualsIgnoreCase("none"))
                {
                    if (!_sheet.PageSetup.PrintTitleRows.Equals(printStyle.RowRange.R1C1Address))
                    {
                        _sheet.PageSetup.PrintTitleRows = printStyle.RowRange.R1C1Address;
                    }
                }

                if (!printStyle.ColsToRepeatAtLeft.EqualsIgnoreCase("none"))
                {
                    if (!_sheet.PageSetup.PrintTitleColumns.Equals(printStyle.ColumnRange.R1C1Address))
                    {
                        _sheet.PageSetup.PrintTitleColumns = printStyle.ColumnRange.R1C1Address;
                    }
                }

                if (_sheet.PageSetup.PrintGridlines != printStyle.Gridlines)
                {
                    _sheet.PageSetup.PrintGridlines = printStyle.Gridlines;
                }

                if (_sheet.PageSetup.BlackAndWhite != printStyle.BlackAndWhite)
                {
                    _sheet.PageSetup.BlackAndWhite = printStyle.BlackAndWhite;
                }

                if (_sheet.PageSetup.Draft != printStyle.DraftQuality)
                {
                    _sheet.PageSetup.Draft = printStyle.DraftQuality;
                }

                if (!String.IsNullOrEmpty(printStyle.Comment))
                {
                    XlPrintLocation printLocation = printStyle.Comment.ToXlPrintLocation();
                    if (_sheet.PageSetup.PrintComments != printLocation)
                    {
                        _sheet.PageSetup.PrintComments = printLocation;
                    }
                }
                if (!String.IsNullOrEmpty(printStyle.CellErrorsAs))
                {
                    XlPrintErrors printErrors = printStyle.CellErrorsAs.ToXlPrintErrors();
                    if (printErrors != _sheet.PageSetup.PrintErrors)
                    {
                        _sheet.PageSetup.PrintErrors = printErrors;
                    }
                }
                if (printStyle.DownThenOver)
                {
                    if (_sheet.PageSetup.Order != XlOrder.xlDownThenOver)
                    {
                        _sheet.PageSetup.Order = XlOrder.xlDownThenOver;
                    }
                }

                if (printStyle.OverThenDown)
                {
                    if (_sheet.PageSetup.Order != XlOrder.xlOverThenDown)
                    {
                        _sheet.PageSetup.Order = XlOrder.xlOverThenDown;
                    }
                }

                _sheet.DebugPrintSettings();
                // ReSharper restore CompareOfFloatsByEqualityOperator
                // ReSharper restore RedundantCheckBeforeAssignment

            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
            finally
            {
                stopwatch.Stop();
                if (debugEnabled) PafApp.GetLogger().DebugFormat("SetPrintRange runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
        }


        /// <summary>
        /// Merge cell text
        /// </summary>
        /// <param name="r"></param>
        /// <param name="sheetPassword"></param>
        public void Merge(ContiguousRange r, string sheetPassword)
        {
            Microsoft.Office.Interop.Excel.Range range = GetRange(r, _sheet);

            range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
            range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

            range.Merge(false);

        }

        /// <summary>
        /// Merge cell text
        /// </summary>
        /// <param name="r"></param>
        /// <param name="viewAxis"></param>
        /// <param name="sheetPassword"></param>
        public void Merge(ContiguousRange r, ViewAxis viewAxis, string sheetPassword)
        {
            try
            {
                Microsoft.Office.Interop.Excel.Range range = GetRange(r, _sheet);

                if (viewAxis == ViewAxis.Col)
                {
                    range.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                }
                else if (viewAxis == ViewAxis.Row)
                {
                    range.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                }

                //PafApp.GetGridApp().UnprotectSheet(sheet, sheetPassword);
                range.Merge(false);
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Unmerge a range.
        /// </summary>
        /// <param name="r">Range to unmerge.</param>
        /// <param name="sheetPassword"></param>
        public void UnMerge(ContiguousRange r, string sheetPassword)
        {
            _sheet.Unprotect(sheetPassword);

            try
            {
                Microsoft.Office.Interop.Excel.Range range = GetRange(r, _sheet);

                range.UnMerge();
            }
            finally
            {
                _sheet.Protect(sheetPassword, false, true, true, true, true, true, true,
                              false, false, true, false, false, false, false, false);
            }
        }

        /// <summary>
        /// Wrap cell text
        /// </summary>
        /// <param name="r"></param>
        public void WrapText(ContiguousRange r)
        {
            Microsoft.Office.Interop.Excel.Range range = GetRange(r, _sheet);

            range.WrapText = true;

        }

        /// <summary>
        /// Sets Row Height
        /// </summary>
        /// <param name="range"></param>
        /// <param name="rowHeight"></param>
        public void SetRowHeight(Range range, float rowHeight)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

            rng.RowHeight = rowHeight;
        }

        /// <summary>
        /// Sets Column Width
        /// </summary>
        /// <param name="range"></param>
        /// <param name="colWidth"></param>
        public void SetColumnWidth(Range range, float colWidth)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

            rng.ColumnWidth = colWidth;
        }

        /// <summary>
        /// Copies the value from 1 cell to another
        /// </summary>
        /// <param name="copyFromCell">The cell the data is copied from</param>
        /// <param name="copyToCell">The cell the data is copied to</param>
        public void CopyCellValue(CellAddress copyFromCell, CellAddress copyToCell)
        {
            Microsoft.Office.Interop.Excel.Range copyFromRange = (Microsoft.Office.Interop.Excel.Range)_sheet.Cells[copyFromCell.Row, copyFromCell.Col];
            Microsoft.Office.Interop.Excel.Range copyToRange = (Microsoft.Office.Interop.Excel.Range)_sheet.Cells[copyToCell.Row, copyToCell.Col];

            copyToRange.Value2 = copyFromRange.Value2;
        }

        /// <summary>
        /// Copies the formula from 1 cell to another
        /// </summary>
        /// <param name="copyFromCell">The cell the data is copied from</param>
        /// <param name="copyToCell">The cell the data is copied to</param>
        public void CopyCellFormula(CellAddress copyFromCell, CellAddress copyToCell)
        {
            Microsoft.Office.Interop.Excel.Range copyFromRange = (Microsoft.Office.Interop.Excel.Range)_sheet.Cells[copyFromCell.Row, copyFromCell.Col];
            Microsoft.Office.Interop.Excel.Range copyToRange = (Microsoft.Office.Interop.Excel.Range)_sheet.Cells[copyToCell.Row, copyToCell.Col];

            copyToRange.Formula = copyFromRange.Formula;
        }

        /// <summary>
        /// Creates a grouping from a range.
        /// </summary>
        /// <param name="range"></param>
        /// <param name="name"></param>
        /// <param name="level"></param>
        private object SetGrouping(Microsoft.Office.Interop.Excel.Range range, string name = null, int? level = null)
        {
            if (range == null)
            {
                PafApp.GetLogger().Warn("Range is null or empty, cannot set row grouping");
                return null;
            }

            if (!String.IsNullOrWhiteSpace(name))
            {
                range.Name = name;
            }
            range.Group(Type.Missing, Type.Missing, Type.Missing, Type.Missing);
            if (level.HasValue)
            {
                range.OutlineLevel = level.Value;
            }
            dynamic test = range.OutlineLevel;
            return test;
        }

        /// <summary>
        /// Changes the orientation of the summary rows (Above or OnRight)
        /// </summary>
        /// <param name="viewAxis">Axis Row or Column</param>
        /// <param name="parentFirst">Parent First (if so the orientation will be changed)</param>
        private void SetGroupingSummaryLocation(ViewAxis viewAxis, bool parentFirst)
        {
            switch (viewAxis)
            {
                case ViewAxis.Row:
                    _sheet.Outline.SummaryRow = !parentFirst ? XlSummaryRow.xlSummaryBelow : XlSummaryRow.xlSummaryAbove;
                    PafApp.GetLogger().InfoFormat("Axis: {0}, parent first: {1}, summary set to: {2}", viewAxis, parentFirst, _sheet.Outline.SummaryRow);
                    break;
                case ViewAxis.Col:
                    _sheet.Outline.SummaryColumn = !parentFirst ? XlSummaryColumn.xlSummaryOnRight : XlSummaryColumn.xlSummaryOnLeft;
                    PafApp.GetLogger().InfoFormat("Axis: {0}, parent first: {1}, summary set to: {2}", viewAxis, parentFirst, _sheet.Outline.SummaryColumn);
                    break;
            }
        }

        /// <summary>
        /// Gets the top merged cell in a ranged of merged cell.
        /// </summary>
        /// <param name="excelRange">The EXCEL range that is merged.</param>
        /// <returns>A PACE cell.</returns>
        private static Cell GetTopMergedCell(Microsoft.Office.Interop.Excel.Range excelRange)
        {
            Cell cell = new Cell(excelRange.Row, excelRange.Column);
            
            object[,] values = (object[,])excelRange.Value2;

            if (values != null)
            {
                for (int c = 1; c <= values.GetUpperBound(1); c++)
                {
                    for (int r = 1; r <= values.GetUpperBound(0); r++)
                    {
                        if (values[r, c]!= null && !String.IsNullOrEmpty(values[r, c].ToString()))
                        {
                            cell.Value = values[r, c];
                            break;
                        }
                    }
                }
            }

            return cell;
        }

        private static Microsoft.Office.Interop.Excel.Range GetRange(IEnumerable<Range> ranges, Worksheet sheet)
        {
            Range consolRange = new Range(ranges);
            return UnionRanges(consolRange, sheet);
        }

        private static Microsoft.Office.Interop.Excel.Range GetRange(Range range, Worksheet sheet)
        {
            Range consolRange = Consolidate(range);
            return UnionRanges(consolRange, sheet);
        }

        private static Microsoft.Office.Interop.Excel.Range UnionRanges(Range consolRange, Worksheet sheet)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            Microsoft.Office.Interop.Excel.Range contigRange = null;
            Microsoft.Office.Interop.Excel.Range rng = null;
            Microsoft.Office.Interop.Excel.Application app = sheet.Application;
            int rangeCounter = 0;
            int contigRangeCount = 0;
            List<Microsoft.Office.Interop.Excel.Range> unionRanges = new List<Microsoft.Office.Interop.Excel.Range>();
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;

            contigRangeCount = consolRange.ContiguousRanges.Count;
            if (debugEnabled) PafApp.GetLogger().DebugFormat("Ranges to Union: {0}", new object[] {contigRangeCount});
            foreach (ContiguousRange contigRng in consolRange.ContiguousRanges)
            {
                    int topLeftRow = contigRng.TopLeft.Row;
                    int topLeftCol = contigRng.TopLeft.Col;
                    int bottomRightRow = contigRng.BottomRight.Row;
                    int bottomLeftCol = contigRng.BottomRight.Col;


                    if (topLeftRow == bottomRightRow && topLeftCol == bottomLeftCol)
                    {
                        contigRange = sheet.get_Range(contigRng.TopLeft.A1Address, Missing.Value);
                    }
                    else
                    {
                        contigRange = sheet.get_Range(contigRng.A1Address, Missing.Value);
                    }

                    if (consolRange.ContiguousRanges.Count == 1)
                    {
                        return contigRange;
                    }

                    rangeCounter++;
                    unionRanges.Add(contigRange);

                    if (rangeCounter == consolRange.ContiguousRanges.Count || unionRanges.Count == 29)
                    {
                        if (rng == null)
                        {
                            rng = app.Union(unionRanges[0], unionRanges[1],
                                contigRangeCount > 2 ? unionRanges[2] : Type.Missing,
                                contigRangeCount > 3 ? unionRanges[3] : Type.Missing,
                                contigRangeCount > 4 ? unionRanges[4] : Type.Missing,
                                contigRangeCount > 5 ? unionRanges[5] : Type.Missing,
                                contigRangeCount > 6 ? unionRanges[6] : Type.Missing,
                                contigRangeCount > 7 ? unionRanges[7] : Type.Missing,
                                contigRangeCount > 8 ? unionRanges[8] : Type.Missing,
                                contigRangeCount > 9 ? unionRanges[9] : Type.Missing,
                                contigRangeCount > 10 ? unionRanges[10] : Type.Missing,
                                contigRangeCount > 11 ? unionRanges[11] : Type.Missing,
                                contigRangeCount > 12 ? unionRanges[12] : Type.Missing,
                                contigRangeCount > 13 ? unionRanges[13] : Type.Missing,
                                contigRangeCount > 14 ? unionRanges[14] : Type.Missing,
                                contigRangeCount > 15 ? unionRanges[15] : Type.Missing,
                                contigRangeCount > 16 ? unionRanges[16] : Type.Missing,
                                contigRangeCount > 17 ? unionRanges[17] : Type.Missing,
                                contigRangeCount > 18 ? unionRanges[18] : Type.Missing,
                                contigRangeCount > 19 ? unionRanges[19] : Type.Missing,
                                contigRangeCount > 20 ? unionRanges[20] : Type.Missing,
                                contigRangeCount > 21 ? unionRanges[21] : Type.Missing,
                                contigRangeCount > 22 ? unionRanges[22] : Type.Missing,
                                contigRangeCount > 23 ? unionRanges[23] : Type.Missing,
                                contigRangeCount > 24 ? unionRanges[24] : Type.Missing,
                                contigRangeCount > 25 ? unionRanges[25] : Type.Missing,
                                contigRangeCount > 26 ? unionRanges[26] : Type.Missing,
                                contigRangeCount > 27 ? unionRanges[27] : Type.Missing,
                                contigRangeCount > 28 ? unionRanges[28] : Type.Missing,
                                Type.Missing);

                            contigRangeCount = contigRangeCount - 29;
                        }
                        else
                        {
                            rng = app.Union(rng, unionRanges[0],
                                contigRangeCount > 1 ? unionRanges[1] : Type.Missing,
                                contigRangeCount > 2 ? unionRanges[2] : Type.Missing,
                                contigRangeCount > 3 ? unionRanges[3] : Type.Missing,
                                contigRangeCount > 4 ? unionRanges[4] : Type.Missing,
                                contigRangeCount > 5 ? unionRanges[5] : Type.Missing,
                                contigRangeCount > 6 ? unionRanges[6] : Type.Missing,
                                contigRangeCount > 7 ? unionRanges[7] : Type.Missing,
                                contigRangeCount > 8 ? unionRanges[8] : Type.Missing,
                                contigRangeCount > 9 ? unionRanges[9] : Type.Missing,
                                contigRangeCount > 10 ? unionRanges[10] : Type.Missing,
                                contigRangeCount > 11 ? unionRanges[11] : Type.Missing,
                                contigRangeCount > 12 ? unionRanges[12] : Type.Missing,
                                contigRangeCount > 13 ? unionRanges[13] : Type.Missing,
                                contigRangeCount > 14 ? unionRanges[14] : Type.Missing,
                                contigRangeCount > 15 ? unionRanges[15] : Type.Missing,
                                contigRangeCount > 16 ? unionRanges[16] : Type.Missing,
                                contigRangeCount > 17 ? unionRanges[17] : Type.Missing,
                                contigRangeCount > 18 ? unionRanges[18] : Type.Missing,
                                contigRangeCount > 19 ? unionRanges[19] : Type.Missing,
                                contigRangeCount > 20 ? unionRanges[20] : Type.Missing,
                                contigRangeCount > 21 ? unionRanges[21] : Type.Missing,
                                contigRangeCount > 22 ? unionRanges[22] : Type.Missing,
                                contigRangeCount > 23 ? unionRanges[23] : Type.Missing,
                                contigRangeCount > 24 ? unionRanges[24] : Type.Missing,
                                contigRangeCount > 25 ? unionRanges[25] : Type.Missing,
                                contigRangeCount > 26 ? unionRanges[26] : Type.Missing,
                                contigRangeCount > 27 ? unionRanges[27] : Type.Missing,
                                contigRangeCount > 28 ? unionRanges[28] : Type.Missing);

                            contigRangeCount = contigRangeCount - 29;
                        }
                        unionRanges.Clear();
                    }
             }
            startTime.Stop();
            if (debugEnabled) PafApp.GetLogger().DebugFormat("UnionRanges runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            return rng;
        }

        /// <summary>
        /// Gets an Excel.Range from a Row/Column pair
        /// </summary>
        /// <param name="row">Row number</param>
        /// <param name="col">Column number.</param>
        /// <param name="sheet">Worksheet where the ranges are located.</param>
        /// <returns>An Excel.Range.</returns>
        private static Microsoft.Office.Interop.Excel.Range GetRange(int row, int col, Worksheet sheet)
        {
            return GetRange(new ContiguousRange(new CellAddress(row, col)), sheet);
        }

        /// <summary>
        /// Gets an Excel.Range from a contiguousrange.
        /// </summary>
        /// <param name="contigRng">The contiguousrange</param>
        /// <param name="sheet">Worksheet where the ranges are located.</param>
        /// <returns>An Excel.Range.</returns>
        private static Microsoft.Office.Interop.Excel.Range GetRange(ContiguousRange contigRng, Worksheet sheet)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            Microsoft.Office.Interop.Excel.Range range = null;
            {
                int topLeftRow = contigRng.TopLeft.Row;
                int topLeftCol = contigRng.TopLeft.Col;
                int bottomRightRow = contigRng.BottomRight.Row;
                int bottomLeftCol = contigRng.BottomRight.Col;

                if (topLeftRow == bottomRightRow && topLeftCol == bottomLeftCol)
                {
                    range = sheet.get_Range(contigRng.TopLeft.A1Address, Missing.Value);
                }
                else
                {
                    range = sheet.get_Range(contigRng.A1Address, Missing.Value);
                }
            }
            startTime.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("GetRange creation, runtime: {0} (ms)", new object[]{startTime.ElapsedMilliseconds.ToString("N0")});

            return range;
        }

        /// <summary>
        /// Consolidates a list of ranges into a Range.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <returns>A range from the list of ranges.</returns>
        private static Range Consolidate2(List<Range> ranges)
        {
            Queue<ContiguousRange> contigRanges1 = new Queue<ContiguousRange>();


            // First convert the List of Ranges to a Queue of ContiguousRanges
            foreach (Range range in ranges)
            {
                foreach (ContiguousRange cRng in range.ContiguousRanges)
                {
                    contigRanges1.Enqueue(cRng);
                }
            }
            return ConsolidationPasses(contigRanges1);
        }

        private static Range Consolidate(Range contigRanges)
        {
            var startTime = Stopwatch.StartNew();

            //If the range is already in sorted order, then skip processing.
            if (contigRanges.Sorted)
            {
                //PafApp.GetLogger().InfoFormat("Range already sorted, skipping Consolidate.");
                return contigRanges;
            }


            int numOfCells = contigRanges.ContiguousRanges.Count;
            Range range = new Range();

            //PafApp.GetLogger().InfoFormat("Pre processing count: {0}", numOfCells);

            foreach (ContiguousRange cr in contigRanges.ContiguousRanges)
            {
                range.CombineOrAddContiguousRange(cr);
            }

            //PafApp.GetLogger().InfoFormat("Post processing count: {0}", range.ContiguousRanges.Count);

            startTime.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Consolidate. Num of cells: {0}, range size: {1}, runtime: {2} (ms)", new object[] { numOfCells, range.ContiguousRanges.Count, startTime.ElapsedMilliseconds.ToString("N0") });

            return range;
        }

        private static Range ConsolidationPasses(Queue<ContiguousRange> contigRanges1)
        {
            var startTime = Stopwatch.StartNew();
            int numOfCells = contigRanges1.Count;
            Queue<ContiguousRange> contigRanges2 = new Queue<ContiguousRange>();
            ContiguousRange contigRng1 = null;
            ContiguousRange contigRng2 = null;
            bool firstFlag = true;
            Range consolidatedRange = new Range();
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;

            //if (debugEnabled) PafApp.GetLogger().Debug("Count before" + contigRanges1.Count);

            // Next, make a first consolidation pass
            while (contigRanges1.Count > 0)
            {
                // Handle first pass through
                if (firstFlag)
                {
                    contigRng1 = contigRanges1.Dequeue();
                    firstFlag = false;

                    // If there was only one ContiguousRange in the queue
                    if (contigRanges1.Count <= 0)
                    {
                        break;
                    }
                }

                contigRng2 = contigRanges1.Dequeue();

                if (contigRng1.TopLeft.Row == contigRng2.TopLeft.Row &&
                    contigRng1.BottomRight.Row == contigRng2.BottomRight.Row &&
                    contigRng1.BottomRight.Col == (contigRng2.TopLeft.Col - 1))
                {
                    contigRng1.BottomRight.Col = contigRng2.BottomRight.Col;
                }
                else if (contigRng1.TopLeft.Col == contigRng2.TopLeft.Col &&
                    contigRng1.BottomRight.Col == contigRng2.BottomRight.Col &&
                    contigRng1.BottomRight.Row == (contigRng2.TopLeft.Row - 1))
                {
                    contigRng1.BottomRight.Row = contigRng2.BottomRight.Row;
                }
                else
                {
                    contigRanges2.Enqueue ((ContiguousRange)contigRng1.Clone());
                    contigRng1 = (ContiguousRange)contigRng2.Clone();
                }
            }

            // Add the last ContiguousRange to the queue
            contigRanges2.Enqueue((ContiguousRange)contigRng1.Clone());

            // Finally, make a second pass
            firstFlag = true;
            while (contigRanges2.Count > 0)
            {
                // Handle first pass through
                if (firstFlag == true)
                {
                    contigRng1 = contigRanges2.Dequeue();
                    firstFlag = false;

                    // If there was only one ContiguousRange in the queue
                    if (contigRanges2.Count <= 0)
                    {
                        break;
                    }
                }

                contigRng2 = contigRanges2.Dequeue();

                if (contigRng1.TopLeft.Row == contigRng2.TopLeft.Row &&
                    contigRng1.BottomRight.Row == contigRng2.BottomRight.Row &&
                    contigRng1.BottomRight.Col == (contigRng2.TopLeft.Col - 1))
                {
                    contigRng1.BottomRight.Col = contigRng2.BottomRight.Col;
                }
                else if (contigRng1.TopLeft.Col == contigRng2.TopLeft.Col &&
                    contigRng1.BottomRight.Col == contigRng2.BottomRight.Col &&
                    contigRng1.BottomRight.Row == (contigRng2.TopLeft.Row - 1))
                {
                    contigRng1.BottomRight.Row = contigRng2.BottomRight.Row;
                }
                else
                {
                    consolidatedRange.AddContiguousRange((ContiguousRange)contigRng1.Clone());
                    contigRng1 = (ContiguousRange)contigRng2.Clone();
                }

            }

            // Add the last ContiguousRange to the Range
            consolidatedRange.AddContiguousRange((ContiguousRange)contigRng1.Clone());
            //if (debugEnabled) PafApp.GetLogger().Debug("Count after" + consolidatedRange.ContiguousRanges.Count);
            //if (debugEnabled) PafApp.GetLogger().Debug("topleftrow:" + ((ContiguousRange)consolidatedRange.ContiguousRanges[0]).TopLeft.Row);
            //if (debugEnabled) PafApp.GetLogger().Debug("topleftcol:" + ((ContiguousRange)consolidatedRange.ContiguousRanges[0]).TopLeft.Col);
            //if (debugEnabled) PafApp.GetLogger().Debug("bottomrightrow:" + ((ContiguousRange)consolidatedRange.ContiguousRanges[0]).BottomRight.Row);
            //if (debugEnabled) PafApp.GetLogger().Debug("bottomrightcol:" + ((ContiguousRange)consolidatedRange.ContiguousRanges[0]).BottomRight.Col);

            startTime.Stop();
            if (debugEnabled) PafApp.GetLogger().DebugFormat("ConsolidationPasses. Num of cells: {0}, range size: {1}, runtime: {2} (ms)", new object[] { numOfCells, consolidatedRange.ContiguousRanges.Count, startTime.ElapsedMilliseconds.ToString("N0") });

            return consolidatedRange;
        }

        /// <summary>
        /// Formats a the borders of a range.
        /// </summary>
        /// <param name="rng">Excel.Range to format.</param>
        /// <param name="borders">Width of the border.</param>
        private void FormatBorders(Microsoft.Office.Interop.Excel.Range rng, int borders)
        {
            Border border = new Border(borders);

            if (border.IsAll() == false)
            {
                rng.Borders[XlBordersIndex.xlInsideHorizontal].LineStyle = Constants.xlNone;
                rng.Borders[XlBordersIndex.xlInsideVertical].LineStyle = Constants.xlNone;
            }

            if (border.IsAll() || border.IsLeft())
            {
                rng.Borders[XlBordersIndex.xlEdgeLeft].LineStyle = XlLineStyle.xlContinuous;
                rng.Borders[XlBordersIndex.xlEdgeLeft].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                rng.Borders[XlBordersIndex.xlEdgeLeft].Weight = XlBorderWeight.xlThin;
            }

            if (border.IsAll() || border.IsRight())
            {
                rng.Borders[XlBordersIndex.xlEdgeRight].LineStyle = XlLineStyle.xlContinuous;
                rng.Borders[XlBordersIndex.xlEdgeRight].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                rng.Borders[XlBordersIndex.xlEdgeRight].Weight = XlBorderWeight.xlThin;
            }

            if (border.IsAll() || border.IsTop())
            {
                rng.Borders[XlBordersIndex.xlEdgeTop].LineStyle = XlLineStyle.xlContinuous;
                rng.Borders[XlBordersIndex.xlEdgeTop].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                rng.Borders[XlBordersIndex.xlEdgeTop].Weight = XlBorderWeight.xlThin;
            }

            if (border.IsAll() || border.IsBottom())
            {
                rng.Borders[XlBordersIndex.xlEdgeBottom].LineStyle = XlLineStyle.xlContinuous;
                rng.Borders[XlBordersIndex.xlEdgeBottom].ColorIndex = XlColorIndex.xlColorIndexAutomatic;
                rng.Borders[XlBordersIndex.xlEdgeBottom].Weight = XlBorderWeight.xlThin;
            }
        }


        /// <summary>
        /// Deletes all the cell notes in a range.
        /// </summary>
        /// <param name="contigRng">Range to delete the cell note in.</param>
        public void DeleteAllCellNotes(ContiguousRange contigRng)
        {
            if (contigRng != null)
            {
                Microsoft.Office.Interop.Excel.Range rng = GetRange(contigRng, _sheet);
                rng.ClearComments();
            }
        }

        /// <summary>
        /// Deletes a cell note in a range.
        /// </summary>
        /// <param name="range">Range to delete the cell note in.</param>
        public void DeleteCellNote(Range range)
        {
            if (range != null)
            {
                Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);
                if (rng.Comment != null)
                {
                    rng.Comment.Delete();
                }
            }
        }

        /// <summary>
        /// Deletes a list of cell note in a range.
        /// </summary>
        /// <param name="range">list of range to delete cell notes.</param>
        public void DeleteCellNotes(List<Range> range)
        {
            if (range != null && range.Count > 0)
            {
                foreach (Range r in range)
                {
                    DeleteCellNote(r);
                }
            }
        }
        
        /// <summary>
        /// Checks a range to see if it contains a cell note.
        /// </summary>
        /// <param name="range">Range to check.</param>
        /// <returns>true if the range contains a cell note, false if not or if the range is not valid.</returns>
        public bool ContainsCellNote(Range range)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

            if (rng != null && rng.Comment != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Modifies a the text in a cell note.
        /// </summary>
        /// <param name="range">Range to set the cell note in.</param>
        /// <param name="text">The new text for the cell note.</param>
        /// <param name="overwriteExistingText">Overwrites the existings text in the cell note.</param> 
        public void ModifyCellNote(Range range, string text, bool overwriteExistingText)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

            if (rng.Comment != null)
            {
                string commentText = rng.Comment.Text(Type.Missing, Type.Missing, Type.Missing);

                if(!commentText.Equals(text))
                {
                    if (overwriteExistingText)
                    {
                        rng.Comment.Text(String.Empty, Type.Missing, Type.Missing);
                    }
                    rng.Comment.Text(text, Type.Missing, Type.Missing);

                    AutoResizeTextBox(rng.Comment);
                }
            }
        }

        /// <summary>
        /// Sets a cell note in a range.
        /// </summary>
        /// <param name="range">Range to set the cell note in.</param>
        /// <param name="cellNote">Cell note to set in the range.</param>
        /// <param name="deleteExisingNote">Delete the existing cell note before inserting the new cell note.</param>
        /// <param name="boldFont">Bold the font.</param>
        public void SetCellNote(Range range, simpleCellNote cellNote, bool deleteExisingNote, bool boldFont)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);

            if (rng.Comment == null)
            {
                Comment comment = rng.AddComment(cellNote.text);
                comment.Visible = cellNote.visible;
   
                if (comment.Shape.DrawingObject != null)
                {
                    TextBox textBox = (TextBox) comment.Shape.DrawingObject;
                    if (textBox != null)
                    {
                        if (boldFont && textBox.Font != null)
                        {
                            textBox.Font.Bold = false;
                        }
                        AutoResizeTextBox(comment);
                        //if the cell note text is empty, then remove the "user name" text.
                        if(String.IsNullOrEmpty(cellNote.text))
                        {
                            textBox.Text = String.Empty;
                        }
                    }
                }
            }
            else if (rng.Comment != null && deleteExisingNote)
            {
                rng.Comment.Delete();
                SetCellNote(range, cellNote, false, boldFont);
            }
            else if (rng.Comment != null && !deleteExisingNote)
            {
                if (!cellNote.text.ToLower().Equals(rng.Comment.Text(Type.Missing, Type.Missing, Type.Missing).ToLower()))
                {
                    rng.Comment.Text(cellNote.text, Type.Missing, Type.Missing);
                    AutoResizeTextBox(rng.Comment);
                }
            }
        }

        /// <summary>
        /// Automatically resize the text box(comment).
        /// </summary>
        /// <param name="comment">Comment.</param>
        private static void AutoResizeTextBox(Comment comment)
        {
            try
            {
                if (comment.Shape.DrawingObject != null)
                {
                    TextBox textBox = (TextBox)comment.Shape.DrawingObject;
                    string text = comment.Text(Type.Missing, Type.Missing, Type.Missing);
                    if (textBox != null)
                    {
                        ControlUtilities c = new ControlUtilities();
                        Size s = c.GetTextBoxSize(
                            text,
                            textBox.Left,
                            textBox.Top,
                            textBox.Height,
                            textBox.Font.Name.ToString(),
                            float.Parse(textBox.Font.Size.ToString()));
                        textBox.Width = s.Width;
                        textBox.Height = s.Height;
                    }
                }
            }
            catch (Exception e)
            {
               PafApp.GetLogger().WarnFormat("Error pasting cell notes:  {0}.  Skipping.", new object[]{e.Message});
            }
        }

        /// <summary>
        /// Sets a list of cell notes.
        /// </summary>
        /// <param name="ranges">List of ranges to set the cell notes in.</param>
        /// <param name="cellNotes">List of cell notes to set in the ranges.</param>
        /// <param name="deleteExisingNote">Delete the existing cell note before inserting the new cell note.</param>
        /// <param name="boldFont">Bold the font.</param>
        public void SetCellNotes(List<Range> ranges, List<simpleCellNote> cellNotes, bool deleteExisingNote, bool boldFont)
        {
            if(ranges.Count != cellNotes.Count)
            {
                throw new ArgumentException("List sizes do not match.");
            }

            for(int i = 0 ; i < ranges.Count; i++)
            {
                SetCellNote(ranges[i], cellNotes[i], deleteExisingNote, boldFont);
            }
        }

        /// <summary>
        /// Sets cell notes in a range.
        /// </summary>
        /// <param name="cellNotes">The dictionary of address/cell notes.</param>
        /// <param name="deleteExisingNote">Delete the existing cell note before inserting the new cell note.</param>
        /// <param name="boldFont">Bold the font.</param>
        public void SetCellNotes(Dictionary<Range, simpleCellNote> cellNotes, bool deleteExisingNote, bool boldFont)
        {
            if (cellNotes != null && cellNotes.Count > 0)
            {
                foreach (KeyValuePair<Range, simpleCellNote> kvp in cellNotes)
                {
                    SetCellNote(kvp.Key, kvp.Value, deleteExisingNote, boldFont);
                }
            }
        }


        /// <summary>
        /// Gets a cell note.
        /// </summary>
        /// <param name="range">Range to obtain the cell note from.</param>
        /// <returns>a cell note if one was found, null if no note was found.</returns>
        public simpleCellNote GetCellNote(Range range)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);
            
            if(rng != null)
            {
                if(rng.Comment != null)
                {
                    simpleCellNote scn = new simpleCellNote();
                    scn.creator = null;
                    scn.text = rng.Comment.Text(Type.Missing, Type.Missing, Type.Missing);
                    scn.visible = rng.Comment.Visible;
                    return scn;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the cell note list.
        /// </summary>
        /// <returns>The entire list of cell notes for the grid.</returns>
        public Dictionary<CellAddress, simpleCellNote> GetCellNotes()
        {
            Dictionary<CellAddress, simpleCellNote> cns = new Dictionary<CellAddress, simpleCellNote>();
            foreach(Comment c in _sheet.Comments)
            {
                Microsoft.Office.Interop.Excel.Range parent = (Microsoft.Office.Interop.Excel.Range) c.Parent;
                
                simpleCellNote scn = new simpleCellNote();
                scn.creator = null;
                scn.text = c.Text(Type.Missing, Type.Missing, Type.Missing);
                scn.visible = c.Visible;
                
                cns.Add(new CellAddress(parent.Row, parent.Column), scn);
            }
            return cns;
        }

        /// <summary>
        /// Sets a hyperlink in a range.
        /// </summary>
        /// <param name="range">Range to place the hyperlink.</param>
        /// <param name="address">Hyperlink address.</param>
        /// <param name="subAddress">Hyperlink subaddress.</param>
        /// <param name="screenTip">Hyperlink screen tip.</param>
        /// <param name="textToDisplay">Hyperlink text to disply in liew of the hyperlink.</param>
        public void SetHyperlinks(Range range, string address, string subAddress,
            string screenTip, string textToDisplay)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);
            if(rng != null)
            {
                rng.Hyperlinks.Add(rng, address, subAddress, screenTip, textToDisplay);
            }
        }

        /// <summary>
        /// Deletes a hyperlink from a range of cells.
        /// </summary>
        /// <param name="range">Range to delete the hyperlink from.</param>
        public void DeleteHyperlink(Range range)
        {
            Microsoft.Office.Interop.Excel.Range rng = GetRange(range, _sheet);
            if(rng != null)
            {
                if(rng.Hyperlinks != null)
                {
                    rng.Hyperlinks.Delete();
                }
            }
        }

        /// <summary>
        /// Clears all the hyperlinks off of a sheet.
        /// </summary>
        public void ClearHyperlinks()
        {
            if (_sheet != null)
            {
                if (_sheet.Hyperlinks != null)
                {
                    _sheet.Hyperlinks.Delete();
                }
            }
        }

        /// <summary>
        /// Gets an array of hyperlinks for an entire grid.
        /// </summary>
        /// <returns>An array of hyperlinks.</returns>
        public Hyperlinks GetHyperlinks()
        {
            if (_sheet != null)
            {
                if (_sheet.Hyperlinks != null)
                {
                    return _sheet.Hyperlinks;
                }
            }
            return null;
        }
    }
}