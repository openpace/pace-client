#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
namespace Titan.Pace.ExcelGridView.Utility
{
    class Border
    {
        private static int BORDER_NONE = 0;

        private static int BORDER_LEFT = 2;

        private static int BORDER_RIGHT = 4;

        private static int BORDER_TOP = 8;

        private static int BORDER_BOTTOM = 16;

        private static int BORDER_ALL = 32;

	    private int border;

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="border">Size of the border.</param>
        public Border(int border)
        {
            this.border = border;
        }
    	
        /// <summary>
        /// Checks if the styles match.
        /// </summary>
        /// <param name="style">Style to match.</param>
        /// <returns>true if the styles match, false if they dont.</returns>
	    private bool IsStyle(int style)
        {
    		
		    return ((border & style) > 0 )? true : false;
	    }

        /// <summary>
        /// Tests to see if no borders exist
        /// </summary>
        /// <returns>true if the style if none, false if not.</returns>
        public bool IsNone()
        {
            return IsStyle(BORDER_NONE);
        }
    	
        /// <summary>
        /// Tests to see if the border a left border.
        /// </summary>
        /// <returns>true if the style if left, false if not.</returns>
	    public bool IsLeft() 
        {
		    return IsStyle(BORDER_LEFT);
	    }
    	
        /// <summary>
        /// Tests to see if the border is a right border.
        /// </summary>
        /// <returns>true if the style if right, false if not.</returns>
	    public bool IsRight() 
        {
		    return IsStyle(BORDER_RIGHT);
	    }
    	
        /// <summary>
        /// Tests to see if the border is a top border.
        /// </summary>
        /// <returns>true if the style if top, false if not.</returns>
	    public bool IsTop() 
        {
		    return IsStyle(BORDER_TOP);
	    }
    	
        /// <summary>
        /// Tests to see if the border is a bottom border.
        /// </summary>
        /// <returns>true if the style if bottom, false if not.</returns>
	    public bool IsBottom() 
        {
		    return IsStyle(BORDER_BOTTOM);
	    }
    	
        /// <summary>
        /// Tests to see if the border is all around.
        /// </summary>
        /// <returns>true if the border is all, false if not.</returns>
	    public bool IsAll() 
        {
		    return IsStyle(BORDER_ALL);
	    }
    }
}
