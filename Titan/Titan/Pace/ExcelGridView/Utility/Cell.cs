﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using System.Drawing;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView.Utility
{
    /// <summary>
    /// Summary description for CellAddress.  A grid cell.
    /// </summary>
    [Serializable]
    public class Cell : IEquatable<Cell>, IComparable<Cell>
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        [DebuggerHidden]
        public Cell(int row, int col)
        {
            CellAddress = new CellAddress(row, col);
        }

        /// <summary>
        /// Overloaded Constructor.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <param name="value">Value of the cell.</param>
        [DebuggerHidden]
        public Cell(int row, int col, object value)
        {
            CellAddress = new CellAddress(row, col);
            Value = value;
        }

        /// <summary>
        /// Gets the CellAddress of the cell.
        /// </summary>
        public CellAddress CellAddress { get; set; }

        /// <summary>
        /// Get/set the back color of the cell.
        /// </summary>
        public Color BackColor { get; set; }

        /// <summary>
        /// Get/set the value of the cell.
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Get/set the font color of the cell.
        /// </summary>
        public Color FontColor { get; set; }

        /// <summary>
        /// Implementation of "IEquatable".  
        /// </summary>
        /// <remarks>
        /// Without this implementation, when you use CellAddress as a key in a hash structure,
        /// the comparsion would always return false because of the way the default comparsion works.
        /// </remarks>
        /// <param name="cell">The cell that you want to compare.</param>
        /// <returns>True if the objects are equal, false if they are not.</returns>
        public bool Equals(Cell cell)
        {
            bool result =  CellAddress.Equals(cell.CellAddress);
            if(result && Value != null)
            {
                return Value.Equals(cell.Value);
            }
            return result;
        }

        /// <summary>
        /// Implementation of "IComparable"
        /// </summary>
        /// <remarks>
        /// Without the implementation, the cells are not sorted correctly in a SortedList or SortDictionary.
        /// </remarks>
        /// <param name="cell">The cell to compare to the instance.</param>
        /// <returns>0 if equal, -1 if the instance is less than the cell, 1 if the instance
        /// is greater than the cell.</returns>
        public int CompareTo(Cell cell)
        {
            return CellAddress.CompareTo(cell.CellAddress);
        }
    }
}
