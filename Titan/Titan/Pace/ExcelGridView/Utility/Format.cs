#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.ExcelGridView.Utility
{
    /// <summary>
    /// 
    /// </summary>
    /// <remarks>
    /// Strings are used because they can always be nullable.  Null values represent properties
    /// that are not set in the XML and therefore are not formatted in the UI.
    /// </remarks>
    [Serializable]
    public class Format : IEquatable<Format>
    {
        string _Alignment;
        string _BgHexFillColor;
        string _Bold;
        string _DoubleUnderline;
        string _FontHexColor;
        string _FontName;
        string _Italics;
        double _Size = 0;
        string _StrikeOut;
        string _UnderLine;
        string _Plannable;
        string _NumberFormat;
        int _Borders;
        
        /// <summary>
        /// Default Constructor
        /// </summary>
        public Format()
        {

        }

        /// <summary>
        /// Overloaded Constructor to fill the object properties.
        /// </summary>
        /// <param name="alignment">Font alignment.</param>
        /// <param name="bgHexFillColor">Background fill color.</param>
        /// <param name="bold">Font bold status.</param>
        /// <param name="doubleUnderline">Font double underline.</param>
        /// <param name="fontHexColor">Font color.</param>
        /// <param name="fontName">Font name.</param>
        /// <param name="italics">Font italics status.</param>
        /// <param name="size">Font size.</param>
        /// <param name="strikeOut">Font strikeout.</param>
        /// <param name="underLine">Font underline.</param>
        /// <param name="plannable">Format plannable status (locked).</param>
        /// <param name="numberFormat">Format number status.</param>
        public Format(string alignment, string bgHexFillColor, string bold, string doubleUnderline, string fontHexColor,
            string fontName, string italics, double size, string strikeOut, string underLine, string plannable,
            string numberFormat)
        {
            _Alignment = alignment;
            _BgHexFillColor = bgHexFillColor;
            _Bold = bold;
            _DoubleUnderline = doubleUnderline;
            _FontHexColor = fontHexColor;
            _FontName = fontName;
            _Italics = italics;
            _Size = size;
            _StrikeOut = strikeOut;
            _UnderLine = underLine;
            _Plannable = plannable;
            _NumberFormat = numberFormat;
        }

        /// <summary>
        /// Get/set font alignment.
        /// </summary>
        public string Alignment
        {
            get
            {
                return _Alignment;
            }

            set
            {
                _Alignment = value;
            }

        }

        /// <summary>
        /// Get/set Background fill color.
        /// </summary>
        public string BgHexFillColor
        {
            get
            {
                return _BgHexFillColor;
            }

            set
            {
                _BgHexFillColor = value;
            }

        }

        /// <summary>
        /// Get/set Bold status.
        /// </summary>
        public string Bold
        {
            get
            {
                return _Bold;
            }

            set
            {
                _Bold = value;
            }

        }

        /// <summary>
        /// Get/set Double underline.
        /// </summary>
        public string DoubleUnderline
        {
            get
            {
                return _DoubleUnderline;
            }

            set
            {
                _DoubleUnderline = value;
            }

        }

        /// <summary>
        /// Get/set Font color.
        /// </summary>
        public string FontHexColor
        {
            get
            {
                return _FontHexColor;
            }

            set
            {
                _FontHexColor = value;
            }

        }

        /// <summary>
        /// Get/set Font name.
        /// </summary>
        public string FontName
        {
            get
            {
                return _FontName;
            }

            set
            {
                _FontName = value;
            }

        }

        /// <summary>
        /// Get/set Italics status.
        /// </summary>
        public string Italics
        {
            get
            {
                return _Italics;
            }

            set
            {
                _Italics = value;
            }

        }

        /// <summary>
        /// Get/set Font size.
        /// </summary>
        public double Size
        {
            get
            {
                return _Size;
            }

            set
            {
                _Size = value;
            }

        }

        /// <summary>
        /// Get/set Font strike out.
        /// </summary>
        public string StrikeOut
        {
            get
            {
                return _StrikeOut;
            }

            set
            {
                _StrikeOut = value;
            }

        }

        /// <summary>
        /// Get/set font underline.
        /// </summary>
        public string UnderLine
        {
            get
            {
                return _UnderLine;
            }

            set
            {
                _UnderLine = value;
            }

        }

        /// <summary>
        /// Get/set plannable (locked) status.
        /// </summary>
        public string Plannable
        {
            get
            {
                return _Plannable;
            }

            set
            {
                _Plannable = value;
            }
        }

        /// <summary>
        /// Get/set the number format.
        /// </summary>
        public string NumberFormat
        {
            get
            {
                return _NumberFormat;
            }

            set
            {
                _NumberFormat = value;
            }
        }

        /// <summary>
        /// Get/set the border.
        /// </summary>
        public int Borders
        {
            get
            {
                return _Borders;
            }

            set
            {
                _Borders = value;
            }
        }

        /// <summary>
        /// Copies all the non null values from the format (overwrites all values).
        /// </summary>
        /// <param name="format">The format to copy from.</param>
        public void CopyAll(Format format)
        {
            if (format == null)
            {
                return;
            }

            if (!String.IsNullOrEmpty(format.Alignment))
            {
                _Alignment = format.Alignment;
            }
            if (!String.IsNullOrEmpty(format.BgHexFillColor))
            {
                _BgHexFillColor = format.BgHexFillColor;
            }
            if (!String.IsNullOrEmpty(format.Bold))
            {
                _Bold = format.Bold;
            }
            if (format.Borders > 0)
            {
                _Borders = format.Borders;
            }
            if (!String.IsNullOrEmpty(format.DoubleUnderline))
            {
                _DoubleUnderline = format.DoubleUnderline;
            }
            if (!String.IsNullOrEmpty(format.FontHexColor))
            {
                _FontHexColor = format.FontHexColor;
            }
            if (!String.IsNullOrEmpty(format.FontName))
            {
                _FontName = format.FontName;
            }
            if (!String.IsNullOrEmpty(format.Italics))
            {
                _Italics = format.Italics;
            }
            if (!String.IsNullOrEmpty(format.NumberFormat))
            {
                _NumberFormat = format.NumberFormat;
            }
            if (!String.IsNullOrEmpty(format.Plannable))
            {
                _Plannable = format.Plannable;
            }
            if (format.Size > 0)
            {
                _Size = format.Size;
            }
            if (!String.IsNullOrEmpty(format.StrikeOut))
            {
                _StrikeOut = format.StrikeOut;
            }
            if (!String.IsNullOrEmpty(format.UnderLine))
            {
                _UnderLine = format.UnderLine;
            }
        }


        /// <summary>
        /// Copies the values of the format(does not overwrite existing values).
        /// </summary>
        /// <param name="format">The format to copy from.</param>
        public void Copy(Format format)
        {
            if (format == null)
            {
                return;
            }

            if (!String.IsNullOrEmpty(format.Alignment) && String.IsNullOrEmpty(_Alignment))
            {
                _Alignment = format.Alignment;
            }

            if (!String.IsNullOrEmpty(format.BgHexFillColor) && String.IsNullOrEmpty(_BgHexFillColor))
            {
                _BgHexFillColor = format.BgHexFillColor;
            }

            if (!String.IsNullOrEmpty(format.Bold) && String.IsNullOrEmpty(_Bold))
            {
                _Bold = format.Bold;
            }

            if (format.Borders > 0 && _Borders == 0)
            {
                _Borders = format.Borders;
            }

            if (!String.IsNullOrEmpty(format.DoubleUnderline) && String.IsNullOrEmpty(_DoubleUnderline))
            {
                _DoubleUnderline = format.DoubleUnderline;
            }

            if (!String.IsNullOrEmpty(format.FontHexColor) && String.IsNullOrEmpty(_FontHexColor))
            {
                _FontHexColor = format.FontHexColor;
            }

            if (!String.IsNullOrEmpty(format.FontName) && String.IsNullOrEmpty(_FontName))
            {
                _FontName = format.FontName;
            }

            if (!String.IsNullOrEmpty(format.Italics) && String.IsNullOrEmpty(_Italics))
            {
                _Italics = format.Italics;
            }

            if (!String.IsNullOrEmpty(format.NumberFormat) && String.IsNullOrEmpty(_NumberFormat))
            {
                _NumberFormat = format.NumberFormat;
            }

            if (!String.IsNullOrEmpty(format.Plannable) && String.IsNullOrEmpty(_Plannable))
            {
                _Plannable = format.Plannable;
            }

            if (format.Size > 0 && _Size == 0)
            {
                _Size = format.Size;
            }

            if (!String.IsNullOrEmpty(format.StrikeOut) && String.IsNullOrEmpty(_StrikeOut))
            {
                _StrikeOut = format.StrikeOut;
            }

            if (!String.IsNullOrEmpty(format.UnderLine) && String.IsNullOrEmpty(_UnderLine))
            {
                _UnderLine = format.UnderLine;
            }
        }

        /// <summary>
        /// Implementation of "IEquatable".  
        /// </summary>
        /// <remarks>
        /// Without this implementation, when you use Format as a key in a hash structure,
        /// the comparsion would always return false because of the way the default comparsion works.
        /// </remarks>
        /// <param name="format">The format object you wish to compare.</param>
        /// <returns>True if the objects are equal, false if they are not.</returns>
        public bool Equals(Format format)
        {
            //if (_Alignment.Equals(format.Alignment) && _BgFillColor.Equals(format.BgFillColor) &&
            //    _Bold.Equals(format.Bold) && _DoubleUnderline.Equals(format.DoubleUnderline) &&
            //    _FontColor.Equals(format.FontColor) && _FontName.Equals(format.FontName) &&
            //    _Italics.Equals(format.Italics) && _Size.Equals(format.Size) &&
            //    _StrikeOut.Equals(format.StrikeOut) && _UnderLine.Equals(format.UnderLine) &&
            //    _Plannable.Equals(format.Plannable) && _NumberFormat.Equals(format.NumberFormat))
            if (ToString().Equals(format.ToString()))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Overloaded shallow clone.
        /// </summary>
        /// <returns>A shallow copied format.</returns>
        public Format Clone()
        {
            return (Format) MemberwiseClone();
        }

        /// <summary>
        /// Gets the hash code for the object.
        /// </summary>
        /// <returns>Returns the hash code for the object.</returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        /// <summary>
        /// Returns a System.String representation of the CellAddress.
        /// </summary>
        /// <returns>Returns a System.String representation of the CellAddress.</returns>
        public override string ToString()
        {
            string[] str = new string []{ _Alignment, _BgHexFillColor, _Bold, _DoubleUnderline, _FontHexColor,
                _FontName, _Italics, _Size.ToString(), _StrikeOut, _UnderLine, _Plannable, _NumberFormat};

            return String.Format("({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11})", str);
        }
    }
}