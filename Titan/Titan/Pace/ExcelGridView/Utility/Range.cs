#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Pace.Application.Core;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView.Utility
{
	/// <summary>
	/// Summary description for Range.
	/// </summary>
    [Serializable]
    public class Range : IShallowCloneable<Range>, IEquatable<Range>
	{
        private readonly List<ContiguousRange> _ContiguousRanges = new List<ContiguousRange>();

        /// <summary>
        /// Are the ContiguousRange in the Range already sorted.
        /// </summary>
        public bool Sorted { set; get; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="cell">Cell to add to the range.</param>
        public Range(CellAddress cell):this()
		{
            _ContiguousRanges.Add(new ContiguousRange(cell, cell.DeepClone()));
		}

        /// <summary>
        /// Default constructor.
        /// </summary>
        public Range()
        {
            Sorted = false;
        }

        /// <summary>
        /// Creates a range from a list of ranges
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        public Range(IEnumerable<Range> ranges)
        {
            List<ContiguousRange> contiguousRanges = new List<ContiguousRange>(100);
            foreach (var r in ranges)
            {
                contiguousRanges.AddRange(r.ContiguousRanges);
            }
            contiguousRanges.Sort();

            AddContiguousRanges(contiguousRanges);
            Sorted = true;
        }
        
        /// <summary>
        /// Adds a new cell to the list of ranges.
        /// </summary>
        /// <param name="cell">Address of the cell.</param>
        public void AddContiguousRange(CellAddress cell)
        {
            CombineOrAddContiguousRange(cell.ToContiguousRange());
        }

        /// <summary>
        /// Adds a list of ContiguousRanges
        /// </summary>
        /// <param name="contiguousRanges"></param>
        public void AddContiguousRanges(List<ContiguousRange> contiguousRanges)
	    {
            foreach (ContiguousRange cr in contiguousRanges)
            {
                CombineOrAddContiguousRange(cr);
            }
	    }

	    /// <summary>
        /// Adds a new ContigouousRange to the list of ranges.
        /// </summary>
        /// <param name="contigRange"></param>
        public void AddContiguousRange(ContiguousRange contigRange)
        {
            //_ContiguousRanges.Add(ContigRange);
            CombineOrAddContiguousRange(contigRange);
        }

        /// <summary>
        /// Combines a ContiguousRange with a neighbor range (if one exists) or just adds the ContiguousRange.
        /// </summary>
        /// <param name="contigRange"></param>
        /// <remarks>TTN-2098 Changed the loop to process backwards.  Since most ranges are added in order, 
        /// this should improve performance.</remarks>
        public void CombineOrAddContiguousRange(ContiguousRange contigRange)
        {
            bool added = false;
            //foreach (ContiguousRange cr in _ContiguousRanges)
            for (int i = _ContiguousRanges.Count - 1; i >= 0; i--)
            {
                //if (cr.CombineRange(contigRange))
                if(_ContiguousRanges[i].CombineRange(contigRange))
                {
                    added = true;
                    break;
                }
            }
            if (!added) _ContiguousRanges.Add(contigRange);
        }

        /// <summary>
        /// Gets a typed listed of ContiguousRanges
        /// </summary>
        public List<ContiguousRange> ContiguousRanges
	    {
            get
            {
                return _ContiguousRanges.ToList();
            }
	    }

        /// <summary>
        /// Gets the count of cell for the range.
        /// </summary>
        public int CellCount
        {
            get
            {
                return ContiguousRanges.Sum(x => x.Cells.Count);
            }
        }

        /// <summary>
        /// Get a shallow copy of the Range.
        /// </summary>
        /// <returns>A shallow copy of the Range.</returns>
        public Range Clone()
        {
            return (Range) MemberwiseClone();
        }

        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="temp">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public bool Equals(Range temp)
        {
            if (temp.ContiguousRanges.Count != ContiguousRanges.Count)
            {
                return false;
            }
            else
            {
                for (int i = 0; i < temp.ContiguousRanges.Count; i++)
                {
                    if (! temp.ContiguousRanges[i].Equals(ContiguousRanges[i]))
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Return the hashcode of the object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        public override int GetHashCode()
        {
            return ContiguousRanges.GetHashCode();
        }

	    public override string ToString()
	    {
	        return String.Join(",", _ContiguousRanges);
	    }
	}
}
