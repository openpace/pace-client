﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

namespace Titan.Pace.ExcelGridView.Utility
{
    internal class TupleConditionalFormat : TupleFormat
    {
        public int? GroupIndex { get; set; }
        public LevelGenerationType Type { get; set; }

        public TupleConditionalFormat(TupleFormat format, LevelGenerationType type, int? index) :this(format)
        {
            Type = type;
            GroupIndex = index;
        }

        public TupleConditionalFormat(TupleFormat format)
        {
            Name = format.Name;
            NumberFormat = format.NumberFormat;
            Plannable = format.Plannable;
            HeaderBorders = format.HeaderBorders;
            Borders = format.Borders;
        }

        public TupleConditionalFormat(string name, LevelGenerationType type, int? index):base()
        {
            Name = name;
            GroupIndex = index;
            Type = type;
        }



        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
            {
                return false;
            }

            TupleConditionalFormat tupleFormat = (TupleConditionalFormat)obj;

            return Type.Equals(tupleFormat.Type) && GroupIndex.Equals(tupleFormat.GroupIndex);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() + (GroupIndex.GetHashCode() / 4) + (Type.GetHashCode() / 4);
        }
    }
}
