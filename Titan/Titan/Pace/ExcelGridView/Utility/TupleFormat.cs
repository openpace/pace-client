#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Titan.Pace.Application.Core;

namespace Titan.Pace.ExcelGridView.Utility
{
    /// <summary>
    /// Contains format informat for a view tuple.
    /// </summary>
    internal class TupleFormat : IShallowCloneable<TupleFormat>
    {
        public TupleFormat()
        {
            Name = String.Empty;
            NumberFormat = String.Empty;
            Plannable = String.Empty;
            HeaderBorders = 0;
            Borders = 0;
        }

        /// <summary>
        /// Get/set the name of the tupleformat.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Get/set the plannable status of the tupleformat.
        /// </summary>
        public string Plannable { get; set; }

        /// <summary>
        /// Get/set the numberformat of the tupleformat. 
        /// </summary>
        public string NumberFormat { get; set; }

        /// <summary>
        /// Get/set the borders of the tupleformat.
        /// </summary>
        public int Borders { get; set; }

        /// <summary>
        /// Get/set the header borders.
        /// </summary>
        public int HeaderBorders { get; set; }

        /// <summary>
        /// Overloaded equals method.
        /// </summary>
        /// <param name="obj">Object to compare.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public override bool Equals(Object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            TupleFormat tupleFormat = (TupleFormat) obj;

            if (! Name.Equals(tupleFormat.Name))
            {
                return false;
            }

            if (! Plannable.Equals(tupleFormat.Plannable))
            {
                return false;
            }

            if (! NumberFormat.Equals(tupleFormat.NumberFormat))
            {
                return false;
            }

            if (!Borders.Equals(tupleFormat.Borders))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Overloaded hash code.
        /// </summary>
        /// <returns>Object hash code.</returns>
        public override int GetHashCode()
        {
            return Name.GetHashCode() / 4 + Plannable.GetHashCode() / 4 + NumberFormat.GetHashCode() / 4 + Borders.GetHashCode()/4;
        }

        public TupleFormat Clone()
        {
            return (TupleFormat) MemberwiseClone();
        }
    }
}