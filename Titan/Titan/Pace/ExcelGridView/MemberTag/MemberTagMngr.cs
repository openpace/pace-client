#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Pace.Application.Extensions;
using Titan.Pace.DataStructures;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView.MemberTag
{
    internal class MemberTagMngr
    {
        private readonly MemberTagCache _MemberTagCache;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public MemberTagMngr()
        {
            _MemberTagCache = new MemberTagCache();
        }

        /// <summary>
        /// The cache of changed member tags
        /// </summary>
        public MemberTagCache MbrTagCache
        {
            get
            {
                return _MemberTagCache;
            }
        }

        /// <summary>
        /// Make member tags clean
        /// </summary>
        public void MakeMbrTagsClean()
        {
            PafApp.GetViewMngr().MakeAllMbrTagsClean();
        }

        /// <summary>
        /// Signals if the client cache is dirty and need to be written back to the server.
        /// </summary>
        /// <returns>true if the client cache is dirty, false if not.</returns>
        public bool IsMbrTagCacheDirty()
        {
            if(_MemberTagCache.ClientMemberTagDirtyCache.Count() > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Signals if the client cache is dirty and need to be written back to the server.
        /// </summary>
        /// <returns>true if the client cache is dirty, false if not.</returns>
        public bool IsMbrTagCacheDirty(ICollection<string> memberTagDefs)
        {
            return memberTagDefs.Any(memberTagDef => _MemberTagCache.ClientMemberTagDirtyCache.ContainsKey(memberTagDef));
        }

        /// <summary>
        /// Gets the member tag data to send the data to the server.
        /// </summary>
        /// <param name="viewMngr">The view manager.</param>
        /// <param name="addedMbrTags">Array of added member tags.</param>
        /// <param name="updatedMbrTags">Array of updated member tags.</param>
        /// <param name="delMbrTags">Array of delete member tags.</param>
        public void GetMbrTagData(ViewMngr viewMngr, out simpleMemberTagData[] addedMbrTags,
            out simpleMemberTagData[] updatedMbrTags, out simpleMemberTagData[] delMbrTags)
        {
            List<Intersection> memberTags = new List<Intersection>();
            foreach(KeyValuePair<int, ViewStateInfo> kvp in viewMngr.Views)
            {
                OlapView op = kvp.Value.GetOlapView();
                foreach (Intersection inter in op.MemberTag.MemberTagIntersections)
                {
                    if (!memberTags.Contains(inter))
                    {
                        memberTags.Add(inter);
                    }
                }
            }

            List<simpleMemberTagData> AddedMbrTags = new List<simpleMemberTagData>();
            List<simpleMemberTagData> UpdatedMbrTags = new List<simpleMemberTagData>();
            List<simpleMemberTagData> DelMbrTags = new List<simpleMemberTagData>();

            Dictionary<string, Dictionary<Intersection, string>>.KeyCollection mbrTags = MbrTagCache.ClientMemberTagDirtyCache.Keys;
            foreach (string mbrTag in mbrTags)
            {
                Dictionary<Intersection, string> values = MbrTagCache.GetChangedValues(mbrTag);
                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(mbrTag);
                foreach (KeyValuePair<Intersection, string> kvp in values)
                {
                    simpleMemberTagData tagData = BuildMemberTagDataObject(kvp.Key, mbrTag, tagDef.type, kvp.Value);

                    if (memberTags.Contains(kvp.Key) && string.IsNullOrEmpty(kvp.Value))
                    {
                        DelMbrTags.Add(tagData);
                    }
                    else if (memberTags.Contains(kvp.Key))
                    {
                        UpdatedMbrTags.Add(tagData);
                    }
                    else
                    {
                        AddedMbrTags.Add(tagData);
                        AddMbrTagIntersection(viewMngr, kvp.Key);
                    }
                }
            }
            addedMbrTags = AddedMbrTags.ToArray();
            delMbrTags = DelMbrTags.ToArray();
            updatedMbrTags = UpdatedMbrTags.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewMngr"></param>
        /// <param name="intersection"></param>
        private static void AddMbrTagIntersection(ViewMngr viewMngr, Intersection intersection)
        {
            foreach (KeyValuePair<int, ViewStateInfo> kvp in viewMngr.Views)
            {
                OlapView op = kvp.Value.GetOlapView();
                if (!op.MemberTag.MemberTagIntersections.Contains(intersection))
                {
                    op.MemberTag.MemberTagIntersections.Add(intersection);
                }
            }
        }


        /// <summary>
        /// Populates, Overlays the member tag changes, then updates the bg fill color.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public void PopulateAndOverlayMemberTags(ViewStateInfo currentView)
        {
            if (currentView != null && currentView.GetOlapView() != null)
            {
                if (currentView.DirtyMbrTags)
                {
                    currentView.DirtyMbrTags = false;

                    pafView pafView = PafApp.GetServiceMngr().GetCurrentPafView(currentView.ViewName);

                    if (pafView != null)
                    {
                        currentView.GetOlapView().ClearMbrTagData();

                        currentView.GetOlapView().PopulateMbrTags(pafView.viewSections[0]);

                        OverlayMemberTagChanges(currentView);
                    }
                }
            }
        } 
        
        /// <summary>
        /// Overlay the member tag changes, and update the bg fill color.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public void OverlayMemberTagChanges(ViewStateInfo currentView)
        {
            if (currentView != null && currentView.GetOlapView() != null)
            {
                pafView pafView = PafApp.GetServiceMngr().GetCurrentPafView(currentView.ViewName);

                currentView.GetOlapView().OverlayChangedMemberTags(pafView.viewSections[0]);

                //if (currentView.GetOlapView().MemberTag.ChangedMemberTagCells != null &&
                //    currentView.GetOlapView().MemberTag.ChangedMemberTagCells.Count > 0)
                if (MbrTagCache.IsDirty)
                {
                    SetChangedMemberTagBgColor();
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewMngr"></param>
        public void ResetBgFillColor(ViewMngr viewMngr)
        {
            Dictionary<string, Dictionary<Intersection, string>>.KeyCollection mbrTags = MbrTagCache.ClientMemberTagDirtyCache.Keys;
            
            foreach (string mbrTag in mbrTags)
            {
                //get the changed values for that member tag.
                Dictionary<Intersection, string> values = MbrTagCache.GetChangedValues(mbrTag);
                //get the member tag def.
                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(mbrTag);
                //for each change.
                foreach (KeyValuePair<Intersection, string> kvp in values)
                {
                    //for each open view.
                    foreach (KeyValuePair<int, ViewStateInfo> kv in viewMngr.Views)
                    {
                        //look for a list of cells.
                        List<CellAddress> cells = kv.Value.GetOlapView().MemberTag.FindMemberTagIntersectionAddress(kvp.Key, tagDef.name);
                        //if we have any cells, then set them as dirty.
                        if (cells != null && cells.Count > 0)
                        {
                            kv.Value.DirtyMbrTagFormatting = true;
                            //kv.Value.MbrTagInterToFormat.Add(kvp.Key, tagDef.name);
                            kv.Value.MbrTagInterToFormat.CacheAddSelections(tagDef.name, tagDef.name, kvp.Key, false);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Sets the bg fill color for the modified member tag cells.
        /// </summary>
        public void SetChangedMemberTagBgColor()
        {
            PafApp.GetViewMngr().RestoreMemberTagChangedCellFormatting();
        }

        /// <summary>
        /// Builds a member tag data object.
        /// </summary>
        /// <param name="inter">intersection.</param>
        /// <param name="mbrTagName">Name of the member tag.</param>
        /// <param name="mbrTagType">Type of the member tag cell.</param>
        /// <param name="data">Member tag data</param>
        /// <returns>A simpleMemberTagData object.</returns>
        private static simpleMemberTagData BuildMemberTagDataObject(Intersection inter, string mbrTagName,
            memberTagType mbrTagType, string data)
        {
            simpleMemberTagData smtd = new simpleMemberTagData();

            smtd.applicationName = PafApp.ApplicationId;

            smtd.compressed = true;

            smtd.creator = PafApp.GetGridApp().LogonInformation.UserName.ToLower();

            smtd.data = data;

            //smtd.id = "";

            smtd.lastUpdated = DateTime.Now;

            smtd.lastUpdatedSpecified = true;

            smtd.memberTagName = mbrTagName;

            smtd.memberTagType = PafApp.GetMemberTagInfo().ConvertMemberTagType(mbrTagType);


            PafSimpleCoordList pscl = new PafSimpleCoordList(inter.ToSimpleCoordList(), true);
            smtd.simpleCoordList = pscl.GetSimpleCoordList;

            return smtd;
        }
    }
}