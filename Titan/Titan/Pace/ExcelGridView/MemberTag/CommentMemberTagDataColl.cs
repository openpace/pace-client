#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Reflection;

namespace Titan.Pace.ExcelGridView.MemberTag
{
    internal class CommentMemberTagDataColl : ICollection<CommentMemberTagData>
    {
        private readonly List<CommentMemberTagData> data = new List<CommentMemberTagData>();
        private readonly Version _Version;

        #region ICollection<CommentMemberTagData> Members

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CommentMemberTagDataColl(IEnumerable<CommentMemberTagData> memberTagData)
        {
            data.AddRange(memberTagData);
            _Version = Assembly.GetExecutingAssembly().GetName().Version;
        }

        /// <summary>
        /// Gets an item.
        /// </summary>
        /// <param name="index">Index of the item to return.</param>
        /// <returns></returns>
        public CommentMemberTagData this[int index]
        {
            get { return data[index]; }
        }

        /// <summary>
        /// Version.
        /// </summary>
        public Version Version
        {
            get { return _Version; }
        }

        /// <summary>
        /// Does not add duplicate items.
        /// </summary>
        /// <param name="item"></param>
        public void Add(CommentMemberTagData item)
        {
            if (!data.Contains(item))
            {
                data.Add(item);
            }
        }

        /// <summary>
        /// Adds a group of elements to the end of the list. 
        /// </summary>
        /// <param name="items">The group of elements to add.</param>
        public void AddRange(List<CommentMemberTagData> items)
        {
            if (items != null && items.Count > 0)
            {
                foreach (CommentMemberTagData test in items)
                {
                    if (!data.Contains(test))
                    {
                        data.Add(test);
                    }
                }
            }
        }

        public void Clear()
        {
            data.Clear();
        }

        public bool Contains(CommentMemberTagData item)
        {
            return data.Contains(item);
        }

        public void CopyTo(CommentMemberTagData[] array, int arrayIndex)
        {
            data.CopyTo(array, arrayIndex);
        }

        public int Count
        {
            get { return data.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public bool Remove(CommentMemberTagData item)
        {
            return data.Remove(item);
        }

        public void Sort()
        {
            data.Sort();
        }

        public CommentMemberTagData FindMemberTagId(string id)
        {
            return Find(id);
        }

        public CommentMemberTagData FindMemberTagValue(string value)
        {
            return FindValue(value);
        }

        public CommentMemberTagData[] ToArray()
        {
            return data.ToArray();
        }

        #endregion

        #region IEnumerable<CommentMemberTagData> Members

        public IEnumerator<CommentMemberTagData> GetEnumerator()
        {
            return data.GetEnumerator();
        }

        #endregion

        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return data.GetEnumerator();
        }

        #endregion

        public CommentMemberTagData Find(string id)
        {
            lock (data)
            {
                // Find will enumerate the list and eval the predicate for each 
                //item and return first item that returns true.

                return data.Find(delegate(CommentMemberTagData d)
             {
                 return d.MemberTagId == id;
             });
            }
        }

        public CommentMemberTagData FindValue(string value)
        {
            lock (data)
            {
                return data.Find(delegate(CommentMemberTagData d)
             {
                 return d.MemberTagValue == value;
             });
            }
        }
    }
}
