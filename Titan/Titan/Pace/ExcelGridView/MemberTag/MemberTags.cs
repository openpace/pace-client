#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Titan.Pace.Application.Extensions;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView.MemberTag
{
    internal class MemberTags
    {
        /// <summary>
        /// The parent olap view.
        /// </summary>
        private OlapView _Parent;

        /// <summary>
        /// Only non blank member tag intersections sent from the server.
        /// MemberTagName, Intersection, CellAddress.
        /// </summary>
        private List<Intersection> _MemberTagIntersections = null;

        /// <summary>
        /// All member tag intersections.
        /// </summary>
        private CacheSelectionsList<string,Intersection, CellAddress> _AllMemberTagIntersections = null;

        /// <summary>
        /// Row tuple border values
        /// CellAddress, cell string value
        /// Only used for initially rendering the boarder(s).
        /// </summary>
        private Dictionary<CellAddress, string> _ColTupleBorderValues = null;

        /// <summary>
        /// Column tuple border values
        /// CellAddress, cell string value
        /// Only used for initially rendering the boarder(s).
        /// </summary>
        private Dictionary<CellAddress, string> _RowTupleBorderValues = null;

        /// <summary>
        /// MemberTagName, Intersection, CellAddress.
        /// </summary>
        private CacheSelectionsList<string, Intersection, CellAddress> _CommentIntersections = null;

        /// <summary>
        /// MamberTagName, Col/Row Pos, range.
        /// </summary>
        private CacheSelectionsList<string, int, ContiguousRange> _MemberTagRanges = null;

        /// <summary>
        /// Position of the member tag, name of the member tag.
        /// </summary>
        private Dictionary<int, string> _RowMbrTagBlanks = null;
        
        /// <summary>
        /// Position of the member tag, name of the member tag.
        /// </summary>
        private Dictionary<int, string> _ColMbrTagBlanks = null;

        /// <summary>
        /// member tag id, member name, object value
        /// </summary>
        private CacheSelectionsList<string, Intersection, CommentMemberTagData> _RowMemberTagCommentValues = null;

        /// <summary>
        /// member tag id, member name, object value
        /// </summary>
        private CacheSelectionsList<string, Intersection, CommentMemberTagData> _ColMemberTagCommentValues = null;

        /// <summary>
        /// member tag id, CellAddress, object value
        /// </summary>
        private CacheSelectionsList<string, CellAddress, CommentMemberTagData> _CommentMemberTagAddrValues = null;

        /// <summary>
        /// position (dimension name), Member tag comment id
        /// </summary>
        private Dictionary<string, List<string>> _RowCommentMemberTags = null;

        /// <summary>
        /// position (dimension name), Member tag comment id
        /// </summary>
        private Dictionary<string, List<string>> _ColCommentMemberTags = null;

        /// <summary>
        /// List of member tag comments.
        /// </summary>
        private List<string> _MemberTagCommentNames = null;

        /// <summary>
        /// 
        /// </summary>
        private CachedDictionary<string, int, int> _RowMbrTagSymetricTupleGroups = null;

        /// <summary>
        /// 
        /// </summary>
        private CachedDictionary<string, int, int> _ColMbrTagSymetricTupleGroups = null;

        /// <summary>
        /// List of intersection that must be made invalid because member tags intersect on the row and col.
        /// </summary>
        private readonly List<CellAddress> _InvalidLockedCells = new List<CellAddress>();

        /// <summary>
        /// Set of cell address that have comments set when the view was originally rendered.
        /// </summary>
        private readonly Set<CellAddress> _ColBorderCommentProcessed = new Set<CellAddress>();

        /// <summary>
        /// Set of cell address that have comments set when the view was originally rendered.
        /// </summary>
        private readonly Set<CellAddress> _RowBorderCommentProcessed = new Set<CellAddress>();

        private Dictionary<string, bool> _MemberTagMerged = new Dictionary<string, bool>();

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="olapView">Associated olap view.</param>
        public MemberTags(OlapView olapView)
        {
            _Parent = olapView;
        }

        /// <summary>
        /// Position of the member tag, name of the member tag.
        /// </summary>
        public Dictionary<int, string> RowMbrTagBlanks
        {
            get
            {
                if (_RowMbrTagBlanks == null)
                {
                    _RowMbrTagBlanks = new Dictionary<int, string>();
                }
                return _RowMbrTagBlanks;
            }
        }

        /// <summary>
        /// Position of the member tag, name of the member tag.
        /// </summary>
        public Dictionary<int, string> ColMbrTagBlanks
        {
            get
            {
                if (_ColMbrTagBlanks == null)
                {
                    _ColMbrTagBlanks = new Dictionary<int, string>();
                }
                return _ColMbrTagBlanks;
            }
        }

        /// <summary>
        /// Only non blank member tag intersections sent from the server.
        /// MemberTagName, Intersection, CellAddress.
        /// </summary>
        public List<Intersection> MemberTagIntersections
        {
            get
            {
                if (_MemberTagIntersections == null)
                {
                    _MemberTagIntersections = new List<Intersection>();
                }
                return _MemberTagIntersections;
            }
        }

        /// <summary>
        /// All member tag intersections.
        /// </summary>
        public CacheSelectionsList<string, Intersection, CellAddress> AllMemberTagIntersections
        {
            get
            {
                if (_AllMemberTagIntersections == null)
                {
                    _AllMemberTagIntersections = new CacheSelectionsList<string, Intersection, CellAddress>();
                }
                return _AllMemberTagIntersections;
            }
        }

        /// <summary>
        /// MamberTagName, Col/Row Pos, range.
        /// </summary>
        public CacheSelectionsList<string, int, ContiguousRange> MemberTagRanges
        {
            get
            {
                if (_MemberTagRanges == null)
                {
                    _MemberTagRanges = new CacheSelectionsList<string, int, ContiguousRange>();
                }
                return _MemberTagRanges;
            }
        }

        /// <summary>
        /// Listing of dimemsion, member name, and the values.
        /// dimemsion, member name, object value
        /// </summary>
        public CacheSelectionsList<string, Intersection, CommentMemberTagData> RowMemberTagCommentValues
        {
            get
            {
                if (_RowMemberTagCommentValues == null)
                {
                    _RowMemberTagCommentValues = new CacheSelectionsList<string, Intersection, CommentMemberTagData>();
                }
                return _RowMemberTagCommentValues;
            }
        }


        /// <summary>
        /// Listing of dimemsion, string, and the values.
        /// dimemsion, member name, object value
        /// </summary>
        public CacheSelectionsList<string, Intersection, CommentMemberTagData> ColMemberTagCommentValues
        {
            get
            {
                if (_ColMemberTagCommentValues == null)
                {
                    _ColMemberTagCommentValues = new CacheSelectionsList<string, Intersection, CommentMemberTagData>();
                }
                return _ColMemberTagCommentValues;
            }
        }

        /// <summary>
        /// Listing of Member tags, CellAddress, and the values.
        /// member tag id, member name, object value
        /// </summary>
        public CacheSelectionsList<string, CellAddress, CommentMemberTagData> CommentMemberTagAddrValues
        {
            get
            {
                if (_CommentMemberTagAddrValues == null)
                {
                    _CommentMemberTagAddrValues = new CacheSelectionsList<string, CellAddress, CommentMemberTagData>();
                }
                return _CommentMemberTagAddrValues;
            }
        }


        /// <summary>
        /// position (dimension name), Member tag comment id
        /// </summary>
        public Dictionary<string, List<string>> RowCommentMemberTags
        {
            get
            {
                if (_RowCommentMemberTags == null)
                {
                    _RowCommentMemberTags = new Dictionary<string, List<string>>();
                }
                return _RowCommentMemberTags;
            }
        }


        /// <summary>
        /// position (dimension name), Member tag comment id
        /// </summary>
        public Dictionary<string, List<string>> ColCommentMemberTags
        {
            get
            {
                if (_ColCommentMemberTags == null)
                {
                    _ColCommentMemberTags = new Dictionary<string, List<string>>();
                }
                return _ColCommentMemberTags;
            }
        }

        /// <summary>
        /// List of member tag comments.
        /// </summary>
        public List<string> MemberTagCommentNames
        {
            get 
            { 
                if(_MemberTagCommentNames == null)
                {
                    _MemberTagCommentNames = new List<string>();
                }
                return _MemberTagCommentNames; 
            }
        }


        public Dictionary<string, bool> MemberTagMerged
        {
            get
            {
                if(_MemberTagMerged == null)
                {
                    _MemberTagMerged = new Dictionary<string, bool>();
                }
                return _MemberTagMerged;
            }
        }

        /// <summary>
        /// A listing of comment member tag intersections and their associated cell addresses (for comment
        /// member tags only)
        /// MemberTagName, Intersection, CellAddress.
        /// </summary>
        public CacheSelectionsList<string, Intersection, CellAddress> CommentIntersections
        {
            get
            {
                if (_CommentIntersections == null)
                {
                    _CommentIntersections = new CacheSelectionsList<string, Intersection, CellAddress>();
                }
                return _CommentIntersections;
            }
        }

        /// <summary>
        /// Column tuple border values
        /// CellAddress, cell string value
        /// Only used for initially rendering the boarder(s).
        /// </summary>
        public Dictionary<CellAddress, string> ColTupleBorderValues
        {
            get
            {
                if (_ColTupleBorderValues == null)
                {
                    _ColTupleBorderValues = new Dictionary<CellAddress, string>();
                }
                return _ColTupleBorderValues;
            }
        }

        /// <summary>
        /// Row tuple border values
        /// CellAddress, cell string value
        /// Only used for initially rendering the boarder(s).
        /// </summary>
        public Dictionary<CellAddress, string> RowTupleBorderValues
        {
            get
            {
                if (_RowTupleBorderValues == null)
                {
                    _RowTupleBorderValues = new Dictionary<CellAddress, string>();
                }
                return _RowTupleBorderValues;
            }
        }

        /// <summary>
        /// A list of Row positions that symetric tuple groups begin
        /// member tag name, member tag row location, number of col to merge.
        /// </summary>
        public CachedDictionary<string, int, int> RowMbrTagSymetricTupleGroups
        {
            get
            {
                if (_RowMbrTagSymetricTupleGroups == null)
                {
                    _RowMbrTagSymetricTupleGroups = new CachedDictionary<string, int, int>();
                }

                return _RowMbrTagSymetricTupleGroups;
            }
        }

        /// <summary>
        /// A list of col positions that symetric tuple groups begin
        /// member tag name, member tag col location, number of row to merge.
        /// </summary>
        public CachedDictionary<string, int, int> ColMbrTagSymetricTupleGroups
        {
            get
            {
                if (_ColMbrTagSymetricTupleGroups == null)
                {
                    _ColMbrTagSymetricTupleGroups = new CachedDictionary<string, int, int>();
                }

                return _ColMbrTagSymetricTupleGroups;
            }
        }

        /// <summary>
        /// The parent olap view.
        /// </summary>
        public OlapView Parent
        {
            get { return _Parent; }
            set { _Parent = value; }
        }

        /// <summary>
        /// Set of cell address that have comments set when the view was originally rendered.
        /// </summary>
        public Set<CellAddress> ColBorderCommentProcessed
        {
            get
            {
                return _ColBorderCommentProcessed;
            }
        }

        /// <summary>
        /// Set of cell address that have comments set when the view was originally rendered.
        /// </summary>
        public Set<CellAddress> RowBorderCommentProcessed
        {
            get
            {
                return _RowBorderCommentProcessed;
            }
        }

        /// <summary>
        /// Adds a cell to the list of intersection that must be made invalid because member tags 
        /// intersect on the row and col.
        /// </summary>
        /// <param name="cell"></param>
        public void AddInvalidLockedCell(CellAddress cell)
        {
            _InvalidLockedCells.Add(cell);
        }

        /// <summary>
        /// Gets a list of intersection that must be made invalid because member tags 
        /// intersect on the row and col.
        /// </summary>
        public List<CellAddress> GetInvalidLockedCells()
        {
            return _InvalidLockedCells;
        }

        /// <summary>
        /// Returns true if a member tag is the first tuple on the view.
        /// </summary>
        /// <param name="viewAxis">Row or column</param>
        /// <returns></returns>
        public bool MemberTagIsFirstTuple(ViewAxis viewAxis)
        {
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    if (ColMbrTagBlanks != null && ColMbrTagBlanks.Count > 0)
                    {
                        return ColMbrTagBlanks.ContainsKey(0);
                    }
                    break;
                case ViewAxis.Row:
                    if (RowMbrTagBlanks != null && RowMbrTagBlanks.Count > 0)
                    {
                        return RowMbrTagBlanks.ContainsKey(0);
                    }
                    break;
            }
            return false;
        }

        /// <summary>
        /// Returns the number of member tag tuples that are at the begining of the view.
        /// </summary>
        /// <returns></returns>
        public int MemberTagBeginingTuples(ViewAxis viewAxis)
        {
            if (!MemberTagIsFirstTuple(viewAxis)) return 0;

            switch (viewAxis)
            {
                case ViewAxis.Col:
                    if (ColMbrTagBlanks != null && ColMbrTagBlanks.Count > 0)
                    {
                        return MemberTagBeginingTuples(ColMbrTagBlanks);
                    }
                    break;
                case ViewAxis.Row:
                    if (RowMbrTagBlanks != null && RowMbrTagBlanks.Count > 0)
                    {
                        return MemberTagBeginingTuples(RowMbrTagBlanks);
                    }
                    break;
            }
            return 0;
        }

        /// <summary>
        /// Returns the number of member tag tuples that are at the begining of the view.
        /// </summary>
        /// <returns></returns>
        private int MemberTagBeginingTuples(Dictionary<int, string> mbrTagBlanks)
        {
            if (mbrTagBlanks != null && mbrTagBlanks.Count > 0)
            {
                var keys = mbrTagBlanks.OrderBy(x => x.Key).Select(x => x.Key).ToList();
                int tupleCount = 0;
                for (int i = 0; i < keys.Count; i++)
                {
                    if (i != keys[i])
                    {
                        return tupleCount;
                    }
                    tupleCount = (i + 1);
                }
                return tupleCount;
            }
            return 0;
        }

        #region Methods


        /// <summary>
        /// Creates a simpleCellNote.
        /// </summary>
        /// <param name="values">Value to go in the cell note.</param>
        public simpleCellNote CreateCellNote(List<List<CommentMemberTagData>> values)
        {
            if(values == null || values.Count == 0)
            {
                return null;
            }

            List<CommentMemberTagData> array = new List<CommentMemberTagData>();

            foreach(List<CommentMemberTagData> list in values)
            {
                foreach(CommentMemberTagData value in list)
                {
                    array.Add(value);
                }
            }

            if (array.Count > 0)
            {
                return CreateCellNote(array.ToArray());
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Creates a simpleCellNote.
        /// </summary>
        /// <param name="value">Value to go in the cell note.</param>
        public simpleCellNote CreateCellNote(CommentMemberTagData[] value)
        {
            simpleCellNote cn = new simpleCellNote();

            //set the application name
            cn.applicationName = PafApp.ApplicationId;

            //get the creator.
            cn.creator = PafApp.GetGridApp().LogonInformation.UserName.ToLower();

            //set the data source.
            cn.dataSourceName = PafApp.DataSourceId;

            //set the last updated.
            cn.lastUpdated = DateTime.Now;
            cn.lastUpdatedSpecified = true;

            //set the simple coord list.
            cn.simpleCoordList = null;

            //set the text..
            cn.text = CommentMemberTagData.GetUpdatedCommentMemberTagString(value);

            //set the visble prop
            cn.visible = false;

            return cn;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewAxis">Axis</param>
        /// <param name="rc">Row or colum</param>
        /// <returns></returns>
        public int GetInvalidLockCellsLength(ViewAxis viewAxis, int rc)
        {
            int i = 0;
            switch(viewAxis)
            {
                case ViewAxis.Col:
                    foreach(CellAddress cell in GetInvalidLockedCells())
                    {
                        if(cell.Col == rc)
                        {
                            i++;
                        }
                    }
                    break;
                case ViewAxis.Row:
                    foreach (CellAddress cell in GetInvalidLockedCells())
                    {
                        if (cell.Row == rc)
                        {
                            i++;
                        }
                    }
                    break;
            }
            return i;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ca"></param>
        /// <returns></returns>
        public bool IsIntersectionInvalid(CellAddress ca)
        {
            if (_InvalidLockedCells.Count == 0)
            {
                return false;
            }
            else
            {
                return _InvalidLockedCells.Contains(ca);
            }
        }

        /// <summary>
        /// Add cell notes to border values.
        /// </summary>
        /// <param name="dimension">Name of the dimension.</param>
        /// <param name="mbrTag">Id of the member tag.</param>
        /// <param name="mbrName">Member value to add the cell note.</param>
        /// <param name="viewAxis">Axis the member is on.</param>
        /// <param name="rowPos">The current row position.</param>
        /// <param name="colPos">The current column position.</param>
        /// <param name="initialCommentBuild">Inital build of the view.</param>
        public void AddCellNotes(string dimension, string mbrTag, string mbrName, ViewAxis viewAxis, int rowPos, int colPos, bool initialCommentBuild)
        {
            if (PafApp.TestHarnessRunning) return;
            if (!String.IsNullOrEmpty(mbrName))
            {
                //int rowPos, colPos;
                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(mbrTag);
                Intersection inter = CreateMbrHeaderIntersection(mbrTag, rowPos, colPos);
                switch (viewAxis)
                {
                    case ViewAxis.Col:
                        {
                            if (inter != null)
                            {
                                List<CommentMemberTagData> value = ColMemberTagCommentValues.GetCachedSelections(dimension, inter);
                                List<int> pos = GetMemberTagPositions(ViewAxis.Row, tagDef);
                                if (value != null && value.Count > 0 && pos != null && Parent.DataRange != null)
                                {
                                    if(pos.Count == 0)
                                    {
                                        pos.Add(0);
                                    }

                                    List<CellAddress> cells = ColTupleBorderValues.ValueKeys(mbrName);

                                    if (cells != null && cells.Count > 0)
                                    {
                                        foreach (CellAddress cell in cells)
                                        {
                                            int col = cell.Col - Parent.DataRange.TopLeft.Col;
                                            int row = cell.Row + pos[0];
                                            //create a temporary intersection for comparsion.
                                            Intersection temp = CreateMbrHeaderIntersection(mbrTag, row, col);
                                            if (temp.Equals(inter))
                                            {
                                                CommentIntersections.CacheAddSelections(mbrTag, inter, cell, false);
                                                CommentMemberTagAddrValues.CacheAddSelections(mbrTag, cell, value.ToArray(), false);
                                                if (!ColBorderCommentProcessed.Contains(cell) || !initialCommentBuild)
                                                {
                                                    if (!CommentMemberTagData.AreValuesBlank(value.ToArray()))
                                                    {
                                                        Range rng = new Range(cell);
                                                        ColBorderCommentProcessed.Add(cell);
                                                        if (Parent.Grid.ContainsCellNote(rng))
                                                        {
                                                            Parent.Grid.ModifyCellNote(rng,CommentMemberTagData.GetUpdatedCommentMemberTagString(value.ToArray()), true);
                                                        }
                                                        else
                                                        {
                                                            Parent.Grid.SetCellNote(rng, CreateCellNote(value.ToArray()),true, true);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    case ViewAxis.Row:
                        {
                            if (inter != null)
                            {
                                List<CommentMemberTagData> value = RowMemberTagCommentValues.GetCachedSelections(dimension, inter);
                                List<int> pos = GetMemberTagPositions(ViewAxis.Col, tagDef);
                                if (value != null && value.Count > 0 && pos != null)
                                {
                                    if (pos.Count == 0)
                                    {
                                        pos.Add(0);
                                    }

                                    List<CellAddress> cells = RowTupleBorderValues.ValueKeys(mbrName);
                                    if (cells != null && cells.Count > 0 && Parent.DataRange != null)
                                    {
                                        foreach (CellAddress cell in cells)
                                        {
                                            int col = cell.Col + pos[0];
                                            int row = cell.Row - Parent.DataRange.TopLeft.Row;
                                            //create a temporary intersection for comparsion.
                                            Intersection temp = CreateMbrHeaderIntersection(mbrTag, row, col);
                                            if (temp.Equals(inter))
                                            {
                                                CommentIntersections.CacheAddSelections(mbrTag, inter, cell, false);
                                                CommentMemberTagAddrValues.CacheAddSelections(mbrTag, cell,value.ToArray(), false);
                                                if (!RowBorderCommentProcessed.Contains(cell) || !initialCommentBuild)
                                                {
                                                    if (!CommentMemberTagData.AreValuesBlank(value.ToArray()))
                                                    {
                                                        Range rng = new Range(cell);
                                                        RowBorderCommentProcessed.Add(cell);
                                                        if (Parent.Grid.ContainsCellNote(rng))
                                                        {
                                                            Parent.Grid.ModifyCellNote(rng,CommentMemberTagData.GetUpdatedCommentMemberTagString(value.ToArray()), true);
                                                        }
                                                        else
                                                        {
                                                            Parent.Grid.SetCellNote(rng, CreateCellNote(value.ToArray()), true, true);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Add cell notes entry to an existing cell note.
        /// </summary>
        /// <param name="dimension">Name of the dimension.</param>
        /// <param name="mbrTag">Id of the member tag.</param>
        /// <param name="mbrName">Member value to add the cell note.</param>
        /// <param name="viewAxis">Axis the member is on.</param>
        /// <param name="cell">New cell address to add the comment to.</param>
        /// <param name="comment">New comment data to add.</param>
        public void AddCellNoteEntry(string dimension, string mbrTag, string mbrName,
            ViewAxis viewAxis, CellAddress cell, CommentMemberTagData comment)
        {
            if (!String.IsNullOrEmpty(mbrName))
            {
                Intersection inter = CreateMbrHeaderIntersection(mbrTag, cell);
                //Intersection inter = CreateMbrHeaderIntersection(mbrTag, cell.Row, cell.Col);
                List<CommentMemberTagData> value = null;
                switch (viewAxis)
                {
                    case ViewAxis.Col:
                        {
                            //value = ColMemberTagCommentValues.GetCachedSelections(dimension, mbrName);
                            value = ColMemberTagCommentValues.GetCachedSelections(dimension, inter);
                            break;
                        }
                    case ViewAxis.Row:
                        {
                            if (inter != null)
                            {
                                //value = RowMemberTagCommentValues.GetCachedSelections(dimension, mbrName);
                                value = RowMemberTagCommentValues.GetCachedSelections(dimension, inter);
                            }
                            break;
                        }

                }
                if (value != null)
                {
                    if (!value.Contains(comment))
                    {
                        value.Add(comment);
                        //value.Sort();
                        CommentIntersections.CacheAddSelections(mbrTag, inter, cell, false);
                        CommentMemberTagAddrValues.CacheSelection(mbrTag, cell, value.ToArray());
                    }
                }
            }
        }



        /// <summary>
        /// Update the comment member tags (cell notes).
        /// </summary>
        /// <param name="mbrTagName">Member tag name.</param>
        /// <param name="inter">Intersection to update</param>
        /// <param name="value">New value.</param>
        public void AddUpdateDeleteCommentMemberTags(string mbrTagName, Intersection inter, string value)
        {
            //Current member tag def.
            memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(mbrTagName);
            //flag to signal if any intersections/cell address have been marked for deletion.
            bool delete = false;
            //flag to signal if any are to be added to 
            bool multipleAdd = false;
            //flag to siginal that we have to delete part of a cell note.
            bool deletePartComment = false;
            //list of CellAddress to modify
            List<CellAddress> cellAddWithMulEntry = new List<CellAddress>();
            //List of new data objects.
            List<CommentMemberTagData> commentMbrTagDataMulEntry = new List<CommentMemberTagData>();
            //List of comments to delete
            List<CommentMemberTagData> partialCommToDelete = new List<CommentMemberTagData>();
            //List of partial cell address to remove
            List<CellAddress> partialCellAddToDelete = new List<CellAddress>();
            //list of intersections to remove
            List<Intersection> interToDelete = new List<Intersection>();
            //list of cell address to remove from the collection.
            List<CellAddress> cellAddToDelete = new List<CellAddress>();
            //Range of cell to modify
            List<CellAddress> commentsToModify = new List<CellAddress>();
            //dimension name.
            string dimName;
            //get the axis and dim name
            ViewAxis viewAxis = GetMemberTagDimension(mbrTagName, out dimName);
            //get the member name
            string mbrName = String.Empty;
            //get the name of the current member.
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    {
                        mbrName = GetMemberFromMemberTagIntersection(
                            inter,
                            Parent.ColDims,
                            ColCommentMemberTags,
                            tagDef);
                        break;
                    }
                case ViewAxis.Row:
                    {
                        mbrName = GetMemberFromMemberTagIntersection(
                            inter,
                            Parent.RowDims,
                            RowCommentMemberTags,
                            tagDef);
                        break;
                    }
            }


            //get a list of cell address for a particular comment member tag, intersection(these are borders).
            List<CellAddress> cells = CommentIntersections.GetCachedSelections(mbrTagName, inter);
            if (cells != null && cells.Count > 0)
            {
                foreach (CellAddress cell in cells)
                {
                    //get a list of comment member tag data for a member tag and cell address combination.
                    CommentMemberTagDataColl comments = new CommentMemberTagDataColl(CommentMemberTagAddrValues.GetCachedSelections(mbrTagName, cell));
                    CommentMemberTagData data = comments.FindMemberTagId(tagDef.label);

                    if (String.IsNullOrEmpty(value))
                    {
                        //delete it!
                        deletePartComment = true;
                        partialCommToDelete.Add(data);
                        partialCellAddToDelete.Add(cell);
                        commentsToModify.Add(cell);
                    }
                    else if (data != null)
                    {
                        //just update it, and mark the cell note address for refresh.
                        data.MemberTagValue = value;
                        commentsToModify.Add(cell);
                    }
                    else if (data == null)//not always true.
                    {
                        //theses entries have multiples entries in one cell note.
                        multipleAdd = true;
                        cellAddWithMulEntry.Add(cell);
                        commentMbrTagDataMulEntry.Add(new CommentMemberTagData(mbrTagName, value));
                        commentsToModify.Add(cell);
                    }
                }
                //add the new comment member tags that have multiple entries in one comment(if any).
                if (multipleAdd & cellAddWithMulEntry.Count > 0)
                {
                    for (int i = 0; i < cellAddWithMulEntry.Count; i++)
                    {
                        AddCellNoteEntry(dimName, mbrTagName, mbrName, viewAxis, cellAddWithMulEntry[i], commentMbrTagDataMulEntry[i]);
                        i++;
                    }
                }
                //remove any partial selections, from the member tag cell note.
                if (deletePartComment && partialCommToDelete.Count > 0)
                {
                    for (int i = 0; i < partialCommToDelete.Count; i++)
                    {
                        if (partialCommToDelete[i] != null)
                        {
                            CommentMemberTagDataColl comments = new CommentMemberTagDataColl(CommentMemberTagAddrValues.GetCachedSelections(mbrTagName, partialCellAddToDelete[i]));
                            CommentMemberTagDataColl borderMbr = null;

                            switch (viewAxis)
                            {
                                case ViewAxis.Col:
                                {
                                    //borderMbr =new CommentMemberTagDataColl(ColMemberTagCommentValues.GetCachedSelections(dimName, mbrName));
                                    borderMbr = new CommentMemberTagDataColl(ColMemberTagCommentValues.GetCachedSelections(dimName, inter));
                                    break;
                                }
                                case ViewAxis.Row:
                                {
                                    //borderMbr =new CommentMemberTagDataColl(RowMemberTagCommentValues.GetCachedSelections(dimName, mbrName));
                                    borderMbr = new CommentMemberTagDataColl(RowMemberTagCommentValues.GetCachedSelections(dimName, inter));
                                    break;
                                }
                            }

                            if (comments.Count > 0)
                            {
                                comments.Remove(partialCommToDelete[i]);
                                CommentMemberTagAddrValues.CacheSelection(mbrTagName, partialCellAddToDelete[i], comments.ToArray());
                                if (borderMbr != null)
                                {
                                    CommentMemberTagData c = borderMbr.FindMemberTagId(partialCommToDelete[i].MemberTagId);
                                    borderMbr.Remove(c);
                                    if (c != null)
                                    {
                                        switch (viewAxis)
                                        {
                                            case ViewAxis.Col:
                                            {
                                                //ColMemberTagCommentValues.CacheSelection(dimName, mbrName, borderMbr.ToArray());
                                                ColMemberTagCommentValues.CacheSelection(dimName, inter, borderMbr.ToArray());
                                                break;
                                            }
                                            case ViewAxis.Row:
                                            {
                                                //RowMemberTagCommentValues.CacheSelection(dimName, mbrName, borderMbr.ToArray());
                                                RowMemberTagCommentValues.CacheSelection(dimName, inter, borderMbr.ToArray());
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                //perform the modification to the cell notes.
                foreach (CellAddress cell in commentsToModify)
                {
                    CommentMemberTagDataColl comments = new CommentMemberTagDataColl(CommentMemberTagAddrValues.GetCachedSelections(mbrTagName, cell));
                    //if all the data values are blank, then it's ok to remove the it.
                    if (CommentMemberTagData.AreValuesBlank(comments.ToArray()))
                    {
                        delete = true;
                        interToDelete.Add(inter);
                        cellAddToDelete.Add(cell);
                        Parent.Grid.DeleteCellNote(new Range(cell));
                    }
                    else
                    {
                        //modify the existing cell note.
                        List<CommentMemberTagData> data = null;
                        switch (viewAxis)
                        {
                            case ViewAxis.Col:
                            {
                                //data = ColMemberTagCommentValues.GetCachedSelections(dimName, mbrName);
                                data = ColMemberTagCommentValues.GetCachedSelections(dimName, inter);
                                break;
                            }
                            case ViewAxis.Row:
                            {
                                //data = RowMemberTagCommentValues.GetCachedSelections(dimName, mbrName);
                                data = RowMemberTagCommentValues.GetCachedSelections(dimName, inter);
                                break;
                            }
                        }
                        Parent.Grid.SetCellNote(
                            new Range(cell),
                            CreateCellNote(data.ToArray()),
                            false,
                            false);
                    }
                }
            }
            else
            {
                //the cell note does not exist, so add it...
                if (viewAxis == ViewAxis.Col && ColCommentMemberTags.Count > 0 && !String.IsNullOrEmpty(value))
                {
                    AddUpdatedCommentMemberTags(
                        mbrTagName,
                        inter,
                        value,
                        viewAxis,
                        ColMemberTagCommentValues,
                        ColCommentMemberTags,
                        mbrName);
                }
                else if (viewAxis == ViewAxis.Row && RowCommentMemberTags.Count > 0 && !String.IsNullOrEmpty(value))
                {
                    AddUpdatedCommentMemberTags(
                        mbrTagName,
                        inter,
                        value,
                        viewAxis,
                        RowMemberTagCommentValues,
                        RowCommentMemberTags,
                        mbrName);
                }
            }
            //remove any member tag references from the collections.
            if (delete)
            {
                DeleteCommentMemberTags(mbrTagName, inter, interToDelete, cellAddToDelete);
            }
        }

        public void MergeMemberTagDataCells()
        {
            memberTagDef pafMbrTagDef;

            foreach (KeyValuePair<int, string> kvp in ColMbrTagBlanks)
            {
                pafMbrTagDef = PafApp.GetMemberTagInfo().GetMemberTag(kvp.Value);
                MergeMemberTagDataCells(pafMbrTagDef);
            }
            foreach (KeyValuePair<int, string> kvp in RowMbrTagBlanks)
            {
                pafMbrTagDef = PafApp.GetMemberTagInfo().GetMemberTag(kvp.Value);
                MergeMemberTagDataCells(pafMbrTagDef);
            }
        }

        /// <summary>
        /// Merge the member tag data cells.
        /// </summary>
        /// <param name="pafMbrTagDef">Member tag data defination.</param>
        public void MergeMemberTagDataCells(memberTagDef pafMbrTagDef)
        {
            if (_AllMemberTagIntersections != null && _AllMemberTagIntersections.Count() > 0)
            {
                Dictionary<Intersection, List<CellAddress>> values = _AllMemberTagIntersections.Values(pafMbrTagDef.name);

                if (values != null)
                {
                    foreach (KeyValuePair<Intersection, List<CellAddress>> kvp in values)
                    {
                        if (kvp.Value != null && kvp.Value.Count > 1)
                        {
                            MergeTupleRanges(kvp.Value, pafMbrTagDef);
                        }
                        //else
                        //{
                        //    if (MemberTagMerged.ContainsKey(pafMbrTagDef.name))
                        //    {
                        //        MemberTagMerged[pafMbrTagDef.name] = false;
                        //    }
                        //    else
                        //    {
                        //        MemberTagMerged.Add(pafMbrTagDef.name, false);
                        //    }
                        //}
                    }
                }
            }
        }

        /// <summary>
        /// Gets a list of contigous ranges/ranges for a list of cell address, for an axis.
        /// </summary>
        /// <param name="cells">List of cell addresses.</param>
        /// <param name="tagDef">Member tag defination.</param>
        /// <param name="range">List of singular ranges to be formatted.</param>
        /// <returns>List of contigous ranges.</returns>
        public List<ContiguousRange> GetRanges(IList<CellAddress> cells, memberTagDef tagDef, ref List<Range> range)
        {
            int endPos = 0;
            int count = 0;
            List<ContiguousRange> lstCr = new List<ContiguousRange>();
            ViewAxis viewAxis = ViewAxis.Unknown;

            foreach (KeyValuePair<int, string> kvp in RowMbrTagBlanks)
            {
                if (kvp.Value.Equals(tagDef.name))
                {
                    viewAxis = ViewAxis.Col;
                    break;
                }
            }
            foreach (KeyValuePair<int, string> kvp in ColMbrTagBlanks)
            {
                if (kvp.Value.Equals(tagDef.name))
                {
                    viewAxis = ViewAxis.Row;
                    break;
                }
            }

            int[] symTupGrps = GetTupleGroupDimPosition(tagDef, viewAxis);

            if (cells.Count > 0 && viewAxis == ViewAxis.Col || viewAxis == ViewAxis.Row)
            {
                //get the rol or col count.
                switch (viewAxis)
                {
                    case ViewAxis.Col:
                        count = GetColCount(cells, 0, symTupGrps);
                        break;
                    case ViewAxis.Row:
                        count = GetRowCount(cells, 0, symTupGrps);
                        break;
                }

                endPos += count + 1;

                ContiguousRange cr;

                if (count > 0)
                {
                    cr = new ContiguousRange(cells[0], cells[endPos - 1]);
                    lstCr.Add(cr);
                }

                while (count > 0 || endPos < cells.Count)
                {
                    int startPos = endPos;
                    //get the rol or col count.
                    switch (viewAxis)
                    {
                        case ViewAxis.Col:
                            count = GetColCount(cells, endPos, symTupGrps);
                            break;
                        case ViewAxis.Row:
                            count = GetRowCount(cells, endPos, symTupGrps);
                            break;
                    }

                    if (count > 0)
                    {
                        endPos += count + 1;

                        cr = new ContiguousRange(cells[startPos], cells[endPos - 1]);
                        lstCr.Add(cr);
                    }
                    else
                    {
                        endPos++;
                    }
                }
                if (lstCr.Count == 0)
                {
                    foreach (CellAddress cell in cells)
                    {
                        range.Add(new Range(cell));
                    }
                }
                else if (endPos < cells.Count)
                {
                    foreach (ContiguousRange conRng in lstCr)
                    {
                        foreach (Cell cell in conRng.Cells)
                        {
                            if (!cells.Contains(cell.CellAddress))
                            {
                                range.Add(new Range(cell.CellAddress));
                            }
                        }
                    }
                }
            }
            
            return lstCr;
        }

        /// <summary>
        /// Merges data tuples on an axis.
        /// </summary>
        /// <param name="cells">Cells</param>
        /// <param name="pafMbrTagDef">Member tag defination.</param>
        private void MergeTupleRanges(IList<CellAddress> cells, memberTagDef pafMbrTagDef)
        {
            List<Range> rng = new List<Range>();
            List<ContiguousRange> crs = GetRanges(cells, pafMbrTagDef, ref rng);
            if (crs != null && crs.Count > 0)
            {
                foreach (ContiguousRange cr in crs)
                {
                    Parent.Grid.Merge(cr, PafApp.GetEventManager().SheetPassword);
                    if (MemberTagMerged.ContainsKey(pafMbrTagDef.name))
                    {
                        MemberTagMerged[pafMbrTagDef.name] = true;
                    }
                    else
                    {
                        MemberTagMerged.Add(pafMbrTagDef.name, true);
                    }
                }
            }
            else 
            {
                if (MemberTagMerged.ContainsKey(pafMbrTagDef.name))
                {
                    MemberTagMerged[pafMbrTagDef.name] = false;
                }
                else
                {
                    MemberTagMerged.Add(pafMbrTagDef.name, false);
                }
            }
        }

        /// <summary>
        /// Gets the row count from a list of cell addresses.
        /// </summary>
        /// <param name="cells">List of cell address to count.</param>
        /// <param name="startPos">Position in the list to start counting.</param>
        /// <param name="symTupGrps">Symetric tuple groups.</param>
        /// <returns>Count of rows.</returns>
        private int GetRowCount(IList<CellAddress> cells, int startPos, int[] symTupGrps)
        {
            int iCount = 0;
            for (int i = startPos; i < cells.Count - 1; i++)
            {
                int?[] val;
                int?[] val2;

                Parent.RowSymetricTupleGroups.TryGetValue(cells[i].Row - Parent.DataRange.TopLeft.Row, out val);
                Parent.RowSymetricTupleGroups.TryGetValue(cells[i + 1].Row - Parent.DataRange.TopLeft.Row, out val2);

                if (symTupGrps != null && symTupGrps.Length > 0)
                {
                    bool okToMerge = false;
                    foreach (int grp in symTupGrps)
                    {
                        if (val[grp] != val2[grp])
                        {
                            okToMerge = false;
                            break;
                        }
                        else
                        {
                            okToMerge = true;
                        }
                    }
                    if (okToMerge && cells[i].Row + 1 == cells[i + 1].Row)
                    {
                        iCount++;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (cells[i].Row + 1 == cells[i + 1].Row)
                {
                    iCount++;
                }
                else
                {
                    break;
                }
            }
            return iCount;
        }

        /// <summary>
        /// Get the tuple group positions for a member tag defination.
        /// </summary>
        /// <param name="tagDef">Member tag defination.</param>
        /// <param name="viewAxis">Axis</param>
        /// <returns>Array of positions.</returns>
        private int[] GetTupleGroupDimPosition(memberTagDef tagDef, ViewAxis viewAxis)
        {
            List<int> val = new List<int>();
            int i = 0;
            Dictionary<string, List<string>> arrayDims;
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    arrayDims = Parent.ColDims;
                    break;
                case ViewAxis.Row:
                    arrayDims = Parent.RowDims;
                    break;
                default:
                    return null;
            }

            foreach (KeyValuePair<string, List<string>> kvp in arrayDims)
            {
                foreach (string dim in tagDef.dims)
                {
                    if (kvp.Key.Contains(dim))
                    {
                        val.Add(i);
                        i++;
                        break;
                    }
                }
            }
            return val.ToArray();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cells"></param>
        /// <param name="startPos"></param>
        /// <returns></returns>
        private int GetRowCount2(IList<CellAddress> cells, int startPos)
        {
            int iCount = 0;
            for (int i = startPos; i < cells.Count - 1; i++)
            {
                if (cells[i].Row + 1 == cells[i + 1].Row)
                {
                    iCount++;
                }
                else
                {
                    break;
                }
            }
            return iCount;
        }

        /// <summary>
        /// Gets the column count from a list of cell addresses.
        /// </summary>
        /// <param name="cells">List of cell address to count.</param>
        /// <param name="startPos">Position in the list to start counting.</param>
        /// <param name="symTupGrps">Symetric tuple groups.</param>
        /// <returns>Count of columns.</returns>
        private int GetColCount(IList<CellAddress> cells, int startPos, int[] symTupGrps)
        {
            int iCount = 0;
            for (int i = startPos; i < cells.Count - 1; i++)
            {
                int?[] val;
                int?[] val2;

                Parent.ColSymetricTupleGroups.TryGetValue(cells[i].Col - Parent.DataRange.TopLeft.Col, out val);
                Parent.ColSymetricTupleGroups.TryGetValue(cells[i + 1].Col - Parent.DataRange.TopLeft.Col, out val2);

                if (symTupGrps != null && symTupGrps.Length > 0)
                {
                    bool okToMerge = false;
                    foreach (int grp in symTupGrps)
                    {
                        if (val[grp] != val2[grp])
                        {
                            okToMerge = false;
                            break;
                        }
                        else
                        {
                            okToMerge = true;
                        }
                    }
                    if (okToMerge && cells[i].Col + 1 == cells[i + 1].Col)
                    {
                        iCount++;
                    }
                    else
                    {
                        break;
                    }
                }
                else if (cells[i].Col + 1 == cells[i + 1].Col)
                {
                    iCount++;
                }
                else
                {
                    break;
                }
            }
            return iCount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cells"></param>
        /// <param name="startPos"></param>
        /// <returns></returns>
        private static int GetColCount2(IList<CellAddress> cells, int startPos)
        {
            int iCount = 0;
            for (int i = startPos; i < cells.Count - 1; i++)
            {
                if (cells[i].Col + 1 == cells[i + 1].Col)
                {
                    iCount++;
                }
                else
                {
                    break;
                }
            }
            return iCount;
        }

        /// <summary>
        /// Is the current cell address a member tag.
        /// </summary>
        /// <param name="addr">Current cell address.</param>
        /// <param name="pafMbrTagDef">The member tag def the is passed back if the current cell
        /// is a member tag.</param>
        /// <returns>true if the cell address is a member tag, false if not.</returns>
        public bool IsMemberTag(CellAddress addr, out memberTagDef pafMbrTagDef)
        {
            bool ret = false;
            pafMbrTagDef = null;

            try
            {
                int adjustCol = addr.Col - Parent.DataRange.TopLeft.Col;
                int adjustRow = addr.Row - Parent.DataRange.TopLeft.Row;

                if (ColMbrTagBlanks != null && RowMbrTagBlanks != null)
                {
                    if (ColMbrTagBlanks.ContainsKey(adjustCol))
                    {
                        pafMbrTagDef = PafApp.GetMemberTagInfo().GetMemberTag(ColMbrTagBlanks[adjustCol]);
                        ret = true;
                    }
                    else if (RowMbrTagBlanks.ContainsKey(adjustRow))
                    {
                        pafMbrTagDef = PafApp.GetMemberTagInfo().GetMemberTag(RowMbrTagBlanks[adjustRow]);
                        ret = true;
                    }
                }
                return ret;
            }
            catch (Exception)
            {
                ret = false;
            }

            return ret;
        }

        /// <summary>
        /// Get member tag data for a specified member tag/axis combination.
        /// </summary>
        /// <param name="name">Name of the member tag.</param>
        /// <param name="pafViewSection">View section that contains the data.</param>
        /// <param name="viewAxis">The axis to retrieve the member tag data for.</param>
        /// <param name="includeNulls">True to return an array will null / blanks</param>
        /// <returns>The string array of member tag data, or null if the member tag data does not exist,
        /// or does not exist on the axis specified.</returns>
        public string[] GetMemberTagData(string name, pafViewSection pafViewSection, ViewAxis viewAxis, bool includeNulls = false)
        {
            string[] str = null;
            if (pafViewSection != null)
            {
                if (viewAxis == ViewAxis.Col && pafViewSection.colMemberTagData != null && pafViewSection.colMemberTagData.Length > 0)
                {
                    foreach (memberTagViewSectionData mt in pafViewSection.colMemberTagData)
                    {
                        bool build = false;
                        if (includeNulls)
                        {
                            if(mt.memberTagName.ToLower().Equals(name.ToLower()))
                            {
                                build = true;
                            }
                        }
                        else
                        {
                            if (mt != null && mt.memberTagValues != null && mt.memberTagValues.ContainsNonNullValues() && mt.memberTagName.ToLower().Equals(name.ToLower()))
                            {
                                build = true;
                            }
                        }
                        if (!build) continue;
                        str = new string[mt.memberTagValues.Length];
                        mt.memberTagValues.CopyTo(str, 0);
                        break;
                    }
                }
                else if (viewAxis == ViewAxis.Row && pafViewSection.rowMemberTagData != null && pafViewSection.rowMemberTagData.Length > 0)
                {
                    foreach (memberTagViewSectionData mt in pafViewSection.rowMemberTagData)
                    {
                        bool build = false;
                        if (includeNulls)
                        {
                            if (mt.memberTagName.ToLower().Equals(name.ToLower()))
                            {
                                build = true;
                            }
                        }
                        else
                        {
                            if (mt != null && mt.memberTagValues != null && mt.memberTagValues.ContainsNonNullValues() && mt.memberTagName.ToLower().Equals(name.ToLower()))
                            {
                                build = true;
                            }
                        }
                        if (!build) continue;
                        str = new string[mt.memberTagValues.Length];
                        mt.memberTagValues.CopyTo(str, 0);
                        break;
                    }
                }
            }
            return str;
        }

        /// <summary>
        /// Cache all the non blank member tag intersections.
        /// </summary>
        /// <param name="memberTagName">Name of the member tag.</param>
        /// <param name="cells">Range of the memeber tag.</param>
        /// <returns>true if the member tags should be marked as dirty, false if not.</returns>
        public bool BuildMemberTagIntersections(string memberTagName, IEnumerable<Cell> cells)
        {
            bool markAsDirty = false;
            foreach (Cell cell in cells)
            {
                Intersection inter = FindMemberTagIntersection(cell.CellAddress);

                if (!MemberTagIntersections.Contains(inter))
                {
                    if (cell.Value != null && !String.IsNullOrEmpty(cell.Value.ToString()))
                    {
                        if(PafApp.GetMbrTagMngr().MbrTagCache.AddMemberToCache(memberTagName, inter, cell.Value.ToString()))
                        {
                            markAsDirty = true;
                        }
                        MemberTagIntersections.Add(inter);
                    }
                    else
                    {
                        if(PafApp.GetMbrTagMngr().MbrTagCache.AddMemberToCache(memberTagName, inter, String.Empty))
                        {
                             markAsDirty = true;
                        }
                    }
                }
                AllMemberTagIntersections.CacheAddSelections(memberTagName, inter, cell.CellAddress, true);
            }
            return markAsDirty;
        }

        /// <summary>
        /// Finds an cell address from a member tag intersection.
        /// </summary>
        /// <param name="inter">Intersection to find.</param>
        /// <param name="memberTagName">Name of the member tag.</param>
        /// <returns>A cellAddress.</returns>
        public List<CellAddress> FindMemberTagIntersectionAddress(Intersection inter, string memberTagName)
        {
            //StringBuilder mbrNameHash;
            memberTagDef sdmp = null;

            if (inter == null || inter.AxisSequence.Length == 0 || inter.Coordinates.Length == 0)
            {
                return null;
            }

            //its a member tag.
            if (inter.AxisSequence.Length < Parent.DimensionCount)
            {
                //string memberTagName = PafApp.GetMemberTagInfo().GetMemberTagName(inter.AxisSequence);
                sdmp = PafApp.GetMemberTagInfo().GetMemberTag(memberTagName);
            }
            if (sdmp == null)
            {
                return null;
            }

            List<CellAddress> cells = AllMemberTagIntersections.GetCachedSelections(memberTagName, inter);

            if(cells != null && cells.Count > 0)
            {
                return cells;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Finds an intersection from a member tag cell address.
        /// </summary>
        /// <param name="addr">address of a member tag cell.</param>
        /// <returns>An intersection object</returns>
        public Intersection FindMemberTagIntersection(CellAddress addr)
        {
            memberTagDef tagDef;

            if (!IsMemberTag(addr, out tagDef))
            {
                return null;
            }

            int rowMbrPos = addr.Row - Parent.StartCell.Row - Parent.ColDims.Count - Parent.ColAliases;
            if (rowMbrPos < 0) return null;

            int colMbrPos = addr.Col - Parent.StartCell.Col - Parent.RowDims.Count - Parent.RowAliases;
            if (colMbrPos < 0) return null;

            Intersection intersection;
            List<string> dims = new List<string>();
            int intersectionAxisSequenceLength;

            if (tagDef.dims != null && tagDef.dims.Length > 0)
            {
                intersection = new Intersection(tagDef.dims);
                intersectionAxisSequenceLength = intersection.AxisSequence.Length;
                dims.AddRange(tagDef.dims);
            }
            else
            {
                return null;
            }

            foreach (string rowDim in Parent.RowDims.Keys)
            {
                if (dims.Contains(rowDim))
                {
                    intersection.SetCoordinate(rowDim, Parent.RowDims[rowDim][rowMbrPos]);
                }
            }

            foreach (string colDim in Parent.ColDims.Keys)
            {
                if (dims.Contains(colDim))
                {
                    intersection.SetCoordinate(colDim, Parent.ColDims[colDim][colMbrPos]);
                }
            }


            //Add Page Members
            for (int j = 0; j < intersectionAxisSequenceLength; j++)
            {
                foreach (string key in Parent.PageDims.Keys)
                {
                    if (dims.Contains(key))
                    {
                        if (intersection.AxisSequence[j] == key)
                        {
                            intersection.SetCoordinate(key, Parent.PageDims[key]);
                            break;
                        }
                    }
                }
            }
            return intersection;
        }

        /// <summary>
        /// Get the colums/row positions of a member tag.
        /// </summary>
        /// <param name="viewAxis">Axis to search.</param>
        /// <param name="tagDef">tag defination.</param>
        /// <returns>a list of positions.</returns>
        public List<int> GetMemberTagPositions(ViewAxis viewAxis, memberTagDef tagDef)
        {
            List<int> pos = new List<int>();

            switch (viewAxis)
            {
                case ViewAxis.Col:
                    foreach (KeyValuePair<int, string> kvp in ColMbrTagBlanks)
                    {
                        if (kvp.Value.Equals(tagDef.name))
                        {
                            pos.Add(kvp.Key);
                        }

                    }
                    break;
                case ViewAxis.Row:
                    foreach (KeyValuePair<int, string> kvp in RowMbrTagBlanks)
                    {
                        if(kvp.Value.Equals(tagDef.name))
                        {
                            pos.Add(kvp.Key);
                        }
                    }
                    break;
            }
            return pos;
        }


          /// <summary>
        /// 
        /// </summary>
        /// <param name="dims"></param>
        /// <param name="members"></param>
        /// <param name="memberTagId"></param>
        /// <param name="simpleMbr"></param>
        /// <returns></returns>
        public Intersection CreateIntersection(string[] dims, string[] members, string memberTagId, memberTagDef simpleMbr)
        {
            Intersection inter = new Intersection(simpleMbr.dims);

            for (int i = 0; i < inter.AxisSequence.Length; i++)
            {
                if (inter.Coordinates[i].Equals(PafAppConstants.INTERSECTION_COORDINATE_NOT_INIT))
                {
                    string tuples;
                    if (Parent.PageDims.TryGetValue(inter.AxisSequence[i], out tuples))
                    {
                        inter.SetCoordinate(inter.AxisSequence[i], tuples);
                    }
                }
            }

            int j = 0;
            foreach (string dim in dims)
            {
                if (inter.ContainsAxis(dim))
                {
                    inter.SetCoordinate(dim, members[j]);
                }
                j++;
            }

            return inter;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dims"></param>
        /// <param name="members"></param>
        /// <param name="memberTagId"></param>
        /// <returns></returns>
        public Intersection CreateIntersection(string[] dims, string[] members, string memberTagId)
        {
            memberTagDef simpleMbr = PafApp.GetMemberTagInfo().GetMemberTag(memberTagId);

            return CreateIntersection(dims, members, memberTagId, simpleMbr);

        }

        /// <summary>
        /// Creates an intersection for a member in the border.
        /// </summary>
        /// <param name="memberTagId">Id of the member tag.</param>
        /// <param name="rowPos"></param>
        /// <param name="colPos"></param>
        /// <returns></returns>
        private Intersection CreateMbrHeaderIntersection(string memberTagId, int rowPos, int colPos)
        {
            if (String.IsNullOrEmpty(memberTagId))
            {
                return null;
            }

            memberTagDef simpleMbr = PafApp.GetMemberTagInfo().GetMemberTag(memberTagId);
            Intersection inter = new Intersection(simpleMbr.dims);

            for (int i = 0; i < inter.AxisSequence.Length; i++)
            {
                if (inter.Coordinates[i].Equals(PafAppConstants.INTERSECTION_COORDINATE_NOT_INIT))
                {
                    string tuples;
                    if (Parent.PageDims.TryGetValue(inter.AxisSequence[i], out tuples))
                    {
                        inter.SetCoordinate(inter.AxisSequence[i], tuples);
                    }
                }
            }


            if (RowCommentMemberTags.Count > 0)
            {
                List<string> interDims = new List<string>();
                if (inter.AxisSequence.Length > 0)
                {
                    interDims.AddRange(inter.AxisSequence);
                }

                foreach (string rowDim in Parent.RowDims.Keys)
                {
                    if (interDims.Contains(rowDim))
                    {
                        List<string> al = Parent.RowDims[rowDim];
                        inter.SetCoordinate(rowDim, al[rowPos].ToString());
                    }
                }
            }

            if (ColCommentMemberTags.Count > 0)
            {
                List<string> interDims = new List<string>();
                if (inter.AxisSequence.Length > 0)
                {
                    interDims.AddRange(inter.AxisSequence);
                }

                foreach (string colDim in Parent.ColDims.Keys)
                {
                    if (interDims.Contains(colDim))
                    {
                        List<string> al = Parent.ColDims[colDim];
                        inter.SetCoordinate(colDim, al[colPos].ToString());
                    }
                }
            }

            return inter;
        }

        /// <summary>
        /// Creates an intersection for a member in the border.
        /// </summary>
        /// <param name="memberTagId">Id of the member tag.</param>
        /// <param name="cell">Cell address.</param>
        /// <returns></returns>
        private Intersection CreateMbrHeaderIntersection(string memberTagId, CellAddress cell)
        {
            //do a bunch of null checks.
            if (String.IsNullOrEmpty(memberTagId) || cell == null || Parent.DataRange == null || 
                Parent.DataRange.TopLeft == null)
            {
                return null;
            }

            memberTagDef simpleMbr = PafApp.GetMemberTagInfo().GetMemberTag(memberTagId);
            Intersection inter = new Intersection(simpleMbr.dims);

            for (int i = 0; i < inter.AxisSequence.Length; i++)
            {
                if (inter.Coordinates[i].Equals(PafAppConstants.INTERSECTION_COORDINATE_NOT_INIT))
                {
                    string tuples;
                    if (Parent.PageDims.TryGetValue(inter.AxisSequence[i], out tuples))
                    {
                        inter.SetCoordinate(inter.AxisSequence[i], tuples);
                    }
                }
            }


            if (RowCommentMemberTags.Count > 0)
            {
                int row;
                if (cell.Row >= Parent.DataRange.TopLeft.Row)
                {
                    row = cell.Row - Parent.DataRange.TopLeft.Row;
                }
                else
                {
                    row = cell.Row;
                }

                //make sure row is not a neg. numb, if so; set it to axis position.
                if (row < 0)
                {
                    row = cell.Row;
                }

                List<string> interDims = new List<string>();
                if (inter.AxisSequence.Length > 0)
                {
                    interDims.AddRange(inter.AxisSequence);
                }

                foreach (string rowDim in Parent.RowDims.Keys)
                {
                    if (interDims.Contains(rowDim))
                    {
                        List<string> al = Parent.RowDims[rowDim];
                        inter.SetCoordinate(rowDim, al[row]);
                    }
                }
            }

            if (ColCommentMemberTags.Count > 0)
            {
                int col;
                if (cell.Col > Parent.DataRange.TopLeft.Col)
                {
                    col = cell.Col - Parent.DataRange.TopLeft.Col;
                }
                else
                {
                    col = cell.Col;
                }


                //make sure col is not a neg. numb, if so; set it to axis position.
                if (col < 0)
                {
                    col = cell.Col;
                }

                List<string> interDims = new List<string>();
                if (inter.AxisSequence.Length > 0)
                {
                    interDims.AddRange(inter.AxisSequence);
                }

                foreach (string colDim in Parent.ColDims.Keys)
                {
                    if (interDims.Contains(colDim))
                    {
                        List<string> al = Parent.ColDims[colDim];
                        inter.SetCoordinate(colDim, al[col]);
                    }
                }
            }

            return inter;
        }

        /// <summary>
        /// Add a new comment member tag, after the view has already been rendered on the screen (calls AddCellNotes).
        /// </summary>
        /// <param name="mbrTagName">Name of the member tag.</param>
        /// <param name="inter">Intersection to add the comment member tag for.</param>
        /// <param name="value">Value to add.</param>
        /// <param name="viewAxis">Axis to add the comemnt member tag to.</param>
        /// <param name="axisMemberTagCommentValues">Member tag comment value.</param>
        /// <param name="axisCommentMbrTags">Dictionary of comment tags for a specific axis.</param>
        /// <param name="mbrName">Name of the member where the comment member tag is located.</param>
        private void AddUpdatedCommentMemberTags(string mbrTagName, Intersection inter, string value, 
            ViewAxis viewAxis, CachedSelections<string, Intersection, CommentMemberTagData> axisMemberTagCommentValues, 
            ICollection<KeyValuePair<string, List<string>>> axisCommentMbrTags, string mbrName)
        {
            if (axisCommentMbrTags.Count > 0)
            {
                string dimension = String.Empty;

                foreach (KeyValuePair<string, List<string>> kvp in axisCommentMbrTags)
                {
                    if (kvp.Value.Contains(mbrTagName))
                    {
                        dimension = kvp.Key;
                        break;
                    }
                }

                List<CellAddress> cells = null;
                int row;
                int col;

                switch(viewAxis)
                {
                    case ViewAxis.Row:
                        cells = FindMemberTagIntersectionAddress(inter, mbrTagName);
                        //cells = RowTupleBorderValues.GetKeys(mbrName);
                        if (cells != null)
                        {
                            foreach (CellAddress cell in cells)
                            {
                                if (cell != null)
                                {
                                    row = cell.Row - Parent.DataRange.TopLeft.Row;
                                    col = cell.Col;

                                    //CellAddress ca = new CellAddress(cell.Row - Parent.DataRange.TopLeft.Row, cell.Col);

                                    axisMemberTagCommentValues.CacheAddSelections(
                                        dimension,
                                        inter, new CommentMemberTagData(mbrTagName, value),
                                            false);

                                    AddCellNotes(dimension, mbrTagName, mbrName, viewAxis, row, col, false);
                                }
                            }
                        }
                        break;
                    case ViewAxis.Col:
                        cells = FindMemberTagIntersectionAddress(inter, mbrTagName);
                        //cells = ColTupleBorderValues.GetKeys(mbrName);
                        if (cells != null)
                        {
                            foreach (CellAddress cell in cells)
                            {
                                if (cell != null)
                                {
                                    col = cell.Col - Parent.DataRange.TopLeft.Col;
                                    row = cell.Row;

                                    //CellAddress ca = new CellAddress(cell.Row - Parent.DataRange.TopLeft.Row, cell.Col);

                                    axisMemberTagCommentValues.CacheAddSelections(
                                        dimension,
                                        inter, new CommentMemberTagData(mbrTagName, value),
                                            false);

                                    AddCellNotes(dimension, mbrTagName, mbrName, viewAxis, row, col, false);
                                }
                            }
                        }
                        break;
                }                
            }
        }

        /// <summary>
        /// Delete a new comment member tag.
        /// </summary>
        /// <param name="mbrTagName">Name of the member tag.</param>
        /// <param name="inter">Intersection to add the comment member tag for.</param>
        /// <param name="interToDelete">Interstions to delete.</param>
        /// <param name="cellAddToDelete">Cells to delete.</param>
        private void DeleteCommentMemberTags(string mbrTagName, Intersection inter,
            ICollection<Intersection> interToDelete, ICollection<CellAddress> cellAddToDelete)
        {

            if (interToDelete != null && interToDelete.Count > 0 && cellAddToDelete != null && cellAddToDelete.Count > 0)
            {
                ViewAxis viewAxis = ViewAxis.Unknown;
                string dimension = String.Empty;


                foreach (KeyValuePair<string, List<string>> kvp in RowCommentMemberTags)
                {
                    if (kvp.Value.Contains(mbrTagName))
                    {
                        viewAxis  = ViewAxis.Row;
                        dimension = kvp.Key;
                        break;
                    }
                }

                if (viewAxis == ViewAxis.Unknown)
                {
                    foreach (KeyValuePair<string, List<string>> kvp in ColCommentMemberTags)
                    {
                        if (kvp.Value.Contains(mbrTagName))
                        {
                            viewAxis = ViewAxis.Col;
                            dimension = kvp.Key;
                            break;
                        }
                    }
                }

                //remove any member tag references from the collections.
                foreach (Intersection inters in interToDelete)
                {
                    CommentIntersections.RemoveSelection(mbrTagName, inters);
                }


                switch (viewAxis)
                {
                    case ViewAxis.Col:
                        {
                            foreach (CellAddress ca in cellAddToDelete)
                            {
                                //string mbrName = ColTupleBorderValues.GetCachedSelection(ca);
                                ColMemberTagCommentValues.RemoveSelection(dimension, inter);
                                CommentMemberTagAddrValues.RemoveSelection(mbrTagName, ca);
                            }
                            break;
                        }
                    case ViewAxis.Row:
                        {
                            foreach (CellAddress ca in cellAddToDelete)
                            {
                                //string mbrName = RowTupleBorderValues.GetCachedSelection(ca);
                                RowMemberTagCommentValues.RemoveSelection(dimension, inter);
                                CommentMemberTagAddrValues.RemoveSelection(mbrTagName, ca);
                            }
                            break;
                        }
                }
            }
        }

        /// <summary>
        /// Add a new comment member tag, after the view has already been rendered on the screen (calls AddCellNotes).
        /// </summary>
        /// <param name="inter">Intersection to add the comment member tag for.</param>
        /// <param name="axisDims">The dictionary of array list tuples.</param>
        /// <param name="commentMbrTags">Dictionary of comment tags for a specific axis.</param>
        /// <param name="tagDef">Member tag defination.</param>
        private static string GetMemberFromMemberTagIntersection(Intersection inter,
            IDictionary<string, List<string>> axisDims,
            IDictionary<string, List<string>> commentMbrTags,
            memberTagDef tagDef)
        {
            StringBuilder mbrNameHash = new StringBuilder();
            if (commentMbrTags.Count > 0)
            {
                List<string> interDims = new List<string>();
                if (inter.AxisSequence.Length > 0)
                {
                    interDims.AddRange(inter.AxisSequence);
                }

                //bool rowFound = false;
                foreach(string dim in axisDims.Keys)
                {
                    if (interDims.Contains(dim) && commentMbrTags.ContainsKey(dim))
                    {
                        List<string> value;
                        if (commentMbrTags.TryGetValue(dim, out value))
                        {
                            if(value.Contains(tagDef.name))
                            {
                                mbrNameHash.Append(inter.GetCoordinate(dim));
                            }
                        }
                    }
                }
            }
            return mbrNameHash.ToString();
        }

        /// <summary>
        /// Gets the member tag dimension Axis.
        /// </summary>
        /// <param name="mbrTagName">name of the member tag dim.</param>
        /// <param name="dimensionName">Name of the dimension.</param>
        /// <returns></returns>
        private ViewAxis GetMemberTagDimension(string mbrTagName, out string dimensionName)
        {
            ViewAxis viewAxis = ViewAxis.Unknown;
            dimensionName = String.Empty;

            foreach (KeyValuePair<string, List<string>> kvp in RowCommentMemberTags)
            {
                if (kvp.Value.Contains(mbrTagName))
                {
                    dimensionName = kvp.Key;
                    return ViewAxis.Row;
                }
            }

            foreach (KeyValuePair<string, List<string>> kvp in ColCommentMemberTags)
            {
                if (kvp.Value.Contains(mbrTagName))
                {
                    dimensionName = kvp.Key;
                    return ViewAxis.Col;
                }
            }

            return viewAxis;
        }

        #endregion Methods
    }
}
