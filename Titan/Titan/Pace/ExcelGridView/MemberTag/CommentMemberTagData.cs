#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Text;
using Titan.PafService;

namespace Titan.Pace.ExcelGridView.MemberTag
{
    internal class CommentMemberTagData : IComparable<CommentMemberTagData>, IEquatable<CommentMemberTagData>
    {
        private string _MemberTagId;
        private string _MemberTagValue;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CommentMemberTagData ()
        {
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="memberTagId">Member tag id.</param>
        /// <param name="memberTagValue">Member tag value.</param>
        public CommentMemberTagData(string memberTagId, string memberTagValue)
        {
            if (!String.IsNullOrEmpty(memberTagId))
            {
                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(memberTagId);
                if (tagDef != null)
                {
                    _MemberTagId = tagDef.label;
                }
            }
            if(!String.IsNullOrEmpty(memberTagValue))
            {
                _MemberTagValue = memberTagValue.Trim();
            }
            else
            {
                _MemberTagValue = String.Empty;
            }
        }

        /// <summary>
        /// Gets/sets the member tag id.
        /// </summary>
        public string MemberTagId
        {
            get { return _MemberTagId; }
            set { _MemberTagId = value; }
        }

        /// <summary>
        /// Gets/sets the member tag value.
        /// </summary>
        public string MemberTagValue
        {
            get { return _MemberTagValue; }
            set { _MemberTagValue = value; }
        }

        /// <summary>
        /// Compares an object.
        /// </summary>
        /// <param name="obj">The object to compare to.</param>
        /// <returns>true if the objects are equal, false if not.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is CommentMemberTagData)) return false;

            CommentMemberTagData temp = (CommentMemberTagData)obj;

            return ToString().Equals(temp.ToString());
        }


        #region IEquatable<CommentMemberTagData> Members

        public bool Equals(CommentMemberTagData other)
        {
            return ToString().Equals(other.ToString());
        }

        #endregion

        /// <summary>
        /// Converts the object to a string.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return _MemberTagId + ": " + _MemberTagValue;
        }

        /// <summary>
        /// Return the hashcode of the object.
        /// </summary>
        /// <returns>The int hashcode of the object.</returns>
        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="memberTagId">Array of member tag ids.</param>
        ///// <param name="memberTagValue">Array of member tag values.</param>
        ///// <returns>An array of CommentMemberTagData.</returns>
        //public static CommentMemberTagData[] CreateArrayOfCommentMemberTags(string[] memberTagId, 
        //    string[] memberTagValue)
        //{
        //    CommentMemberTagData[] comments = null;
        //    if (memberTagId != null && memberTagValue != null && memberTagId.Length == memberTagValue.Length)
        //    {
        //        comments = new CommentMemberTagData[memberTagId.Length];
        //        for (int i = 0; i < comments.Length; i++)
        //        {
        //            comments[i] = new CommentMemberTagData();
        //            if (!String.IsNullOrEmpty(memberTagId[i]))
        //            {
        //                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(memberTagId[i]);
        //                if (tagDef != null)
        //                {
        //                    comments[i].MemberTagId = tagDef.label;
        //                }
        //            }
        //            else
        //            {
        //                comments[i].MemberTagId = String.Empty;
        //            }

        //            if (!String.IsNullOrEmpty(memberTagValue[i]))
        //            {
        //                comments[i].MemberTagValue = memberTagValue[i];
        //            }
        //            else
        //            {
        //                comments[i].MemberTagValue = String.Empty;
        //            }
        //        }
        //    }
        //    return comments;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberTagIdToInclude">List of member tag id to include from the member tag id array.</param>
        /// <param name="memberTagId">Array of member tag ids.</param>
        /// <param name="memberTagValue">Array of member tag values.</param>
        /// <returns>An array of CommentMemberTagData.</returns>
        public static CommentMemberTagData[] CreateArrayOfCommentMemberTags(
            List<string> memberTagIdToInclude,
            string[] memberTagId,
            string[] memberTagValue)
        {
            List<CommentMemberTagData> comments = new List<CommentMemberTagData>();
            if (memberTagId != null && memberTagValue != null && memberTagId.Length == memberTagValue.Length)
            {
                int i = 0;
                while (i < memberTagId.Length)
                {
                    if (memberTagId[i] != null && memberTagIdToInclude.Contains(memberTagId[i]))
                    {
                        if (!String.IsNullOrEmpty(memberTagId[i]))
                        {
                            memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(memberTagId[i]);
                            if (tagDef != null)
                            {
                                if (String.IsNullOrEmpty(memberTagValue[i]))
                                {
                                    comments.Add(new CommentMemberTagData(tagDef.name, String.Empty));
                                }
                                else
                                {
                                    comments.Add(new CommentMemberTagData(tagDef.name, memberTagValue[i]));
                                }
                            }
                        }
                    }
                    i++;
                }
            }
            return comments.ToArray();
        }

        /// <summary>
        /// Create the Comment member tag string.
        /// </summary>
        /// <param name="values">Value to go in the comment member tag.</param>
        public static string GetUpdatedCommentMemberTagString(List<List<CommentMemberTagData>> values)
        {
            if (values == null || values.Count == 0)
            {
                return String.Empty;
            }

            List<CommentMemberTagData> array = new List<CommentMemberTagData>();

            foreach (List<CommentMemberTagData> list in values)
            {
                foreach (CommentMemberTagData value in list)
                {
                    array.Add(value);
                }
            }

            if (array.Count > 0)
            {
                return GetUpdatedCommentMemberTagString(array.ToArray());
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Create the Comment member tag string.
        /// </summary>
        /// <param name="value">Value to go in the comment member tag.</param>
        public static string GetUpdatedCommentMemberTagString(CommentMemberTagData[] value)
        {
            //build the comment string
            StringBuilder sb = new StringBuilder(String.Empty);
            if (value != null && value.Length > 0)
            {
                foreach (CommentMemberTagData v in value)
                {
                    sb.Append(v + PafAppConstants.NL);
                }
            }
            return sb.ToString().Trim();
        }

        /// <summary>
        /// Inspects the values of a CommentMemberTagData[] to see if the values are all blank.
        /// </summary>
        /// <param name="values">the value array</param>
        /// <returns>true if the array contains all blank values, false if not.</returns>
        public static bool AreValuesBlank(CommentMemberTagData[] values)
        {
            if (values != null && values.Length > 0)
            {
                foreach (CommentMemberTagData v in values)
                {
                    if(!String.IsNullOrEmpty(v.MemberTagValue))
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        #region IComparable<CommentMemberTagData> Members

        /// <summary>
        /// Overloaded CompareTo
        /// </summary>
        /// <param name="other">CommentMemberTagData</param>
        /// <returns>CompareOperator</returns>
        public int CompareTo(CommentMemberTagData other)
        {
            if(other == null)
            {
                return 1;
            }
            if(_MemberTagId == null)
            {
                return -1;
            }

            return String.Compare(_MemberTagId, other.MemberTagId, true);

        }

        #endregion

    }
}