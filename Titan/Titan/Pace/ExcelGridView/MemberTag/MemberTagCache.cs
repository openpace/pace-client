#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.Pace.DataStructures;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.ExcelGridView.MemberTag
{
    internal class MemberTagCache
    {
        /// <summary>
        /// member tag name, intersection, value
        /// Cache of members that need to be saved, updated.
        /// </summary>
        private readonly CachedDictionary<string, Intersection, string> _MbrTagDirtyCache;

        /// <summary>
        /// Cache of member that have been saved.
        /// </summary>
        private readonly CachedDictionary<string, Intersection, string> _MbrCacheSaved;

        /// <summary>
        /// Member tag cache.
        /// </summary>
        private readonly CachedDictionary<string, Intersection, string> _MbrTagCache;

        /// <summary>
        /// Member tag modified cache.
        /// </summary>
        private readonly CachedDictionary<string, Intersection, string> _MbrTagModifiedCache;

        /// <summary>
        /// The is dirty flag.
        /// </summary>
        private bool _IsDirty = false;

        public MemberTagCache()
        {
            _MbrTagDirtyCache = new CachedDictionary<string, Intersection, string>();
            _MbrCacheSaved = new CachedDictionary<string, Intersection, string>();
            _MbrTagCache = new CachedDictionary<string, Intersection, string>();
            _MbrTagModifiedCache = new CachedDictionary<string, Intersection, string>();
        }

        /// <summary>
        /// The client member tag cache of member that need to be saved, or updated.
        /// </summary>
        public CachedDictionary<string, Intersection, string> ClientMemberTagDirtyCache
        {
            get { return _MbrTagDirtyCache; }
        }


        /// <summary>
        /// The client member tag cache.
        /// </summary>
        public CachedDictionary<string, Intersection, string> ClientMemberTagCache
        {
            get { return _MbrTagCache; }
        }

        /// <summary>
        /// Gets a dictionary of server modified modified member tags.
        /// </summary>
        /// <param name="memberTagName">name of the member tag.</param>
        /// <returns>dictionary of Intersection/string value, or null.</returns>
        public Dictionary<Intersection, string> ServerModifiedMemberTags(string memberTagName)
        {
            if (_MbrTagModifiedCache.ContainsKey(memberTagName))
            {
                return _MbrTagModifiedCache.Values(memberTagName);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Is the cache dirty or not.
        /// </summary>
        public bool IsDirty
        {
            get { return _IsDirty; }
            set { _IsDirty = value; }
        }

        /// <summary>
        /// Are their members in the saved cached.
        /// </summary>
        public bool SavedMembers
        {
            get
            {
                if (_MbrCacheSaved != null && _MbrCacheSaved.Count() > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// Clears all the caches (Called when a saved occures).
        /// </summary>
        public void Clear()
        {
            MovedToSaved();
            _MbrTagDirtyCache.Clear();
            _MbrTagCache.Clear();
            _MbrTagModifiedCache.Clear();
            _IsDirty = false;
        }

        /// <summary>
        /// Resets the member tag cache, and sets the dirty cache to false.
        /// Called when a reload occures.
        /// </summary>
        public void Reset()
        {
            _MbrTagDirtyCache.Clear();
            _MbrCacheSaved.Clear();
            _IsDirty = false;
        }

        /// <summary>
        /// Add/update a value in the member tag value cache.
        /// </summary>
        /// <param name="memberTagName">Name of the member tag.</param>
        /// <param name="key">Key value to add/update</param>
        /// <param name="value">Value to add/update</param>
        public void AddUpdate(string memberTagName, Intersection key, string value)
        {
            if (_MbrTagDirtyCache.ContainsKey(memberTagName))
            {
                _MbrTagDirtyCache.CacheAddSelections(memberTagName, key, new string[] { value }, true);
            }
            else
            {
                _MbrTagDirtyCache.CacheSelection(memberTagName, key, new string[] { value });
            }
            RecalcDirty();
        }

        /// <summary>
        /// Removes a value from the delete cache.
        /// </summary>
        /// <param name="key">Name of the member tag selection to remove</param>
        /// <returns>true if removed, false if not or if key is null.</returns>
        public void RemoveDelete(string key)
        {
            if (!String.IsNullOrEmpty(key))
            {
                CachedDictionary<string, Intersection, string> dictionary = _MbrTagDirtyCache;// GetDictionary(MemberTagCacheType.Delete);
                dictionary.RemoveSelection(key);
            }
            RecalcDirty();
        }

        /// <summary>
        /// Get all the changed and added intersections for a certain member tag.
        /// </summary>
        /// <param name="memberTagName">Name of the member tag.</param>
        /// <returns>a dictionary of values, or null if no changes exist.</returns>
        public Dictionary<Intersection, string> GetChangedValues(string memberTagName)
        {
            if (_MbrTagDirtyCache != null && _MbrTagDirtyCache.Count() > 0)
            {
                return _MbrTagDirtyCache.Values(memberTagName);
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Get all the changed and added intersections for a certain member tag.
        /// </summary>
        /// <param name="memberTagName">Name of the member tag.</param>
        /// <returns>a dictionary of values, or null if no changes exist.</returns>
        public Dictionary<Intersection, string> GetAllValues(string memberTagName)
        {
            Dictionary<Intersection, string> ret = null;
            if (_MbrTagDirtyCache != null)
            {
                Dictionary<Intersection, string> m1 = _MbrTagDirtyCache.Values(memberTagName);
                ret = new Dictionary<Intersection, string>();

                if (_MbrCacheSaved.Values(memberTagName) != null)
                {
                    foreach (KeyValuePair<Intersection, string> kvp in _MbrCacheSaved.Values(memberTagName))
                    {
                        ret.Add(kvp.Key, kvp.Value);
                    }
                }

                if (m1 != null)
                {
                    foreach (KeyValuePair<Intersection, string> kvp in m1)
                    {
                        if (!ret.ContainsKey(kvp.Key))
                        {
                            ret.Add(kvp.Key, kvp.Value);
                        }
                        {
                            ret.Remove(kvp.Key);
                            ret.Add(kvp.Key, kvp.Value);
                        }
                    }
                }
            }
            return ret;
        }


        /// <summary>
        /// Get all the changed and added intersections for a certain member tag.
        /// </summary>
        /// <returns>a dictionary of values, or null if no changes exist.</returns>
        public Dictionary<Intersection, string> GetAllValues()
        {
            Dictionary<Intersection, string> ret = null;
            if (_MbrTagDirtyCache != null)
            {
                Dictionary<string, Dictionary<Intersection, string>>.KeyCollection keys = _MbrTagDirtyCache.Keys;
                foreach (string key in keys)
                {
                    Dictionary<Intersection, string> m1 = _MbrTagDirtyCache.Values(key);
                    ret = new Dictionary<Intersection, string>();

                    if (_MbrCacheSaved.Values(key) != null)
                    {
                        foreach (KeyValuePair<Intersection, string> kvp in _MbrCacheSaved.Values(key))
                        {
                            ret.Add(kvp.Key, kvp.Value);
                        }
                    }

                    if (m1 != null)
                    {
                        foreach (KeyValuePair<Intersection, string> kvp in m1)
                        {
                            if (!ret.ContainsKey(kvp.Key))
                            {
                                ret.Add(kvp.Key, kvp.Value);
                            }
                            {
                                ret.Remove(kvp.Key);
                                ret.Add(kvp.Key, kvp.Value);
                            }
                        }
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// Adds a value to the member tag cache.
        /// </summary>
        /// <param name="memberTagName">Member tag name.</param>
        /// <param name="intersection">Intersection</param>
        /// <param name="value">Value to add.</param>
        /// <returns>true if a modified value was sent down from the server and added to the "ServerModified" collection.</returns>
        public bool AddMemberToCache(string memberTagName, Intersection intersection, string value)
        {
            string val;
            bool ret = false;
            if (!_MbrTagCache.TryGetValue(memberTagName, intersection, out val))
            {
                _MbrTagCache.CacheSelection(memberTagName, intersection, value);
            }
            else
            {
                if (_MbrTagCache.TryGetValue(memberTagName, intersection, out val))
                {
                    if (!value.ToLower().Equals(val.ToLower()))
                    {
                        _MbrTagModifiedCache.CacheSelection(memberTagName, intersection, value);
                        ret = true;
                    }
                    //TTN-1971
                    else
                    {
                        _MbrTagModifiedCache.RemoveSelection(memberTagName, intersection);
                    }
                }
            }
            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        private void RecalcDirty()
        {
            if(_MbrTagDirtyCache != null && _MbrTagDirtyCache.Count() > 0)
            {
                _IsDirty = true;
            }
            else
            {
                _IsDirty = false;
            }
        }

        /// <summary>
        /// Moved the temporary dictionary to the saved dictionary.
        /// </summary>
        private void MovedToSaved()
        {
            foreach(string k in _MbrTagDirtyCache.Keys)
            {
                Dictionary<Intersection, string> values = GetChangedValues(k);
                
                foreach (KeyValuePair<Intersection, string> kvp in values)
                {
                    _MbrCacheSaved.CacheSelection(k, kvp.Key, kvp.Value);
                }
            }
        }
    }
}