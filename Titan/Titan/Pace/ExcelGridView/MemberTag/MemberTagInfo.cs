#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Pace.ExcelGridView.MemberTag
{
    /// <summary>
    /// Class to extract information about member tags.
    /// </summary>
    internal class MemberTagInfo
    {
        private readonly Dictionary<string, memberTagDef> _MemberTags;

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="pafSimpleMbrTagDefs">Array of pafSimpleMemberTagDef</param>
        public MemberTagInfo(memberTagDef[] pafSimpleMbrTagDefs)
        {
            _MemberTags = new Dictionary<string, memberTagDef>();
            BuildMemberTagDictionary(pafSimpleMbrTagDefs);
        }

        /// <summary>
        /// Builds the member tag cache dictionary.
        /// </summary>
        /// <param name="pafSimpleMbrTagDefs">Array of pafSimpleMemberTagDef</param>
        private void BuildMemberTagDictionary(memberTagDef[] pafSimpleMbrTagDefs)
        {
            if(pafSimpleMbrTagDefs != null && pafSimpleMbrTagDefs.Length > 0)
            {
                foreach (memberTagDef psmtd in pafSimpleMbrTagDefs)
                {
                    if (psmtd != null)
                    {
                        _MemberTags.Add(psmtd.name, psmtd);
                    }
                }
            }
        }

        /// <summary>
        /// Gets a pafSimpleMemberTagDef
        /// </summary>
        /// <param name="name">name of the member tag def.</param>
        /// <returns>the tag def if it exists, null if not.</returns>
        public memberTagDef GetMemberTag(string name)
        {
            if (! String.IsNullOrEmpty(name))
            {
                memberTagDef tag;
                if(_MemberTags.TryGetValue(name, out tag))
                {
                    return tag;
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the name of the member tag from an array of dims that make up a member tag.
        /// </summary>
        /// <param name="dims">Array of dimensions.</param>
        /// <returns>the name of the member tag, or empty string.</returns>
        public string GetMemberTagName(string[] dims)
        {
            if (_MemberTags != null && _MemberTags.Count > 0)
            {
                foreach (KeyValuePair<string, memberTagDef> kvp in _MemberTags)
                {
                    if(kvp.Value.dims.Length == dims.Length)
                    {
                        int i = 0;
                        foreach(string dim in kvp.Value.dims)
                        {
                            if(!dim.Equals(dims[i]))
                            {
                                break;
                            }
                            i++;
                        }
                        return kvp.Key;
                    }
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// Is the name of the member tag name a valid member tag def.
        /// </summary>
        /// <param name="name">name of the valid member tag def.</param>
        /// <returns>true if the member tag def is valid, false if not.</returns>
        public bool isMemberTag(string name)
        {
            memberTagDef tag = GetMemberTag(name);
            if (tag != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets all the member tag defs. 
        /// </summary>
        /// <returns>all the tag def if they exists, null if not.</returns>
        public memberTagDef[] GetMemberTags()
        {
            if (_MemberTags.Count > 0)
            {
                memberTagDef[] arr = new memberTagDef[_MemberTags.Count];
                _MemberTags.Values.CopyTo(arr, 0);
                return arr;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets the member tag isEditable status.
        /// </summary>
        /// <param name="name">name of the member tag.</param>
        /// <returns>true if the member tag def is edibale, false if not.</returns>
        public bool isEditable(string name)
        {
            memberTagDef tag = GetMemberTag(name);
            if (tag != null)
            {
                return tag.editable;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the member tag isCommentVisible status.
        /// </summary>
        /// <param name="name">name of the member tag.</param>
        /// <returns>true if the member tag isCommentVisible, false if not.</returns>
        public bool isCommentVisible(string name)
        {
            memberTagDef tag = GetMemberTag(name);
            if (tag != null)
            {
                return tag.commentVisible;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the member tag label.
        /// </summary>
        /// <param name="name">name of the member tag.</param>
        /// <returns>the member tag label, String.Empty if the member tag name if not found.</returns>
        public string label(string name)
        {
            memberTagDef tag = GetMemberTag(name);
            if (tag != null)
            {
                if (tag.label != null)
                {
                    return tag.label;
                }
                else
                {
                    return String.Empty;
                }
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the member tag type.
        /// </summary>
        /// <param name="name">name of the member tag</param>
        /// <returns>the member tag type, null if the member tag name if not found.</returns>
        public memberTagType GetMemberTagType(string name)
        {
            memberTagDef tag = GetMemberTag(name);
            if (tag != null)
            {
                return tag.type; 
            }
            else
            {
                return default(memberTagType);
            }
        }

        /// <summary>
        /// Converts the Paf.Server.memberTagType enum 
        /// because the server is too lazy to do it by it's self.
        /// </summary>
        /// <param name="mbrTagType">memberTagType</param>
        /// <returns>a member tag string constats.</returns>
        public string ConvertMemberTagType(memberTagType mbrTagType)
        {
            switch(mbrTagType)
            {
                case memberTagType.FORMULA:
                    return PafAppConstants.MEMBER_TAG_TYPE_FORMULA;
                case memberTagType.HYPERLINK:
                    return PafAppConstants.MEMBER_TAG_TYPE_HYPERLINK;
                case memberTagType.TEXT:
                    return PafAppConstants.MEMBER_TAG_TYPE_TEXT;
            }
            return String.Empty;
        }
    }
}