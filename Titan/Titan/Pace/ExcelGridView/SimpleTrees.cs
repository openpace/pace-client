#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// Class to hold multiple PafSimpleTrees.  This allows you to access the tree by dimension name or index.
    /// </summary>
    [Obsolete("Replaced by SimpleDimTrees", false)]
    internal class SimpleTrees
    {
        #region Private Variables
        /// <summary>
        /// Structure to track names of trees.
        /// </summary>
        private Dictionary<string, int> _TreeNameHash;

        /// <summary>
        /// List to store the selectability of the SimpleTrees.
        /// </summary>
        private List<bool> _TreeSelectable;

        /// <summary>
        /// List to store the PafSimpleTrees.
        /// </summary>
        private List<pafSimpleDimTree> _Trees;

        /// <summary>
        /// List to store the SimpleTreeMemebers (hashed PafSimpleMembers).
        /// </summary>
        private List<SimpleTreeMembers> _TreeMembers;

        Dictionary<string,Dictionary<string, int>> _TraversedMembers;

        #endregion Private Variables

        #region Constructor
        /// <summary>
        /// Constructor.
        /// </summary>
        public SimpleTrees()
        {
            _Trees = new List<pafSimpleDimTree>();
            _TreeNameHash = new Dictionary<string, int>();
            _TreeSelectable = new List<bool>();
            _TreeMembers = new List<SimpleTreeMembers>();
            _TraversedMembers = new Dictionary<string, Dictionary<string, int>>();
        }

        #endregion Constructor

        #region Private Members

        /// <summary>
        /// Removes an item from the list and the hash.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <param name="index">Tree's hash index.</param>
        private void Remove(string treeName, int index)
        {
            _Trees.RemoveAt(index);
            _TreeNameHash.Remove(treeName);
            _TreeSelectable.RemoveAt(index);
            _TreeMembers.RemoveAt(index);
        }

        #region Private Members

        private SimpleTreeMembers GetTreeMembers(string treeName)
        {
            if (String.IsNullOrEmpty(treeName)) return null;

            int i = _TreeNameHash[treeName];

            if (i > -1)
            {
                return _TreeMembers[i];

            }
            return null;
        }

        #endregion Private Members

        #endregion Private Members

        #region Public Members
        /// <summary>
        /// Add a PafSimpleTree to the objects internal list.
        /// </summary>
        /// <param name="item">PafSimpleTree object to insert.</param>
        /// <param name="treeName">Case sensitive name of tree.  Can be used to access tree.</param>
        /// <param name="isSelectable">Can the user select from this dimension.</param> 
        public void Add(pafSimpleDimTree item, string treeName, bool isSelectable)
        {
            _Trees.Add(item);
            _TreeNameHash.Add(treeName, _Trees.Count - 1);
            _TreeSelectable.Insert(_Trees.Count - 1, isSelectable );
            _TreeMembers.Add(new SimpleTreeMembers(item));
        }

        /// <summary>
        /// Finds the children of a parent within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        public string[] GetChildren(string treeName, string parent)
        {

            SimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if(simpleTree == null) return null;

            return simpleTree.GetChildren(parent);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="treeName"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public string GetParent(string treeName, string child)
        {

            SimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if (simpleTree != null)
            {
                pafSimpleDimMember simpleMember = simpleTree.GetPafSimpleMember(child);
                if (simpleMember != null)
                {
                    return simpleMember.parentKey;
                }
            }
            return null;
        }

        /// <summary>
        /// Finds the children of a parent within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        public SimpleTreeChildMembers[] GetChildrenAlias(string treeName, string parent, string aliasTable)
        {

            SimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            return simpleTree.GetChildrenAliases(aliasTable, parent);
        }

        /// <summary>
        /// Finds the alias of a member within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member">The name of the member, for which you want the alias</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>The alias string</returns>
        public string GetAlias(string treeName, string member, string aliasTable)
        {

            SimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if (simpleTree == null) return null;

            return simpleTree.GetPafSimpleMemeberAliasValue(member, aliasTable);
            //return simpleTree.GetPafSimpleMemeberAliasValue(member, "Default");
        }

        /// <summary>
        /// Finds the a member within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member">The name of the member, for which you want the alias</param>
        /// <returns>A pafSimpleDimMember</returns>
        public pafSimpleDimMember GetMember(string treeName, string member)
        {

            SimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if (simpleTree == null) return null;

            return simpleTree.GetPafSimpleMember(member);
        }


        /// <summary>
        /// Returns an array of aliases for an array of Paf Simple Members.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="members">The array of member names for which you want the aliases</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>The alias string</returns>
        public string[] GetAlias(string treeName, string[] members, string aliasTable)
        {

            SimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if (simpleTree == null) return null;

            return simpleTree.GetPafSimpleMemeberAliasValue(members, aliasTable);
            //return simpleTree.GetPafSimpleMemeberAliasValue(members, "Default");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="treeName"></param>
        /// <returns></returns>
        public string GetFirstLevelZeroMember(string treeName)
        {
            SimpleTreeMembers simpleTree = _TreeMembers[_TreeNameHash[treeName]];

            foreach (KeyValuePair<string, int> member in GetTraversedMembers(treeName))
            {
                if (simpleTree.GetPafSimpleMember(member.Key).pafSimpleDimMemberProps.levelNumber == 0)
                {
                    string memberName = member.Key;
                    return memberName;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the Prev or Next member at the same level within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member"></param>
        /// <param name="offset"></param>
        /// <returns>A string member name</returns>
        public string GetOffset(string treeName, string member, int offset)
        {
            //var st = Stopwatch.StartNew();
            int offsetCounter = 0;
            pafSimpleDimTree tree = GetTree(treeName);
            
            if (GetTraversedMembers(treeName).ContainsKey(member))
            {
                int memberIndex = GetTraversedMembers(treeName)[member];
                int memberCounter = memberIndex;

                //find the offset in the array
                if (offset == 0)
                {
                    //st.Stop();
                    //PafApp.GetLogger().InfoFormat("GetOffset runtime: {0} (ms)", new object[] { st.ElapsedMilliseconds.ToString("N0") });
                    return member;
                }
                else if (offset < 0)
                {
                    while (memberCounter > 0)
                    {
                        memberCounter--;
                        SimpleTreeMembers simpleTree = _TreeMembers[_TreeNameHash[treeName]];

                        if (simpleTree.GetPafSimpleMember(tree.traversedMembers[memberIndex]).pafSimpleDimMemberProps.levelNumber == 
                            simpleTree.GetPafSimpleMember(tree.traversedMembers[memberCounter]).pafSimpleDimMemberProps.levelNumber)
                        {
                            offsetCounter--;
                        }

                        if (offsetCounter == offset)
                        {
                            //st.Stop();
                            //PafApp.GetLogger().InfoFormat("GetOffset runtime: {0} (ms)", new object[] { st.ElapsedMilliseconds.ToString("N0") });
                            return tree.traversedMembers[memberCounter];
                        }
                    }
                }
                else if (offset > 0)
                {
                    while (memberCounter < tree.traversedMembers.Length - 1)
                    {
                        memberCounter++;
                        SimpleTreeMembers simpleTree = _TreeMembers[_TreeNameHash[treeName]];

                        if (simpleTree.GetPafSimpleMember(tree.traversedMembers[memberIndex]).pafSimpleDimMemberProps.levelNumber == 
                            simpleTree.GetPafSimpleMember(tree.traversedMembers[memberCounter]).pafSimpleDimMemberProps.levelNumber)
                        {
                            offsetCounter++;
                        }

                        if (offsetCounter == offset)
                        {
                            //st.Stop();
                            //PafApp.GetLogger().InfoFormat("GetOffset runtime: {0} (ms)", new object[] { st.ElapsedMilliseconds.ToString("N0") });
                            return tree.traversedMembers[memberCounter];
                        }
                    }
                }
            }

            //st.Stop();
            //PafApp.GetLogger().InfoFormat("GetOffset runtime: {0} (ms)", new object[] { st.ElapsedMilliseconds.ToString("N0") });

            //return "" if the member is not in the tree.  E.g. - the Member Time
            return String.Empty;
        }

        /// <summary>
        /// Gets an array of tree names.
        /// </summary>
        /// <returns>An array of dimension names</returns>
        public string[] GetTreeNames()
        {
            string []names = null;
            _TreeNameHash.Keys.CopyTo(names, 0);

            return names;
        }

        /// <summary>
        /// Returns a PafSimpleTree, with a specific name.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <returns>A PafSimpleTree</returns>
        public pafSimpleDimTree GetTree(int index)
        {
            try
            {
                return _Trees[index];
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns a PafSimpleTree, with a specific name.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to return.</param>
        /// <returns>A PafSimpleTree</returns>
        public pafSimpleDimTree GetTree(string treeName)
        {
            try
            {
                int i = _TreeNameHash[treeName];
                return _Trees[i];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Dictionary<string, int> GetTraversedMembers(string treeName)
        {
            try
            {
                if (_TraversedMembers == null)
                {
                    _TraversedMembers = new Dictionary<string, Dictionary<string, int>>();
                }

                if (! _TraversedMembers.ContainsKey(treeName))
                {
                    Dictionary<string, int> members = new Dictionary<string, int>();

                    int i = 0;
                    foreach(string travMember in _Trees[_TreeNameHash[treeName]].traversedMembers)
                    {
                        members.Add(travMember, i++);
                    }

                    _TraversedMembers.Add(treeName, members);
                }
           
                return _TraversedMembers[treeName];
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to return if the tree is selectable.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <returns>Returns a boolean if the tree is selectable.</returns>
        public bool IsTreeSelectable(string treeName)
        {
            try
            {
                int i = _TreeNameHash[treeName];
                return _TreeSelectable[i];
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the selectability of the SimpleTree.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <param name="isSelectable">Select or deselect the tree.</param>
        public void TreeSelectable(int index, bool isSelectable)
        {
            try
            {
                _TreeSelectable[index] = isSelectable;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Sets the selectability of the SimpleTree.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <param name="isSelectable">Select or deselect the tree.</param>
        public void TreeSelectable(string treeName, bool isSelectable)
        {
            try
            {
                int i = _TreeNameHash[treeName];
                _TreeSelectable[i] = isSelectable;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Method to return if the tree is selectable.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <returns>Returns a boolean if the tree is selectable.</returns>
        public bool IsTreeSelectable(int index)
        {
            try
            {
                return _TreeSelectable[index];
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Enumerator accessor for the SImple Tree
        /// </summary>
        /// <returns>Enumerator for the SimpleTree</returns>
        public MyEnumerator GetEnumerator() 
        {
            return new MyEnumerator(_Trees);
        }

        /// <summary>
        /// Removes an item at a specific location.
        /// </summary>
        /// <param name="index">The location to remove the item</param>
        public void RemoveAt(int index)
        {
            if (index > -1)
            {
                string treeName = _Trees[index].id;

                if (!String.IsNullOrEmpty(treeName))
                {
                    Remove(treeName, index);
                }
            }
        }

        /// <summary>
        /// Removes an item with a specific name.
        /// </summary>
        /// <param name="treeName">The name of the tree to remove.</param>
        public void Remove(string treeName)
        {
            if (String.IsNullOrEmpty(treeName)) return;

            int index = _TreeNameHash[treeName];
            Remove(treeName, index);
        }

        /// <summary>
        /// Removes all elements from the SimpleTree structure.
        /// </summary>
        public void Clear()
        {
            if(_Trees != null)
                _Trees.Clear();

            if(_TreeNameHash != null)
                _TreeNameHash.Clear();

            if(_TreeSelectable != null)
                _TreeSelectable.Clear();

            if(_TreeMembers != null)
                _TreeMembers.Clear();

            if(_TraversedMembers != null)
                _TraversedMembers.Clear();
        }

        /// <summary>
        /// Returns the capacity of the internal list.
        /// </summary>
        /// <returns>int containg the capacity of the internal list.</returns>
        public int Capacity()
        {
            return _Trees.Capacity;
        }

        /// <summary>
        /// Returns the number of items contained in the internal list.
        /// </summary>
        /// <returns>Integer with the count of the items in the list.</returns>
        public int Count()
        {
            return _Trees.Count;
        }

        #endregion Public Members

        #region myenumerator
        /// <summary>
        /// Enumerator for the SimpleTree class.
        /// </summary>
        internal class MyEnumerator
        {
            private int nIndex;
            private List<pafSimpleDimTree> collection;
            public MyEnumerator(List<pafSimpleDimTree> coll)
            {
                collection = coll;
                nIndex = -1;
            }
            public bool MoveNext()
            {
                nIndex++;
                return (nIndex < collection.Count);
            }
            public pafSimpleDimTree Current
            {
                get
                {
                    return (collection[nIndex]);
                }
            }
        }
        #endregion myenumerator
        
    }
}