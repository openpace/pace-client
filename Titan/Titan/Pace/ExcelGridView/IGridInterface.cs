#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;
using IPrintStytle = Titan.Pace.Application.Core.IPrintStyle;
using Range = Titan.Pace.ExcelGridView.Utility.Range;

namespace Titan.Pace.ExcelGridView
{
    internal interface IGridInterface
	{
        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="textArray">Values to update the cells with.</param>
        void SetValueDoubleArray(ContiguousRange contigRng, double[,] textArray);

        ///// <summary>
        ///// Gets the values of a range of contiguous cells.
        ///// </summary>
        ///// <param name="contigRng">The range of cells to retrieve.</param>
        ///// <returns>An object containing the values from the grid.</returns>
        //object[,] GetValueDoubleArray(ContiguousRange contigRng);

        /// <summary>
        /// Gets the values of a range of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to retrieve.</param>
        /// <returns>An string array containing the values from the grid.</returns>
        object[,] GetValueObjectArray(ContiguousRange contigRng);

        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="objectArray">Values to update the cells with.</param>
        void SetValueObject(ContiguousRange contigRng, Object objectArray);

        /// <summary>
        /// Update an array of formula cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="objectFormulaArray">Formuals to update the cells with.</param>
        void SetFormulaArray(ContiguousRange contigRng, Object objectFormulaArray);

        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="ranges">The range of cells to update.</param>
        /// <param name="objectVal">Values to update the cells with.</param>
        void SetValueObject(List<Range> ranges, Object objectVal);

        /// <summary>
        /// Set the string value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <param name="text">String to place into the cell.</param>
        void SetValueText(int row, int col, string text);


        /// <summary>
        /// Gets the formula value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <returns>The formula of the cell as a string.</returns>
        object GetValueFormula(int row, int col);

        /// <summary>
        /// Set the value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <param name="value">Double to place into the cell.</param>
        void SetValueDouble(int row, int col, double value);

        /// <summary>
        /// Gets the value of a single cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <returns>The value of the cell.</returns>
        double GetValueDouble(int row, int col);

        /// <summary>
        /// Update an array of contiguous cells.
        /// </summary>
        /// <param name="contigRng">The range of cells to update.</param>
        /// <param name="textArray">Values to update the cells with.</param>
        void SetValueTextArray(ContiguousRange contigRng, string[,] textArray);

        /// <summary>
        /// Update an list of ranges with a single text value.
        /// </summary>
        /// <param name="ranges">The range of cells to update.</param>
        /// <param name="value">Values to update the cells with.</param>
        void SetValueTextRange(List<Range> ranges, string value);

        /// <summary>
        /// Clears out the value of a list of ranges and locks the cells.
        /// </summary>
        /// <param name="ranges">List of ranges to clear.</param>
        /// <param name="lockRange">Lock the range from being updated.</param>
        void SetBlankFormats(List<Range> ranges, bool lockRange);

        /// <summary>
        /// Set a list of ranges as protected (changes interior color, and locks)
        /// </summary>
        /// <param name="range">List of ranges to set as protected.</param>
        /// <param name="color">Color that represents a proteced cell.</param>
        void SetProtectedFormats(Range range, Color color);

        /// <summary>
        /// Set a list of ranges as protected (changes interior color, and locks)
        /// </summary>
        /// <param name="ranges">List of ranges to set as protected.</param>
        /// <param name="c">Color that represents a proteced cell.</param>
        void SetProtectedFormats(List<Range> ranges, Color c);

        /// <summary>
        /// Sets the horizontal alignment of a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set alignment.</param>
        /// <param name="alignment">Alignment value name.</param>
        void SetHorizontalAlignment(List<Range> ranges, string alignment);

        /// <summary>
        /// Set the BgFillColor(interior.color) of a range of cell.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <param name="c">Name of the color.</param>
        void SetBgFillColor(List<Range> ranges, string c);

        /// <summary>
        /// Set the BgFillColor(interior.color) of a range of cell.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <param name="c">The Color color.</param>
        void SetBgFillColor(List<Range> ranges, Color c);

        /// <summary>
        /// Set the BgFillColor(interior.color) of a range of cell.
        /// </summary>
        /// <param name="range">The range.</param>
        /// <param name="c">The Color color.</param>
        void SetBgFillColor(Range range, Color c);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="row"></param>
        /// <param name="col"></param>
        /// <param name="bgFillColor"></param>
        /// <param name="isUnlocked"></param>
        /// <param name="numericFormat"></param>
        void GetCellFormatInformation(int row, int col, ref string bgFillColor, ref string isUnlocked,  ref string numericFormat);

        /// <summary>
        /// Set/clear the bold formatting of a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set/clear bold.</param>
        /// <param name="bold">String representation of boolean value.</param>
        void SetBold(List<Range> ranges, string bold);

        /// <summary>
        /// Sets the font color for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to format.</param>
        /// <param name="color">An hex color string.</param>
        void SetFontColor(List<Range> ranges, string color);

        /// <summary>
        /// Sets the font style for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to to format.</param>
        /// <param name="fontName">Name of font.</param>
        void SetFontName(List<Range> ranges, string fontName);

        /// <summary>
        /// Sets the italic status of the list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges</param>
        /// <param name="italics">String representation of boolean value.</param>
        void SetItalics(List<Range> ranges, string italics);

        /// <summary>
        /// Sets the italic status of the list of ranges.
        /// </summary>
        /// <param name="range">List of ranges</param>
        /// <param name="topMergedCell">The top cell in the merged cell, this is null if the range
        /// is not merged.</param>
        bool IsMerged(Range range, out Cell topMergedCell);

        /// <summary>
        /// Sets the font size on a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set the font size.</param>
        /// <param name="fontSize">New size of the font.</param>
        void SetFontSize(List<Range> ranges, double fontSize);

        /// <summary>
        /// Sets the strikeout value for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <param name="strikeout">Strikeout value.</param>
        void SetStrikeout(List<Range> ranges, string strikeout);
        
        /// <summary>
        /// Sets the underline on a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to update.</param>
        /// <param name="underline">String representation of the boolean value.</param>
        void SetUnderline(List<Range> ranges, string underline);

        /// <summary>
        /// Sets the double underline on a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to update.</param>
        /// <param name="doubleUnderline">String representation of the boolean value.</param>
        void SetDoubleUnderline(List<Range> ranges, string doubleUnderline);

        /// <summary>
        /// Sets the border to a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to update.</param>
        /// <param name="borders">The size of the border.</param>
        void SetBorders(List<Range> ranges, int borders);

        /// <summary>
        /// Sets the border to a list of ranges.
        /// </summary>
        /// <param name="range">The contiguous range to set the border.</param>
        /// <param name="borders">The size of the border.</param>
        void SetBorders(ContiguousRange range, int borders);

        /// <summary>
        /// Locks a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to lock.</param>
        void Lockcells(List<Range> ranges);

        /// <summary>
        /// Locks a Contiguous Range.
        /// </summary>
        /// <param name="range">Contiguous Range to lock.</param>
        void Lockcells(ContiguousRange range);

        /// <summary>
        /// Unlocks a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to unlock.</param>
        void UnLockCells(List<Range> ranges);

        /// <summary>
        /// Set a style to a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to set the styles.</param>
        /// <param name="format">The format to be applied to the list of ranges.</param>
        void SetStyles(List<Range> ranges, Format format);

        /// <summary>
        /// Set a style to a list of ranges.
        /// </summary>
        /// <param name="name">Name of the global style.</param>
        /// <param name="ranges">List of ranges to set the styles.</param>
        /// <param name="format">The format to be applied to the list of ranges.</param>
        void SetStyles(string name, List<Range> ranges, Format format);

        /// <summary>
        /// Sets the numeric format for a list of ranges.
        /// </summary>
        /// <param name="ranges">List of ranges to format.</param>
        /// <param name="format">Numeric format to apply.</param>
        void SetNumericFormat(List<Range> ranges, string format);

        /// <summary>
        /// Creats a group from a range.
        /// </summary>
        /// <param name="range">Ranges to update.</param>
        /// <param name="name">Name of the group.</param>
        /// <param name="level">Outline level.</param>
        object SetRowGrouping(ContiguousRange range, string name = null, int? level = null);

        /// <summary>
        /// Creats a group from a range.
        /// </summary>
        /// <param name="range">Ranges to update.</param>
        /// <param name="name">Name of the group.</param>
        /// <param name="level">Outline level.</param>
        object SetColumnGrouping(ContiguousRange range, string name = null, int? level = null);

        /// <summary>
        /// Gives the ability to flip the grouping layout
        /// </summary>
        /// <param name="viewAxis">Row or Column</param>
        /// <param name="parentFirst">Are parents listed before children</param>
        void SetOutlineSummaryLocation(ViewAxis viewAxis, bool parentFirst);

        /// <summary>
        /// Expands a column grouping one level
        /// </summary>
        /// <param name="cellAddress">Ranges to update.</param>
        bool ExpandColumnGrouping(CellAddress cellAddress);

        /// <summary>
        /// Expands a column grouping one level
        /// </summary>
        /// <param name="cellAddress">Ranges to update.</param>
        bool ExpandRowGrouping(CellAddress cellAddress);

        /// <summary>
        /// Sets the grouping levels
        /// </summary>
        /// <param name="rowLevel">Row Level</param>
        /// <param name="columnLevel">Column Level</param>
        void SetGroupingLevels(int? rowLevel, int? columnLevel);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="range"></param>
        /// <param name="criteria"></param>
        void SetConditionalFormatting(Range range, abstractPaceConditionalStyle criteria);


        /// <summary>
        /// Gets the numeric format for a cell.
        /// </summary>
        /// <param name="row">Row index.</param>
        /// <param name="col">Column index.</param>
        /// <returns>The numeric format for the cell.</returns>
        string GetNumericFormat(int row, int col);

        /// <summary>
        /// Merge a contiguous range of cells.
        /// </summary>
        /// <param name="r">The range to merge.</param>
        /// <param name="across">True to merge cells in each row of the specified range 
        /// as separate merged cells. The default value is False.</param>
        void SetMergedCells(ContiguousRange r, bool across);

        /// <summary>
        /// Shows/hides the column headers.
        /// </summary>
        /// <param name="show">true to show the heading, false to hide the headings.</param>
        void SetColumnHeader(bool show);

        /// <summary>
        /// Selects a cell.
        /// </summary>
        /// <param name="cell">The cell to select.</param>
        void Select(Cell cell);

         /// <summary>
        /// Selects a range.
        /// </summary>
        /// <param name="range">The range to select.</param>
        void Select(Range range);

        /// <summary>
        /// Get the curretnly active cell.
        /// </summary>
        /// <returns>The currently active cell.</returns>
		Cell GetActiveCell();

        /// <summary>
        /// Positions the Window based on the top left cell displayed in the window
        /// </summary>
        /// <param name="topLeftCell"></param>
        void SetWindowPosition(Cell topLeftCell);

        /// <summary>
        /// Gets the top left cell displayed in the window
        /// </summary>
        /// <returns></returns>
        Cell GetWindowPosition();

        /// <summary>
        /// Gets the first range if non-contigous ranges are selected
        /// </summary>
        /// <returns>A ContiguougRange of selected cells.</returns>
        Range GetSelectedCells();

        /// <summary>
        /// The number of cells in the current range selection
        /// </summary>
        /// <returns></returns>
        long GetSelectedCellCount();

        /// <summary>
        /// Enable/disable the firing of applicaiton events.
        /// </summary>
        /// <param name="flag">true to enable events, false to disable events.</param>
        void EnableEvents(bool flag);

        /// <summary>
        /// Clears all the contents of the sheet.
        /// </summary>
        void Clear();

        /// <summary>
        /// Clears all the groupings of the sheet.
        /// </summary>
        void ClearGroupings();

        /// <summary>
        /// Clears all the groupings of the sheet.
        /// </summary>
        /// <param name="viewAxis">Axis to clear groupings</param>
        /// <param name="level">Dept of Excel outline</param>
        void ClearGroupings( ViewAxis viewAxis, int level);

        /// <summary>
        /// Clears all the contents of a ContiguousRange.
        /// </summary>
        /// <param name="r">The ContiguousRange to clear.</param>
        void Clear(ContiguousRange r);

        /// <summary>
        /// Sets the autofit of a particular range.
        /// </summary>
        /// <param name="r">The ContiguousRange to autofit.</param>
        void AutoFitColumn(ContiguousRange r);

        /// <summary>
        /// Sets the autofit of a particular range.
        /// </summary>
        /// <param name="r">The ContiguousRange to autofit.</param>
        void AutoFitRow(ContiguousRange r);

        /// <summary>
        /// Gets the address of the range.
        /// </summary>
        /// <param name="r">The ContiguousRange to get the address of.</param>
        /// <returns>Gets the address of the ContiguousRange.</returns>
        string GetAddress(ContiguousRange r);

        /// <summary>
        /// Clears the print area for the current object.
        /// </summary>
        void ClearPrintRange();

        /// <summary>
        /// Sets the print range and properties of the current workbook.
        /// </summary>
        /// <param name="r">Range to set the print at.</param>
        /// <param name="printStyle">Print style.</param>
        void SetPrintRange(ContiguousRange r, IPrintStytle printStyle);

        /// <summary>
        /// Merge cell text
        /// </summary>
        /// <param name="r">Range</param>
        /// <param name="sheetPassword">Password</param>
        void Merge(ContiguousRange r, string sheetPassword);

        /// <summary>
        /// Merge cell text
        /// </summary>
        /// <param name="r"></param>
        /// <param name="viewAxis"></param>
        /// <param name="sheetPassword"></param>
        void Merge(ContiguousRange r, ViewAxis viewAxis, string sheetPassword);

        /// <summary>
        /// Unmerge a range.
        /// </summary>
        /// <param name="r">Range to unmerge.</param>
        /// <param name="sheetPassword">Sheet password.</param>
        void UnMerge(ContiguousRange r, string sheetPassword);

        /// <summary>
        /// Wrap cell text
        /// </summary>
        /// <param name="r"></param>
        void WrapText(ContiguousRange r);

        /// <summary>
        /// Set the height of an Excel row
        /// </summary>
        /// <param name="range"></param>
        /// <param name="rowHeight"></param>
        void SetRowHeight(Range range, float rowHeight);

        /// <summary>
        /// Set the width of an Excel column
        /// </summary>
        /// <param name="range"></param>
        /// <param name="colWidth"></param>
        void SetColumnWidth(Range range, float colWidth);

        /// <summary>
        /// Copies the value from 1 cell to another
        /// </summary>
        /// <param name="copyFromCell">The cell the data is copied from</param>
        /// <param name="copyToCell">The cell the data is copied to</param>
        void CopyCellValue(CellAddress copyFromCell, CellAddress copyToCell);

        /// <summary>
        /// Copies the formula from 1 cell to another
        /// </summary>
        /// <param name="copyFromCell">The cell the data is copied from</param>
        /// <param name="copyToCell">The cell the data is copied to</param>
        void CopyCellFormula(CellAddress copyFromCell, CellAddress copyToCell);

        /// <summary>
        /// Checks a range to see if it contains a cell note.
        /// </summary>
        /// <param name="range">Range to check.</param>
        /// <returns>true if the range contains a cell note, false if not or if the range is not valid.</returns>
        bool ContainsCellNote(Range range);

        /// <summary>
        /// Modifies a the text in a cell note.
        /// </summary>
        /// <param name="range">Range to set the cell note in.</param>
        /// <param name="text">The new text for the cell note.</param>
        /// <param name="overwriteExistingText">Overwrites the existings text in the cell note.</param>
        void ModifyCellNote(Range range, string text, bool overwriteExistingText);

        /// <summary>
        /// Sets a cell note in a range.
        /// </summary>
        /// <param name="range">Range to set the cell note in.</param>
        /// <param name="cellNote">Cell note to set in the range.</param>
        /// <param name="deleteExisingNote">Delete the existing cell note before inserting the new cell note.</param>
        /// <param name="boldFont">Bold the font.</param>
        void SetCellNote(Range range, simpleCellNote cellNote, bool deleteExisingNote, bool boldFont);

        /// <summary>
        /// Sets cell notes in a range.
        /// </summary>
        /// <param name="ranges">List of ranges to set the cell notes in.</param>
        /// <param name="cellNotes">List of cell notes to set in the ranges.</param>
        /// <param name="deleteExisingNote">Delete the existing cell note before inserting the new cell note.</param>
        /// <param name="boldFont">Bold the font.</param>
        void SetCellNotes(List<Range> ranges, List<simpleCellNote> cellNotes, bool deleteExisingNote, bool boldFont);

        /// <summary>
        /// Sets cell notes in a range.
        /// </summary>
        /// <param name="cellNotes">The dictionary of address/cell notes.</param>
        /// <param name="deleteExisingNote">Delete the existing cell note before inserting the new cell note.</param>
        /// <param name="boldFont">Bold the font.</param>
        void SetCellNotes(Dictionary<Range, simpleCellNote> cellNotes, bool deleteExisingNote, bool boldFont);

        /// <summary>
        /// Gets a cell note.
        /// </summary>
        /// <param name="range">Range to obtain the cell note from.</param>
        /// <returns></returns>
        simpleCellNote GetCellNote(Range range);

        /// <summary>
        /// Gets the cell note list.
        /// </summary>
        /// <returns>The entire list of cell notes for the grid.</returns>
        Dictionary<CellAddress, simpleCellNote> GetCellNotes();

        /// <summary>
        /// Deletes a list of cell note in a range.
        /// </summary>
        /// <param name="range">list of range to delete cell notes.</param>
        void DeleteCellNotes(List<Range> range);

        /// <summary>
        /// Deletes a cell note in a range.
        /// </summary>
        /// <param name="range">Range to delete the cell note in.</param>
        void DeleteCellNote(Range range);

        /// <summary>
        /// Deletes all the cell notes in a range.
        /// </summary>
        /// <param name="contigRng">Range to delete the cell note in.</param>
        void DeleteAllCellNotes(ContiguousRange contigRng);

        /// <summary>
        /// Sets a hyperlink in a range.
        /// </summary>
        /// <param name="range">Range to place the hyperlink.</param>
        /// <param name="address">Hyperlink address.</param>
        /// <param name="subAddress">Hyperlink subaddress.</param>
        /// <param name="screenTip">Hyperlink screen tip.</param>
        /// <param name="textToDisplay">Hyperlink text to disply in liew of the hyperlink.</param>
        void SetHyperlinks(Range range, string address, string subAddress,
            string screenTip, string textToDisplay);

        /// <summary>
        /// Deletes a hyperlink from a range of cells.
        /// </summary>
        /// <param name="range">Range to delete the hyperlink from.</param>
        void DeleteHyperlink(Range range);

        /// <summary>
        /// Clears all the hyperlinks off of a sheet.
        /// </summary>
        void ClearHyperlinks();

        /// <summary>
        /// Gets an array of hyperlinks for an entire grid.
        /// </summary>
        /// <returns>An array of hyperlinks.</returns>
        Hyperlinks GetHyperlinks();

        /// <summary>
        /// Grid(Excel) Version Number
        /// </summary>
        float Version { get;}

        /// <summary>
        /// Returns the name of the sheet.
        /// </summary>
        string Name { get; }
	}
}