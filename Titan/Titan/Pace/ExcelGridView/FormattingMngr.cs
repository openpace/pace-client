#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Titan.Pace.Application.Extensions;
using Titan.Pace.ExcelGridView.MemberTag;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Properties;
using Tuple = Titan.Pace.DataStructures.Tuple;

namespace Titan.Pace.ExcelGridView
{
    internal class FormattingMngr
    {
        private ViewStateInfo _CurrentView;

        internal ViewStateInfo CurrentView
        {
            [DebuggerHidden]
            get { return _CurrentView; }
            [DebuggerHidden]
            set { _CurrentView = value; }
        }

        /// <summary>
        /// Applies formatting to the view.
        /// </summary>
        public void ApplyFormatting(ViewStateInfo currentView, bool resetProtectionMngrFormatting)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            PafApp.GetLogger().Info("Start Formatting");

            CurrentView = currentView;

            OlapView olapView = currentView.GetOlapView();

            //Header Formatting.
            ApplyFormats(ExcelBuildView.FormatRange.Headers);

            //View Header Formatting.
            ApplyFormats(ExcelBuildView.FormatRange.ViewHeaders);

            if (!olapView.IsEmpty)
            {
                switch (olapView.PrimaryFormattingAxis)
                {
                    case ViewAxis.Row:
                        ApplyFormats(ExcelBuildView.FormatRange.DataRow);
                        ApplyFormats(ExcelBuildView.FormatRange.DataCol);
                        //ApplyConditionalFormats(ExcelBuildView.FormatRange.DataCol, condFormatName);
                        break;
                    case ViewAxis.Col:
                        ApplyFormats(ExcelBuildView.FormatRange.DataCol);
                        ApplyFormats(ExcelBuildView.FormatRange.DataRow);
                        //ApplyConditionalFormats(ExcelBuildView.FormatRange.DataRow, condFormatName);
                        break;
                    case ViewAxis.Page:
                    case ViewAxis.Unknown:
                        break;
                }

                ApplyConditionalFormat(olapView);
        

                if (resetProtectionMngrFormatting)
                {
                    //Apply Singleton Rules formatting
                    ApplySingletonRulesFormats();

                    //Apply BOH Protection Formatting
                    ApplyBOHProtectedFormats();

                    //Apply Session Lock Formatting
                    ApplySessionLockProtectedFormats(true);
                }
                else
                {
                    RestoreChangedCellFormatting();
                    RestoreUserLockedCellFormatting();
                    RestoreProtectedChangedCellFormatting();
                }

                //Forward Plannable and non-plannable formatting
                //This will over write Singleton Rules Formating
                ApplyPlannableFormats();

                //Format blanks
                ApplyBlankFormatting();

                //Restore the member tag formatting.
                RestoreMemberTagChangedCellFormatting();

                //Restore the member tag formatting.
                ApplyMemberTagProtectedFormatting();

                //Autofit the Row Axis columns
                CurrentView.GetGrid().AutoFitColumn(olapView.RowRange);

                //Put Border around data range
                CurrentView.GetGrid().SetBorders(olapView.DataRange, 32);

                //Merge duplicate headers
                ApplyMergeFormatting();

                //Set row height and column widths
                ApplyRowHeightsColWidths();

                //Set Column Axis range to wrap text
                //TTN-1283
                if (Settings.Default.AutoFitColumnHeaderRowHeight)
                {
                    CurrentView.GetGrid().WrapText(olapView.ColRange);
                    CurrentView.GetGrid().AutoFitRow(olapView.ColRange);
                }
            }

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyFormatting, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Sets all the styles for a worksheet.
        /// </summary>
        /// <param name="currentView">Current view.</param>
        public void ResetFormatChanges(ViewStateInfo currentView)
        {
            CurrentView = currentView;
            ResetFormatChanges();
        }

        ///// <summary>
        ///// Resets all the member tag cells for a worksheet.
        ///// </summary>
        ///// <param name="currentView">Current view.</param>
        //public void ResetMemberTagFormatChanges(ViewStateInfo currentView)
        //{
        //    CurrentView = currentView;

        //    switch (CurrentView.GetOlapView().PrimaryFormattingAxis)
        //    {
        //        case "0":
        //            ApplyFormats(ExcelBuildView.FormatRange.DataRow);
        //            ApplyFormats(ExcelBuildView.FormatRange.DataCol);
        //            break;
        //        case "1":
        //            ApplyFormats(ExcelBuildView.FormatRange.DataCol);
        //            ApplyFormats(ExcelBuildView.FormatRange.DataRow);
        //            break;
        //        case "2":
        //            break;
        //    }
        //}

        /// <summary>
        /// Sets all the styles for a worksheet.
        /// </summary>
        private void ResetFormatChanges()
        {
            Dictionary<Format, List<Range>> changedCells = PafApp.GetChangedCellMngr().GetChangedCells(CurrentView.HashCode.ToString());

            if (changedCells != null)
            {
                foreach (KeyValuePair<Format, List<Range>> kv in changedCells)
                {
                    CurrentView.GetGrid().SetStyles(kv.Value, kv.Key);
                }
            }
        }

        //02/28/2008 - KRM DEAD CODE!
        ///// <summary>
        ///// Resets the formatting for a single range.
        ///// </summary>
        ///// <param name="range">The range to reformat</param>
        //public void ResetFormatChanges(Range range)
        //{
        //    Dictionary<Format, List<Range>> changedCells = PafApp.GetChangedCellMngr().GetChangedCells(CurrentView.HashCode.ToString());

        //    if (changedCells != null)
        //    {
        //        foreach (KeyValuePair<Format, List<Range>> kv in changedCells)
        //        {
        //            foreach (Range r in kv.Value)
        //            {
        //                //TODO Need to fix IEquiptable overloaded operator.
        //                if(r.ContiguousRanges[0].ToString().Equals(range.ContiguousRanges[0].ToString()))
        //                {
        //                    List<Range> rng = new List<Range>();
        //                    rng.Add(range);
        //                    CurrentView.GetGrid().SetStyles(rng, kv.Key);
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// Resets the formatting for a single range.
        /// </summary>
        /// <param name="range">The range to reformat</param>
        public void ResetFormatChanges(List<Range> range)
        {
            Dictionary<Format, List<Range>> changedCells = PafApp.GetChangedCellMngr().GetChangedCells(CurrentView.HashCode.ToString());

            if (changedCells != null)
            {
                foreach (KeyValuePair<Format, List<Range>> kv in changedCells)
                {
                    foreach (Range r in kv.Value)
                    {
                        //TODO Need to fix IEquiptable overloaded operator.
                        if (r.ContiguousRanges[0].ToString().Equals(range[0].ContiguousRanges[0].ToString()))
                        {
                            CurrentView.GetGrid().SetStyles(range, kv.Key);
                            return;
                        }
                    }
                }
            }
        }


        ///// <summary>
        ///// Resets the formatting for a single range.
        ///// </summary>
        ///// <param name="currentView">The current view state.</param>
        //public void ResetMemberTagFormatChanges(ViewStateInfo currentView)
        //{
        //    if (currentView.MbrTagInterToFormat == null || currentView.MbrTagInterToFormat.Count == 0)
        //    {
        //        return;
        //    }

        //    OlapView op = currentView.GetOlapView();
        //    MemberTags mt = op.MemberTag;


        //    foreach (KeyValuePair<Intersection, string> kvp in currentView.MbrTagInterToFormat)
        //    {
        //        memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(kvp.Value);
        //        List<CellAddress> cells = mt.FindMemberTagIntersectionAddress(kvp.Key, tagDef.name);
        //        if (cells.Count > 0)
        //        {
        //            Format format = GetDefaultCellFormat(cells[0]);
        //            List<Range> lst = new List<Range>();
        //            if (cells.Count == 1)
        //            {
        //                Range rng = new Range(cells[0]);
        //                lst.Add(rng);
        //            }
        //            else
        //            {
        //                List<ContiguousRange> crs = mt.GetRanges(cells, tagDef, ref lst);
        //                if (crs.Count > 0)
        //                {
        //                    foreach (ContiguousRange cr in crs)
        //                    {
        //                        Range rng = new Range();
        //                        rng.AddContiguousRange(cr);
        //                        lst.Add(rng);
        //                    }
        //                }
        //            }
        //            op.Grid.SetStyles(lst, format);
        //        }
        //    }
        //    currentView.MbrTagInterToFormat.Clear();
        //}


        /// <summary>
        /// Resets the formatting for a single range.
        /// </summary>
        /// <param name="currentView">The current view state.</param>
        //public void ResetMemberTagFormatChanges(Dictionary<Intersection, string> memberTagIntersections)
        public void ResetMemberTagFormatChanges(ViewStateInfo currentView)
        {
            if (currentView.MbrTagInterToFormat == null || currentView.MbrTagInterToFormat.Count() == 0)
            {
                return;
            }

            OlapView op = currentView.GetOlapView();
            MemberTags mt = op.MemberTag;

            //Dictionary<string, List<Intersection>> values = currentView.MbrTagInterToFormat.Values();

            Dictionary<string, Dictionary<string, List<Intersection>>>.KeyCollection mbrTags = currentView.MbrTagInterToFormat.Keys;

            //foreach (KeyValuePair<Intersection, string> kvp in currentView.MbrTagInterToFormat)
            foreach (string mbrTag in mbrTags)
            {
                memberTagDef tagDef = PafApp.GetMemberTagInfo().GetMemberTag(mbrTag);

                Dictionary<string, List<Intersection>> formatTag = currentView.MbrTagInterToFormat.Values(mbrTag);

                foreach (KeyValuePair<string, List<Intersection>> kvp in formatTag)
                {
                    if (kvp.Value != null && kvp.Value.Count > 0)
                    {
                        foreach (Intersection inter in kvp.Value)
                        {
                            List<CellAddress> cells = mt.FindMemberTagIntersectionAddress(inter, tagDef.name);
                            if (cells.Count > 0)
                            {
                                Format format = GetDefaultCellFormat(cells[0]);
                                List<Range> lst = new List<Range>();
                                if (cells.Count == 1)
                                {
                                    Range rng = new Range(cells[0]);
                                    lst.Add(rng);
                                }
                                else
                                {
                                    List<ContiguousRange> crs = mt.GetRanges(cells, tagDef, ref lst);
                                    if (crs.Count > 0)
                                    {
                                        foreach (ContiguousRange cr in crs)
                                        {
                                            Range rng = new Range();
                                            rng.AddContiguousRange(cr);
                                            lst.Add(rng);
                                        }
                                    }
                                }
                                op.Grid.SetStyles(lst, format);
                            }
                        }
                    }
                }
            }
            currentView.MbrTagInterToFormat.Clear();
        }

        /// <summary>
        /// Apply formatting to the data ranges for all PAFBlank members
        /// </summary>
        /// <param name="currentView">The current view to apply the formatting to.</param>
        public void ApplyBlankFormatting(ViewStateInfo currentView)
        {
            CurrentView = currentView;
            ApplyBlankFormatting();
        }

        /// <summary>
        /// Restore the formatting for all changed member tag cells.
        /// </summary>
        public void RestoreMemberTagChangedCellFormatting()
        {
            //populate mbr tags.
            foreach (KeyValuePair<int, string> kvp in PafApp.GetViewMngr().CurrentOlapView.MemberTag.ColMbrTagBlanks)
            {
                RestoreMemberTagChangedCellFormatting(kvp.Value);
            }

            //populate mbr tags.
            foreach (KeyValuePair<int, string> kvp in PafApp.GetViewMngr().CurrentOlapView.MemberTag.RowMbrTagBlanks)
            {
                RestoreMemberTagChangedCellFormatting(kvp.Value);
            }
        }

        /// <summary>
        /// Applies Protected Formatting To Member Tags
        /// </summary>
        public void ApplyMemberTagProtectedFormatting()
        {
            Stopwatch startTime = Stopwatch.StartNew();

            bool colActionPerformed = ApplyMemberTagProtectedFormatting(ViewAxis.Col);
            bool rowActionPerformed = ApplyMemberTagProtectedFormatting(ViewAxis.Row);

            if (colActionPerformed || rowActionPerformed)
            {
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("ApplyMemberTagProtectedFormatting, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }

        }

        /// <summary>
        /// Restore the formatting for all changed member tag cells.
        /// </summary>
        /// <param name="mbrTagName">Name of the member tag.</param>
        public void RestoreMemberTagChangedCellFormatting(string mbrTagName)
        {
            Dictionary<Intersection, string> values = PafApp.GetMbrTagMngr().MbrTagCache.GetChangedValues(mbrTagName);

            if (values != null)
            {
                Range range = new Range();
                foreach (KeyValuePair<Intersection, string> kvp in values)
                {
                    List<CellAddress> ca = PafApp.GetViewMngr().CurrentOlapView.MemberTag.FindMemberTagIntersectionAddress(kvp.Key, mbrTagName);
                    if (ca != null && ca.Count > 0)
                    {
                        foreach (CellAddress cell in ca)
                        {
                            range.AddContiguousRange(cell);
                        }
                    }
                }
                if (range.CellCount > 0)
                {
                    //change the bg fill color.
                    PafApp.GetViewMngr().CurrentOlapView.Grid.SetBgFillColor(
                        range,
                        PafApp.GetPafAppSettingsHelper().GetMemberTagChangedColor());
                }
            }
        }

        /// <summary>
        /// Applies protected formatting to member tags on the specified axis
        /// </summary>
        /// <param name="viewAxis"></param>
        private bool ApplyMemberTagProtectedFormatting(ViewAxis viewAxis)
        {
            bool actionPerformed = false;
            Dictionary<int, string> axisMemberTagBlanks;
            Dictionary<TupleFormat, List<Tuple>> axisDataStyles;
            switch (viewAxis)
            {
                case ViewAxis.Col:
                    axisMemberTagBlanks = PafApp.GetViewMngr().CurrentOlapView.MemberTag.ColMbrTagBlanks;
                    axisDataStyles = CurrentView.GetOlapView().DataColStyles;
                    break;
                case ViewAxis.Row:
                    axisMemberTagBlanks = PafApp.GetViewMngr().CurrentOlapView.MemberTag.RowMbrTagBlanks;
                    axisDataStyles = CurrentView.GetOlapView().DataRowStyles;
                    break;
                default:
                    return false;
            }

            if (axisDataStyles == null || axisMemberTagBlanks == null) return false;

            foreach (var style in axisDataStyles)
            {
                TupleFormat tupleFormat = style.Key;
                if (String.IsNullOrWhiteSpace(tupleFormat.Plannable) || Convert.ToBoolean(tupleFormat.Plannable)) continue;
                foreach (Tuple tuple in style.Value.Where(tuple => tuple.IsMemberTag))
                {
                    actionPerformed = true;
                    string tagName;
                    if (axisMemberTagBlanks.TryGetValue(tuple.Position, out tagName))
                    {
                        ApplyMemberTagProtectedFormatting(tagName);
                    }
                    else
                    {
                        ApplyMemberTagProtectedFormatting(CurrentView.GetOlapView().FindTupleDataRange(tuple, viewAxis));
                    }
                }
            }

            return actionPerformed;
        }

        /// <summary>
        /// Apply Protected Formatting To Non-Editable Member Tags.
        /// </summary>
        /// <param name="mbrTagName">Name of the member tag.</param>
        private void ApplyMemberTagProtectedFormatting(string mbrTagName)
        {
            Dictionary<Intersection, List<CellAddress>> values = PafApp.GetViewMngr().CurrentOlapView.MemberTag.AllMemberTagIntersections.Values(mbrTagName);

            if (values == null) return;

            Range range = new Range();
            foreach (KeyValuePair<Intersection, List<CellAddress>> kvp in values)
            {
                if (kvp.Key.ContainsPafBlank()) continue;
                List<CellAddress> ca = PafApp.GetViewMngr().CurrentOlapView.MemberTag.FindMemberTagIntersectionAddress(kvp.Key, mbrTagName);
                if (ca != null && ca.Count > 0)
                {
                    foreach (CellAddress cell in ca)
                    {
                        range.AddContiguousRange(cell);
                    }
                }
            }
            ApplyMemberTagProtectedFormatting(range);
        }


        /// <summary>
        /// Apply Protected Formatting To Non-Editable Member Tags.
        /// </summary>
        /// <param name="range">Name of the member tag.</param>
        private void ApplyMemberTagProtectedFormatting(Range range)
        {
            if (range != null && range.CellCount > 0)
            {
                PafApp.GetViewMngr().CurrentOlapView.Grid.SetProtectedFormats(range, PafApp.GetPafAppSettingsHelper().GetNonPlannableProtectedColor());
            }
        }

        /// <summary>
        /// Restore the formatting for all changed cells.  Currently used after a sort.
        /// </summary>
        public void RestoreChangedCellFormatting()
        {
            foreach (Intersection changedInter in PafApp.GetViewMngr().CurrentProtectionMngr.Changes)
            {
                List<CellAddress> cellAddresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(changedInter);

                if (cellAddresses != null)
                {
                    foreach (CellAddress ca in cellAddresses)
                    {
                        PafApp.GetViewMngr().CurrentProtectionMngr.ChangedRange.AddContiguousRange(ca);
                    }
                }
            }

            //Set the color for the changed cells
            if (PafApp.GetViewMngr().CurrentProtectionMngr.ChangedRange.ContiguousRanges.Count > 0)
            {
                PafApp.GetViewMngr().CurrentGrid.SetBgFillColor(
                    PafApp.GetViewMngr().CurrentProtectionMngr.ChangedRange,
                    PafApp.GetPafAppSettingsHelper().GetSystemLockColor());
                //Next, clear the list of changed cell addresses
                PafApp.GetViewMngr().CurrentProtectionMngr.ChangedRange = null;
            }
        }

        /// <summary>
        /// Restore the formatting for all protected cells.  Currently used after a sort.
        /// </summary>
        public void RestoreProtectedChangedCellFormatting()
        {
            PafApp.GetViewMngr().CurrentProtectionMngr.BuildProtectedCellsRange();

            //Set the color for the protected cells
            if (PafApp.GetViewMngr().CurrentProtectionMngr.ProtectedRange.ContiguousRanges.Count > 0)
            {
                PafApp.GetViewMngr().CurrentGrid.SetProtectedFormats(
                    PafApp.GetViewMngr().CurrentProtectionMngr.ProtectedRange,
                    PafApp.GetPafAppSettingsHelper().GetProtectedColor());
                //Next, clear the list of changed cell addresses
                PafApp.GetViewMngr().CurrentProtectionMngr.ProtectedRange = null;
            }
            
        }

        /// <summary>
        /// Restore the formatting for all user locked cells.  Currently used after a sort.
        /// </summary>
        public void RestoreUserLockedCellFormatting()
        {
            //TTN-1561
            List<Intersection> allLocks =
                new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.GlobalLocks.Count() +
                                       PafApp.GetViewMngr().CurrentProtectionMngr.UserLocks.Count);

            allLocks.AddRange(from cs in PafApp.GetViewMngr().CurrentProtectionMngr.GlobalLocks.AllSessionLocks() 
                              from c in cs.GetCoordinates()
                              select new Intersection(cs.Dimensions, c.Coordinates));

            allLocks.AddRange(PafApp.GetViewMngr().CurrentProtectionMngr.UserLocks);



            foreach (Intersection intersection in allLocks)
            {
                List<CellAddress> cellAddresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);

                if (cellAddresses != null)
                {
                    foreach (CellAddress ca in cellAddresses)
                    {
                        PafApp.GetViewMngr().CurrentProtectionMngr.UserLockedRange.AddContiguousRange(ca);
                    }
                }
            }

            //Set the color for the changed cells
            if (PafApp.GetViewMngr().CurrentProtectionMngr.UserLockedRange.ContiguousRanges.Count > 0)
            {
                PafApp.GetViewMngr().CurrentGrid.SetProtectedFormats(
                    PafApp.GetViewMngr().CurrentProtectionMngr.UserLockedRange,
                    PafApp.GetPafAppSettingsHelper().GetUserLockColor());
                //Next, clear the list of changed cell addresses
                PafApp.GetViewMngr().CurrentProtectionMngr.UserLockedRange = null;
            }
        }

        /// <summary>
        /// Restores the original formatting for a cell, as it was rendered from the server.
        /// </summary>
        /// <param name="ca">The cell address to be refreshed/restored.</param>
        public void RestoreOriginalCellFormat(CellAddress ca)
        {
            if (ca != null)
            {
                Format format;
                if (CurrentView.DefaultFormats.TryGetValue(ca.GetHashCode(), out format))
                {
                    List<Range> ranges = new List<Range>();
                    ranges.Add(new Range(ca));
                    PafApp.GetViewMngr().CurrentGrid.SetStyles(ranges, format);
                }
            }
        }

        /// <summary>
        /// Apply formatting to the data ranges for all PAFBlank members
        /// </summary>
        private void ApplyBlankFormatting()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            List<Range> ranges = new List<Range>();
            string blankMember = Settings.Default.Blank;

            //Row Blanks
            foreach (int position in CurrentView.GetOlapView().RowBlanks)
            {
                if (!CurrentView.GetOlapView().MemberTag.RowMbrTagBlanks.ContainsKey(position))
                {
                    Tuple memberTuple = new Tuple(position, ViewAxis.Row, blankMember);
                    Range memberRange = CurrentView.GetOlapView().FindTupleDataRange(memberTuple, ViewAxis.Row);
                    ranges.Add(memberRange);
                }
            }

            //Column Blanks
            foreach (int position in CurrentView.GetOlapView().ColBlanks)
            {
                if (!CurrentView.GetOlapView().MemberTag.ColMbrTagBlanks.ContainsKey(position))
                {
                    Tuple memberTuple = new Tuple(position, ViewAxis.Col, blankMember);
                    Range memberRange = CurrentView.GetOlapView().FindTupleDataRange(memberTuple, ViewAxis.Col);
                    ranges.Add(memberRange);
                }
            }

            //Format the data ranges for the Blanks
            if (ranges.Count > 0)
            {
                CurrentView.GetGrid().SetBlankFormats(ranges, true);
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyBlankFormatting, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        ///// <summary>
        ///// 
        ///// </summary>
        //private void ApplyRowNumericFormats()
        //{
        //    List<Range> RowRanges = null;

        //    foreach (KeyValuePair<string, List<Tuple>> kvp in CurrentView.GetOlapView().NumericRowFormats)
        //    {
        //        RowRanges = new List<Range>();
        //        foreach (Tuple memberTuple in kvp.Value)
        //        {
        //            Range memberRange = CurrentView.GetOlapView().FindTupleDataRange(memberTuple);
        //            RowRanges.Add(memberRange);
        //        }

        //        CurrentView.GetGrid().SetNumericFormat(RowRanges, kvp.Key);
        //    }

        //}

        ///// <summary>
        ///// 
        ///// </summary>
        //private void ApplyColNumericFormats()
        //{
        //    List<Range> ColRanges = null;

        //    foreach (KeyValuePair<string, List<Tuple>> kvp in CurrentView.GetOlapView().NumericColFormats)
        //    {
        //        ColRanges = new List<Range>();
        //        foreach (Tuple memberTuple in kvp.Value)
        //        {
        //            Range memberRange = CurrentView.GetOlapView().FindTupleDataRange(memberTuple);
        //            ColRanges.Add(memberRange);
        //        }

        //        CurrentView.GetGrid().SetNumericFormat(ColRanges, kvp.Key);
        //    }

        //}

        private void ApplyConditionalFormat(OlapView olapView)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            if (olapView == null) return;

            string condFormatName = olapView.ConditionalFormat;
            if (String.IsNullOrWhiteSpace(condFormatName)) return;


            var condFormat = PafApp.GetConditionalFormat(olapView.ConditionalFormat);
            if (condFormat == null) return;

            string primaryDim = condFormat.primaryDimension.name;
            ViewAxis primaryDimAxis = olapView.FindDimensionAxis(primaryDim);
            if (primaryDimAxis == ViewAxis.Page)
            {
                primaryDimAxis = olapView.PrimaryFormattingAxis;
            }

            Dictionary<TupleConditionalFormat, List<Tuple>> styles = null;


            switch (primaryDimAxis)
            {
                case ViewAxis.Col:
                    styles = CurrentView.GetOlapView().DataColConditionalStyles;
                    break;
                case ViewAxis.Row:
                    styles = CurrentView.GetOlapView().DataRowConditionalStyles;
                    break;
            }

            if (styles != null && styles.Count > 0)
            {
                FormatThreadConditional(styles, primaryDimAxis, condFormat);
            }

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyConditionalFormat: {0}, runtime: {1} (ms)", condFormatName, startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="formatRange"></param>
        private void ApplyFormats(ExcelBuildView.FormatRange formatRange)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            bool overrideAxis = false;
            Dictionary<TupleFormat, List<Tuple>> styles = null;

            switch (formatRange)
            {
                case ExcelBuildView.FormatRange.Headers:
                    styles = CurrentView.GetOlapView().HeaderStyles;
                    break;
                case ExcelBuildView.FormatRange.DataCol:
                    styles = CurrentView.GetOlapView().DataColStyles;
                    if (CurrentView.GetOlapView().PrimaryFormattingAxis == ViewAxis.Row)
                    {
                        overrideAxis = true;
                    }
                    break;
                case ExcelBuildView.FormatRange.DataRow:
                    styles = CurrentView.GetOlapView().DataRowStyles;
                    if (CurrentView.GetOlapView().PrimaryFormattingAxis == ViewAxis.Col)
                    {
                        overrideAxis = true;
                    }
                    break;
                case ExcelBuildView.FormatRange.MemberTagCol:
                    styles = CurrentView.GetOlapView().DataColStyles;
                    break;
                case ExcelBuildView.FormatRange.MemberTagRow:
                    styles = CurrentView.GetOlapView().DataRowStyles;
                    break;
                case ExcelBuildView.FormatRange.ViewHeaders:
                    FormatHeaders(
                        CurrentView.GetOlapView().ViewHeaderStyles,
                        "Header",
                        CurrentView.GetOlapView().DataRange,
                        Settings.Default.AutoFitHeaderRows);
                    return;
            }

            if (styles == null) return;

            PafApp.GetLogger().InfoFormat("Formatting: {0}", formatRange);

            FormatThread(styles, formatRange, overrideAxis);

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyFormats, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Format the view headers.
        /// </summary>
        /// <param name="styles">Grouping of styles.</param>
        /// <param name="styleName">Name of the style to use.</param>
        /// <param name="dataRange">View Data range.</param>
        /// <param name="autoFitHeaderRows">Autofit the row height.</param>
        private void FormatHeaders(IEnumerable<KeyValuePair<string, List<Range>>> styles, 
            string styleName, ContiguousRange dataRange, bool autoFitHeaderRows)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            int j = 1;
            string globalStyle;
            Format format = new Format();
            Dictionary<string, PafService.pafStyle> globalStyles = PafApp.GetGlobalStyles();
            List<Range> ranges = null;

            foreach (KeyValuePair<string, List<Range>> kvp in styles)
            {
                globalStyle = kvp.Key.ToLower().Trim();
                ranges = kvp.Value;

                if (globalStyles != null)
                {
                    if (globalStyles.ContainsKey(globalStyle))
                    {
                        format = SetFormat(globalStyles[globalStyle]);
                    }
                }

                CurrentView.GetGrid().SetStyles(CurrentView.HashCode + "." + styleName + j, ranges, format);

                //The merge formatting cannot be applied to views without both a row and column axis - dataRange doesn't exist
                if (CurrentView.GetOlapView().IsEmpty == false && dataRange != null && dataRange.BottomRight != null)
                {
                    foreach (Range rng in ranges)
                    {
                        foreach (ContiguousRange crng in rng.ContiguousRanges)
                        {

                            if (autoFitHeaderRows)
                            {
                                //NOTE To be rewritten in Arlington.
                                CurrentView.GetGrid().AutoFitRow(crng);
                            }

                            CurrentView.GetGrid().SetMergedCells(
                                new ContiguousRange(
                                    new CellAddress(crng.TopLeft.Row, crng.TopLeft.Col),
                                    new CellAddress(crng.TopLeft.Row, dataRange.BottomRight.Col)),
                                false);
                        }
                    }
                }
                j++;
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("FormatHeaders, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        private void FormatThreadConditional(IEnumerable<KeyValuePair<TupleConditionalFormat, List<Tuple>>> styles, ViewAxis formatViewAxis, conditionalFormat conditionalFormat)
        {
           
            if (styles == null) return;

            foreach (var style in styles)
            {
                List<Range> ranges = new List<Range>(style.Value.Count);
                abstractPaceConditionalStyle condStyle = PafApp.GetConditionalStyle(style.Key.Name);
                if (condStyle == null) continue;

                foreach (Tuple tuple in style.Value)
                {
                    Range memberRange = CurrentView.GetOlapView().FindTupleDataRangeCondFormat(formatViewAxis, tuple, conditionalFormat);

                    //Do not add duplicate Blank ranges to the List
                    if (memberRange != null)
                    {
                        ranges.Add(memberRange);
                    }
                }

                Stopwatch startTime = Stopwatch.StartNew();
                Range formatRange = new Range(ranges);
                PafApp.GetLogger().InfoFormat("Applying Conditional Style: {0} to Range: {1}", condStyle.name, formatRange);
                CurrentView.GetGrid().SetConditionalFormatting(formatRange, condStyle);
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("Applied Conditional Style: {0}, runtime: {1} (ms)", condStyle.name, startTime.ElapsedMilliseconds.ToString("N0"));
            }

        }


        /// <summary>
        /// Formats the data section.
        /// </summary>
        /// <param name="styles"></param>
        /// <param name="formatRange"></param>
        /// <param name="overrideAxis"></param>
        private void FormatThread(IEnumerable<KeyValuePair<TupleFormat, List<Tuple>>> styles, ExcelBuildView.FormatRange formatRange, bool overrideAxis)
        {
            List<Range> ranges = null;
            Range memberRange = null;
            string globalStyle = "";
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;
            

            foreach (KeyValuePair<TupleFormat, List<Tuple>> kvp in styles)
            {
                Format format = null;
                ranges = new List<Range>();

                foreach (Tuple tuple in kvp.Value)
                {
                    switch (formatRange)
                    {
                        case ExcelBuildView.FormatRange.DataCol:
                            memberRange = CurrentView.GetOlapView().FindTupleDataRange(tuple, ViewAxis.Col);
                            break;
                        case ExcelBuildView.FormatRange.DataRow:
                            memberRange = CurrentView.GetOlapView().FindTupleDataRange(tuple, ViewAxis.Row);
                            break;
                        case ExcelBuildView.FormatRange.Headers:
                            memberRange = CurrentView.GetOlapView().FindTupleMemberRange(tuple);
                            break;
                        case ExcelBuildView.FormatRange.MemberTagCol:
                            if(tuple.IsMemberTag)
                            {
                                memberRange = CurrentView.GetOlapView().FindTupleDataRange(tuple, ViewAxis.Col);
                            }
                            break;
                        case ExcelBuildView.FormatRange.MemberTagRow:
                            if (tuple.IsMemberTag)
                            {
                                memberRange = CurrentView.GetOlapView().FindTupleDataRange(tuple, ViewAxis.Row);
                            }
                            break;
                    }
                    //Do not add duplicate Blank ranges to the List
                    if (memberRange != null)
                    {
                        ranges.Add(memberRange);
                    }
                }

                Dictionary<string, pafStyle> globalStyles = PafApp.GetGlobalStyles();
                globalStyle = kvp.Key.Name.ToLower();

                if (globalStyles != null)
                {
                    if (globalStyles.ContainsKey(globalStyle))
                    {
                        format = SetFormat(globalStyles[globalStyle]);
                    }
                }

                //If there are no GlobalStyles for a tuple
                if (format == null)
                {
                    format = new Format();
                }

                if(formatRange == ExcelBuildView.FormatRange.MemberTagRow || 
                    formatRange == ExcelBuildView.FormatRange.MemberTagCol)
                {
                    if (String.IsNullOrEmpty(format.BgHexFillColor))
                    {
                        format.BgHexFillColor = PafAppConstants.xlColorIndexNone.ToString();
                    }
                }

                //Protect cells
                //if (kvp.Key.Plannable != null && kvp.Key.Plannable != "")
                if (!String.IsNullOrEmpty(kvp.Key.Plannable))
                {
                    format.Plannable = kvp.Key.Plannable;
                }

                //Set numeric format
                //if (kvp.Key.NumberFormat != null && kvp.Key.NumberFormat != "")
                if (!String.IsNullOrEmpty(kvp.Key.NumberFormat))
                {
                    format.NumberFormat = kvp.Key.NumberFormat;
                }

                //Set Borders
                format.Borders = kvp.Key.Borders;

                BuildDefaultFormats(ranges, format);

                if (overrideAxis)
                {
                    CurrentView.GetGrid().SetStyles(ranges, format);
                }
                if (!overrideAxis && ranges.Count > 0)
                {
                    if (debugEnabled) PafApp.GetLogger().DebugFormat("Set Styles {0} : {1} : {2} : {3}", new object[] { formatRange, kvp.Key.NumberFormat, kvp.Key.Plannable, kvp.Key.Borders });
                    string formatName = String.Format("{0}:{1}:{2}:{3}", new object[] { globalStyle, kvp.Key.NumberFormat, kvp.Key.Plannable, kvp.Key.Borders });
                    CurrentView.GetGrid().SetStyles(formatName, ranges, format);
                    if (debugEnabled) PafApp.GetLogger().Debug("Set Styles Complete");
                    //NOTE 01/30/2008 - KRM added (i think this may speed up formatting)
                    //clear out the collections.
                    ranges.Clear();
                    memberRange = null;
                }
            }
        }

        /// <summary>
        /// Builds the dictionary of default formats for a view as it is rendered from the server.
        /// </summary>
        /// <param name="ranges">List of ranges.</param>
        /// <param name="format">The format associated with the ranges.</param>
        private void BuildDefaultFormats(List<Range> ranges, Format format)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            foreach (Range rng in ranges)
            {
                foreach (ContiguousRange cr in rng.ContiguousRanges)
                {
                    foreach (Cell ca in cr.Cells)
                    {
                        Format oldFormat = null;
                        if (CurrentView.DefaultFormats.TryGetValue(ca.CellAddress.GetHashCode(), out oldFormat))
                        {
                            Format f = format.Clone();
                            oldFormat.CopyAll(f);
                            CurrentView.DefaultFormats[ca.CellAddress.GetHashCode()] = oldFormat;
                        }
                        else
                        {
                            Format f = format.Clone();
                            f.Copy(CurrentView.DefaultFormat);
                            CurrentView.DefaultFormats.Add(ca.CellAddress.GetHashCode(), f);
                        }
                    }
                }
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("default view formats, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Gets the default cell format from when the view was rendered from the server.
        /// </summary>
        /// <param name="ca">Address of the cell.</param>
        /// <returns>the cell format if one was found.  null if no format was found or the cell address
        /// was null or invalid.</returns>
        public Format GetDefaultCellFormat(CellAddress ca)
        {
            Format format = null;

            if (PafApp.GetViewMngr().CurrentView.DefaultFormats != null && ca != null)
            {
                PafApp.GetViewMngr().CurrentView.DefaultFormats.TryGetValue(ca.GetHashCode(), out format);
            }

            return format;
        }

        /// <summary>
        /// 
        /// </summary>
        private void ApplyPlannableFormats()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            if (CurrentView.GetOlapView().NonPlannableRange != null)
            {
                CurrentView.GetGrid().SetProtectedFormats(CurrentView.GetOlapView().NonPlannableRange,
                PafApp.GetPafAppSettingsHelper().GetNonPlannableProtectedColor());
            }

            //I think that Invalid formatting should go here - I may need to change this later
            if (CurrentView.GetOlapView().InValidAttributeRange != null)
            {
                CurrentView.GetGrid().SetProtectedFormats(CurrentView.GetOlapView().InValidAttributeRange, 
                    PafApp.GetPafAppSettingsHelper().GetNonPlannableProtectedColor());
            }

            //Apply Forward Plannable formatting last per Carolyn
            if (CurrentView.GetOlapView().ForwardPlannableRange != null)
            {
                CurrentView.GetGrid().SetProtectedFormats(CurrentView.GetOlapView().ForwardPlannableRange, 
                    PafApp.GetPafAppSettingsHelper().GetForwardPlannableProtectedColor());
            }

            //Apply invliad member tag formatting.
            if (CurrentView.GetOlapView().InValidMemberTagRange != null && CurrentView.GetOlapView().InValidMemberTagRange.CellCount > 0)
            {
                CurrentView.GetGrid().SetProtectedFormats(CurrentView.GetOlapView().InValidMemberTagRange,
                    PafApp.GetPafAppSettingsHelper().GetNonPlannableProtectedColor());
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyPlannableFormats, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Applies protection formatting to singleton rule intersections
        /// </summary>
        private void ApplySingletonRulesFormats()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            if (CurrentView.GetOlapView().SingletonRuleRange != null)
            {
                CurrentView.GetGrid().SetProtectedFormats(CurrentView.GetOlapView().SingletonRuleRange, 
                    PafApp.GetPafAppSettingsHelper().GetProtectedColor());
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplySingletonRulesFormats, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Applies protection formatting to BOH intersections
        /// </summary>
        private void ApplyBOHProtectedFormats()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            if (CurrentView.GetOlapView().BOHProtectedRange != null)
            {
                CurrentView.GetGrid().SetProtectedFormats(CurrentView.GetOlapView().BOHProtectedRange, 
                    PafApp.GetPafAppSettingsHelper().GetProtectedColor());
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyBOHProtectedFormats, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Applies protection formatting to Session Lock intersections
        /// </summary>
        public void ApplySessionLockProtectedFormats(bool applyProtectionFormatting)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            if (applyProtectionFormatting && CurrentView.GetOlapView().SessionLockProtectedRange != null)
            {
                RestoreProtectedChangedCellFormatting();
            }

            if (CurrentView.GetOlapView().SessionLockProtectedRange != null)
            {
                CurrentView.GetGrid().SetProtectedFormats(CurrentView.GetOlapView().SessionLockProtectedRange,
                    PafApp.GetPafAppSettingsHelper().GetSessionLockColor());
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplySessionLockProtectedFormats, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Applies a merge format to a sheet.
        /// </summary>
        private void ApplyMergeFormatting()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            if (CurrentView.GetOlapView().MergedCellRanges.ContainsKey(ViewAxis.Col))
            {
                foreach (ContiguousRange range in CurrentView.GetOlapView().MergedCellRanges[ViewAxis.Col])
                {
                    CurrentView.GetGrid().Merge(range, ViewAxis.Col, PafApp.GetEventManager().SheetPassword);

                }
            }

            if (CurrentView.GetOlapView().MergedCellRanges.ContainsKey(ViewAxis.Row))
            {
                foreach (ContiguousRange range in CurrentView.GetOlapView().MergedCellRanges[ViewAxis.Row])
                {
                    CurrentView.GetGrid().Merge(range, ViewAxis.Row, PafApp.GetEventManager().SheetPassword);
                }
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyMergeFormatting, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Applies row heights and column widths
        /// </summary>
        private void ApplyRowHeightsColWidths()
        {
            Stopwatch startTime = Stopwatch.StartNew();
            //modifed for aliases
            int rowOffset = CurrentView.GetOlapView().RowOffset;

            foreach (KeyValuePair<float, List<int>> kv in CurrentView.GetOlapView().RowHeights)
            {
                Range range = new Range();
                foreach (int row in kv.Value)
                {
                    range.AddContiguousRange(new CellAddress(row + rowOffset, 1));
                }

                if (range.ContiguousRanges.Count > 0)
                {
                    CurrentView.GetGrid().SetRowHeight(range, kv.Key);
                }
            }

            //modifed for aliases
            int colOffset = CurrentView.GetOlapView().ColOffset;

            foreach (KeyValuePair<float, List<int>> kv in CurrentView.GetOlapView().ColWidths)
            {
                Range range = new Range();
                foreach (int col in kv.Value)
                {
                    range.AddContiguousRange(new CellAddress(1, col + colOffset));
                }

                if (range.ContiguousRanges.Count > 0)
                {
                    CurrentView.GetGrid().SetColumnWidth(range, kv.Key);
                }
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ApplyRowHeightsColWidths, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Applies a global style to a list of ranges.
        /// </summary>
        /// <param name="globalStyles">The global style to apply to the ranges.</param>
        /// <returns></returns>
        /// <remarks>KRM modified 1/31/2008 - Fixed for new server stack stuff.</remarks>
        private static Format SetFormat(pafStyle globalStyles)
        {
            Format format = new Format();

            if (globalStyles.alignment != null && globalStyles.alignment.value != null)
            {
                format.Alignment = globalStyles.alignment.value;
            }
            
            if (globalStyles.bgHexFillColor != null)
            {
                format.BgHexFillColor = globalStyles.bgHexFillColor;
            }
            if (globalStyles.boldSpecified)
            {
                format.Bold = globalStyles.bold.ToString();
            }
            //TTN-1351
            if (globalStyles.fontHexColor != null)
            {
                //TTN-1351
                format.FontHexColor = globalStyles.fontHexColor;
            }

            if (globalStyles.fontName != null)
            {
                format.FontName = globalStyles.fontName;
            }

            if (globalStyles.italicsSpecified)
            {
                format.Italics = globalStyles.italics.ToString();
            }
            
            if (globalStyles.sizeSpecified)
            {
                format.Size = (double)globalStyles.size;
            }
            
            if (globalStyles.strikeOutSpecified)
            {
                format.StrikeOut = globalStyles.strikeOut.ToString();
            }
            
            if (globalStyles.underlineSpecified)
            {
                format.UnderLine = globalStyles.underline.ToString();
            }

            if (globalStyles.doubleUnderlineSpecified)
            {
                format.DoubleUnderline = globalStyles.doubleUnderline.ToString();
            }

            return format;
        }
    }
}
