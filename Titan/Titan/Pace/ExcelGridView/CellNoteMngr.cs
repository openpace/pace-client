#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Text;
using Titan.Pace.Application.Extensions;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView
{
    internal abstract class CellNoteMngr
    {
        protected CellNoteCache _ClientCache;

        /// <summary>
        /// view name, intersection, list of duplicate cell addresses
        /// </summary>
        protected CacheSelectionsList<string, Intersection, CellAddress> _DuplicateIntersections;

        /// <summary>
        /// view name, CellAddress, Intersection
        /// </summary>
        protected CacheSelectionsList<string, CellAddress, Intersection> _Intersections;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CellNoteMngr()
        {
            _ClientCache = new CellNoteCache();
            _DuplicateIntersections = new CacheSelectionsList<string, Intersection, CellAddress>();
            _Intersections = new CacheSelectionsList<string, CellAddress, Intersection>();
        }

        /// <summary>
        /// Get a cell note from the client cell note cache.
        /// </summary>
        /// <param name="intersection">The intersection to get the note from.</param>
        /// <returns>The cell note if one exists, null if no cell note exists in that intersecion.</returns>
        protected simpleCellNote GetNoteFromCache(Intersection intersection)
        {
            simpleCellNote value = null;
            try
            {
                if(_ClientCache.ClientCellNoteCache.TryGetValue(intersection, out value))
                {
                    return value;
                }
                else
                {
                    return value;
                }
            }
            catch (Exception)
            {
                return value;
            }
        }

        /// <summary>
        /// Signals if the client cache is dirty and need to be written back to the server.
        /// </summary>
        /// <returns>true if the client cache is dirty, false if not.</returns>
        public abstract bool IsClientCacheDirty();

        /// <summary>
        /// Cache a cell note in the client cell note cache.
        /// </summary>
        /// <param name="intersection">The intersection to cache the note in.</param>
        /// <param name="cn">The cell note to cache.</param>
        protected void CacheNote(Intersection intersection, simpleCellNote cn)
        {
            _ClientCache.AddUpdateCellNoteToClientCache(intersection, cn);
        }

        /// <summary>
        /// Adds a note to the delete note bucket.
        /// </summary>
        /// <param name="intersection">The intersection to remvoe the note from.</param>
        private void RemoveNote(Intersection intersection)
        {
            _ClientCache.AddCellNoteToDeleteBucket(intersection);
            _ClientCache.CellNoteInsert.Remove(intersection);
            _ClientCache.CellNoteUpdates.Remove(intersection);
            _ClientCache.ClientCellNoteCache.Remove(intersection);
        }

        /// <summary>
        /// Clears out the client cell note cache.
        /// </summary>
        public abstract void ClearCache();

        /// <summary>
        /// Clears out the client delete, insert, and update buckets.
        /// </summary>
        public abstract void ClearBuckets();

        /// <summary>
        /// Gets the client cell note cache.
        /// </summary>
        public abstract Dictionary<Intersection, simpleCellNote> ClientCache
        {
            get;
        }

        /// <summary>
        /// Gets the cache of SimpleCellNotes that need to be inserted in the database.
        /// </summary>
        public abstract simpleCellNote[] GetCellNoteInsertBucketForInsert();

        /// <summary>
        /// Gets the cache of SimpleCellNotes that need to be updated into the database.
        /// </summary>
        public abstract simpleCellNote[] GetCellNoteInsertBucketForUpdate();

        /// <summary>
        /// Gets the simpleCoordList of addresses that need to be deleted from the database.
        /// </summary>
        public abstract simpleCoordList GetCellNoteInsertBucketForDelete();

        /// <summary>
        /// Takes the target cell address intersection and finds any cell addresses with the same intersection.
        /// The specified source address is the most updated address, so this function will handle 
        /// updating or deleting the necessary cell notes in the duplicate intersection/cell addresses.
        /// </summary>
        /// <param name="cellAddress">The target cell to copy.</param>
        /// <param name="currentView">The associated view.</param>
        public abstract void CopyAndDeleteCellNotes(CellAddress cellAddress, ViewStateInfo currentView);

        /// <summary>
        /// Copies necessary cell notes to duplicate intersections.  Then performs then updates the client 
        /// cache with the inserts, updates, deletes.
        /// </summary>
        /// <param name="currentView">The associated view.</param>
        public abstract void CopyAndWriteAllUpdatesInsertsDeletsToClientCache(ViewStateInfo currentView);

        /// <summary>
        /// Removed the cached intersections.
        /// </summary>
        /// <param name="viewName">Name of the view to removed the cached intersecions for.</param>
        public abstract void ClearCachedIntersections(string viewName);

        /// <summary>
        /// Renders the notes on the view.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public abstract void RenderNotes(ViewStateInfo currentView);

        /// <summary>
        /// Renders the notes on the view.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public abstract void UpdateNotes(ViewStateInfo currentView);

        /// <summary>
        /// Renders the notes on the view.
        /// </summary>
        /// <param name="currentView">The currently associated view.</param>
        public abstract void ClearCellNotes(ViewStateInfo currentView);

        /// <summary>
        /// Updates the client cache from an array of simple cell notes.
        /// </summary>
        /// <param name="simpleNotes">Array of simple cell notes.</param>
        public abstract void UpdateClientCacheFromServerCache(simpleCellNote[] simpleNotes);

        /// <summary>
        /// Determines if the cell address is in a valid location for a cell note on the view.
        /// </summary>
        /// <param name="cellAddress">Currently active cell.</param>
        /// <param name="currentView">The associated view.</param>
        public abstract bool isCellAddressValidForCellNote(CellAddress cellAddress, ViewStateInfo currentView);

        /// <summary>
        /// Takes the source cell address intersection and finds any cell addresses with the same intersection.
        /// The specified source address is the most updated address, so this function will handle 
        /// updating or deleting the necessary cell notes in the duplicate intersection/cell addresses.
        /// </summary>
        /// <param name="intersection">Intersection of the cell address.</param>
        /// <param name="sourceCell">The target cell to copy.</param>
        /// <param name="currentView">The associated view.</param>
        protected void CopyAndDeleteCellNotes(Intersection intersection, CellAddress sourceCell,
                                            ViewStateInfo currentView)
        {

            if (!isCellAddressValidForCellNote(sourceCell, currentView))
            {
                return;
            }

            List<CellAddress> ca = FindDuplicateIntersections(intersection, currentView);

            try
            {
                if (ca != null)
                {
                    simpleCellNote cn = currentView.GetOlapView().Grid.GetCellNote(
                        new Range(sourceCell));

                    simpleCellNote ccn = GetNoteFromCache(intersection);

                    if(cn == null && ccn != null)
                    {
                        List<Range> ranges = new List<Range>();
                        foreach (CellAddress c in ca)
                        {
                            if (!c.Equals(intersection))
                            {
                                ranges.Add(new Range(c));
                                DeleteCellNote(c, currentView);
                            }
                        }
                        currentView.GetOlapView().Grid.DeleteCellNotes(ranges);
                    }
                    else if (cn != null)
                    {
                        List<Range> ranges = new List<Range>();
                        List<simpleCellNote> cellNotes = new List<simpleCellNote>();
                        foreach (CellAddress c in ca)
                        {
                            if (!c.Equals(intersection))
                            {
                                ranges.Add(new Range(c));
                                cellNotes.Add(cn);
                            }
                        }
                        //refreshes the value in the cell note.
                        currentView.GetOlapView().Grid.SetCellNotes(ranges, cellNotes, false, false);
                        //updates the value in the cache.
                        WriteUpdatesToClientCache(ca[0], currentView, cn);
                    }
                }
                else
                {
                    //its a single address, we need to check 4 a deleted note.
                    simpleCellNote cn = currentView.GetOlapView().Grid.GetCellNote(new Range(sourceCell));
                    if (cn != null)
                    {
                        WriteUpdatesToClientCache(sourceCell, currentView, cn);
                    }
                    DeleteCellNote(sourceCell, currentView);
                }
            }
            catch(COMException)
            {
                return;
            }
        }

        /// <summary>
        /// Returns the valid cell address and cellnotes for and view/olapview.
        /// </summary>
        /// <param name="currentView">The associated view.</param>
        /// <param name="sheetIntNotes"></param>
        /// <returns>a dictionary of celladdress and cellnotes</returns>
        protected Dictionary<Range, simpleCellNote> GetNotesForView(ViewStateInfo currentView,
                                                                  out Dictionary<Intersection, simpleCellNote> sheetIntNotes)
        {
            Dictionary<Intersection, simpleCellNote> cc = _ClientCache.ClientCellNoteCache;
            Dictionary<Range, simpleCellNote> sheetCache = new Dictionary<Range, simpleCellNote>();
            sheetIntNotes = new Dictionary<Intersection, simpleCellNote>();

            foreach (KeyValuePair<Intersection, simpleCellNote> kvp in cc)
            {
                List<CellAddress> ca = FindIntersectionAddress2(kvp.Key, currentView);
                if (ca != null && ca.Count > 0)
                {
                    sheetIntNotes.Add(kvp.Key, kvp.Value);
                    foreach (CellAddress cellAddress in ca)
                    {
                        sheetCache.Add(new Range(cellAddress), kvp.Value);
                        Debug.WriteLine("GetNotesForView, Value: " + kvp.Value.text);
                    }
                }
            }
            return sheetCache;
        }

        /// <summary>
        /// Takes a source address looks at the client cache and grid to see if the note has been
        /// deleted.  If so it is removed from the appropriate caches.
        /// </summary>
        /// <param name="sourceAddress">Source cell address</param>
        /// <param name="currentView">The view associated.</param>
        /// <returns>true if successful, false if not.</returns>
        private bool DeleteCellNote(CellAddress sourceAddress,  ViewStateInfo currentView)
        {
            bool ret = false;
            try
            {
                if (sourceAddress != null)
                {
                    Intersection intersection = FindIntersection(sourceAddress, currentView);

                    simpleCellNote cacheNote = GetNoteFromCache(intersection);
                    simpleCellNote gridNote = currentView.GetOlapView().Grid.GetCellNote(new Range(sourceAddress));
                    if (cacheNote != null && gridNote == null)
                    {
                        RemoveNote(intersection);
                        ret = true;
                    }
                    else if (cacheNote == null && gridNote != null)
                    {
                        RemoveNote(intersection);
                        ret = true;
                    }
                }
            }
            catch(COMException)
            {
                ret = false;
            }
            catch(Exception)
            {
                ret = false;
            }
            return ret;
        }

        /// <summary>
        /// Wrapper for the OLAP view find intersection call.  Caches the intersections.
        /// </summary>
        /// <param name="cellAddress">Cell address to find the interseciton for.</param>
        /// <param name="currentView">Current view.</param>
        /// <returns>intersection for the cell address or null if it does not exist.</returns>
        protected Intersection FindIntersection(CellAddress cellAddress, ViewStateInfo currentView)
        {
            Intersection ret;
            try
            {
                List<Intersection> intersection;
                intersection = _Intersections.GetCachedSelections(currentView.ViewName, cellAddress);

                if (intersection != null && intersection.Count > 0)
                {
                    ret = intersection[0];
                }
                else
                {
                    Intersection ints = currentView.GetOlapView().FindIntersection(cellAddress);
                    _Intersections.CacheSelection(
                        currentView.ViewName,
                        cellAddress,
                        new Intersection[] {ints});
                    ret = ints;
                }
            }
            catch(Exception)
            {
                ret = null;
            }
            return ret;
        }

        /// <summary>
        /// Wrapper for the OLAP view FindIntersectionAddresses2 call.  Uses a cache.
        /// </summary>
        /// <param name="intersection">The intersection of the cell address.</param>
        /// <param name="currentView">The associated view.</param>
        /// <returns>A list of  cell addresses with the same Intersection or null,
        /// if only one intersection exists on the view.</returns>
        protected List<CellAddress> FindIntersectionAddress2(Intersection intersection, ViewStateInfo currentView)
        {
            if (intersection != null && currentView != null && currentView.GetOlapView() != null)
            {

                List<CellAddress> ca;

                ca = _DuplicateIntersections.GetCachedSelections(currentView.ViewName, intersection);

                if (ca == null)
                {
                    ca = currentView.GetOlapView().FindIntersectionAddress(intersection);
                }
                if (ca != null)
                {
                    _DuplicateIntersections.CacheSelection(currentView.ViewName, intersection, ca.ToArray());
                    return ca;
                }
            }
            return null;
        }

        /// <summary>
        /// Takes a cell address and returns a list of cell address with the same Intersections.
        /// </summary>
        /// <param name="intersection">The intersection of the cell address.</param>
        /// <param name="currentView">The associated view.</param>
        /// <returns>A list of  cell addresses with the same Intersection or null,
        /// if only one intersection exists on the view.</returns>
        private List<CellAddress> FindDuplicateIntersections(Intersection intersection, ViewStateInfo currentView)
        {
            if (intersection != null)
            {
                List<CellAddress> ca;

                ca = _DuplicateIntersections.GetCachedSelections(currentView.ViewName, intersection);

                if (ca == null)
                {
                    ca = currentView.GetOlapView().FindIntersectionAddress(intersection);
                }
                if (ca != null)
                {
                    //only one occurance of this intersection on the view, so just return..
                    if (ca.Count <= 1)
                    {
                        return null;
                    }
                    else
                    {
                        _DuplicateIntersections.CacheSelection(currentView.ViewName, intersection, ca.ToArray());
                        return ca;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Creates a simpleCellNote.
        /// </summary>
        /// <param name="its">Intersection to create the cell note from.</param>
        /// <param name="noteToClone">Simple cell note to clone.</param>
        protected static simpleCellNote CreateCellNote(Intersection its, simpleCellNote noteToClone)
        {
            string lastUpdated = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.CellNotes.LastUpdated");

            simpleCellNote cn = new simpleCellNote();

            //set the simple coord list.
            cn.simpleCoordList = its.ToSimpleCoordList();

            //set the visble prop
            cn.visible = false;

            //set the application name
            if(noteToClone != null && !String.IsNullOrEmpty(noteToClone.applicationName))
            {
                cn.applicationName = String.Copy(noteToClone.applicationName);
            }
            else
            {
                cn.applicationName = PafApp.ApplicationId;
            }

            //get the creator.
            if (noteToClone != null && !String.IsNullOrEmpty(noteToClone.creator))
            {
                cn.creator = String.Copy(noteToClone.creator);
            }
            else
            {
                cn.creator = PafApp.GetGridApp().LogonInformation.UserName.ToLower();
            }

            //set the data source.
            if (noteToClone != null && !String.IsNullOrEmpty(noteToClone.dataSourceName))
            {
                cn.dataSourceName = String.Copy(noteToClone.dataSourceName);
            }
            else
            {
                cn.dataSourceName = PafApp.DataSourceId;
            }

            //only clone the text.
            if(noteToClone != null && !String.IsNullOrEmpty(noteToClone.text))
            {
                cn.text = String.Copy(noteToClone.text);
            }

            //set the last updated.
            cn.lastUpdated = DateTime.Now;
            cn.lastUpdatedSpecified = true;
            //set the text..
            cn.text = RemoveLastUpdatedFromCellNoteHeading(cn);
            cn.text = lastUpdated + PafApp.GetGridApp().LogonInformation.UserName +
                      PafAppConstants.NL + cn.lastUpdated + PafAppConstants.NL + cn.text;

            return cn;
        }


        /// <summary>
        /// Creates a simpleCellNote.
        /// </summary>
        /// <param name="its">Intersection to create the cell note from.</param>
        /// <param name="value">Value to go in the cell note.</param>
        protected static simpleCellNote CreateCellNote(Intersection its, string value)
        {
            simpleCellNote cn = new simpleCellNote();

            //set the application name
            cn.applicationName = PafApp.ApplicationId;

            //get the creator.
            cn.creator = PafApp.GetGridApp().LogonInformation.UserName.ToLower();

            //set the data source.
            cn.dataSourceName = PafApp.DataSourceId;

            //set the last updated.
            cn.lastUpdated = DateTime.Now;
            cn.lastUpdatedSpecified = true;

            //set the simple coord list.
            cn.simpleCoordList = its.ToSimpleCoordList();

            //set the text..
            cn.text = value;

            //set the visble prop
            cn.visible = false;

            return cn;
        }


        /// <summary>
        /// Updates the simple coord list, data source name, application name, last update,
        /// visible to false and updates the last updated heading in the text field.
        /// </summary>
        /// <param name="kvp"></param>
        protected static void UpdateCellNoteInformation(KeyValuePair<Intersection, simpleCellNote> kvp)
        {
            string lastUpdated = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.CellNotes.LastUpdated");

            //set the simple coord list.
            if (kvp.Value.simpleCoordList == null)
            {
                kvp.Value.simpleCoordList = kvp.Key.ToSimpleCoordList();
            }
            //set the visble prop
            kvp.Value.visible = false;
            //set the data source.
            kvp.Value.dataSourceName = PafApp.DataSourceId;
            //set the application name
            kvp.Value.applicationName = PafApp.ApplicationId;
            
            //get the creator.
            if(String.IsNullOrEmpty(kvp.Value.creator))
            {
                kvp.Value.creator = PafApp.GetGridApp().LogonInformation.UserName.ToLower();
            }
            //set the last updated.
            kvp.Value.lastUpdated = DateTime.Now;
            //set the text..
            kvp.Value.text = RemoveLastUpdatedFromCellNoteHeading(kvp.Value);
            kvp.Value.text = lastUpdated + PafApp.GetGridApp().LogonInformation.UserName +
                             PafAppConstants.NL + kvp.Value.lastUpdated + PafAppConstants.NL + kvp.Value.text;
        }

        private static string RemoveLastUpdatedFromCellNoteHeading(simpleCellNote simpleNote)
        {
            string lastUpdated = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.CellNotes.LastUpdated");
            if (simpleNote != null)
            {
                string[] lines = simpleNote.text.Split(new string[] { PafAppConstants.NL }, StringSplitOptions.RemoveEmptyEntries);
                if (lines != null && lines.Length > 0)
                {
                    if (lines[0].Contains(lastUpdated))
                    {
                        DateTime dt;
                        if (DateTime.TryParse(lines[1], out dt))
                        {
                            StringBuilder sb = new StringBuilder();
                            for (int i = 2; i < lines.Length; i++)
                            {
                                sb.Append(lines[i]);
                                sb.Append(PafAppConstants.NL);
                            }
                            return sb.ToString().Trim();
                        }
                    }
                }
                return simpleNote.text;
            }
            else
            {
                return String.Empty;
            }
        }

        /// <summary>
        /// Write updates and inserts to the client cell note cache, update and insert bucket.
        /// </summary>
        /// <param name="newCellNotes">Dictionary of cell notes to search for updates and inserts.</param>
        /// <param name="currentView">The associated view.</param>
        protected void WriteUpdatesToClientCache(Dictionary<CellAddress, simpleCellNote> newCellNotes,
                                               ViewStateInfo currentView)
        {
            if (newCellNotes != null && newCellNotes.Count > 0)
            {
                foreach (KeyValuePair<CellAddress, simpleCellNote> kvp in newCellNotes)
                {
                    WriteUpdatesToClientCache(kvp.Key, currentView, kvp.Value);
                }
            }
        }

        /// <summary>
        /// Compares the two dictionaries and markes the necessary cells notes for deletion and removes them from
        /// the client cache.
        /// </summary>
        /// <param name="origCellNotes">Dictionary of original cell notes, before user made modifications.</param>
        /// <param name="newCellNotes">Dictionary of cell notes with user modifications.</param>
        /// <param name="currentView">The associated view.</param>
        protected void WriteDeletesToClientCache(Dictionary<Intersection, simpleCellNote> origCellNotes,
                                               Dictionary<CellAddress, simpleCellNote> newCellNotes, ViewStateInfo currentView)
        {
            if (newCellNotes != null && newCellNotes.Count >= 0 && origCellNotes != null && origCellNotes.Count > 0)
            {
                foreach (KeyValuePair<Intersection, simpleCellNote> kvp in origCellNotes)
                {
                    List<CellAddress> ca = FindIntersectionAddress2(kvp.Key, currentView);
                    if (ca != null && ca.Count > 0)
                    {
                        if (!newCellNotes.ContainsKey(ca[0]))
                        {
                            RemoveNote(kvp.Key);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Write updates and inserts to the client cell note cache, update and insert bucket.
        /// </summary>
        /// <param name="cellAddress">Cell address</param>
        /// <param name="currentView">Associated olap view</param>
        /// <param name="cellNote">Cell note</param>
        private void WriteUpdatesToClientCache(CellAddress cellAddress, ViewStateInfo currentView, 
                                               simpleCellNote cellNote)
        {
            Intersection intersection = FindIntersection(cellAddress, currentView);

            if (intersection != null)
            {
                simpleCellNote c = GetNoteFromCache(intersection);
                if (c == null)
                {
                    //we have to insert it...
                    _ClientCache.AddUpdateCellNoteToClientCache(intersection, cellNote);
                    _ClientCache.AddUpdateCellNoteToInsertBucket(intersection, cellNote);
                }
                    //only add an update if an insert is not pending
                else if (!c.text.Trim().Equals(cellNote.text.Trim()) &&
                         !_ClientCache.CellNoteInsert.ContainsKey(intersection))
                {
                    cloneSimpleNoteDate(c, cellNote);
                    _ClientCache.AddUpdateCellNoteToClientCache(intersection, cellNote);
                    _ClientCache.AddUpdateCellNoteToUpdateBucket(intersection, cellNote);
                }
                    //since an insert is pending, update it...
                else if (!c.text.Trim().Equals(cellNote.text.Trim()) &&
                         _ClientCache.CellNoteInsert.ContainsKey(intersection))
                {
                    cloneSimpleNoteDate(c, cellNote);
                    _ClientCache.AddUpdateCellNoteToClientCache(intersection, cellNote);
                    _ClientCache.AddUpdateCellNoteToInsertBucket(intersection, cellNote);
                }
            }
        }

        /// <summary>
        /// Copies all the data from one cell note to the other except for the
        /// text and visible properties.
        /// </summary>
        /// <param name="source">Source simple cell note.</param>
        /// <param name="dest">Destination simple cell note.</param>
        private static void cloneSimpleNoteDate(simpleCellNote source, simpleCellNote dest)
        {
            if (! String.IsNullOrEmpty(source.applicationName))
            {
                dest.applicationName = source.applicationName;
            }
            
            if(source.creator != null)
            {
                dest.creator = source.creator;
            }

            if (!String.IsNullOrEmpty(source.dataSourceName))
            {
                dest.dataSourceName = source.dataSourceName;
            }
            
            dest.id = source.id;

            if (source.lastUpdated != null)
            {
                dest.lastUpdated = source.lastUpdated;
            }

            if(source.simpleCoordList != null)
            {
                dest.simpleCoordList = source.simpleCoordList;
            }
        }

        /// <summary>
        /// Gets the cell note that has been modified from a list of cell address that have the
        /// same intersection.
        /// </summary>
        /// <param name="ca">List of duplicate cell address(have the same intersection).</param>
        /// <param name="intersection">Intersection of the list of duplication addresses.</param>
        /// <param name="currentView">The associated view.</param>
        /// <param name="deletedCell">returns a deleted cell address if one if found.</param>
        /// <returns>the cell note if its found, null if nothing is found.</returns>
        private simpleCellNote GetMasterCellNote(List<CellAddress> ca, Intersection intersection, 
                                                 ViewStateInfo currentView, out CellAddress deletedCell)
        {
            simpleCellNote mn = GetNoteFromCache(intersection);
            deletedCell = null;

            if (ca != null)
            {
                List<simpleCellNote> scn = GetCellNotes(ca, currentView);
                if (scn != null && scn.Count > 0)
                {
                    //if (mn == null)
                    //{
                    //    mn = scn[0];
                    //}

                    for (int i = scn.Count - 1; i >= 0; i--)
                    {
                        if(scn[i] == null)
                        {
                            deletedCell = ca[i];
                            scn.RemoveAt(i);
                        }
                        else if (mn != null)
                        {
                            if (scn[i].text.Trim().Equals(mn.text.Trim()))
                            {
                                scn.RemoveAt(i);
                            }
                        }
                    }
                }
                if(scn != null && scn.Count == 1)
                {
                    mn = scn[0];
                }
                else
                {
                    mn = null;
                }
            }
            return mn;
        }

        /// <summary>
        /// Gets the cell note for a list of cell addresses.
        /// </summary>
        /// <param name="ca">List of duplicate cell address(have the same intersection).</param>
        /// <param name="currentView">The associated view.</param>
        /// <returns>a list of simple cell notes.</returns>
        private static List<simpleCellNote> GetCellNotes(ICollection<CellAddress> ca, 
                                                         ViewStateInfo currentView)
        {
            List<simpleCellNote> lst = null;
            try
            {
                if (ca != null)
                {
                    lst = new List<simpleCellNote>(ca.Count);
                    foreach (CellAddress c in ca)
                    {
                        simpleCellNote cn = currentView.GetOlapView().Grid.GetCellNote(new Range(c));
                        if (cn != null)
                        {
                            lst.Add(cn);
                        }
                        else
                        {
                            lst.Add(null);
                        }
                    }
                }
            }
            catch(COMException)
            {
                lst = null;
            }
            return lst;
        }

        /// <summary>
        /// Takes the target cell address and finds any cell addresses with the same intersection.
        /// It then finds the cell note that is the has been updated most recently(because all 
        /// duplicate cell address will match until a user changes it) however as soon as a user clicks
        /// on the cell that cell note will be update to the most recent text.  As a result at most only
        /// one cell note can be different from the other at one time, which would me that, that is
        /// the newly updated note.  
        /// </summary>
        /// <param name="cellAddress">Cell address</param>
        /// <param name="currentView">The olap view associated with the view.</param>
        protected void CopyCellNotesToDuplicateCells(CellAddress cellAddress, 
                                                   ViewStateInfo currentView)
        {
            Intersection intersection = FindIntersection(cellAddress, currentView);
            if (intersection == null) return;

            List<CellAddress> ca = FindDuplicateIntersections(intersection, currentView);
            if (ca == null) return;
                 
            try
            {
                CellAddress deletedCell;
                simpleCellNote scn = GetMasterCellNote(
                    ca,
                    intersection,
                    currentView, out deletedCell);

                if (scn != null)
                {
                    List<Range> ranges = new List<Range>();
                    List<simpleCellNote> cellNotes = new List<simpleCellNote>();
                    foreach (CellAddress c in ca)
                    {
                        ranges.Add(new Range(c));
                        cellNotes.Add(scn);
                    }
                    currentView.GetOlapView().Grid.SetCellNotes(ranges, cellNotes, false, false);
                    WriteUpdatesToClientCache(ca[0], currentView, scn);
                }
                if (scn == null && deletedCell != null)
                {
                    List<Range> ranges = new List<Range>();
                    foreach (CellAddress c in ca)
                    {
                        if (!c.Equals(intersection))
                        {
                            ranges.Add(new Range(c));
                            DeleteCellNote(c, currentView);
                        }
                    }
                    currentView.GetOlapView().Grid.DeleteCellNotes(ranges);
                }
                
            }
            catch(COMException)
            {
                return;
            }
            catch(Exception)
            {
                return;
            }
        }
    }
}