#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Compression.PafSimpleCellNotes;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Exceptions;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Application.Forms;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView.MemberTag;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Properties;
using PrintStyle = Titan.Pace.Application.Core.PrintStyle;
using Range = Titan.Pace.ExcelGridView.Utility.Range;
using Tuple = Titan.Pace.DataStructures.Tuple;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// Pulls the metadata and data from the mid-tier and builds a view on an Excel worksheet
    /// </summary>
    internal class ExcelBuildView
    {
        #region Private Variables

        private const int GRID_START_ROW = 4;
        private ViewMngr _ViewMngr;
        private OlapView _OlapView;
        private IGridInterface _Grid;
        private int _SheetHashCode;
        private bool _ResetProtectionMngr;

        #endregion Private Variables

        #region Public Variables



        #endregion Public Variables

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sht">Excel worksheet to perform the formatting.</param>
        /// <param name="viewName">name of the view.</param>
        /// <param name="resetProtectionMngr">reset the protection manager.</param>
        /// <param name="attributeView">Does the view contain attributes.</param>
        public ExcelBuildView(Worksheet sht, string viewName, bool resetProtectionMngr, bool attributeView)
            : this(sht,
                    viewName,
                    resetProtectionMngr, PafApp.GetGridApp().GetStyleFormat(PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Default.Style.Name")),
                    attributeView)
        {
        }

        ///// <summary>
        ///// Constructor
        ///// </summary>
        ///// <param name="sht">Excel worksheet to perform the formatting.</param>
        ///// <param name="viewName">name of the view.</param>
        ///// <param name="resetProtectionMngr">reset the protection manager.</param>
        ///// <param name="lockMngr">Lock manager to use.  Overrides any GlobalLockMngr</param>
        //public ExcelBuildView(Worksheet sht, string viewName, bool resetProtectionMngr, LockMngr lockMngr)
        //    : this(sht,
        //            viewName,
        //            resetProtectionMngr, PafApp.GetGridApp().GetStyleFormat(PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Default.Style.Name")),
        //            lockMngr)
        //{
        //}

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="sht">Excel worksheet to perform the formatting.</param>
        /// <param name="viewName">name of the view.</param>
        /// <param name="resetProtectionMngr">reset the protection manager.</param>
        /// <param name="format">Defulat format</param>
        /// <param name="attributeView">Does the view contain attributes.</param>
        public ExcelBuildView(Worksheet sht, string viewName, bool resetProtectionMngr, Format format, bool attributeView)
        {
            _ResetProtectionMngr = resetProtectionMngr;

            _ViewMngr = PafApp.GetViewMngr();
            _SheetHashCode = sht.GetHashCode();
            _ViewMngr.ResetView(_SheetHashCode, _ResetProtectionMngr);
            _Grid = new ExcelGridImpl(sht);
            //Stores the Excel worksheet name and the grid interface in a hash table in the ViewMngr class
            //Moved from the top of InitGrid.
            _ViewMngr.RegisterGrid(_SheetHashCode, viewName, _Grid, format, attributeView);

            _OlapView = _ViewMngr.CurrentOlapView;

        }


        #endregion Constructor

        #region Public Enums

        /// <summary>
        /// Range to format.
        /// </summary>
        public enum FormatRange
        {
            Headers,
            DataCol,
            DataRow,
            ViewHeaders,
            MemberTagCol,
            MemberTagRow
        }

        #endregion Public Enums

        #region Public Methods

        /// <summary>
        /// This method is called by a user event to coordinate the view building process
        /// </summary>
        /// <param name="pafView">The pafView returned from the server.</param>
        /// <param name="setPrintRange">Automatically set the print range.</param>
        /// <param name="setRowGroups">Set the Excel Row Groups</param>
        /// <param name="setColumnGroups">Set the Excel Column Groups</param>
        /// <param name="rowParentFirst">The view have a row user selector with Parent First specified.</param>
        /// <param name="colParentFirst">The view have a column user selector with Parent First specified.</param>
        public void InitializeGrid(pafView pafView, bool setPrintRange, bool setRowGroups = true, bool setColumnGroups = true, bool? rowParentFirst = null, bool? colParentFirst = null)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            _OlapView = _ViewMngr.CurrentOlapView;
            _Grid.Clear();
            InitializePOV(pafView, rowParentFirst, colParentFirst);

            bool wasRendered = _OlapView.Render(_Grid, pafView, Settings.Default.ShowPageMembers, setRowGroups, setColumnGroups);

            //if the view was rendered, then continue.
            if (wasRendered)
            {
                if (!PafApp.TestHarnessRunning)
                {
                    //uncompress the cell notes.
                    if (pafView.viewSections[0].cellNotes != null && pafView.viewSections[0].cellNotes.Length > 0)
                    {
                        PafSimpleCellNotes pscn = new PafSimpleCellNotes(pafView.viewSections[0].cellNotes);
                        pscn.uncompressData();
                    }

                    _ViewMngr.CurrentView.SetCellNotes(pafView.viewSections[0].cellNotes);
                }

                if (_OlapView.IsEmpty == false)
                {
                    if (_ResetProtectionMngr)
                    {
                        //Apply Non-Plannable & Forward Plannable Locks
                        PafApp.GetViewMngr().CurrentProtectionMngr.AddSystemLocks();

                        //Add the locks for Singleton rules
                        PafApp.GetViewMngr().CurrentProtectionMngr.AddSingletonRules();

                        //Add the locks for BOH rules
                        if (Settings.Default.BOHProtection)
                        {
                            PafApp.GetViewMngr().CurrentProtectionMngr.AddBOHProtection();
                        }
                        //Add any existing session locks.
                        PafApp.GetViewMngr().CurrentProtectionMngr.AddSessionLocks();
                    }
                }


                //Apply the View formatting
                //TODO this call seems to take a looooonnnnggg time!!!!
                _ViewMngr.ApplyFormatting(_ResetProtectionMngr);

                try
                {
                    Globals.ThisWorkbook.Application.DisplayAlerts = false;
                    _OlapView.MemberTag.MergeMemberTagDataCells();
                }
                finally
                {
                    Globals.ThisWorkbook.Application.DisplayAlerts = true;
                }

                if (!_OlapView.IsEmpty && _OlapView.DataRange != null)
                {
                    if (_ResetProtectionMngr)
                    {
                        //Apply Protection Processing triggered by BOH rules
                        PafApp.GetViewMngr().CurrentProtectionMngr.PruneDependencies();
                    }

                    
                    _ViewMngr.CurrentView.PrintStyle = new PrintStyle(pafView.printStyle, _OlapView.ColRange, _OlapView.RowRange);
                    //per Carolyn we don't want to take the hit in Office 2010 (ie 14)
                    //if (setPrintRange) //Set the print range.
                    //{
                    //    if (PafApp.GetGridApp().VersionAsFloat >= 14)
                    //    {
                    //        _ViewMngr.CurrentView.GetGrid().SetPrintRange(new ContiguousRange(
                    //            _ViewMngr.CurrentView.GetOlapView().HeaderRange.TopLeft,
                    //            _ViewMngr.CurrentView.GetOlapView().DataRange.BottomRight),
                    //             _ViewMngr.CurrentView.PrintStyle);
                    //    }
                    //}

                    //Fix TTN-319 If the view section is read only, then lock it down.
                    if (pafView.viewSections[0].readOnly)
                    {
                        PafApp.GetViewMngr().CurrentGrid.Lockcells(
                            PafApp.GetViewMngr().CurrentOlapView.DataRange);
                        //Fix TTN-722.
                        PafApp.GetViewMngr().CurrentView.ReadOnly = true;
                    }
                    else
                    {
                        //Fix TTN-722.
                        PafApp.GetViewMngr().CurrentView.ReadOnly = false;
                    }

                    bool screenUpdate = _OlapView.RowGroupSpec == null && _OlapView.ColGroupSpec == null;

                    //TTN-2503
                    CellAddress freePaneAdd = _OlapView.DataRange.TopLeft;
                    if (_OlapView.FreezePaneCell != null)
                    {
                        PafApp.GetLogger().InfoFormat("Freeze pane overridden to cell: {0} ", new object[] { freePaneAdd.ToString()});
                        freePaneAdd = _OlapView.FreezePaneCell;
                    }
                    PafApp.GetGridApp().SetFreezePane(freePaneAdd, screenUpdate);
                }
            }
            else
            {
                string expander;
                string exception;

                if (pafView.viewSections[0].suppressed)
                {
                    expander = PafApp.GetGridApp().ActionsPane.GetCachedSuppressZeroSelectionsAsString(pafView.name);
                    exception = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.SuppressedViewNoData");
                }
                else
                {
                    expander = String.Empty;
                    exception = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.ViewNoHasData");
                }

                PafApp.MessageBox().Show(
                   exception,
                   PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                   expander,
                   SystemIcons.Information,
                   frmMessageBox.frmMessageBoxButtons.OK);

                throw new NoDataException(exception);
            }

            _ViewMngr.CurrentView.ViewHasBeenBuilt = true;
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("InitializeGrid (includes formatting time), runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Pull metadata from the mid-tier and populate the OlapView row/col arraylists
        /// </summary>
        /// <param name="pafView"></param>
        /// <param name="rowParentFirst">The view have a row user selector with Parent First specified.</param>
        /// <param name="colParentFirst">The view have a column user selector with Parent First specified.</param>
        public void InitializePOV(pafView pafView, bool? rowParentFirst = null, bool? colParentFirst = null)
        {
            int startRow = GRID_START_ROW;
            const int viewSectionNumber = 0;
            string blankMember = Settings.Default.Blank;

            Stopwatch stopWatch = Stopwatch.StartNew();
            
            pafViewSection viewSection = pafView.viewSections[viewSectionNumber]; // will be pulled from pafView

            //If their are more than 2 headers, then adjust the start row accordingly.
            if (pafView.viewSections[viewSectionNumber].pafViewHeaders != null)
            {
                if (pafView.viewSections[viewSectionNumber].pafViewHeaders.Length > 2)
                    startRow = startRow + (pafView.viewSections[viewSectionNumber].pafViewHeaders.Length - 2);
            }

            _OlapView.StartCell = new CellAddress(startRow, 1);//will be pulled from pafView
            _OlapView.PrimaryFormattingAxis = Convert.ToString(viewSection.primaryFormattingAxis).ParseEnum<ViewAxis>();
            //_OlapView.PrimaryFormattingAxis = viewSection.primaryFormattingAxis.ToString();
            _OlapView.DimensionPriority = viewSection.dimensionsPriority;
            _OlapView.IsSuppressed = viewSection.suppressed;
            _OlapView.HasAttributes = viewSection.HasAttributes();
            _OlapView.ConditionalFormat = viewSection.conditionalFormatName;

            #region Member Tags
            //add the member tag comment names.
            if(pafView.viewSections[viewSectionNumber].memberTagCommentEntries != null)
            {
                foreach(memberTagCommentEntry commentEntry in pafView.viewSections[viewSectionNumber].memberTagCommentEntries)
                {
                    if(!String.IsNullOrEmpty(commentEntry.name))
                    {
                        _OlapView.MemberTag.MemberTagCommentNames.Add(commentEntry.name);
                    }
                }

                //_OlapView.MemberTag.MemberTagCommentNames.AddRange(pafView.viewSections[viewSectionNumber].memberTagCommentNames);
            }

            //cache the comment member tag id/psoitions
            if(pafView.viewSections[viewSectionNumber].rowMemberTagCommentPositions != null &&
                pafView.viewSections[viewSectionNumber].rowMemberTagCommentPositions.Length > 0)
            {
                foreach (memberTagCommentPosition row in pafView.viewSections[viewSectionNumber].rowMemberTagCommentPositions)
                {
                    //_OlapView.MemberTag.RowCommentMemberTags.Add(row.axis, row.name);
                    List<string> values;
                    if (_OlapView.MemberTag.RowCommentMemberTags.TryGetValue(row.axis, out values))
                    {
                        values.Add(row.name);
                    }
                    else
                    {
                        values = new List<string> {row.name};
                        _OlapView.MemberTag.RowCommentMemberTags.Add(row.axis, values);
                    }
                }
            }

            //cache the comment member tag id/psoitions
            if (pafView.viewSections[viewSectionNumber].colMemberTagCommentPositions != null &&
                pafView.viewSections[viewSectionNumber].colMemberTagCommentPositions.Length > 0)
            {
                foreach(memberTagCommentPosition col in pafView.viewSections[viewSectionNumber].colMemberTagCommentPositions)
                {
                    //_OlapView.MemberTag.ColCommentMemberTags.Add(col.axis, col.name);
                    List<string> values;
                    if (_OlapView.MemberTag.ColCommentMemberTags.TryGetValue(col.axis, out values))
                    {
                        values.Add(col.name);
                    }
                    else
                    {
                        values = new List<string> {col.name};
                        _OlapView.MemberTag.ColCommentMemberTags.Add(col.axis, values);
                    }
                }
            }

            #endregion Member Tags

            #region Page Tuples

            //Page tuples(really single members)
            if (viewSection.pageTuples != null)
            {
                foreach (pageTuple t in viewSection.pageTuples)
                {
                    _OlapView.PageDims.Add(t.axis, t.member);

                    //Populate dictionary of unique view members
                    if (!_OlapView.ViewMembers.ContainsKey(t.member))
                    {
                        _OlapView.ViewMembers.Add(t.member, "Page");
                    }
                }
            }

            #endregion Page Tuples

            #region Row Tuples

            //Row tuples
            if (viewSection.rowTuples != null)
            {
                int r = 0;
                List<string>[] rowLists = null;
                foreach(viewTuple rowTuple in viewSection.rowTuples)
                {
                    if (rowLists == null)
                    {
                        rowLists = new List<string>[rowTuple.memberDefs.Length];
                        for (int k = 0; k < rowLists.Length; k++)
                        {
                            rowLists[k] = new List<string>(viewSection.rowTuples.Length);
                        }
                    }

                    //Row tuple members
                    int dimNum = 0;
                    foreach(string memberDef in rowTuple.memberDefs)
                    {
                        string dimName = viewSection.rowAxisDims[dimNum];

                        rowLists[dimNum].Add(memberDef);

                        //Populate dictionary of unique view members
                        if (!_OlapView.ViewMembers.ContainsKey(memberDef))
                        {
                            _OlapView.ViewMembers.Add(memberDef, "Row");
                        }

                        //Build a dictionary of member tag comments.
                        if (_OlapView.MemberTag.RowCommentMemberTags != null && _OlapView.MemberTag.RowCommentMemberTags.Count > 0)
                        {
                            AddCommentMemberTagItems(rowTuple, dimName, 
                                _OlapView.MemberTag.RowMemberTagCommentValues,
                                _OlapView.MemberTag.RowCommentMemberTags,
                                viewSection.rowAxisDims,
                                _OlapView);

                        }
                        dimNum++;
                    }

                    if (rowParentFirst.HasValue)
                    {
                        _OlapView.RowParentFirst.Add(r, rowParentFirst.Value);
                    }
                    else if (rowTuple.parentFirstSpecified)
                    {
                        _OlapView.RowParentFirst.Add(r, rowTuple.parentFirst);
                    }

                    string mbrHash = String.Concat(rowTuple.memberDefs);
                    //Add to RowDimHash
                    if (_OlapView.RowDimHash.ContainsKey(mbrHash))
                    {
                        _OlapView.RowDimHash[mbrHash].Add(r);
                    }
                    else
                    {
                        List<int> mbrList = new List<int> {r};
                        _OlapView.RowDimHash.Add(mbrHash, mbrList);
                    }

                    //this is a memeber tag
                    if (rowTuple.memberTag)
                    {
                        CopyMemberTagTupleProtection(rowTuple);
                    }

                    //if (rowTuple.secCondFormatSpecified && rowTuple.secCondFormat)
                    if(rowTuple.tupleConditionalStyle != null && !rowTuple.tupleConditionalStyle.primary)
                    {
                        _OlapView.SecondaryConditionalTuples.Add(mbrHash);
                    }

                    //Get Header Styles
                    Styles(rowTuple, r, ViewAxis.Row, FormatRange.Headers);

                    //Get Data Row Styles
                    Styles(rowTuple, r, ViewAxis.Row, FormatRange.DataRow);

                    //Row tuple numeric formatting
                    if (rowTuple.numberFormat != null)
                    {
                        if (_OlapView.NumericRowFormats.ContainsKey(rowTuple.numberFormat.pattern))
                        {
                            _OlapView.NumericRowFormats[rowTuple.numberFormat.pattern].Add(new Tuple(r, ViewAxis.Row, rowTuple.memberTag));
                        }
                        else
                        {
                            List<Tuple> tuples = new List<Tuple> { new Tuple(r, ViewAxis.Row, rowTuple.memberTag) };
                            _OlapView.NumericRowFormats.Add(rowTuple.numberFormat.pattern, tuples);
                        }
                    }

                    //Store locations for blank rows
                    if (rowTuple.memberDefs[rowTuple.memberDefs.Length - 1].ToUpper() == blankMember)
                    {
                        _OlapView.RowBlanks.Add(r);
                    }
                    //This a memeber tag
                    else if (rowTuple.memberTag)
                    {
                        _OlapView.RowBlanks.Add(r);
                        _OlapView.MemberTag.RowMbrTagBlanks.Add(r, rowTuple.memberDefs[0]);
                    }

                    //Store row heights
                    if (rowTuple.rowHeightSpecified)
                    {
                        float f = rowTuple.rowHeight;
                        if (_OlapView.RowHeights.ContainsKey(f))
                        {
                            _OlapView.RowHeights[f].Add(r);
                        }
                        else
                        {
                            List<int> positions = new List<int> {r};
                            _OlapView.RowHeights.Add(f, positions);
                        }
                    }

                    //Store the Symetric tuple groups and positions
                    if (rowTuple.derivedHeaderGroupNo != null)
                    {
                        _OlapView.RowSymetricTupleGroups.Add(r, rowTuple.derivedHeaderGroupNo);
                        if (rowTuple.memberTag)
                        {
                            //if there are two are more tuples then we need to merge them, if not no merge is necessary.
                            if (rowTuple.memberDefs.Length >= 2)
                            {
                                _OlapView.MemberTag.RowMbrTagSymetricTupleGroups.CacheSelection(rowTuple.memberDefs[0], r, rowTuple.memberDefs.Length);
                            }
                        }
                    }
                    r++;
                }
                int pos = 0;
                if (rowLists != null && rowLists.Length > 0)
                {
                    foreach (List<string> rowList in rowLists)
                    {
                        _OlapView.RowDims.Add(viewSection.rowAxisDims[pos++], rowList);
                    }
                }
            }

            #endregion Row Tuples

            #region Column Tuples

            //Column tuples
            if (viewSection.colTuples != null)
            {
                int c = 0;
                List<string>[] colLists = null;
                foreach(viewTuple colTuple in viewSection.colTuples)
                {
                    if (colLists == null)
                    {
                        colLists = new List<string>[colTuple.memberDefs.Length];
                        for (int k = 0; k < colLists.Length; k++)
                        {
                            colLists[k] = new List<string>(viewSection.colTuples.Length);
                        }
                    }

                    //Col tuple members
                    int dimNum = 0;
                    foreach(string memberDef in colTuple.memberDefs)
                    {
                        string dimName = viewSection.colAxisDims[dimNum];

                        colLists[dimNum].Add(memberDef);

                        //Populate dictionary of unique view members
                        if (!_OlapView.ViewMembers.ContainsKey(memberDef))
                        {
                            _OlapView.ViewMembers.Add(memberDef, "Col");
                        }

                        //Build a dictionary of member tag comments.
                        if (_OlapView.MemberTag.ColCommentMemberTags != null && _OlapView.MemberTag.ColCommentMemberTags.Count > 0)
                        {
                            List<string> value;
                            if (_OlapView.MemberTag.ColCommentMemberTags.TryGetValue(dimName, out value))
                            {
                                AddCommentMemberTagItems(colTuple, dimName,
                                    _OlapView.MemberTag.ColMemberTagCommentValues,
                                    _OlapView.MemberTag.ColCommentMemberTags,
                                    viewSection.colAxisDims,
                                    _OlapView);
                            }
                        }
                        dimNum++;
                    }

                    if (colParentFirst.HasValue)
                    {
                        _OlapView.ColParentFirst.Add(c, colParentFirst.Value);
                    }
                    else if (colTuple.parentFirstSpecified)
                    {
                        _OlapView.ColParentFirst.Add(c, colTuple.parentFirst);
                    }

                    //Add to ColDimHash
                    string mbrHash = String.Concat(colTuple.memberDefs);
                    if (_OlapView.ColDimHash.ContainsKey(mbrHash))
                    {
                        _OlapView.ColDimHash[mbrHash].Add(c);
                    }
                    else
                    {
                        List<int> mbrList = new List<int> {c};
                        _OlapView.ColDimHash.Add(mbrHash, mbrList);
                    }

                    //this is a memeber tag
                    if (colTuple.memberTag)
                    {
                        CopyMemberTagTupleProtection(colTuple);
                    }

                    //if (colTuple.secCondFormatSpecified && colTuple.secCondFormat)
                    if (colTuple.tupleConditionalStyle != null && !colTuple.tupleConditionalStyle.primary)
                    {
                        _OlapView.SecondaryConditionalTuples.Add(mbrHash);
                    }

                    //Get Header Styles
                    Styles(colTuple, c, ViewAxis.Col, FormatRange.Headers);

                    //Get Data Column Styles
                    Styles(colTuple, c, ViewAxis.Col, FormatRange.DataCol);

                    if (colTuple.numberFormat != null)
                    {
                        //Col tuple numeric formatting
                        if (_OlapView.NumericColFormats.ContainsKey(colTuple.numberFormat.pattern))
                        {
                            _OlapView.NumericColFormats[colTuple.numberFormat.pattern].Add(new Tuple(c, ViewAxis.Col, colTuple.memberTag));
                        }
                        else
                        {
                            List<Tuple> tuples = new List<Tuple> { new Tuple(c, ViewAxis.Col, colTuple.memberTag) };
                            _OlapView.NumericColFormats.Add(colTuple.numberFormat.pattern, tuples);
                        }
                    }

                    //Store locations for blank cols
                    if (colTuple.memberDefs[colTuple.memberDefs.Length - 1].ToUpper() == blankMember)
                    {
                        _OlapView.ColBlanks.Add(c);
                    }
                    //this is a memeber tag
                    else if (colTuple.memberTag)
                    {
                        _OlapView.ColBlanks.Add(c);
                        _OlapView.MemberTag.ColMbrTagBlanks.Add(c, colTuple.memberDefs[0]);
                    }

                    //Store col widths
                    if (colTuple.columnWidthSpecified)
                    {
                        float f = colTuple.columnWidth;
                        if (_OlapView.ColWidths.ContainsKey(f))
                        {
                            _OlapView.ColWidths[f].Add(c);
                        }
                        else
                        {
                            List<int> positions = new List<int> {c};
                            _OlapView.ColWidths.Add(f, positions);
                        }
                    }

                    //Store the Symetric tuple groups and positions
                    if (colTuple.derivedHeaderGroupNo != null)
                    {
                        _OlapView.ColSymetricTupleGroups.Add(c, colTuple.derivedHeaderGroupNo);
                        if (colTuple.memberTag)
                        {
                            //if there are two are more tuples then we need to merge them, if not no merge is necessary.
                            if (colTuple.memberDefs.Length >= 2)
                            {
                                _OlapView.MemberTag.ColMbrTagSymetricTupleGroups.CacheSelection(colTuple.memberDefs[0], c, colTuple.memberDefs.Length);
                            }
                        }
                    }
                    c++;
                }

                int pos = 0;
                if (colLists != null && colLists.Length > 0)
                {
                    foreach (List<string> colList in colLists)
                    {
                        _OlapView.ColDims.Add(viewSection.colAxisDims[pos++], colList);
                    }
                }
            }

            #endregion Column Tuples

            #region View Headers

            //Add View Header Formatting
            if (viewSection.pafViewHeaders != null)
            {
                
                for (int i = 0; i < viewSection.pafViewHeaders.Length; i++)
                {
                    if (viewSection.pafViewHeaders[i] != null)
                    {
                        _OlapView.Headers.Add(viewSection.pafViewHeaders[i].label);

                        if (pafView.viewSections[viewSectionNumber].pafViewHeaders[i].globalStyleName != null)
                        {
                            if (_OlapView.ViewHeaderStyles.ContainsKey(pafView.viewSections[viewSectionNumber].pafViewHeaders[i].globalStyleName))
                            {
                                _OlapView.ViewHeaderStyles[pafView.viewSections[viewSectionNumber].pafViewHeaders[i].globalStyleName].Add(new Range(new CellAddress(i + 1, 1)));
                            }
                            else
                            {
                                List<Range> rng = new List<Range> {new Range(new CellAddress(i + 1, 1))};
                                _OlapView.ViewHeaderStyles.Add(pafView.viewSections[viewSectionNumber].pafViewHeaders[i].globalStyleName, rng);
                            }
                        }
                    }
                }
            }

            #endregion View Headers

            #region Locked Collections

            int rAliases = 0;
            int cAliases = 0;
            //need to get primary Row count.
            if (_OlapView.RowDims.Count > 0)
            {
                string[] rDims = new string[_OlapView.RowDims.Count];
                _OlapView.RowDims.Keys.CopyTo(rDims, 0);
                rAliases = PafApp.GetViewTreeView().GetAdditionalRowColumnFormat(pafView.name, rDims);
            }
            //need to get primary Col count.
            if (_OlapView.ColDims.Count > 0)
            {
                string[] cDims = new string[_OlapView.ColDims.Count];
                _OlapView.ColDims.Keys.CopyTo(cDims, 0);
                cAliases = PafApp.GetViewTreeView().GetAdditionalRowColumnFormat(pafView.name, cDims);
            }


            //Set startRow & startCol - Fixed for Aliases!
            startRow = _OlapView.StartCell.Row + _OlapView.ColDims.Count + cAliases;
            int startCol = _OlapView.StartCell.Col + _OlapView.RowDims.Count + rAliases;

            //Forward Plannable Locked Intersections
            if (viewSection.forwardPlannableLockedCell != null && viewSection.forwardPlannableLockedCell.Length > 0)
            {
                Range range = viewSection.forwardPlannableLockedCell.GetRanges(startRow, startCol);
                range.Sorted = true;
                if (range.ContiguousRanges.Count > 0)
                {
                    _OlapView.ForwardPlannableRange = range;
                }
            }

            //Non-Plannable Locked Intersections
            if (viewSection.notPlannableLockedCells != null && viewSection.notPlannableLockedCells.Length > 0)
            {
                Range range = viewSection.notPlannableLockedCells.GetRanges(startRow, startCol);
                range.Sorted = true;
                if (range.ContiguousRanges.Count > 0)
                {
                    _OlapView.NonPlannableRange = range;
                }
            }

            //Invalid Attribute Locked Intersections
            if (viewSection.invalidAttrIntersectionsLC != null && viewSection.invalidAttrIntersectionsLC.Length > 0)
            {
                Range range = viewSection.invalidAttrIntersectionsLC.GetRanges(startRow, startCol);
                if (range.ContiguousRanges.Count > 0)
                {
                    _OlapView.InValidAttributeRange = range;
                }
            }


            //Invalid Member Tag Intersections
            if (viewSection.invalidMemberTagIntersectionsLC != null && viewSection.invalidMemberTagIntersectionsLC.Length > 0)
            {
                Range range = viewSection.invalidMemberTagIntersectionsLC.GetRanges(startRow, startCol);

                if (range.ContiguousRanges.Count > 0)
                {

                    _OlapView.InValidMemberTagRange = range;
                    //TTN-1972
                    foreach (ContiguousRange cr in range.ContiguousRanges)
                    {
                        foreach (Cell cellAddress in cr.Cells)
                        {
                            _OlapView.MemberTag.AddInvalidLockedCell(cellAddress.CellAddress);
                        }
                    }
                }
            }

            //Session Locks
            if (viewSection.sessionLockedCells != null && viewSection.sessionLockedCells.Length > 0)
            {
                Range range = viewSection.sessionLockedCells.GetRanges(startRow, startCol);

                if (range.ContiguousRanges.Count > 0)
                {
                    _OlapView.SessionLockProtectedRange = range;
                }
            }

            //Invalid Replication Intersections
            if (viewSection.invalidReplicationIntersections != null && viewSection.invalidReplicationIntersections.Length > 0)
            {
                foreach (simpleCoordList scl in viewSection.invalidReplicationIntersections)
                {
                    if(scl.compressed)
                    {
                        PafSimpleCoordList pscl = new PafSimpleCoordList(scl);
                        pscl.uncompressData();
                        _OlapView.InValidReplicationRange.Add(pscl);

                    }
                    else
                    {
                        PafSimpleCoordList pscl = new PafSimpleCoordList(scl);
                        _OlapView.InValidReplicationRange.Add(pscl);
                    }
                }
            }

            #endregion Locked Collections

            #region Default Sorting

            //add sort stuff
            //TTN-2516 & TTN-2552
            if (!viewSection.empty && viewSection.sortingTuples != null && !_OlapView.ServerSortOverridden && _OlapView.DataRange != null)
            {
                DateTime startTime = DateTime.Now;

                List<int> sortPositions = new List<int>();
                bool isInDataRange = true;
                //Build the row list to contain all rows with selected cells.
                List<ContiguousRange> crs = new List<ContiguousRange> {_OlapView.DataRange};
                foreach (ContiguousRange contigRange in crs)
                {
                    foreach (int row in contigRange.Rows)
                    {
                        //Determine if every position in the selected range is in the View Section Data Range
                        if (!PafApp.GetViewMngr().CurrentOlapView.DataRange.InContiguousRange(row))
                        {
                            isInDataRange = false;
                            break;
                        }

                        //modified for aliases
                        int selectedPosition = row - _OlapView.StartCell.Row - (_OlapView.ColDims.Count - 1 + _OlapView.ColAliases);
                        if (!sortPositions.Contains(selectedPosition))
                        {
                            sortPositions.Add(selectedPosition);
                        }
                    }

                    //Get out of outer loop after breaking out of inner loop.
                    if (!isInDataRange)
                    {
                        break;
                    }
                }

                _OlapView.SortSelections.Clear();
                _OlapView.SortPositions.Clear();
                _OlapView.SortPositions.AddRange(sortPositions);



                SortSelection sortSelections = new SortSelection(sortPositions);

                Dictionary<int, TupleItem> tupleItems = new Dictionary<int, TupleItem>();
                int position = 0;
                foreach (string dim in _OlapView.ColDims.Keys)
                {
                    foreach (string member in _OlapView.ColDims[dim])
                    {
                        //Only include non blanks
                        memberTagDef mbrTagDef = PafApp.GetMemberTagInfo().GetMemberTag(member);
                        if (member.ToUpper() != blankMember && mbrTagDef == null)
                        {
                            string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(PafApp.GetViewMngr().CurrentView.ViewName, dim);
                            //string mbrName = PafApp.GetGridApp().GetSimpleTrees().GetAlias(dim, member, aliasTable) ?? member;
                            string mbrName = PafApp.SimpleDimTrees.GetMemberAliasValue(dim, member, aliasTable) ?? member;
                            TupleItem tupleItem;
                            if (!tupleItems.TryGetValue(position, out tupleItem))
                            {
                                tupleItems.Add(position, new TupleItem(position, mbrName, true));
                            }
                            else
                            {
                                tupleItem.TupleString += " : " + mbrName;
                            }
                            position++;
                        }
                    }
                    position = 0;
                }

                foreach (sortingTuple t in viewSection.sortingTuples)
                {
                    int intCount = 0;
                    StringBuilder columnSelection = new StringBuilder();
                    foreach (string intersectionDimName in t.intersection.dimensions)
                    {
                        if (_OlapView.ColDims.ContainsKey(intersectionDimName))
                        {
                            string memberName = t.intersection.coordinates[intCount];
                            if (_OlapView.ColDims[intersectionDimName].Any(member => member.Equals(memberName)))
                            {
                                string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(pafView.name, intersectionDimName);
                                string p, a;
                                PafApp.GetViewTreeView().GetAliasesValues(pafView.name, intersectionDimName, aliasTable, memberName, out p, out a);
                                columnSelection.Append(p).Append(" : ");
                            }
                        }
                        intCount++;
                        if (intCount == t.intersection.dimensions.Count())
                        {
                            columnSelection.Remove(columnSelection.Length - 3, 3);
                        }
                    }

                    TupleItem tuple = tupleItems.Values.ToList().Find(x => x.TupleString == columnSelection.ToString());
                    if (tuple != null)
                    {
                        //Set the sort selections.
                        bool sortDir = t.sortOrder == sortOrder.Ascending;
                        sortSelections.SortDirectionSelections.Add(sortDir);
                        sortSelections.AreColumnHeadersSelected = true;
                        SortBy sortBy = new SortBy(tuple.Position, sortDir);
                        _OlapView.SortByList.Add(sortBy);
                        sortSelections.SortOnColumnSelections.Add(tuple.TupleString);
                    }
                }
                _OlapView.ServerSortSpecified = true;
                _OlapView.SortSelections.Add(sortSelections);

                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Completed sort view structure build (POV): {0} (s)", new object []{ (DateTime.Now - startTime).TotalSeconds });
            }

            #endregion Default Sorting

            #region Grouping
            
            if(viewSection.groupingSpecs != null && viewSection.groupingSpecs.Length > 0)
            {
                foreach (var spec in viewSection.groupingSpecs)
                {
                    if(spec == null) continue;
                    GroupingSpec gs = new GroupingSpec(spec);
                    switch (gs.ViewAxis)
                    {
                        case ViewAxis.Row:
                            _OlapView.RowGroupSpec = gs;
                            break;
                        case ViewAxis.Col:
                            _OlapView.ColGroupSpec = gs;
                            break;
                    }
                }
            }
        
            #endregion Grouping

            #region Freeze Pane
            //TTN-2503
            if (viewSection.rowFreezePaneIndex > 0 || viewSection.colFreezePaneIndex > 0)
            {
                _OlapView.FreezePaneCell = new CellAddress(viewSection.rowFreezePaneIndex, viewSection.colFreezePaneIndex);
            }


            #endregion Freeze Pane

            //If either or both of the row/col axis tuples do not exist
            if (viewSection.empty)
            {
                _OlapView.IsEmpty = viewSection.empty;
            }
            stopWatch.Stop();
            PafApp.GetLogger().InfoFormat("InitializePOV  runtime: {0} (ms)", stopWatch.ElapsedMilliseconds.ToString("N0"));
        }
        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Adds comment member tags to the axis collections.
        /// </summary>
        /// <param name="axisTuple">View tuple for the axis.</param>
        /// <param name="dimName">Name of the dimension.</param>
        /// <param name="axisMemberTagCommentValues">The comment member tag values for the axis.</param>
        /// <param name="axisCommentMemberTags">Comment member tags for the axis.</param>
        /// <param name="axisDims"></param>
        /// <param name="olapView"></param>
        private void AddCommentMemberTagItems(viewTuple axisTuple, string dimName,
            CachedSelections<string, Intersection, CommentMemberTagData> axisMemberTagCommentValues,
            IDictionary<string, List<string>> axisCommentMemberTags, 
            string[] axisDims, OlapView olapView)
        {

            List<string> value;
            if (axisCommentMemberTags.TryGetValue(dimName, out value))
            {
                Intersection inter = olapView.MemberTag.CreateIntersection(axisDims, axisTuple.memberDefs, value[0]);

                List<CommentMemberTagData> data = axisMemberTagCommentValues.GetCachedSelections(dimName, inter);

                CommentMemberTagData[] newData = CommentMemberTagData.CreateArrayOfCommentMemberTags(value, axisTuple.memberTagCommentNames, axisTuple.memberTagCommentValues);

                foreach (CommentMemberTagData item in newData)
                {
                    if (data == null || !data.Contains(item))
                    {
                        axisMemberTagCommentValues.CacheAddSelections(
                            dimName,
                            inter,
                            item,
                            true);
                    }
                }
            }
        }

        /// <summary>
        /// Copies the MemberTagEditable settings to the Plannable property.
        /// </summary>
        /// <param name="tuple">the view tuple to copy the settings.</param>
        private void CopyMemberTagTupleProtection(viewTuple tuple)
        {
            if(tuple.isMemberTagEditableSpecified)
            {
                tuple.plannableSpecified = true;
                tuple.plannable = tuple.isMemberTagEditable;
            }
            else
            {
                tuple.plannableSpecified = true;
                tuple.plannable = false;
            }
        }

        /// <summary>
        /// Formatting Styles
        /// </summary>
        /// <param name="viewTuple"></param>
        /// <param name="position"></param>
        /// <param name="viewAxis"></param>
        /// <param name="formatRange">Range enum to format.</param>
        private void Styles(viewTuple viewTuple, int position, ViewAxis viewAxis, FormatRange formatRange)
        {
            TupleFormat tupleFormat = new TupleFormat();
            TupleConditionalFormat condTupleFormat = null;
            bool notNullFlag = false;

            if (formatRange == FormatRange.DataCol || formatRange == FormatRange.DataRow)
            {
                if (viewTuple.dataGlobalStyleName != null)
                {
                    tupleFormat.Name = viewTuple.dataGlobalStyleName;
                    notNullFlag = true;
                }
                if (viewTuple.numberFormat != null)
                {
                    tupleFormat.NumberFormat = viewTuple.numberFormat.pattern;
                    notNullFlag = true;
                }
                if (viewTuple.plannableSpecified)
                {
                    tupleFormat.Plannable = viewTuple.plannable.ToString();
                    notNullFlag = true;
                }
                if (viewTuple.dataBorder != null)
                {
                    tupleFormat.Borders = viewTuple.dataBorder.border;
                    notNullFlag = true;
                }
                if (viewTuple.tupleConditionalStyle != null && !String.IsNullOrWhiteSpace(viewTuple.tupleConditionalStyle.styleName))
                {
                    //condTupleFormat = tupleFormat.Clone();
                    condTupleFormat = new TupleConditionalFormat(tupleFormat, viewTuple.tupleConditionalStyle.indexType.ToLevelGenerationType(),  viewTuple.tupleConditionalStyle.styleIndex);
                    condTupleFormat.NumberFormat = String.Empty;
                    condTupleFormat.Name = viewTuple.tupleConditionalStyle.styleName;
                    notNullFlag = true;
                }

            }
            else if (formatRange == FormatRange.Headers)
            {
                if (viewTuple.headerGlobalStyleName != null)
                {
                    tupleFormat.Name = viewTuple.headerGlobalStyleName;
                }

                if (viewTuple.headerBorder != null)
                {
                    tupleFormat.Borders = viewTuple.headerBorder.border;
                }

                tupleFormat.Plannable = "false";
                notNullFlag = true;
            }

            if (!notNullFlag) return;

            
            //Only add memberdefs for a tuple to the dataStyles dictionary if
            //the tupleFormat has a property set.
            Dictionary<TupleFormat, List<Tuple>> dataStyles = GetStyles(formatRange);

            Tuple tuple = new Tuple(position, viewAxis, viewTuple.memberTag, viewTuple.memberDefs);

            if (dataStyles.ContainsKey(tupleFormat))
            {
                dataStyles[tupleFormat].Add(tuple);
            }
            else
            {
                List<Tuple> mbrDefList = new List<Tuple>();
                dataStyles.Add(tupleFormat, mbrDefList);
                dataStyles[tupleFormat].Add(tuple);
            }
            
            if (condTupleFormat != null)
            {
                Dictionary<TupleConditionalFormat, List<Tuple>> condDataStyles = GetConditionalStyles(formatRange);
                List<Tuple> tupleList;
                if (condDataStyles.TryGetValue(condTupleFormat, out tupleList))
                {
                    tupleList.Add(tuple);
                }
                else
                {
                    List<Tuple> mbrDefList = new List<Tuple> {tuple};
                    condDataStyles.Add(condTupleFormat, mbrDefList);

                }
            }
            
        }              

        /// <summary>
        /// Gets a dictionary of styles.
        /// </summary>
        /// <param name="formatRange">Range enum to format.</param>
        /// <returns>A dictionary of styles</returns>
        private Dictionary<TupleFormat, List<Tuple>> GetStyles(FormatRange formatRange)
        {
            Dictionary<TupleFormat, List<Tuple>> styles = null;
            OlapView olapView = PafApp.GetViewMngr().CurrentOlapView;

            if (formatRange == FormatRange.DataCol)
            {
                styles = olapView.DataColStyles;
            }
            else if (formatRange == FormatRange.DataRow)
            {
                styles = olapView.DataRowStyles;
            }
            else if (formatRange == FormatRange.Headers)
            {
                styles = olapView.HeaderStyles;
            }

            return styles;
        }

        /// <summary>
        /// Gets a dictionary of styles.
        /// </summary>
        /// <param name="formatRange">Range enum to format.</param>
        /// <returns>A dictionary of styles</returns>
        private Dictionary<TupleConditionalFormat, List<Tuple>> GetConditionalStyles(FormatRange formatRange)
        {
            Dictionary<TupleConditionalFormat, List<Tuple>> styles = null;
            OlapView olapView = PafApp.GetViewMngr().CurrentOlapView;

            if (formatRange == FormatRange.DataCol)
            {
                styles = olapView.DataColConditionalStyles;
            }
            else if (formatRange == FormatRange.DataRow)
            {
                styles = olapView.DataRowConditionalStyles;
            }
            

            return styles;
        }

        #endregion Private Methods
    }
}
