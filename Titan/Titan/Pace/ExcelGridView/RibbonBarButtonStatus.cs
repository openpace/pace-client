﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using Titan.Pace.Application.Extensions;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.Rules;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Pace.ExcelGridView
{
    public class RibbonBarButtonStatus
    {
        private bool? _lockEnabled = null;
        private readonly Microsoft.Office.Interop.Excel.Range _activeRange;

        /// <summary>
        /// Returns:
        /// true if the range does not contain a replication
        /// false: The range contains a cell that's replicated
        /// null: if the cell or range is protected, read only, etc
        /// </summary>
        private bool? ReplicateEnabled { get; set; }

        /// <summary>
        /// Returns:
        /// true if the range does not contain a lift
        /// false: The range contains a cell that's lift
        /// null: if the cell or range is protected, read only, etc
        /// </summary>
        private bool? LiftEnabled { get; set; }

        /// <summary>
        /// Should the replication buttons be enabled
        /// </summary>
        private bool Replicate { get; set; }

        /// <summary>
        /// SHould the unreplicate buttons be enabled
        /// </summary>
        private bool Unreplicate { get; set; }

        /// <summary>
        /// Should the lift buttons be enabled
        /// </summary>
        private bool Lift { get; set; }

        /// <summary>
        /// Should the unlift buttons be enabled.
        /// </summary>
        private bool Unlift { get; set; }

        /// <summary>
        /// Current pafPlanSessionResponse lift enabled flag
        /// </summary>
        public bool SessionRespLiftEnabled { get; set; }

        /// <summary>
        /// Current pafPlanSessionResponse lift all enabled flag
        /// </summary>
        public bool SessionRespLiftAllEnabled { get; set; }

        /// <summary>
        /// Current pafPlanSessionResponse replication enabled flag
        /// </summary>
        public bool SessionRespReplicateEnabled { get; set; }

        /// <summary>
        /// Current pafPlanSessionResponse replicate all enabled flag
        /// </summary>
        public bool SessionRespReplicateAllEnabled { get; set; }

        /// <summary>
        /// True if the lock button should be enabled
        /// </summary>
        public bool LockEnabled
        {
            get
            {
                if(_lockEnabled == null)
                {
                    _lockEnabled = LockButtonEnabled(_activeRange);
                }
                return _lockEnabled.Value;
            }

        }

        public RibbonBarButtonStatus(pafPlanSessionResponse sessionResponse)
        {
            _activeRange = (Microsoft.Office.Interop.Excel.Range)Globals.ThisWorkbook.Application.Selection;

            if (_activeRange == null || _activeRange.IsLarge())
            {
                ReplicateEnabled = false;
                LiftEnabled = false;
                Replicate = false;
                Unreplicate = false;
                Lift = false;
                Unlift = false;
                _lockEnabled = false;
            }
            else
            {
                bool? replicateEnabled, liftEnabled;
                bool replicate, unreplicate, lift, unlift;

                IsCellAllocated(_activeRange, out replicateEnabled, out liftEnabled);
                GetAllocationButtonStatus(_activeRange, out replicate, out unreplicate, out lift, out unlift);

                ReplicateEnabled = replicateEnabled;
                LiftEnabled = liftEnabled;
                Replicate = replicate;
                Unreplicate = unreplicate;
                Lift = lift;
                Unlift = unlift;
            }

            if (sessionResponse != null)
            {
                SessionRespLiftAllEnabled = sessionResponse.liftAllEnabled;
                SessionRespLiftEnabled = sessionResponse.liftEnabled;
                SessionRespReplicateAllEnabled = sessionResponse.replicateAllEnabled;
                SessionRespReplicateEnabled = sessionResponse.replicateEnabled;

            }
        }


        /// <summary>
        /// True to enable the master (top level) lift button
        /// </summary>
        /// <returns></returns>
        public bool EnableMasterLiftButton()
        {
            return (LiftEnabled != null && LiftEnabled.Value && Lift || Unlift) && (VisibleMasterLiftButton());
        }

        /// <summary>
        /// True to enable the master (top level) replicate button
        /// </summary>
        /// <returns></returns>
        public bool EnableMasterReplicateButton()
        {
            return (ReplicateEnabled != null && ReplicateEnabled.Value && Replicate || Unreplicate) && (VisibleMasterReplicateButton());
        }

        /// <summary>
        /// True to visible the master (top level) lift button
        /// </summary>
        /// <returns></returns>
        public bool VisibleMasterLiftButton()
        {
            return (SessionRespLiftAllEnabled || SessionRespLiftEnabled);
        }

        /// <summary>
        /// True to visible the master (top level) replicate button
        /// </summary>
        /// <returns></returns>
        public bool VisibleMasterReplicateButton()
        {
            return (SessionRespReplicateEnabled || SessionRespReplicateAllEnabled);
        }

        /// <summary>
        /// True if the Lift button meets all the criteria to be enabled.
        /// </summary>
        /// <returns></returns>
        public bool EnableLiftButton()
        {
            return LiftEnabled != null && LiftEnabled.Value && Lift;
        }

        /// <summary>
        /// True if the UnLift button meets all the criteria to be enabled.
        /// </summary>
        /// <returns></returns>
        public bool EnableUnliftButton()
        {
            return Unlift;
        }

        /// <summary>
        /// True if the replicate button meets all the criteria to be enabled.
        /// </summary>
        /// <returns></returns>
        public bool EnableReplicateButton()
        {
            return ReplicateEnabled != null && ReplicateEnabled.Value && Replicate;
        }

         /// <summary>
        /// True if the Unreplicate button meets all the criteria to be enabled.
        /// </summary>
        /// <returns></returns>
        public bool EnableUnreplicateButton()
        {
            return Unreplicate;
        }

        /// <summary>
        /// True if the paste shape should be enabled.
        /// </summary>
        /// <returns></returns>
        public bool PasteShapeEnabled()
        {
            if (PafApp.GetGridApp().InCutCopyMode())
            {
                return true;
            }
            return Clipboard.ContainsData(DataFormats.CommaSeparatedValue) || Clipboard.ContainsData("XML Spreadsheet");
        }

        #region Private

        /// <summary>
        /// Determines if the current cell is replicated or lifted.  
        /// If so then then unreplicate menu is ungreyed.
        /// If the cell if not replicated, then the replication menu is enabled.
        /// </summary>
        private void IsCellAllocated(Microsoft.Office.Interop.Excel.Range activeRange, out bool? replicateEnabled, out bool? liftEnabled)
        {
            replicateEnabled = null;
            liftEnabled = null;
            try
            {
                Stopwatch stopwatch = Stopwatch.StartNew();

                ViewMngr viewMngr = PafApp.GetViewMngr();
                OlapView olapView = viewMngr.CurrentOlapView;
                ProtectionMngr protectionMngr = viewMngr.CurrentProtectionMngr;

                foreach (Cell cell in from Microsoft.Office.Interop.Excel.Range cells in activeRange select new Cell(cells.Row, cells.Column))
                {
                    if (!olapView.DataRange.InContiguousRange(cell) ||
                        viewMngr.CurrentView.ReadOnly)
                    {
                        return;
                    }
                    //get the intersection of the cell.
                    Intersection intersection = olapView.FindIntersection(cell.CellAddress);
                    //make sure we have a valid intersection.
                    if (intersection == null) return;
                    int uniqueId = intersection.UniqueID;
                    //Adjust the cell for alias columns, etc.
                    Cell adjustedCell = olapView.AdjustCellAddress(cell);
                    //the activecell is not a blank
                    if (!olapView.RowBlanks.Contains(adjustedCell.CellAddress.Row) &&
                        !olapView.ColBlanks.Contains(adjustedCell.CellAddress.Col))
                    {
                        bool inGlobal = !(protectionMngr.GlobalLocks == null
                                        || !protectionMngr.GlobalLocks.ContainsIntersection(intersection));


                        bool inProtMngr = (!inGlobal)
                                          && (!protectionMngr.SystemLocks.Contains(uniqueId))
                                          && (!protectionMngr.UserLocks.Contains(intersection))
                                          && (!protectionMngr.ProtectedCells.Contains(uniqueId))
                                          && (!protectionMngr.InvalidReplicationCells.Contains(uniqueId));

                        if (inProtMngr)
                        {
                            if (!replicateEnabled.HasValue || replicateEnabled.Value)
                            {
                                replicateEnabled = !protectionMngr.Replication.IsIntersectionAllocated(intersection);
                            }

                            if (!liftEnabled.HasValue || liftEnabled.Value)
                            {
                                liftEnabled = !protectionMngr.Lift.IsIntersectionAllocated(intersection);
                            }
                            if (!liftEnabled.Value && !replicateEnabled.Value)
                            {
                                return;
                            }
                        }
                        
                    }
                }
                stopwatch.Stop();
                PafApp.GetLogger().InfoFormat("IsCellAllocated  runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
            catch (Exception)
            {
            }
        }

        private void GetAllocationButtonStatus(Microsoft.Office.Interop.Excel.Range range, out bool enableReplicate, out bool enableUnreplicate, out bool enableLift, out bool enableUnlift)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            enableReplicate = false;
            enableUnreplicate = false;
            enableLift = false;
            enableUnlift = false;
           

            List<Cell> cells = (from Microsoft.Office.Interop.Excel.Range c in range select new Cell(c.Row, c.Column)).ToList();
            if (cells.Count > 0)
            {
                GetAllocationButtonStatus(cells, out enableReplicate, out enableUnreplicate, out enableLift, out enableUnlift);
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("GetAllocationButtonStatus  runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Determines if the current cell is replicated.  
        /// If so then then unreplicate menu is ungreyed.
        /// If the cell if not replicated, then the replication menu is enabled.
        /// </summary>
        /// <param name="cells">Currently active cell.</param>
        /// <param name="enableReplicate"></param>
        /// <param name="enableUnreplicate"></param>
        /// <param name="enableLift"></param>
        /// <param name="enableUnLift"></param>
        private void GetAllocationButtonStatus(ICollection<Cell> cells, out bool enableReplicate, out bool enableUnreplicate, out bool enableLift, out bool enableUnLift)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            ViewMngr viewMngr = PafApp.GetViewMngr();
            OlapView olapView = viewMngr.CurrentOlapView;
            ProtectionMngr protectionMngr = viewMngr.CurrentProtectionMngr;

            List<AllocationSpec> lifts = new List<AllocationSpec>(10);
            List<AllocationSpec> replicates = new List<AllocationSpec>(10);
            int cellCount = cells.Count;
            try
            {
                foreach (Cell cell in cells)
                {
                    AllocationSpec lift = new AllocationSpec(cell.ToString());
                    AllocationSpec rep = new AllocationSpec(cell.ToString());

                    if (!olapView.DataRange.InContiguousRange(cell) || viewMngr.CurrentView.ReadOnly)
                    {
                        lifts.Add(lift);
                        replicates.Add(rep);
                        return;
                    }
                    //get the intersection of the cell.
                    Intersection intersection = olapView.FindIntersection(cell.CellAddress);
                    //make sure we have a valid intersection.
                    if (intersection == null)
                    {
                        lifts.Add(lift);
                        replicates.Add(rep);
                        return;
                    }
                    int uniqueId = intersection.UniqueID;
                    //the activecell is not a blank
                    Cell adjustedCell = olapView.AdjustCellAddress(cell);

                    if (!olapView.RowBlanks.Contains(adjustedCell.CellAddress.Row) &&
                        !olapView.ColBlanks.Contains(adjustedCell.CellAddress.Col))
                    {
                        bool inGlobal = !(protectionMngr.GlobalLocks == null ||
                                          !protectionMngr.GlobalLocks.ContainsIntersection(intersection));


                        bool inProtMngr = (!inGlobal) &&
                                          (!protectionMngr.SystemLocks.Contains(uniqueId)) &&
                                          (!protectionMngr.UserLocks.Contains(intersection)) &&
                                          (!protectionMngr.ProtectedCells.Contains(uniqueId)) &&
                                          (!protectionMngr.InvalidReplicationCells.Contains(uniqueId));

                        if (!inProtMngr) break;

                        bool replicateEnabled = !protectionMngr.Replication.AllocateAllCells.Contains(intersection)
                                                && !protectionMngr.Replication.AllocateExistingCells.Contains(intersection);
                        rep.Allocate = replicateEnabled;
                        rep.Unallocate = !replicateEnabled;

                        if (rep.Allocate)
                        {
                            bool liftEnabled = !protectionMngr.Lift.AllocateAllCells.Contains(intersection) &&
                                               !protectionMngr.Lift.AllocateExistingCells.Contains(intersection);
                            lift.Allocate = liftEnabled;
                            lift.Unallocate = !liftEnabled;
                            if (lift.Unallocate)
                            {
                                rep.Allocate = false;
                                rep.Unallocate = false;
                            }
                        }

                    }
                    lifts.Add(lift);
                    replicates.Add(rep);
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                enableReplicate = false;
                enableUnreplicate = false;
                enableLift = false;
                enableUnLift = false;
                if (cellCount == lifts.Count)
                {
                    var r = lifts.Where(a => a.Allocate).ToList();
                    enableLift = r.Count == cellCount;
                    var u = lifts.Where(a => a.Unallocate).ToList();
                    enableUnLift = u.Count == cellCount;
                }

                if (cellCount == replicates.Count)
                {
                    var r = replicates.Where(a => a.Allocate).ToList();
                    enableReplicate = r.Count == cellCount;
                    var u = replicates.Where(a => a.Unallocate).ToList();
                    enableUnreplicate = u.Count == cellCount;
                }

            }
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("GetAllocationButtonStatus - runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Determines if Lock/unlock buttons should be enabled/disabled depending on the current cell.
        /// </summary>
        private bool LockButtonEnabled(Microsoft.Office.Interop.Excel.Range activeRange)
        {
            ViewMngr viewMngr = PafApp.GetViewMngr();
            ViewStateInfo currentView = PafApp.GetViewMngr().CurrentView;
            OlapView olapView = viewMngr.CurrentOlapView;
            ProtectionMngr protectionMngr = viewMngr.CurrentProtectionMngr;
            try
            {
                if (PafApp.GetGridApp().InCutCopyMode()) return false;
                foreach (Cell cell in from Microsoft.Office.Interop.Excel.Range cells in activeRange select new Cell(cells.Row, cells.Column))
                {
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug(cell.CellAddress);
                    //If the sheet is not plannable(read only) then don't show the buttons Fix (TTN-313).
                    //If the view does not have a data range, disable the buttons fix (TTN-318)
                    //or if the Cell is not in the data range, or the View is read only
                    if (!currentView.Plannable || olapView.StartCell == null || !olapView.DataRange.InContiguousRange(cell) || currentView.ReadOnly)
                    {
                        return false;
                    }
                    //if the user right clicks in the data range on the view
                    if (!olapView.DataRange.InContiguousRange(cell) || currentView.ReadOnly)
                    {
                        return false;
                    }
                    Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell.CellAddress);

                    Cell adjustedCell = olapView.AdjustCellAddress(cell);

                    if (olapView.RowBlanks.Contains(adjustedCell.CellAddress.Row) ||
                        olapView.ColBlanks.Contains(adjustedCell.CellAddress.Col))
                    {
                        return false;
                    }

                    bool inGlobal = !(protectionMngr.GlobalLocks == null || !protectionMngr.GlobalLocks.ContainsIntersection(intersection));

                    //the activecell is not locked, protected, or read-only
                    if ((inGlobal) ||
                        (protectionMngr.UserLocks.Contains(intersection)) ||
                        (protectionMngr.SystemLocks.Contains(intersection.UniqueID)) ||
                        (protectionMngr.ProtectedCells.Contains(intersection.UniqueID)))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        #endregion Private
    }
}
