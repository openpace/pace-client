﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Application.Forms;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;
using Titan.Properties;
using Office = Microsoft.Office.Core;

// Follow these steps to enable the Ribbon (XML) item:

// 1: Copy the following code block into the ThisAddin, ThisWorkbook, or ThisDocument class.

//  protected override Microsoft.Office.Core.IRibbonExtensibility CreateRibbonExtensibilityObject()
//  {
//      return new PaceRibbon();
//  }

// 2. Create callback methods in the "Ribbon Callbacks" region of this class to handle user
//    actions, such as clicking a button. Note: if you have exported this Ribbon from the Ribbon designer,
//    move your code from the event handlers to the callback methods and modify the code to work with the
//    Ribbon extensibility (RibbonX) programming model.

// 3. Assign attributes to the control tags in the Ribbon XML file to identify the appropriate callback methods in your code.  

// For more information, see the Ribbon XML documentation in the Visual Studio Tools for Office Help.


namespace Titan.Pace.ExcelGridView
{
    [ComVisible(true)]
    public class PaceRibbon : Office.IRibbonExtensibility
    {
        private const string PaceTabKey = "TabPace";
        private const string Excel2007RibbonUiNameSpace = @"http://schemas.microsoft.com/office/2006/01/customui";
        private const string Excel2010RibbonUiNameSpace = @"http://schemas.microsoft.com/office/2009/07/customui";

        private Office.IRibbonUI _ribbon;
        private int _ruleSetIndex;
        private RibbonBarButtonStatus _allocationStatus;
        public const bool RibbonVisible = true;
        public string ExcelRibbonUiNamespace { get; set; }
        public int ExcelVersion { get; set; }
        public bool Excel2007 { get; set; }
        public RibbonBarButtonStatus ButtonStatus
        {
            get
            {
                if (_allocationStatus == null)
                {
                    BuildUserAllocationStatus();
                }
                return _allocationStatus;
            }
        }

        /// <summary>
        /// Returns true if the RibbonBar is visible.
        /// </summary>
        public bool Visible
        {
            get { return RibbonVisible; }
        }

        /// <summary>
        /// Current ruleset.
        /// </summary>
        public bool RuleSetSet { get; set; }

        internal event Events.ViewSectionGroupChangedHandler ViewSectionGroupChanged;

        #region IRibbonExtensibility Members

        public string GetCustomUI(string ribbonId)
        {
            ExcelVersion = Convert.ToInt32(PafApp.GetGridApp().VersionAsFloat);
            if (ExcelVersion == 12)
            {
                ExcelRibbonUiNamespace = Excel2007RibbonUiNameSpace;
                Excel2007 = true;
            }
            else
            {
                ExcelRibbonUiNamespace = Excel2010RibbonUiNameSpace;
                Excel2007 = false;
            }

            return
                GetResourceText(Excel2007
                    ? "Titan.Pace.ExcelGridView.PaceRibbon-2007.xml"
                    : "Titan.Pace.ExcelGridView.PaceRibbon.xml");
        }

        #endregion

        #region Ribbon Callbacks

        //Create callback methods here. For more information about adding callback methods, select the Ribbon XML item in Solution Explorer and then press F1

        public void Ribbon_Load(Office.IRibbonUI ribbonUi)
        {
            _ribbon = ribbonUi;
            Invalidate();
            SelectOurTab();
        }

        /// <summary>
        /// Callback to set enabled status of Excel buttons.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool GetEnableLinks(Office.IRibbonControl control)
        {
            if (control == null) return false;

            Microsoft.Office.Interop.Excel.Range rng =
                (Microsoft.Office.Interop.Excel.Range) Globals.ThisWorkbook.Application.Selection;
            Cell targetCell = new Cell(rng.Row, rng.Column);

            Worksheet sheet = (Worksheet) Globals.ThisWorkbook.ActiveSheet;
            bool ourSheet = false;
            if (sheet != null)
            {
                if (PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    ourSheet = PafApp.GetCommandBarMngr().EnableHyperlinkButtonRibbon(targetCell);
                }
                else
                {
                    ourSheet = true;
                }
            }
            return ourSheet;
        }

        /// <summary>
        /// Callback to set enabled status of Excel buttons.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool GetEnableCellNote(Office.IRibbonControl control)
        {
            if (control == null) return false;

            Microsoft.Office.Interop.Excel.Range rng =
                (Microsoft.Office.Interop.Excel.Range) Globals.ThisWorkbook.Application.Selection;
            Cell targetCell = new Cell(rng.Row, rng.Column);

            Worksheet sheet = (Worksheet) Globals.ThisWorkbook.ActiveSheet;
            bool ourSheet = false;
            if (sheet != null)
            {
                if (PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    ourSheet = PafApp.GetCommandBarMngr().EnableCellNotesButtonsRibbon(targetCell);
                }
                else
                {
                    ourSheet = true;
                }
            }
            return ourSheet;
        }

        /// <summary>
        /// Enables the custom class plan menu.
        /// </summary>
        /// <param name="viewName">Name of the current view.</param>
        /// <param name="menuName">Menu item to validate</param>
        public bool GetCustomCommandMenusEnabled(string viewName, string menuName)
        {
            if (PafApp.GetPafPlanSessionResponse() != null && PafApp.GetPafPlanSessionResponse().customMenuDefs != null)
            {
                customMenuDef[] menuDefs = PafApp.GetPafPlanSessionResponse().customMenuDefs;
                foreach (customMenuDef menuDef in menuDefs.Where(menuDef => menuDef.key.Equals(menuName)))
                {
                    //If View Filters exist they enable/disable custom menus based on the filters
                    if (menuDef.viewFilter != null && menuDef.viewFilter[0] != null)
                    {
                        return menuDef.viewFilter.Any(view => view.Trim() == viewName.Trim());
                    }
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Callback to set enabled status of Excel buttons.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool GetEnable(Office.IRibbonControl control)
        {
            if (control == null) return false;
            Worksheet sheet = (Worksheet) Globals.ThisWorkbook.ActiveSheet;
            bool ourSheet = true;
            if (sheet != null)
            {
                ourSheet =
                    !PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);
            }
            return ourSheet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool IsRoleBuilt(Office.IRibbonControl control)
        {
            return PafApp.RoleBuilt;
        }

        //public void ApplyProtections(Office.IRibbonControl control)
        //{
        //    Worksheet sheet = (Worksheet) Globals.ThisWorkbook.Application.ActiveWorkbook.ActiveSheet;
        //    Microsoft.Office.Interop.Excel.Range range = (Microsoft.Office.Interop.Excel.Range)Globals.ThisWorkbook.Application.Selection;
        //    ColorScale v = (ColorScale)range.FormatConditions.AddColorScale(3);
        //}


        public bool ClusteringVisible(Office.IRibbonControl control)
        {
            try
            {
                return PafApp.ClusteringEnabled;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Callback to set the buttons visible status
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool ButtonVisible(Office.IRibbonControl control)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            var ret = ButtonVisiblePrivate(control, ButtonStatus);

            stopwatch.Stop();
            //PafApp.GetLogger().InfoFormat("ButtonVisible - Button: {0}, Value: {1},  runtime: {2} (ms)", control.Id, ret, stopwatch.ElapsedMilliseconds.ToString("N0"));

            return ret;
        }

        /// <summary>
        /// Callback to set the buttons visible status
        /// </summary>
        /// <param name="control"></param>
        /// <param name="allocationStatus"></param>
        /// <returns></returns>
        private bool ButtonVisiblePrivate(Office.IRibbonControl control, RibbonBarButtonStatus allocationStatus)
        {
            try
            {
                Worksheet sheet = (Worksheet)Globals.ThisWorkbook.ActiveSheet;
                bool isPaceSheet = PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);

                if (sheet == null) return false;
                switch (control.Id)
                {
                    case "msoRoleFilter":
                        return PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig != null && PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig.isUserFilteredUow;
                    case "msoSplitButtonTestHarness":
                        return PafApp.IsTestingUser;
                    case "msoChangePassword":
                        return !PafApp.IsLdapUser && PafApp.ClientPasswordResetEnabled;
                    case "msoSessionLock":
                    case "msoSessionLockRight":
                        return PafApp.GetPafAppSettingsHelper().GetGlobalSessionLockEnabled();
                    case "msoLoadTest":
                    case "msoRecordTest":
                        return !PafApp.GetTestHarnessManager().IsRecording;
                    case "msoStopTest":
                        return PafApp.GetTestHarnessManager().IsRecording;

                    case "msoReplicateSplit":
                    case "msoReplicateMenu":
                    case "msoReplicate":
                    case "msoRemoveReplication":
                    case "groupReplicate":
                        return allocationStatus.VisibleMasterReplicateButton();
                    case "msoReplicateExisting":
                        return allocationStatus.SessionRespReplicateEnabled;
                    case "msoReplicateAll":
                        return allocationStatus.SessionRespReplicateAllEnabled;

                    case "msoLiftSplit":
                    case "msoLiftMenu":
                    case "msoLift":
                    case "msoRemoveLift":
                    case "groupLift":
                        return allocationStatus.VisibleMasterLiftButton();
                    case "msoLiftExisting":
                        return allocationStatus.SessionRespLiftEnabled;
                    case "msoLiftAll":
                        return allocationStatus.SessionRespLiftAllEnabled;
                    case "groupViewOptions":
                        if (!isPaceSheet) return false;
                        var viewTree = PafApp.GetViewTreeView();
                        if (viewTree == null) return false;
                        var view = PafApp.GetViewMngr().CurrentView;
                        if (view == null) return false;
                        GroupingSpec row = viewTree.GetViewGroupingSpec(ViewAxis.Row, view.ViewName);
                        GroupingSpec col = viewTree.GetViewGroupingSpec(ViewAxis.Col, view.ViewName);
                        return row != null || col != null;
                    case "separatorGrouCell1":
                        return (allocationStatus.VisibleMasterReplicateButton()) && (allocationStatus.VisibleMasterLiftButton()) && PafApp.GetPafAppSettingsHelper().GetGlobalSessionLockEnabled();
                }
                return false;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return false;
            }
        }

        /// <summary>
        /// Callback to set the custom menu buttons enabled status
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool CustomMenuEnabled(Office.IRibbonControl control)
        {
            try
            {
                string viewName = PafApp.GetViewMngr().CurrentView.ViewName;
                return GetCustomCommandMenusEnabled(viewName, control.Id);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Callback to set the buttons enabled status
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool ButtonEnabled(Office.IRibbonControl control)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            bool ret = false;

            ret = ButtonEnabledPrivate(control, ButtonStatus);

            stopwatch.Stop();
            //PafApp.GetLogger().InfoFormat("ButtonEnable - Button: {0}, Value: {1},  runtime: {2} (ms)", control.Id, ret, stopwatch.ElapsedMilliseconds.ToString("N0"));
            return ret;
        }

        /// <summary>
        /// Callback to set the buttons enabled status
        /// </summary>
        /// <param name="control"></param>
        /// <param name="allocationStatus"></param>
        /// <returns></returns>
        private bool ButtonEnabledPrivate(Office.IRibbonControl control, RibbonBarButtonStatus allocationStatus)
        {
            try
            {
                Worksheet sheet = (Worksheet)Globals.ThisWorkbook.ActiveSheet;
                bool isPaceSheet = PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);
                bool calculationPending = PafApp.AreCalculationsPending;
                if (sheet == null) return false;
                var viewTree = PafApp.GetViewTreeView();
                var view = PafApp.GetViewMngr().CurrentView;
                pafPlanSessionResponse planSessionResponse = PafApp.GetPafPlanSessionResponse();
                switch (control.Id)
                {
                    case "msoLogin":
                        return true;
                    case "msoRole":
                    case "msoRefresh":
                        return PafApp.GetPafAuthResponse() != null;
                    case "msoRoleFilter":
                        return !PafApp.GetGridApp().RoleSelector.UserSelectedRole.Equals(String.Empty) && !PafApp.GetGridApp().RoleSelector.UserPlanTypeSpecString.Equals(String.Empty);
                    case "msoUndoChanges":
                    case "msoSessionLock":
                    case "msoSessionLockRight":
                        return isPaceSheet;
                    case "msoUndo":
                        return PafApp.UndoAvailable;
                    case "msoSort":
                    case "msoRuleset":
                        return (isPaceSheet && !calculationPending);
                    case "msoCustomAction":
                        if (view == null) return false;
                        if (!view.ViewHasBeenBuilt) return false;
                        if (!isPaceSheet || calculationPending)
                        {
                            return false;
                        }
                        return planSessionResponse != null && planSessionResponse.customMenuDefs != null &&
                               planSessionResponse.customMenuDefs.Length > 0;
                    case "msoCalculate":
                        return calculationPending;
                    case "msoSaveData":
                        if (calculationPending)
                        {
                            return false;
                        }
                        return PafApp.GetSaveWorkMngr().DataWaitingToBeSaved || PafApp.GetCellNoteMngr().IsClientCacheDirty() || PafApp.GetMbrTagMngr().IsMbrTagCacheDirty();
                    case "msoLockCell":
                        return allocationStatus.LockEnabled;
                    case "msoOptions":
                        return true;
                    case "msoHelp":
                        return true;
                    case "msoAbout":
                        return true;
                    case "msoPasteWithoutProtection":
                        return allocationStatus.PasteShapeEnabled();
                    case "msoSplitButtonTestHarness":
                        return planSessionResponse != null;
                    case "msoAssortment":
                        return false;
                    case "msoLoadTest":
                    case "msoRecordTest":
                        return !PafApp.GetTestHarnessManager().IsRecording;
                    case "msoStopTest":
                        return PafApp.GetTestHarnessManager().IsRecording;
                    case "msoChangePassword":
                        return !PafApp.GetGridApp().LogonInformation.DomainUser;
                    case "msoLiftExisting":
                    case "msoLiftAll":
                        return allocationStatus.EnableLiftButton();
                    case "msoLiftSplit":
                    case "msoLift":
                        return allocationStatus.EnableMasterLiftButton();
                    case "msoRemoveLift":
                        return allocationStatus.EnableUnliftButton();
                    case "msoReplicateExisting":
                    case "msoReplicateAll":
                        return allocationStatus.EnableReplicateButton();
                    case "msoReplicateSplit":
                    case "msoReplicate":
                        return allocationStatus.EnableMasterReplicateButton();
                    case "msoRemoveReplication":
                        return allocationStatus.EnableUnreplicateButton();
                    case "msoColumnGroups":
                        if (!isPaceSheet) return false;
                        if (viewTree == null) return false;
                        if (view == null) return false;
                        return viewTree.GetViewGroupingSpec(ViewAxis.Col, view.ViewName) != null;
                    case "msoRowGroups":
                        if (!isPaceSheet) return false;
                        if (viewTree == null) return false;
                        if (view == null) return false;
                        return viewTree.GetViewGroupingSpec(ViewAxis.Row, view.ViewName) != null;

                }
                return false;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return false;
            }
        }

        
        /// <summary>
        /// Creats the CustomAction menu.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public string GetCustomActionMenuContent(Office.IRibbonControl control)
        {
            StringBuilder menuXml = new StringBuilder();
            menuXml.Append("<menu xmlns=\"").Append(ExcelRibbonUiNamespace).Append("\">");
            List<string> groupsProcessed = new List<string>(5);

            if (PafApp.GetPafPlanSessionResponse() != null && PafApp.GetPafPlanSessionResponse().customMenuDefs != null)
            {

                //process the menu items in groups
                customMenuDef[] customMenus = PafApp.GetPafPlanSessionResponse().customMenuDefs;
                foreach (var customMenu in customMenus)
                {
                    if (!String.IsNullOrWhiteSpace(customMenu.subMenuName))
                    {
                        if (groupsProcessed.Contains(customMenu.subMenuName)) continue;
                        var groups = customMenus.Where(x => x.subMenuName == customMenu.subMenuName).ToList();

                        if (groups.Count > 0)
                        {
                            menuXml.Append(groups.ToRibbonButtons(groups[0].subMenuName, true));
                        }
                        groupsProcessed.Add(customMenu.subMenuName);
                    }
                    else
                    {
                        menuXml.Append(customMenu.ToRibbonButton());
                    }
                }
            }
            menuXml.Append(@"</menu>");

            return menuXml.ToString();
        }

        /// <summary>
        /// Sets the label of our Ribbon Bar tab.
        /// All this does is UCASE the name for Office 2013
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public string GetAddInRibbonLabel(Office.IRibbonControl control)
        {
            string text = GetLocalizationResourceString("Application.Excel.Menu.Name");
            try
            {
                return ExcelVersion > 14 ? text.ToUpper() : text;
            }
            catch (Exception)
            {
                return "Pace";
            }
        }

        /// <summary>
        /// Sets the visible status of the TabAddIns (old commandbars) or TabPace (new ribbon bar)
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool GetTabVisible(Office.IRibbonControl control)
        {
            switch (control.Id)
            {
                case PaceTabKey:
                    return Visible;
                    //return Settings.Default.UseRibbonBar;
                case "TabAddIns":
                    return true;
                    //return !Settings.Default.UseRibbonBar;
            }
            return true;
        }

        /// <summary>
        /// Return the image size for a control
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public Office.RibbonControlSize GetImageSize(Office.IRibbonControl control)
        {
            try
            {
                switch (control.Id)
                {
                    case "msoSplitButtonHelp":
                        if (!PafApp.IsTestingUser && PafApp.IsLdapUser)
                        {
                            return Office.RibbonControlSize.RibbonControlSizeRegular; 
                        }
                        return Office.RibbonControlSize.RibbonControlSizeLarge;
                    case "msoPasteWithoutProtection":
                        return Office.RibbonControlSize.RibbonControlSizeRegular;
                        //return !PafApp.GetPafAppSettingsHelper().GetGlobalSessionLockEnabled() ? Office.RibbonControlSize.RibbonControlSizeLarge : Office.RibbonControlSize.RibbonControlSizeRegular;
                    case "msoUndo":
                        return Office.RibbonControlSize.RibbonControlSizeLarge;
                }
                return Office.RibbonControlSize.RibbonControlSizeRegular;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return Office.RibbonControlSize.RibbonControlSizeRegular;
            }
        }

        /// <summary>
        /// Callback to set the buttons enabled status
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public string GetLabel(Office.IRibbonControl control)
        {
            return GetLabelPrivate(control, ButtonStatus);
        }

        /// <summary>
        /// Private method to set labels.  
        /// GetLabel is directly called by Excel.  
        /// Use that method to set any globals, etc.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="allocationStatus"></param>
        /// <returns></returns>
        private string GetLabelPrivate(Office.IRibbonControl control, RibbonBarButtonStatus allocationStatus)
        {
            try
            {
                switch (control.Id)
                {
                    case "msoLogin":
                        return GetLocalizationResourceString("Application.Excel.Menu.Login");
                    case "msoRole":
                        return GetLocalizationResourceString("Application.Excel.Menu.SelectRole");
                    case "msoRefresh":
                        return GetLocalizationResourceString("Application.Excel.Menu.RefreshView");
                    case "msoRoleFilter":
                        return GetLocalizationResourceString("Application.Excel.Menu.RoleFilter");
                    case "msoUndoChanges":
                        return GetLocalizationResourceString("Application.Excel.Menu.UndoChanges");
                    case "msoUndo":
                        return GetLocalizationResourceString("Application.Excel.Menu.Undo");
                    case "msoSessionLock":
                    case "msoSessionLockRight":
                        return GetLocalizationResourceString("Application.Excel.Menu.SessionLock");
                    case "msoSort":
                        return GetLocalizationResourceString("Application.Excel.Menu.Sort");
                    case "msoCalculate":
                        return GetLocalizationResourceString("Application.Excel.Menu.Calculate");
                    case "msoPasteWithoutProtection":
                        return GetLocalizationResourceString("Application.Excel.Menu.PasteShape");
                    case "msoRuleset":
                        return GetLocalizationResourceString("Application.Excel.Menu.ChangeRuleSet");
                    case "msoCustomAction":
                        return GetLocalizationResourceString("Application.Excel.Menu.CustomAction");
                    case "msoSaveData":
                        return GetLocalizationResourceString("Application.Excel.Menu.SaveChanges");
                    case "msoLockCell":
                    case "msoLockCellRight":
                        return GetLocalizationResourceString("Application.Excel.Menu.LockCell");
                    case "msoOptions":
                        return GetLocalizationResourceString("Application.Excel.Menu.Options");
                    case "msoHelp":
                        return GetLocalizationResourceString("Application.Excel.Menu.Help");
                    case "msoAbout":
                        return GetLocalizationResourceString("Application.Excel.Menu.About");
                    case "msoSplitButtonTestHarness":
                        return GetLocalizationResourceString("Application.Excel.Menu.Help");
                    case "msoAssortment":
                        return GetLocalizationResourceString("Application.Excel.Menu.CreateAssortment");
                    case "msoLoadTest":
                        return GetLocalizationResourceString("Application.Excel.Menu.LoadTest");
                    case "msoRecordTest":
                        return GetLocalizationResourceString("Application.Excel.Menu.RecordStart");
                    case "msoStopTest":
                        return GetLocalizationResourceString("Application.Excel.Menu.RecordStop");
                    case "msoChangePassword":
                        return GetLocalizationResourceString("Application.Excel.Menu.ChangePassword");
                    case "msoSplitButtonTestHarnessButton":
                        return GetLocalizationResourceString("Application.Excel.Menu.TestHarness");
                    case "groupUser":
                        return GetLocalizationResourceString("Application.Excel.Menu.Group.User");
                    case "groupData":
                        return GetLocalizationResourceString("Application.Excel.Menu.Group.Data");
                    case "groupView":
                        return GetLocalizationResourceString("Application.Excel.Menu.Group.View");
                    case "groupCell":
                        return GetLocalizationResourceString("Application.Excel.Menu.Group.Cell");
                    case "groupGeneral":
                        return GetLocalizationResourceString("Application.Excel.Menu.Group.General");
                    case "msoDocumentActions":
                        return GetLocalizationResourceString("Application.Excel.Menu.TaskPane");
                    case "msoReplicateSplit":
                        if (allocationStatus.EnableUnreplicateButton())
                        {
                            return GetLocalizationResourceString("Application.Excel.Menu.RemoveReplicate");
                        }
                        else
                        {
                            //perform the action determined by global settings - default to all but if that isn't enabled
                            //replicate existing TTN-2384
                            return GetLocalizationResourceString(PafApp.GetPafPlanSessionResponse().replicateAllEnabled ? "Application.Excel.Menu.ReplicateAll" : "Application.Excel.Menu.ReplicateExisting");
                        }
                    case "msoReplicateExisting":
                    case "msoLiftExisting":
                        return GetLocalizationResourceString("Application.Excel.Menu.Existing");
                    case "msoReplicateAll":
                    case "msoLiftAll":
                        return GetLocalizationResourceString("Application.Excel.Menu.All");
                    case "msoRemoveReplication":
                    case "msoRemoveLift":
                        return GetLocalizationResourceString("Application.Excel.Menu.Remove");
                    case "msoLiftSplit":
                        if (allocationStatus.EnableUnliftButton())
                        {
                            return GetLocalizationResourceString("Application.Excel.Menu.RemoveLift");
                        }
                        else
                        {
                            //perform the action determined by global settings - default to all but if that isn't enabled
                            //lift existing TTN-2384
                            return GetLocalizationResourceString(PafApp.GetPafPlanSessionResponse().liftAllEnabled ? "Application.Excel.Menu.LiftAll" : "Application.Excel.Menu.LiftExisting");
                        }
                    case "msoColumnGroups":
                        return GetLocalizationResourceString("Application.Excel.Menu.ColumnGroups");
                    case "msoRowGroups":
                        return GetLocalizationResourceString("Application.Excel.Menu.RowGroups");
                    case "groupViewOptions":
                        return GetLocalizationResourceString("Application.Excel.Menu.Group.ViewOptions");
                    case "msoViewGroupings":
                        return GetLocalizationResourceString("Application.Excel.Menu.ViewGroupLabel");
                    case "groupReplicate":
                        return GetLocalizationResourceString("Application.Excel.Menu.Replication");
                    case "groupLift":
                        return GetLocalizationResourceString("Application.Excel.Menu.Lift");
                }
                return String.Empty;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return String.Empty;
            }
        }

        /// <summary>
        /// Callback to set the Screentip (bold text on top of tooltip)
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public string GetScreenTip(Office.IRibbonControl control)
        {
            try
            {
                switch (control.Id)
                {
                    case "msoSaveData":
                        return GetLocalizationResourceString("Application.Excel.Menu.ScreenTip.SaveData");
                    case "msoRefresh":
                        return GetLocalizationResourceString("Application.Excel.Menu.Screentip.Refresh");
                    case "msoUndoChanges":
                        return GetLocalizationResourceString("Application.Excel.Menu.Screentip.UndoChanges");
                    case "msoUndo":
                        return GetLocalizationResourceString("Application.Excel.Menu.Screentip.Undo");
                    case "msoPasteWithoutProtection":
                        return GetLocalizationResourceString("Application.Excel.Menu.Screentip.PasteShape");
                    case "msoDocumentActions":
                        return GetLocalizationResourceString("Application.Excel.Menu.ShowHideTaskPane");
                    case "msoColumnGroups":
                        return GetLocalizationResourceString("Application.Excel.Menu.Screentip.RowGroups");
                    case "msoRowGroups":
                        return GetLocalizationResourceString("Application.Excel.Menu.Screentip.ColumnGroups");

                }
                return String.Empty;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return String.Empty;
            }
        }


        /// <summary>
        /// Callback to set the Supertip Text
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public string GetSupertip(Office.IRibbonControl control)
        {
            try
            {
                switch (control.Id)
                {
                    case "msoLogin":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Login");
                    case "msoRole":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Role");
                    case "msoRefresh":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Refresh");
                    case "msoRoleFilter":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.RoleFilter");
                    case "msoUndoChanges":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.UndoChanges");
                    case "msoUndo":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Undo");
                    case "msoSessionLock":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.SessionLock");
                    case "msoSort":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Sort");
                    case "msoCalculate":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Calculate");
                    case "msoPasteWithoutProtection":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.PasteShape");
                    case "msoRuleset":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Ruleset");
                    case "msoCustomAction":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.CustomAction");
                    case "msoSaveData":
                        return GetLocalizationResourceString(PafApp.AreCalculationsPending ? "Application.Excel.Menu.Tooltip.SaveDataCalculationsPending" : "Application.Excel.Menu.Tooltip.SaveData");
                    case "msoLockCell":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Lockcell");
                    case "msoOptions":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Options");
                    case "msoHelp":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Help");
                    case "msoAbout":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.About");
                    case "msoSplitButtonTestHarnessButton":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Testharness");
                    case "msoAssortment":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Assortment");
                    case "msoLoadTest":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.LoadTest");
                    case "msoRecordTest":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.RecordTest");
                    case "msoStopTest":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.StopTest");
                    case "msoChangePassword":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.Password");
                    case "msoDocumentActions":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.ShowHideTaskPane");
                    case "msoColumnGroups":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.ColumnGroups");
                    case "msoRowGroups":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.RowGroups");
                    case "msoLiftExisting":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.LiftExisting");
                    case "msoLiftAll":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.LiftAll");
                    case "msoRemoveLift":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.RemoveLift");
                    case "msoReplicateExisting":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.ReplicateExisting");
                    case "msoReplicateAll":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.ReplicateAll");
                    case "msoRemoveReplication":
                        return GetLocalizationResourceString("Application.Excel.Menu.Tooltip.RemoveReplicate");
                }
                return String.Empty;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return String.Empty;
            }
        }

        /// <summary>
        /// Gets the string value of the currently selected RuleSet
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public string GetRuleSetSelectedIemId(Office.IRibbonControl control)
        {
            if (!RuleSetSet)
            {
                RuleSetSet = true;
                int i = 0;
                foreach (string item in PafApp.GetPafPlanSessionResponse().ruleSetList)
                {
                    if (item.EqualsIgnoreCase(PafApp.GetPafPlanSessionResponse().defaultRuleSetName))
                    {
                        _ruleSetIndex = i;
                        break;
                    }
                    i++;
                }
            }

            return PafApp.GetPafPlanSessionResponse().ruleSetList[_ruleSetIndex];
 
        }

        /// <summary>
        /// Gets the count of the rulesets
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public int GetRuleSetCount(Office.IRibbonControl control)
        {
            if (PafApp.GetPafPlanSessionResponse() == null || PafApp.GetPafPlanSessionResponse().ruleSetList == null || PafApp.GetPafPlanSessionResponse().ruleSetList.Length == 0)
            {
                return 0;
            }
            return PafApp.GetPafPlanSessionResponse().ruleSetList.Length;
        }

        /// <summary>
        /// Gets the string label of the ruleset
        /// </summary>
        /// <param name="control"></param>
        /// <param name="itemIndex"></param>
        /// <returns></returns>
        public string GetRuleSetLabel(Office.IRibbonControl control, int itemIndex)
        {
            
            return PafApp.GetPafPlanSessionResponse().ruleSetList[itemIndex];
        }

        /// <summary>
        /// Callback when an item is clicked in a Gallery or DropDown
        /// </summary>
        /// <param name="control"></param>
        /// <param name="text"></param>
        /// <param name="index"></param>
        public void GalleryClick(Office.IRibbonControl control, string text, int index)
        {
            switch (control.Id)
            {
                case "msoRuleset":
                    _ruleSetIndex = index;
                    PafApp.GetEventManager().DropDownEventHandler("m_ChangeRuleSet", text);
                    break;
            }
        }

        /// <summary>
        /// Callback when a custom action item is clicked.
        /// </summary>
        /// <param name="control"></param>
        public void CustomMenuItemClick(Office.IRibbonControl control)
        {
            PafApp.GetEventManager().CommandBarEventHandler("m_Custom", control.Id);
        }

        /// <summary>
        /// Callback for a control click
        /// </summary>
        /// <param name="control"></param>
        /// <param name="cancelDefault"></param>
        public void ExcelButtonCallback(Office.IRibbonControl control, ref bool cancelDefault)
        {
            try
            {
                switch (control.Id)
                {
                    case "Copy":
                        cancelDefault = false;
                        InvalidateAll();
                        break;
                    case "FilePrint":
                        cancelDefault = false;
                        bool cancel = false;
                        PafApp.GetEventManager().ThisWorkbook_BeforePrint(ref cancel);
                        break;
                    case "SortDialog":
                        PafApp.GetGridApp().SaveTaskPaneCurrentStatus();
                        frmSort sort = new frmSort();
                        sort.ShowDialogifDataRangeSelected();
                        break;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Warn(ex);
                throw;
            }
            cancelDefault = false;
            InvalidateAll();
        }

        /// <summary>
        /// Gets the state of a toggle button.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public bool ButtonState(Office.IRibbonControl control)
        {
            try
            {
                switch (control.Id)
                {
                    case "msoDocumentActions":
                        return PafApp.GetEventManager().TaskPaneCommandBar.Visible;
                }
                return false;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Warn(ex);
                return false;
            }
        }

        /// <summary>
        /// Callback for a toggle button.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="pressed"></param>
        public void ButtonPressed(Office.IRibbonControl control, bool pressed)
        {
            try
            {
                Worksheet sheet = (Worksheet)Globals.ThisWorkbook.ActiveSheet;
                if (sheet == null) return;
                var viewTree = PafApp.GetViewTreeView();
                var view = PafApp.GetViewMngr().CurrentView;

                switch (control.Id)
                {
                    case "msoDocumentActions":
                        PafApp.GetGridApp().SetTaskPaneStatus(pressed);
                        break;
                    case "msoColumnGroups":
                        if (viewTree == null) break;
                        if (view == null) break;
                        GroupingSpec cSpec = viewTree.GetViewGroupingSpec(ViewAxis.Col, view.ViewName);
                        if (cSpec != null)
                        {
                            cSpec.ApplyGroups = pressed;
                            PafApp.GetEventManager().CommandBarEventHandler("m_ColumnGroup", pressed.ToString());
                            if (ViewSectionGroupChanged != null)
                            {
                                ViewSectionGroupChanged.Invoke(this, new ViewSectionGroupChangedEventArgs(view.ViewName, ViewAxis.Col, pressed, cSpec.DimensionName));
                            }
                        }
                        break;
                    case "msoRowGroups":
                        if (viewTree == null) break;
                        if (view == null) break;
                        GroupingSpec rSpec = viewTree.GetViewGroupingSpec(ViewAxis.Row, view.ViewName);
                        if (rSpec != null)
                        {
                            rSpec.ApplyGroups = pressed;
                            PafApp.GetEventManager().CommandBarEventHandler("m_RowGroup", pressed.ToString());
                            if (ViewSectionGroupChanged != null)
                            {
                                ViewSectionGroupChanged.Invoke(this, new ViewSectionGroupChangedEventArgs(view.ViewName, ViewAxis.Row, pressed, rSpec.DimensionName));
                            }
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Warn(ex);
            }
        }

        /// <summary>
        /// Callback for a toggle button.
        /// </summary>
        /// <param name="control"></param>
        public bool GetButtonPressed(Office.IRibbonControl control)
        {
            try
            {
                var viewTree = PafApp.GetViewTreeView();
                var view = PafApp.GetViewMngr().CurrentView;

                switch (control.Id)
                {
                    case "msoColumnGroups":
                        if (viewTree == null) return false;
                        if (view == null) return false;
                        GroupingSpec cSpec = viewTree.GetViewGroupingSpec(ViewAxis.Col, view.ViewName);
                        bool cApply = cSpec != null && cSpec.ApplyGroups;
                        if (cApply)
                        {
                            return cSpec.GroupsApplied == null || cSpec.GroupsApplied.GetValueOrDefault();
                        }
                        return false;
                    case "msoRowGroups":
                        if (viewTree == null) return false;
                        if (view == null) return false;
                        GroupingSpec rSpec = viewTree.GetViewGroupingSpec(ViewAxis.Row, view.ViewName);
                        bool rApply = rSpec != null && rSpec.ApplyGroups;
                        if (rApply)
                        {
                            return rSpec.GroupsApplied == null || rSpec.GroupsApplied.GetValueOrDefault();
                        }
                        return false;
                    default:
                        return true;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Warn(ex);
                return false;
            }
        }

        /// <summary>
        /// Callback for a control click
        /// </summary>
        /// <param name="control"></param>
        public void ButtonClick(Office.IRibbonControl control)
        {
            ButtonClickPrivate(control, ButtonStatus);
        }

        /// <summary>
        /// Callback for a control click
        /// </summary>
        /// <param name="control"></param>
        /// <param name="allocationStatus"></param>
        private void ButtonClickPrivate(Office.IRibbonControl control, RibbonBarButtonStatus allocationStatus)
        {
            try
            {
                switch (control.Id)
                {
                    case "msoLogin":
                        PafApp.GetEventManager().CommandBarEventHandler("t_Login", String.Empty);
                        break;
                    case "msoRole":
                        PafApp.GetEventManager().CommandBarEventHandler("t_SelectRole", String.Empty);
                        break;
                    case "msoRoleFilter":
                        PafApp.GetEventManager().CommandBarEventHandler("t_SelectRoleFilter", String.Empty);
                        break;
                    case "msoRefresh":
                        PafApp.GetEventManager().CommandBarEventHandler("t_Refresh", String.Empty);
                        break;
                    case "msoUndoChanges":
                        PafApp.GetEventManager().CommandBarEventHandler("t_UndoChanges", String.Empty);
                        break;
                    case "msoUndo":
                        PafApp.GetEventManager().CommandBarEventHandler("t_Undo", String.Empty);
                        break;
                    case "msoCalculate":
                        PafApp.GetEventManager().CommandBarEventHandler("t_Calculate", String.Empty);
                        PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler("t_Calculate");
                        break;
                    case "msoSaveData":
                        PafApp.GetEventManager().CommandBarEventHandler("t_SaveChanges", String.Empty);
                        break;
                    case "msoLockCell":
                        PafApp.GetEventManager().CommandBarEventHandler("t_LockCell", String.Empty);
                        break;
                    case "msoSessionLock":
                        PafApp.GetEventManager().CommandBarEventHandler("t_SessionLock", String.Empty);
                        break;
                    case "msoOptions":
                        PafApp.GetEventManager().CommandBarEventHandler("m_Options", String.Empty);
                        break;
                    case "msoSort":
                        PafApp.GetEventManager().CommandBarEventHandler("m_Sort", String.Empty);
                        break;
                    case "msoHelp":
                        PafApp.GetEventManager().CommandBarEventHandler("m_Help", String.Empty);
                        break;
                    case "msoAbout":
                        PafApp.GetEventManager().CommandBarEventHandler("m_About", String.Empty);
                        break;
                    case "msoPasteShape":
                        PafApp.GetEventManager().CommandBarEventHandler("m_About", String.Empty);
                        break;
                    case "msoAssortment":
                        PafApp.GetEventManager().CommandBarEventHandler("m_CreateAssortment", String.Empty);
                        break;
                    case "msoChangePassword":
                        PafApp.GetEventManager().CommandBarEventHandler("m_ChangePassword", String.Empty);
                        break;
                    case "msoLoadTest":
                        PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler("t_LoadTest");
                        break;
                    case "msoRecordTest":
                        PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler("t_Record");
                        break;
                    case "msoStopTest":
                        PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler("t_Record");
                        break;
                    case "msoDocumentActions":
                        PafApp.GetEventManager().CommandBarEventHandler("t_TaskPane", String.Empty);
                        break;
                    case "msoPasteWithoutProtection":
                        PafApp.GetEventManager().CommandBarEventHandler("t_PasteShape", String.Empty);
                        break;
                    case "msoReplicateSplit":
                    case "msoReplicate":
                        if (allocationStatus.EnableUnreplicateButton())
                        {
                            PafApp.GetEventManager().CommandBarEventHandler("r_Unreplicate", String.Empty);
                        }
                        else
                        {
                            //perform the action determined by global settings - default to all but if that isn't enabled
                            //replicate existing TTN-2384
                            PafApp.GetEventManager().CommandBarEventHandler(PafApp.GetPafPlanSessionResponse().replicateAllEnabled ? "r_ReplicateAll": "r_ReplicateExisting", String.Empty);
                        }
                        break;
                    case "msoLiftSplit":
                        if (allocationStatus.EnableUnliftButton())
                        {
                            PafApp.GetEventManager().CommandBarEventHandler("r_Unlift", String.Empty);
                        }
                        else
                        {
                            //perform the action determined by global settings - default to all but if that isn't enabled
                            //lift existing TTN-2384
                            PafApp.GetEventManager().CommandBarEventHandler(PafApp.GetPafPlanSessionResponse().liftAllEnabled ? "r_LiftAll" : "r_LiftExisting", String.Empty);
                        }
                        break;
                    case "msoRemoveReplication":
                        PafApp.GetEventManager().CommandBarEventHandler("r_Unreplicate", String.Empty);
                        break;
                    case "msoRemoveLift":
                        PafApp.GetEventManager().CommandBarEventHandler("r_Unlift", String.Empty);
                        break;
                    case "msoReplicateExisting":
                        PafApp.GetEventManager().CommandBarEventHandler("r_ReplicateExisting", String.Empty);
                        break;
                    case "msoReplicateAll":
                        PafApp.GetEventManager().CommandBarEventHandler("r_ReplicateAll", String.Empty);
                        break;
                    case "msoLiftExisting":
                        PafApp.GetEventManager().CommandBarEventHandler("r_LiftExisting", String.Empty);
                        break;
                    case "msoLiftAll":
                        PafApp.GetEventManager().CommandBarEventHandler("r_LiftAll", String.Empty);
                        break;

                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Warn(ex);
            }
        }


        /// <summary>
        /// Callback for custom icons.
        /// </summary>
        /// <param name="control"></param>
        /// <returns></returns>
        public Bitmap GetImage(Office.IRibbonControl control)
        {
            return GetImagePrivate(control, ButtonStatus);
        }

        /// <summary>
        /// Callback for custom icons.
        /// </summary>
        /// <param name="control"></param>
        /// <param name="allocationStatus"></param>
        /// <returns></returns>
        private Bitmap GetImagePrivate(Office.IRibbonControl control, RibbonBarButtonStatus allocationStatus)
        {
            switch (control.Id)
            {
                case "msoLogin":
                        return Resources.login;
                case "msoRole":
                    return Resources.role2;
                case "msoSessionLock":
                case "msoSessionLockRight":
                    return PafApp.GetViewMngr().CurrentProtectionMngr != null &&
                           PafApp.GetViewMngr().CurrentProtectionMngr.SessionLocks != null &&
                           PafApp.GetViewMngr().CurrentProtectionMngr.SessionLocks.Count > 0
                               ? Resources.red_lock
                               : Resources.red_unlock;
                case "msoSaveData":
                    return Resources.database_save;
                case "msoHelp":
                    return Resources.help_32;
                case "msoLoadTest":
                    return Resources.play;
                case "msoUndo":
                    return Resources.rewind32;
                case "msoRefresh":
                    return Resources.refresh32;
                case "msoPasteWithoutProtection":
                    return Resources.black_paste;
                case "msoRecordTest":
                    return Resources.record;
                case "msoStopTest":
                    return Resources.stop;
                case "msoOptions":
                    return Resources.settings;
                case "msoCustomAction":
                    return Resources.script32;
                case "msoCalculate":
                    return Resources.calculator;
                case "msoSplitButtonTestHarnessButton":
                    if (PafApp.GetTestHarnessManager().IsRecording && !PafApp.GetTestHarnessManager().IsRunning)
                    {
                        return Resources.record;
                    }
                    if (PafApp.GetTestHarnessManager().IsRunning && !PafApp.GetTestHarnessManager().IsRecording)
                    {
                        return Resources.play;
                    }
                    return Resources.tick;
                case "msoLiftSplit":
                    return allocationStatus.EnableUnliftButton() ? Resources.Remove_16x16 : Resources.green_up_arrow;
                case "msoLiftAll":
                    return Resources.yellow_up_arrow;
                case "msoLiftExisting":
                    return Resources.green_up_arrow;
                case "msoReplicateSplit":
                    return allocationStatus.EnableUnreplicateButton() ? Resources.Remove_16x16 : Resources.three_green_down_arrows;
                case "msoReplicateAll":
                    return Resources.three_yellow_down_arrows;
                case "msoReplicateExisting":
                    return Resources.three_green_down_arrows;
                case "msoRemoveLift":
                case "msoRemoveReplication":
                    return Resources.Remove_16x16;

            }
            return null;
        }

        /// <summary>
        /// Calls ribbon.Invalidate()
        /// </summary>
        private void Invalidate()
        {
            InvalidateAll();
        }

        /// <summary>
        /// Invalidate all the msoId's
        /// </summary>
        public void InvalidateCellGroup()
        {
            if (_ribbon == null) return;
            _ribbon.InvalidateControl("msoLockCell");
            _ribbon.InvalidateControl("msoSessionLock");
            _ribbon.InvalidateControl("msoPasteWithoutProtection");

            _ribbon.InvalidateControl("msoReplicate");
            _ribbon.InvalidateControl("msoReplicateMenu");
            _ribbon.InvalidateControl("msoReplicateExisting");
            _ribbon.InvalidateControl("msoReplicateAll");
            _ribbon.InvalidateControl("msoRemoveReplication");
            _ribbon.InvalidateControl("msoReplicateSplit");

            _ribbon.InvalidateControl("msoLift");
            _ribbon.InvalidateControl("msoLiftSplit");
            _ribbon.InvalidateControl("msoLiftMenu");
            _ribbon.InvalidateControl("msoLiftExisting");
            _ribbon.InvalidateControl("msoLiftAll");
            _ribbon.InvalidateControl("msoRemoveLift");

            ResetUserAllocation();

            _ribbon.InvalidateControl("msoSaveData");
        }

        /// <summary>
        /// Invalidate all the msoId's
        /// </summary>
        public void InvalidateViewOptionsGroup()
        {
            if (_ribbon == null) return;
            _ribbon.InvalidateControl("groupViewOptions");
            _ribbon.InvalidateControl("msoRowGroups");
            _ribbon.InvalidateControl("msoColumnGroups");
        }

        /// <summary>
        /// Invalidate all the msoId's
        /// </summary>
        public void InvalidateAll()
        {
            if (_ribbon == null) return;
            _ribbon.Invalidate();
            ResetUserAllocation();
        }

        /// <summary>
        /// Selects the Pace tab.
        /// </summary>
        public void SelectOurTab()
        {
            if (_ribbon == null) return;
            _ribbon.ActivateTab(PaceTabKey);
        }

        private void ResetUserAllocation()
        {
            _allocationStatus = null;
        }

        private void BuildUserAllocationStatus()
        {
            _allocationStatus = new RibbonBarButtonStatus(PafApp.GetPafPlanSessionResponse());

        }

        #endregion

        #region Helpers

        private string GetLocalizationResourceString(string resourceName)
        {
            return PafApp.GetLocalization().GetResourceManager().GetString(resourceName);
        }

        private static string GetResourceText(string resourceName)
        {
            Assembly asm = Assembly.GetExecutingAssembly();
            string[] resourceNames = asm.GetManifestResourceNames();
            for (int i = 0; i < resourceNames.Length; ++i)
            {
                if (string.Compare(resourceName, resourceNames[i], StringComparison.OrdinalIgnoreCase) == 0)
                {
                    using (StreamReader resourceReader = new StreamReader(asm.GetManifestResourceStream(resourceNames[i])))
                    {
                        if (resourceReader != null)
                        {
                            return resourceReader.ReadToEnd();
                        }
                    }
                }
            }
            return null;
        }

        #endregion
    }
}
