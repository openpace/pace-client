#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using Titan.PafService;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.ExcelGridView
{
    /// <summary>
    /// Client cell note cache.
    /// </summary>
    internal class CellNoteCache
    {
        private Dictionary<Intersection, simpleCellNote> _ClientCnc;
        private Dictionary<Intersection, simpleCellNote> _InsertsCnc;
        private Dictionary<Intersection, simpleCellNote> _UpdatesCnc;
        private List<Intersection> _DeletesCnc;

        /// <summary>
        /// Default constructor.
        /// </summary>
        public CellNoteCache()
        {
            _ClientCnc = new Dictionary<Intersection, simpleCellNote>();
            _InsertsCnc = new Dictionary<Intersection, simpleCellNote>();
            _UpdatesCnc = new Dictionary<Intersection, simpleCellNote>();
            _DeletesCnc = new List<Intersection>();
        }

        /// <summary>
        /// The client cell note cache.
        /// </summary>
        public Dictionary<Intersection, simpleCellNote> ClientCellNoteCache
        {
            get { return _ClientCnc; }
        }

        /// <summary>
        /// Add/update a note in the insert bucket.
        /// </summary>
        public void AddUpdateCellNoteToClientCache(Intersection intersection, simpleCellNote simpleCellNote)
        {
            if (_ClientCnc.ContainsKey(intersection))
            {
                _ClientCnc[intersection] = simpleCellNote;
            }
            else
            {
                _ClientCnc.Add(intersection, simpleCellNote);
            }
        }

        /// <summary>
        /// The newly created cell notes.
        /// </summary>
        public Dictionary<Intersection, simpleCellNote> CellNoteInsert
        {
            get { return _InsertsCnc; }
        }

        /// <summary>
        /// Add/update a note in the insert bucket.
        /// </summary>
        public void AddUpdateCellNoteToInsertBucket(Intersection intersection, simpleCellNote simpleCellNote)
        {
            if(_InsertsCnc.ContainsKey(intersection))
            {
                _InsertsCnc[intersection] = simpleCellNote;
            }
            else
            {
                _InsertsCnc.Add(intersection, simpleCellNote);
            }
        }

        /// <summary>
        /// Cell notes that have been upated.
        /// </summary>
        public Dictionary<Intersection, simpleCellNote> CellNoteUpdates
        {
            get { return _UpdatesCnc; }
        }


        /// <summary>
        /// Add/update a note in the update bucket.
        /// </summary>
        public void AddUpdateCellNoteToUpdateBucket(Intersection intersection, simpleCellNote simpleCellNote)
        {
            if (_UpdatesCnc.ContainsKey(intersection))
            {
                _UpdatesCnc[intersection] = simpleCellNote;
            }
            else
            {
                _UpdatesCnc.Add(intersection, simpleCellNote);
            }
        }

        /// <summary>
        /// Cell notes marked for deletion.
        /// </summary>
        public List<Intersection> CellNoteDeletes
        {
            get { return _DeletesCnc; }
        }

        /// <summary>
        /// Add a note in the delete bucket.
        /// </summary>
        public void AddCellNoteToDeleteBucket(Intersection intersection)
        {
            if (!_DeletesCnc.Contains(intersection))
            {
                _DeletesCnc.Add(intersection);
            }
        }
    }
}