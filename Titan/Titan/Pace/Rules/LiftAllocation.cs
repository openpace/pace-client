﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;

namespace Titan.Pace.Rules
{
    /// <summary>
    /// Lift Allocation.
    /// </summary>
    internal class LiftAllocation : BaseUserAllocation 
    {
        public LiftAllocation(ProtectionMngr parent)
            : base(parent)
        {
            RegexStringAll = PafAppConstants.LIFT_REGEX_STRING_LA;
            RegexStringExisting = PafAppConstants.LIFT_REGEX_STRING_L;
            RegexStringBoth = PafAppConstants.LIFT_REGEX_STRING_L_OR_LA;
            AllocateAllFormatSuffix = PafAppConstants.LIFT_ALL_FORMAT_SUFFIX;
            AllocateExistingFormatSuffix = PafAppConstants.LIFT_EXISTING_FORMAT_SUFFIX;
            AllocateLeadingSymbols = PafAppConstants.LIFT_LEADING_SYMBOLS;
        }

        /// <summary>
        /// Perform lift on a ContiguousRange.
        /// </summary>
        /// <param name="range">The cell to lift.</param>
        /// <param name="overwireNumericFormat">Overwrite the existing numeric format in the cell.</param>
        /// <param name="replicationType">The type of lift to perform.</param>
        public void LiftCells(List<Cell> range, bool overwireNumericFormat, LiftType replicationType)
        {
            AllocateCells(range, overwireNumericFormat, replicationType.ToAllocationType());
        }

        /// <summary>
        /// Perform lift on a cell.
        /// </summary>
        /// <param name="cell">The cell to lift.</param>
        /// <param name="overwireNumericFormat">Overwrite the existing numeric format in the cell.</param>
        /// <param name="liftType">The type of lift to perform.</param>
        public void LiftCell(CellAddress cell, bool overwireNumericFormat, LiftType liftType)
        {
            AllocateCell(cell, overwireNumericFormat, liftType.ToAllocationType());
        }

        /// <summary>
        /// Gets the lift type of a cell.
        /// </summary>
        /// <param name="cell">The address of the cell</param>
        /// <returns>The lift type.</returns>
        public LiftType GetLiftType(CellAddress cell)
        {
            double d;
            return GetLiftType(cell, out d);
        }

        /// <summary>
        /// Gets the lift type of a cell.
        /// </summary>
        /// <param name="cell">The address of the cell</param>
        /// <param name="cellValue">The value of the lift.</param>
        /// <returns>The lift type.</returns>
        public LiftType GetLiftType(CellAddress cell, out double cellValue)
        {
            return GetAllocationType(cell, out cellValue).ToLiftType();
        }

        /// <summary>
        /// Parses a string and returns the type of lift.
        /// </summary>
        /// <param name="replicationString">The string to parse.</param>
        /// <param name="value">The numeric value to lift (out parameter).</param>
        /// <returns>The lift type.</returns>
        public LiftType GetLiftType(object replicationString, out double value)
        {
            return GetAllocationType(replicationString, out value).ToLiftType();
        }
    }
}