#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;

namespace Titan.Pace.Rules 
{
    internal class RuleGroup
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public RuleGroup()
        {
            Rules = new Dictionary<string, Rule>();
            Dependencies = new List<string>();
        }

        /// <summary>
        /// Get/set the Dictionary of rules.
        /// </summary>
        public Dictionary<string, Rule> Rules { get; set; }

        /// <summary>
        /// Get/set the list of string dependencies.
        /// </summary>
        public List<string> Dependencies { get; set; }
    }
}

