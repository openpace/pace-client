﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Titan.Pace.Application.Extensions;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;

namespace Titan.Pace.Rules
{
    /// <summary>
    /// Base type for a user allocation (Replication, Lift).
    /// </summary>
    internal abstract class BaseUserAllocation
    {
        /// <summary>
        /// Parent protection manager.
        /// </summary>
        protected ProtectionMngr Parent { get; set; }
        
        /// <summary>
        /// Gets or sets the allocated cells for a view.
        /// </summary>
        public List<Intersection> AllocateExistingCells { get; set; }

        /// <summary>
        /// Gets or sets the allocated cells for a view.
        /// </summary>
        public List<Intersection> AllocateAllCells { get; set; }

        /// <summary>
        /// REPLICATE_REGEX_STRING_RA
        /// </summary>
        public string RegexStringAll { get; set; }

        /// <summary>
        /// REPLICATE_REGEX_STRING_R
        /// </summary>
        public string RegexStringExisting { get; set; }

        /// <summary>
        /// REPLICATE_REGEX_STRING_R_OR_A
        /// </summary>
        public string RegexStringBoth { get; set; }

        /// <summary>
        /// REPLICATE_ALL_FORMAT_SUFFIX
        /// </summary>
        public string AllocateAllFormatSuffix { get; set; }

        /// <summary>
        /// REPLICATE_EXISTING_FORMAT_SUFFIX
        /// </summary>
        public string AllocateExistingFormatSuffix { get; set; }

        /// <summary>
        /// LIFT_LEADING_SYMBOLS
        /// </summary>
        public string AllocateLeadingSymbols { get; set; }


        protected BaseUserAllocation(ProtectionMngr parent)
        {
            Parent = parent;
            AllocateExistingCells = new List<Intersection>();
            AllocateAllCells = new List<Intersection>();
        }

        #region Public Methods

        /// <summary>
        /// Clears the internal lists.
        /// </summary>
        public void Clear()
        {
            AllocateAllCells.Clear();
            AllocateExistingCells.Clear();
        }

        /// <summary>
        /// Returns true if a allocation cell is present, false if not.
        /// </summary>
        public bool AllocationExists
        {
            get { return AllocateAllCells.Count != 0 || AllocateExistingCells.Count != 0; }
        }

        /// <summary>
        /// Checks to see if a cell is replicated.
        /// </summary>
        /// <param name="cell">The cell to check for replication.</param>
        /// <returns>true if the cell is replicated, false if not.</returns>
        public bool IsCellAllocated(CellAddress cell)
        {
            if (cell != null)
            {
                Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);

                if (intersection != null)
                {
                    return IsIntersectionAllocated(intersection);
                }
            }
            return false;
        }

        /// <summary>
        /// Checks to see if a intersection is replicated.
        /// </summary>
        /// <param name="intersection">The cell to check for replication.</param>
        /// <returns>true if the cell is replicated, false if not.</returns>
        public bool IsIntersectionAllocated(Intersection intersection)
        {
            if (intersection != null)
            {
                return (AllocateAllCells.Contains(intersection) || AllocateExistingCells.Contains(intersection));
            }
            return false;
        }

        /// <summary>
        /// Changes a values in a allocated cell.
        /// </summary>
        /// <param name="cell">The cell to rereplicate.</param>
        public void ReAllocateCell(CellAddress cell)
        {
            //Process the changed cell
            Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);

            if (AllocateAllCells.Contains(intersection))
            {
                AllocateCell(cell, false, AllocationType.All);
                return;
            }

            if (AllocateExistingCells.Contains(intersection))
            {
                AllocateCell(cell, false, AllocationType.Existing);
            }
        }

        /// <summary>
        /// Unreplicate a cells.
        /// </summary>
        /// <param name="cells">The cell to unreplicate.</param>
        /// <param name="sheetHash">Hash code of the sheet.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        public void UnAllocateCells(List<Cell> cells, string sheetHash, string sheetName)
        {
            foreach (Cell cell in cells)
            {
                UnAllocateCell(cell.CellAddress, sheetHash, sheetName);
            }
        }

        /// <summary>
        /// Unreplicate a cell.
        /// </summary>
        /// <param name="cell">The cell to unreplicate.</param>
        /// <param name="sheetHash">Hash code of the sheet.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        public void UnAllocateCell(CellAddress cell, string sheetHash, string sheetName)
        {
            //Process the changed cell
            Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);
            //Remove the intersection from the replication lists.
            AllocateAllCells.Remove(intersection);
            AllocateExistingCells.Remove(intersection);
            //do the normal pp stuff.
            Parent.HandleChangedCells(cell, sheetHash, sheetName, false);
            //Store the changed intersections to be formatted
            List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);
            if (cellAdresses != null)
            {
                List<Range> rng = cellAdresses.Select(cellAddress => new Range(new CellAddress(cellAddress.Row, cellAddress.Col))).ToList();
                //reset the formatting changes back to the defaults.
                PafApp.GetFormattingMngr().ResetFormatChanges(rng);
                //reset the change formatting color.
                PafApp.GetViewMngr().CurrentGrid.SetBgFillColor(rng, PafApp.GetPafAppSettingsHelper().GetSystemLockColor());
            }
        }

        /// <summary>
        /// Removes all the formating from the cell text minus any commas, replication information, or decimal points.
        /// </summary>
        /// <param name="cellText">The text of the cell, which contains the numeric format.</param>
        /// <param name="cellFormat">The numeric format of the cell.</param>
        /// <returns>The cell minus the commas, replication information, or decimal points.</returns>
        /// <remarks>TTN-871</remarks>
        public string RemoveFormatInfoFromString(string cellText, string cellFormat)
        {
            string returnString = cellText;
            Regex reggie = new Regex(AllocateLeadingSymbols, RegexOptions.IgnoreCase);
            Regex r2 = new Regex(RegexStringBoth, RegexOptions.IgnoreCase);
            foreach (char c in cellFormat)
            {
                if (!c.IsDouble())
                {
                    if (!reggie.IsMatch(Convert.ToString(c)) && !r2.IsMatch(Convert.ToString(c)))
                    {
                        returnString = returnString.Replace(Convert.ToString(c), String.Empty);
                    }
                }
            }
            return returnString.Trim();
        }

        /// <summary>
        /// Parses the user entered value, to determine if its valid entry.
        /// </summary>
        /// <param name="userEnteredValue">The string value enterd by the user.</param>
        /// <param name="regularExpression">The regular expression used to parse the user entered value.</param>
        /// <param name="value">The valid numeric value that is to be replicated.</param>
        /// <returns>true if the value is value, false if not.</returns>
        public bool IsExpressionValidForReplication(object userEnteredValue, string regularExpression, out double value)
        {
            return IsExpressionValidForReplication(userEnteredValue.ToString(), regularExpression, out value);
        }

        #endregion Public Methods


        #region Proteced Methods

       

        protected AllocationType GetAllocationType(object replicationString, out double value)
        {
            if (IsExpressionValidForReplication(Convert.ToString(replicationString), RegexStringAll, out value))
            {
                return AllocationType.All;
            }

            if (IsExpressionValidForReplication(Convert.ToString(replicationString), RegexStringExisting, out value))
            {
                return AllocationType.Existing;
            }

            return AllocationType.None;
        }

        protected AllocationType GetAllocationType(CellAddress cell)
        {
            double d;
            return GetAllocationType(cell, out d);
        }

        protected AllocationType GetAllocationType(CellAddress cell, out double cellValue)
        {
            //Process the changed cell
            Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);
            cellValue = 0;

            if (intersection == null)
            {
                return AllocationType.None;
            }

            //set the double value
            cellValue = PafApp.GetViewMngr().CurrentGrid.GetValueDouble(cell.Row, cell.Col);

            if (AllocateAllCells.Contains(intersection))
            {
                return AllocationType.All;
            }

            if (AllocateExistingCells.Contains(intersection))
            {
                return AllocationType.Existing;
            }

            return AllocationType.None;
        }

        protected void AllocateCells(List<Cell> range, bool overwireNumericFormat, AllocationType replicationType)
        {
            foreach (Cell cell in range.Where(cell => PafApp.GetViewMngr().CurrentOlapView.DataRange.InContiguousRange(cell)))
            {
                AllocateCell(cell.CellAddress, overwireNumericFormat, replicationType);
            }
        }

        protected void AllocateCell(CellAddress cell, bool overwireNumericFormat, AllocationType replicationType)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            //Process the changed cell
            Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);
            IGridInterface grid = PafApp.GetViewMngr().CurrentGrid;
            OlapView olapView = PafApp.GetViewMngr().CurrentOlapView;
            ViewStateInfo viewState = PafApp.GetViewMngr().CurrentView;

            if (!Parent.IsCellChanged(cell))
            {
                string sheetHash = Convert.ToString(viewState.HashCode);
                Parent.HandleChangedCells(cell, sheetHash, grid.Name, false);
                bool changedRanges = Parent.ChangedRange.ContiguousRanges != null && Parent.ChangedRange.ContiguousRanges.Count > 0;

                Format tempFormat = PafApp.GetFormattingMngr().GetDefaultCellFormat(cell);
                if (tempFormat != null)
                {
                    ChangedCellInfo cc = new ChangedCellInfo(sheetHash, cell, Change.UserChanged, tempFormat);
                    PafApp.GetChangedCellMngr().ChangeCell(cc, PafApp.GetChangedCellMngr().SheetCellTracker[sheetHash], false);
                }

                //Set the color for the changed Cells
                if (changedRanges)
                {
                    PafApp.GetViewMngr().CurrentGrid.SetBgFillColor(Parent.ChangedRange, PafApp.GetPafAppSettingsHelper().GetSystemLockColor());
                }
            }
            
            //remove the intersection for the change list.
            if (Parent.Changes.Contains(intersection))
            {
                Parent.Changes.Remove(intersection);
            }
            //add the intersection to on the replication lists.
            switch (replicationType)
            {
                case AllocationType.All:
                    if (!AllocateAllCells.Contains(intersection))
                    {
                        AllocateAllCells.Add(intersection);
                    }
                    break;
                case AllocationType.Existing:
                    if (!AllocateExistingCells.Contains(intersection))
                    {
                        AllocateExistingCells.Add(intersection);
                    }
                    break;
            }

            //Store the changed intersections to be formatted
            List<CellAddress> cellAdresses = olapView.FindIntersectionAddress(intersection);
            if (cellAdresses != null)
            {
                List<Range> rng = new List<Range>();
                foreach (CellAddress t in cellAdresses)
                {
                    rng.Add(new Range(t));
                    //If an intersection exists multiple times in a view section, copy the value at the changed cell address
                    //to the other cell addresses.  I am not using an array copy here because I don't think it is needed - this
                    //may need to be changed.
                    if (!t.Equals(cell))
                    {
                        grid.EnableEvents(false);
                        grid.CopyCellValue(cell, t);
                        grid.EnableEvents(true);
                    }
                }
            
                if (overwireNumericFormat)
                {
                    string tempNumericFormat = grid.GetNumericFormat(cell.Row, cell.Col);
                    string numericFormat;

                    //we need to format the cells for replication.
                    switch (replicationType)
                    {
                        case AllocationType.All:
                            numericFormat = tempNumericFormat.InsertString(";", AllocateAllFormatSuffix);
                            //TTN-1013
                            numericFormat += PafAppConstants.BLANK_CHAR + AllocateAllFormatSuffix;
                            grid.SetNumericFormat(rng, numericFormat);
                            break;
                        case AllocationType.Existing:
                            numericFormat = tempNumericFormat.InsertString(";", AllocateExistingFormatSuffix);
                            //TTN-1013
                            numericFormat += PafAppConstants.BLANK_CHAR + AllocateExistingFormatSuffix;
                            grid.SetNumericFormat(rng, numericFormat);
                            break;
                    }
                }
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("AllocateCell, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }

        #endregion Proteced Methods

        #region Private Methods
        /// <summary>
        /// Parses the user entered value, to determine if its valid entry.
        /// </summary>
        /// <param name="userEnteredValue">The string value enterd by the user.</param>
        /// <param name="regularExpression">The regular expression used to parse the user entered value.</param>
        /// <param name="value">The valid numeric value that is to be replicated.</param>
        /// <returns>true if the value is value, false if not.</returns>
        private bool IsExpressionValidForReplication(string userEnteredValue, string regularExpression, out double value)
        {
            bool ret = false;
            bool containPercent = false;
            value = 0;

            Regex rLead = new Regex(
                AllocateLeadingSymbols,
                RegexOptions.IgnoreCase);

            if (String.IsNullOrWhiteSpace(userEnteredValue)) return false;

            if (userEnteredValue.Substring(0, 1).IsDouble() || rLead.IsMatch(userEnteredValue.Substring(0, 1)))
            {
                Regex reggie = new Regex(regularExpression, RegexOptions.IgnoreCase);
                //remove any extra stuff from the user entered value...
                Regex reggie2 = new Regex(@"\%", RegexOptions.IgnoreCase);

                if (reggie.IsMatch(userEnteredValue))
                {
                    //replace the regular expression with an empty string
                    string s = reggie.Replace(userEnteredValue, String.Empty, 1);

                    //see if the string contains a percentage.
                    if (reggie2.IsMatch(s))
                    {
                        containPercent = true;
                        s = reggie2.Replace(s, String.Empty, 1);
                    }

                    if (s.Trim().IsDouble())
                    {
                        if (!containPercent)
                        {
                            value = Double.Parse(s.Trim());
                        }
                        else
                        {
                            value = (Double.Parse(s.Trim()) / 100);
                        }
                        ret = true;
                    }
                }
            }
            return ret;
        }

       
        #endregion Private Methods
    }
}
