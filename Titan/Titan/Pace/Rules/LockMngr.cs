﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Linq;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Base.Data;
using Titan.Palladium;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Rules
{
    internal class LockMngr
    {
        public LockMngr()
        {
            UserSessionLocks = new List<CoordinateSet>();
            UserSessionParentLocks = new List<CoordinateSet>();
            UserSessionAttributeParentLocks = new List<CoordinateSet>();
        }

        /// <summary>
        /// All User locked intersections (Session)
        /// </summary>
        public List<CoordinateSet> UserSessionLocks { get; private set; }

        /// <summary>
        /// Parent Only User locked intersections (Session)
        /// </summary>
        public List<CoordinateSet> UserSessionParentLocks { get; private set; }

        /// <summary>
        /// Attribute Parent Only User locked intersections (Session)
        /// These are removed from UserSessionParentLocks
        /// </summary>
        public List<CoordinateSet> UserSessionAttributeParentLocks { get; private set; }

        /// <summary>
        /// Adds or combines the UserSessionLocks coordinateset.
        /// </summary>
        /// <param name="item"></param>
        public void AddUserSessionLocksCoordinateSet(CoordinateSet item)
        {
            foreach (CoordinateSet cs in UserSessionLocks)
            {
                if (cs.Dimensions.ArrayEqual(item.Dimensions))
                {
                    cs.AddCoordinates(item.GetCoordinates());
                    return;
                }
            }
            UserSessionLocks.Add(item);
        }

        /// <summary>
        /// Adds or combines the UserSessionParentLocks coordinateset.
        /// </summary>
        /// <param name="item"></param>
        /// <param name="hasAttributes"></param>
        public void AddUserSessionParentLocksCoordinateSet(CoordinateSet item, bool hasAttributes)
        {
            if (!hasAttributes)
            {
                foreach (CoordinateSet cs in UserSessionParentLocks)
                {
                    if (cs.Dimensions.ArrayEqual(item.Dimensions))
                    {
                        cs.AddCoordinates(item.GetCoordinates());
                        return;
                    }
                }
                UserSessionParentLocks.Add(item);
            }
            else
            {
                AddUserSessionAttributeParentLocksCoordinateSet(item);
            }
        }

        /// <summary>
        /// Adds or combines the UserSessionAttributeParentLocks coordinateset.
        /// </summary>
        /// <param name="item"></param>
        private void AddUserSessionAttributeParentLocksCoordinateSet(CoordinateSet item)
        {
            foreach (CoordinateSet cs in UserSessionParentLocks)
            {
                if (cs.Dimensions.ArrayEqual(item.Dimensions))
                {
                    cs.AddCoordinates(item.GetCoordinates());
                    return;
                }
            }
            UserSessionAttributeParentLocks.Add(item);
        }


        /// <summary>
        /// Checks both CoordinateSet list for the existence of an Intersection.
        /// </summary>
        /// <param name="intersection"></param>
        /// <returns></returns>
        public bool ContainsIntersection(Intersection intersection)
        {
            Coordinate c = new Coordinate(intersection.Coordinates);
            return UserSessionLocks.Any(cs => cs.ContainsCoordinate(c)) ||
                   UserSessionParentLocks.Any(cs => cs.ContainsCoordinate(c));
        }

        /// <summary>
        /// Returns all the Parent Locks (Attribute and Non Attribute)
        /// </summary>
        /// <returns></returns>
        public List<CoordinateSet> AllUserSessionParentLocks()
        {
            List<CoordinateSet> temp = new List<CoordinateSet>(2);
            if (UserSessionParentLocks != null && UserSessionParentLocks.Count > 0)
            {
                temp.AddRange(UserSessionParentLocks);
            }
            if (UserSessionAttributeParentLocks != null && UserSessionAttributeParentLocks.Count > 0)
            {
                temp.AddRange(UserSessionAttributeParentLocks);
            }
            return temp;
        }

        /// <summary>
        /// Gets a list of all the CoordinateSet
        /// </summary>
        /// <returns>List of CoordinateSet</returns>
        public List<CoordinateSet> AllSessionLocks()
        {
            List<CoordinateSet> temp = new List<CoordinateSet>(2);
            temp.AddRange(UserSessionLocks);
            if (UserSessionParentLocks != null && UserSessionParentLocks.Count > 0)
            {
                temp.AddRange(UserSessionParentLocks);
            }

            //foreach (CoordinateSet c in UserSessionParentLocks)
            //{
            //    bool add = true;
            //    foreach (CoordinateSet cs in temp)
            //    {
            //        if (c.CanCombine(cs) && c.Count() == 1)
            //        {
            //            if (cs.ContainsCoordinate(c.GetCoordinates()[0]))
            //            {
            //                add = false;
            //                break;
            //            }
            //        }
            //    }
            //    if(add) temp.Add(c);
                
            //}

            return temp;
        }

        /// <summary>
        /// Gets the count (of Coordinates) in the GlobalLockMngr.
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return UserSessionLocks.Sum(cs => cs.Count()) + UserSessionParentLocks.Sum(cs => cs.Count());
        }

        /// <summary>
        /// Clear the internal lists.
        /// </summary>
        public void Clear()
        {
            UserSessionLocks = null;
            UserSessionParentLocks = null;
            UserSessionAttributeParentLocks = null;
            UserSessionLocks = new List<CoordinateSet>();
            UserSessionParentLocks = new List<CoordinateSet>();
            UserSessionAttributeParentLocks = new List<CoordinateSet>();
        }

    }
}
