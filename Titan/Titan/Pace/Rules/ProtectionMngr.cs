#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Application.Win32;
using Titan.Pace.Base.Data;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.Rules.Utility;
using Titan.PafService;
using Titan.Palladium;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Palladium.Rules;

namespace Titan.Pace.Rules
{
    internal class ProtectionMngr
    {
        /// <summary>
        /// Constant for FORWARDPLANNABLE
        /// </summary>
        private const string ForwardPlannable = "FORWARDPLANNABLE";

        private Stopwatch _stopwatch;
        private HashSet<int> _changedIntersections;
        private Dictionary<int, RuleInfo> _rules;
        private int _ruleCounter;
        private Range _userLockedRange;
        private Range _changedRange;
        private Range _protectedRange;
        //private PafDataCache _dataCache;
        private List<RuleSet> _ruleSets;
        private Dictionary<string, MeasureDef> _measureDefs;
#pragma warning disable 67
// ReSharper disable InconsistentNaming
        public event ProtectionMngr_UserLockedCell UserLockedCell;
        public delegate void ProtectionMngr_UserLockedCell(object sender, UserLockedCellEventArgs EventArgs);
        public event ProtectionMngr_SessionLockedCell SessionLockedCell;
        public delegate void ProtectionMngr_SessionLockedCell(object sender, SessionLockedCellEventArgs EventArgs);
        public event ProtectionMngr_AddChangedCell AddChangedCell;
        public event ProtectionMngr_AddChangedCells AddChangedCells;
        public delegate void ProtectionMngr_AddChangedCell(object sender, AddChangedCellEventArgs EventArgs);
        public delegate void ProtectionMngr_AddChangedCells(object sender, AddChangedCellsEventArgs EventArgs);
#pragma warning restore 67
// ReSharper restore InconsistentNaming

        public ProtectionMngr()
        {
            PasteShape = false;
            ClearProtection();
        }

        /// <summary>
        /// Clears Class variable values
        /// </summary>
        public void ClearProtection()
        {
            SystemLocks = new HashSet<int>();
            ProtectedRules = new List<string>();
            UserLocks = new HashSet<Intersection>();
            SessionLocks = new HashSet<int>();
            Changes = new HashSet<Intersection>();
            UnLockedChanges = new HashSet<Intersection>();
            InvalidReplicationCells = new HashSet<int>();
            ChangeCells = new Queue<Object>();
            ProtectCells = new Queue<Object>();
            PasteShape = false;
            Replication = new ReplicationAllocation(this);
            Lift = new LiftAllocation(this);
            //GlobalLocks = new GlobalLockMngr();
            //ProtectedCells = new Dictionary<int, Intersection>();
            ProtectedCells = new HashSet<int>();
            ProtectedCellsSet = new CoordinateSet();
            _changedRange = null;
            _protectedRange = null;
            Dependencies = new Dictionary<string, Dictionary<string, Dictionary<int, Dictionary<int, List<string>>>>>(3);
            //_dataCache = null;
            _changedIntersections = new HashSet<int>();
            _ruleSets = null;
            AllRuleSets = null;
            _rules = new Dictionary<int, RuleInfo>();
            _ruleCounter = 0;
            _measureDefs = null;
            
        }

        ///// <summary>
        ///// Gets the object that stores change and protection info for all intersections in the UOW
        ///// </summary>
        //public PafDataCache DataCache
        //{
        //    get { return _dataCache ?? (_dataCache = new PafDataCache()); }
        //}

        /// <summary>
        /// Replication allocation type.
        /// </summary>
        public ReplicationAllocation Replication { get; private set; }

        /// <summary>
        /// Lift allocation type
        /// </summary>
        public LiftAllocation Lift { get; private set; }

        /// <summary>
        /// System Locked intersections
        /// </summary>
        public HashSet<int> SystemLocks { get; set; }

        /// <summary>
        /// Cells that cannot be replicated.
        /// </summary>
        public HashSet<int> InvalidReplicationCells { get; set; }

        /// <summary>
        /// User locked intersections
        /// </summary>
        public HashSet<Intersection> UserLocks { get; private set; }

        /// <summary>
        /// Sesion (user type lock) locked intersections
        /// </summary>
        public HashSet<int> SessionLocks { get; private set; }

        /// <summary>
        /// Global Locks.
        /// </summary>
        public LockMngr GlobalLocks { get; set; }

        /// <summary>
        /// Changes passed to middle tier
        /// </summary>
        public HashSet<Intersection> Changes { get; set; }

        /// <summary>
        /// Changes passed to middle tier
        /// </summary>
        public HashSet<Intersection> UnLockedChanges { get; set; }

        /// <summary>
        /// Protected cells that are passed to middle tier
        /// </summary>
        public HashSet<int> ProtectedCells { get; set; }

        /// <summary>
        /// Protected cells that are passed to middle tier
        /// </summary>
        public CoordinateSet ProtectedCellsSet { get; set; }

        /// <summary>
        /// List to hold every ruleset sent from the server.  
        /// </summary>
        /// <remarks>Note that _ruleSets collection will only contain RuleSet that contain rule groups.</remarks>
        public HashSet<RuleSet>  AllRuleSets { get; private set; }

        /// <summary>
        /// Gets the current ruleset
        /// </summary>
        public RuleSet CurrentRuleset
        {
            get
            {
                return AllRuleSets.Where(rs => !String.IsNullOrEmpty(rs.Name)).FirstOrDefault(rs => rs.Name.Equals(PafApp.RuleSetName));
            }
        }

        ///// <summary>
        ///// Protected cells that are passed to middle tier
        ///// </summary>
        //public Dictionary<int, Intersection> ProtectedCells { get; set; }

        ///// <summary>
        ///// Protected cells that are passed to middle tier
        ///// </summary>
        //public HashSet<Intersection> ProtectedCellsSet
        //{
        //    get
        //    {
        //        return ProtectedCells.DictionaryToHashSet();
        //    }
        //}


        /// <summary>
        /// List of the rules that are passed to middle tier
        /// </summary>
        public List<string> ProtectedRules { get; set; }

        /// <summary>
        /// Changed cells that are added to change queu which is processed in ClearQueues
        /// </summary>
        public Queue<object> ChangeCells { get; set; }

        /// <summary>
        /// Protect cells that are added to the protected cells queu which is processed in ClearQueues
        /// </summary>
        public Queue<object> ProtectCells { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Range ChangedRange
        {
            get { return _changedRange ?? (_changedRange = new Range()); }

            set
            {
                _changedRange = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Range ProtectedRange
        {
            get { return _protectedRange ?? (_protectedRange = new Range()); }

            set
            {
                _protectedRange = value;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public Range UserLockedRange
        {
            get { return _userLockedRange ?? (_userLockedRange = new Range()); }

            set
            {
                _userLockedRange = value;
            }
        }

        /// <summary>
        /// Dependencies.
        /// In the first dictionary the key is the dimension name.
        /// In the second dictionary the key is the coordinate.
        /// In the third dictionary the key is an integer which maps back to the RuleSet Type (extracted from the CacheBlock)
        /// The List of string in the last dictionary is the list of dependent measures.
        /// </summary>
        public Dictionary<string, Dictionary<string, Dictionary<int, Dictionary<int, List<string>>>>> Dependencies { get; private set; }

        /// <summary>
        /// A flag that identifies a change that should be sent to the sever as a change, but not as a lock
        /// </summary>
        public bool PasteShape { get; set; }


        #region PublicMethods

        /// <summary>
        /// Gets the protected cells range (does a calculation every time)
        /// </summary>
        /// <returns></returns>
        public void BuildProtectedCellsRange()
        {
            CoordinateSet coordinateSet = PafApp.GetViewMngr().CurrentProtectionMngr.ProtectedCellsSet;
            List<CellAddress> cells = new List<CellAddress>(coordinateSet.Count());
            foreach (Coordinate c in coordinateSet.GetCoordinates())
            {
                Intersection kv = new Intersection(coordinateSet.Dimensions, c.Coordinates);
                List<CellAddress> cellAddresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(kv);
                if (cellAddresses != null)
                {
                    cells.AddRange(cellAddresses.Where(ca => ca != null));
                }
            }
            cells.Sort();
            ProtectedRange = cells.GetRange();
        }

        /// <summary>
        /// Checks to see if the ProtectionManager has any calculations waiting to be performed.
        /// </summary>
        /// <returns>True if calculations are pending, false if no calculation(s) are pending.</returns>
        public bool CalculationsPending()
        {
            try
            {
                //02/08/2007 KRM added replicate, so calcuate button is ungreyed when replicate is by itself.
                //08/06/2007 KRM Per P-Mack added user locks.  Now if a user locks a cell that is considered a
                //change.
                if ((Changes != null && Changes.Count > 0) ||
                    (Replication.AllocateAllCells != null && Replication.AllocateAllCells.Count > 0) ||
                    (Replication.AllocateExistingCells != null && Replication.AllocateExistingCells.Count > 0) ||
                    (Lift.AllocateAllCells != null && Lift.AllocateAllCells.Count > 0) ||
                    (Lift.AllocateExistingCells != null && Lift.AllocateExistingCells.Count > 0) ||
                    (UnLockedChanges != null && UnLockedChanges.Count > 0) ||
                    (UserLocks != null && UserLocks.Count > 0))
                {
                    return true;
                }

                return false;
            }
            catch(Exception)
            {
                return false;
            }
        }


        /// <summary>
        /// This method creates a List of RuleSet objects (containing Rule Groups) from 
        /// the rule sets in the cache block.  The cache block is returned to the client
        /// when the unit of work is defined.
        /// </summary>
        /// <returns>List of RuleSets</returns>
        public List<RuleSet> GetRuleSets()
        {
            //DateTime startTime = DateTime.Now;

            if (_ruleSets == null)
            {
                
                pafClientCacheBlock cacheBlock = PafApp.CacheBlock;
                var memberTagMap = cacheBlock.ToRuleSetMemberTagDictionary();
                _ruleSets = new List<RuleSet>();
                AllRuleSets = new HashSet<RuleSet>();

                string time = PafApp.CacheBlock.mdbDef.timeDim;
                const string timeHorizon = PafAppConstants.TIME_HORIZON_DIM;
                string measure = PafApp.CacheBlock.mdbDef.measureDim;

                foreach (ruleSet tempRuleSet in cacheBlock.ruleSets)
                {
                    if (tempRuleSet.dimension == time) continue;

                    RuleSet ruleSet = new RuleSet(tempRuleSet.dimension,
                                                  tempRuleSet.type,
                                                  tempRuleSet.name,
                                                  measure, 
                                                  timeHorizon);

                    if (!String.IsNullOrEmpty(ruleSet.Name) && memberTagMap.ContainsKey(ruleSet.Name))
                    {
                        ruleSet.HasMemberTagFormulas = true;
                        ruleSet.ReferencedMbrTagDefs = memberTagMap[ruleSet.Name];
                    }

                    //if the ruleGroups are null, then go to the next ruleSet
                    //but add the ruleset to the master list.
                    if (tempRuleSet.ruleGroups == null)
                    {
                        AllRuleSets.Add(ruleSet);
                        continue;
                    }

                    foreach (ruleGroup tempRuleGroup in tempRuleSet.ruleGroups)
                    {
                        RuleGroup ruleGroup = new RuleGroup();
                        foreach (rule tempRule in tempRuleGroup.rules)
                        {
                            Rule rule = new Rule(
                                tempRule.formula.expression.Trim(),
                                tempRule.formula.resultTerm.Trim());

                            ArrayList formulaTokens = RulesUtility.getFormulaTokens(tempRule.formula.resultTerm.Trim());

                            //if the term consists of a function then ...
                            if (formulaTokens != null && formulaTokens.Count > 1)
                            {
                                //rule.Function = formulaTokens[0].ToString();
                                rule.SetFunction(formulaTokens[0].ToString());
                                //remove the function, the rest of the tokens are params
                                formulaTokens.Remove(formulaTokens[0]);
                                rule.Params = formulaTokens.ToArray();
                                //the first param is the member
                                ruleGroup.Dependencies.Add(formulaTokens[0].ToString());
                                //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("0:" + formulaTokens[0].ToString());
                                ruleGroup.Rules.Add(formulaTokens[0].ToString(), rule);
                            }
                            else
                            {
                                ruleGroup.Dependencies.Add(tempRule.formula.resultTerm.Trim());
                                try
                                {
                                    ruleGroup.Rules.Add(tempRule.formula.resultTerm.Trim(), rule);
                                }
                                catch (Exception)
                                {
                                    throw new Exception(String.Format(
                                            PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.DuplicateRuleResultTerm"),
                                            new object[] { tempRuleSet.name, tempRule.formula.resultTerm }));

                                }
                            }
                        }

                        ruleSet.RuleGroups.Add(ruleGroup);
                    }
                    _ruleSets.Add(ruleSet);
                    AllRuleSets.Add(ruleSet);
                }
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("GetRuleSets, runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            return _ruleSets;
        }

        /// <summary>
        /// Non-plannable and forward plannable intersections are added directly to the SystemLocks list
        /// </summary>
        public void AddSystemLocks()
        {
            _stopwatch = Stopwatch.StartNew();

            Intersection intersection;

            Stopwatch stopWatch1 = Stopwatch.StartNew();
            long cellCount = 0;
            if (PafApp.GetViewMngr().CurrentOlapView.ForwardPlannableRange != null)
            {
                foreach (ContiguousRange contigRange in PafApp.GetViewMngr().CurrentOlapView.ForwardPlannableRange.ContiguousRanges)
                {
                    foreach (Cell cll in contigRange.Cells)
                    {
                        intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cll.CellAddress);

                        //Populate the Data Cache with change flags for each changed intersection
                        //DataCache.setCellValue(intersection.Coordinates, 1);
                        //_changedIntersections.Add(intersection.UniqueString);
                        _changedIntersections.Add(intersection.UniqueID);

                        //Add the locked cell to the list of locked cells
                        if (!SystemLocks.Contains(intersection.UniqueID))
                        {
                            SystemLocks.Add(intersection.UniqueID);
                        }
                        cellCount++;
                    }
                }
            }
            stopWatch1.Stop();
            PafApp.GetLogger().InfoFormat("Add System Locks(processing ForwardPlannableRange), runtime: {0} (ms), processed: {1} cells.", new object[] { stopWatch1.ElapsedMilliseconds.ToString("N0"), cellCount.ToString("N0") });

            Stopwatch startTime2 = Stopwatch.StartNew();
            cellCount = 0;
            if (PafApp.GetViewMngr().CurrentOlapView.NonPlannableRange != null)
            {
                foreach (ContiguousRange contigRange in PafApp.GetViewMngr().CurrentOlapView.NonPlannableRange.ContiguousRanges)
                {

                    foreach (Cell cll in contigRange.Cells)
                    {
                        intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cll.CellAddress);

                        //Populate the Data Cache with change flags for each changed intersection
                        //DataCache.setCellValue(intersection.Coordinates, 1);
                        //_changedIntersections.Add(intersection.UniqueString);
                        _changedIntersections.Add(intersection.UniqueID);

                        //TTN-1574
                        SystemLocks.Add(intersection.UniqueID);

                        cellCount++;

                        ////Add the locked cell to the list of locked cells)
                        //if (!this.SystemLocks.Contains(intersection))
                        //{
                        //    this.SystemLocks.Add(intersection);
                        //    //AddtoIntersectionFilter(intersection);
                        //}
                    }
                }
            }
            startTime2.Stop();
            PafApp.GetLogger().InfoFormat("Add System Locks(processing NonPlannableRange), runtime: {0} (ms), processed: {1} cells.", new object[] { startTime2.ElapsedMilliseconds.ToString("N0"), cellCount.ToString("N0") });

            Stopwatch startTime3 = Stopwatch.StartNew();
            //Add Invalid Attribute intersections to the system locks
            cellCount = 0;
            if (PafApp.GetViewMngr().CurrentOlapView.InValidAttributeRange != null)
            {
                foreach (ContiguousRange contigRange in PafApp.GetViewMngr().CurrentOlapView.InValidAttributeRange.ContiguousRanges)
                {
                    foreach (Cell cll in contigRange.Cells)
                    {
                        intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cll.CellAddress);

                        //Populate the Data Cache with change flags for each changed intersection
                        //DataCache.setCellValue(intersection.Coordinates, 1);
                        //_changedIntersections.Add(intersection.UniqueString);
                        _changedIntersections.Add(intersection.UniqueID);

                        //Add the locked cell to the list of locked cells
                        if (!SystemLocks.Contains(intersection.UniqueID))
                        {
                            SystemLocks.Add(intersection.UniqueID);
                        }
                        cellCount++;
                    }
                }
            }
            startTime3.Stop();
            PafApp.GetLogger().InfoFormat("Add System Locks(processing InValidAttributeRange), runtime: {0} (ms), processed: {1} cells.", new object[] { startTime3.ElapsedMilliseconds.ToString("N0"), cellCount.ToString("N0") });

            Stopwatch startTime4 = Stopwatch.StartNew();
            //Add Invalid Attribute intersections to the system locks
            cellCount = 0;
            if (PafApp.GetViewMngr().CurrentOlapView.InValidMemberTagRange != null)
            {
                foreach (ContiguousRange contigRange in PafApp.GetViewMngr().CurrentOlapView.InValidMemberTagRange.ContiguousRanges)
                {
                    foreach (Cell cll in contigRange.Cells)
                    {
                        intersection = PafApp.GetViewMngr().CurrentOlapView.MemberTag.FindMemberTagIntersection(cll.CellAddress);

                        //Add the locked cell to the list of locked cells
                        if (!SystemLocks.Contains(intersection.UniqueID))
                        {
                            SystemLocks.Add(intersection.UniqueID);
                        }
                        cellCount++;
                    }
                }
            }
            startTime4.Stop();
            PafApp.GetLogger().InfoFormat("Add System Locks(processing InValidMemberTagRange), runtime: {0} (ms), processed {1} cells.", new object[] { startTime4.ElapsedMilliseconds.ToString("N0"), cellCount.ToString("N0") });

            Stopwatch startTime5 = Stopwatch.StartNew();
            //add invalid replication intersection to prot mngr, not a system lock but a replication lock.
            if(PafApp.GetViewMngr().CurrentOlapView.InValidReplicationRange != null)
            {
                foreach (PafSimpleCoordList scl in PafApp.GetViewMngr().CurrentOlapView.InValidReplicationRange)
                {
                    Intersection[] intersections = scl.GetSimpleCoordList.ToIntersections();
                    foreach (Intersection inter in intersections)
                    {
                        int uniqueId = inter.UniqueID;
                        if (!InvalidReplicationCells.Contains(uniqueId))
                        {
                            InvalidReplicationCells.Add(uniqueId);
                        }
                    }
                }
            }
            startTime5.Stop();
            PafApp.GetLogger().InfoFormat("Add System Locks(processing InValidReplicationRange), runtime {0} (ms)", new object[] { startTime5.ElapsedMilliseconds.ToString("N0") });


            startTime5 = Stopwatch.StartNew();
            if (PafApp.GetViewMngr().CurrentOlapView.SessionLockProtectedRange != null)
            {
                GlobalLocks = new LockMngr();
                foreach (ContiguousRange contigRange in PafApp.GetViewMngr().CurrentOlapView.SessionLockProtectedRange.ContiguousRanges)
                {
                    CoordinateSet coordinateSet = new CoordinateSet();
                    foreach (Cell cll in contigRange.Cells)
                    {
                        string[] dims;
                        Coordinate c = PafApp.GetViewMngr().CurrentOlapView.FindCoordinate(cll.CellAddress, out dims);
                        if (coordinateSet.Dimensions == null)
                        {
                            coordinateSet.Dimensions = dims;
                        }
                        coordinateSet.AddCoordinate(c);
                    }
                    GlobalLocks.UserSessionLocks.Add(coordinateSet);
                }
                foreach (CoordinateSet cs in PafApp.GetViewMngr().GlobalLockMngr.UserSessionAttributeParentLocks)
                {
                    GlobalLocks.AddUserSessionParentLocksCoordinateSet(cs, true);
                }
                //foreach (CoordinateSet cs in PafApp.GetViewMngr().GlobalLockMngr.UserSessionParentLocks)
                //{
                //    GlobalLocks.AddUserSessionParentLocksCoordinateSet(cs, false);
                //}
            }

            startTime5.Stop();
            PafApp.GetLogger().InfoFormat("Add Session Locks, runtime {0} (ms)", new object[] { startTime5.ElapsedMilliseconds.ToString("N0") });


            _stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("AddSystemLocks, runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));

        }

        /// <summary>
        /// Call by Eval and Select View.  Builds/formats the list of session locks.
        /// </summary>
        public void AddSessionLocks()
        {
            if (GlobalLocks != null)
            {
                AddSessionLocks(GlobalLocks, null, null);
            }
        }

        /// <summary>
        /// Called by the UI Add Lock toolbar/menu/right-click buttons.  Builds a list of user locks.
        /// </summary>
        /// <param name="globalLockMngr"></param>
        /// <param name="sheetHash"></param>
        /// <param name="sheetName"></param>
        public void AddSessionLocks(LockMngr globalLockMngr, string sheetHash, string sheetName)
        {
            _stopwatch = Stopwatch.StartNew();

            //Do the First half of the Session Locks
            //This is broken up like this so garbage collection can occur 
            //on this method (which can create a large amount of temp Intersections)
            Range rangeToFormat = AddSessionLocksPrivate();
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;

            Stopwatch sw1 = Stopwatch.StartNew();
            if (debugEnabled) PafApp.GetLogger().DebugFormat("Memory Check 4: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            if (rangeToFormat != null && rangeToFormat.CellCount > 0)
            {
                PruneDependencies(false);
            }
            if (debugEnabled) PafApp.GetLogger().Debug("Memory Check 5: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            sw1.Stop();
            if (debugEnabled) PafApp.GetLogger().DebugFormat("AddSessionLock (loop3-PruneDep) runtime: {0} (ms)", new object[] { sw1.ElapsedMilliseconds.ToString("N0") });

            sw1.Reset();
            sw1.Start();
            if (rangeToFormat != null && rangeToFormat.CellCount > 0)
            {
                if (!String.IsNullOrEmpty(sheetHash) && !String.IsNullOrEmpty(sheetName))
                {
                    BuildProtectedCellsRange();
                    if (ProtectedRange != null && ProtectedRange.CellCount > 0)
                    {
                        PafApp.GetViewMngr().CurrentGrid.SetProtectedFormats(ProtectedRange,PafApp.GetPafAppSettingsHelper().GetProtectedColor());
                    }

                    Color userLockClr = PafApp.GetPafAppSettingsHelper().GetSessionLockColor();
                    PafApp.GetViewMngr().CurrentGrid.SetProtectedFormats(rangeToFormat, userLockClr);
                }
                else
                {
                    PafApp.GetViewMngr().CurrentOlapView.SessionLockProtectedRange = rangeToFormat;
                }
            }
            PafApp.GetLogger().InfoFormat("AddSessionLock Formatting runtime: {0} (ms)", new object[] { sw1.ElapsedMilliseconds.ToString("N0") });


            //If their is someone listening for the Session Lock event, then fire it, if not just continue.
            if (SessionLockedCell != null && !String.IsNullOrEmpty(sheetName) && !String.IsNullOrEmpty(sheetHash))
            {
                SessionLockedCell(this, new SessionLockedCellEventArgs(
                    sheetName,
                    sheetHash,
                    globalLockMngr.AllUserSessionParentLocks(),
                    new ProtectionMngrListTracker(
                        new List<Intersection>(ProtectedCellsSet.ToIntersections()),
                        new List<Intersection>(Changes),
                        new List<Intersection>(UserLocks),
                        new List<Intersection>(UnLockedChanges),
                        new List<Intersection>(Replication.AllocateAllCells),
                        new List<Intersection>(Replication.AllocateExistingCells),
                        new List<Intersection>(Lift.AllocateAllCells),
                        new List<Intersection>(Lift.AllocateExistingCells),
                        new List<Intersection>(GlobalLocks.AllUserSessionParentLocks().ToIntersections()))));
            }

            _stopwatch.Stop();
            ProtectionMgnrStatus();
            if (debugEnabled) PafApp.GetLogger().Debug("Memory Check 6: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            PafApp.GetLogger().InfoFormat("AddSessionLocks runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
        }


        /// <summary>
        /// Called by the UI Add Lock toolbar/menu/right-click buttons.  Builds a list of user locks.
        /// </summary>
        private Range AddSessionLocksPrivate()
        {
            if (GlobalLocks == null) return null;
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;
            //Create a hashset of changedCell, this improves the lookup over a queue
            HashSet<int> changedCells = new HashSet<int>(ChangeCells.Cast<Intersection>().ToList().Select(x => x.UniqueID));
            //Create a list to hold the intersecstion that aren't found on the view.
            List<Intersection> notFound = new List<Intersection>();
            //Get the measure dim off the cacheblock
            string measureDim = PafApp.CacheBlock.mdbDef.measureDim;
            //Create the Range that will be formatted.
            List<CellAddress> formatCells = new List<CellAddress>();
            //Dictionary to hold CellAddress/Intersection Combination
            Dictionary<CellAddress, Intersection> temp = new Dictionary<CellAddress, Intersection>(GlobalLocks.Count());
            

            if(debugEnabled) PafApp.GetLogger().Debug("Memory Check 1: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");

            //PafApp.GetLogger().Info("All Session Locks Size: " + GlobalLocks.AllSessionLocks().Count);
            //PafApp.GetLogger().Info("ProtectedCells Size: " + ProtectedCells.Count);
            //PafApp.GetLogger().Info("SystemLocks Size: " + SystemLocks.Count);
            //PafApp.GetLogger().Info("SessionLocks Size: " + SessionLocks.Count);

            Stopwatch sw1 = Stopwatch.StartNew();
            foreach(CoordinateSet cs in GlobalLocks.AllSessionLocks())
            {
                //PafApp.GetLogger().Info("Children in parent session lock: " + cs.GetCoordinates().Count);
                foreach (Coordinate c in cs.GetCoordinates())
                {
                    //Intersection intersection = new Intersection(cs.Dimensions, c.Coordinates);
                    //string measureMember = intersection.GetCoordinate(measureDim);
                    //MeasureDef md = MeasureDefs[measureMember];
                    //if (md == null || md.RecalcTbOveride != MeasureType.Aggregate)
                    //{
                    //    PafApp.GetLogger()
                    //          .DebugFormat("AddSessionLock, droping metric: {0} (ms)", new object[] {measureMember});
                    //    continue;
                    //}

                    int uniqueId = c.UniqueId;
                    //the cell is not locked or protected, get the celladdress
                    if ((!ProtectedCells.Contains(uniqueId)) &&
                        (!SystemLocks.Contains(uniqueId)) &&
                        (!SessionLocks.Contains(uniqueId)))
                    {
                        Intersection intersection = new Intersection(cs.Dimensions, c.Coordinates);

                        //Store the locked cell addresses to be formatted
                        List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);
                        if (cellAdresses != null)
                        {
                            formatCells.AddRange(cellAdresses);
                            foreach (CellAddress t in cellAdresses)
                            {
                                //Add the locked cells to the ChangedCellMngr
                                //rangeToFormat.AddContiguousRange(t);
                                if (!temp.ContainsKey(t))
                                {
                                    temp.Add(t, intersection);
                                }
                            }
                        }
                        else
                        {
                            notFound.Add(intersection);
                        }
                    }
                }
            }
            sw1.Stop();
            if (debugEnabled) PafApp.GetLogger().DebugFormat("AddSessionLock (loop1) runtime: {0} (ms)", new object[] { sw1.ElapsedMilliseconds.ToString("N0") });
            if (debugEnabled) PafApp.GetLogger().Debug("Memory Check 2: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");

            sw1 = Stopwatch.StartNew();
            var cells = temp.Keys.ToList();
            //PafApp.GetLogger().Info("Cells list size: " + cells.Count);
            //PafApp.GetLogger().Info("Not Found list size: " + notFound.Count);
            if (cells.Count > 0)
            {
                cells.Sort();
                foreach (CellAddress cell in cells)
                {
                    Intersection intersection = temp[cell];
                    int uniqueId = intersection.UniqueID;
                    //the cell is not locked or protected
                    if ((!ProtectedCells.Contains(uniqueId)) &&
                        (!SystemLocks.Contains(uniqueId)) &&
                        (!SessionLocks.Contains(uniqueId)))
                    {
                        SessionLocks.Add(uniqueId);

                        //User Locks are added to change queu which is processed in ClearQueus
                        //Protected changes are also added to change queu in the PruneDependencies method
                        //User entry changes are also added to this queu in the HandleChangedCells method
                        if (!changedCells.Contains(uniqueId))
                        {
                            ChangeCells.Enqueue(intersection);
                            changedCells.Add(uniqueId);
                        }
                    }
                    temp.Remove(cell);
                }
                //PafApp.GetLogger().Info("pre prune: " + SessionLocks.Count);
                //DumpProtectionMgnrStatus();
                PruneDependencies(false);
                //PafApp.GetLogger().Info("post prune: " + SessionLocks.Count);
                //DumpProtectionMgnrStatus();
            }
            sw1.Stop();
            if (debugEnabled) PafApp.GetLogger().DebugFormat("AddSessionLock (loop2) runtime: {0} (ms)", new object[] { sw1.ElapsedMilliseconds.ToString("N0") });
            if (debugEnabled) PafApp.GetLogger().Debug("Memory Check 3: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");

            sw1 = Stopwatch.StartNew();
            foreach (Intersection intersection in notFound)
            {
                int uniqueId = intersection.UniqueID;
                if ((!ProtectedCells.Contains(uniqueId)) &&
                    (!SystemLocks.Contains(uniqueId)) &&
                    (!SessionLocks.Contains(uniqueId)))
                {
                    SessionLocks.Add(uniqueId);

                    if (!changedCells.Contains(uniqueId))
                    {
                        ChangeCells.Enqueue(intersection);
                        changedCells.Add(uniqueId);
                    }
                }
            }
            //PafApp.GetLogger().Info("session locks list size: " + SessionLocks.Count);
            //PafApp.GetLogger().Info("ProtectedCells Size: " + ProtectedCells.Count);
            //PafApp.GetLogger().Info("ProtectedRules Size: " + ProtectedRules.Count);
            //PafApp.GetLogger().Info("SystemLocks Size: " + SystemLocks.Count);
            //PafApp.GetLogger().Info("SessionLocks Size: " + SessionLocks.Count);
            sw1.Stop();
            if (debugEnabled) PafApp.GetLogger().DebugFormat("AddSessionLock (loop3) runtime: {0} (ms)", new object[] { sw1.ElapsedMilliseconds.ToString("N0") });

            cells.Sort();

            return cells.GetRange();
        }
    
        /// <summary>
        /// Called by the UI Add Lock toolbar/menu/right-click buttons.  Builds a list of user locks.
        /// </summary>
        /// <param name="contigRange"></param>
        /// <param name="sheetHash"></param>
        /// <param name="sheetName"></param>
        public void AddUserLock(ContiguousRange contigRange, string sheetHash, string sheetName)
        {
            if (contigRange == null) return;
            if (GlobalLocks == null) GlobalLocks = new LockMngr();
            foreach (Cell cell in contigRange.Cells)
            {
                if (PafApp.GetViewMngr().CurrentOlapView.DataRange.InContiguousRange(cell))   
                {
                    Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell.CellAddress);

                    Cell adjustedCell = PafApp.GetViewMngr().CurrentOlapView.AdjustCellAddress(cell);

                    //the cell is not a blank
                    if (!PafApp.GetViewMngr().CurrentOlapView.RowBlanks.Contains(adjustedCell.CellAddress.Row) &&
                        !PafApp.GetViewMngr().CurrentOlapView.ColBlanks.Contains(adjustedCell.CellAddress.Col))
                    {
                        int uniqueId = intersection.UniqueID;
                        //the cell is not locked or protected
                        if ((!ProtectedCells.Contains(uniqueId))
                            && (!SystemLocks.Contains(uniqueId))
                            && (!UserLocks.Contains(intersection)))
                        {
                            //Store the locked cell addresses to be formatted
                            List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);
                            if (cellAdresses != null)
                            {
                                for (int i = 0; i < cellAdresses.Count; i++)
                                {
                                    //Add the locked cells to the ChangedCellMngr
                                    PafApp.GetChangedCellMngr()
                                          .AddCell(sheetHash,
                                                   new ChangedCellInfo(sheetHash, cellAdresses[i], Change.Locked));

                                    //Set the color for the user locked cell
                                    Color userLockClr = PafApp.GetPafAppSettingsHelper().GetUserLockColor();
                                    Range range = new Range(cellAdresses[i]);
                                    PafApp.GetViewMngr().CurrentGrid.SetProtectedFormats(range, userLockClr);
                                }
                            }
                            //Add the new user locked cell to the list of user locked cells
                            if (!UserLocks.Contains(intersection))
                            {
                                UserLocks.Add(intersection);
                            }

                            //User Locks are added to change queu which is processed in ClearQueus
                            //Protected changes are also added to change queu in the PruneDependencies method
                            //User entry changes are also added to this queu in the HandleChangedCells method
                            if (!ChangeCells.Contains(intersection))
                            {
                                ChangeCells.Enqueue(intersection);
                            }

                            PruneDependencies();

                            //If their is someone listening for the UserLock event, then fire it, if not just continue.
                            if (UserLockedCell != null)
                            {
                                UserLockedCell(this, new UserLockedCellEventArgs(
                                    sheetName,
                                    sheetHash,
                                    cell,
                                    new ProtectionMngrListTracker(
                                        new List<Intersection>(ProtectedCellsSet.ToIntersections()),
                                        new List<Intersection>(Changes),
                                        new List<Intersection>(UserLocks),
                                        new List<Intersection>(UnLockedChanges),
                                        new List<Intersection>(Replication.AllocateAllCells),
                                        new List<Intersection>(Replication.AllocateExistingCells),
                                        new List<Intersection>(Lift.AllocateAllCells),
                                        new List<Intersection>(Lift.AllocateExistingCells),
                                        new List<Intersection>(GlobalLocks.AllUserSessionParentLocks().ToIntersections()))));
                            }
                        }
                    }
                }
            }
        }

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="cell"></param>
        ///// <param name="sheet"></param>
        //public void RemoveUserLock(Cell cell, string sheet)
        //{
        //    Intersection intersection = PafApp.GetViewMngr().GetOlapView(sheet).
        //        FindIntersection(cell.CellAddress);

        //    if (PafApp.GetChangedCellMngr().SheetCellTracker[sheet].ContainsKey(cell.CellAddress))
        //    {
        //        //Restore the interior cell Color prior to the lock
        //        ChangedCellInfo changedCell = PafApp.GetChangedCellMngr().SheetCellTracker[sheet][cell.CellAddress].Pop();
        //        Color userLockClr = ColorTranslator.FromHtml(changedCell.CellFormat.BgFillColor);
        //        PafApp.GetViewMngr().GetGrid(sheet).SetBgFillColor(cell.CellAddress.Row, cell.CellAddress.Col, userLockClr);
        //        ContiguousRange contigRange = new ContiguousRange(cell.CellAddress);
        //        PafApp.GetViewMngr().GetGrid(sheet).UnLockCells(contigRange);

        //        //Remove the user lock from the Changed Cell Manager
        //        PafApp.GetChangedCellMngr().SheetCellTracker[sheet].Remove(cell.CellAddress);
        //    }

        //    //Restore the Data Cache with 0 for the unlocked intersection
        //    DataCache.setCellValue(intersection.Coordinates, 0);

        //    //Remove the user locked cell from the list of user locked cells
        //    if (this.UserLocks.Contains(intersection))
        //        this.UserLocks.Remove(intersection);

        //    ////Remove the user locked cell from the list of locked cells
        //    //if (this.ViewLocks.ContainsKey(intersection))
        //    //    this.ViewLocks.Remove(intersection);

        //}

        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="cell"></param>
        ///// <param name="sheet"></param>
        //public void RemoveAllUserLocks(Cell cell, string sheet)
        //{
        //    for (int i = this.UserLocks.Count - 1; i >= 0; i--)
        //    {
        //        CellAddress cellAddr = PafApp.GetViewMngr().GetOlapView(sheet).FindIntersectionAddress(this.UserLocks[i]);

        //        if (PafApp.GetChangedCellMngr().SheetCellTracker[sheet].ContainsKey(cellAddr))
        //        {
        //            //Restore the interior cell Color prior to the lock
        //            ChangedCellInfo changedCell = PafApp.GetChangedCellMngr().SheetCellTracker[sheet][cellAddr].Pop();
        //            Color userLockClr = ColorTranslator.FromHtml(changedCell.CellFormat.BgFillColor);
        //            PafApp.GetViewMngr().GetGrid(sheet).SetBgFillColor(cellAddr.Row, cellAddr.Col, userLockClr);
        //            ContiguousRange contigRange = new ContiguousRange(cellAddr);
        //            PafApp.GetViewMngr().GetGrid(sheet).UnLockCells(contigRange);
                    
        //            //Remove the user lock from the Changed Cell Manager
        //            PafApp.GetChangedCellMngr().SheetCellTracker[sheet].Remove(cellAddr);
        //        }

        //        //Restore the Data Cache with 0 for the unlocked intersection
        //        DataCache.setCellValue(this.UserLocks[i].Coordinates, 0);

        //        //Remove the user locked cell from the list of user locked cells
        //        this.UserLocks.Remove(this.UserLocks[i]);

        //        ////Remove the user locked cell from the list of locked cells
        //        //if (this.ViewLocks.ContainsKey(this.UserLocks[i]))
        //        //    this.ViewLocks.Remove(this.UserLocks[i]);
        //    }
        //}

        /// <summary>
        /// Checks to see if a cell has changed.
        /// </summary>
        /// <param name="cell">The cell for a change.</param>
        /// <returns>true if the cell is changed, false if not.</returns>
        public bool IsCellChanged(CellAddress cell)
        {
            if (cell != null)
            {
                Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);

                if (intersection != null)
                {
                    return (Changes.Contains(intersection));
                }
            }
            return false;
        }

        /// <summary>
        /// Adds each cell intersection changed by the user in the grid to the ChangeCells queue for
        /// processing.  The intersection is also added to a list of changed cells that is sent to the
        /// mid-tier.  Also, the intersection is added to a Range object for formatting.
        /// </summary>
        /// <param name="cell">Cell address.</param>
        /// <param name="sheetHash">Hash code of the sheet.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <param name="ignoreChanges">Don't add the cell change to the list of changed cells.</param>
        public void HandleChangedCells(CellAddress cell, string sheetHash, string sheetName, bool ignoreChanges)
        {
            _stopwatch = Stopwatch.StartNew();
            //Process the changed cell
            Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);

            bool isAlreadyInChanged = true;

            if (PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape && !ignoreChanges)
            {
                if (!UnLockedChanges.Contains(intersection))
                {
                    //Do not add "pasteshape" intersections to the UserLocks list.  The UserLocks
                    //list is passed back to the mid-tier for use in evaluation.
                    //Also, do not add "pasteshape" to the ChangeCells queue for protection processing.
                    UnLockedChanges.Add(intersection);

                    //Intersection added to Changes
                    isAlreadyInChanged = false;
                }

                //Store the changed intersections to be formatted
                List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);
                if (cellAdresses != null)
                {
                    for (int i = 0; i < cellAdresses.Count; i++)
                    {
                        //Format changed cells only once
                        if (isAlreadyInChanged == false && !Changes.Contains(intersection))
                        {
                            ChangedRange.AddContiguousRange(cellAdresses[i]);

                            Format format = PafApp.GetFormattingMngr().GetDefaultCellFormat(cell);
                         
                            bool plannable =(!(ProtectCells.Contains(intersection) || UserLocks.Contains(intersection) || GlobalLocks.ContainsIntersection(intersection)));

                            format.Plannable = plannable.ToString();

                            //Add the changed cells to the ChangedCellMngr
                            PafApp.GetChangedCellMngr().AddCell(
                                sheetHash,
                                new ChangedCellInfo(sheetHash, sheetName, cellAdresses[i], intersection, 0,
                                                    Change.UserChanged, format));
                        }

                        //If an intersection exists multiple times in a view section, copy the value at the changed cell address
                        //to the other cell addresses.  I am not using an array copy here because I don't think it is needed - this
                        //may need to be changed.
                        if (!cellAdresses[i].Equals(cell))
                        {
                            PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);
                            PafApp.GetViewMngr().CurrentGrid.CopyCellValue(cell, cellAdresses[i]);
                            PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                        }
                    }
                }
            }
            else
            {
                if (!Changes.Contains(intersection) && !ignoreChanges)
                {
                    //Changes passed to middle tier
                    Changes.Add(intersection);

                    //Changed cells are added to change queu which is processed in ClearQueus
                    //Protected changes are also added to change queu in the PruneDependencies method
                    //User Locks are also added to this queu in the AddUserLock method
                    if (!ChangeCells.Contains(intersection))
                    {
                        ChangeCells.Enqueue(intersection);
                    }

                    //Intersection added to Changes
                    isAlreadyInChanged = false;
                }

                //Store the changed intersections to be formatted
                List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);
                if (cellAdresses != null)
                {
                    for (int i = 0; i < cellAdresses.Count; i++)
                    {
                        //Format changed cells only once
                        if (!isAlreadyInChanged && !UnLockedChanges.Contains(intersection) && !ignoreChanges)
                        {
                            ChangedRange.AddContiguousRange(cellAdresses[i]);

                            //Add the changed cells to the ChangedCellMngr
                            PafApp.GetChangedCellMngr().AddCell(sheetHash,
                                                                new ChangedCellInfo(sheetHash, sheetName,
                                                                                    cellAdresses[i], 0,
                                                                                    Change.UserChanged));
                        }

                        //If an intersection exists multiple times in a view section, copy the value at the changed cell address
                        //to the other cell addresses.  I am not using an array copy here because I don't think it is needed - this
                        //may need to be changed.
                        if (!cellAdresses[i].Equals(cell))
                        {
                            PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);
                            PafApp.GetViewMngr().CurrentGrid.CopyCellValue(cell, cellAdresses[i]);
                            PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                        }
                    }
                }
            }
            _stopwatch.Stop();
            if(_stopwatch.ElapsedMilliseconds > 30) PafApp.GetLogger().InfoFormat("Long function call - HandleChangedCells runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));

        }

        /// <summary>
        /// Handles the changes for the member tags.
        /// </summary>
        /// <param name="cell">Cell address.</param>
        /// <param name="sheetHash">Hash code of the sheet.</param>
        /// <param name="sheetName">Name of the sheet.</param>
        /// <param name="mbrTagName">Name of the member tag.</param>
        /// <param name="copyFormula">Copy the formula instead of the value.</param>
        public void HandleChangedMemberTagCells(CellAddress cell, string sheetHash, 
            string sheetName, string mbrTagName, bool copyFormula)
        {
            _stopwatch = Stopwatch.StartNew();
            //Process the changed cell
            Intersection intersection = PafApp.GetViewMngr().CurrentOlapView.FindIntersection(cell);

            //Store the changed intersections to be formatted
            List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.MemberTag.FindMemberTagIntersectionAddress(intersection, mbrTagName);
            if (cellAdresses != null)
            {
                for (int i = 0; i < cellAdresses.Count; i++)
                {
                    //If an intersection exists multiple times in a view section, copy the value at the changed cell address
                    //to the other cell addresses.  I am not using an array copy here because I don't think it is needed - this
                    //may need to be changed.
                    if (!cellAdresses[i].Equals(cell))
                    {
                        PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);
                        if (copyFormula)
                        {
                            PafApp.GetViewMngr().CurrentGrid.CopyCellFormula(cell, cellAdresses[i]);
                        }
                        else
                        {
                            PafApp.GetViewMngr().CurrentGrid.CopyCellValue(cell, cellAdresses[i]);
                        }
                        PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                    }
                }
            }
            _stopwatch.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("HandleChangedMemberTagCells runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
        }


        /// <summary>
        /// Prune dependencies for changed cells and determine intersections to protect
        /// </summary>
        public void PruneDependencies()
        {
            PruneDependencies(true);
        }

        /// <summary>
        /// Prune dependencies for changed cells and determine intersections to protect
        /// </summary>
        /// <param name="addChangesToManager">True to add the cells to the ChangedCellMngr()</param>
        private void PruneDependencies(bool addChangesToManager)
        {
            _stopwatch = Stopwatch.StartNew();
            int ruleSetId = 0;
            bool isTimeBalMeasureinQueue = false;
            //For each dimension in the intersection ...
            string timeDim = PafApp.CacheBlock.mdbDef.timeDim;
            string measureDim = PafApp.CacheBlock.mdbDef.measureDim;
            string yearDim = PafApp.CacheBlock.mdbDef.yearDim;
            //Create a hashset to hold/query the changed cell.  This is much faster than looking into the queue
            HashSet<int> changedCells = new HashSet<int>(ChangeCells.Cast<Intersection>().ToList().Select(x => x.UniqueID));
            //Get the List of RuleSets
            List<RuleSet> ruleSets = GetRuleSets();
            //View Members
            Dictionary<string, string> viewMembers = PafApp.GetViewMngr().CurrentOlapView.ViewMembers;

            InitDependencies(ruleSets);

            _changedIntersections = _changedIntersections.Resize(ChangeCells.Count);

            while (ChangeCells.Count > 0)
            {
                Intersection changeIntersection = (Intersection)ChangeCells.Dequeue();
                int uniqueId = changeIntersection.UniqueID;
                changedCells.Remove(uniqueId);

                //Populate the Data Cache with change flags for each changed intersection
                //DataCache.setCellValue(changeIntersection.Coordinates, 1);
                //_changedIntersections.Add(changeIntersection.UniqueString);
                _changedIntersections.Add(uniqueId);
                //_changedIntersection.Add(changeIntersection.UniqueString, true);

                AddDependencies(changeIntersection, ruleSets);

                string[] coordinates = changeIntersection.Coordinates;
                string[] axissequence = changeIntersection.AxisSequence;

                for (int i = 0; i < axissequence.Length; i++)
                {
                    string dimName = axissequence[i];
                    if (dimName == yearDim) continue;

                    string coordinate = coordinates[i];
                    TimeSlice timeSlice = new TimeSlice(changeIntersection, timeDim, yearDim);
                    //bool isTime = false;
                    //if (dimName == timeDim)
                    //{
                    //    isTime = true;
                    //    coordinate = TimeSlice.TranslateTimeYearIs(changeIntersection, timeDim, yearDim);
                    //    dimName = PafAppConstants.TIME_HORIZON_DIM;
                    //}


                    Dictionary<string, Dictionary<int, Dictionary<int, List<string>>>> dimDependencies;
                    Dictionary<int, Dictionary<int, List<string>>> axisCoordDependencies;

                    //If the dimension is Time, then we must use the Time Horizon dimesnsion.
                    string tempDimName = dimName;
                    if (tempDimName == timeDim)
                    { 
                        tempDimName = PafAppConstants.TIME_HORIZON_DIM;
                        coordinate = changeIntersection.TranslateTimeYearIs(timeDim, yearDim);
                    }
                    //if the dim, and coordinate is not in the dependency list, then go to the next dim.
                    if (!Dependencies.TryGetValue(tempDimName, out dimDependencies))
                    {
                        continue;
                    }
                    if (!dimDependencies.TryGetValue(coordinate, out axisCoordDependencies))
                    {
                        continue;
                    }

                    if (dimName.EqualsIgnoreCase(timeDim))
                    {
                        MeasureDef measDef;
                        if (MeasureDefs.TryGetValue(changeIntersection.GetCoordinate(measureDim), out measDef))
                        {
                            #region MeasureDef
                            // MeasureDef measDef = MeasureDefs[changeIntersection.GetCoordinate(measureDim)];
                            //There are 3 rule sets.  Use only one rule set based on the following Rules:
                            //Type 0 - Aggregate and Recalc (and not defined in paf_measures)
                            //Type 5 - Time Balance First
                            //Type 6 - Time Balance Last
                            if (measDef.Type == MeasureType.Aggregate || measDef.Type == MeasureType.None)
                            {
                                // No time protection should be applied to aggregate/recalc measures after a time balance measure
                                // has been detected as part of a protection transaction
                                if (isTimeBalMeasureinQueue)
                                {
                                    continue;
                                }

                                ruleSetId = 0;
                            }
                            else if (measDef.Type == MeasureType.Recalc)
                            {
                                if (measDef.RecalcTbOveride == MeasureType.Aggregate || measDef.Type == MeasureType.None)
                                {
                                    // No time protection should be applied to aggregate/recalc measures after a time balance measure
                                    // has been detected as part of a protection transaction
                                    if (isTimeBalMeasureinQueue)
                                    {
                                        continue;
                                    }

                                    ruleSetId = 0;
                                }
                                else if (measDef.RecalcTbOveride == MeasureType.TimeBalFirst)
                                {
                                    ruleSetId = 5;
                                    isTimeBalMeasureinQueue = true;
                                }
                                else if (measDef.RecalcTbOveride == MeasureType.TimeBalLast)
                                {
                                    ruleSetId = 6;
                                    isTimeBalMeasureinQueue = true;
                                }
                            }
                            else if (measDef.Type == MeasureType.TimeBalFirst)
                            {
                                ruleSetId = 5;
                                isTimeBalMeasureinQueue = true;
                            }
                            else if (measDef.Type == MeasureType.TimeBalLast)
                            {
                                ruleSetId = 6;
                                isTimeBalMeasureinQueue = true;
                            }
                            #endregion MeasureDef
                        }
                        //Measure not found in MeasureDefs
                        else
                        {
                            ruleSetId = 0;
                        }
                    }
                    else if (dimName.EqualsIgnoreCase(measureDim))
                    {
                        ruleSetId = 2;
                    }
                    else
                    {
                        ruleSetId = 0;
                    }


                    Dictionary<int, List<string>> axisCoordDependenciesRuleSet;
                    //if (axisCoordinates.ContainsKey(ruleSetId))
                    if (axisCoordDependencies.TryGetValue(ruleSetId, out axisCoordDependenciesRuleSet))
                    {
                        foreach (KeyValuePair<int, List<string>> memberDependency in axisCoordDependenciesRuleSet)
                        {
                            //Get the corresponding rule for the dependency List
                            //memberDependency.Key is an integer counter that maps back to _Rules
                            RuleInfo ruleInfo = _rules[memberDependency.Key];
                            RuleSet ruleSet = ruleSets[ruleInfo.RuleSetIndex];
                            RuleGroup ruleGroup = ruleSet.RuleGroups[ruleInfo.RuleGroupIndex];
                            Rule changedInterRule = ruleGroup.Rules[coordinate];

                            int zeroCounter = 0;
                            string unlockedDependency = "";
                            string offsetMember;
                            bool isOutsideUow;
                            foreach (string dependency in memberDependency.Value)
                            {
                                //Set the dependency coordinates
                                //string[] newCoordinates = SetCoordinate(axissequence, coordinates, dimName, dependency);

                                //if this is the time dim, then set the coordinate value to the Time Slice,
                                //if not then get the coordinate from the Intersection
                                string coordValue = dimName == timeDim ?
                                    timeSlice.ToString() : changeIntersection.GetCoordinate(dimName);

                                //If the Coordinate Value equals the dependency, then go to the next dependency.
                                if (coordValue == dependency) continue;

                                //If the dimension is Time, then we must create a TimeSlice,
                                //then do the replacements to create the UniqueString
                                string newUniqueString;
                                if (dimName == timeDim)
                                {
                                    //Convert the dependency to a TimeSlice
                                    //Then do two seperate Replacements (Time, Year)
                                    TimeSlice depTimeSlice = new TimeSlice(dependency);

                                    newUniqueString = changeIntersection.UniqueString.Replace(timeSlice.Year, depTimeSlice.Year);
                                    newUniqueString = newUniqueString.Replace(timeSlice.Period, depTimeSlice.Period);
                                }
                                else
                                {
                                    newUniqueString = changeIntersection.UniqueString.Replace(coordValue, dependency);
                                }

                                isOutsideUow = false;
                                //Handle Perpetual Inventory Rule Groups
                                if (changedInterRule.Function == FunctionType.AtPrev || changedInterRule.Function == FunctionType.AtNext)
                                {
                                    Rule dependencyRule = ruleGroup.Rules[dependency];

                                    if (changedInterRule.Term != dependencyRule.Term)
                                    {

                                        GetPeriodOffset(changedInterRule, changeIntersection, out offsetMember, out isOutsideUow);

                                        //An offset member was found
                                        if (offsetMember != String.Empty)
                                        {
                                            string innerDimName = changedInterRule.Params[1].ToString();
                                            //if dim is time, then offsetMember == "FY2006||Q1" fix it.
                                            if (innerDimName == timeDim)
                                            {
                                                TimeSlice offsetTimeSlice = new TimeSlice(offsetMember);

                                                string yearCoord = changeIntersection.GetCoordinate(yearDim);
                                                string timeCoord = changeIntersection.GetCoordinate(timeDim);

                                                newUniqueString = newUniqueString.Replace(yearCoord, offsetTimeSlice.Year);
                                                newUniqueString = newUniqueString.Replace(timeCoord, offsetTimeSlice.Period);
                                            }
                                            else
                                            {
                                                //newCoordinates = SetCoordinate(axissequence, newCoordinates, changedInterRule.Params[1].ToString(), offsetMember);
                                                coordValue = changeIntersection.GetCoordinate(innerDimName);
                                                newUniqueString = newUniqueString.Replace(coordValue, offsetMember);
                                            }
                                        }
                                    }
                                }

                                //if the new coordinate is outside of the UOW or not locked (protected/changed)
                                //Intersection temp = new Intersection(axissequence, newCoordinates);
                                //bool changed = _changedIntersections.Contains(temp.UniqueString);
                                //bool changed = _changedIntersections.Contains(newUniqueString);
                                //bool changed = _changedIntersection.ContainsKey(newUniqueString);
                                //if (isOutsideUow || !_changedIntersections.Contains(newUniqueString))
                                if (isOutsideUow || !_changedIntersections.Contains(newUniqueString.GetHashCode()))
                                {
                                    zeroCounter++;
                                    if (zeroCounter > 1)
                                    {
                                        break;
                                    }
                                    else
                                    {
                                        //the only dependency that is not locked
                                        unlockedDependency = dependency;
                                    }
                                }
                            }

                            //When only a single dependency is left, protect the cell with the same dimensionality as the 
                            //changed cell except the dimension member of the changed cell is replaced with the dependency member...
                            if (zeroCounter == 1)
                            {
                                Intersection changeIntersection2;

                                //Filter out off-screen dependencies except for Times and Measures dims
                                if (dimName.EqualsIgnoreCase(measureDim) ||
                                    dimName.EqualsIgnoreCase(timeDim) ||
                                    viewMembers.ContainsKey(unlockedDependency))
                                {
                                    changeIntersection2 = changeIntersection.DeepClone();

                                    if (dimName == timeDim)
                                    {
                                        changeIntersection2.ApplyTimeHorizonCoord(unlockedDependency, timeDim, yearDim);
                                    }
                                    else
                                    {
                                        changeIntersection2.SetCoordinate(dimName, unlockedDependency);
                                    }

                                    //Handle complex rules with formulas on the left side of the equals
                                    //Handle Perpetual Inventory Rule Groups
                                    if (changedInterRule.Function == FunctionType.AtPrev || changedInterRule.Function == FunctionType.AtNext)
                                    {

                                        GetPeriodOffset(changedInterRule, changeIntersection, out offsetMember, out isOutsideUow);

                                        //if next/prev formula does exist, but no offset member is found in the UOW then continue
                                        if (isOutsideUow)
                                        {
                                            continue;
                                        }

                                        //An offset member was found
                                        if (offsetMember != String.Empty)
                                        {
                                            string innerDimName = changedInterRule.Params[1].ToString();
                                            //if dim is time, then offsetMember == "FY2006||Q1" fix it.
                                            if (innerDimName == timeDim)
                                            {
                                                changeIntersection2.ApplyTimeHorizonCoord(offsetMember, timeDim, yearDim);
                                            }
                                            else
                                            {
                                                changeIntersection2.SetCoordinate(innerDimName, offsetMember);
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    continue;
                                }

                                int uniqueId2 = changeIntersection2.UniqueID;
                                if ((!ProtectedCells.Contains(uniqueId2)) &&
                                    (!SystemLocks.Contains(uniqueId2)) &&
                                    (!UserLocks.Contains(changeIntersection2)) )
                                    //&& (!SessionLocks.Contains(changeIntersection2.UniqueID)))
                                {
                                    //Protect cells are added to the protected cells queu which is processed in ClearQueus
                                    ProtectCells.Enqueue(changeIntersection2);
                                    //Protected cells are passed to middle tier
                                    //ProtectedCells.Add(changeIntersection2.UniqueID, changeIntersection2);
                                    ProtectedCells.Add(uniqueId2);
                                    ProtectedCellsSet.AddCoordinate(changeIntersection2);
                                    //Protected changes are added to the change queu which is processed in ClearQueus
                                    //User entry changes are also added to this queu in the HandleChangedCells method
                                    //User Locks are also added to this queu in the AddUserLock method
                                    if (!changedCells.Contains(uniqueId2))
                                    {
                                        ChangeCells.Enqueue(changeIntersection2);
                                        changedCells.Add(uniqueId2);
                                    }


                                    //Capture the rule that causes the protection
                                    Rule protectedInterRule = ruleGroup.Rules[unlockedDependency];
                                    ProtectedRules.Add(protectedInterRule.Term + " = " + protectedInterRule.Expression);
                                }
                            }
                        }
                    }
                }
            }
            ClearQueues(addChangesToManager);
            _stopwatch.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("PruneDependencies runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
        }



        ///// <summary>
        ///// Initially protects intersections with single rule rulegroups
        ///// </summary>
        public void AddSingletonRules()
        {
            _stopwatch = Stopwatch.StartNew();
            Range range = new Range();
            List<RuleSet> ruleSets = GetRuleSets();
            List<CellAddress> cells = new List<CellAddress>(); 
            HashSet<string> cellSet = new HashSet<string>();

            foreach (RuleSet ruleSet in ruleSets)
            {
                //Filter out all but the user selected Measure Rule Set
                if (ruleSet.Dimension.EqualsIgnoreCase(PafApp.CacheBlock.mdbDef.measureDim))
                {
                    if (!ruleSet.Name.EqualsIgnoreCase(PafApp.RuleSetName))
                    {
                        continue;
                    }
                }
                
                foreach (RuleGroup ruleGroup in ruleSet.RuleGroups)
                {
                    if (ruleGroup.Rules.Count == 1)
                    {
                        IDictionaryEnumerator enumerator1 = ruleGroup.Rules.GetEnumerator();
                        enumerator1.MoveNext();
                        string term = ruleGroup.Rules[enumerator1.Key.ToString()].Term;
                        List<Intersection> intersections = PafApp.GetViewMngr().CurrentOlapView.FindMemberIntersections(term);

                        
                        foreach (Intersection intersection in intersections)
                        {
                            //Populate the Data Cache with change flags for each changed intersection
                            //DataCache.setCellValue(intersection.Coordinates, 1);
                            //_changedIntersections.Add(intersection.UniqueString);
                            _changedIntersections.Add(intersection.UniqueID);
                            //_changedIntersections.Add(intersection.UniqueString, true);

                            //Add the locked cell to the list of locked cells
                            if (!ProtectedCells.Contains(intersection.UniqueID))
                            {
                                //ProtectedCells.Add(intersection.UniqueID, intersection);
                                ProtectedCells.Add(intersection.UniqueID);
                                ProtectedCellsSet.AddCoordinate(intersection);
                            }

                            //Store the changed intersections to be formatted
                            List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);
                            if (cellAdresses != null)
                            {
                                foreach (CellAddress x in cellAdresses)
                                {
                                    if (!cellSet.Contains(x.ToString()))
                                    {
                                        cellSet.Add(x.ToString());
                                        cells.Add(x);
                                    }
                                }
                            }
                        }
                    }
                }   
            }

            if (cells.Count > 0)
            {
                cells.Sort();
                Range temp = CellAddress.GetRange(cells);
                if (temp != null && temp.CellCount > 0)
                {
                    range.AddContiguousRanges(temp.ContiguousRanges);
                    range.Sorted = true;
                }
            }
            if (range.ContiguousRanges.Count > 0)
            {
                PafApp.GetViewMngr().CurrentOlapView.SingletonRuleRange = range;
            }
            _stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("AddSingletonRules runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
        }


        /// <summary>
        /// 
        /// </summary>
        public void AddBOHProtection()
        {
            _stopwatch = Stopwatch.StartNew();
            //Get the active Version
            string planningVersion = PafApp.GetPafPlanSessionResponse().planningVersion;
            Dictionary<string, string> measureTypes = new Dictionary<string, string>();
            //Is the active version a Forward Plannable Version
            bool forwardPlannable = false;
            foreach (simpleVersionDef version in PafApp.CacheBlock.versionDefs)
            {
                if (version.name == planningVersion)
                {
                    if (version.type.EqualsIgnoreCase(ForwardPlannable))
                    {
                        forwardPlannable = true;
                    }
                    break;
                }
            }
            //Apply BOHProtectionj for the current year
            ApplyBohProtection(true, forwardPlannable, planningVersion);
            //Apply BOHProtection for non-current years
            ApplyBohProtection(false, forwardPlannable, planningVersion);

            _stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("AddBOHProtection runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
        }


        public void GetChangedAndLockedCells(bool attributeView, out simpleCoordList changedCells, out simpleCoordList lockedCells, out simpleCoordList[] sessionLockedCells)
        {
            _stopwatch = Stopwatch.StartNew();
            //Build locks list from the list of changes and the list of user locks
            HashSet<Intersection> locks = new HashSet<Intersection>();
            //Build session locks list
            HashSet<Intersection> sessionLocks = new HashSet<Intersection>();
            //Build changes list from the list of changes and the list of unlocked changes
            List<Intersection> allChanges = new List<Intersection>();
            sessionLockedCells = null;
            
            foreach (Intersection change in Changes)
            {
                if (!ProtectedCells.Contains(change.UniqueID))
                {
                    //Locks list
                    locks.Add(change);
                }

                //Changes List
                allChanges.Add(change);
            }

            //Session Locks
            string measureDimension = PafApp.CacheBlock.mdbDef.measureDim;
            string yearsDimension = PafApp.CacheBlock.mdbDef.yearDim;
            //string MeasureRoot = PafApp.CacheBlock.mdbDef.measureRoot;
            //if (GlobalLocks != null && GlobalLocks.AllUserSessionParentLocks() != null)
            LockMngr mngr = PafApp.GetViewMngr().GlobalLockMngr;
            if (mngr != null && mngr.AllUserSessionParentLocks() != null)
            {
                foreach (CoordinateSet cs in mngr.AllUserSessionParentLocks())
                {
                    List<Coordinate> coordinates = cs.GetCoordinates();
                    foreach (Coordinate coordinate in coordinates)
                    {
                        sessionLocks.UnionWith(GetFloorIntersections(measureDimension, yearsDimension, new Intersection(cs.Dimensions, coordinate.Coordinates)));
                    }
                }
            }

            //If a non attribute view, then join the two sets.
            if (!attributeView)
            {
                locks.UnionWith(sessionLocks);
            }
            //TTN-2478 - Set the sessionLockedCell no matter the view type.
            sessionLockedCells = sessionLocks.ToSimpleCoordListCollection(true).ToArray();
            //else
            //{
            //    sessionLockedCells = sessionLocks.ToSimpleCoordListCollection(true).ToArray();
            //}

            //UserLocks
            foreach (Intersection intersection in UserLocks)
            {
                locks.UnionWith(GetFloorIntersections(measureDimension, yearsDimension, intersection));
            }

            //Changes List
            foreach (Intersection intersection in
                UnLockedChanges.Where(intersection => !allChanges.Contains(intersection)))
            {
                allChanges.Add(intersection);
            }


            //Locked Cells
            lockedCells = locks.ToSimpleCoordList(false);
            

            //Reset the coordinates list.
            changedCells = allChanges.ToSimpleCoordList(false);

            _stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("GetChangedAndLockedCells runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
        }


        private IEnumerable<Intersection> GetFloorIntersections(string measureDimension, string yearsDimension, Intersection intersection)
        {
            string coord = intersection.GetCoordinate(measureDimension);
            List<Intersection> floorInter = null;
            //If the user selected the MeasureRoot, then blow out the intersections to the floor,
            //if not then check to see if the selected measure can be blown down.
            if (coord == measureDimension)
            {
                floorInter = intersection.ExpandToFloor(measureDimension, PafApp.SimpleDimTrees);
            }
            else
            {
                floorInter = intersection.ExpandToFloor(measureDimension, coord, PafApp.SimpleDimTrees);
            }
            return floorInter.ExpandToFloor(yearsDimension, PafApp.SimpleDimTrees);
        }

        public void ProtectionMgnrStatus()
        {
            if (!PafApp.GetLogger().IsDebugEnabled) return;
            PafApp.GetLogger().DebugFormat("System Locks size: {0}", new object[] { SystemLocks.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Protected Cells size: {0}", new object[] { ProtectedCells.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Protected Rules size: {0}", new object[] { ProtectedRules.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("UserLocks size: {0}", new object[] { UserLocks.Count.ToString("N0") });
            if (GlobalLocks != null) PafApp.GetLogger().DebugFormat("GlobalLockMngr size: {0}", new object[] { GlobalLocks.Count().ToString("N0") });
            PafApp.GetLogger().DebugFormat("Session Locks size: {0}", new object[] { SessionLocks.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Changes size: {0}", new object[] { Changes.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Changed Intersections size: {0}", new object[] { _changedIntersections.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Unlocked Changes size: {0}", new object[] { UnLockedChanges.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Dependencies Changes size: {0}", new object[] { Dependencies.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("RuleSets size: {0}", new object[] { _ruleSets.Count.ToString("N0") });
            if (_measureDefs != null) PafApp.GetLogger().DebugFormat("MeasureDefs size: {0}", new object[] { _measureDefs.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Invalid Replication Cells size: {0}", new object[] { InvalidReplicationCells.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Change Cells size: {0}", new object[] { ChangeCells.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Replicate All Cells size: {0}", new object[] { Replication.AllocateAllCells.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Replicate Existing Cells size: {0}", new object[] { Replication.AllocateExistingCells.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Lift All Cells size: {0}", new object[] { Lift.AllocateAllCells.Count.ToString("N0") });
            PafApp.GetLogger().DebugFormat("Lift Existing Cells size: {0}", new object[] { Lift.AllocateExistingCells.Count.ToString("N0") });
        }



        #endregion PublicMethods





        #region PrivateMethods

        private void InitDependencies(IEnumerable<RuleSet> ruleSets)
        {
            foreach (string axissequence in ruleSets.Select(ruleSet => ruleSet.Dimension).Where(axissequence => !Dependencies.ContainsKey(axissequence)))
            {
                Dependencies.Add(axissequence,new Dictionary<string, Dictionary<int, Dictionary<int, List<string>>>>());
            }
        }


        /// <summary>
        /// Get the dependencies for each cell added to the Change Queue
        /// </summary>
        /// <param name="intersection"></param>
        /// <param name="ruleSets">List of rule sets to process</param>
        private void AddDependencies(Intersection intersection, IEnumerable<RuleSet> ruleSets)
        {
            //DateTime startTime = DateTime.Now;

            string currentRuleSetName = PafApp.RuleSetName;
            string yearDim = PafApp.CacheBlock.mdbDef.yearDim;
            string timeDim = PafApp.CacheBlock.mdbDef.timeDim;
            string measureDim = PafApp.CacheBlock.mdbDef.measureDim;

            foreach (RuleSet ruleSet in ruleSets)
            {

                //Filter out all but the user selected Measure Rule Set
                if (ruleSet.IsMeasure)
                {
                    if (ruleSet.Name != currentRuleSetName)
                    {
                        continue;
                    }
                }

                
                string axissequence = ruleSet.Dimension;
                string coordinate = null;
                if (axissequence == PafAppConstants.TIME_HORIZON_DIM)
                {
                    coordinate = intersection.TranslateTimeYearIs(timeDim, yearDim);
                }
                else
                {
                    coordinate = intersection.GetCoordinate(axissequence);
                }

                //If Dependencies doesn't contain the member or the ruleset type
                Dictionary<string, Dictionary<int, Dictionary<int, List<string>>>> axisDependencies = Dependencies[axissequence];
                
                //bool containsCoordinate = axisDependencies.ContainsKey(coordinate);
                bool containsCoordinate = false;
                bool containsRuleSet = false;

                Dictionary<int, Dictionary<int, List<string>>> axisCoordDependencies;
                Dictionary<int, List<string>> axisCoordDependenciesRuleSet = null;
                if (axisDependencies.TryGetValue(coordinate, out axisCoordDependencies))
                {
                    containsCoordinate = true;
                    if (axisCoordDependencies.TryGetValue(ruleSet.Type, out axisCoordDependenciesRuleSet))
                    {
                        containsRuleSet = true;
                    }

                }

                if ((!containsCoordinate) || (containsCoordinate && !containsRuleSet))
                {
                    //Get the Measure Type to indicate which Time Balance RuleSet is used
                    MeasureType measType = MeasureType.None;
                    if (ruleSet.IsTime)
                    {
                        string coord = intersection.GetCoordinate(measureDim);

                        MeasureDef md;
                        if (MeasureDefs.TryGetValue(coord, out md))
                        {
                            measType = md.Type;
                        }
                    }

                    //Add matching rule groups for the intersection member (coordinate) to Dependencies
                    foreach (RuleGroup ruleGroup in ruleSet.RuleGroups)
                    {
                        //If the intersection member for the dimension is the same as the rule term
                        if (ruleGroup.Rules.ContainsKey(coordinate))
                        {
                            bool storeRule = false;

                            //Populate the dependency list - it contains the intersection member in the list
                            List<string> dependencyList = new List<string>(ruleGroup.Dependencies);

                            Dictionary<int, List<string>> memberDependencies = null;
                            if (!containsCoordinate)
                            {
                                memberDependencies = new Dictionary<int, List<string>>(3);
                                memberDependencies.Add(_ruleCounter, dependencyList);
                                Dictionary<int, Dictionary<int, List<string>>> byType = new Dictionary<int, Dictionary<int, List<string>>>();
                                byType.Add(ruleSet.Type, memberDependencies);
                                axisDependencies.Add(coordinate, byType);
                                axisCoordDependencies = byType;
                                axisCoordDependenciesRuleSet = memberDependencies;
                                storeRule = true;
                                containsCoordinate = true;
                                containsRuleSet = true;

                            }
                            else
                            {
                                if (!containsRuleSet)
                                {
                                    memberDependencies = new Dictionary<int, List<string>>(3);
                                    memberDependencies.Add(_ruleCounter, dependencyList);
                                    axisCoordDependencies.Add(ruleSet.Type, memberDependencies);
                                    axisCoordDependenciesRuleSet = memberDependencies;
                                    storeRule = true;
                                    containsRuleSet = true;
                                }
                                else
                                {
                                    axisCoordDependenciesRuleSet.Add(_ruleCounter, dependencyList);
                                    storeRule = true;
                                }
                            }

                            if (storeRule)
                            {
                                //Store rule location for lookup purposes later
                                //RuleInfo ruleInfo = new RuleInfo();
                                //ruleInfo.RuleSetIndex = GetRuleSets().IndexOf(ruleSet);
                                //ruleInfo.RuleGroupIndex = ruleSet.RuleGroups.IndexOf(ruleGroup);
                                //ruleInfo.MeasureType = measType;
                                _rules.Add(_ruleCounter++, new RuleInfo(GetRuleSets().IndexOf(ruleSet), ruleSet.RuleGroups.IndexOf(ruleGroup), measType));
                            }
                        }
                    }
                }
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("AddDependencies, runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
        }

        /// <summary>
        /// 
        /// </summary>
        private Dictionary<string, MeasureDef> MeasureDefs
        {
            get
            {
                if (_measureDefs == null)
                {
                    _measureDefs = new Dictionary<string, MeasureDef>();

                    foreach (simpleMeasureDef measure in PafApp.CacheBlock.measureDefs)
                    {
                        MeasureDef measureDef = new MeasureDef(measure.name, measure.recalcTBOveride, measure.type);
                        _measureDefs.Add(measure.name, measureDef);
                    }
                }
                return _measureDefs;
            }
        }

        /// <summary>
        /// Clear the Protected Cells Queue
        /// </summary>
        /// <param name="addToChgCellMngr">True to add the cells to the ChangedCellMngr()</param>
        private void ClearQueues(bool addToChgCellMngr)
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<CellAddress> cells = new List<CellAddress>(ProtectCells.Count);

            if (!addToChgCellMngr)
            {
                //ProtectedCells.Clear();
                ProtectCells.Clear();
                sw.Stop();
                PafApp.GetLogger().InfoFormat("ClearQueues runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
                return;
            }

            while (ProtectCells.Count > 0)
            {
                //Get the Protected Intersection
                Intersection protectedIntrsctn = (Intersection)ProtectCells.Dequeue();
                List<CellAddress> cellAddresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(protectedIntrsctn);

                //If the intersection exists in the view
                if (cellAddresses == null) continue;
                foreach (CellAddress t in cellAddresses)
                {
                    //Add the protected cells to the ChangedCellMngr
                    PafApp.GetChangedCellMngr().AddCell(
                        Convert.ToString(PafApp.GetViewMngr().CurrentView.HashCode),
                        new ChangedCellInfo(
                            Convert.ToString(PafApp.GetViewMngr().CurrentView.HashCode),
                            t,
                            Change.Protected));
                    //Apply formatting and lock the protected intersection
                    //Add the protected cell to a range to be protected later
                    //ProtectedRange.AddContiguousRange(cellAddresses[i]);
                    cells.Add(t);
                }
            }

            cells.Sort();
            Range rng = cells.GetRange();

            if (rng != null)
            {
                ProtectedRange = rng;
            }

            //Set the color for the protected cells
            if (ProtectedRange.ContiguousRanges.Count > 0)
            {
                PafApp.GetViewMngr().CurrentGrid.SetProtectedFormats(ProtectedRange
                   , PafApp.GetPafAppSettingsHelper().GetProtectedColor());
                //Next, clear the list of changed cell addresses
                ProtectedRange = null;
            }
            sw.Stop();
            PafApp.GetLogger().InfoFormat("ClearQueues runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Clear the Protected Cells Queue
        /// </summary>
        private void ClearQueues1()
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<CellAddress> protectedCellAddresses = new List<CellAddress>();
            //bool setFlag; //Only set the DataCache cell value 1 time per intersection

            while (ProtectCells.Count > 0)
            {
                //Get the Protected Intersection
                Intersection protectedIntrsctn = (Intersection)ProtectCells.Dequeue();
                protectedCellAddresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(protectedIntrsctn);
                //setFlag = false;

                //If the intersection exists in the view
                if (protectedCellAddresses != null)
                {
                    for (int i = 0; i < protectedCellAddresses.Count; i++)
                    {
                        //Add the protected cells to the ChangedCellMngr
                        PafApp.GetChangedCellMngr().AddCell(
                            PafApp.GetViewMngr().CurrentView.HashCode.ToString(),
                                new ChangedCellInfo(
                                    PafApp.GetViewMngr().CurrentView.HashCode.ToString(),
                                    protectedCellAddresses[i],
                                    Change.Protected));

                        //Apply formatting and lock the protected intersection
                        //Add the protected cell to a range to be protected later
                        ProtectedRange.AddContiguousRange(protectedCellAddresses[i]);
                    }
                }
            }

            //Set the color for the protected cells
            if (ProtectedRange.ContiguousRanges.Count > 0)
            {
                PafApp.GetViewMngr().CurrentGrid.SetProtectedFormats(ProtectedRange
                   , PafApp.GetPafAppSettingsHelper().GetProtectedColor());
                //Next, clear the list of changed cell addresses
                ProtectedRange = null;
            }
            sw.Stop();
            PafApp.GetLogger().InfoFormat("ClearQueues runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="isCurrentYear"></param>
        /// <param name="forwardPlannable"></param>
        /// <param name="planningVersion"></param>
        private void ApplyBohProtection(bool isCurrentYear, bool forwardPlannable, string planningVersion)
        {
            List<string> protectedPeriods = new List<string>();
            _stopwatch = Stopwatch.StartNew();
            SimpleDimTrees dimTrees = PafApp.SimpleDimTrees;

            //Only protect the first open week of the UOW for the current year if the planning version is Forward Plannable
            string bohPeriod;
            if (forwardPlannable && isCurrentYear)
            {
                string lastPeriod = PafApp.CacheBlock.lastPeriod;
                //bohPeriod = PafApp.GetGridApp().GetSimpleTrees().GetOffset(PafApp.CacheBlock.mdbDef.timeDim, lastPeriod, 1);
                bohPeriod = dimTrees.GetOffset(PafApp.CacheBlock.mdbDef.timeDim, lastPeriod, 1);

                //Is the week following the last period a part of the UOW?
                if (bohPeriod == "")
                {
                    //If not, protect BOH in the first week of the UOW
                    bohPeriod = dimTrees.GetFirstLevelZeroMember(PafApp.CacheBlock.mdbDef.timeDim);
                    //TTN-2605, Since the tree has not zero level members, just return the first floor member
                    if (String.IsNullOrWhiteSpace(bohPeriod))
                    {
                        bohPeriod = dimTrees.GetFirstFloorMember(PafApp.CacheBlock.mdbDef.timeDim);
                    }

                }
            }
            else
            {
                bohPeriod = dimTrees.GetFirstLevelZeroMember(PafApp.CacheBlock.mdbDef.timeDim);
                //TTN-2605, Since the tree has not zero level members, just return the first floor member
                if (String.IsNullOrWhiteSpace(bohPeriod))
                {
                    bohPeriod = dimTrees.GetFirstFloorMember(PafApp.CacheBlock.mdbDef.timeDim);
                }
            }

            protectedPeriods.Add(bohPeriod);

            //Protect all appropriate upstream weeks
            string protectedPeriod;
            do
            {
                //protectedPeriod = PafApp.GetGridApp().GetSimpleTrees().GetParent(PafApp.CacheBlock.mdbDef.timeDim, bohPeriod);
                protectedPeriod = dimTrees.GetParentMemberName(PafApp.CacheBlock.mdbDef.timeDim, bohPeriod);

                if (protectedPeriod != null)
                {
                    protectedPeriods.Add(protectedPeriod);
                    bohPeriod = protectedPeriod;
                }

            } while (protectedPeriod != null);

            //Find and store the BOH Measures
            List<String> bohMeasureList = new List<string>();
            foreach (KeyValuePair<String, MeasureDef> kv in MeasureDefs)
            {
                if (kv.Value.Type == MeasureType.TimeBalFirst || (kv.Value.Type == MeasureType.Recalc &&
                         kv.Value.RecalcTbOveride == MeasureType.TimeBalFirst))
                {
                    bohMeasureList.Add(kv.Value.Name);
                }
            }


            //loop through the protected time periods
            List<CellAddress> cells = new List<CellAddress>();
            String currentYr = PafApp.GetPafPlanSessionResponse().clientCacheBlock.currentYear;

            HashSet<Intersection> changedCells = new HashSet<Intersection>();
            changedCells.UnionWith(ChangeCells.Cast<Intersection>().ToList());

            foreach (string member in protectedPeriods)
            {
                List<Intersection> intersections = PafApp.GetViewMngr().CurrentOlapView.FindMemberIntersections(member);

                foreach (Intersection intersection in intersections)
                {
                    if (MeasureDefs.ContainsKey(intersection.GetCoordinate(PafApp.CacheBlock.mdbDef.measureDim)))
                    {
                        if (intersection.GetCoordinate(PafApp.CacheBlock.mdbDef.versionDim) == planningVersion)
                        {
                            //Only add BOH protection to the current year and future years
                            if ((isCurrentYear && intersection.GetCoordinate(PafApp.CacheBlock.mdbDef.yearDim) == currentYr)
                                || (!isCurrentYear && intersection.GetCoordinate(PafApp.CacheBlock.mdbDef.yearDim).CompareTo(currentYr) > 0))
                            {
                                MeasureDef measDef = MeasureDefs[intersection.GetCoordinate(PafApp.CacheBlock.mdbDef.measureDim)];
                                if (measDef.Type == MeasureType.TimeBalFirst || (measDef.Type == MeasureType.Recalc &&
                                    measDef.RecalcTbOveride == MeasureType.TimeBalFirst))
                                {
                                    //Protected changes are added to the change queu which is processed in PruneDependencies
                                    //if (!this.ChangeCells.Contains(intersection))
                                    //{
                                    //    this.ChangeCells.Enqueue(intersection);
                                    //}
                                    if (!changedCells.Contains(intersection))
                                    {
                                        changedCells.Add(intersection);
                                        ChangeCells.Enqueue(intersection);
                                    }

                                    //It is faster to add the BOHProtected intersections to the ProtectedCells list
                                    //and format the BOHProtected cells prior to calling PruneDependencies

                                    //Add the locked cell to the list of locked cells
                                    if (!ProtectedCells.Contains(intersection.UniqueID))
                                    {
                                        //ProtectedCells.Add(intersection.UniqueID, intersection);
                                        ProtectedCells.Add(intersection.UniqueID);
                                        ProtectedCellsSet.AddCoordinate(intersection);
                                    }

                                    //Store the changed intersections to be formatted
                                    List<CellAddress> cellAdresses = PafApp.GetViewMngr().CurrentOlapView.FindIntersectionAddress(intersection);
                                    if (cellAdresses != null)
                                    {
                                        cells.AddRange(cellAdresses);
                                        //for (int i = 0; i < cellAdresses.Count; i++)
                                        //{
                                        //    range.AddContiguousRange(cellAdresses[i]);
                                        //}
                                    }
                                }
                                else
                                {
                                    //Protect all off-screen interesection for BOH measures
                                    foreach (string bohMeasure in bohMeasureList)
                                    {
                                        Intersection newIntersection = intersection.DeepClone();

                                        newIntersection.SetCoordinate(PafApp.CacheBlock.mdbDef.measureDim, bohMeasure);

                                        //Protected changes are added to the change queu which is processed in PruneDependencies
                                        //if (!this.ChangeCells.Contains(newIntersection))
                                        //{
                                        //    this.ChangeCells.Enqueue(newIntersection);
                                        //}
                                        if (!changedCells.Contains(newIntersection))
                                        {
                                            changedCells.Add(newIntersection);
                                            ChangeCells.Enqueue(newIntersection);
                                        }

                                        //Add the locked cell to the list of locked cells
                                        if (!ProtectedCells.Contains(newIntersection.UniqueID))
                                        {
                                            //ProtectedCells.Add(newIntersection.UniqueID, newIntersection);
                                            ProtectedCells.Add(newIntersection.UniqueID);
                                            ProtectedCellsSet.AddCoordinate(newIntersection);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                cells.Sort();
                Range range = cells.GetRange();
                range.Sorted = true;

                if (range.ContiguousRanges.Count > 0)
                {
                    PafApp.GetViewMngr().CurrentOlapView.BOHProtectedRange = range;
                }
            }
            _stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ApplyBohProtection runtime: {0} (ms)", _stopwatch.ElapsedMilliseconds.ToString("N0"));
        }


        /// <summary>
        /// Handle Perpetual Inventory Rule Groups
        /// </summary>
        /// <param name="changedInterRule"></param>
        /// <param name="changeIntersection"></param>
        /// <param name="offsetMember"></param>
        /// <param name="outsideUowFlag"></param>
        private void GetPeriodOffset(Rule changedInterRule, Intersection changeIntersection, out string offsetMember, out bool outsideUowFlag)
        {
            //DateTime startTime = DateTime.Now;
            string timeDim = PafApp.CacheBlock.mdbDef.timeDim;
            string yearDim = PafApp.CacheBlock.mdbDef.yearDim;
            SimpleDimTrees dimTrees = PafApp.SimpleDimTrees;
            offsetMember = "";
            outsideUowFlag = false;

            if (changedInterRule.Params != null && changedInterRule.Params.Length >= 3 && changedInterRule.Params[2].IsDouble())
            {
                string dimName = changedInterRule.Params[1].ToString();
                int functionOffset = int.Parse(changedInterRule.Params[2].ToString());
                bool bCrossYears = true;
                if (changedInterRule.Params.Length > 3)
                {
                    bCrossYears = Convert.ToBoolean(changedInterRule.Params[3]);
                }
                //assigns direction to the offset
                int offset;
                if (changedInterRule.Function == FunctionType.AtNext)
                {
                    offset = -1 * functionOffset;
                }
                else
                {
                    offset = functionOffset;
                }

                if (dimName == timeDim)
                {
                    TimeSlice timeSlice = new TimeSlice(changeIntersection, timeDim, yearDim);
                    //string offsetMember1 = PafApp.GetGridApp().GetSimpleTrees().GetOffset(PafAppConstants.TIME_HORIZON_DIM, timeSlice.ToString(), offset);
                    offsetMember = dimTrees.GetOffset(PafAppConstants.TIME_HORIZON_DIM, timeSlice.ToString(), offset);
                    //if (offsetMember != offsetMember1)
                    //{
                    //    PafApp.GetLogger().InfoFormat("New: {0}, Old: {1}", new object[] { offsetMember1, offsetMember });
                    //    PafApp.GetLogger().ErrorFormat("Offset member error: offset: {0}", new object[] { offset });
                    //}


                    if (!bCrossYears && !String.IsNullOrEmpty(offsetMember))
                    {
                        TimeSlice temp = new TimeSlice(offsetMember);
                        if (!timeSlice.YearEqual(temp))
                        {
                            offsetMember = String.Empty;
                        }
                    }
                }
                else
                {
                    //Gets the member name for the offset member
                    //offsetMember = PafApp.GetGridApp().GetSimpleTrees().GetOffset(dimName, changeIntersection.GetCoordinate(dimName), offset);
                    offsetMember = dimTrees.GetOffset(dimName, changeIntersection.GetCoordinate(dimName), offset);
                }
                //if there is no offset member then the protected intersection does not exist
                outsideUowFlag = String.IsNullOrEmpty(offsetMember);
            }
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("GetPeriodOffset, runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
        }

        /// <summary>
        /// Accepts an array of dimensions and members for an existing intersection.  The method also
        /// accepts a dimension and a member in that dimension.  The method ,in effect, creates a new 
        /// intersection by updating the existing intersection with the member parameter value.
        /// </summary>
        /// <param name="axissequence"></param>
        /// <param name="coordinates"></param>
        /// <param name="dimension"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        private string[] SetCoordinate(string[] axissequence, string[] coordinates,
            string dimension, string member)
        {
            //string[] newCoordinates = (string[])coordinates.Clone();
            string[] newCoordinates = new string[coordinates.Length];
            Array.Copy(coordinates, newCoordinates, coordinates.Length);

            for (int i = 0; i < axissequence.Length; i++)
            {
                if (axissequence[i] == dimension)
                {
                    newCoordinates[i] = member;
                    break;
                }
            }

            return newCoordinates;
        }

        #endregion PrivateMethods


    }
}
