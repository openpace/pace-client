#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections;

//test
namespace Titan.Pace.Rules.Utility
{
    internal class RulesUtility
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public RulesUtility()
        {
        }

        /// <summary>
        /// Gets an ArrayList of formula tokens for an expression.
        /// </summary>
        /// <param name="expression">Expression to evaluate.</param>
        /// <returns>An arraylist of tokens.</returns>
        public static ArrayList getFormulaTokens(string expression)
        {
            String token = "";
            String[] rawTokens;
            //char[] mathtokes =  { '+', '-', '*', '/', '^', '(', ')', ',' };
            char[] mathtokes =  { '(', ')', ',' };
            ArrayList tokens = null;

            rawTokens = expression.Split(mathtokes);
            tokens = new ArrayList(rawTokens.GetUpperBound(0));

            for (int i = 0; i <= rawTokens.GetUpperBound(0); i++)
            {
                token = rawTokens[i];
                token = token.Trim();
                token = token.TrimStart('"');
                token = token.TrimEnd('"');

                if (token.Length > 0)
                {
                    tokens.Add(token);
                }
            }
            return tokens;
        }
    }
}
