#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

namespace Titan.Pace.Rules.Utility
{
    internal class MeasureDef
    {
        /// <summary>
        /// Constant for Aggregate Mesaure Type "AGGREGATE"
        /// </summary>
        private const string AggregateMeasure = "AGGREGATE";

        /// <summary>
        /// Constant for TIMEBALFIRST
        /// </summary>
        private const string TimeBalFirst = "TIMEBALFIRST";

        /// <summary>
        /// Constant for TIMEBALLAST
        /// </summary>
        private const string TimeBalLast = "TIMEBALLAST";

        /// <summary>
        /// Constant for FORWARDPLANNABLE
        /// </summary>
        private const string ForwardPlannable = "FORWARDPLANNABLE";

        /// <summary>
        /// Constant for RECALC
        /// </summary>
        private const string Recalc = "RECALC";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="recalcTbOveride"></param>
        /// <param name="type"></param>
        public MeasureDef(string name, string recalcTbOveride, string type)
        {
            Name = name;
            RecalcTbOveride = ToMeasureType(recalcTbOveride);
            //Type = type;
            Type = ToMeasureType(type);
        }

        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MeasureType RecalcTbOveride { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public MeasureType Type { get; set; }


        private static MeasureType ToMeasureType(string measureType)
        {
            switch (measureType.ToUpper())
            {
                case TimeBalFirst:
                    return MeasureType.TimeBalFirst;
                case TimeBalLast:
                    return MeasureType.TimeBalLast;
                case ForwardPlannable:
                    return MeasureType.ForwardPlannable;
                case AggregateMeasure:
                    return MeasureType.Aggregate;
                case Recalc:
                    return MeasureType.Recalc;
                default:
                    return MeasureType.None;
            }
        }
    }
}
