#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

namespace Titan.Pace.Rules.Utility
{
    internal class RuleInfo
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public RuleInfo()
        {
            RuleSetIndex = 0;
            RuleGroupIndex = 0;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public RuleInfo(int ruleSetIndex, int ruleGroupIndex, MeasureType measureType)
        {
            RuleSetIndex = ruleSetIndex;
            RuleGroupIndex = ruleGroupIndex;
            MeasureType = measureType;
        }

        /// <summary>
        /// Get/set the rulesetindex.
        /// </summary>
        public int RuleSetIndex { get; set; }

        /// <summary>
        /// Get/set the rulegroupindex.
        /// </summary>
        public int RuleGroupIndex { get; set; }

        public MeasureType MeasureType { get; set; }
    }
}
