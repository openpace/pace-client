#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.Rules
{
    internal class Rule
    {
        /// <summary>
        /// Constant for @NEXT
        /// </summary>
        private const string AtNext = "@NEXT";

        /// <summary>
        /// Constant for @PREV
        /// </summary>
        private const string AtPrev = "@PREV";

        /// <summary>
        /// Constructor.
        /// </summary>
        public Rule()
        {
            Expression = String.Empty;
            Term = String.Empty;
            //Function = String.Empty;
            Function = FunctionType.None;
            Params = null;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Rule(string expression, string term)
        {
            Expression = expression;
            Term = term;
            //Function = String.Empty;
            Function = FunctionType.None;
            Params = null;
        }

        /// <summary>
        /// Get/set the rules expression.
        /// </summary>
        public string Expression { get; set; }

        /// <summary>
        /// Get/set the rules term.
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        /// Get/set the rules function.
        /// </summary>
        public FunctionType Function { get; set; }

        /// <summary>
        /// Get/set the rules parameters.
        /// </summary>
        public object[] Params { get; set; }

        /// <summary>
        /// Sets/converts the String to a  Function Type  and sets to the Function property.
        /// </summary>
        /// <param name="function"></param>
        public void SetFunction(string function)
        {
            Function = ToFunctionType(function);
        }


        private static FunctionType ToFunctionType(string measureType)
        {
            switch (measureType.ToUpper())
            {
                case AtNext:
                    return FunctionType.AtNext;
                case AtPrev:
                    return FunctionType.AtPrev;
                default:
                    return FunctionType.None;
            }
        }
    }
}
