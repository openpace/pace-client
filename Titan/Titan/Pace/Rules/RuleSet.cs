#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.Pace.Application.Extensions;
using Titan.Palladium;

namespace Titan.Pace.Rules
{
    internal class RuleSet
    {
        private readonly string _measureDimName;
        private readonly string _timeDimName;
        private string _dimension;

        /// <summary>
        /// Constructor.
        /// </summary>
        public RuleSet(string dimension, int type, string name, string measureDimName, string timeDimName)
        {
            _measureDimName = measureDimName;
            _timeDimName = timeDimName;
            Dimension = dimension;
            Type = type;
            RuleGroups = new List<RuleGroup>();
            Name = name;
            HasMemberTagFormulas = false;
            SetDimTypes();
        }

        /// <summary>
        /// Get/set the ruleset dimension.
        /// </summary>
        public string Dimension
        {
            get { return _dimension; }
            set 
            { 
                _dimension = value;
                SetDimTypes();
            }
        }

        /// <summary>
        /// Get/set the ruleset type.
        /// </summary>
        public int Type { get; set; }

        /// <summary>
        /// Get/set the list of rulegroups.
        /// </summary>
        public List<RuleGroup> RuleGroups { get; set; }

        /// <summary>
        /// Get/set Ruleset name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Returns true if the RuleSet is for the Measures dime
        /// </summary>
        public bool IsMeasure { get; private set; }

        /// <summary>
        /// Returns true if the RuleSEt if for the Time dimensino.
        /// </summary>
        public bool IsTime { get; private set; }

        /// <summary>
        /// Returns true if the rule set contains a member tag function.
        /// </summary>
        public bool HasMemberTagFormulas { get; set; }

        /// <summary>
        /// Set of referenced member tag defs
        /// </summary>
        public HashSet<String> ReferencedMbrTagDefs { get; set; }

        private void SetDimTypes()
        {
            IsTime = _dimension.EqualsIgnoreCase(_timeDimName);

            IsMeasure = _dimension.EqualsIgnoreCase(_measureDimName);
        }
    }
}
