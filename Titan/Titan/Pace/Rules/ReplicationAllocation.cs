﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;

namespace Titan.Pace.Rules
{
    /// <summary>
    /// Replication.
    /// </summary>
    internal class ReplicationAllocation : BaseUserAllocation 
    {
        public ReplicationAllocation(ProtectionMngr parent):base(parent)
        {
            RegexStringAll = PafAppConstants.REPLICATE_REGEX_STRING_RA;
            RegexStringExisting = PafAppConstants.REPLICATE_REGEX_STRING_R;
            RegexStringBoth = PafAppConstants.REPLICATE_REGEX_STRING_R_OR_A;
            AllocateAllFormatSuffix = PafAppConstants.REPLICATE_ALL_FORMAT_SUFFIX;
            AllocateExistingFormatSuffix = PafAppConstants.REPLICATE_EXISTING_FORMAT_SUFFIX;
            AllocateLeadingSymbols = PafAppConstants.REPLICATE_LEADING_SYMBOLS;
        }

        /// <summary>
        /// Perform replication on a ContiguousRange.
        /// </summary>
        /// <param name="range">The cell to replicate.</param>
        /// <param name="overwireNumericFormat">Overwrite the existing numeric format in the cell.</param>
        /// <param name="replicationType">The type of replication to perform.</param>
        public void ReplicateCells(List<Cell> range, bool overwireNumericFormat, ReplicationType replicationType)
        {
            AllocateCells(range, overwireNumericFormat, replicationType.ToAllocationType());
        }

        /// <summary>
        /// Perform replication on a cell.
        /// </summary>
        /// <param name="cell">The cell to replicate.</param>
        /// <param name="overwireNumericFormat">Overwrite the existing numeric format in the cell.</param>
        /// <param name="replicationType">The type of replication to perform.</param>
        public void ReplicateCell(CellAddress cell, bool overwireNumericFormat, ReplicationType replicationType)
        {
            AllocateCell(cell, overwireNumericFormat, replicationType.ToAllocationType());
        }

        /// <summary>
        /// Gets the replication type of a cell.
        /// </summary>
        /// <param name="cell">The address of the cell</param>
        /// <returns>The replicaiton type.</returns>
        public ReplicationType GetReplicationType(CellAddress cell)
        {
            double d;
            return GetReplicationType(cell, out d);
        }

        /// <summary>
        /// Gets the replication type of a cell.
        /// </summary>
        /// <param name="cell">The address of the cell</param>
        /// <param name="cellValue">The value of the replication.</param>
        /// <returns>The replicaiton type.</returns>
        public ReplicationType GetReplicationType(CellAddress cell, out double cellValue)
        {
            return GetAllocationType(cell, out cellValue).ToReplicationType();
        }

        /// <summary>
        /// Parses a string and returns the type of replication.
        /// </summary>
        /// <param name="replicationString">The string to parse.</param>
        /// <param name="value">The numeric value to replicate (out parameter).</param>
        /// <returns>The replicaiton type.</returns>
        public ReplicationType GetReplicationType(object replicationString, out double value)
        {
            return GetAllocationType(replicationString, out value).ToReplicationType();
        }
    }
}