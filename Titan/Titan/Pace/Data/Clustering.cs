﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Linq;

namespace Titan.Pace.Data
{
    
    
    public partial class Clustering {
        /// <summary>
        /// Adds a dimension to the table.
        /// </summary>
        /// <param name="dimName"></param>
        /// <returns></returns>
        public int AddDimension(string dimName)
        {
            int dimKey = GetDimensionKey(dimName);
            if (dimKey <= 0)
            {
                DimensionRow row = Dimension.NewDimensionRow();
                row.Name = dimName;
                Dimension.Rows.Add(row);
                dimKey = row.Key;
            }
            return dimKey;
        }

        /// <summary>
        /// Gets a key for a dimension name.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns>The key or -1 if the query results in no items found.</returns>
        public int GetDimensionKey(string dimensionName)
        {
            DimensionRow tr = Dimension.FirstOrDefault(x => x.Name.Equals(dimensionName));

            return tr == null ? -1 : tr.Key;
        }

        /// <summary>
        /// Gets a key for a dimension name.
        /// </summary>
        /// <param name="settingsName"></param>
        /// <param name="dimKey"></param>
        /// <returns>The key or -1 if the query results in no items found.</returns>
        public int GetSettingsKey(string settingsName, int dimKey)
        {
            SettingsRow tr = Settings.FirstOrDefault(x => x.Name.Equals(settingsName) && x.DimKey == dimKey);

            return tr == null ? -1 : tr.Key;
        }

        public List<SettingsRow> GetSettings(string dim)
        {
            int dimKey = GetDimensionKey(dim);
            return Settings.Where(x => x.DimKey == dimKey).ToList();

        }

        public List<string> GetSettingsNames(string dim)
        {
            List<SettingsRow> rows = GetSettings(dim);

            return rows.Select(x => x.Name).ToList();

        }

        public int AddSettings(int dimKey, string name, int level)
        {
            int setKey = GetSettingKey(dimKey, name);

            if (setKey <= 0)
            {

                SettingsRow sr = Settings.NewSettingsRow();
                sr.DimKey = dimKey;
                sr.Name = name;
                sr.Level = level;

                Settings.Rows.Add(sr);

                setKey = sr.Key;
            }
            return setKey;
        }


        public int GetSettingKey(int dimKey, string name)
        {
            SettingsRow sr = Settings.FirstOrDefault(x => x.DimKey == dimKey && x.Name == name);

            return sr == null ? -1 : sr.Key;
        }

        public List<string> GetSelections(int dimKey, int sKey)
        {
            return Selections.Where(x => x.DimKey == dimKey && x.SettingsKey == sKey).Select(y => y.Value).ToList();
        }

        public IEnumerable<SelectionsRow> GetSelectionsRows(int dimKey, int sKey)
        {
            return Selections.Where(x => x.DimKey == dimKey && x.SettingsKey == sKey).ToList();
        }

        public List<string> GetSelections(string dim, string name)
        {
            int dimKey = GetDimensionKey(dim);
            int sKey = GetSettingsKey(name, dimKey);
            return GetSelections(dimKey, sKey);
        }

        public void AddSelection(string settingsName, string dim, string value)
        {
            int dimKey = GetDimensionKey(dim);
            int settingsKey = GetSettingsKey(settingsName, dimKey);

            AddSelection(settingsKey, dimKey, value);
        }

        public void AddSelection(int settingsKey, int dimKey, string value)
        {

            SelectionsRow sr = Selections.NewSelectionsRow();
            sr.DimKey = dimKey;
            sr.SettingsKey = settingsKey;
            sr.Value = value;

            Selections.Rows.Add(sr);
        }

        public void RemoveSelections(int settingsKey, int dimKey)
        {

            IEnumerable<SelectionsRow> dr = GetSelectionsRows(dimKey, settingsKey);
            foreach (SelectionsRow sr in dr)
            {
                Selections.Rows.Remove(sr);
            }
        }

        public string GetMemberFormat(string memberName)
        {
            var members = MeasureFormats.Where(x => x.MemberName.Equals(memberName)).ToList();
            return members.Count != 0 ? members[0].NumericFormat : null;
        }

        public void AddOrUpdateMemberFormat(string memberName, string format)
        {
            var members = MeasureFormats.Where(x => x.MemberName.Equals(memberName)).ToList();
            if (members.Count != 0)
            {
                members[0].NumericFormat = format;
            }
            else
            {
                MeasureFormatsRow row = MeasureFormats.NewMeasureFormatsRow();
                row.MemberName = memberName;
                row.NumericFormat = format;

                MeasureFormats.Rows.Add(row);
            }
        }
    }
}
