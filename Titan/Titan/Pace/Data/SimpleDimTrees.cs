﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Transactions;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Base.Data;
using Titan.Pace.Base.Mdb;
using Titan.PafService;
using Titan.Palladium;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Data
{
    public partial class SimpleDimTrees
    {
        partial class DimensionLevelGenerationDataTable
        {
        }
    
        private Dictionary<string, long>  _dimensionMap = new Dictionary<string, long>(20);
        private Dictionary<string, long> _memberMap = new Dictionary<string, long>(1000);
        private Dictionary<string, long> _memberParentMap = new Dictionary<string, long>(1000);
        private Dictionary<string, int> _memberLevelMap = new Dictionary<string, int>(500);
        private Dictionary<string, int> _memberGenMap = new Dictionary<string, int>(500);
        private Dictionary<string, long> _aliasTableMap = new Dictionary<string, long>(10);

        /// <summary>
        /// Gets the name of the column where the Member Name is stored.
        /// </summary>
        public string MemberColumName
        {   
            get { return Members.MemberNameColumn.ColumnName; }
        }

        /// <summary>
        /// Gets the name of the column where the Member's Generation is stored.
        /// </summary>
        public string GenerationColumName
        {
            get { return Members.GenerationColumn.ColumnName; }
        }

        /// <summary>
        /// Gets the name of the column where the Member's Generation is stored.
        /// </summary>
        public string LevelColumName
        {
            get { return Members.LevelColumn.ColumnName; }
        }

        /// <summary>
        /// Gets the name of the column where the Member's key is stored.
        /// </summary>
        public string MemberKeyColumn
        {
            get { return Members.MemberKeyColumn.ColumnName; }
        }

        /// <summary>
        /// Gets the name of the column where the Member's parent key is stored.
        /// </summary>
        public string ParentKeyColum
        {
            get { return Members.ParentKeyColumn.ColumnName; }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="isBaseDimension"></param>
        /// <param name="levelZeroOnly"></param>
        /// <param name="parentDimKey"></param>
        /// <param name="hidden"></param>
        public void AddTree(pafSimpleDimTree tree, bool isBaseDimension, bool levelZeroOnly, long? parentDimKey, bool hidden)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            if (tree == null) return;

            string dimensionName = tree.id.Trim();
            long dimKey = AddDimension(dimensionName, isBaseDimension, levelZeroOnly, tree.discontig, parentDimKey, hidden);
            AddAliasTables(dimKey, tree.aliasTableNames);
            foreach (pafSimpleDimMember s in tree.memberObjects)
            {
                Dictionary<long, string> aliasValues = GetAliasValues(tree, s);
                bool hasChildren = !(s.childKeys == null || s.childKeys.Length == 0);
                string memberName = s.key.Trim();
                if (String.IsNullOrEmpty(s.parentKey))
                {
                    AddMember(dimKey, null, memberName, s.pafSimpleDimMemberProps, hasChildren, aliasValues);
                }
                else
                {
                    long parentKey = GetMemberKey(s.parentKey.Trim(), dimKey);
                    AddMember(dimKey, parentKey, memberName, s.pafSimpleDimMemberProps, hasChildren, aliasValues);
                }

            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("AddTree: {0} runtime: {1} (ms)", new object[] { dimensionName, stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="generationInfo"></param>
        /// <param name="levelInfo"></param>
        public void AddTreeInformation(string dimensionName, dimLevelGenInfo generationInfo = null, dimLevelGenInfo levelInfo = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            long dimKey = GetDimensionKey(dimensionName, true);

            if (generationInfo != null)
            {
                
                for (int i = 0; i < generationInfo.nameArray.Length; i++)
                {
                    AddTreeInformation(dimKey, generationInfo.nameArray[i], generationInfo.numberArray[i].GetValueOrDefault(), false);
                }
            }

            if (levelInfo != null)
            {
                for (int i = 0; i < levelInfo.nameArray.Length; i++)
                {
                    AddTreeInformation(dimKey, levelInfo.nameArray[i], levelInfo.numberArray[i].GetValueOrDefault());
                }
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("AddTreeInformation runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Returns true if the dimension is Synthetic
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public bool IsDimensionSynthetic(string dimensionName)
        {
            long dimensionKey = GetDimensionKey(dimensionName, false);

            if (dimensionKey < 0) return false;

            return IsDimensionSynthetic(dimensionKey);
        }

        /// <summary>
        /// Returns true if the dimension is Synthetic
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <returns></returns>
        public bool IsDimensionSynthetic(long dimensionKey)
        {
            var query = Dimension.Where(x => x.Key == dimensionKey && x.Synthetic).ToList();
            
            return query.Count > 0;
        }

        /// <summary>
        /// Gets a key for a member name.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="dimKey"></param>
        /// <returns>The key or -1 if the query results in no items found.</returns>
        public long GetMemberKey(string name, long dimKey)
        {
            long memberKey;
            string memberHash = GetMemberHash(dimKey, name);

            if (_memberMap.TryGetValue(memberHash, out memberKey))
            {
                return memberKey;
            }

            MembersRow tr = Members.FirstOrDefault(x => x.MemberName == name && x.DimKey == dimKey);

            if (tr != null)
            {
                _memberMap.Add(memberHash, tr.MemberKey);
                return tr.MemberKey;
            }
            return -1;
        }

        /// <summary>
        /// Gets a member key for a member name.
        /// </summary>
        /// <param name="name">Member name</param>
        /// <param name="dimKey">Dimension key</param>
        /// <param name="parentKey">Parent's key</param>
        /// <returns>The key or -1 if the query results in no items found.</returns>
        public long GetMemberKey(string name, long dimKey, long parentKey)
        {
            string memberHash = String.Concat(dimKey, parentKey, name);

            long memberKey;
            if (_memberParentMap.TryGetValue(memberHash, out memberKey))
            {
                return memberKey;
            }

            MembersRow tr = Members.FirstOrDefault(x => x.MemberName == name && x.DimKey == dimKey && x.ParentKey == parentKey);

            if (tr != null)
            {
                _memberParentMap.Add(memberHash, tr.MemberKey);
                return tr.MemberKey;
            }
            return -1;

            //return tr == null ? -1 : tr.MemberKey;
        }

        /// <summary>
        /// Gets a key for a member name.
        /// </summary>
        /// <param name="name">Member name</param>
        /// <param name="dimensionName">Dimension key</param>
        /// <returns>The key or -1 if the query results in no items found.</returns>
        public long GetMemberKey(string name, string dimensionName)
        {
            long dimKey = GetDimensionKey(dimensionName, false);

            return GetMemberKey(name, dimKey);
        }

        /// <summary>
        /// Gets the list of dimensions
        /// </summary>
        /// <param name="baseOnly">True to only return base dimensions.</param>
        /// <param name="queryHidden">Query hidden dimensions.</param>
        public List<string> GetDimensionNames(bool baseOnly = false, bool queryHidden = false)
        {
            List<string> result = new List<string>();
            foreach (DimensionRow dr in Dimension)
            {
                bool cont = true;
                //user specified not to query hidden dims, 
                //and the dim is hidden, so continue
                if (!queryHidden && dr.Hidden) continue;

                if (baseOnly)
                {
                    cont = dr.BaseDimension;
                }
                if (cont)
                {
                    
                    result.Add(dr.Name);
                }
            }
            return result;
        }

        /// <summary>
        /// Gets the list of attribute dimensions
        /// </summary>
        /// <param name="dimensionName">Base dimension to retrieve attribute names.</param>
        public List<string> GetAssociatedAttributeNames(string dimensionName)
        {
            long dimKey = GetDimensionKey(dimensionName, false);

            return dimKey > 0 ? GetAssociatedAttributeNames(dimKey) : null;
        }

        /// <summary>
        /// Gets the list of attribute dimensions
        /// </summary>
        /// <param name="dimensionKey">Base dimension to retrieve attribute names.</param>
        public List<string> GetAssociatedAttributeNames(long dimensionKey)
        {
            List<string> attributes = new List<string>(5);
            var tr = Dimension.Where(x => x.ParentKey == dimensionKey);
            attributes.AddRange(Enumerable.Select(tr, t => t.Name));
            return attributes;
        }

        /// <summary>
        /// Gets a key for a dimension name.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="queryHidden"></param>
        /// <returns>The key or -1 if the query results in no items found.</returns>
        public long GetDimensionKey(string dimensionName, bool queryHidden = false)
        {
            long key;
            if (_dimensionMap.TryGetValue(dimensionName, out key))
            {
                return key;
            }

            DimensionRow tr = Dimension.FirstOrDefault(x => x.Name.Equals(dimensionName));

            if (tr != null && !queryHidden && tr.Hidden) return -1;

            if (tr != null)
            {
                //Don't cache hidden dims.
                if (!tr.Hidden)
                {
                    _dimensionMap.Add(dimensionName, tr.Key);
                }
            }

            return tr == null ? -1 : tr.Key;
        }

        /// <summary>
        /// Gets base/attribute status of a dimension.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns>True if the dimension is a base dimension, false if an attribute dimension.</returns>
        public bool IsDimensionBase(string dimensionName)
        {
            long dimensionKey = GetDimensionKey(dimensionName, false);
            return dimensionKey >= 0 && IsDimensionBase(dimensionKey);
        }


        /// <summary>
        /// Gets base/attribute status of a dimension.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <returns>True if the dimension is a base dimension, false if an attribute dimension.</returns>
        public bool IsDimensionBase(long dimensionKey)
        {
            DimensionRow tr = Dimension.FirstOrDefault(x => x.Key == dimensionKey);
            return tr != null && tr.BaseDimension;
        }


        /// <summary>
        /// Gets the Level Zero Only value for a dimension.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns>The level zero only va</returns>
        public bool GetDimensionLevelZeroOnly(string dimensionName)
        {
            long dimKey = GetDimensionKey(dimensionName, false);
            return dimKey >= 0 && GetDimensionLevelZeroOnly(dimKey);
        }

        /// <summary>
        /// Gets the Level Zero Only value for a dimension.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <returns>The level zero only va</returns>
        public bool GetDimensionLevelZeroOnly(long dimensionKey)
        {
            DimensionRow tr = Dimension.FirstOrDefault(x => x.Key == dimensionKey);
            return tr != null && tr.LevelZeroOnly;
        }

        /// <summary>
        /// Gets the Hidden property for a dimension.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public bool GetDimensionIsHidden(string dimensionName)
        {
            long dimKey = GetDimensionKey(dimensionName, false);
            return dimKey <= 0;
        }

        /// <summary>
        /// Gets the max member level for a specified dimension.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="checkForLevelZeroOnly">Checks the dim table to verify for level zero selection restrictions.</param>
        /// <returns>The max member level</returns>
        public int GetMemberMaxLevel(long dimensionKey, bool checkForLevelZeroOnly)
        {
            if(checkForLevelZeroOnly)
            {
                if(GetDimensionLevelZeroOnly(dimensionKey))
                {
                    return Members.Where(x => x.DimKey == dimensionKey).Min(y => y.Level);
                }
            }
            return Members.Where(x => x.DimKey == dimensionKey).Max(y => y.Level);
        }

        /// <summary>
        /// Gets the level of a member.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        /// <remarks>Backed by a cache.</remarks>
        public int GetMemberLevel(string dimensionName, string memberName)
        {
            long dimensionKey = GetDimensionKey(dimensionName);
            int level;

            string memberHash = GetMemberHash(dimensionKey, memberName);

            if (_memberLevelMap.TryGetValue(memberHash, out level))
            {
                return level;
            }

            MembersRow mr = GetMember(dimensionName, memberName);

            //Nothing found.
            if (mr == null) return -1;

            level = mr.Level;
            _memberLevelMap.Add(memberHash, level);
            return level;
        }

        /// <summary>
        /// Gets the generation of a member.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        /// <remarks>Backed by a cache.</remarks>
        public int GetMemberGeneration(string dimensionName, string memberName)
        {
            long dimensionKey = GetDimensionKey(dimensionName);
            int gen;

            string memberHash = GetMemberHash(dimensionKey, memberName);

            if (_memberGenMap.TryGetValue(memberHash, out gen))
            {
                return gen;
            }

            MembersRow mr = GetMember(dimensionName, memberName);

            //Nothing found.
            if (mr == null) return -1;

            gen = mr.Generation;
            _memberGenMap.Add(memberHash, gen);
            return gen;
        }

        /// <summary>
        /// Returns a distint list of Level numbers.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public List<int> GetLevels(string dimensionName)
        {
            long dimKey = GetDimensionKey(dimensionName);
            List<int> result = Members.Where(x => x.DimKey == dimKey).Select(x => x.Level).Distinct().OrderByDescending(x => x).ToList();

            return result;
        }

        /// <summary>
        /// Returns a distinct list of Generation numbers.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public List<int> GetGenerations(string dimensionName)
        {
            long dimKey = GetDimensionKey(dimensionName);
            List<int> result = Members.Where(x => x.DimKey == dimKey).Select(x => x.Generation).Distinct().OrderBy(x => x).ToList();
            return result;
        }

        /// <summary>
        /// Finds the Prev or Next member at the same level within a specific PafSimpleTree.
        /// </summary>
        /// <param name="dimensionName">The name of the PafSimpleTree to search.</param>
        /// <param name="member"></param>
        /// <param name="offset"></param>
        /// <returns>A string member name</returns>
        public string GetOffset(string dimensionName, string member, int offset)
        {
            //var st = Stopwatch.StartNew();
            long dimKey = GetDimensionKey(dimensionName, true);
            MembersRow row = GetMember(dimKey, member);
            string result = String.Empty;
            //TTN-2282
            if (row == null) return result;
            long memberKey = row.MemberKey;
            int level = row.Level;

            if (memberKey > -1)
            {
                //find the offset in the array
                if (offset == 0)
                {
                    result = member;
                }
                else if (offset < 0)
                {
                    var offsetMbr = GetLesserOffset(dimKey, memberKey, level);
                    if (offsetMbr != null)
                    {
                        result = offsetMbr.MemberName;
                    }
                }
                else if (offset > 0)
                {
                    var offsetMbr = GetGreatorOffset(dimKey, memberKey, level);
                    if (offsetMbr != null)
                    {
                        result = offsetMbr.MemberName;
                    }
                }
            }

            //st.Stop();
            //PafApp.GetLogger().InfoFormat("GetOffset runtime: {0} (ms)", new object[] { st.ElapsedMilliseconds.ToString("N0") });

            return result;
        }

        /// <summary>
        /// Gets the first member that's at level 0 on the dimension tree.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public string GetFirstLevelZeroMember(string dimensionName)
        {
            long dimensionKey = GetDimensionKey(dimensionName);

            var zeroLevel =  Members.Where(x => x.DimKey == dimensionKey && x.Level == 0).OrderBy(y => y.MemberKey).FirstOrDefault();

            return zeroLevel == null ? null : zeroLevel.MemberName;
        }

        /// <summary>
        /// Returns true if the specifed member is the root member of the dimensions.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public bool IsRoot(string dimensionName, string memberName)
        {
            MembersRow row = GetRoot(dimensionName);
            return row != null && memberName.EqualsIgnoreCase(row.MemberName);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public string GetRootMemberName(string dimensionName)
        {
            MembersRow root = GetRoot(dimensionName);
            return root == null ? String.Empty : root.MemberName;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public MembersRow GetRoot(string dimensionName)
        {
            long dimensionKey = GetDimensionKey(dimensionName, false);

            if (dimensionKey < 0) return null;

            return GetRoot(dimensionKey);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <returns></returns>
        public MembersRow GetRoot(long dimensionKey)
        {
            MembersRow query = Members.First(x => x.DimKey == dimensionKey && x.ParentKey == 0);
            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public MembersRow GetFirstMember(string dimensionName, int level)
        {
            long dimKey = GetDimensionKey(dimensionName);
            return GetFirstMember(dimKey, level);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public MembersRow GetFirstMember(long dimensionKey, int level)
        {
            List<MembersRow> query = Members.Where(x => x.DimKey == dimensionKey && x.Level == level).OrderBy(y => y.MemberKey).Take(1).ToList();
            return query.Count > 0 ? query[0] : null;
        }

        /// <summary>
        /// Gets the Synthetic Members for a dimension
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <returns></returns>
        public List<string> GetSyntheticMembers(long dimensionKey)
        {
            if (!IsDimensionSynthetic(dimensionKey)) return null;

            MembersRow mr = GetRoot(dimensionKey);

            long key = mr.MemberKey;

            List<MembersRow> rows = GetChildren(dimensionKey, key);

            return rows.AsEnumerable().Select(x => x.MemberName).ToList();

        }

        /// <summary>
        /// Gets the Synthetic Members for a dimension
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public List<string> GetSyntheticMembers(string dimensionName)
        {
            if (!IsDimensionSynthetic(dimensionName)) return null;

            long dimKey = GetDimensionKey(dimensionName, false);
            if (dimKey < 0) return null;

            return GetSyntheticMembers(dimKey);

        }


        /// <summary>
        /// Performs a traversal starting at the member specifed. 
        /// Returns the lowest level members under the specified branch.
        /// </summary>
        /// <param name="dimensionName">Dimension to traverse</param>
        /// <param name="branchName">Member on tree where traversal will start. (if null the root member will be used)</param>
        /// <returns></returns>
        public List<MembersRow> GetFloorMembers(string dimensionName, string branchName = null)
        {
            long dimKey = GetDimensionKey(dimensionName, false);
            if (dimKey < 0) return null;

            long memberKey = 0;
            //If branch is null, then use the root as the starting branch.
            if (String.IsNullOrWhiteSpace(branchName))
            {
                var root = GetRoot(dimKey);
                if (root == null) return null;
                memberKey = root.MemberKey;
            }
            else
            {
                memberKey = GetMemberKey(branchName, dimKey);
                if (memberKey < 0) return null;
            }

            return GetFloorMembers(dimKey, memberKey);

        }

        /// <summary>
        /// Performs a traversal starting at the member specifed. 
        /// Returns the lowest level members under the specified branch.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberKey"></param>
        /// <returns></returns>
        private List<MembersRow> GetFloorMembers(long dimensionKey, long memberKey)
        {
            List<MembersRow> members = new List<MembersRow>();

            members = GetMembers(dimensionKey, memberKey, members);

            return members.AsEnumerable().Where(x => !x.HasChildren).ToList();

        }

        /// <summary>
        /// Performs a traversal starting at the member specifed. 
        /// Returns the lowest level member under the specified branch.
        /// </summary>
        /// <param name="dimensionName">Dimension to traverse</param>
        /// <param name="branchName">Member on tree where traversal will start. (if null the root member will be used)</param>
        /// <returns>The first floor member for the specified dimension and branch.</returns>
        public string GetFirstFloorMember(string dimensionName, string branchName = null)
        {
            List<MembersRow> members = GetFloorMembers(dimensionName, branchName);

            var firstFloorMbr = members.OrderBy(y => y.MemberKey).FirstOrDefault();

            return firstFloorMbr == null ? null : firstFloorMbr.MemberName;
        }

        /// <summary>
        /// Performs a traversal starting at the member specifed. 
        /// Returns the lowest level members under the specified branch.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="branchName"></param>
        /// <returns></returns>
        public List<string> GetFloorMembersName(string dimensionName, string branchName)
        {
            long dimKey = GetDimensionKey(dimensionName, false);
            if (dimKey < 0) return null;
            long memberKey = GetMemberKey(branchName, dimKey);
            if (memberKey < 0) return null;

            return GetFloorMembers(dimKey, memberKey).AsEnumerable().Where(x => !x.HasChildren).Select(x => x.MemberName).ToList();

        }

        /// <summary>
        /// Performs a traversal starting at the root node. 
        /// </summary>
        /// <returns>The members for all dimensions</returns>
        public Dictionary<string, List<string>> GetAllMembers(bool baseOnly = true)
        {
            Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
            List<string> dims = GetDimensionNames(baseOnly, false);
            foreach (string dim in dims)
            {
                long dimKey = GetDimensionKey(dim, false);
                result.Add(dim, GetAllMembers(dimKey));
            }
            return result;
        }

        /// <summary>
        /// Performs a traversal starting at the root node. 
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <returns></returns>
        public List<string> GetAllMembers(long dimensionKey)
        {
            List<MembersRow> members = new List<MembersRow>();

            long rootKey = GetRoot(dimensionKey).MemberKey;

            members = GetMembers(dimensionKey, rootKey, members);

            return members.AsEnumerable().Select(x => x.MemberName).ToList();
        }

        /// <summary>
        /// Performs a traversal starting at the member specifed. 
        /// Returns the lowest level members under the specified branch.
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="member"></param>
        /// <returns></returns>
        public List<MembersRow> GetAllMembers(string dimension, string member)
        {
            long dimensionKey = GetDimensionKey(dimension);
            long memberKey = GetMemberKey(member, dimensionKey);

            return GetAllLevelZeroMembers(dimensionKey, memberKey);
        }

        /// <summary>
        /// Performs a traversal starting at the member specifed. 
        /// Returns the lowest level members under the specified branch.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberKey"></param>
        /// <returns></returns>
        public List<MembersRow> GetAllLevelZeroMembers(long dimensionKey, long memberKey)
        {
            List<MembersRow> members = new List<MembersRow>();

            members = GetMembers(dimensionKey, memberKey, members);

            return members.AsEnumerable().Where(x => !x.HasChildren).ToList();

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<DimLevelGenInfo> GetDimensionInformation(string dimensionName, DimensionInfoType type)
        {
            long dimKey = GetDimensionKey(dimensionName, true);
            bool isLevel = type == DimensionInfoType.Level;
            var infos = DimensionLevelGeneration.Where(x => x.DimKey == dimKey && x.Level == isLevel).ToList();
            if (infos.Count == 0) return null;
            return infos.Select(info => new DimLevelGenInfo(info.DimensionRow.Name, info.Name, info.Number, type)).ToList();
        }

        /// <summary>
        /// Gets a members parent name.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="childMemberName"></param>
        /// <returns></returns>
        public string GetParentMemberName(string dimensionName, string childMemberName)
        {
            long dimKey = GetDimensionKey(dimensionName);
            long memberKey = GetMemberKey(childMemberName, dimKey);
            MembersRow mr = GetMember(dimKey, memberKey);
            if (mr == null || mr.MembersRowParent == null)
            {
                return null;
            }
            return mr.MembersRowParent.MemberName;
        }

        /// <summary>
        /// Gets a member row.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public MembersRow GetMember(string dimensionName, string memberName)
        {
            long dimensionKey = GetDimensionKey(dimensionName);
            long memberKey = GetMemberKey(memberName, dimensionKey);
            return GetMember(dimensionKey, memberKey);
        }

        /// <summary>
        /// Gets a member row.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public MembersRow GetMember(long dimensionKey, string memberName)
        {
            long memberKey = GetMemberKey(memberName, dimensionKey);
            return GetMember(dimensionKey, memberKey);
        }

        /// <summary>
        /// Gets a member row.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberKey"></param>
        /// <returns></returns>
        public MembersRow GetMember(long dimensionKey, long memberKey)
        {
            return Members.FirstOrDefault(x => x.DimKey == dimensionKey && x.MemberKey == memberKey);
        }

        /// <summary>
        /// Gets all the members from a specified member.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberKey"></param>
        /// <param name="memberList"></param>
        /// <param name="includeMember"></param>
        /// <returns></returns>
        public List<MembersRow> GetMembers(long dimensionKey, long memberKey, List<MembersRow> memberList, bool includeMember = true)
        {
            //search for the current node
            MembersRow query = Members.FirstOrDefault(x => x.DimKey == dimensionKey && x.MemberKey == memberKey);
            if (query == null) return memberList;

            //add the current node.
            if(includeMember) memberList.Add(query);

            //Get the children of the current node.
            List<MembersRow> children = GetChildren(query.DimKey, query.MemberKey);
            if (children.Count > 0)
            {
                //Loop thru the children and aggregate the members. 
                memberList = children.Aggregate(memberList, (current, row) => GetMembers(row.DimKey, row.MemberKey, current));
            }
            return memberList;

        }

        /// <summary>
        /// Gets all the descendants of the specified member.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public List<string> GetDescendants(string dimensionName, string memberName)
        {
            long dimensionKey = GetDimensionKey(dimensionName);
            long memberKey = GetMemberKey(memberName, dimensionKey);

            return GetDescendants(dimensionKey, memberKey);
        }

        /// <summary>
        /// Gets the children of the specified member.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public List<MembersRow> GetChildren(long dimensionKey, string memberName)
        {
            long memberKey = GetMemberKey(memberName, dimensionKey);
            return GetChildren(dimensionKey, memberKey);
        }

        /// <summary>
        /// Gets all the members at a specified level
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="level"></param>
        /// <param name="queryHidden"></param>
        /// <returns></returns>
        public List<string> GetMembersAtLevel(string dimension, int level, bool queryHidden = true)
        {
            long dimensionKey = GetDimensionKey(dimension, queryHidden );
            return Members.Where(x => x.DimKey == dimensionKey && x.Level == level).OrderBy(r => r.MemberKey).Select(m => m.MemberName).ToList();
        }

        /// <summary>
        /// Gets all the members at a specified level
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="generation"></param>
        /// <param name="queryHidden"></param>
        /// <returns></returns>
        public List<string> GetMembersAtGeneration(string dimension, int generation, bool queryHidden = true)
        {
            long dimensionKey = GetDimensionKey(dimension, queryHidden);
            return Members.Where(x => x.DimKey == dimensionKey && x.Generation == generation).OrderBy(r => r.MemberKey).Select(m => m.MemberName).ToList();
        }

        /// <summary>
        /// Gets an alias key for an alias name.
        /// </summary>
        /// <param name="name"></param>
        /// <returns>The key or -1 if the query results in no items found.</returns>
        public long GetAliasTableKey(string name)
        {

            long key;
            if (_aliasTableMap.TryGetValue(name, out key))
            {
                return key;
            }

            AliasTableRow tr = AliasTable.FirstOrDefault(x => x.TableName == name);

            if (tr != null)
            {
                _aliasTableMap.Add(name, tr.Key);
                return tr.Key;
            }
            return -1;

            //return tr == null ? -1 : tr.Key;
        }

        /// <summary>
        /// Gets an alias value for a table/member
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="memberKey"></param>
        /// <returns></returns>
        public string GetAliasValue(string tableName, long memberKey)
        {
            long aliasKey = GetAliasTableKey(tableName);

            var row = MemberAlias.FirstOrDefault(x => x.MemberKey == memberKey && x.AliasKey == aliasKey);
            return row != null ? row.Alias : String.Empty;
        }

        ///// <summary>
        ///// Get the alias values for a set of alias tables.
        ///// </summary>
        ///// <param name="dimKey">Dimension</param>
        ///// <param name="member">Member to find.</param>
        ///// <returns>A Dictionary of strings</returns>
        //public Dictionary<string, string> GetAliasValues(long dimKey, string member)
        //{
        //    Dictionary<string, string> aliasValues = new Dictionary<string, string>();
        //    List<string> aliasTables = GetAliasTables(dimKey);
        //    long memberKey = GetMemberKey(member, dimKey);

        //    foreach (string aliasTable in aliasTables)
        //    {
        //        string aliasValue = GetAliasValue(aliasTable, memberKey);
        //        if (!String.IsNullOrEmpty(aliasValue))
        //        {
        //            aliasValues.Add(aliasTable, aliasValue);
        //        }
        //    }

        //    return aliasValues;
        //}

        /// <summary>
        /// Gets the list of AliasTableNames for a specified dimension.
        /// </summary>
        /// <param name="dimemsionName">Name of the dimension</param>
        /// <param name="queryHidden">Query hidden dimensions</param>
        /// <returns></returns>
        public List<string> GetAliasTables(string dimemsionName, bool queryHidden = true)
        {
            long dimensionKey = GetDimensionKey(dimemsionName, queryHidden);

            return GetAliasTables(dimensionKey);
        }

        /// <summary>
        /// Gets the list of AliasTableNames for a specified dimension.
        /// </summary>
        /// <param name="dimensionKey">Name of the dimension</param>
        /// <returns></returns>
        public List<string> GetAliasTables(long dimensionKey)
        {
            var results = DimensionAlias.Where(x => x.DimKey == dimensionKey);

            return Enumerable.Select(results, result => result.AliasTableRow.TableName).ToList();
        }


        /// <summary>
        /// Gets the member alias value
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="member"></param>
        /// <param name="aliasTable"></param>
        /// <returns></returns>
        public string GetMemberAliasValue(string dimension, string member, string aliasTable)
        {
            long dimKey = GetDimensionKey(dimension);
            long memberKey = GetMemberKey(member, dimKey);
            long aliasKey = GetAliasTableKey(aliasTable);

            return GetMemberAliasValue(memberKey, aliasKey);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="memberKey"></param>
        /// <param name="aliasKey"></param>
        /// <returns>The alias value or null if the query results if not items are found.</returns>
        public string GetMemberAliasValue(long memberKey, long aliasKey)
        {
            MemberAliasRow tr = MemberAlias.FirstOrDefault(x => x.MemberKey == memberKey && x.AliasKey == aliasKey);
            return tr == null ? null : tr.Alias;
        }

        /// <summary>
        /// Gets a list of LockId's
        /// </summary>
        /// <param name="setStatus">Nullable. Lock status or null to return all.</param>
        /// <returns></returns>
        public List<long> GetLockIds(bool? setStatus)
        {
            return setStatus.HasValue ? LockStatus.Where(x => x.Set == setStatus.Value).Select(x => x.Id).ToList() : LockStatus.Select(x => x.Id).ToList();
        }

        /// <summary>
        /// Returns and intersection from an Session Lock Id.
        /// </summary>
        /// <param name="lockId"></param>
        /// <param name="dimPriority"></param>
        /// <returns></returns>
        public Intersection GetIntersection(long lockId, string[] dimPriority)
        {
            List<string> dimPriority1 = dimPriority.ToList();
            var query = LockIntersections.Where(x => x.LockId == lockId);

            foreach (var q in Enumerable.Where(query, q => !dimPriority1.Contains(q.MembersRowParent.DimensionRow.Name)))
            {
                dimPriority1.Add(q.MembersRowParent.DimensionRow.Name);
            }

            Intersection result = new Intersection(dimPriority1);

            foreach(var q in query)
            {
                result.SetCoordinate(q.MembersRowParent.DimensionRow.Name, q.MembersRowParent.MemberName);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="aliasKey"></param>
        /// <returns>The alias value or null if the query results if not items are found.</returns>
        public bool DimensionAliasRowExists(long dimensionKey, long aliasKey)
        {
            List<DimensionAliasRow> tr = DimensionAlias.Where(x => x.DimKey == dimensionKey && x.AliasKey == aliasKey).ToList();
            return tr.Count != 0;
        }

        /// <summary>
        /// Finds an intersection in the lock intersection table.
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="memberKey"></param>
        /// <param name="lockKey"></param>
        /// <returns></returns>
        public List<LockIntersectionsRow> FindIntersection(
            long dimKey,
            long memberKey,
            long lockKey)
        {
            return (from x in Enumerable.AsEnumerable(LockIntersections) where x.DimKey == dimKey && x.MemberKey == memberKey && x.LockId == lockKey select x).ToList();
        }

        /// <summary>
        /// Adds a lock intersection from a Pace Intersection object.
        /// </summary>
        /// <param name="intersection"></param>
        /// <param name="lockId"></param>
        /// <returns>true if the insert was successful, false if failed (and the transaction is rolled back)</returns>
        public bool AddLockIntersection(Intersection intersection, long lockId)
        {
            try
            {
                using (TransactionScope transaction = new TransactionScope())
                {
                    foreach (string dim in intersection.AxisSequence)
                    {
                        if(!AddLockIntersection(dim, intersection.GetCoordinate(dim), lockId))
                        {
                            throw new Exception(String.Format("Error creating lock intersection for dim: {0}, coord: {1}", new object[]{dim, intersection.GetCoordinate(dim)}));
                        }
                    }

                    transaction.Complete();
                }
                return true;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                return false;
            }
        }

        /// <summary>
        /// Adds a lock intersection.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberName"></param>
        /// <param name="lockId"></param>
        public bool AddLockIntersection(string dimensionName, string memberName, long lockId)
        {
            long dimKey = GetDimensionKey(dimensionName, false);
            if (dimKey <= 0) return false;

            long memberKey = GetMemberKey(memberName, dimKey);
            if (memberKey <= 0) return false;

            return AddLockIntersection(dimKey, memberKey, lockId);
        }

        /// <summary>
        /// Adds a lock intersection.
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="memberKey"></param>
        /// <param name="lockId"></param>
        public bool AddLockIntersection(long dimKey, long memberKey, long lockId)
        {
            LockIntersectionsRow row = LockIntersections.NewLockIntersectionsRow();
            row.DimKey = dimKey;
            row.MemberKey = memberKey;
            row.LockId = lockId;
            LockIntersections.AddLockIntersectionsRow(row);

            return true;
        }

        /// <summary>
        /// Adds a member alias row.
        /// </summary>
        /// <param name="aliasKey"></param>
        /// <param name="memberKey"></param>
        /// <param name="aliasValue"></param>
        public void AddMemberAliasRow(long aliasKey, long memberKey, string aliasValue)
        {
            if (GetMemberAliasValue(memberKey, aliasKey) == null)
            {
                MemberAliasRow row = MemberAlias.NewMemberAliasRow();
                row.AliasKey = aliasKey;
                row.MemberKey = memberKey;
                row.Alias = aliasValue;
                MemberAlias.AddMemberAliasRow(row);
            }
        }

        /// <summary>
        /// Clears the Lock Status table.
        /// Because of relations this will cascase to the LockIntersections table.
        /// </summary>
        public void ClearLockStatusTable()
        {
            LockIntersections.Clear();
            LockStatus.Clear();
        }

        /// <summary>
        /// Add a lock status row.
        /// </summary>
        /// <param name="enabled"></param>
        /// <param name="key"></param>
        /// <param name="autoGenerateKey"></param>
        /// <returns></returns>
        public long AddLockStatus(bool enabled, long key, bool autoGenerateKey)
        {
            if(autoGenerateKey)
            {
                if(LockStatus.Count == 0)
                {
                    key = 1;
                }
                else
                {
                    long tempKey = LockStatus.DefaultIfEmpty().Max(x => x.Id);
                    key = tempKey + 1; 
                }
            }

            LockStatus.AddLockStatusRow(key, enabled);
            return key;
        }

        /// <summary>
        ///  Adds a member to the table.
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="parentKey"></param>
        /// <param name="memberName"></param>
        /// <param name="memberProps"></param>
        /// <param name="hasChildren"></param>
        /// <param name="aliasValues"></param>
        /// <returns></returns>
        public long AddMember(long dimKey, long? parentKey, string memberName, pafSimpleDimMemberProps memberProps, bool hasChildren, Dictionary<long, string> aliasValues)
        {

            long key = parentKey != null ? GetMemberKey(memberName, dimKey, parentKey.GetValueOrDefault()) : GetMemberKey(memberName, dimKey);

            if (key <= 0)
            {
                MembersRow row = Members.NewMembersRow();
                row.DimKey = dimKey;
                row.MemberName = memberName;
                row.Level = memberProps.levelNumber;
                row.Generation = memberProps.generationNumber;
                row.HasChildren = hasChildren;
                row.ReadOnly = memberProps.readOnly;
                row.Synthetic = memberProps.synthetic;
                row.TimeBalance = memberProps.timeBalanceOption;
                row.TwoPassCalc = memberProps.twoPassCalc;

                if (parentKey != null)
                {
                    row.ParentKey = parentKey.GetValueOrDefault();
                }
                Members.Rows.Add(row);

                //key = GetMemberKey(memberName, dimKey);
                key = row.MemberKey;

                if (aliasValues != null && aliasValues.Keys.Count > 0)
                {
                    foreach (KeyValuePair<long, string> kvp in aliasValues)
                    {
                        AddMemberAliasRow(kvp.Key, key, kvp.Value);
                    }
                }
            }

            return key;
        }

        /// <summary>
        /// Adds an alias table.
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public long AddAliasTable(string tableName)
        {
            long aliasKey = GetAliasTableKey(tableName);
            if(aliasKey <= 0)
            {
                AliasTableRow ar = AliasTable.AddAliasTableRow(tableName);
                aliasKey = ar.Key;
            }
            return aliasKey;
        }

        /// <summary>
        /// Add the alias to the alias table and the DimensionAlias table.
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="tableName"></param>
        public void AddAliasTables(long dimKey, string[] tableName)
        {
            foreach (string s in tableName)
            {
                long aliasKey = AddAliasTable(s);
                AddDimensionAlias(dimKey, aliasKey);
            }
        }

        /// <summary>
        /// Adds a dimension alias to the table.
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="aliasKey"></param>
        public void AddDimensionAlias(long dimKey, long aliasKey)
        {
            if (!DimensionAliasRowExists(dimKey, aliasKey))
            {
                DimensionAliasRow dar = DimensionAlias.NewDimensionAliasRow();
                dar.DimKey = dimKey;
                dar.AliasKey = aliasKey;
                DimensionAlias.AddDimensionAliasRow(dar);
            }
        }

        /// <summary>
        /// Adds a dimension to the table.
        /// </summary>
        /// <param name="dimName"></param>
        /// <param name="isBaseDimension"></param>
        /// <param name="levelZeroOnly"></param>
        /// <param name="isDiscontiguous"></param>
        /// <param name="parentDimKey"></param>
        /// <param name="hidden">True if the dim is hidden i.e.</param>
        /// <returns></returns>
        public long AddDimension(string dimName, bool isBaseDimension, bool levelZeroOnly, bool isDiscontiguous, long? parentDimKey, bool hidden)
        {
            long dimKey = GetDimensionKey(dimName, false);
            if (dimKey <= 0)
            {
                DimensionRow row = Dimension.NewDimensionRow();
                row.Name = dimName;
                row.BaseDimension = isBaseDimension;
                row.LevelZeroOnly = levelZeroOnly;
                row.Synthetic = isDiscontiguous;
                row.Hidden = hidden;

                if (parentDimKey != null)
                {
                    row.ParentKey = parentDimKey.GetValueOrDefault();
                }
                Dimension.Rows.Add(row);
                

                //key = GetMemberKey(memberName, dimKey);
                //DimensionRow row = Dimension.AddDimensionRow(dimName, isBaseDimension, levelZeroOnly, isDiscontiguous);
                dimKey = row.Key;
            }
            return dimKey;
        }

        /// <summary>
        /// Gets a data table with the MemberName and any attributes value columns.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="queryHidden"></param>
        /// <returns></returns>
        public DataTable ToMeberAliasDataTable(string dimensionName, bool queryHidden)
        {
            DataTable dt = ToTreeListDataTable(dimensionName);
            dt.PrimaryKey = null;
            //Get the alias tables.
            List<string> tables = GetAliasTables(dimensionName, queryHidden);
            tables.Add(Members.MemberNameColumn.ColumnName);

            for (int i = dt.Columns.Count - 1; i >= 0; i--)
            {
                if (!tables.Contains(dt.Columns[i].ColumnName))
                {
                    dt.Columns.Remove(dt.Columns[i]);
                }
            }

            //DataColumn key = dt.GetDataColumn(Members.MemberNameColumn.ColumnName);
            //if (key != null)
            //{
            //    dt.PrimaryKey = new[] { key };
            //}

            return dt;
        }

        public DataTable ToTreeListDataTable(List<string> dimensions)
        {
            List<DataTable> ds = new List<DataTable>(5);
            ds.AddRange(dimensions.Select(attribute => ToTreeListDataTable(attribute)));
            DataTable merge = new DataTable();
            foreach (DataTable dt in ds)
            {
                merge.Merge(dt);
            }
            return merge;}

        /// <summary>
        /// Returns a DataTable that can be directly bound to a XtraTreeList and adds an additional column
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="columnName"></param>
        /// <param name="columnType"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public DataTable ToTreeListDataTable(string dimensionName, string columnName, Type columnType, object value)
        {
            DataTable dt = ToTreeListDataTable(dimensionName);
            DataColumn dc = new DataColumn(columnName, columnType);
            dc.DefaultValue = value;
            dt.Columns.Add(dc);

            return dt;

        }

        /// <summary>
        /// Returns a DataTable that can be directly bound to a XtraTreeList
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        public DataTable ToTreeListDataTable(string dimensionName)
        {
            long dimKey = GetDimensionKey(dimensionName, true);

            DataTable mainTable = new DataTable { TableName = dimensionName };
            DataColumn column1 = new DataColumn("MemberKey", typeof(Int64));
            DataColumn column2 = new DataColumn("ParentKey", typeof(Int64));
            mainTable.Columns.Add(column1);
            mainTable.Columns.Add(column2);
            mainTable.PrimaryKey = new[] { column1, column2 };
            var tables = GetAliasTables(dimensionName, true);

            //Add the new columns.
            foreach (var table in tables)
            {
                string tableName = table;
                DataTable temp =  (from x in MemberAlias.AsEnumerable()
                        where x.AliasTableRow.TableName.Equals(tableName) &&
                                x.MembersRow.DimKey == dimKey
                        select new
                        {
                            x.MemberKey,
                            x.MembersRow.ParentKey,
                            x.MembersRow.MemberName,
                            x.MembersRow.Level,
                            x.MembersRow.Generation,
                            x.Alias
                        }).CopyToDataTableEx(null, null);
                
                DataColumn dc = temp.Columns["Alias"];
                dc.Caption = tableName;
                dc.ColumnName = tableName;

                mainTable.Merge(temp);
            }
            return mainTable;
        }

        /// <summary>
        /// Converts the dataset to a bindable grid dataset.
        /// </summary>
        /// <returns></returns>
        public DataTable ToGridDataTable(string tableName)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            //DataSet finalDs = new DataSet();
            DataTable mainTable = new DataTable { TableName = tableName };
            DataColumn column1 = new DataColumn("Id", typeof(Int64))
                {AutoIncrement = true, AutoIncrementSeed = 1, AutoIncrementStep = 1, ReadOnly = true};
            DataColumn column2 = new DataColumn("Set", typeof(Boolean));
            mainTable.Columns.Add(column1);
            mainTable.Columns.Add(column2);

            // Set the primary key column.
            mainTable.PrimaryKey = new[] { column1 };

            foreach (var dim in GetDimensionNames())
            {
                string dimName = dim;
                var query =
                    (from results in LockIntersections.AsEnumerable()
                     where results.MembersRowParent.DimensionRow.Name == dimName
                     select new
                     {
                         results.LockRowsRow.Set,
                         results.LockRowsRow.Id,
                         results.MembersRowParent.MemberName
                     });


                DataTable dt = query.CopyToDataTableEx(null, null);
                dt.TableName = "Temp";

                DataColumn dc = dt.Columns[MemberColumName];
                dc.Caption = dimName;
                dc.ColumnName = dimName;

                mainTable.Merge(dt);
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ToGridDataTable runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

            return mainTable;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="memberKey"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private MembersRow GetLesserOffset(long dimKey, long memberKey, int level)
        {
            return Members.Where(x => x.DimKey == dimKey && x.MemberKey < memberKey && x.Level == level).OrderByDescending(x => x.MemberKey).FirstOrDefault();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="memberKey"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        private MembersRow GetGreatorOffset(long dimKey, long memberKey, int level)
        {
            return Members.Where(x => x.DimKey == dimKey && x.MemberKey > memberKey && x.Level == level).OrderBy(x => x.MemberKey).FirstOrDefault();
        }


        #region Pivate Members

        /// <summary>
        /// Gets a hash of DimName and MemberName
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public string GetMemberHash(long dimensionKey, string memberName)
        {
            return String.Concat(dimensionKey, memberName);
        }

        /// <summary>
        /// Gets a hash of DimName and MemberName
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public string GetMemberHash(string dimensionName, string memberName)
        {
            long dimensionKey = GetDimensionKey(dimensionName);
            return GetMemberHash(dimensionKey, memberName);
        }

        /// <summary>
        /// Get the alias values for a set of alias tables.
        /// </summary>
        /// <param name="pafTree">Server Simple Tree</param>
        /// <param name="member">Member to find.</param>
        /// <returns>A Dictionary of strings</returns>
        private Dictionary<long, string> GetAliasValues(pafSimpleDimTree pafTree, pafSimpleDimMember member)
        {
            Dictionary<long, string> aliasValues = new Dictionary<long, string>();
            IEnumerable<string> aliasTables = pafTree.aliasTableNames;

            foreach (string aliasTable in aliasTables)
            {
                string aliasValue = pafTree.GetAlias(member, aliasTable);
                if (!String.IsNullOrEmpty(aliasValue))
                {
                    aliasValues.Add(GetAliasTableKey(aliasTable), aliasValue.Trim());
                }
            }

            return aliasValues;
        }

        /// <summary>
        /// Add the tree information record.
        /// </summary>
        /// <param name="dimKey"></param>
        /// <param name="name"></param>
        /// <param name="number"></param>
        /// <param name="isLevel"></param>
        private void AddTreeInformation(long dimKey, string name, int number, bool isLevel = true)
        {
            var row = DimensionLevelGeneration.NewDimensionLevelGenerationRow();
            row.DimKey = dimKey;
            row.Name = name;
            row.Number = number;
            row.Level = isLevel;
            DimensionLevelGeneration.AddDimensionLevelGenerationRow(row);
        }


        /// <summary>
        /// Gets the children of the specified member.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberKey"></param>
        /// <returns></returns>
        private List<MembersRow> GetChildren(long dimensionKey, long memberKey)
        {
            return Members.Where(x => x.DimKey == dimensionKey && x.ParentKey == memberKey).ToList();
        }

        /// <summary>
        /// Gets all the descendants of the specified member.
        /// </summary>
        /// <param name="dimensionKey"></param>
        /// <param name="memberKey"></param>
        /// <returns></returns>
        private List<string> GetDescendants(long dimensionKey, long memberKey)
        {
            List<MembersRow> members = new List<MembersRow>();

            members = GetMembers(dimensionKey, memberKey, members, false);

            return members.AsEnumerable().Select(x => x.MemberName).ToList();
        }


        #endregion Private Members

        #region Internal Members

        /// <summary>
        /// Returns and intersection from an Session Lock Id.
        /// </summary>
        /// <param name="lockId"></param>
        /// <param name="dimPriority"></param>
        /// <returns></returns>
        internal CoordinateSet GetCoordinate(long lockId, string[] dimPriority)
        {
            List<string> dimPriority1 = new List<string>();
            var query = LockIntersections.Where(x => x.LockId == lockId);

            //add base dims
            foreach (string dim in dimPriority)
            {
                string dimName = dim;
                if (query.Count(x => x.MembersRowParent.DimensionRow.Name == dimName) > 0)
                {
                    dimPriority1.Add(dim);
                }
            }

            //add any extra dimensions.
            foreach (var q in Enumerable.Where(query, q => !dimPriority1.Contains(q.MembersRowParent.DimensionRow.Name)))
            {
                dimPriority1.Add(q.MembersRowParent.DimensionRow.Name);
            }


            List<string> priority = dimPriority1.ToList();
            CoordinateSet cd = new CoordinateSet(dimPriority1);
            Coordinate coord = new Coordinate(priority.Count);

            foreach (var q in query)
            {
                //result.SetCoordinate(q.MembersRowParent.DimensionRow.Name, q.MembersRowParent.MemberName);
                int pos = priority.FindIndex(x => x.Equals(q.MembersRowParent.DimensionRow.Name));
                coord.SetCoordinate(pos, q.MembersRowParent.MemberName);
            }

            cd.AddCoordinate(coord);

            return cd;
        }

        /// <summary>
        /// Returns and intersection from an Session Lock Id.
        /// </summary>
        /// <param name="lockId"></param>
        /// <returns></returns>
        internal bool HasAttributes(long lockId)
        {
            var query = LockIntersections.Where(x => x.LockId == lockId);

            //add base dims
            foreach (var q in query)
            {
                if (!q.MembersRowParent.DimensionRow.BaseDimension)
                {
                    return true;
                }
            }

            return false;
        }


        #endregion Internal Members
    }
}
