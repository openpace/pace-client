#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Drawing;
using System.IO;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Extensions;
using Titan.Pace.DataStructures;
using Titan.PafService;

namespace Titan.Pace.Application
{
    internal class PafAppSettingsHelpers
    {
        private readonly ColorUtilities _cu;

        public PafAppSettingsHelpers ()
        {
            _cu = new ColorUtilities();
        }

        /// <summary>
        /// Gets the full path to the saved role filter serialized xml
        /// </summary>
        /// <returns></returns>
        private string RoleFilterSavedSelectionsFile()
        {
            if (!Directory.Exists(PafAppConstants.SETTINGS_PATH))
            {
                Directory.CreateDirectory(PafAppConstants.SETTINGS_PATH);
            }
            return Path.Combine(PafAppConstants.SETTINGS_PATH, PafAppConstants.ROLE_FILTER_FILE_NAME);
        }

        /// <summary>
        /// Gets the role filter saved selections
        /// </summary>
        /// <returns></returns>
        public SavedFilterSelections RoleFilterSavedSelections()
        {

            //Check the global.
            if (!PersistRoleFilterSelectionsEnabled())
            {
                PafApp.GetLogger().Warn("Role filter persist selections (isGlobalSaveRoleFilterSelectionsEnabled) is disabled on server.  No settings will be loaded or saved.");
                return new SavedFilterSelections();
            }

            PafApp.GetLogger().InfoFormat("Loading role filter saved setting from: {0}", RoleFilterSavedSelectionsFile());

            var result = ObjectExtension.Load<SavedFilterSelections>(RoleFilterSavedSelectionsFile());
            if (result != null)
            {
                result.PostDeserialize();
            }

            return result ?? new SavedFilterSelections();
        }

        /// <summary>
        /// Sets (saves) the Role Filter Saved Selections
        /// </summary>
        /// <param name="selections">Dictionary saved selections</param>
        public void SaveRoleFilterSavedSelections(SavedFilterSelections selections)
        {

            //Check the global.
            if (!PersistRoleFilterSelectionsEnabled())
            {
                PafApp.GetLogger().Warn("Role filter persist selections (isGlobalSaveRoleFilterSelectionsEnabled) is disabled on server.  No settings will be saved.");
                return;
            }

            if (selections == null) return;

            if (selections.IgnoreAllDimensions)
            {
                selections.ClearAllKeyValues();
            }


            selections.PreSerialize();

            PafApp.GetLogger().InfoFormat("Saving role filter saved setting to: {0}", RoleFilterSavedSelectionsFile());

            selections.Save(RoleFilterSavedSelectionsFile());
        }

        /// <summary>
        /// Gets the full path to the saved user selections serialized xml
        /// </summary>
        /// <returns></returns>
        private string UserSelectionsSavedSelectionsFile()
        {
            if (!Directory.Exists(PafAppConstants.SETTINGS_PATH))
            {
                Directory.CreateDirectory(PafAppConstants.SETTINGS_PATH);
            }
            return Path.Combine(PafAppConstants.SETTINGS_PATH, PafAppConstants.USER_SEL_FILE_NAME);
        }


        /// <summary>
        /// Gets the role filter saved selections
        /// </summary>
        /// <returns></returns>
        public SavedFilterSelections UserSelectorSavedSelections()
        {

            //Check the global.
            if (!PersistUserSelectionSelectionsEnabled())
            {
                PafApp.GetLogger().Warn("Persist user selections (isGlobalSaveUserSelectorSelectionsEnabled) is disabled on server.  No settings will be loaded or saved.");
                return new SavedFilterSelections();
            }

            PafApp.GetLogger().InfoFormat("Loading user selections settings from: {0}", UserSelectionsSavedSelectionsFile());

            var result = ObjectExtension.Load<SavedFilterSelections>(UserSelectionsSavedSelectionsFile());
            if (result != null)
            {
                result.PostDeserialize();
            }

            return result ?? new SavedFilterSelections();
        }

        /// <summary>
        /// Sets (saves) the User Selector Saved Selections
        /// </summary>
        /// <param name="selections">Dictionary saved selections</param>
        public void SaveUserSelectorSavedSelections(SavedFilterSelections selections)
        {

            //Check the global.
            if (!PersistUserSelectionSelectionsEnabled())
            {
                PafApp.GetLogger().Warn("Persist user selections (isGlobalSaveUserSelectorSelectionsEnabled) is disabled on server.  No settings will be saved.");
                return;
            }

            if (selections == null) return;

            selections.PreSerialize();

            PafApp.GetLogger().InfoFormat("Saving user selections settings to: {0}", UserSelectionsSavedSelectionsFile());

            selections.Save(UserSelectionsSavedSelectionsFile());
        }

        /// <summary>
        /// Returns the globalSessionLocksEnabled server property.
        /// </summary>
        /// <returns></returns>
        public bool GetGlobalSessionLockEnabled()
        {
            return PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalSessionLocksEnabled;
        }

        /// <summary>
        /// Returns the globalSaveUserSelectorSelectionsEnabled server property.
        /// </summary>
        /// <returns></returns>
        public bool PersistUserSelectionSelectionsEnabled()
        {
            return PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalSaveUserSelectorSelectionsEnabled;
        }

        /// <summary>
        /// Returns the globalSaveRoleFilterSelectionsEnabled server property.
        /// </summary>
        /// <returns></returns>
        public bool PersistRoleFilterSelectionsEnabled()
        {
            return PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalSaveRoleFilterSelectionsEnabled;
        }

        /// <summary>
        /// Gets the global suppress zero columns suppress zero setting from the server.
        /// </summary>
        /// <returns>the setting if the settings object exists, if not false is returned.</returns>
        public bool GetGlobalSuppressZeroColumnsSuppressed()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalSuppressZeroSettings != null)
            {
                if (PafApp.GetPafAppSettings().globalSuppressZeroSettings.columnsSuppressedSpecified)
                {
                    return PafApp.GetPafAppSettings().globalSuppressZeroSettings.columnsSuppressed;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the global suppress zero row suppress zero setting from the server.
        /// </summary>
        /// <returns>the setting if the settings object exists, if not false is returned.</returns>
        public bool GetGlobalSuppressZeroRowsSuppressed()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalSuppressZeroSettings != null)
            {
                if (PafApp.GetPafAppSettings().globalSuppressZeroSettings.rowsSuppressedSpecified)
                {
                    return PafApp.GetPafAppSettings().globalSuppressZeroSettings.rowsSuppressed;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the global suppress visible suppress zero setting from the server.
        /// </summary>
        /// <returns>the setting if the settings object exists, if not false is returned.</returns>
        public bool GetGlobalSuppressZeroVisible()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalSuppressZeroSettings != null)
            {
                if (PafApp.GetPafAppSettings().globalSuppressZeroSettings.visibleSpecified)
                {
                    return PafApp.GetPafAppSettings().globalSuppressZeroSettings.visible;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the global suppress enabled suppress zero setting from the server.
        /// </summary>
        /// <returns>the setting if the settings object exists, if not false is returned.</returns>
        public bool GetGlobalSuppressZeroEnabled()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalSuppressZeroSettings != null)
            {
                if (PafApp.GetPafAppSettings().globalSuppressZeroSettings.enabledSpecified)
                {
                    return PafApp.GetPafAppSettings().globalSuppressZeroSettings.enabled;
                }
            }
            return false;
        }

        /// <summary>
        /// Gets the global alias table value.
        /// </summary>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>the alias table value, or the default constant if none exists.</returns>
        public string GetGlobalAliasTableName(string dimName)
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalAliasMappings != null)
            {
                foreach (aliasMapping am in PafApp.GetPafAppSettings().globalAliasMappings)
                {
                    if (am != null && am.dimName != null)
                    {
                        if (am.dimName.ToLower().Equals(dimName.ToLower()))
                        {
                            return am.aliasTableName;
                        }
                    }
                }
            }
            return PafAppConstants.ALIAS_DEFAULT_TABLE_NAME;
        }

        /// <summary>
        /// Gets primary row column format.
        /// </summary>
        /// <param name="dimName">Name of the dimension.</param>
        /// <returns>the primary row/col format value, or the default constant if none exists.</returns>
        public string GetPrimaryRowColumnFormat(string dimName)
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().globalAliasMappings != null)
            {
                foreach (aliasMapping am in PafApp.GetPafAppSettings().globalAliasMappings)
                {
                    if (am != null && am.dimName != null)
                    {
                        if (am.dimName.ToLower().Equals(dimName.ToLower()))
                        {
                            return am.primaryRowColumnFormat;
                        }
                    }
                }
            }
            return String.Empty;
        }

        /// <summary>
        /// Gets the foward plannable protected color.
        /// </summary>
        /// <returns>The foward plannable protected color.</returns>
        public Color GetForwardPlannableProtectedColor()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appColors != null &&
                !String.IsNullOrEmpty(PafApp.GetPafAppSettings().appColors.forwardPlannableProtectedColor))
            {
                Color c = _cu.HexStringToColor(PafApp.GetPafAppSettings().appColors.forwardPlannableProtectedColor);
                if(! _cu.ValidateBackColor(c))
                {
                    return PafAppConstants.FORWARD_PLANNABLE_PROTECTED_COLOR;
                }
                else
                {
                    return c;
                }
            }
            else
            {
                return PafAppConstants.FORWARD_PLANNABLE_PROTECTED_COLOR;
            }
        }

        /// <summary>
        /// Gets the non plannable protected color.
        /// </summary>
        /// <returns>The non plannable protected color.</returns>
        public Color GetNonPlannableProtectedColor()
        {
            try
            {
                if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appColors != null &&
                    !String.IsNullOrEmpty(PafApp.GetPafAppSettings().appColors.nonPlannableProtectedColor))
                {
                    Color c = _cu.HexStringToColor(PafApp.GetPafAppSettings().appColors.nonPlannableProtectedColor);
                    if(! _cu.ValidateBackColor(c))
                    {
                        return PafAppConstants.NON_PLANNABLE_PROTECTED_COLOR;
                    }
                    else
                    {
                        return c;
                    }
                }
                else
                {
                    return PafAppConstants.NON_PLANNABLE_PROTECTED_COLOR;
                }
            }
            catch(Exception)
            {
                return PafAppConstants.NON_PLANNABLE_PROTECTED_COLOR;
            }
        }

        /// <summary>
        /// Gets the protected color.
        /// </summary>
        /// <returns>The protected color.</returns>
        public Color GetProtectedColor()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appColors != null &&
                !String.IsNullOrEmpty(PafApp.GetPafAppSettings().appColors.protectedColor))
            {
                Color c = _cu.HexStringToColor(PafApp.GetPafAppSettings().appColors.protectedColor);
                if(! _cu.ValidateBackColor(c))
                {
                    return PafAppConstants.PROTECTED_COLOR;
                }
                else
                {
                    return c;
                }
            }
            else
            {
                return PafAppConstants.PROTECTED_COLOR;
            }
        }

        /// <summary>
        /// Gets the system lock color.
        /// </summary>
        /// <returns>The system lock color.</returns>
        public Color GetSystemLockColor()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appColors != null &&
                !String.IsNullOrEmpty(PafApp.GetPafAppSettings().appColors.systemLockColor))
            {
                Color c = _cu.HexStringToColor(PafApp.GetPafAppSettings().appColors.systemLockColor);
                if(! _cu.ValidateBackColor(c))
                {
                    return PafAppConstants.SYSTEM_LOCK_COLOR;
                }
                else
                {
                    return c;
                }
            }
            else
            {
                return PafAppConstants.SYSTEM_LOCK_COLOR;
            }
        }

        /// <summary>
        /// Gets the user lock color.
        /// </summary>
        /// <returns>The user lock color.</returns>
        public Color GetUserLockColor()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appColors != null &&
                !String.IsNullOrEmpty(PafApp.GetPafAppSettings().appColors.userLockColor))
            {
                Color c = _cu.HexStringToColor(PafApp.GetPafAppSettings().appColors.userLockColor);
                if(! _cu.ValidateBackColor(c))
                {
                    return PafAppConstants.USER_LOCK_COLOR;
                }
                else
                {
                    return c;
                }
            }
            else
            {
                return PafAppConstants.USER_LOCK_COLOR;
            }
        }

        /// <summary>
        /// Gets the session lock color.
        /// </summary>
        /// <returns>The user lock color.</returns>
        public Color GetSessionLockColor()
        {
            #if DEBUG
                return Color.YellowGreen;
            #endif
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appColors != null &&
                !String.IsNullOrEmpty(PafApp.GetPafAppSettings().appColors.userLockColor))
            {
                Color c = _cu.HexStringToColor(PafApp.GetPafAppSettings().appColors.userLockColor);
                if (!_cu.ValidateBackColor(c))
                {
                    return PafAppConstants.USER_LOCK_COLOR;
                }
                else
                {
                    return c;
                }
            }
            else
            {
                return PafAppConstants.USER_LOCK_COLOR;
            }
        }

        /// <summary>
        /// Gets the member tag changed color.
        /// </summary>
        /// <returns>The user lock color.</returns>
        public Color GetMemberTagChangedColor()
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appColors != null &&
                !String.IsNullOrEmpty(PafApp.GetPafAppSettings().appColors.noteColor))
            {
                Color c = _cu.HexStringToColor(PafApp.GetPafAppSettings().appColors.noteColor);
                if (!_cu.ValidateBackColor(c))
                {
                    return PafAppConstants.MEMBER_TAG_COLOR;
                }
                else
                {
                    return c;
                }
            }
            else
            {
                return PafAppConstants.MEMBER_TAG_COLOR;
            }
        }
    }   
}