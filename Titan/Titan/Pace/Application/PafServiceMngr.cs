#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web.Services.Protocols;
using Titan.Pace.Application.Compression.PafSimpleCellNotes;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Base.Data;
using Titan.Pace.DataStructures;
using Titan.PafService;

namespace Titan.Pace.Application
{
    internal class PafServiceMngr
    {
        //Store the pafView for each View - modified by sorting
        private readonly Dictionary<string, pafView> _PafViews  = new Dictionary<string, pafView>();

        //Store the Row Tuples from the Paf View for each view - never modified 
        private readonly Dictionary<string, viewTuple[]> _RowTuples = new Dictionary<string, viewTuple[]>();

        //Store the Col Tuples from the Paf View for each view - never modified 
        private readonly Dictionary<string, viewTuple[]> _ColTuples = new Dictionary<string, viewTuple[]>();

        /// <summary>
        /// Evaluate a view on the PafServer.
        /// </summary>
        /// <param name="dataSlice">The data slice to evaluate.</param>
        /// <param name="changedCells">List of cells that changed on the data slice.</param>
        /// <param name="lockedCells">List of locked cells on the data slice.</param>
        /// <param name="protectedCells">List of protected cells on the data slice.</param>
        /// <param name="replicateAllCells">List of cells to replicate "All" on the server.</param>
        /// <param name="replicateExistingCells">List of cells to replicate "Existing" on the server.</param>
        /// <param name="liftAllCells">List of cells to lift "All" on the server.</param>
        /// <param name="liftExistingCells">List of cells to lift "Existing" on the server.</param>
        /// <param name="sessionLockedCells"></param>
        /// <param name="protectedFormulas">String array of protected formulas on the data slice.</param>
        /// <param name="sessionToken">The session token provided by the server.</param>
        /// <param name="ruleSet">The session token provided by the server.</param>
        /// <param name="clientId">Client id provided by the server.</param>
        /// <param name="viewName">Name of the view to evaluate.</param>
        /// <param name="rowsSuppressed">Are the rows to be suppressed on the view.</param>
        /// <param name="columnsSuppressed">Are the columns to be suppressed on the view.</param>
        /// <param name="outPafView">The pafView that is returned if the evaluate is dirty.</param>
        /// <returns>A pafDataSlice with the updated information.</returns>
        public pafDataSlice EvaluateView(pafDataSlice dataSlice, simpleCoordList changedCells,
            simpleCoordList lockedCells, simpleCoordList protectedCells, simpleCoordList replicateAllCells,
            simpleCoordList replicateExistingCells, simpleCoordList liftAllCells,
            simpleCoordList liftExistingCells, simpleCoordList[] sessionLockedCells,
            string[] protectedFormulas, string ruleSet, string sessionToken,
            string clientId, string viewName, bool rowsSuppressed,bool columnsSuppressed, out pafView outPafView)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            //init the outPafView
            outPafView = null;
            //create the view request.
            evaluateViewRequest evalviewrequest = new evaluateViewRequest();
            //set the view request dataslice.
            evalviewrequest.dataSlice = dataSlice;
            //set the view request properties.
            evalviewrequest.sessionToken = sessionToken;
            //set the columns suppressed.
            evalviewrequest.columnsSuppressed = columnsSuppressed;
            //set the rows suppressed.
            evalviewrequest.rowsSuppressed = rowsSuppressed;
            //Set the client id.
            evalviewrequest.clientId = clientId;
            //Set the view name.
            evalviewrequest.viewName = viewName;
            //Set the Attribute Evaluation Session Locks.
            if (sessionLockedCells != null && sessionLockedCells.Length > 0)
            {
                evalviewrequest.sessionLockedCells = sessionLockedCells;
            }
            //set the view request changed cells
            if (changedCells != null)
            {
                if (!changedCells.compressed)
                {
                    PafSimpleCoordList pscl = new PafSimpleCoordList(changedCells);
                    int len = pscl.NumberOfCoordinates();
                    pscl.compressData();
                    evalviewrequest.changedCells = pscl.GetSimpleCoordList;
                    evalviewrequest.changedCells.axis = null;
                    evalviewrequest.changedCells.coordinates = null;
                    PafApp.GetLogger().InfoFormat("evaluateView changedCells collection size: {0}", new object[] {len});
                }
                else
                {
                    evalviewrequest.changedCells = changedCells;
                    //PafApp.GetLogger().InfoFormat("evaluateView changedCells collection size: {0}", new object[] { changedCells. });
                }

            }
            else
            {
                evalviewrequest.changedCells = null;
            }

            //set the view request locked cells.
            if (lockedCells != null)
            {
                if (!lockedCells.compressed)
                {
                    PafSimpleCoordList pscl = new PafSimpleCoordList(lockedCells);
                    int len = pscl.NumberOfCoordinates();
                    pscl.compressData();
                    evalviewrequest.lockedCells = pscl.GetSimpleCoordList;
                    evalviewrequest.lockedCells.axis = null;
                    evalviewrequest.lockedCells.coordinates = null;
                    PafApp.GetLogger().InfoFormat("evaluateView lockedCells collection size: {0}", new object[] {len});
                }
                else
                {
                    evalviewrequest.lockedCells = lockedCells;
                }
            }
            else
            {
                evalviewrequest.lockedCells = null;
            }


            //set the view request protected cells.
            if (protectedCells != null)
            {
                if (!protectedCells.compressed)
                {
                    PafSimpleCoordList pscl = new PafSimpleCoordList(protectedCells);
                    int len = pscl.NumberOfCoordinates();
                    pscl.compressData();
                    evalviewrequest.protectedCells = pscl.GetSimpleCoordList;
                    evalviewrequest.protectedCells.axis = null;
                    evalviewrequest.protectedCells.coordinates = null;
                    PafApp.GetLogger().InfoFormat("evaluateView protectedCells collection size: {0}", new object[] {len});
                }
                else
                {
                    evalviewrequest.protectedCells = protectedCells;
                }

            }
            else
            {
                evalviewrequest.protectedCells = null;
            }

            //set the replicate all cells.
            if (replicateAllCells != null)
            {
                if (!replicateAllCells.compressed)
                {
                    PafSimpleCoordList pscl = new PafSimpleCoordList(replicateAllCells);
                    int len = pscl.NumberOfCoordinates();
                    pscl.compressData();
                    evalviewrequest.replicateAllCells = pscl.GetSimpleCoordList;
                    evalviewrequest.replicateAllCells.axis = null;
                    evalviewrequest.replicateAllCells.coordinates = null;
                    PafApp.GetLogger().InfoFormat("evaluateView replicateAllCells collection size: {0}", new object[] {len});
                }
                else
                {
                    evalviewrequest.replicateAllCells = replicateAllCells;
                }
            }
            else
            {
                evalviewrequest.replicateAllCells = null;
            }

            //set the replicate existing cells.
            if (replicateExistingCells != null)
            {
                if (!replicateExistingCells.compressed)
                {
                    PafSimpleCoordList pscl = new PafSimpleCoordList(replicateExistingCells);
                    int len = pscl.NumberOfCoordinates();
                    pscl.compressData();
                    evalviewrequest.replicateExistingCells = pscl.GetSimpleCoordList;
                    evalviewrequest.replicateExistingCells.axis = null;
                    evalviewrequest.replicateExistingCells.coordinates = null;
                    PafApp.GetLogger().InfoFormat("evaluateView replicateExistingCells collection size: {0}", new object[] {len});
                }
                else
                {
                    evalviewrequest.replicateExistingCells = replicateExistingCells;
                }
            }
            else
            {
                evalviewrequest.replicateExistingCells = null;
            }

            //set the lift all cells.
            if (liftAllCells != null)
            {
                if (!liftAllCells.compressed)
                {
                    PafSimpleCoordList pscl = new PafSimpleCoordList(liftAllCells);
                    int len = pscl.NumberOfCoordinates();
                    pscl.compressData();
                    evalviewrequest.liftAllCells = pscl.GetSimpleCoordList;
                    evalviewrequest.liftAllCells.axis = null;
                    evalviewrequest.liftAllCells.coordinates = null;
                    PafApp.GetLogger().InfoFormat("evaluateView liftAllCells collection size: {0}", new object[] {len});
                }
                else
                {
                    evalviewrequest.liftAllCells = liftAllCells;
                }
            }
            else
            {
                evalviewrequest.liftAllCells = null;
            }

            //set the lift existing cells.
            if (liftExistingCells != null)
            {
                if (!liftExistingCells.compressed)
                {
                    PafSimpleCoordList pscl = new PafSimpleCoordList(liftExistingCells);
                    int len = pscl.NumberOfCoordinates();
                    pscl.compressData();
                    evalviewrequest.liftExistingCells = pscl.GetSimpleCoordList;
                    evalviewrequest.liftExistingCells.axis = null;
                    evalviewrequest.liftExistingCells.coordinates = null;
                    PafApp.GetLogger().InfoFormat("evaluateView liftExistingCells collection size: {0}", new object[] {len});
                }
                else
                {
                    evalviewrequest.liftExistingCells = liftExistingCells;
                }
            }
            else
            {
                evalviewrequest.liftExistingCells = null;
            }


            //set the view request protected formulas.
            evalviewrequest.protectedFormulas = protectedFormulas;
            //set the ruleset chosen  for the view
            evalviewrequest.ruleSetName = ruleSet;
            PafApp.GetLogger().InfoFormat("evaluateView using RuleSet: {0}", ruleSet);

            //Pass evaluateView to the middle tier and get axis new data slice
            //Get the data slice from the evaluateViewResponse.
            outPafView = PafApp.GetPafService().evaluateView(evalviewrequest);

            pafDataSlice newDataSlice = null;

            if (outPafView != null)
            {
                //TTN-1523
                PafApp.UndoAvailable = true;

                pafViewSection outPafViewSecion = outPafView.viewSections[0];
                newDataSlice = outPafViewSecion.pafDataSlice;
                bool suppressed = outPafViewSecion.suppressed;


                //If the view is not suppressed, then we only need to update the dataslice
                //if the view is suppressed, then we need to cache the entire view section.
                //TTN-2026
                if (!suppressed)
                {
                    //Get the Base64 data string and convert it to the data slice
                    newDataSlice.Uncompress();

                    //Update the cached pafView object for the view with the new dataslice from the server.
                    GetCurrentPafView(viewName).viewSections[0].pafDataSlice = newDataSlice;
                }
                else
                {
                    outPafView.Uncompress();
                    SetCurrentPafView(outPafView, viewName);
                }
            }

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("evaluateView runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

            return newDataSlice;
        }

        /// <summary>
        /// Saves, updates, and/or deletes an array of member tags to the server.
        /// </summary>
        /// <param name="addedMbrTags">The member tags to add.</param>
        /// <param name="updatedMbrTags">The member tags to add.</param>
        /// <param name="deletedMbrTags">The member tags to delete.</param>
        /// <returns></returns>
        public bool SaveMemberTags(simpleMemberTagData[] addedMbrTags,
            simpleMemberTagData[] updatedMbrTags, simpleMemberTagData[] deletedMbrTags)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            pafSaveMbrTagRequest mbrTags = new pafSaveMbrTagRequest();
            mbrTags.addMemberTags = addedMbrTags;
            mbrTags.updateMemberTags = updatedMbrTags;
            mbrTags.deleteMemberTags = deletedMbrTags;

            //other props.
            mbrTags.clientId = PafApp.ClientId;
            mbrTags.sessionToken = PafApp.GetPafAuthResponse().securityToken.sessionToken;

            pafSuccessResponse resp = PafApp.GetPafService().saveMemberTagData(mbrTags);

            try
            {
                if (resp != null)
                {
                    return resp.success;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("saveMemberTagData, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }

        }

        /// <summary>
        /// Saves, updates, and/or deletes an array of cell notes to the server.
        /// </summary>
        /// <param name="notesToAdd">Array of simple cell notes to add.</param>
        /// <param name="deleteNoteIntersections">Simplecoordlist of notes to delete.</param>
        /// <param name="notesToUpdate">Array of simple cell notes to update.</param>
        /// <returns>true if successful, false is not.</returns>
        public bool SaveCellNotes(simpleCellNote[] notesToAdd, simpleCellNote[] notesToUpdate, 
            simpleCoordList deleteNoteIntersections)
        {
            PafSimpleCellNotes nta = null;
            PafSimpleCellNotes ntu = null;
            PafSimpleCoordList dni = null;

            Stopwatch startTime = Stopwatch.StartNew();

            pafSaveNotesRequest cellNotes = new pafSaveNotesRequest();

            //add notes.
            if(notesToAdd != null && notesToAdd.Length > 0)
            {
                nta = new PafSimpleCellNotes(notesToAdd);
                nta.compressData();
                cellNotes.addNotes = notesToAdd;
            }
            else
            {
                cellNotes.addNotes = null;
            }

            //update notes.
            if (notesToUpdate != null && notesToUpdate.Length > 0)
            {
                ntu = new PafSimpleCellNotes(notesToUpdate);
                ntu.compressData();
                cellNotes.updateNotes = notesToUpdate;
            }
            else
            {
                cellNotes.updateNotes = null;
            }

            //delete notes.
            if(deleteNoteIntersections != null && !deleteNoteIntersections.compressed)
            {
                dni = new PafSimpleCoordList(deleteNoteIntersections, true);
                cellNotes.deleteNoteIntersections = dni.GetSimpleCoordList;
            }
            else if(deleteNoteIntersections != null && deleteNoteIntersections.compressed)
            {
                cellNotes.deleteNoteIntersections = deleteNoteIntersections;
            }
            else
            {
                cellNotes.deleteNoteIntersections = null;
            }

            //other props.
            cellNotes.clientId = PafApp.ClientId;
            cellNotes.sessionToken = PafApp.GetPafAuthResponse().securityToken.sessionToken;

            //the response.
            pafSaveNotesResponse resp = PafApp.GetPafService().saveCellNotes(cellNotes);

            try
            {
                if (resp != null)
                {
                    return resp.success;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("saveCellNotes, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }
        }


        /// <summary>
        /// Get a pafView from the PafService.
        /// </summary>
        /// <param name="viewName">The name of the view to return from the server.</param>
        /// <param name="sheetHash">The hash code of the current sheet.</param>
        /// <param name="pafUserSel">User selections to be passed to the server(for dynamic view).  
        /// This parameter can be null.</param>
        /// <param name="rowsSuppressed">Suppress the rows on the view.</param>
        /// <param name="columnsSuppressed">Suppress the columns on the view.</param>
        /// <param name="descendantAttributeIntersections"></param>
        /// <returns>A pafView.</returns>
        public pafView GetPafView(string viewName, int sheetHash, pafUserSelection[] pafUserSel,
            bool rowsSuppressed, bool columnsSuppressed, List<CoordinateSet> descendantAttributeIntersections)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            //Create the view request.
            viewRequest viewReq = new viewRequest();
            //Set the view request properties.
            viewReq.viewName = viewName;
            viewReq.sessionToken = PafApp.GetPafAuthResponse().securityToken.sessionToken;
            viewReq.clientId = PafApp.ClientId;
            viewReq.userSelections = pafUserSel;
            viewReq.compressResponse = true;
            viewReq.rowsSuppressed = rowsSuppressed;
            viewReq.columnsSuppressed = columnsSuppressed;
            if (descendantAttributeIntersections != null)
            {
                viewReq.sessionLockedCells = descendantAttributeIntersections.ToSimpleCoordListCollection().ToArray();
            }

            //Cache the pafView - modified by sorting
            pafView pafView = PafApp.GetPafService().getView(viewReq);
            bool suppressed = false;

            pafView.Uncompress();

            SetCurrentPafView(pafView, viewName);

            //Cache the pafView RowTuples - never modified
            if (pafView.viewSections[0].rowTuples != null)
            {
                if (_RowTuples.ContainsKey(viewName))
                {
                    _RowTuples[viewName] = (viewTuple[])pafView.viewSections[0].rowTuples.Clone();
                }
                else
                {
                    _RowTuples.Add(viewName, (viewTuple[])pafView.viewSections[0].rowTuples.Clone());
                }
            }

            //Cache the pafView RowTuples - never modified
            if (pafView.viewSections[0].colTuples != null)
            {
                if (_ColTuples.ContainsKey(viewName))
                {
                    _ColTuples[viewName] = (viewTuple[])pafView.viewSections[0].colTuples.Clone();
                }
                else
                {
                    _ColTuples.Add(viewName, (viewTuple[])pafView.viewSections[0].colTuples.Clone());
                }
            }

            //Get the view suppression.
            foreach (pafViewSection viewSection in pafView.viewSections.Where(viewSection => viewSection != null))
            {
                suppressed = viewSection.suppressed;
            }

            //Test to see if the current olap view has previously been sorted, don't sort if the view
            //has been suppressed.
            if (PafApp.GetViewMngr().GetOlapView(sheetHash) != null &&
                PafApp.GetViewMngr().GetOlapView(sheetHash).IsSorted &&!suppressed)
            {
                if (!pafView.dirtyFlag)
                {
                    PafApp.GetViewMngr().SortPafView(pafView, PafApp.GetViewMngr().GetOlapView(sheetHash).CurrentSortOrder);
                }
            }
            else if (suppressed && PafApp.GetViewMngr().CurrentView != null && 
                PafApp.GetViewMngr().CurrentView.ViewHasBeenBuilt)
            {
                if (pafView.dirtyFlag)
                {
                    if (PafApp.GetViewMngr().GetOlapView(sheetHash) != null)
                    {
                        PafApp.GetViewMngr().GetOlapView(sheetHash).ClearSortData();
                    }
                }
            }

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("getView, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

            //pafView.Save<pafView>(@"c:\", @"pafview.xml");

            return pafView;  
        }

        /// <summary>
        /// The pafView object most recently populated from the server - modified by Sorting
        /// </summary>
        public pafView GetCurrentPafView(string viewName)
        {
            pafView outView;
            return _PafViews.TryGetValue(viewName, out outView) ? outView : null;
        }

        /// <summary>
        /// Sets/updates the pafView in the collection of cached views.
        /// </summary>
        /// <param name="pafView"></param>
        /// <param name="viewName"></param>
        public void SetCurrentPafView(pafView pafView, string viewName)
        {
            if (_PafViews.ContainsKey(viewName))
            {
                _PafViews[viewName] = pafView;
            }
            else
            {
                _PafViews.Add(viewName, pafView);
            }
        }

        /// <summary>
        /// The pafView RowTuples - never modified
        /// </summary>
        public viewTuple[] GetCurrentRowTuples(string viewName)
        {
            if (_RowTuples.ContainsKey(viewName))
            {
                return _RowTuples[viewName];
            }

            return null;
        }


        /// <summary>
        /// The pafView ColTuples - never modified
        /// </summary>
        public viewTuple[] GetCurrentColTuples(string viewName)
        {
            if (_ColTuples.ContainsKey(viewName))
            {
                return _ColTuples[viewName];
            }

            return null;
        }

        /// <summary>
        /// Gets the PafMdb properties.
        /// </summary>
        /// <returns></returns>
        public pafMdbProps GetPafMdbProps()
        {
            //PafApp.ClientId,
            //PafApp.GetPafAuthResponse().securityToken.sessionToken);

            try
            {
                PafServiceProviderService pafsvc = PafApp.GetPafService();

                pafMdbPropsRequest mdbProps = new pafMdbPropsRequest
                    {clientId = PafApp.ClientId, sessionToken = PafApp.SessionToken};

                pafMdbPropsResponse resp = pafsvc.getMdbProps(mdbProps);

                if (resp != null)
                {
                    return resp.mdbProps;
                }
                else
                {
                    return null;
                }
            }
            catch (SoapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="sessionToken"></param>
        /// <param name="baseDimension"></param>
        /// <param name="selectedBaseMember"></param>
        /// <param name="requestedAttributeDimension"></param>
        /// <param name="selectedAttrSelections"></param>
        /// <returns></returns>
        /// <remarks>TTN-1645</remarks>
        public static pafValidAttrResponse GetValidAttributeMembers(string clientId, string sessionToken,
            string baseDimension, string[] selectedBaseMember, string requestedAttributeDimension,
            pafDimSpec[] selectedAttrSelections)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            try
            {
                PafServiceProviderService pafsvc = PafApp.GetPafService();

                pafValidAttrRequest validAttrMemb = new pafValidAttrRequest
                                                           {
                                                               clientId = clientId,
                                                               reqAttrDim = requestedAttributeDimension,
                                                               selAttrSpecs = selectedAttrSelections,
                                                               selBaseDim = baseDimension,
                                                               selBaseMembers = selectedBaseMember,
                                                               sessionToken = sessionToken
                                                           };

                pafValidAttrResponse resp = pafsvc.getValidAttributeMembers(validAttrMemb);

                if (resp != null)
                {
                    return resp;
                }
                else
                {
                    return null;
                }
            }
            catch (SoapException)
            {
                throw;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
            finally
            {
                startTime.Stop();
                PafApp.GetLogger().InfoFormat("getValidAttributeMembers, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            }
        }

        public viewInfoResponse ViewInformation(string viewName)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            //create the view request.
            var request = new viewInfoRequest();
            //Set the client id.
            request.clientId = PafApp.ClientId;
            //Set the view name.
            request.viewName = viewName;
            //Set the Attribute Evaluation Session Locks.

            //Get the data slice from the evaluateViewResponse.
            var viewInfo = PafApp.GetPafService().getViewInformation(request);

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("ViewInformation runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

            return viewInfo;
        }
    }
}