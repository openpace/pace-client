#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows.Forms;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Forms;
using Titan.PafService;
using Titan.Palladium.TestHarness;

namespace Titan.Pace.Application
{
    /// <summary>
    /// 
    /// </summary>
    internal class UowCalculator : Form
    {
        private Label _message;
        private DevExpress.XtraEditors.SimpleButton _cmdCancel;
        private BackgroundWorker _backgroundWorker1;
        private DevExpress.XtraEditors.MarqueeProgressBarControl _marqueeProgressBarControl1;

        // This delegate enables asynchronous calls for setting
        // the text property on a TextBox control.
        delegate void SetTextCallback(string text);

        /// <summary>
        /// Allows the userid to be modified.
        /// which will force the PopulateRoleFilter to be reloaded.
        /// </summary>
        public string UserId { get; set; }

        public string RoleId { get; set; }

        public string SeasonId { get; set; }

        public string StatusBarMessage { get; private set; }

        public pafPlannerConfig PafPlannerConfig { get; private set; }

        private bool TestHarnessIsRunning { get; set; }

        //Flag to signal if Webservice call is running
        private bool _isRunning;

        //Result returned by async web call
        private bool _globalResult;

        /// <summary>
        /// Signals to the caller if the UOW has been successfully calcutated.
        /// </summary>
        public  bool IsUowCalculated
        {
            get; private set;
        }

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool SetForegroundWindow(IntPtr hWnd);

        public UowCalculator()
        {
            InitializeComponent();

            _cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
            
            //Setup async callbacks
            PafApp.GetOnStartPlanSessionCompleted -= PafAppOnGetOnStartPlanSessionCompleted;
            PafApp.GetOnStartPlanSessionCompleted += PafAppOnGetOnStartPlanSessionCompleted;

            PafApp.AsyncOperationsError -= PafAppAsyncOperationsError;
            PafApp.AsyncOperationsError += PafAppAsyncOperationsError;

            PafApp.AsyncCannotCancel -= PafAppAsyncCannotCancel;
            PafApp.AsyncCannotCancel += PafAppAsyncCannotCancel;

            PafApp.AsyncCanCancel -= PafAppAsyncCanCancel;
            PafApp.AsyncCanCancel += PafAppAsyncCanCancel;
        }



        /** 
        Possible Scenarios
        1.	Role Filter: true, invalid intersection: true
            a.	Do not perform section 1.2.3 
            b.	Show role filter form
        2.	Role Filter: true, invalid intersection: false
            a.	Do not perform section 1.2.3 
            b.	Show role filter form
        3.	Role Filter: false, invalid intersection: true
            a.	Perform section 1.2.3 
        4.	Role Filter: false, invalid intersection: false
            a.	Check global min and max, if they are blank
                i.	Just start normal planning session
            b.	Perform section 1.2.3 - using the global min and max values.
        **/

        public DialogResult CalculateUow(Form sender, pafPlannerConfig pafPlannerConfig,
            string userId, string roleId, string seasonId)
        {

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Calculate UOW, userId: {0}", new [] {userId});
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Calculate UOW, roleId: {0}", new[] { roleId });
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Calculate UOW, seasonId: {0}", new[] { seasonId });

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Calculate UOW, isDataFilteredUow: {0}", new[] { pafPlannerConfig.isDataFilteredUow });
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Calculate UOW, isUserFilteredUow: {0}", new[] { pafPlannerConfig.isUserFilteredUow });
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Calculate UOW, isUserFilteredMultiSelect: {0}", new[] { pafPlannerConfig.isUserFilteredMultiSelect });
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Calculate UOW, isFilteredSubtotals: {0}", new[] { pafPlannerConfig.isFilteredSubtotals });
            
            if (pafPlannerConfig != null)
            {
                PafPlannerConfig = pafPlannerConfig;
            }
            else
            {
                return DialogResult.Abort;
                //return false;
            }

            TestHarnessIsRunning = false;

            return CalculateUow(sender, 
                userId, 
                roleId, 
                seasonId, 
                pafPlannerConfig.isDataFilteredUow,
                pafPlannerConfig.isUserFilteredUow,
                true,
                pafPlannerConfig.isUserFilteredMultiSelect,
                pafPlannerConfig.isFilteredSubtotals,
                null);



        }

        public DialogResult CalculateUow(Form sender, pafPlannerConfig pafPlannerConfig,
            string userId, string roleId, string seasonId, RecordedTest rt)
        {
            if (pafPlannerConfig != null)
            {
                PafPlannerConfig = pafPlannerConfig;
            }
            else
            {
                return DialogResult.Abort;
                //return false;
            }

            TestHarnessIsRunning = true;

            return CalculateUow(sender,
                userId,
                roleId,
                seasonId,
                rt.InvalidIntersection,
                rt.RoleFilterEnabled,
                false,
                false,
                rt.FilteredSubtotals,
                rt.RoleFilterSelections);

        }

        /// <summary>
        /// Perform a UOW cell check and if it passes, start a planning session.
        /// </summary>
        /// <param name="sender">Sender, the form the created this call.</param>
        /// <param name="userId">Current user id.</param>
        /// <param name="roleId">Role id.</param>
        /// <param name="seasonId">season id.</param>
        /// <param name="isDataFilteredUow">Invalid Intersecions</param>
        /// <param name="isUserFilteredUow">Role filter disabled/enabled.</param>
        /// <param name="isUserFilteredMultiSelect">Role filter multi-select disabled/enabled.</param>
        /// <param name="isFilteredSubtotals">Filtered Subtotals</param>
        /// <param name="showRoleFilterDialog">Show the role filter form.</param>
        /// <param name="pafDimSpecs">Specified pafDimSpec.</param>
        /// <returns>true or false</returns>
        private DialogResult CalculateUow(Form sender, string userId, string roleId, string seasonId,
            bool isDataFilteredUow, bool isUserFilteredUow, bool showRoleFilterDialog, bool isUserFilteredMultiSelect, bool isFilteredSubtotals, pafDimSpec[] pafDimSpecs)
        {
            IsUowCalculated = true;
            DialogResult result = DialogResult.None;
            bool isMaxUow;
            bool isLargeUow;

            //isDataFilteredUow is AKA: invlid intersections
            //isUserFilteredUow is AKA: Role Filter.

            if (isUserFilteredUow && isDataFilteredUow ||
               isUserFilteredUow && !isDataFilteredUow)
            {

                //force a role filter repop if the role or season has changed.
                if (userId != UserId || roleId != RoleId || seasonId != SeasonId)
                {
                    UserId = userId;
                    RoleId = roleId;
                    SeasonId = seasonId;
                    PafApp.PopulateRoleFilter(roleId, seasonId, isDataFilteredUow, isFilteredSubtotals);
                }

                //hide the sender form.
                HideSenderForm(sender);

                //don't show role filter when running the test harness.
                if(showRoleFilterDialog)
                {
                    //show the role filter dialog box.
                    if (!PafApp.GetGridApp().ShowRoleFilterDialogBox(isUserFilteredMultiSelect))
                    {
                        Debug.WriteLine("user clicked cancel, on the role filter dialog.");
                        IsUowCalculated = false;
                        return DialogResult.Cancel;
                        //return false;
                    }
                }


                //New Code - TTN-2262
                NativeWindow mainWindow = new NativeWindow();
                try
                {
                    mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

                    pafDimSpec[] dimSpec = showRoleFilterDialog ? PafApp.GetGridApp().RoleFilter.PafDimSpecs : pafDimSpecs;

                    Show(mainWindow);


                    _isRunning = true;
                    Task.Factory.StartNew(() => ProcessRoleFilterSelectionsAsync(isDataFilteredUow,isFilteredSubtotals, dimSpec));

                    //I know this should not be necessary, however I don't want to rewrite this entire form.
                    while (_isRunning)
                    {
                        System.Windows.Forms.Application.DoEvents();
                    }

                    //result = _globalResult ? DialogResult.No : DialogResult.OK;
                    result = _globalResult ? DialogResult.OK : DialogResult.No;
               
                }
                finally
                {
                    Hide();
                    SetForegroundWindow(mainWindow.Handle);
                    mainWindow.ReleaseHandle();
                }
                //End New Code


                //NativeWindow mainWindow = new NativeWindow();
                //try
                //{
                //    mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

                //    _progressBar.Style = ProgressBarStyle.Marquee;

                //    pafDimSpec[] dimSpec = null;
                //    if(showRoleFilterDialog)
                //    {
                //        dimSpec = PafApp.GetGridApp().RoleFilter.PafDimSpecs;
                //    }
                //    else
                //    {
                //        dimSpec = pafDimSpecs;
                //    }


                //    ProcessRoleFilterDoWorkEventArgs eventArgs =
                //        new ProcessRoleFilterDoWorkEventArgs(sender, isDataFilteredUow, dimSpec);

                //     _backgroundWorker1.RunWorkerAsync(eventArgs);

                   
                //    Show(mainWindow);

                   
                //    while (_backgroundWorker1.IsBusy)
                //    {
                //        System.Windows.Forms.Application.DoEvents();
                //    }
              

                //    if (!eventArgs.ReturnValue)
                //    {
                //        result = DialogResult.No;
                //    }
                //    else
                //    {
                //        result = DialogResult.OK;
                //    }

                //}
                //finally
                //{
                //    Hide();
                //    SetForegroundWindow(mainWindow.Handle);
                //    mainWindow.ReleaseHandle();
                //}                

            }
            else if (!isUserFilteredUow && isDataFilteredUow)
            {
                //create a dim spec.
                pafDimSpec[] pafDimSpec = new pafDimSpec[0];

                //get the data filtered UOW value
                bool dataFilUow = PafApp.GetGridApp().RoleSelector.SuppressInvalidInx;
                //get the filtered subtotals value
                bool filteredSubtotals = PafApp.GetGridApp().RoleSelector.FilteredSubtotals;

                //Get the Filtered UOW size.
                GetFilteredUowSize(roleId, seasonId, dataFilUow, filteredSubtotals, pafDimSpec);

                //Check the UOW size.
                DialogResult dr = PerformUowCellCheck(out isLargeUow, out isMaxUow);

                //if the result is ok, then create the planning session.
                if (dr == DialogResult.OK)
                {
                    Debug.WriteLine("create planning session with the filtered selections.");
                    PafApp.CreatePlanningSession(roleId, seasonId, dataFilUow, filteredSubtotals);
                }
                else //if not reshow the previous screen.
                {
                    Debug.WriteLine("failed UOW cell check, showing previous screen.");
                    IsUowCalculated = false;
                    result = DialogResult.No;
                }
            }
            else if (!isUserFilteredUow && !isDataFilteredUow)
            {
                int? large;
                int? max;
                //get the min and max values, from the planner conf and global values.
                GetCellMinMax(out large, out max);

                //get the filtered subtotals value
                bool filteredSubtotals = PafApp.GetGridApp().RoleSelector.FilteredSubtotals;

                //check for null min and max values
                //if so, just start a planning session...
                if (large == null && max == null)
                {
                    PafApp.CreatePlanningSession(roleId, seasonId, false, filteredSubtotals);
                }
                else
                {
                    //create a dim spec.
                    pafDimSpec[] pafDimSpec = new pafDimSpec[0];

                    //Get the Filtered UOW size.
                    GetFilteredUowSize(roleId, seasonId, false, filteredSubtotals, pafDimSpec);

                    //Check the UOW size.
                    DialogResult resp = PerformUowCellCheck(out isLargeUow, out isMaxUow);

                    //if the result is ok, then create the planning session.
                    if (resp == DialogResult.OK)
                    {
                        PafApp.CreatePlanningSession(roleId, seasonId, false, filteredSubtotals);
                    }
                    else
                    {
                        Debug.WriteLine("failed UOW cell check, showing previous screen.");
                        IsUowCalculated = false;
                        result = DialogResult.No;
                    }
                }
            }
            return result;
            //return IsUowCalculated;
        }

        /// <summary>
        /// Process the Role Filter (don't touch GUI components - will get cross-thread error!)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="isDataFilteredUow"></param>
        /// <param name="isFilteredSubtotals"></param>
        /// <param name="pafDimSpecs"></param>
        private bool ProcessRoleFilterSelections(Form sender, bool isDataFilteredUow, bool isFilteredSubtotals, pafDimSpec[] pafDimSpecs)
        {
            try
            {
                bool isMaxUow;
                bool isLargeUow;
                bool result = true;

                //set the text - in a safe way.
                SetText(PafApp.GetLocalization().GetResourceManager().GetString("Application.Message.UOW.BuildingUOW"));

                //repopulate the Role Filter, to workaround a server bug.  
                //where it does not see to remember the role filter.
                PafApp.PopulateRoleFilter(RoleId, SeasonId, isDataFilteredUow, isFilteredSubtotals);

                //get the data filtered UOW value
                bool dataFilUow = false;
                bool filteredSubTotals = false;
                if (!TestHarnessIsRunning)
                {
                    if (isDataFilteredUow)
                    {
                        dataFilUow = PafApp.GetGridApp().RoleFilter.SuppressInvalidInxChk;
                    }
                    if (isFilteredSubtotals)
                    {
                        filteredSubTotals = PafApp.GetGridApp().RoleFilter.FilteredSubtotalsChk;
                    }
                }
                else
                {
                    dataFilUow = isDataFilteredUow;
                    filteredSubTotals = isFilteredSubtotals;
                }

                //Get the Filtered UOW size.
                GetFilteredUowSize(
                    RoleId,
                    SeasonId,
                    dataFilUow,
                    filteredSubTotals,
                    pafDimSpecs);

                //Check the UOW size.
                DialogResult dr = PerformUowCellCheck(out isLargeUow, out isMaxUow);

                //if the result is ok, then create the planning session.
                if (dr == DialogResult.OK)
                {
                    Debug.WriteLine("create planning session with the filtered selections.");
                    //TTN-1035
                    if (isLargeUow)
                    {
                        //set the text - in a safe way.
                        SetText(
                            PafApp.GetLocalization().GetResourceManager().GetString(
                                "Application.Message.UOW.BuildingLargeUOW"));
                    }
                    //SetForegroundWindow(mainWindow.Handle);
                    PafApp.CreatePlanningSession(RoleId, SeasonId, dataFilUow, isFilteredSubtotals);
                }
                else //if not reshow the previous screen.
                {
                    Debug.WriteLine("failed UOW cell check, showing previous screen.");
                    //CalculateUow(sender, PafPlannerConfig, UserId, RoleId, SeasonId);
                    result = false;
                }
                return result;
            }
            finally
            {
                //this.Hide();
                StatusBarMessage = String.Empty;
                //SetForegroundWindow(mainWindow.Handle);
                //mainWindow.ReleaseHandle();
            }
        }

        /// <summary>
        /// Process the Role Filter (don't touch GUI components - will get cross-thread error!)
        /// </summary>
        /// <param name="isDataFilteredUow"></param>
        /// <param name="isFilteredSubtotals"></param>
        /// <param name="pafDimSpecs"></param>
        private void ProcessRoleFilterSelectionsAsync(bool isDataFilteredUow, bool isFilteredSubtotals, pafDimSpec[] pafDimSpecs)
        {
            try
            {
                bool isMaxUow;
                bool isLargeUow;

                //set the text - in a safe way.
                SetText(PafApp.GetLocalization().GetResourceManager().GetString("Application.Message.UOW.SendingUOW"));

                //repopulate the Role Filter, to workaround a server bug.  
                //where it does not see to remember the role filter.
                PafApp.PopulateRoleFilter(RoleId, SeasonId, isDataFilteredUow, isFilteredSubtotals);

                //get the data filtered UOW value
                bool dataFilUow = false;
                bool filteredSubTotals = false;
                if (!TestHarnessIsRunning)
                {
                    if (isDataFilteredUow)
                    {
                        dataFilUow = PafApp.GetGridApp().RoleFilter.SuppressInvalidInxChk;
                    }
                    if (isFilteredSubtotals)
                    {
                        filteredSubTotals = PafApp.GetGridApp().RoleFilter.FilteredSubtotalsChk;
                    }
                }
                else
                {
                    dataFilUow = isDataFilteredUow;
                    filteredSubTotals = isFilteredSubtotals;
                }

                //Get the Filtered UOW size.
                GetFilteredUowSize(
                    RoleId,
                    SeasonId,
                    dataFilUow,
                    filteredSubTotals,
                    pafDimSpecs);

                //Check the UOW size.
                DialogResult dr = PerformUowCellCheck(out isLargeUow, out isMaxUow);

                //if the result is ok, then create the planning session.
                if (dr == DialogResult.OK)
                {
                    Debug.WriteLine("create planning session with the filtered selections.");
                    //TTN-1035
                    if (isLargeUow)
                    {
                        //set the text - in a safe way.
                        SetText(PafApp.GetLocalization().GetResourceManager().GetString("Application.Message.UOW.BuildingLargeUOW"));
                    }
                    //SetForegroundWindow(mainWindow.Handle);
                    PafApp.CreatePlanningSessionAsync(RoleId, SeasonId, dataFilUow, filteredSubTotals);
                }
                else //if not reshow the previous screen.
                {
                    Debug.WriteLine("failed UOW cell check, showing previous screen.");
                    _globalResult = false;
                    _isRunning = false;
                }
            }
            finally
            {
                //this.Hide();
                //StatusBarMessage = String.Empty;
                //SetForegroundWindow(mainWindow.Handle);
                //mainWindow.ReleaseHandle();
            }
        }

        private void backgroundWorker1_DoWork(object sender, System.ComponentModel.DoWorkEventArgs e)
        {
            ProcessRoleFilterDoWorkEventArgs eventArgs = (ProcessRoleFilterDoWorkEventArgs)e.Argument;

            bool result = ProcessRoleFilterSelections(eventArgs.Sender, eventArgs.IsDataFilteredUow, eventArgs.IsFilteredSubtotals, eventArgs.PafDimSpec);

            eventArgs.ReturnValue = result;

        }

        private void PafAppOnGetOnStartPlanSessionCompleted(object sender, GetOnStartPlanSessionCompletedEventArgs eventArgs)
        {
            _globalResult = true;
            _isRunning = false;
        }

        private void PafAppAsyncOperationsError(object sender, PafWebserviceErrorEventArgs eventArgs)
        {
            _globalResult = false;
            _isRunning = false;
        }

        private void PafAppAsyncCanCancel(object sender)
        {
            SetText(PafApp.GetLocalization().GetResourceManager().GetString("Application.Message.UOW.BuildingUOW"));
            Invoke((MethodInvoker)delegate()
            {
                _cmdCancel.Enabled = true;
            });
        }


        private void PafAppAsyncCannotCancel(object sender)
        {
            SetText(PafApp.GetLocalization().GetResourceManager().GetString("Application.Message.UOW.ProcessingUOW"));
            Invoke((MethodInvoker)delegate()
            {
                _cmdCancel.Enabled = false;
            });
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            SetText(PafApp.GetLocalization().GetResourceManager().GetString("Application.Message.UOW.CancelBuildingUOW"));
            PafApp.CancelAsyncWebCall();
        }

        /// <summary>
        /// Gets the cell large and max values.
        /// </summary>
        /// <param name="large">Large value.</param>
        /// <param name="max">Max value.</param>
        private void GetCellMinMax(out int? large, out int? max)
        {
            try
            {
                large = null;
                max = null;
                int? glarge = null;
                int? gmax = null;

                if (PafPlannerConfig == null)
                {
                    return;
                }

                int? rlarge =  PafPlannerConfig.uowSizeLargeSpecified == false? null : (int?) PafPlannerConfig.uowSizeLarge;
                int? rmax = PafPlannerConfig.uowSizeMaxSpecified == false ? null : (int?) PafPlannerConfig.uowSizeMax;

                if (PafApp.GetPafAppSettings() != null)
                {
                    glarge = PafApp.GetPafAppSettings().globalUowSizeLargeSpecified == false ? null : (int?) PafApp.GetPafAppSettings().globalUowSizeLarge;
                    gmax = PafApp.GetPafAppSettings().globalUowSizeMaxSpecified == false ? null : (int?) PafApp.GetPafAppSettings().globalUowSizeMax;
                }

                if (rlarge != null)
                {
                    large = rlarge;
                }
                else
                {
                    if (glarge != null)
                    {
                        large = glarge;
                    }
                }

                if (rmax != null)
                {
                    max = rmax;
                }
                else
                {
                    if (gmax != null)
                    {
                        max = gmax;
                    }
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                large = null;
                max = null;
            }
        }

        /// <summary>
        /// Perform a UOW cell check.  No planning session is created.
        /// </summary>
        /// <param name="isLargeUow"></param>
        /// <param name="isMaxUow"></param>
        /// <returns>DialogResult.OK if cell count is within the criteria, DialogResult.No
        /// if the cell count is outside the criteria.</returns>
        private DialogResult PerformUowCellCheck(out bool isLargeUow, out bool isMaxUow)
        {
            isLargeUow = false;
            isMaxUow = false;
            try
            {
                int? large;
                int? max;
                long? uowResSize = 0;
                DialogResult res = DialogResult.OK;

                if (PafPlannerConfig == null)
                {
                    return res;
                }

                //get the min and max values, from the planner conf and global values.
                GetCellMinMax(out large, out max);

                //check the UOW size response
                if (PafApp.GetPafGetFilteredUOWSizeResponse() != null)
                {
                    uowResSize = PafApp.GetPafGetFilteredUOWSizeResponse().uowCellCount;
                }

                //perform the "Zero" check, before checking min/min!
                if (uowResSize == 0)
                {
                    if (PafApp.GetPafGetFilteredUOWSizeResponse() != null && PafApp.GetPafGetFilteredUOWSizeResponse().emptyDimensions != null)
                    {
                        string[] emptyDims = PafApp.GetPafGetFilteredUOWSizeResponse().emptyDimensions;

                        PafApp.MessageBox().Show(
                            PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.ZeroUow"),
                            PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                            String.Format(
                                PafApp.GetLocalization().GetResourceManager().GetString(
                                    "Application.Warning.ZeroUow.Details"), new string[] {String.Join(",", emptyDims)}),
                            SystemIcons.Question,
                            frmMessageBox.frmMessageBoxButtons.OK);
                    }
                    else
                    {
                        PafApp.MessageBox().Show(
                            PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.ZeroData"),
                            PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                            "",
                            SystemIcons.Question,
                            frmMessageBox.frmMessageBoxButtons.OK);
                    }
                    //user can only choose cancel...
                    return DialogResult.No;
                }

                //check for null min and max values
                //if so, return without prompting...
                if (large == null && max == null)
                {
                    return res;
                }

                //TTN-1809 (Add null check, if fails then if statement below catches)
                //perform the "Large" check, where Max is null.
                if (uowResSize >= large && max != null && uowResSize <= max)
                {
                    float n = uowResSize.Value;
                    float d = max.Value;
                    float x = (n/d);
                    isLargeUow = true;

                    return PafApp.MessageBox().Show(
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.LargeUow"),
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        String.Format(PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.Uow.Filter.Details"), 
                            new String[] {x.ToString("P0")}),
                        SystemIcons.Question,
                        frmMessageBox.frmMessageBoxButtons.OK_CANCEL);
                }

                //TTN-1809
                //perform the "Large" only check, Max is null.
                if (uowResSize >= large && max == null)
                {
                    float x = uowResSize.Value;
                    isLargeUow = true;

                    return PafApp.MessageBox().Show(
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.LargeUow"),
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        String.Format(PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.Uow.LargeFilter.Details"),
                            new String[] { x.ToString("N0") }),
                        SystemIcons.Question,
                        frmMessageBox.frmMessageBoxButtons.OK_CANCEL);
                }

                //perform the "Maximum" check.
                if (uowResSize >= max)
                {
                    float n = uowResSize.Value;
                    float d = max.Value;
                    float x = (n / d);
                    isMaxUow = true;

                    PafApp.MessageBox().Show(
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.MaxUow"),
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        String.Format(PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.Uow.Filter.Details"),
                            new String[] { x.ToString("P0")}),
                        SystemIcons.Exclamation,
                        frmMessageBox.frmMessageBoxButtons.OK);
                    //user can only choose cancel...
                    return DialogResult.No;
                }
                //for some reason uowResSize < 0, which means some error occured on the server.  So,
                //handle it.
                if (uowResSize < 0)
                {
                    PafApp.MessageBox().Show(
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.ErrorCalculatingUow"),
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        String.Empty,
                        SystemIcons.Error,
                        frmMessageBox.frmMessageBoxButtons.OK);
                    //user can only choose cancel...
                    return DialogResult.No;
                }

                return res;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return DialogResult.No;
            }
        }

        /// <summary>
        /// Hides the sender form, and sets Excel as the foregroundwindow.  Also
        /// sleeps the thread so we don't get a flicker.
        /// </summary>
        /// <param name="sender">Parent form.</param>
        private void HideSenderForm(Form sender)
        {
            if (sender == null)
            {
                return;
            }
            NativeWindow excelWindow = new NativeWindow();
            try
            {
                //make sure the sender is not visible anymore.
                excelWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
                //set excel as the forground window, so we don't loose focus or get a "flicker"
                SetForegroundWindow(excelWindow.Handle);
                //Hide the sender so it does not appear under the status bar.
                sender.Hide();
                //Added so that the sender has time to hide.
                //it seems that large PopulateRoleFilter calls will not allow this window to hide
                //so when the role filter window goes away below the sender will still be visible along 
                //with the status bar -- BAD!
                System.Threading.Thread.Sleep(Properties.Settings.Default.RoleFilterWindowThreadSleep);
            }
            finally
            {
                excelWindow.ReleaseHandle();
            }
        }

        /// <summary>
        /// Get the filtered Uow size.
        /// </summary>
        /// <param name="role">Role id.</param>
        /// <param name="seasonId">season id.</param>
        /// <param name="invalidInxSuppSel">isInvalidIntersectionSuppressionSelected</param>
        /// <param name="filteredSubtotalsSelected">Filtered subtotals.</param>
        /// <param name="pafUserSels">Paf user selections.</param>
        private void GetFilteredUowSize(string role, string seasonId,
            bool invalidInxSuppSel, bool filteredSubtotalsSelected, pafDimSpec[] pafUserSels)
        {
            PafApp.GetFilteredUowSize(role, seasonId, invalidInxSuppSel, filteredSubtotalsSelected, pafUserSels);
        }

        private void InitializeComponent()
        {
            this._message = new System.Windows.Forms.Label();
            this._backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this._cmdCancel = new DevExpress.XtraEditors.SimpleButton();
            this._marqueeProgressBarControl1 = new DevExpress.XtraEditors.MarqueeProgressBarControl();
            ((System.ComponentModel.ISupportInitialize)(this._marqueeProgressBarControl1.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // _message
            // 
            this._message.AccessibleDescription = "";
            this._message.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._message.Location = new System.Drawing.Point(2, 22);
            this._message.Name = "_message";
            this._message.Size = new System.Drawing.Size(330, 21);
            this._message.TabIndex = 7;
            this._message.Text = "_";
            this._message.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this._message.UseMnemonic = false;
            this._message.UseWaitCursor = true;
            // 
            // _backgroundWorker1
            // 
            this._backgroundWorker1.WorkerReportsProgress = true;
            this._backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this._backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this._backgroundWorker1_RunWorkerCompleted);
            // 
            // _cmdCancel
            // 
            this._cmdCancel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this._cmdCancel.Enabled = false;
            this._cmdCancel.Location = new System.Drawing.Point(338, 22);
            this._cmdCancel.Name = "_cmdCancel";
            this._cmdCancel.Size = new System.Drawing.Size(56, 21);
            this._cmdCancel.TabIndex = 9;
            this._cmdCancel.Text = "_";
            this._cmdCancel.UseWaitCursor = true;
            this._cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // _marqueeProgressBarControl1
            // 
            this._marqueeProgressBarControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this._marqueeProgressBarControl1.EditValue = "";
            this._marqueeProgressBarControl1.Location = new System.Drawing.Point(2, 3);
            this._marqueeProgressBarControl1.Name = "_marqueeProgressBarControl1";
            this._marqueeProgressBarControl1.Properties.Appearance.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this._marqueeProgressBarControl1.Properties.EndColor = System.Drawing.Color.Green;
            this._marqueeProgressBarControl1.Properties.MarqueeAnimationSpeed = 80;
            this._marqueeProgressBarControl1.Properties.ProgressViewStyle = DevExpress.XtraEditors.Controls.ProgressViewStyle.Solid;
            this._marqueeProgressBarControl1.Properties.ReadOnly = true;
            this._marqueeProgressBarControl1.Properties.StartColor = System.Drawing.Color.Green;
            this._marqueeProgressBarControl1.Size = new System.Drawing.Size(392, 14);
            this._marqueeProgressBarControl1.TabIndex = 5;
            this._marqueeProgressBarControl1.UseWaitCursor = true;
            // 
            // UowCalculator
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(397, 48);
            this.ControlBox = false;
            this.Controls.Add(this._marqueeProgressBarControl1);
            this.Controls.Add(this._cmdCancel);
            this.Controls.Add(this._message);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UowCalculator";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.UseWaitCursor = true;
            ((System.ComponentModel.ISupportInitialize)(this._marqueeProgressBarControl1.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        /// <summary>
        /// Sets the text in the label.  This method IS thread safe.
        /// </summary>
        /// <param name="text"></param>
        private void SetText(string text)
        {
            // InvokeRequired required compares the thread ID of the
            // calling thread to the thread ID of the creating thread.
            // If these threads are different, it returns true.
            if (_message.InvokeRequired)
            {
                Invoke((MethodInvoker)delegate()
                {
                    _message.Text = text;
                });
            }
            else
            {
                _message.Text = text;
            }
        }

        private void _backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(e.Error != null)
            {
                PafApp.MessageBox().Show(e.Error);
            }
        }


        
    }

    internal class ProcessRoleFilterDoWorkEventArgs : EventArgs
    {
        public Form Sender { get; set; }
        public bool IsDataFilteredUow { get; set; }
        public bool IsFilteredSubtotals { get; set; }
        public pafDimSpec[] PafDimSpec { get; set; }
        public bool ReturnValue { get; set; }

        public ProcessRoleFilterDoWorkEventArgs(Form sender, bool isDataFilteredUow, bool isFilteredSubtotals, pafDimSpec[] pafDimSpec)
        {
            Sender = sender;
            IsDataFilteredUow = isDataFilteredUow;
            IsFilteredSubtotals = isFilteredSubtotals;
            PafDimSpec = pafDimSpec;
        }

    }
}