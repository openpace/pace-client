#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Runtime.InteropServices;

namespace Titan.Pace.Application.Win32
{
    internal static class ChangeExcelIcon
    {
        [DllImport("User32.dll")]
        private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("User32.dll")]
        private static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("User32.dll")]
        private static extern IntPtr DrawMenuBar(IntPtr hwndParent);

        [DllImport("User32.dll")]
        private static extern IntPtr SendMessage(IntPtr hwnd, uint wMsg, int wParam, IntPtr lParam);

        [DllImport("shell32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.Winapi)]
        private static extern IntPtr ExtractIcon(IntPtr hInstance, string strFileName, uint uiIconIndex);

        /// <summary>
        /// Change the Excel icon in the title bar.
        /// </summary>
        /// <param name="iconHandle">Handle to the icon.</param>
        /// <param name="lpClassName">Class name of the window.</param>
        /// <param name="windowName">Name of the window.</param>
        public static void SetExcelWindowIcon(IntPtr iconHandle, string lpClassName, string windowName)
        {
            IntPtr lngXLHwnd;

            uint WM_SETICON = 128;
            int ICON_BIG = 1;
            int ICON_SMALL = 0;

            lngXLHwnd = FindWindow(lpClassName, windowName);

            SendMessage(lngXLHwnd, WM_SETICON, ICON_SMALL, iconHandle);
            SendMessage(lngXLHwnd, WM_SETICON, ICON_BIG, iconHandle);
            DrawMenuBar(lngXLHwnd);
        }

        /// <summary>
        /// Change the Excel icon in the title bar.
        /// </summary>
        /// <param name="iconPath">Path to the icon.</param>
        /// <param name="lpClassName">Class name of the window.</param>
        /// <param name="windowName">Name of the window.</param>
        public static void SetExcelWindowIcon(string iconPath, string lpClassName, string windowName)
        {
            IntPtr lngXLHwnd;
            IntPtr lngIcon;

            uint WM_SETICON = 128;
            int ICON_BIG = 1;
            int ICON_SMALL = 0;


            lngXLHwnd = FindWindow(lpClassName, windowName);
            lngIcon = ExtractIcon(IntPtr.Zero, iconPath, 0);

            SendMessage(lngXLHwnd, WM_SETICON, ICON_SMALL, lngIcon);
            SendMessage(lngXLHwnd, WM_SETICON, ICON_BIG, lngIcon);
            DrawMenuBar(lngXLHwnd);
        }
    }
}
