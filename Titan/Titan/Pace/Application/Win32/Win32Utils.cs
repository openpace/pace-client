#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;

namespace Titan.Pace.Application.Win32
{
    internal static class Win32Utils
    {
        /// <summary>
        /// Kills a process.
        /// </summary>
        /// <param name="hwnd">Pointer to the hwnd of the process to kill.</param>
        /// <remarks>if the hwnd does not exist, no exception is thrown.</remarks>
        public static void KillProcess(IntPtr hwnd)
        {
            int pid = 0;
            Win32DllImports.GetWindowThreadProcessId(hwnd, out pid);
            if (pid != 0)
            {
                KillProcess(pid);
            }
        }

        /// <summary>
        /// Kills a process.
        /// </summary>
        /// <param name="pid">Process id to kill.</param>
        /// <remarks>if the process does not exist, no exception is thrown.</remarks>
        public static void KillProcess(int pid)
        {
            Process process = Process.GetProcessById(pid);
            if(process != null)
            {
                try
                {
                    process.Kill();
                }
                catch(Exception ex)
                {
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// Opens the default web browser to the specified location.
        /// </summary>
        /// <param name="url"></param>
        public static void OpenDefaultBrowser(string url)
        {
            Process myProcess = new Process();

            try
            {
                // true is the default, but it is important not to set it to false
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = url;
                myProcess.Start();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }


        public static long GetMemoryUsageInBytes()
        {
             return GC.GetTotalMemory(true);
        }

        public static long GetMemoryUsageInMb()
        {
            return GetMemoryUsageInBytes() / 1048576;
        }
    }
}
