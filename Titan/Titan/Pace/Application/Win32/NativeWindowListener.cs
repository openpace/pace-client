#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Titan.Pace.Application.Win32
{
    class NativeWindowListener : NativeWindow
    {
        // Windows API functions and constants
        [DllImport("user32.dll")]
        private static extern int FindWindowEx(
        int hwndParent, int hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("user32", SetLastError = true)]
        private static extern int RegisterHotKey(IntPtr hwnd, int id, int fsModifiers, int vk);
        [DllImport("user32", SetLastError = true)]
        private static extern int UnregisterHotKey(IntPtr hwnd, int id);
        [DllImport("kernel32", SetLastError = true)]
        private static extern short GlobalAddAtom(string lpString);
        [DllImport("kernel32", SetLastError = true)]
        private static extern short GlobalDeleteAtom(short nAtom);


        private const int MOD_ALT = 1;
        private const int MOD_CONTROL = 2;
        private const int MOD_SHIFT = 4;
        private const int MOD_WIN = 8;
        private const int WM_HOTKEY = 0x312;

        private int hWndDesk;
        private int hWndWb;
        private short hotkeyID;  // the id for the hotkey
        public event ShiftF9 onShiftF9;
        public delegate void ShiftF9(Excel.Worksheet sheet);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hWnd"></param>
        /// <param name="caption"></param>
        public NativeWindowListener(int hWnd, string caption)
        {
            hWndDesk = FindWindowEx(hWnd, 0, "XLDESK", "");
            hWndWb = FindWindowEx(hWndDesk, 0, "EXCEL7", caption);
            AssignHandle((IntPtr) hWndDesk);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        [DebuggerHidden]
        protected override void WndProc(ref Message m)
        {
            // let the base class process the message
            base.WndProc(ref m);

            // if this is a WM_HOTKEY message, notify the parent object
            if (m.Msg == WM_HOTKEY)
            {
                Excel.Worksheet sheet = (Excel.Worksheet)Globals.ThisWorkbook.Application.ActiveSheet;

                try
                {
                    if (PafApp.GetViewMngr().CurrentProtectionMngr.CalculationsPending())
                    {
                        onShiftF9(sheet);
                    }
                }
                catch (ArgumentException ex)
                {
                    //No grid exists, so don't do anything.
                    PafApp.GetLogger().Warn(ex.Message, ex);
                }
                catch(Exception ex)
                {
                    PafApp.GetLogger().Warn(ex.Message, ex);
                }
            }
        }

        /// <summary>
        /// Register a global hot key
        /// </summary>
        /// <param name="hotkey"></param>
        /// <param name="modifiers"></param>
        public void RegisterGlobalHotKey(Keys hotkey, int modifiers)
        {
            try
            {
                // use the GlobalAddAtom API to get a unique ID (as suggested by MSDN docs)
                string atomName = System.Threading.Thread.CurrentThread.ManagedThreadId.ToString("X8");
                hotkeyID = GlobalAddAtom(atomName);
                if (hotkeyID == 0)
                {
                    throw new Exception(PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Exception.UnabletoGenerateHotkeyID") +
                        Marshal.GetLastWin32Error().ToString());
                }

                // register the hotkey, throw if any error
                if (RegisterHotKey(this.Handle, hotkeyID, modifiers, (int)hotkey) == 0)
                {
                    throw new Exception(PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Exception.UnabletoRegisterHotkey") +
                        Marshal.GetLastWin32Error().ToString());
                }
            }
            catch (Exception)
            {
                // clean up if hotkey registration failed
                UnregisterGlobalHotKey();
            }
        }

        /// <summary>
        /// Unregister a global hotkey
        /// </summary>
        public void UnregisterGlobalHotKey()
        {
            if (this.hotkeyID != 0)
            {
                UnregisterHotKey(this.Handle, hotkeyID);

                // clean up the atom list
                GlobalDeleteAtom(hotkeyID);
                hotkeyID = 0;
                ReleaseHandle();
            }
        }
    }
}