﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class DictionaryExtension
    {
        /// <summary>
        /// Converts a dictionary to a hashset.
        /// </summary>
        /// <typeparam name="TK">Type key of the dictionary.</typeparam>
        /// <typeparam name="TV">Type value of the dictionary.</typeparam>
        /// <param name="x"></param>
        /// <returns></returns>
        public static HashSet<TV> DictionaryToHashSet<TK, TV>(this Dictionary<TK, TV> x)
        {
            return x.Values.ToHashSet();
        }

        /// <summary>
        /// Converts a dictionary to a hashset.
        /// </summary>
        /// <typeparam name="TK">Type key of the dictionary.</typeparam>
        /// <typeparam name="TV">Type value of the dictionary.</typeparam>
        /// /// <param name="x">Dictonary to extend.</param>
        public static List<TV> DictionaryToList<TK, TV>(this Dictionary<TK, TV> x)
        {
            return x.Values.ToList();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <typeparam name="K"></typeparam>
        /// <param name="x"></param>
        /// <param name="keyArray"></param>
        /// <returns></returns>
        public static List<T>[] ConvertToArrayOfLists<T, K>(this Dictionary<K, List<T>> x, K[] keyArray)
        {
            // Make sure that map is not null
            if (x == null)
            {
                throw new NullReferenceException("conversion error - map or key array cannot be null");
                // Verify sizes are same
            }
            else if (x.Count != keyArray.Length)
            {
                throw new ArgumentException("conversion error - map and key array are not equal in size.");
            }

            // Create array
		    int mapSize = x.Count;
		    List<T>[] arrayOfLists = new List<T>[mapSize];

            // Populate array from map
		    int i = 0;
		    foreach (K key in keyArray) 
            {
			    //List<T> list = x.get(key);
                List<T> list = x[key];
			    if (list == null) 
                {
				    throw new ArgumentException("conversion error - key array element not found in map");
			    }
			    arrayOfLists[i++] = list;
		    }
		    return arrayOfLists;
        }

        /// <summary>
        /// If the existing dictionary key exists the value if updated, if not a new key/value is inserted.
        /// </summary>
        /// <typeparam name="TK">Dictionary Key datatype</typeparam>
        /// <typeparam name="TV">Dictionary Value datatype</typeparam>
        /// <param name="dictionary">Dictionary</param>
        /// <param name="key">Key to add</param>
        /// <param name="value">Value to add</param>
        public static void AddOrUpdate<TK, TV>(this Dictionary<TK, TV> dictionary, TK key, TV value)
        {
            if (dictionary.ContainsKey(key))
            {
                dictionary[key] = value;
            }
            else
            {
                dictionary.Add(key, value);
            }
        }

        /// <summary>
        /// Gets the dictionary value for the specified key, or returns the default data value if not found.
        /// </summary>
        /// <typeparam name="TK">Dictionary Key datatype</typeparam>
        /// <typeparam name="TV">Dictionary Value datatype</typeparam>
        /// <param name="dictionary">Dictionary</param>
        /// <param name="key">Key to add</param>
        /// <returns></returns>
        public static TV ValueOrDefault<TK, TV>(this Dictionary<TK, TV> dictionary, TK key)
        {
            TV value;
            return dictionary.TryGetValue(key, out value) ? dictionary[key] : default(TV);
        }


        /// <summary>
        /// Get the keys that match a specified value.
        /// </summary>
        /// <typeparam name="TK">Dictionary Key datatype</typeparam>
        /// <typeparam name="TV">Dictionary Value datatype</typeparam>
        /// <param name="dictionary">Dictionary</param>
        /// <param name="value">Value to search for.</param>
        /// <returns>a list of keys.</returns>
        public static List<TK> ValueKeys<TK, TV>(this Dictionary<TK, TV> dictionary, TV value)
        {
            List<TK> keys = new List<TK>();
            if (dictionary.ContainsValue(value))
            {
                keys.AddRange(from kvp in dictionary where kvp.Value.Equals(value) select kvp.Key);
            }
            return keys;
        }
    }
}
