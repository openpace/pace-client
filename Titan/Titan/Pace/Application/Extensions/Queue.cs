﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class QueueExtension
    {

        /// <summary>
        /// Returns the item in the front of the Queue.  
        /// This is identical to the normal Peek() function.
        /// </summary>
        /// <typeparam name="T">Item contained in the Queue.</typeparam>
        /// <param name="x">Queue to extend.</param>
        /// <returns>The item in the front of the queue.</returns>
        public static T PeekFront<T>(this Queue<T> x)
        {
            return x.Peek();
        }

        /// <summary>
        /// Returns the item in the rear of the Queue.  
        /// </summary>
        /// <typeparam name="T">Item contained in the Queue.</typeparam>
        /// <param name="x">Queue to extend.</param>
        /// <returns>The item in the rear of the queue.</returns>
        public static T PeekBack<T>(this Queue<T> x)
        {
            if(x == null || x.Count == 0) throw new NullReferenceException("Queue is null.");
            return x.ElementAt(x.Count - 1);
        }
    }
}
