﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;
using Range = Microsoft.Office.Interop.Excel.Range;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class ExcelExtensions
    {
        /// <summary>
        /// Converts an Excel.Range to a ContigousRange
        /// </summary>
        /// <param name="selection"></param>
        /// <returns></returns>
        public static ContiguousRange ToContiguousRange(this Range selection)
        {
            try
            {
                // get active cell (top left)
                int row = selection.Offset[0, 0].Row;
                int col = selection.Offset[0, 0].Column;
                Cell activeCell = new Cell(row, col);

                Cell bottomRight = new Cell(row, col);
                if (selection.Cells.CountLarge > 1)
                {
                    //Get the bottom right cell
                    int row2 = selection.Offset[selection.Rows.Count - 1, selection.Columns.Count - 1].Row;
                    int col2 = selection.Offset[selection.Rows.Count - 1, selection.Columns.Count - 1].Column;
                    bottomRight = new Cell(row2, col2);
                }

                return new ContiguousRange(activeCell.CellAddress, bottomRight.CellAddress);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Converts a string to an XL Paper type.
        /// </summary>
        /// <param name="x">String to convert.</param>
        /// <returns></returns>
        public static XlPaperSize ToXlPaperSize(this string x)
        {    
            switch(x)
            {
                case "Letter":
                    return XlPaperSize.xlPaperLetter;
                case "Tabloid":
                    return XlPaperSize.xlPaperTabloid;
                case "Legal":
                    return XlPaperSize.xlPaperLegal;
                case "Executive":
                    return XlPaperSize.xlPaperExecutive;
                case "A3":
                    return XlPaperSize.xlPaperA3;
                case "A4":
                    return XlPaperSize.xlPaperA4;
                case "B4(JIS)":
                    return XlPaperSize.xlPaperB4;
                case "B5(JIS)":
                    return XlPaperSize.xlPaperB5;
                case "Envelope #10":
                    return XlPaperSize.xlPaperB4;
                case "Envelope Monarch":
                    return XlPaperSize.xlPaperEnvelopeMonarch;
                default:
                    return XlPaperSize.xlPaperLetter;
            }
        }

        /// <summary>
        /// Converts a string to an XL Page Orientation.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static XlPageOrientation ToXlPageOrientation(this string x)
        {
            switch (x)
            {
                case "Portrait":
                    return XlPageOrientation.xlPortrait;
                case "Landscape":
                    return XlPageOrientation.xlLandscape;
                default:
                    return XlPageOrientation.xlPortrait;
            }
        }

        /// <summary>
        /// Converts a string to an XlPrintLocation.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static XlPrintLocation ToXlPrintLocation(this string x)
        {
            switch (x)
            {
                case "(None)":
                    return XlPrintLocation.xlPrintNoComments;
                case "At end of sheet":
                    return XlPrintLocation.xlPrintSheetEnd;
                case "As displayed on sheet":
                    return XlPrintLocation.xlPrintInPlace;
                default:
                    return XlPrintLocation.xlPrintNoComments;
            }
        }

        /// <summary>
        /// Converts a string to an XlPrintErrors.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static XlPrintErrors ToXlPrintErrors(this string x)
        {
            switch (x)
            {
                case "<blank>":
                    return XlPrintErrors.xlPrintErrorsBlank;
                case "displayed":
                    return XlPrintErrors.xlPrintErrorsDisplayed;
                case "--":
                    return XlPrintErrors.xlPrintErrorsDash;
                case "#N/A":
                    return XlPrintErrors.xlPrintErrorsNA;
                default:
                    return XlPrintErrors.xlPrintErrorsDisplayed;
            }
        }

        /// <summary>
        /// Prints the sheet's print style to the .log4net debug log (if the log level is set).
        /// </summary>
        /// <param name="sheet"></param>
        public static void DebugPrintSettings(this Worksheet sheet)
        {
            if(!PafApp.GetLogger().IsDebugEnabled) return;

            try
            {
                PafApp.GetLogger().DebugFormat("Getting print settings off sheet: {0}", new[] { sheet.Name });

                PafApp.GetLogger().DebugFormat("Print Style Zoom: {0}", new[] {sheet.PageSetup.Zoom});

                PafApp.GetLogger().DebugFormat("Print Style Orientation: {0}", new[] { sheet.PageSetup.Orientation.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style FitToPagesTall: {0}", new[] { sheet.PageSetup.FitToPagesTall.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style FitToPagesWide: {0}", new[] { sheet.PageSetup.FitToPagesWide.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style PaperSize: {0}", new[] { sheet.PageSetup.PaperSize.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style FirstPageNumber: {0}", new[] { sheet.PageSetup.FirstPageNumber.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style LeftMargin: {0}", new[] { sheet.PageSetup.LeftMargin.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style RightMargin: {0}", new[] { sheet.PageSetup.RightMargin.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style TopMargin: {0}", new[] { sheet.PageSetup.TopMargin.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style BottomMargin: {0}", new[] { sheet.PageSetup.BottomMargin.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style HeaderMargin: {0}", new[] { sheet.PageSetup.HeaderMargin.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style FooterMargin: {0}", new[] { sheet.PageSetup.FooterMargin.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style CenterHorizontally: {0}", new[] { sheet.PageSetup.CenterHorizontally.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style CenterVertically: {0}", new[] { sheet.PageSetup.CenterVertically.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style CenterHeader: {0}", new[] { sheet.PageSetup.CenterHeader });

                PafApp.GetLogger().DebugFormat("Print Style CenterFooter: {0}", new[] { sheet.PageSetup.CenterFooter });

                PafApp.GetLogger().DebugFormat("Print Style PrintTitleRows: {0}", new[] { sheet.PageSetup.PrintTitleRows });

                PafApp.GetLogger().DebugFormat("Print Style PrintTitleColumns: {0}", new[] { sheet.PageSetup.PrintTitleColumns });

                PafApp.GetLogger().DebugFormat("Print Style PrintGridlines: {0}", new[] { sheet.PageSetup.PrintGridlines.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style BlackAndWhite: {0}", new[] { sheet.PageSetup.BlackAndWhite.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style Draft: {0}", new[] { sheet.PageSetup.Draft.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style PrintComments: {0}", new[] { sheet.PageSetup.PrintComments.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style PrintErrors: {0}", new[] { sheet.PageSetup.PrintErrors.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style Order: {0}", new[] { sheet.PageSetup.Order.ToString() });

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Determines if the size of a range is too large (our criteria)
        /// </summary>
        /// <param name="range"></param>
        /// <param name="debug"></param>
        /// <returns></returns>
        public static bool IsLarge(this Range range, bool debug = false)
        {

            if (range == null) return false;

            bool maxCell = false;
            long cellCount = Convert.ToInt64(range.Cells.CountLarge);
            long rowCount = Convert.ToInt64(range.Rows.CountLarge);
            long colCount = Convert.ToInt64(range.Columns.CountLarge);
            if (cellCount == PafAppConstants.EXCEL12_MAX_CELL_COUNT)
            {
                PafApp.GetLogger().WarnFormat("Selected: {0} cells. Please try again with a smaller selection.", cellCount.ToString("N0"));
                maxCell = true;
            }
            if (rowCount == PafAppConstants.EXCEL12_ROW_LIMIT)
            {
                PafApp.GetLogger().WarnFormat("Selected: {0} rows. Please try again with a smaller selection.", rowCount.ToString("N0"));
                maxCell = true;
            }
            if (colCount == PafAppConstants.EXCEL12_COLUMN_LIMIT)
            {
                PafApp.GetLogger().WarnFormat("Selected: {0} columns. Please try again with a smaller selection.", rowCount.ToString("N0"));
                maxCell = true;
            }
            if (debug)
            {
                PafApp.GetLogger().DebugFormat("Total number of cells selected: {0}", cellCount.ToString("N0"));
                PafApp.GetLogger().DebugFormat("Total number of rows selected: {0}", rowCount.ToString("N0"));
                PafApp.GetLogger().DebugFormat("Total number of columns selected: {0}", colCount.ToString("N0"));
            }
            return maxCell;
        }

        /// <summary>
        /// Gets the number of cell in the range
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public static long CellCount(this Range range)
        {
            return range == null ? 0 : Convert.ToInt64(range.Cells.CountLarge);
        }

        /// <summary>
        /// Gets the number of rows in the range
        /// </summary>
        /// <param name="range"></param>
        /// <returns></returns>
        public static long RowCount(this Range range)
        {
            return range == null ? 0 : Convert.ToInt64(range.Rows.CountLarge);
        }
    }
}
