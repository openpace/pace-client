﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Diagnostics;
using System.Windows.Forms;
using Titan.Pace.Application.Controls.TreeView;

namespace Titan.Pace.Application.Extensions
{
    internal static class TreeNodeExtensions
    {
        /// <summary>
        /// Expands the tree node as well as the parent nodes.
        /// </summary>
        /// <param name="node"></param>
        public static void ExpandNode(this TreeNode node)
        {
            if (node != null)
            {
                if (!node.IsExpanded)
                {
                    node.Expand();
                    if (node.Parent != null)
                    {
                        ExpandNode(node.Parent);
                    }
                }
            }
        }

        /// <summary>
        /// Converts a TreeNode to a PaceTreeNode
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        [DebuggerHidden]
        public static PaceTreeNode ToPaceTreeNode(this TreeNode node)
        {
            return (PaceTreeNode) node;
        }
    }
}
