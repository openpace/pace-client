﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using DevExpress.XtraEditors;

namespace Titan.Pace.Application.Extensions
{
    internal static class ComboBoxEditExtension
    {
        /// <summary>
        /// Adds an array of string as items in a ComboBoxEdit
        /// </summary>
        /// <param name="x">ComboBoxEdit </param>
        /// <param name="values">List/Array of items.</param>
        /// <param name="selectedText">Item to set as default.  If null no item is set.</param>
        public static void AddSelectionItems(this ComboBoxEdit x, IEnumerable<string> values, string selectedText)
        {
            x.Properties.Items.Clear();
            x.Properties.Items.Add(String.Empty);
            foreach (string s in values)
            {
                string item = s;
                x.Properties.Items.Add(item);
                if (item == selectedText)
                {
                    x.SelectedItem = s;
                }
            }
        }
    }
}
