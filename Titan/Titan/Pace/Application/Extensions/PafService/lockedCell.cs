﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Linq;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class lockedCellExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="startRow"></param>
        /// <param name="startCol"></param>
        /// <returns></returns>
        public static Range GetRanges(this lockedCell[] x, int startRow, int startCol)
        {
            startRow = startRow - 1;
            startCol = startCol - 1;
            List<CellAddress> cells = new List<CellAddress>(x.Length);

            cells.AddRange(x.Select(t => new CellAddress(startRow + t.rowIndex, startCol + t.colIndex)));

            return CellAddress.GetRange(cells);
        }
    }
}
