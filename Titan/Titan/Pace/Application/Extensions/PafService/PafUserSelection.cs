﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Linq;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class PafUserSelectionExtension
    {
        /// <summary>
        /// Compares two pafUserSelections for equality
        /// </summary>
        /// <param name="pafUserSelections"></param>
        /// <param name="compareTo"></param>
        /// <returns></returns>
        public static bool AreEqual(this pafUserSelection[] pafUserSelections, pafUserSelection[] compareTo)
        {
            return !pafUserSelections.Where((t, i) => !AreEqual(t, compareTo[i])).Any();
        }

        /// <summary>
        /// Compares two pafUserSelections for equality
        /// </summary>
        /// <param name="x"></param>
        /// <param name="compareTo"></param>
        /// <returns></returns>
        public static bool AreEqual(this pafUserSelection x, pafUserSelection compareTo)
        {
            if (!x.dimension.Equals(compareTo.dimension)) return false;
            if (!x.id.Equals(compareTo.id)) return false;
            if (!x.multiples.Equals(compareTo.multiples)) return false;
            if (!x.pafAxis.AreEqual(compareTo.pafAxis)) return false;
            return !x.values.Where((t, i) => !t.Equals(compareTo.values[i])).Any();
        }
    }
}
