﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Pace.mdb;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class PafServerExtensions
    {
        /// <remarks/>
        public static SimpleDimMember[] BuildArray(this pafSimpleDimMember[] items)
        {
            SimpleDimMember[] sdm = new SimpleDimMember[items.Length];
            int i = 0;
            foreach(pafSimpleDimMember item in items)
            {
                int j = Convert.ToInt32(item.pafSimpleDimMemberProps.id);
                SimpleDimMember member = new SimpleDimMember(
                    item.childKeys,
                    item.key,
                    new SimpleDimMemberProps(
                        item.pafSimpleDimMemberProps.aliasKeys,
                        item.pafSimpleDimMemberProps.aliasValues,
                        item.pafSimpleDimMemberProps.consolidationType,
                        item.pafSimpleDimMemberProps.generationNumber,
                        j,
                        item.pafSimpleDimMemberProps.levelNumber), 
                    item.parentKey);

                sdm[i] = member;
                i++;
            }
            return sdm;
        }
    }
}
