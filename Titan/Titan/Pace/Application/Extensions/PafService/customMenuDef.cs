﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    internal static class customMenuDefExtension
    {
        /// <summary>
        /// Converts a customMenuDef to an Office RibbonButton
        /// </summary>
        /// <param name="x">customMenuDef</param>
        /// <returns>RibbonButton xml</returns>
        public static string ToRibbonButton(this customMenuDef x)
        {
            StringBuilder buttonXml = new StringBuilder();

            string key = x.key.RemoveSpaces();

            if (x.beginGroup)
            {
                buttonXml.Append("<menuSeparator id=\"").Append("sep_").Append(x.key).Append("\" />");
            }

            string msoId = String.Empty;
            if (!String.IsNullOrWhiteSpace(x.msoImageId))
            {
                msoId = " imageMso=\"" + x.msoImageId + "\"";
            }

            buttonXml.Append("<button id=\"").Append(key).Append("\"")
                           .Append(" label=\"").Append(x.caption).Append("\"")
                           .Append(" getEnabled=\"CustomMenuEnabled\"")
                           .Append(" onAction=\"CustomMenuItemClick\"")
                           .Append(msoId)
                           .Append("/>");

            return buttonXml.ToString();
        }

        /// <summary>
        /// Converts an array customMenuDef to an Office RibbonButton
        /// </summary>
        /// <param name="x">customMenuDef array</param>
        /// <param name="menuName">Name of </param>
        /// <param name="createMenu"></param>
        /// <returns></returns>
        public static string ToRibbonButtons(this IEnumerable<customMenuDef> x, string menuName = null, bool createMenu = true)
        {
            StringBuilder menuXml = new StringBuilder();
            if (x == null) return String.Empty;

            List<customMenuDef> items = x.ToList();
            if (items.Count == 0) return String.Empty;


            if (createMenu && !String.IsNullOrWhiteSpace(menuName))
            {
                if (items[0].beginGroup)
                {
                    menuXml.Append("<menuSeparator id=\"").Append("sep_").Append(items[0].key).Append("\" />");
                    items[0].beginGroup = false;
                }
                menuXml.Append("<menu id=\"menu_").Append(menuName.RemoveSpaces()).Append("\" label=\"").Append(menuName).Append("\">");
            }

            foreach (customMenuDef menu in items)
            {
                menuXml.Append(menu.ToRibbonButton());
            }

            if (createMenu && !String.IsNullOrWhiteSpace(menuName))
            {
                menuXml.Append("  </menu>");
            }

            return menuXml.ToString();
        }
    }
}
