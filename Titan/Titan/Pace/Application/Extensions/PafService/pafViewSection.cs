﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    internal static class pafViewSectionExtension
    {
        /// <summary>
        /// Converts a compressed string to an array of lockedCell
        /// </summary>
        /// <param name="x">Compressed String</param>
        /// <param name="groupDelimiter">Group Delimiter</param>
        /// <param name="elementDelimiter">Element Delimiter</param>
        /// <returns></returns>
        public static lockedCell[] ToLockedCells(this string x, string groupDelimiter, string elementDelimiter)
        {
            if (String.IsNullOrEmpty(x)) return null;
            List<List<String>> uncompressedString = x.ConverttoListofLists(groupDelimiter, elementDelimiter);
            List<lockedCell> lockedCells = new List<lockedCell>(uncompressedString.Count);
            foreach (List<String> lCell in uncompressedString)
            {
                lockedCell lockedCell = new lockedCell();
                int i = 0;
                foreach (String position in lCell)
                {
                    switch (i++)
                    {
                        case 0:
                        {
                            lockedCell.rowIndex = int.Parse(position);
                            break;
                        }
                        case 1:
                        {
                            lockedCell.colIndex = int.Parse(position);
                            break;
                        }
                        default:
                        {
                            break;
                        }
                    }
                }
                lockedCells.Add(lockedCell);
            }
            return lockedCells.ToArray();
        }

        /// <summary>
        /// Returns true if the pafViewSection has attributes.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static bool HasAttributes(this pafViewSection x)
        {
            return x.attributeDims != null;
        }
    }
}
