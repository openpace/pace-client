﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    internal static class abstractPaceConditionalStyleExtension
    {
        /// <summary>
        /// Gets the ConditionalStyleType of abstractPaceConditionalStyle
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static ConditionalStyleType GetStyleType(this abstractPaceConditionalStyle x)
        {
            Type type = x.GetType();
            if (type == typeof(colorScale))
            {
                return ConditionalStyleType.ColorScale;
            }
            if (type == typeof(iconStyle))
            {
                return ConditionalStyleType.IconSet;
            }
            if (type == typeof(dataBars))
            {
                return ConditionalStyleType.DataBar;
            }
            return ConditionalStyleType.None;
        }

        /// <summary>
        /// Converts (casts) an abstractPaceConditionalStyle to a colorScale
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static colorScale ToColorScale(this abstractPaceConditionalStyle x)
        {
            Type type = x.GetType();
            if (type == typeof(colorScale))
            {
                return (colorScale) x;
            }
            return null;
        }

        /// <summary>
        /// Converts (casts) an abstractPaceConditionalStyle to a iconStyle
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static iconStyle ToIconStyle(this abstractPaceConditionalStyle x)
        {
            Type type = x.GetType();
            if (type == typeof(iconStyle))
            {
                return (iconStyle)x;
            }
            return null;
        }

        /// <summary>
        /// Converts (casts) an abstractPaceConditionalStyle to a dataBars
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static dataBars ToDataBars(this abstractPaceConditionalStyle x)
        {
            Type type = x.GetType();
            if (type == typeof(dataBars))
            {
                return (dataBars)x;
            }
            return null;
        }
    }
}
