﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Data;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Data;
using DevExpress.XtraTreeList.Nodes;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    public static class pafViewTreeItemExtension
    {

        public const string DummyNode = "__dummy__";

        /// <summary>
        /// Converts the array of pafViewTreeItems to a data table
        /// </summary>
        /// <param name="baseViews">The PafViewTreeView(s) to add to the node.</param>
        /// <param name="parentKeyColumnName"></param>
        /// <param name="memberKeyColumnName"></param>
        /// <param name="memberColumnName"></param>
        /// <param name="canSelectColumnName"></param>
        /// <param name="addDummyNode"></param>
        /// <param name="dummyNodeName"></param>
        /// <returns></returns>
        public static DataTable ToTreeListDataTable(this IEnumerable<pafViewTreeItem> baseViews, string parentKeyColumnName,
            string memberKeyColumnName, string memberColumnName, string canSelectColumnName, bool addDummyNode = true, string dummyNodeName = DummyNode)
        {
            if (baseViews != null)
            {
                DataTable dt = new DataTable("Views");

                dt.Columns.Add(parentKeyColumnName, typeof(int));
                dt.Columns.Add(memberKeyColumnName, typeof(int));
                dt.Columns.Add(memberColumnName, typeof(string));
                dt.Columns.Add(canSelectColumnName, typeof(bool));
                int parKey = 0;
                int mbrKey = 1;

                //if (addDummyNode)
                //{
                //    List<object> temp2 = new List<object>(3) { null, 1, DummyNode, true };
                //    dt.Rows.Add(temp2.ToArray());
                //}

                foreach (pafViewTreeItem node in baseViews)
                {
                    if (node != null)
                    {
                        if (!node.group)
                        {
                            //The node is not a group so just add it.
                            //PaceTreeNode item = new PaceTreeNode(node.label, false, 2, 2);
                            //tree.Nodes.Add(item);
                            List<object> temp = new List<object>(3) { parKey, mbrKey, node.label, true };
                            dt.Rows.Add(temp.ToArray());
                            mbrKey++;
                        }
                        else
                        {
                            //The node has children.
                            //PaceTreeNode item = new PaceTreeNode(node.label, true, 1, 1);
                            //tree.Nodes.Add(item);

                            List<object> temp = new List<object>(3) { null, mbrKey, node.label, false };
                            dt.Rows.Add(temp.ToArray());
                            parKey = mbrKey;
                            mbrKey++;

                            dt = ConstructViewTree(dt, node.items, ref parKey, ref mbrKey);
                        }
                    }
                }

                if (addDummyNode)
                {
                    List<object> temp2 = new List<object>(3) { null, mbrKey + 1, DummyNode, true };
                    dt.Rows.Add(temp2.ToArray());
                }

                return dt;
            }
            return null;
        }


        private static DataTable ConstructViewTree(DataTable dt, IEnumerable<pafViewTreeItem> baseViews, ref int parKey, ref int mbrKey)
        {
            if (baseViews != null)
            {
                int backKey = parKey;
                foreach (pafViewTreeItem node in baseViews)
                {
                    if (!node.group)
                    {
                        //The node is not a group so just add it.
                        //PaceTreeNode item = new PaceTreeNode(node.label, false, 2, 2);
                        //treeNode.Nodes.Add(item);
                        List<object> temp = new List<object>(3) { backKey, mbrKey, node.label, true };
                        dt.Rows.Add(temp.ToArray());
                        mbrKey++;
                    }
                    else
                    {
                        //The node has children, so add the dummy nodes.
                        //PaceTreeNode item = new PaceTreeNode(node.label, true, 1, 1);
                        //treeNode.Nodes.Add(item);

                        //ConstructViewTree(item, node.items);
                        List<object> temp = new List<object>(3) { parKey, mbrKey, node.label, false };
                        dt.Rows.Add(temp.ToArray());
                        parKey = mbrKey;
                        mbrKey++;

                        dt = ConstructViewTree(dt, node.items, ref parKey, ref mbrKey);
                    }
                }
            }
            return dt;
        }


        /// <summary>
        /// Create an unbound tree list.
        /// </summary>
        /// <param name="baseViews"></param>
        /// <param name="tree"></param>
        /// <param name="memberColName"></param>
        /// <param name="canSelColumn"></param>
        /// <param name="descriptionColumn"></param>
        /// <param name="dynamicColumn"></param>
        /// <returns></returns>
        public static TreeList CreateUnboundTree(this IEnumerable<pafViewTreeItem> baseViews, TreeList tree, string memberColName, string canSelColumn,
            string descriptionColumn, string dynamicColumn)
        {
            if (baseViews != null)
            {
                tree.BeginUpdate();
                tree.Columns.Add();
                tree.Columns[0].Caption = memberColName;
                tree.Columns[0].VisibleIndex = 0;
                tree.Columns.Add();
                tree.Columns[1].Caption = canSelColumn;
                tree.Columns[1].VisibleIndex = 1;
                tree.Columns[1].UnboundType = UnboundColumnType.Boolean;
                tree.Columns[1].Visible = false;
                tree.Columns.Add();
                tree.Columns[2].Caption = descriptionColumn;
                tree.Columns[2].VisibleIndex = 2;
                tree.Columns[2].Visible = false;
                tree.Columns.Add();
                tree.Columns[3].Caption = dynamicColumn;
                tree.Columns[3].UnboundType =  UnboundColumnType.Boolean;
                tree.Columns[3].VisibleIndex = 3;
                tree.Columns[3].Visible = false;
                tree.EndUpdate();

                tree.BeginUnboundLoad();
                foreach (pafViewTreeItem node in baseViews)
                {
                    if (node != null)
                    {
                        if (!node.group)
                        {
                            //The node is not a group so just add it.
                            tree.AppendNode(new object[] { node.label, true, node.desc, node.userSelections != null }, null);
                        }
                        else
                        {
                            TreeListNode item = tree.AppendNode(new object[] { node.label, false, node.desc, false }, null);
                            tree = CreateUnboundTree(tree, item, node.items);
                        }
                    }
                }
                tree.EndUnboundLoad();
            }
            return tree;
        }

        /// <summary>
        /// Constructs a tree view.
        /// </summary>
        /// <param name="treeNode">The node to add the new nodes to.</param>
        /// <param name="baseViews">The PafViewTreeView(s) to add to the node.</param>
        private static TreeList CreateUnboundTree(TreeList tree, TreeListNode treeNode, IEnumerable<pafViewTreeItem> baseViews)
        {
            if (baseViews != null)
            {
                foreach (pafViewTreeItem node in baseViews)
                {
                    if (!node.group)
                    {
                        //The node is not a group so just add it.
                        tree.AppendNode(new object[] { node.label, true, node.desc, node.userSelections != null }, treeNode);

                    }
                    else
                    {
                        //The node has children, so add the dummy nodes.
                        TreeListNode item = tree.AppendNode(new object[] { node.label, false, node.desc, false }, treeNode);
                        tree = CreateUnboundTree(tree, item, node.items);
                    }
                }
            }
            return tree;
        }


    }
}
