﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using System.Linq;
using Titan.Pace.Application.Compression.PafViewSection;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    internal static class pafViewExtension
    {
        /// <summary>
        /// Returns true if the pafView has a pafViewSection that contains attributes.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        [DebuggerHidden]
        public static bool HasAttributes(this pafView x)
        {
            return x.viewSections.Where(viewSection => viewSection != null).Any(viewSection => viewSection.HasAttributes());
        }

        /// <summary>
        /// Uncompress a pafView.
        /// </summary>
        /// <param name="x">Paf View to expand.</param>
        public static void Uncompress(this pafView x)
        {
            Stopwatch startTime = Stopwatch.StartNew();

            //pafView.Save<pafView>(@"c:\", "pafView.xml");

            foreach (pafViewSection viewSection in x.viewSections)
            {
                if (String.IsNullOrEmpty(PafAppConstants.ELEMENT_DELIM))
                {
                    PafAppConstants.ELEMENT_DELIM = viewSection.elementDelim;
                }

                if (String.IsNullOrEmpty(PafAppConstants.GROUP_DELIM))
                {
                    PafAppConstants.GROUP_DELIM = viewSection.groupDelim;
                }

                if (viewSection == null) continue;


                ViewTuples col = new ViewTuples(viewSection.colTuples);
                col.uncompressData();

                ViewTuples row = new ViewTuples(viewSection.rowTuples);
                row.uncompressData();

                PafViewSection pvs = new PafViewSection(viewSection);
                pvs.uncompressData();

                if (viewSection.compForwardPlannableLockedCell != null)
                {
                    viewSection.forwardPlannableLockedCell = viewSection.compForwardPlannableLockedCell.ToLockedCells(viewSection.groupDelim, viewSection.elementDelim);
                }

                if (viewSection.compInvalidIntersectionsLC != null)
                {
                    viewSection.invalidAttrIntersectionsLC = viewSection.compInvalidIntersectionsLC.ToLockedCells(viewSection.groupDelim, viewSection.elementDelim);
                }

                if (viewSection.compNotPlannableLockedCells != null)
                {
                    viewSection.notPlannableLockedCells = viewSection.compNotPlannableLockedCells.ToLockedCells(viewSection.groupDelim, viewSection.elementDelim);
                }

                if (viewSection.compSessionLockedCells != null)
                {
                    viewSection.sessionLockedCells = viewSection.compSessionLockedCells.ToLockedCells(viewSection.groupDelim, viewSection.elementDelim);
                }

                //Uncompress the data slice.
                viewSection.pafDataSlice.Uncompress();
            }
            startTime.Stop();
            PafApp.GetLogger().InfoFormat("Uncompress, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }
    }
}
