﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Titan.Pace.Data;
using Titan.PafService;
using Titan.Properties;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class pafPlanSessionResponseExtension
    {
        /// <summary>
        /// Creates a SimpleDimTress object from a pafPlanSessionResponse
        /// </summary>
        /// <param name="x">Plan Session Response</param>
        /// <param name="pafAuthResponse">Auth Response</param>
        /// <param name="pafMdbProps">MDB Props</param>
        /// <returns></returns>
        public static SimpleDimTrees ToSimpleDimTrees(this pafPlanSessionResponse x, pafAuthResponse pafAuthResponse, pafMdbProps pafMdbProps)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            SimpleDimTrees result = new SimpleDimTrees();
            HashSet<string> leafOnlyDims = new HashSet<string>(Settings.Default.LeafLevelOnlyDimensions.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));


            //Add base dimension (visible)
            List<pafSimpleDimTree> baseTrees = x.GetVisibleBaseDimensionsTrees(pafAuthResponse, pafMdbProps);
            foreach (var baseTree in baseTrees)
            {
                dimLevelGenInfo genInfo = null;
                dimLevelGenInfo levelInfo = null;
                if (pafMdbProps.genInfoArray != null && pafMdbProps.genInfoArray.Length > 0)
                {
                    genInfo = pafMdbProps.genInfoArray.ToList().FirstOrDefault(y => y.dimension == baseTree.id);
                }
                if (pafMdbProps.levelInfoArray != null && pafMdbProps.levelInfoArray.Length > 0)
                {
                    levelInfo = pafMdbProps.levelInfoArray.ToList().FirstOrDefault(y => y.dimension == baseTree.id);
                }
                result = baseTree.ToSimpleDimTree(true, leafOnlyDims.Contains(baseTree.id), false, null, genInfo, levelInfo, result);
            }
            //result = baseTrees.Aggregate(result, (current, tree) => tree.ToSimpleDimTree(true, leafOnlyDims.Contains(tree.id), false, null, current));
            
            //add hidden base trees
            List<pafSimpleDimTree> hiddenBaseTrees = x.GetHiddenBaseDimensionsTrees(pafAuthResponse, pafMdbProps);
            foreach (var hiddenBaseTree in hiddenBaseTrees)
            {
                dimLevelGenInfo genInfo = null;
                dimLevelGenInfo levelInfo = null;
                if (pafMdbProps.genInfoArray != null && pafMdbProps.genInfoArray.Length > 0)
                {
                    genInfo = pafMdbProps.genInfoArray.ToList().FirstOrDefault(y => y.dimension == hiddenBaseTree.id);
                }
                if (pafMdbProps.levelInfoArray != null && pafMdbProps.levelInfoArray.Length > 0)
                {
                    levelInfo = pafMdbProps.levelInfoArray.ToList().FirstOrDefault(y => y.dimension == hiddenBaseTree.id);
                }
                result = hiddenBaseTree.ToSimpleDimTree(true, leafOnlyDims.Contains(hiddenBaseTree.id), true, null, genInfo, levelInfo, result);
            }
            //result = hiddenBaseTrees.Aggregate(result, (current, tree) => tree.ToSimpleDimTree(true, leafOnlyDims.Contains(tree.id), true, null, current));

            //Add Attribute dimension
            //TODO Don't allow attribute dims to be used in locks.
            List<pafSimpleDimTree> attrTrees = x.GetAttributeDimensionTrees(pafAuthResponse);
            foreach (pafSimpleDimTree tree in attrTrees)
            {
                string parent = pafMdbProps.GetAttributeBaseDimensionParent(tree.id);
                long? parentKey = null;
                if (!String.IsNullOrEmpty(parent))
                {
                    parentKey = result.GetDimensionKey(parent);
                }
                dimLevelGenInfo genInfo = null;
                dimLevelGenInfo levelInfo = null;
                if (pafMdbProps.genInfoArray != null && pafMdbProps.genInfoArray.Length > 0)
                {
                    genInfo = pafMdbProps.genInfoArray.ToList().FirstOrDefault(y => y.dimension == tree.id);
                }
                if (pafMdbProps.levelInfoArray != null && pafMdbProps.levelInfoArray.Length > 0)
                {
                    levelInfo = pafMdbProps.levelInfoArray.ToList().FirstOrDefault(y => y.dimension == tree.id);
                }

                result = tree.ToSimpleDimTree(false, leafOnlyDims.Contains(tree.id), false, parentKey, genInfo, levelInfo, result);
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ToSimpleDimTrees runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

            return result;

        }

        /// <summary>
        /// Gets a  List pafSimpleDimTree for the visible Base Dimensions.
        /// </summary>
        /// <param name="x">Plan Session Response</param>
        /// <param name="pafAuthResponse">Auth Response</param>
        /// <param name="pafMdbProps">MDB Props</param>
        /// <returns>List of pafSimpleDimTree</returns>
        public static List<pafSimpleDimTree> GetVisibleBaseDimensionsTrees(this pafPlanSessionResponse x, pafAuthResponse pafAuthResponse, pafMdbProps pafMdbProps)
        {
            return (from tree in x.dimTrees
                    let isAttribute = pafAuthResponse.IsDimensionAttribute(tree.id)
                    where pafMdbProps.IsDimensionVisible(tree.id) && !isAttribute
                    select tree).ToList();
        }

        /// <summary>
        /// Gets a  List pafSimpleDimTree for the visible Base Dimensions.
        /// </summary>
        /// <param name="x">Plan Session Response</param>
        /// <param name="pafAuthResponse">Auth Response</param>
        /// <param name="pafMdbProps">MDB Props</param>
        /// <returns>List of pafSimpleDimTree</returns>
        public static List<pafSimpleDimTree> GetHiddenBaseDimensionsTrees(this pafPlanSessionResponse x, pafAuthResponse pafAuthResponse, pafMdbProps pafMdbProps)
        {
            return (from tree in x.dimTrees
                    let isAttribute = pafAuthResponse.IsDimensionAttribute(tree.id)
                    where !pafMdbProps.IsDimensionVisible(tree.id) && !isAttribute
                    select tree).ToList();
        }

        /// <summary>
        /// Gets a  List pafSimpleDimTree for the Attribute Dimensions.
        /// </summary>
        /// <param name="x">Plan Session Response</param>
        /// <param name="pafAuthResponse">Auth Response</param>
        /// <returns>List of pafSimpleDimTree</returns>
        public static List<pafSimpleDimTree> GetAttributeDimensionTrees(this pafPlanSessionResponse x, pafAuthResponse pafAuthResponse)
        {
            return (from tree in x.dimTrees
                    let isAttribute = pafAuthResponse.IsDimensionAttribute(tree.id)
                    where isAttribute
                    select tree).ToList();
        }
    }
}
