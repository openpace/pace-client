﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class PafAxisExtension
    {
        /// <summary>
        /// Compares to pafAxis to see if they are equal.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="compareTo"></param>
        /// <returns></returns>
        public static bool AreEqual(this pafAxis x, pafAxis compareTo)
        {
            if (!x.colAxis.Equals(compareTo.colAxis)) return false;
            if (!x.pageAxis.Equals(compareTo.pageAxis)) return false;
            return x.rowAxis.Equals(compareTo.rowAxis) && x.value.Equals(compareTo.value);
        }

        /// <summary>
        /// Converts a pafAxis to an Axis.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static ViewAxis ToAxis(this pafAxis x)
        {
            return x.value.ParseEnum<ViewAxis>();
        }

        public static int ToPafAxis(this ViewAxis x)
        {
            switch (x)
            {
                case ViewAxis.Col:
                    return 1;
                case ViewAxis.Row:
                    return 0;
                case ViewAxis.Page:
                    return 2;
            }
            return -1;
        }
    }
}
