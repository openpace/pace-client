﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Titan.Pace.Base.Data;
using Titan.Pace.DataStructures;
using Titan.PafService;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class simpleCoordListExtension
    {
        /// <summary>
        /// Builds an array of intersections from a simpleCoordList.
        /// </summary>
        /// <param name="x">The simpleCoordList to build the array of intersections from.</param>
        /// <returns>an array of intersections, or a empty Intersection array if the coordList is null.</returns>
        public static Intersection[] ToIntersections(this simpleCoordList x)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            if (x == null) return null;

            //if compress, uncompress first.
            if (x.compressed)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(x);
                if (pscl.isCompressed())
                {
                    pscl.uncompressData();
                }
                x = pscl.GetSimpleCoordList;
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ToIntersections(simpleCoordList) - Uncompress runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

            stopwatch = Stopwatch.StartNew();

            //then perform null checks.
            if (x == null) return new Intersection[0];
            if (x.coordinates == null) return new Intersection[0];
            if (x.axis == null) return new Intersection[0];

            Intersection[] intersections = new Intersection[x.coordinates.Length / x.axis.Length];

            int axisCnt = x.axis.Length;
            string[] axis = new string[axisCnt];
            Array.Copy(x.axis, axis, axisCnt);
            for (int i = 0; i < intersections.Length; i++)
            {
                String[] coords = new String[axisCnt];
                for (int j = 0; j < axisCnt; j++)
                {
                    coords[j] = x.coordinates[(i * axisCnt) + j];
                }
                intersections[i] = new Intersection(axis, coords);
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ToIntersections(simpleCoordList) - Create Intersections runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

            return intersections;
        }

        /// <summary>
        /// Builds an List of intersections from a simpleCoordList.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
         public static List<Intersection> ToIntersections(this IEnumerable<simpleCoordList> x)
         {
             Stopwatch stopwatch = Stopwatch.StartNew();

             List<Intersection> results = new List<Intersection>();

             foreach(simpleCoordList scl in x)
             {
                 results.AddRange(scl.ToIntersections());
             }
             stopwatch.Stop();
             PafApp.GetLogger().InfoFormat("ToIntersections(IEnumerable<simpleCoordList>) runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

             return results;
         }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
         public static CoordinateSet ToCoordinateSet(this simpleCoordList x)
         {
             Stopwatch stopwatch = Stopwatch.StartNew();

             //if compress, uncompress first.
             if (x.compressed)
             {
                 PafSimpleCoordList pscl = new PafSimpleCoordList(x);
                 if (pscl.isCompressed())
                 {
                     pscl.uncompressData();
                 }
                 x = pscl.GetSimpleCoordList;
             }

             stopwatch.Stop();
             PafApp.GetLogger().InfoFormat("ToCoordinateSet(simpleCoordList) - Uncompress runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

             stopwatch = Stopwatch.StartNew();

             int lenght = x.coordinates.Length / x.axis.Length;

             int axisCnt = x.axis.Length;
             string[] axis = new string[axisCnt];
             Array.Copy(x.axis, axis, axisCnt);

             CoordinateSet cs = new CoordinateSet(axis);

             for (int i = 0; i < lenght; i++)
             {
                 String[] coords = new String[axisCnt];
                 for (int j = 0; j < axisCnt; j++)
                 {
                     coords[j] = x.coordinates[(i * axisCnt) + j];
                 }
                 cs.AddCoordinate(coords);
             }

             stopwatch.Stop();
             PafApp.GetLogger().InfoFormat("ToCoordinateSet(simpleCoordList) - Create Intersections runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

             return cs;
         }
    }
}