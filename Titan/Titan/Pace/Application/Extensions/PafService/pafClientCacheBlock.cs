﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    internal static class pafClientCacheBlockExtension
    {
        public static Dictionary<string, HashSet<String>> ToRuleSetMemberTagDictionary(this pafClientCacheBlock cacheBlock)
        {
            Dictionary<string, HashSet<String>> result = new Dictionary<string, HashSet<String>>();
            if (cacheBlock.ruleSetMemberTagFuncs != null && cacheBlock.ruleSetMemberTagFuncs.Length > 0)
            {
                foreach (pafClientCacheBlockEntry entry in cacheBlock.ruleSetMemberTagFuncs)
                {
                    result.Add(entry.key, entry.value.ToHashSet());
                }
            }
            return result;
        }
    }
}
