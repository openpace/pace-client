﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Linq;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class pafMdbPropsExtension
    {
        /// <summary>
        /// Returns true if a dimension is in the MdbProperties (i.e. visible).
        /// </summary>
        /// <param name="x">pafMdbProps object</param>
        /// <param name="dimensionName">Dim Name</param>
        /// <returns></returns>
        public static bool IsDimensionVisible(this pafMdbProps x, string dimensionName)
        {
            if (x.baseDims.Any(s => s.ToLower().Equals(dimensionName.ToLower())))
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Returns the associated base dimension name for a supplied attribute dimension.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="attributeDim"></param>
        /// <returns></returns>
        public static string GetAttributeBaseDimensionParent(this pafMdbProps x, string attributeDim)
        {
            if (x == null || x.cachedAttributeDims == null) return null;

            for (int i = 0; i < x.baseDimLookupKeys.Length; i++)
            {
                if (x.baseDimLookupKeys[i] == attributeDim)
                {
                    return x.baseDimLookupValues[i];
                }
            }
            return null;
        }
    }
}
