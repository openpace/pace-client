﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Linq;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    internal static class pafDimSpecExtension
    {
        /// <summary>
        /// Creates a new pafDimSpec
        /// </summary>
        /// <param name="dimension"></param>
        /// <returns></returns>
        public static pafDimSpec CreateNew(string dimension)
        {
            pafDimSpec temp = new pafDimSpec();
            temp.dimension = dimension;
            return temp;
        }

        /// <summary>
        /// Creates a new pafDimSpec
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="item"></param>
        /// <returns></returns>
        public static pafDimSpec CreateNew(string dimension, string item)
        {
            return CreateNew(dimension, new [] {item});
        }

        /// <summary>
        /// Creates a new pafDimSpec.
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="expressionList"></param>
        /// <returns></returns>
        public static pafDimSpec CreateNew(string dimension, IEnumerable<string> expressionList)
        {
            pafDimSpec temp = new pafDimSpec();
            temp.dimension = dimension;
            if (expressionList != null)
            {
                temp.expressionList = expressionList.ToArray();
            }
            return temp;
        }
    }
}
