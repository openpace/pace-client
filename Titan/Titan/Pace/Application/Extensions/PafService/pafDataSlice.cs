﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    internal static class pafDataSliceExtension
    {
        /// <summary>
        /// Uncompress a pafDataSlice
        /// </summary>
        /// <param name="x"></param>
        public static void Uncompress(this pafDataSlice x)
        {
            Stopwatch startTime = Stopwatch.StartNew();
            if (x == null)
            {
                PafApp.GetLogger().Warn("Data slice is null.");
            }
            else if (x.compressed)
            {
                x.data = FromBase64(x.compressedData);
            }
            else
            {
                PafApp.GetLogger().Info("Data slice is not compressed.");
            }

            startTime.Stop();
            PafApp.GetLogger().InfoFormat("Uncompress, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
        }


        /// <summary>
        /// Convert a Base64 string to a double array of data
        /// </summary>
        /// <param name="x">the Base64 string</param>
        /// <returns></returns>
        public static double?[] FromBase64(string x)
        {
 
            if (x.Length > 0 && x.Length % 12 == 0)
            {
                int arrayLength = x.Length / 12;
                double?[] dataSlice = new double?[arrayLength];
                int startIndex = 0;

                for (int i = 0; i < arrayLength; i++)
                {
                    string subString = x.Substring(startIndex, 12);
                    byte[] bytes = Convert.FromBase64String(subString);

                    dataSlice[i] = BitConverter.ToDouble(bytes, 0);
                    startIndex += 12;
                }
                return dataSlice;
            }
            else
            {
                return new double?[0];
            }
        }
    }
}
