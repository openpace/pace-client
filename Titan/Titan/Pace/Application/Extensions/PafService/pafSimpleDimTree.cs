﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using Titan.Pace.Data;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class pafSimpleDimTreeExtension
    {
        /// <summary>
        /// Converts a pafSimpleDimTree to a SimpleDimTrees
        /// </summary>
        /// <param name="pafTree"></param>
        /// <param name="baseDim">Is the tree a base dimension.</param>
        /// <param name="levelZeroOnly"></param>
        /// <param name="hidden">Hide the tree from the users.</param>
        /// <param name="parentKey">Parent Base dim (N/A if baseDim is true)</param>
        /// <param name="generationInfo">Dimension generation label information.</param>
        /// <param name="levelInfo">Level label information.</param>
        /// <param name="simpleDimTrees">If null a new SimpleDimTrees object is created.</param>
        /// <returns></returns>
        public static SimpleDimTrees ToSimpleDimTree(this pafSimpleDimTree pafTree, bool baseDim, bool levelZeroOnly, bool hidden = false, 
            long? parentKey = null, dimLevelGenInfo generationInfo = null, dimLevelGenInfo levelInfo = null, SimpleDimTrees simpleDimTrees = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            SimpleDimTrees result = simpleDimTrees ?? new SimpleDimTrees();

            result.AddTree(pafTree, baseDim, levelZeroOnly, parentKey, hidden);
            result.AddTreeInformation(pafTree.id, generationInfo, levelInfo);

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ToSimpleDimTree runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

            return result;
        }

        /// <summary>
        /// Get the alias values for a set of alias tables.
        /// </summary>
        /// <param name="pafTree">Server Simple Tree</param>
        /// <param name="member">Member to find.</param>
        /// <returns>A Dictionary of strings</returns>
        public static Dictionary<string, string> GetAliasValues(this pafSimpleDimTree pafTree, pafSimpleDimMember member)
        {
            Dictionary<string, string> aliasValues = new Dictionary<string, string>();
            IEnumerable<string> aliasTables = pafTree.aliasTableNames;

            foreach (string aliasTable in aliasTables)
            {
                string aliasValue = GetAlias(pafTree, member, aliasTable);
                if (!String.IsNullOrEmpty(aliasValue))
                {
                    aliasValues.Add(aliasTable, aliasValue);
                }
            }

            return aliasValues;
        }

        /// <summary>
        /// Finds the alias of a member within a specific PafSimpleTree.
        /// </summary>
        /// <param name="pafTree"></param>
        /// <param name="member">The name of the member, for which you want the alias</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>The alias string</returns>
        public static string GetAlias(this pafSimpleDimTree pafTree, pafSimpleDimMember member, string aliasTable)
        {

            pafSimpleDimMember[] simpleTree = pafTree.memberObjects;

            if (simpleTree == null) return null;

            return simpleTree.GetPafSimpleMemeberAliasValue(member, aliasTable);
        }


        /// <summary>
        /// Expands (decompresses) an array of pafSimpleDimTree
        /// </summary>
        /// <param name="simpleDimTrees"></param>
        public static void Uncompress(this IEnumerable<pafSimpleDimTree> simpleDimTrees)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            foreach (pafSimpleDimTree simpleDimTree in simpleDimTrees)
            {
                DateTime startTime2 = DateTime.Now;

                if (simpleDimTree.compressed)
                {
                    List<String> aliasTableNames = simpleDimTree.compAliasTableNames.ConverttoList(simpleDimTree.elementDelim);
                    Dictionary<int, String> memberIndices = simpleDimTree.compMemberIndex.ConverttoDictionary(simpleDimTree.groupDelim, simpleDimTree.elementDelim);
                    List<List<String>> parentChildRecords = simpleDimTree.compParentChild.ConverttoListofLists(simpleDimTree.groupDelim, simpleDimTree.elementDelim);
                    Dictionary<int, ChildMember> simpleDimMembers = new Dictionary<int, ChildMember>();
                    List<pafSimpleDimMember> simpleDimMembersList = new List<pafSimpleDimMember>();
                    List<String> traversedMembers = new List<string>();

                    foreach (List<String> childRecord in parentChildRecords)
                    {
                        pafSimpleDimMember simpleDimMember = new pafSimpleDimMember();
                        pafSimpleDimMemberProps simpleMemberProps = new pafSimpleDimMemberProps();
                        int i = 0;
                        int j = 0;
                        string pKey = "";
                        string cKey = "";
                        int pIndex = 0;
                        int cIndex = 0;
                        string[] aliasValues = new string[childRecord.Count - 6];

                        foreach (string record in childRecord)
                        {
                            switch (i++)
                            {
                                case 0:
                                    {
                                        if (!record.Equals("null"))
                                        {
                                            pIndex = int.Parse(record);
                                            memberIndices.TryGetValue(pIndex, out pKey);
                                        }
                                        simpleDimMember.parentKey = pKey;
                                        break;
                                    }
                                case 1:
                                    {
                                        cIndex = int.Parse(record);
                                        memberIndices.TryGetValue(cIndex, out cKey);
                                        simpleDimMember.key = cKey;
                                        break;
                                    }
                                case 2:
                                    {
                                        simpleMemberProps.generationNumber = int.Parse(record);
                                        break;
                                    }
                                case 3:
                                    {
                                        simpleMemberProps.levelNumber = int.Parse(record);
                                        break;
                                    }
                                case 4:
                                    {
                                        simpleMemberProps.timeBalanceOption = Convert.ToInt32(record);
                                        break;
                                    }
                                case 5:
                                    {
                                        simpleMemberProps.twoPassCalc = Convert.ToInt32(record) == 1;
                                        break;
                                    }
                                default:
                                    {
                                        aliasValues[j++] = record;
                                        break;
                                    }
                            }
                        }
                        simpleMemberProps.aliasKeys = aliasTableNames.ToArray();
                        simpleMemberProps.aliasValues = aliasValues;
                        simpleDimMember.pafSimpleDimMemberProps = simpleMemberProps;

                        ChildMember parentMember;
                        simpleDimMembers.TryGetValue(pIndex, out parentMember);

                        if (parentMember != null)
                        {
                            if (parentMember.Children == null)
                            {
                                parentMember.Children = new List<string>();
                            }

                            parentMember.Children.Add(cKey);
                            simpleDimMembers[pIndex].SimpleDimMember.childKeys = parentMember.Children.ToArray();
                        }

                        ChildMember childMember = new ChildMember {SimpleDimMember = simpleDimMember};
                        simpleDimMembers.Add(cIndex, childMember);
                        simpleDimMembersList.Add(simpleDimMember);
                        traversedMembers.Add(cKey);
                    }
                    simpleDimTree.aliasTableNames = aliasTableNames.ToArray();
                    simpleDimTree.memberObjects = simpleDimMembersList.ToArray();
                    simpleDimTree.traversedMembers = traversedMembers.ToArray();
                }
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Expanding: " + simpleDimTree.id + ", runtime: " + (DateTime.Now - startTime2).TotalSeconds + " (s)");
            }
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("Expand runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }
    }
    internal class ChildMember
    {
        pafSimpleDimMember simpleDimMember;
        List<string> children;

        /// <summary>
        /// 
        /// </summary>
        public pafSimpleDimMember SimpleDimMember
        {
            get { return simpleDimMember; }
            set { simpleDimMember = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public List<string> Children
        {
            get { return children; }
            set { children = value; }
        }
    }
}
