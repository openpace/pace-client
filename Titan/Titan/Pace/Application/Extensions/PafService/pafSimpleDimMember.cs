﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Linq;
using Titan.PafService;

namespace Titan.Pace.Application.Extensions.PafService
{
    /// <remarks/>
    internal static class pafSimpleDimMemberExtension
    {
        /// <summary>
        /// Gets the alias value for a paf simple member.
        /// </summary>
        /// <param name="treeMembers">Array of tree members.</param>
        /// <param name="member">The member name.</param>
        /// <param name="aliasTable">The name of the alias table.</param>
        /// <returns>The alias value if it exists, null if nothing exists.</returns>
        public static string GetPafSimpleMemeberAliasValue(this pafSimpleDimMember[] treeMembers, pafSimpleDimMember member, string aliasTable)
        {
            int index = Array.IndexOf(member.pafSimpleDimMemberProps.aliasKeys, aliasTable);
            return index >= 0 ? treeMembers.Where(s => s.Equals(member)).Select(s => s.pafSimpleDimMemberProps.aliasValues[index]).FirstOrDefault() : null;
        }
    }
}