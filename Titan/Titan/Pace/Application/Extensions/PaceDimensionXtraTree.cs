﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;
using Titan.Pace.Application.Controls.TreeView;

namespace Titan.Pace.Application.Extensions
{
    internal static class PaceDimensionXtraTreeExtension
    {
        /// <summary>
        /// Unchecks all the valid nodes in the tree.
        /// </summary>
        /// <param name="x"></param>
        public static void UncheckAllValid(this PaceDimensionXtraTree x)
        {
            UncheckAllNodes op = new UncheckAllNodes(x);
            x.NodesIterator.DoOperation(op);
        }

        /// <summary>
        /// Checks all the valid nodes in the tree.
        /// </summary>
        /// <param name="x"></param>
        public static void CheckAllValid(this PaceDimensionXtraTree x)
        {
            CheckAllNodes op = new CheckAllNodes(x);
            x.NodesIterator.DoOperation(op);
        }

        /// <summary>
        /// Checks all the valid descendants of a specified node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        public static void CheckAllValidDescendants(this PaceDimensionXtraTree x, TreeListNodes nodes)
        {
            CheckAllNodes op = new CheckAllNodes(x);
            x.NodesIterator.DoLocalOperation(op, nodes);
        }

        /// <summary>
        /// Checks all the nodes at a specified level.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        /// <param name="levelColumnName"></param>
        /// <param name="level"></param>
        public static void CheckAllNodesAtLevel(this PaceDimensionXtraTree x, TreeListNodes nodes, string levelColumnName, int level)
        {
            CheckAllNodesAtLevel op = new CheckAllNodesAtLevel(x, levelColumnName, level);
            x.NodesIterator.DoLocalOperation(op, nodes);
        }

        /// <summary>
        /// Checks all the nodes at a specified generation.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        /// <param name="generationColumnName"></param>
        /// <param name="generation"></param>
        public static void CheckAllNodesAtGeneration(this PaceDimensionXtraTree x, TreeListNodes nodes, string generationColumnName, int generation)
        {
            CheckAllNodesAtGeneration op = new CheckAllNodesAtGeneration(x, generationColumnName, generation);
            x.NodesIterator.DoLocalOperation(op, nodes);
        }

        /// <summary>
        /// Checks all the valid children of a specified node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        public static void CheckAllValidChildren(this PaceDimensionXtraTree x, TreeListNodes nodes)
        {
            List<TreeListNode> validNodes = new List<TreeListNode>(nodes.Count);
            validNodes.AddRange(from TreeListNode t in nodes where x.IsNodeCheckable(t) select t);
            x.CheckNodes(validNodes, false, false);
        }

        /// <summary>
        /// Checks all the valid grandchildren of a node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        public static void CheckAllValidGrandChildren(this PaceDimensionXtraTree x, TreeListNodes nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                CheckAllValidChildren(x, node.Nodes);
            }
        }

        /// <summary>
        /// Checks all Ancestors of a node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="child"></param>
        public static void CheckAllValidAncestors(this PaceDimensionXtraTree x, TreeListNode child)
        {
            x.CheckValidParent(child);
            if (child.ParentNode != null)
            {
                x.CheckAllValidAncestors(child.ParentNode);
            }
        }

        /// <summary>
        /// Checks the parent of a node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="child"></param>
        public static void CheckValidParent(this PaceDimensionXtraTree x, TreeListNode child)
        {
            if (child.ParentNode != null && IsNodeCheckable(x, child.ParentNode))
            {
                child.ParentNode.Checked = true;
            }
        }


        /// <summary>
        /// Return is the specified node can be checked.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="node"></param>
        /// <returns></returns>
        public static bool IsNodeCheckable(this PaceDimensionXtraTree tree, TreeListNode node)
        {
            //If we've filtered out this node, don't let the user select it.
            if ((byte)node.GetValue(tree.VisibleColumnName) == 0) return false;
            if (!node.Visible) return false;

            if (tree.MultiCheck && tree.SelectableLevels.Count == 0 && tree.SelectableGenerations.Count == 0 && tree.UnselectableLevels.Count == 0) return true;


            int level = (int)node.GetValue(tree.LevelColumnName);

            //Check to see if the level is checkable.
            if (tree.SelectableLevels.Count > 0 && !tree.SelectableLevels.Contains(level))
            {
                return false;
            }
            if (tree.UnselectableLevels.Count > 0 && tree.UnselectableLevels.Contains(level))
            {
                return false;
            }
            int generation = (int) node.GetValue(tree.GenerationColumnName);
            //Check to see if the generation is checkable.
            if (tree.SelectableGenerations.Count > 0 && !tree.SelectableGenerations.Contains(generation))
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Checks to see if the node has a visible child.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        public static bool HasVisibleChild(this PaceDimensionXtraTree x, TreeListNodes nodes)
        {
            return nodes.Cast<TreeListNode>().Any(x.IsNodeCheckable);
        }

        /// <summary>
        /// Checks to see if the node has a visible HasVisibleDescendant.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        public static bool HasVisibleDescendant(this PaceDimensionXtraTree x, TreeListNodes nodes)
        {
            foreach (TreeListNode node in nodes)
            {
                VisibleDescendant op = new VisibleDescendant(x);
                x.NodesIterator.DoLocalOperation(op, node.Nodes);
                if (op.HasVisibleDescendant)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Checks to see if the node has a visible ancestor.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="child"></param>
        public static bool HasVisibleAncestor(this PaceDimensionXtraTree x, TreeListNode child)
        {
            if (x.IsParentValid(child))
            {
                return true;
            }

            return child.ParentNode != null && x.HasVisibleAncestor(child.ParentNode);
        }

        /// <summary>
        /// Checks the parent of a node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="child"></param>
        public static bool IsParentValid(this PaceDimensionXtraTree x, TreeListNode child)
        {
            if (child.ParentNode != null && IsNodeCheckable(x, child.ParentNode))
            {
                return true;
            }
            return false;
        }

    }
    /// <summary>
    /// The operation class that checks all nodes in a TreeListNodes nodes
    /// </summary>
    internal class CheckAllNodes : TreeListOperation
    {
        public PaceDimensionXtraTree Parent { get; set; }
        public CheckAllNodes(PaceDimensionXtraTree parent)
            : base()
        {
            Parent = parent;
        }
        public override void Execute(TreeListNode node)
        {
            if (node.CheckState == CheckState.Unchecked)
            {
                if (Parent.IsNodeCheckable(node))
                {
                    node.Checked = true;
                }
            }
        }
    }

    /// <summary>
    /// The operation class that unchecks all nodes in a TreeListNodes nodes
    /// </summary>
    internal class UncheckAllNodes : TreeListOperation
    {
        public PaceDimensionXtraTree Parent { get; set; }
        public UncheckAllNodes(PaceDimensionXtraTree parent): base()
        {
            Parent = parent;
        }
        public override void Execute(TreeListNode node)
        {
            if (node.CheckState == CheckState.Checked)
            {
                if (Parent.IsNodeCheckable(node))
                {
                    node.Checked = false;
                }
            }
        }
    }

    /// <summary>
    /// The operation class that checks to see if the node has a vibible VisibleDescendant.
    /// </summary>
    internal class VisibleDescendant : TreeListOperation
    {
        public PaceDimensionXtraTree Parent { get; set; }
        public bool HasVisibleDescendant { get; set; }
        public VisibleDescendant(PaceDimensionXtraTree parent)
            : base()
        {
            Parent = parent;
            HasVisibleDescendant = false;
        }
        public override void Execute(TreeListNode node)
        {
            if (!HasVisibleDescendant &&  Parent.IsNodeCheckable(node))
            {
                HasVisibleDescendant = true;
            }
        }
    }

    /// <summary>
    /// The operation class that checks all nodes in a TreeListNodes nodes at a specified level
    /// </summary>
    internal class CheckAllNodesAtLevel : TreeListOperation
    {
        public PaceDimensionXtraTree Parent { get; set; }
        public int Level { get; set; }
        public string LevelColumn { get; set; }
        public CheckAllNodesAtLevel(PaceDimensionXtraTree parent, string levelColumn, int level)
            : base()
        {
            Parent = parent;
            LevelColumn = levelColumn;
            Level = level;
        }
        public override void Execute(TreeListNode node)
        {
            if (node.CheckState == CheckState.Unchecked)
            {
                if (Parent.IsNodeCheckable(node))
                {
                    if (Convert.ToInt32(node.GetValue(LevelColumn)) == Level)
                    {
                        node.Checked = true;
                    }
                }
            }
        }
    }

    /// <summary>
    /// The operation class that checks all nodes in a TreeListNodes nodes at a specified generation
    /// </summary>
    internal class CheckAllNodesAtGeneration : TreeListOperation
    {
        public PaceDimensionXtraTree Parent { get; set; }
        public int Generation { get; set; }
        public string GenerationColumn { get; set; }
        public CheckAllNodesAtGeneration(PaceDimensionXtraTree parent, string generationColumn, int generation)
            : base()
        {
            Parent = parent;
            GenerationColumn = generationColumn;
            Generation = generation;
        }
        public override void Execute(TreeListNode node)
        {
            if (node.CheckState == CheckState.Unchecked)
            {
                if (Parent.IsNodeCheckable(node))
                {
                    if (Convert.ToInt32(node.GetValue(GenerationColumn)) == Generation)
                    {
                        node.Checked = true;
                    }
                }
            }
        }
    }
}
