﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraEditors;

namespace Titan.Pace.Application.Extensions
{
    internal static class PaceMessageBox
    {
        private static readonly string Text;
        private const string StyleName = "Metropolis";

        static PaceMessageBox()
        {
            Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");
        }

        public static void Show(IWin32Window owner, string caption, MessageBoxButtons buttons)
        {
            UserLookAndFeel lookAndFeel = new UserLookAndFeel(owner);
            XtraMessageBox.Show(lookAndFeel, caption, Text, buttons);
        }

        public static void Show(IWin32Window owner, Exception ex, MessageBoxButtons buttons)
        {
            UserLookAndFeel lookAndFeel = new UserLookAndFeel(owner);
            lookAndFeel.Style = LookAndFeelStyle.Flat;
            lookAndFeel.SkinName = StyleName;
            lookAndFeel.UseDefaultLookAndFeel = false;
            XtraMessageBox.Show(lookAndFeel, ex.Message, Text, buttons);
        }
    }
}
