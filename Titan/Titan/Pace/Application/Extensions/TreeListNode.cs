﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Linq;
using DevExpress.XtraTreeList.Nodes;
using Titan.Pace.Data;

namespace Titan.Pace.Application.Extensions
{
    public static class TreeListNodeExtension
    {
        /// <summary>
        /// Expands a TreeListNode
        /// </summary>
        /// <param name="x"></param>
        public static void Expand(this TreeListNode x)
        {
            if (x != null && !x.Expanded)
            {
                x.Expanded = true;
            }
        }

        /// <summary>
        /// Select all the children of a node.
        /// </summary>
        /// <param name="x"></param>
        public static void CheckAllChildren(this TreeListNode x)
        {
            foreach (TreeListNode node in x.Nodes)
            {
                node.Checked = true;
            }
        }

        /// <summary>
        /// Select all the children of a node.
        /// </summary>
        /// <param name="x"></param>
        public static void CheckAllDescendants(this TreeListNode x)
        {
            foreach (TreeListNode node in x.Nodes)
            {
                node.Checked = true;
                if (node.HasChildren)
                {
                    CheckAllChildren(node);
                }
            }
        }

        /// <summary>
        /// Checks all Ancestors of a node.
        /// </summary>
        /// <param name="child"></param>
        public static void CheckAllAncestors(this TreeListNode child)
        {
            child.CheckParent();
            if (child.ParentNode != null)
            {
                child.ParentNode.CheckAllAncestors();
            }
        }

        /// <summary>
        /// Checks the parent of a node.
        /// </summary>
        /// <param name="child"></param>
        public static void CheckParent(this TreeListNode child)
        {
            if (child.ParentNode != null)
            {
                child.ParentNode.Checked = true;
            }
        }

        /// <summary>
        /// Returns the member name (based off SimpleDimTrees) of a node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="tree"></param>
        /// <returns></returns>
        public static string GetSimpleTreeText(this TreeListNode x, SimpleDimTrees tree)
        {
            return x.GetDisplayText(tree.Members.MemberNameColumn.ColumnName);
        }

        /// <summary>
        /// Gets the text of the first selection.
        /// </summary>
        /// <returns></returns>
        public static string GetText(this TreeListNode x, string columnTextToReturn)
        {
            return x != null ? x.GetDisplayText(columnTextToReturn) : null;
        }

        /// <summary>
        /// Gets the text of an array of nodes.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetNodesText(this List<TreeListNode> x, string columnTextToReturn)
        {
            List<string> temp = new List<string>(x.Count);
            temp.AddRange(x.Select(node => GetText(node, columnTextToReturn)));
            return temp;
        }
        
    }

    ///// <summary>
    ///// The operation class that checks all nodes in a TreeListNodes nodes
    ///// </summary>
    //internal class CheckAllChildren : TreeListOperation
    //{
    //    public CheckAllChildren()
    //        : base()
    //    {
    //    }
    //    public override void Execute(TreeListNode node)
    //    {
    //        if (node.CheckState == CheckState.Unchecked)
    //        {
    //            node.Checked = true;
    //        }
    //    }
    //}
}
