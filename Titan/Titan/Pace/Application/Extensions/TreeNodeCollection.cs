﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Linq;
using System.Windows.Forms;
using Titan.Pace.Application.Controls.TreeView;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class TreeNodeCollectionExtensions
    {
        /// <summary>
        /// Changes the display text of all the PaceTreeNodes in a collection.
        /// </summary>
        /// <param name="x">Extended TreeNodeCollection</param>
        /// <param name="aliasTable">Alias table to display</param>
        public static void ChangeTreeNodeText(this TreeNodeCollection x, string aliasTable)
        {
            foreach (PaceTreeNode tn in x.Cast<PaceTreeNode>().TakeWhile(tn => !tn.Text.Equals("Dummy")))
            {
                tn.AliasTableToDisplay = aliasTable;
                tn.UpdateNodeText();

                if (tn.Nodes != null && tn.Nodes.Count > 0)
                {
                    ChangeText(tn.Nodes, aliasTable);
                }
            }
        }


        /// <summary>
        /// Changes the text of a PaceTreeNode.
        /// </summary>
        /// <param name="x">Extended TreeNodeCollection</param>
        /// <param name="aliasTable">Alias table to display</param>
        private static void ChangeText(this TreeNodeCollection x, string aliasTable)
        {
            foreach (PaceTreeNode tn in x.Cast<PaceTreeNode>().TakeWhile(tn => !tn.Text.Equals("Dummy")))
            {
                tn.AliasTableToDisplay = aliasTable;
                tn.UpdateNodeText();

                if (tn.Nodes != null && tn.Nodes.Count > 0)
                {
                    ChangeText(tn.Nodes, aliasTable);
                }
            }
        }
    }
}
