﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;

namespace Titan.Pace.Application.Extensions
{
    internal static class HashSet
    {

        /// <summary>
        /// Returns a new HashSet with a set capacity.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="increaseBy"></param>
        /// <returns></returns>
        public static HashSet<T> Resize<T>(this HashSet<T> source, int increaseBy)
        {
            int size = source.Count + increaseBy;
            List<T> temp = new List<T>(size);
            temp.AddRange(source);
            return new HashSet<T>(temp);
        }
    }
}
