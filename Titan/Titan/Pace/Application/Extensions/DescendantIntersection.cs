﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using Titan.Pace.Base.Data;
using Titan.Pace.DataStructures;

namespace Titan.Pace.Application.Extensions
{
    internal static class DescendantIntersectionExtension
    {
        ///// <summary>
        ///// Converts a List of DescendantIntersection to the List of Descendant intersections.
        ///// </summary>
        ///// <param name="desIntersections"></param>
        ///// <returns></returns>
        //public static List<List<Intersection>> DescendantsToList(this List<DescendantIntersection> desIntersections)
        //{
        //    if (desIntersections == null || desIntersections.Count == 0) return null;

        //    List<List<Intersection>> result = new List<List<Intersection>>(desIntersections.Count);
        //    result.AddRange(desIntersections.Select(di => di.Descendants));
        //    return result;
        //}

        /// <summary>
        /// Converts a List of DescendantIntersection to the List of Parent intersections.
        /// </summary>
        /// <param name="desIntersections"></param>
        /// <returns></returns>
        public static List<CoordinateSet> ParentsToList(this List<DescendantIntersection> desIntersections)
        {
            if (desIntersections == null || desIntersections.Count == 0) return null;

            List<CoordinateSet> result = new List<CoordinateSet>(desIntersections.Count);
            foreach (DescendantIntersection di in desIntersections)
            {
                result.Add(di.Parent);
            }
            return result;
        }
    }
}
