﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Diagnostics;
using DevExpress.XtraGrid;

namespace Titan.Pace.Application.Extensions
{
    internal static class GridControlExtension
    {
        /// <summary>
        /// Binds a data table to a Grid Control.
        /// </summary>
        /// <param name="x">DevExpress Grid Control</param>
        /// <param name="dataObject">Data object (DataTable, etc.)</param>
        public static void BindData(this GridControl x, object dataObject)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            x.BeginUpdate();
            x.DataSource = dataObject;
            x.EndUpdate();

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("BindDataSetToGrid: {0}, runtime: {1} (ms)", new object[] { x.Name, stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

    }
}
