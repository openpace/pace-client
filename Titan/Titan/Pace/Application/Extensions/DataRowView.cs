﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Titan.Pace.Application.Extensions
{
    internal static class DataRowViewExtension
    {
        /// <summary>
        /// Gets a list of DataRows from a DataRowView
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static IEnumerable<DataRow> ToDataRows(this IEnumerable<DataRowView> x)
        {
            return x.Select(row => row.Row).ToList();
        }

        /// <summary>
        /// Gets a list of DataRows from a DataRowView
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static IEnumerable<DataRow> ToDataRows(this DataRowView x)
        {
            return x.Row.ToSingleList();
        }
    }
}
