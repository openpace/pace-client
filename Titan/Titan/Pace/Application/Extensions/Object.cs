﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class ObjectExtension
    {
        /// <summary>
        /// Parses an Enum and creates it from a int.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(this int value)
        {
            return (T) Enum.ToObject(typeof(T), value);
        }

        /// <summary>
        /// Converts an object to a List of objects.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="x">Object to convert to a list.</param>
        public static List<T> ToSingleList<T>(this T x)
        {
            return new List<T> {x};
        }
         

        /// <summary>
        /// Checks to see if a value is numeric (double).
        /// </summary>
        /// <param name="x">Object to extend.</param>
        /// <returns>True if the value is numeric, false if the value is non numeric.</returns>
        public static bool IsDouble(this object x)
        {
            try
            {
                double dummy;
                return double.TryParse(x.ToString(), NumberStyles.Any, null, out dummy);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Checks to see if a value is numeric (double).
        /// </summary>
        /// <param name="x">Object to extend.</param>
        /// <returns>True if the value is numeric, false if the value is non numeric.</returns>
        public static bool IsObjDouble(object x)
        {
            try
            {
                double dummy;
                return double.TryParse(x.ToString(), NumberStyles.Any, null, out dummy);
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Gets the double value of an object (if possible).
        /// </summary>
        /// <param name="x">Object to extend.</param>
        /// <param name="throwExceptionOnInvalidParse">True to throw and exception if the object cannot be parsed into a double, false to just return 0.0</param>
        /// <returns>The double representation of the object, or 0.0 if the object is not a double and cRef="throwExceptionOnInvalidParse" is false</returns>
        /// <exception cref="ArgumentException">If the object is not a double and  cref="throwExceptionOnInvalidParse" is true.</exception>
        public static double GetDouble(this object x, bool throwExceptionOnInvalidParse = false)
        {
            try
            {
                double dummy;
                if (double.TryParse(x.ToString(), NumberStyles.Any, null, out dummy))
                {
                    return dummy;
                }
                if (throwExceptionOnInvalidParse)
                {
                    throw new ArgumentException("Argument is not a double.");
                }
                return 0.0;
            }
            catch
            {
                if (throwExceptionOnInvalidParse)
                {
                    throw new ArgumentException("Argument is not a double.");
                }
                return 0.0;
            }
        }

        /// <summary>
        /// Gets the int value of an object (if possible).
        /// </summary>
        /// <param name="x">Object to extend.</param>
        /// <param name="throwExceptionOnInvalidParse">True to throw and exception if the object cannot be parsed into a int, false to just return 0</param>
        /// <returns>The int representation of the object, or 0 if the object is not a double and cRef="throwExceptionOnInvalidParse" is false</returns>
        /// <exception cref="ArgumentException">If the object is not a int and  cref="throwExceptionOnInvalidParse" is true.</exception>
        public static int? GetInteger(this object x, bool throwExceptionOnInvalidParse = false)
        {
            try
            {
                int dummy;
                if (int.TryParse(x.ToString(), NumberStyles.Any, null, out dummy))
                {
                    return dummy;
                }
                if (throwExceptionOnInvalidParse)
                {
                    throw new ArgumentException("Argument is not an integer.");
                }
                return null;
            }
            catch
            {
                if (throwExceptionOnInvalidParse)
                {
                    throw new ArgumentException("Argument is not an integer.");
                }
                return null;
            }
        }

        /// <summary>
        /// Checks the value to see if it's an integer.
        /// </summary>
        /// <param name="x">Object to extend.</param>
        /// <param name="result">The parsed integer value.</param>
        /// <returns>true if it's an integer, false if not.</returns>
        public static bool IsInteger(this object x, out int result)
        {
            result = 0;

            return !x.IsNull() && int.TryParse(x.ToString(), out result);
        }

        /// <summary>
        /// Checks if an object is null.
        /// </summary>
        /// <param name="x">Object to extend.</param>
        /// <returns>true if the object is null, false if non-null.</returns>
        public static bool IsNull(this object x)
        {
            return x == null;
        }

        
        /// <summary>
        /// Serializes an object to an xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="x">Object to extend.</param>
        /// <param name="filePath">Path to serialize the file to.</param>
        /// <param name="fileName">Filename to serialize to.</param>
        /// <param name="typeInherited"></param>
        public static void Save<T>(this T x, string filePath, string fileName, Type typeInherited = null)
        {
            string pathFileName = Path.Combine(filePath, fileName);
            x.Save(pathFileName, typeInherited);
        }

        /// <summary>
        /// Serializes an object to an xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="x">Object to extend.</param>
        /// <param name="fileName">Filename to serialize to.</param>
        /// <param name="typeInherited"></param>
        public static void Save<T>(this T x, string fileName, Type typeInherited = null)
        {

            if (String.IsNullOrEmpty(fileName))
            {
                throw new NullReferenceException("Filename cannot be null.");
            }


            if (!Path.HasExtension(fileName))
            {
                fileName += ".xml";
            }

            //using (var stream = File.Create(fileName))
            using (var writer = XmlWriter.Create(fileName))
            {
                if (typeInherited == null)
                {
                    new XmlSerializer(typeof(T)).Serialize(writer, x);
                }
                else
                {
                    Type[] types = new[] { typeInherited };
                    new XmlSerializer(typeof(T), types).Serialize(writer, x);
                }
            }
        }

        /// <summary>
        /// Loads an object from a serialized xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="filePath">Path to serialize the file to.</param>
        /// <param name="fileName"></param>
        /// <param name="typeInherited"></param>
        public static T Load<T>(string filePath, string fileName, Type typeInherited = null)
        {
            string pathFileName = Path.Combine(filePath, fileName);
            return Load<T>(pathFileName, typeInherited);
        }

        /// <summary>
        /// Loads an object from a serialized xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="fileName"></param>
        /// <param name="typeInherited"></param>
        public static T Load<T>(string fileName, Type typeInherited = null)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                throw new NullReferenceException("Filename cannot be null.");
            }

            if (!Path.HasExtension(fileName))
            {
                fileName += ".xml";
            }

            if (!File.Exists(fileName))
            {
                return default(T);
            }

            T obj;

            //using (var stream = File.OpenRead(fileName))
            using (var reader = XmlReader.Create(fileName))
            {
                if (typeInherited == null)
                {
                    obj = (T)new XmlSerializer(typeof(T)).Deserialize(reader);
                    //return (T)new XmlSerializer(typeof(T)).Deserialize(stream);
                }
                else
                {
                    Type[] types = new[] {typeInherited};
                    obj = (T)new XmlSerializer(typeof(T), types).Deserialize(reader);
                    //return (T)new XmlSerializer(typeof(T), types).Deserialize(stream);
                }
            }
            return obj;
        }
    }
}
