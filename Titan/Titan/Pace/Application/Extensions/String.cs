﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using Titan.Pace.Application.Connection;
using Titan.Properties;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class StringExtensions
    {

        /// <summary>
        /// Parses an Enum and creates it from a string.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        public static T ParseEnum<T>(this string value)
        {
            return (T)Enum.Parse(typeof(T), value, true);
        }

        /// <summary>
        /// Generates a unique string using a GUID.
        /// </summary>
        /// <returns></returns>
        public static string ToGuid()
        {
            long i = Guid.NewGuid().ToByteArray().Aggregate<byte, long>(1, (current, b) => current*((int) b + 1));
            return string.Format("{0:x}", i - DateTime.Now.Ticks);
        }

        /// <summary>
        /// Add spaces before Capital Letters
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string AddSpacesToSentence(this string x)
        {
            if (String.IsNullOrEmpty(x))
            {
                return string.Empty;
            }
            StringBuilder newText = new StringBuilder(x.Length * 2);
            newText.Append(x[0]);
            for (int i = 1; i < x.Length; i++)
            {
                if (char.IsUpper(x[i]) && x[i - 1] != ' ')
                {
                    newText.Append(' ');
                }
                newText.Append(x[i]);
            }
            return newText.ToString();
        }

        /// <summary>
        /// Perform a string comparsion, but ignore the case of the strings.
        /// Trim() is called on the strings before the comparison is performed.
        /// </summary>
        /// <param name="x">string to extend</param>
        /// <param name="compareTo">String to perform the comparsion against.</param>
        /// <param name="performTrim">Trim() is called on the strings before the comparison is performed.</param>
        /// <returns>true if the strings match, false if not or if either of the strings are null.</returns>
        public static bool EqualsIgnoreCase(this string x, string compareTo, bool performTrim)
        {
            if (x == null || compareTo == null) return false;

            return performTrim ? EqualsIgnoreCase(x.Trim(), compareTo.Trim()) : EqualsIgnoreCase(x, compareTo);
        }

        /// <summary>
        /// Perform a string comparsion, but ignore the case of the strings.
        /// </summary>
        /// <param name="x">string to extend</param>
        /// <param name="compareTo">String to perform the comparsion against.</param>
        /// <returns>true if the strings match, false if not or if either of the strings are null.</returns>
        public static bool EqualsIgnoreCase(this string x, string compareTo)
        {
            if (x == null || compareTo == null) return false;

            return x.Equals(compareTo, StringComparison.OrdinalIgnoreCase);

            //return System.String.Compare(x.Trim(), compareTo.Trim(), true, CultureInfo.CurrentUICulture) == 0;
        }

        /// <summary>
        /// Create a list with a single item.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static List<string> ToSingleList(this string x)
        {
            return x.ToSingleList<string>();
        }

        /// <summary>
        /// Convert a delimited string into a list of strings
        /// </summary>
        /// <param name="x">Source string to parse.</param>
        /// <param name="delimiter">Delimiter to parse the string with.</param>
        /// <returns></returns>
        public static List<string> ConverttoList(this string x, string delimiter)
        {
            //string[] stringArray = x.Split(new [] { delimiter }, StringSplitOptions.RemoveEmptyEntries);

            List<string> stringArray = x.SplitString(delimiter);

            return stringArray;
        }

        /// <summary>
        /// Custom implementation of String.Split (avoids OOM issues)
        /// </summary>
        /// <param name="x">Source string to parse.</param>
        /// <param name="delimiter">Delimiter to parse the string with.</param>
        /// <returns></returns>
        /// <remarks>http://www.codeproject.com/Articles/176031/Out-of-Memory-Exception-A-simple-string-Split-can</remarks>
        public static List<string> SplitString(this string x, string delimiter)
        {
            Stopwatch sw = Stopwatch.StartNew();
            List<string> strings;
                                                               //21000000
            //Testing with Helzberg shows this should be set at: 10000000
            //However the lower this value is set, the slower it will be.
            if (x.Length > Settings.Default.CoordListSplitMax)
            {
                PafApp.GetLogger().Info("SplitString, string lenght: " + x.Length);
                strings = x.SplitManual(delimiter, false, true);
                sw.Stop();
                PafApp.GetLogger().Info("SplitString(Manual), runtime: " + sw.ElapsedMilliseconds.ToString("N0") + " (ms)");
            }
            else
            {
                strings = x.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
            return strings;
        }

        /// <summary>
        /// Splits a string
        /// </summary>
        /// <param name="expression">Expression to split</param>
        /// <param name="delimiter">Delimiter</param>
        /// <param name="ignoreCase"></param>
        /// <param name="removeEmpty">Remove any empty splits</param>
        /// <returns></returns>
        public static List<string> SplitManual(this string expression, string delimiter, bool ignoreCase, bool removeEmpty)
        {
            int startIndex = 0;
            List<string> values = new List<string>();
            
            
            if(String.IsNullOrEmpty(delimiter))
            {
                return values;
            }

            for (int charIndex = 0; charIndex < expression.Length - 1; charIndex++)
            {
                if ((string.Compare(expression.Substring(charIndex, delimiter.Length), delimiter, ignoreCase) == 0))
                {
                    string temp = expression.Substring(startIndex, charIndex - startIndex);
                    if (!String.IsNullOrEmpty(temp.Trim()))
                    {
                        values.Add(temp.Trim());
                    }
                    startIndex = charIndex + delimiter.Length;
                }
            }

            if (startIndex < expression.Length)
            {
                values.Add(expression.Substring(startIndex, expression.Length - startIndex));
            }

            if (removeEmpty)
            {
                string s = values[values.Count - 1];
                if (s.Contains(delimiter))
                {
                    values[values.Count - 1] = s.Replace(delimiter, String.Empty);
                }
            }

            return values;
        }
        
        

        /// <summary>
        /// Convert a delimited string into a Dictionary of strings
        /// </summary>
        /// <param name="x">Source string to parse.</param>
        /// <param name="delimiter1">First delimiter to parse.</param>
        /// <param name="delimiter2">Second delimiter to parse.</param>
        /// <returns></returns>
        public static List<List<String>> ConverttoListofLists(this string x, string delimiter1, string delimiter2)
        {
            List<String> longStrings = x.ConverttoList(delimiter1);

            return longStrings.Select(str => ConverttoList(str, delimiter2)).ToList();
        }

        /// <summary>
        /// Convert a delimited string into a Dictionary
        /// </summary>
        /// <param name="x">Source string to parse.</param>
        /// <param name="delimiter1">First delimiter to parse.</param>
        /// <param name="delimiter2">Second delimiter to parse.</param>
        /// <returns></returns>
        public static Dictionary<int, String> ConverttoDictionary(this string x, string delimiter1, string delimiter2)
        {
            List<String> longStrings = x.ConverttoList(delimiter1);

            return longStrings.Select(str => ConverttoList(str, delimiter2)).ToDictionary(shortStrings => int.Parse(shortStrings[1]), shortStrings => shortStrings[0]);
        }

        /// <summary>
        /// Creates a delimited string from a string array, and an element delimiter.
        /// </summary>
        /// <param name="x">String array to delimit</param>
        /// <param name="elementDelimiter">Delimiter to add between each array element.</param>
        /// <returns></returns>
        public static string CreateDelimitedString(this string[] x, string elementDelimiter)
        {

            return String.Join(elementDelimiter, x);
        }


        /// <summary>
        /// Splits a string into an array.
        /// </summary>
        /// <param name="x">String to split.</param>
        /// <param name="delimiter">Delimiter (used to split string)</param>
        /// <param name="removeBlanks">Remove blank values from the return string array.</param>
        /// <returns>An array of strings.</returns>
        public static string[] SplitString(this string x, string delimiter, bool removeBlanks)
        {
            if (String.IsNullOrEmpty(x) || String.IsNullOrEmpty(delimiter))
            {
                return null;
            }

            //StringSplitOptions sso = removeBlanks ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            StringSplitOptions sso;
            if(removeBlanks)
            {
                sso = StringSplitOptions.RemoveEmptyEntries;
            }
            else
            {
                sso = StringSplitOptions.None;
            }

            string[] stringArray = x.Split(new [] { delimiter }, sso);

            return stringArray;
        }

        /// <summary>
        /// Inserts a string inside of another string at the first found occurence of stringToFind.
        /// </summary>
        /// <param name="x">The string to search.</param>
        /// <param name="stringToFind">The string to find.</param>
        /// <param name="insertionString">The string to insert.</param>
        public static string InsertString(this string x, string stringToFind, string insertionString)
        {
            int pos = x.IndexOf(stringToFind, StringComparison.Ordinal);
            return pos > 0 ? x.Insert(pos, insertionString) : x;
        }

        /// <summary>
        /// Validates if the string is in a valid URI/URL format.
        /// </summary>
        /// <param name="x">String to extend/validate.</param>
        /// <returns>true if the string is in a valid format, false if not.</returns>
        public static bool IsUrlValid(this string x)
        {
            try
            {
                Uri uri;
                if (Uri.TryCreate(x, UriKind.Absolute, out uri))
                {
                    if(!UriScheme.SchemeIsValid(uri.Scheme))
                    {
                        return false;
                    }
                    if (uri.Port == -1)
                    {
                        return false;
                    }
                    if (String.IsNullOrEmpty(uri.Host))
                    {
                        return false;
                    }

                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            
            }
        }
        /// <summary>
        /// Removes invalid chars from the file name.
        /// </summary>
        /// <param name="pathFileName">Path/file name</param>
        /// <returns>True if the path is valid, false if not.</returns>
        public static string RemoveInvalidFileChars(this string pathFileName)
        {
            Char[] invalidChars = Path.GetInvalidFileNameChars();

            List<char> badChars = invalidChars.ToList();
            badChars.Add('-');
            badChars.Add(' ');


            foreach (char c in badChars)
            {
                if (pathFileName.Contains(c))
                {
                    pathFileName = pathFileName.Replace(Convert.ToString(c), String.Empty);
                }
            }
            return pathFileName;
        }

        /// <summary>
        /// Prints a string to a file.
        /// </summary>
        /// <param name="s">String to print to file.</param>
        /// <param name="pathFileName">Full path and file name to save the file.</param>
        public static void ToFile(this string s, string pathFileName)
        {
            StringBuilder sb = new StringBuilder(s);
            sb.ToFile(pathFileName);
        }

        /// <summary>
        /// Prints a stringbuilder to a file.
        /// </summary>
        /// <param name="sb">StringBuilder to print to file.</param>
        /// <param name="pathFileName">Full path and file name to save the file.</param>
        public static void ToFile(this StringBuilder sb, string pathFileName)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            using (StreamWriter outfile = new StreamWriter(pathFileName))
            {
                outfile.Write(sb.ToString());
            }
            stopwatch.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("ToFile() runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Returns true if the string is a number.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsNumeric(this string s)
        {
            float output;
            return float.TryParse(s, out output);
        }

        /// <summary>
        /// Returns true if the string is an integer.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool IsInteger(this string s)
        {
            int output;
            return int.TryParse(s, out output);
        }

        /// <summary>
        /// Returns true if any cell in the array is non null.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static bool ContainsNonNullValues(this IEnumerable<string> s)
        {
            return s.Any(str => !String.IsNullOrWhiteSpace(str));
        }

        /// <summary>
        /// Returns true if any cell in the array is non null.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static bool ContainsNonNullValues(this string[,] x)
        {
            for (int c = 0; c < x.GetLength(0); c++)
            {
                for (int r = 0; r < x.GetLength(1); r++)
                {
                    if (!String.IsNullOrWhiteSpace(x[c, r]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        /// <summary>
        /// Removes all the blank spaces from a string.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string RemoveSpaces(this string x)
        {
            return !String.IsNullOrWhiteSpace(x) ? x.Replace(" ", String.Empty) : null;
        }
    }
}
