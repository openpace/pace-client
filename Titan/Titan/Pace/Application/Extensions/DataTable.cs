﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class DataTableExtension
    {

        public static void UpdateColumnValue(this DataTable dt, string columnName, object oldValue, object newValue)
        {
            DataRow[] rows = dt.Select(columnName + "= " + oldValue);
            foreach (DataRow row in rows)
            {
                row[columnName] = newValue;
            }
            if (dt.GetChanges() != null)
            {
                dt.AcceptChanges();
            }
        }

        /// <summary>
        /// Gets the ordinal (positon) of a data column in a data table.
        /// </summary>
        /// <param name="dt">Data Table to search.</param>
        /// <param name="columnName">Name of the column to find.</param>
        /// <returns>The ordianl of the column, or -1 if the column is not found.</returns>
        public static int GetColumnOrdinal(this DataTable dt, string columnName)
        {
            if (String.IsNullOrEmpty(columnName)) return -1;

            foreach (DataColumn dc in dt.Columns.Cast<DataColumn>().Where(dc => dc.ColumnName.Equals(columnName)))
            {
                return dc.Ordinal;
            }
            return -1;
        }

        /// <summary>
        /// Moves a datacolumhn to a new position.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnName"></param>
        /// <param name="newPosition"></param>
        public static void MoveDataColumn(this DataTable dt, string columnName, int newPosition)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            DataColumn column = dt.GetDataColumn(columnName);
            if (column != null)
            {
                column.SetOrdinal(newPosition);
            }

            stopwatch.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("MoveDataColumn Runtime {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

        /// <summary>
        /// Change the captions/column names in a data table.
        /// </summary>
        /// <param name="dt">Data table to modify.</param>
        /// <param name="newCaptions">List of new captions.</param>
        /// <param name="changeColumnNames">True to also change the column names, false to ignore.</param>
        public static void ChangeCaptions(this DataTable dt, List<string> newCaptions, bool changeColumnNames)
        {
            if(newCaptions.Count != dt.Columns.Count)
            {
                throw new ArgumentException("Invlid number of new captions.");
            }

            int i = 0;
            foreach(DataColumn dc in dt.Columns )
            {
                dc.Caption = newCaptions[i];
                if(changeColumnNames) dc.ColumnName = dc.Caption;
                i++;
            }
        }



        /// <summary>
        /// Adds a calcualted column to the data table.
        /// </summary>
        /// <param name="x">DataTable</param>
        /// <param name="columnName"></param>
        /// <param name="columnType"></param>
        /// <param name="expression"></param>
        /// <param name="columnCaption"></param>
        /// <param name="displayFormat"></param>
        public static void AddCalculatedExpression(this DataTable x, string columnName, Type columnType, string expression, string columnCaption , string displayFormat)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            x.AddColumn(columnName, columnType, expression, columnCaption, displayFormat);

            x.AcceptChanges();

            stopwatch.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Add Calculated Expression Column: {0}, Runtime {1} (ms)", new[] { columnName, stopwatch.ElapsedMilliseconds.ToString("N0") });
        }




        /// <summary>
        /// Creates a new table 
        /// </summary>
        /// <param name="x">Table to filter.</param>
        /// <param name="fieldName">Field (column) to get the distinct values.</param>
        /// <returns></returns>
        public static List<string> FilterToDistinctList(this DataTable x, string fieldName)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var test = (from d in x.AsEnumerable()
                        select new
                        {
                            Name = d.Field<object>(fieldName)
                        }).Distinct().ToList();

            List<string> items = test.Select(id => id.Name).OfType<string>().ToList(); ;

            items.Sort();

            sw.Stop();

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("FilterToDistinctList runtime: {0}", new[] { sw.ElapsedMilliseconds.ToString("N0").ToString() });

            return items;
        }

        /// <summary>
        /// Removes duplicate rows from a data table (looks at all columns).
        /// </summary>
        /// <param name="x">DataTable to remove duplicates.</param>
        /// <param name="columnsToIgnore">List of column names to ignore (these could be key columns, etc)</param>
        /// <returns>A data table with the duplicates removed.</returns>
        public static DataTable RemoveDuplicateRows(this DataTable x, List<string> columnsToIgnore)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (columnsToIgnore == null) columnsToIgnore = new List<string>();

            HashSet<string> hSet = new HashSet<string>();
            List<int> duplicateList = new List<int>();

            int i = 0;
            foreach (DataRow drow in x.Rows)
            {
                if (drow.RowState == DataRowState.Deleted || drow.RowState == DataRowState.Detached) continue;
                StringBuilder sb = new StringBuilder();
                foreach (DataColumn dc in x.Columns.Cast<DataColumn>().Where(dc => !columnsToIgnore.Contains(dc.ColumnName)))
                {
                    sb.Append(Convert.ToString(drow[dc.ColumnName]));
                }
                if (hSet.Contains(sb.ToString()))
                {
                    duplicateList.Add(i);
                }
                else
                {
                    hSet.Add(sb.ToString());
                }
                i++;
            }

            duplicateList.Reverse();

            foreach (int r in duplicateList)
            {
                x.Rows.RemoveAt(r);
            }

            if (x.GetChanges() != null)
            {
                x.AcceptChanges();
            }

            sw.Stop();

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("RemoveDuplicateRows runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));

            return x;
        }

        /// <summary>
        /// Finds an Intersection in a DataTable
        /// </summary>
        /// <param name="x">DataTable to search.</param>
        /// <param name="intersection">Intersection to find.</param>
        /// <returns>True if the DataTable contains the intersection.</returns>
        public static bool ContainsIntersection(this DataTable x, Intersection intersection)
        {
            Stopwatch sw = Stopwatch.StartNew();

            List<string> intersectionCols = intersection.AxisSequence.ToList();

            foreach (DataRow drow in x.Rows)
            {
                if (drow.RowState == DataRowState.Deleted || drow.RowState == DataRowState.Detached) continue;
                StringBuilder sb = new StringBuilder();
                Intersection temp = new Intersection(intersection.AxisSequence);
                foreach (DataColumn dc in x.Columns.Cast<DataColumn>().Where(dc => intersectionCols.Contains(dc.ColumnName)))
                {
                    temp.SetCoordinate(dc.ColumnName, Convert.ToString(drow[dc.ColumnName]));
                    sb.Append(Convert.ToString(drow[dc.ColumnName]));
                }
                if (temp.Equals(intersection))
                {
                    sw.Stop();
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("ContainsIntersection runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));
                    return true;
                }
            }

            sw.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("ContainsIntersection runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));

            return false;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static DataTable RemoveDuplicates(this DataTable dt)
        {
            // Find the unique contacts in the table.
            IEnumerable<DataRow> query = dt.AsEnumerable().Distinct(DataRowComparer.Default);

            DataSet ds = new DataSet();
            ds.Tables.Add(query.CopyToDataTable());

            return ds.Tables[0];
        }


        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn to place where clause on.</param>
        /// <param name="value">Value to contains (Contains)</param>
        /// <returns>List of DataRow</returns>
        public static List<DataRow> SelectContains(this DataTable x, string columnName, string value)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var lst =  (from myRow in x.AsEnumerable()
                           where myRow.Field<string>(columnName).Contains(value)
                            select myRow).ToList();

            sw.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("SelectContains runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));

            return lst;
        }
         


        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="orderBy">Column to orderby</param>
        /// <param name="ascending">Sort Assending (false to sort desending)</param>
        /// <param name="numRowToReturn">Filter the resulting DataTable</param>
        /// <returns>DataTable</returns>
        public static DataTable OrderBy(this DataTable x, string orderBy, bool ascending, int numRowToReturn)
        {
            Stopwatch sw = Stopwatch.StartNew();

            DataColumn dataColumn = x.GetDataColumn(orderBy);
            object d = new double();
            object s = String.Empty;
            int i = new int();
            object objectToPass = null;
            if (dataColumn.DataType == typeof(string))
            {
                objectToPass = s;
            }
            else if (dataColumn.DataType == typeof(double))
            {
                objectToPass = d;
            }
            else if (dataColumn.DataType == typeof(Int32))
            {
                objectToPass = i;
            }

            DataTable dt = OrderBy(x, objectToPass, orderBy, ascending, numRowToReturn);

            sw.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("OrderBy Runtime {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));

            return dt;
        }

        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        ///<typeparam name="T">DataType of the DataColumn.</typeparam>
        /// <param name="value">Value type</param>
        /// <param name="orderBy">Column to orderby</param>
        /// <param name="ascending">Sort Assending (false to sort desending)</param>
        /// <param name="numRowToReturn">Filter the resulting DataTable</param>
        /// <returns>DataTable</returns>
        public static DataTable OrderBy<T>(this DataTable x, T value, string orderBy, bool ascending, int numRowToReturn)
        {
            OrderedEnumerableRowCollection<DataRow> query = null;
            if (ascending)
            {
                query = from c in x.AsEnumerable()
                        orderby c.Field<T>(orderBy)
                        select c;
            }
            else
            {
                query = from c in x.AsEnumerable()
                        orderby c.Field<T>(orderBy) descending
                        select c;
            }


            List<DataRow> rows = numRowToReturn > -1 ? query.Take(numRowToReturn).ToList() : query.ToList();

            DataTable dt =  rows.CopyToDataTable();
            return dt;
        }

        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <typeparam name="T">DataType of the DataColumn.</typeparam>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn to place where clause on.</param>
        /// <param name="value">Value must equal (where)</param>
        /// <returns>List of DataRow</returns>
        public static List<DataRow> SelectWhere<T>(this DataTable x, string columnName, T value)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var lst =  (from myRow in x.AsEnumerable()
                    where myRow.Field<T>(columnName).Equals(value)
                    select myRow).ToList();

            sw.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("SelectWhere<T> runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));

            return lst;
        }

        

        /// <summary>
        /// Gets the data type of a DataColumn
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn</param>
        /// <returns>The data type.</returns>
        public static Type GetDataColumnType(this DataTable x, string columnName)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (String.IsNullOrEmpty(columnName)) throw new ArgumentException("Column name cannot be null or blank.");

            return !x.Columns.Contains(columnName) ? null : x.Columns[columnName].DataType;
        }

        /// <summary>
        /// Gets the DataColumn name
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnOrdinal">Ordinal (position) of the DataColumn</param>
        /// <returns>The data column.</returns>
        public static string GetDataColumnName(this DataTable x, int columnOrdinal)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (columnOrdinal == -1) throw new ArgumentException("Invalid Column Ordinal.");

            return x.Columns[columnOrdinal].ColumnName;
        }

        /// <summary>
        /// Gets the list of DataColumn names
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="stopperColumn">Column to stop at.</param>
        /// <returns>The data columns.</returns>
        public static List<string> GetDataColumnNames(this DataTable x, string stopperColumn)
        {
            if(stopperColumn == null)
            {
                stopperColumn = String.Empty;
            }

            return x.Columns.Cast<DataColumn>().TakeWhile(c => !c.ColumnName.Equals(stopperColumn)).Select(c => c.ColumnName).ToList();
        }


        /// <summary>
        /// Gets the DataColumn
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn</param>
        /// <returns>The data column.</returns>
        public static DataColumn GetDataColumn(this DataTable x, string columnName)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (String.IsNullOrEmpty(columnName)) throw new ArgumentException("Column name cannot be null or blank.");

            return !x.Columns.Contains(columnName) ? null : x.Columns[columnName];
        }

        /// <summary>
        /// Adds a DataColumn to a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the column to add.</param>
        /// <param name="columnType">Data type of the column.</param>
        /// <param name="expression">Optional.  Expression for the DataColumn.</param>
        /// <param name="columnCaption">Caption of the column</param>
        /// <param name="displayFormat">Numeric format of the column.</param>
        /// <returns>The DataColumn.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static DataColumn AddColumn(this DataTable x, string columnName, Type columnType, string expression , string columnCaption, string displayFormat)
        {
            Stopwatch sw = Stopwatch.StartNew();

            DataColumn dc = null;
            if (x == null)
            {
                throw new ArgumentException("DataTable argument is null.");
            }

            if (!x.Columns.Contains(columnName))
            {
                //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("AddColumn.ColumnNotFound");
                //Stopwatch sw2 = Stopwatch.StartNew();
                if (String.IsNullOrEmpty(expression))
                {
                    dc = x.Columns.Add(columnName, columnType);
                }
                else
                {
                    dc = x.Columns.Add(columnName, columnType, expression);
                }
                //sw2.Stop();
                //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("AddColumn.Columns.Add: {0} (ms)", new[] { sw2.ElapsedMilliseconds.ToString() });
            }
            else
            {
                dc = x.Columns[columnName];
                if(!dc.Expression.Equals(expression)) dc.Expression = expression;
            }

            if(!String.IsNullOrEmpty(columnCaption) && dc != null)
            {
                dc.Caption = columnCaption;
            }

            if (displayFormat != null)
            {
                if (dc.ExtendedProperties.ContainsKey("DisplayFormat"))
                {
                    dc.ExtendedProperties.Remove("DisplayFormat");
                }
                dc.ExtendedProperties.Add("DisplayFormat", displayFormat);
            }

            sw.Stop();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("AddColumn runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));

            return x.Columns[columnName];
         
        }
        /// <summary>
        /// Custom CopyToDataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="table"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static DataTable CopyToDataTableEx<T>(this IEnumerable<T> source, DataTable table, LoadOption? options )
        {
            Stopwatch sw = Stopwatch.StartNew();

            var ret = new ObjectShredder<T>().Shred(source, table, options);

            sw.Stop();

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("CopyToDataTableEx runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));

            return ret;
        }


        /// <summary>
        /// This method will convert the supplied DataTable 
        /// to XML string.
        /// </summary>
        /// <param name="x">DataTable to be converted.</param>
        /// <returns>XML string format of the DataTable.</returns>
        public  static string ToXml(this DataTable x)
        {
            Stopwatch sw = Stopwatch.StartNew();

            DataSet dsData = new DataSet();
            try
            {
                StringBuilder sbSql = new StringBuilder();
                StringWriter swSql = new StringWriter(sbSql);
                dsData.Merge(x, true, MissingSchemaAction.AddWithKey);
                dsData.Tables[0].TableName = "SampleDataTable";
                foreach (DataColumn col in dsData.Tables[0].Columns)
                {
                    col.ColumnMapping = MappingType.Attribute;
                }
                dsData.WriteXml(swSql, XmlWriteMode.WriteSchema);

                sw.Stop();

                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("ToXml runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));

                return sbSql.ToString();
            }
            catch (Exception sysException)
            {
                throw sysException;
            }
        }

        /// <summary>
        /// Loads a DataTable from a serialized XML string.
        /// </summary>
        /// <param name="tableXml"></param>
        public static DataTable FromXml(string tableXml)
        {
            Stopwatch sw = Stopwatch.StartNew();

            StringReader reader = new StringReader(tableXml);
            DataTable dt = new DataTable();

            dt.ReadXmlSchema(reader);
            dt.ReadXml(reader);
            DataSet ds = new DataSet();
            ds.ReadXml(reader);


            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("FromXml runtime: {0}", sw.ElapsedMilliseconds.ToString("N0"));
            return dt;
        }

        /// <summary>
        /// Converts a DataTable to a StringBuilder.
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static StringBuilder ToStringBuilder(this DataTable dt)
        {
            Stopwatch sw = Stopwatch.StartNew();
            PafApp.GetLogger().InfoFormat("{0} Table.", dt.TableName);
            StringBuilder master  = new StringBuilder(5000);
            StringBuilder s = new StringBuilder("\t");
            for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            {
                s.Append(dt.Columns[curCol].ColumnName.Trim()).Append("\t");

            }
            master.AppendLine(s.ToString());
            s = new StringBuilder("\t");
            for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    string sTemp = dt.Rows[curRow][curCol].ToString().Trim();
                    if (sTemp.Contains("\r\n"))
                    {
                        sTemp = sTemp.Replace("\r\n", " ");
                    }
                    s.Append(sTemp).Append("\t");
                }
                master.AppendLine(s.ToString());
                s = new StringBuilder("\t");
            }
            sw.Stop();
            PafApp.GetLogger().InfoFormat("DataTable ToString runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
            return master;
        }

        /// <summary>
        /// Prints a datatable to the log4net file (Note this is an expensive operation).
        /// </summary>
        /// <param name="dt"></param>
        public static void Print(this DataTable dt)
        {
            Stopwatch sw = Stopwatch.StartNew();
            if (PafApp.GetLogger().IsDebugEnabled)
            {
                PafApp.GetLogger().DebugFormat("{0} Table.", dt.TableName);
            }
            else
            {
                return;
            }

            PafApp.GetLogger().Debug(dt.ToStringBuilder().ToString());

            //StringBuilder s = new StringBuilder("\t");
            //for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            //{
            //    s.Append(dt.Columns[curCol].ColumnName.Trim()).Append("\t");

            //}
            //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug(s.ToString());
            //s = new StringBuilder("\t");
            //for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
            //{
            //    for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            //    {
            //        string sTemp = dt.Rows[curRow][curCol].ToString().Trim();
            //        if(sTemp.Contains("\r\n"))
            //        {
            //            sTemp = sTemp.Replace("\r\n", " ");
            //        }
            //        s.Append(sTemp).Append("\t");
            //    }
            //    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug(s.ToString());
            //    s = new StringBuilder("\t");
            //}
            sw.Stop();

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("DataTable Print runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Prints a datatable to the console window (Note this is an expensive operation).
        /// </summary>
        /// <param name="dt"></param>
        public static void PrintToConsole(this DataTable dt)
        {
            Stopwatch sw = Stopwatch.StartNew();
            Console.WriteLine("{0} Table.", dt.TableName);
            StringBuilder s = new StringBuilder("\t");
            for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            {
                s.Append(dt.Columns[curCol].ColumnName.Trim()).Append("\t");

            }
            Console.Write(s.ToString());
            s = new StringBuilder("\t");
            for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    string sTemp = dt.Rows[curRow][curCol].ToString().Trim();
                    if (sTemp.Contains("\r\n"))
                    {
                        sTemp = sTemp.Replace("\r\n", " ");
                    }
                    s.Append(sTemp).Append("\t");
                }
                Console.Write(s.ToString());
                s = new StringBuilder("\t");
            }
            sw.Stop();

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("DataTable PrintToConsole runtime: {0} (ms)", sw.ElapsedMilliseconds.ToString("N0"));
        }

       

        /// <summary>
        /// Saves a dataset to an html file.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="pathFileName">Full path file name to save the file.</param>
        public static void ToHtml(this DataTable dataTable, string pathFileName)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.Append("<head>");
            sb.Append("<title>");
            sb.Append("Page-");
            sb.Append(Guid.NewGuid().ToString());
            sb.Append("</title>");
            sb.Append("</head>");
            sb.Append("<body>");


            sb.Append(dataTable.ToHtml());

            sb.Append("&nbsp;");
            sb.Append("&nbsp;");


            sb.Append("</body>");
            sb.Append("</html>");


            sb.ToFile(pathFileName);

            stopwatch.Stop();

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("DataTable ToHtml runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

        public static string ToHtml(this DataTable dataTable)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.AppendFormat(@"<H2>");
            sb.AppendFormat(dataTable.TableName);
            sb.AppendFormat(@"</H2>");

            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");

            sb.Append("<TR ALIGN='CENTER'>");

            //first append the column names.
            foreach (DataColumn column in dataTable.Columns)
            {
                sb.Append("<TH>");
                sb.Append(column.ColumnName);
                sb.Append("</TH>");
            }

            sb.Append("</TR>");

            // next, the column values.
            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append("<TR ALIGN='CENTER'>");

                foreach (DataColumn column in dataTable.Columns)
                {
                    sb.Append("<TD>");
                    if (row[column].ToString().Trim().Length > 0)
                    {
                        sb.Append(row[column]);
                    }
                    else
                    {
                        sb.Append("&nbsp;");
                    }
                    sb.Append("</TD>");
                }

                sb.Append("</TR>");
            }
            sb.Append("</TABLE>");


            stopwatch.Stop();

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("DataTable ToHtml runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString("N0") });

            return sb.ToString();
        }
    }
}
