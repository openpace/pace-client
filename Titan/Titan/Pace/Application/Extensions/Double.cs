﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.Application.Extensions
{
    /// <summary>
    /// Extension methods for a 
    /// </summary>
    internal static class DoubleExtension
    {
        /// <summary>
        /// Gets the zero status value of a double.
        /// </summary>
        /// <param name="x">Extended double value.</param>
        /// <returns>true if the value is zero, false if not.</returns>
        public static bool IsZero(this double x)
        {
            return x.Equals(0.0);
        }

        /// <summary>
        /// Rounds a double to a specified number of significant digits.
        /// </summary>
        /// <param name="x">Double to round.</param>
        /// <param name="digits">Number of significant digits.</param>
        /// <returns></returns>
        public static double Round(this double x, int digits)
        {
            return Math.Round(x, digits);
        }
    }
}
