﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Pace.Base.Data;
using Titan.Pace.DataStructures;
using Titan.PafService;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Application.Extensions
{
    internal static class CoordinateSetExtension
    {
        public static List<Intersection> ToIntersections(this List<CoordinateSet> x)
        {
            return (from cs in x from c in cs.GetCoordinates() select new Intersection(cs.Dimensions, c.Coordinates)).ToList();
        }

        /// <summary>
        /// Converts an array of CoordinateSet into a simpleCoordList.
        /// </summary>
        /// <param name="x">The array of intersections to convert.</param>
        /// <returns>A simpleCoordList</returns>
        public static simpleCoordList ToSimpleCoordList(this CoordinateSet x)
        {
            try
            {
                //Cord list to hold the changed cells.
                simpleCoordList simpleCoordList = new simpleCoordList();
                //List of coordinates.
                List<string> coordinates = new List<string>();

                foreach (Coordinate t in x.GetCoordinates())
                {
                    coordinates.AddRange(t.Coordinates);
                }

                simpleCoordList.axis = x.Dimensions ?? new string[0];

                simpleCoordList.coordinates = coordinates.ToArray();

                return simpleCoordList;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Converts an array of CoordinateSet into a simpleCoordList.
        /// </summary>
        /// <param name="x">The array of CoordinateSet to convert.</param>
        /// <returns>A simpleCoordList</returns>
        public static List<simpleCoordList> ToSimpleCoordListCollection(this IEnumerable<CoordinateSet> x)
        {
            List<simpleCoordList> result = new List<simpleCoordList>();
            try
            {
                result.AddRange(x.Select(inter => new PafSimpleCoordList(inter.ToSimpleCoordList(), true).GetSimpleCoordList));

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Creates a CoordinateSet from two different CoordinatesSet that have the same Dimensionality.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static void Combine(this CoordinateSet x, CoordinateSet other)
        {
            if (!x.Dimensions.ArrayEqual(other.Dimensions)) return;
            x.AddCoordinates(other.GetCoordinates());
        }

        /// <summary>
        /// Creates a CoordinateSet from two different CoordinatesSet that have the same Dimensionality.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool CanCombine(this CoordinateSet x, CoordinateSet other)
        {
            return x.Dimensions.ArrayEqual(other.Dimensions);
        }
    }
}
