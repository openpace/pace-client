﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.PafService;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class IntersectionExtension
    {
        /// <summary>
        /// Converts an array of Intersections into a simpleCoordList.
        /// </summary>
        /// <param name="x">The intersection to convert.</param>
        /// <returns>A simpleCoordList</returns>
        public static simpleCoordList ToSimpleCoordList(this Intersection x)
        {
            return x != null ? ToSimpleCoordList(new[] { x }, false) : null;
        }

        /// <summary>
        /// Converts an array of Intersections into a simpleCoordList.
        /// </summary>
        /// <param name="x">The array of intersections to convert.</param>
        /// <param name="compress">True to compress the simple coord list.</param>
        /// <returns>A simpleCoordList</returns>
        public static simpleCoordList ToSimpleCoordList(this IEnumerable<Intersection> x, bool compress)
        {
            try
            {
                //Cord list to hold the changed cells.
                simpleCoordList simpleCoordList = new simpleCoordList();
                //List of coordinates.
                List<string> coordinates = new List<string>();

                Intersection ix = null;
                foreach (Intersection t in x)
                {
                    ix = t;
                    for (int j = 0; j < ix.AxisSequence.Length; j++)
                    {
                        coordinates.Add(ix.Coordinates[j]);
                    }
                }

                simpleCoordList.axis = ix != null ? ix.AxisSequence : new string[0];

                simpleCoordList.coordinates = coordinates.ToArray();

                if (!compress)
                {
                    return simpleCoordList;
                }
                else
                {
                    return new PafSimpleCoordList(simpleCoordList, compress).GetSimpleCoordList;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Converts an array of Intersections into a simpleCoordList.
        /// </summary>
        /// <param name="x">The array of intersections to convert.</param>
        /// <param name="compress">True to compress the simple coord list.</param>
        /// <returns>A simpleCoordList</returns>
        public static List<simpleCoordList> ToSimpleCoordListCollection(this IEnumerable<Intersection> x, bool compress)
        {
            List<simpleCoordList> result = new List<simpleCoordList>();
            try
            {
                result.AddRange(x.Select(inter => new PafSimpleCoordList(ToSimpleCoordList(inter), compress).GetSimpleCoordList));

                return result;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<Intersection> ExpandToFloor(this List<Intersection> x, string dimensionName, SimpleDimTrees dimTrees)
        {
            List<Intersection> results = new List<Intersection>();
            foreach (Intersection intersection in x)
            {
                results.AddRange(intersection.ExpandToFloor(dimensionName, dimTrees));
            }
            return results;
        }

        public static List<Intersection> ExpandToFloor(this Intersection x, string dimensionName, SimpleDimTrees dimTrees)
        {
            List<Intersection> results = new List<Intersection>();

            SimpleDimTrees.MembersRow row = dimTrees.GetRoot(dimensionName);
            if (x.GetCoordinate(dimensionName).EqualsIgnoreCase(row.MemberName))
            {
                List<string> members = dimTrees.GetFloorMembersName(dimensionName, row.MemberName);
                foreach (string member in members)
                {
                    Intersection temp = x.DeepClone();
                    if (member != temp.GetCoordinate(dimensionName))
                    {
                        
                        temp.SetCoordinate(dimensionName, member);
                        results.Add(temp);
                    }
                    results.Add(temp);
                }
            }
            else
            {
                results.Add(x);
            }
            return results;
        }

        public static List<Intersection> ExpandToFloor(this Intersection x, string dimensionName, string memberName, SimpleDimTrees dimTrees)
        {
            List<Intersection> results = new List<Intersection>();

            results.Add(x.DeepClone());

            List<string> members = dimTrees.GetFloorMembersName(dimensionName, memberName);
            foreach (string member in members)
            {
                if (member == memberName) continue;
                Intersection temp = x.DeepClone();
                temp.SetCoordinate(dimensionName, member);
                results.Add(temp);
            }


            return results;
        }

        /// <summary>
        /// Return true if the Intersection contains a member that's a blank.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static bool ContainsPafBlank(this Intersection x)
        {
            return x != null && x.ContainsCoordinate(PafAppConstants.PAF_BLANK);
        }
    }
}
