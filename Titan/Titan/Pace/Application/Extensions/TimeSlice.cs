﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Application.Extensions
{
    internal static class TimeSliceExtension
    {
        /// <summary>
        /// Update the cell intersection using the specified time horizon coordinate
        /// </summary>
        /// <param name="cellIs"></param>
        /// <param name="timeHorizonCoord"></param>
        /// <param name="timeDim"></param>
        /// <param name="yearDim"></param>
        public static void ApplyTimeHorizonCoord(this Intersection cellIs, string timeHorizonCoord, string timeDim, string yearDim)
        {

            // Look for period/year delimiter. Throw an error if the delimiter is not found
            // or if it is the last character.
            int pos = FindTimeHorizonDelim(timeHorizonCoord);

            // Split time horizon coordinate into period and year
            cellIs.SetCoordinate(timeDim, timeHorizonCoord.Substring(pos + PafAppConstants.TIME_HORIZON_MBR_DELIM_LEN));
            cellIs.SetCoordinate(yearDim, timeHorizonCoord.Substring(0, pos));
        }



        /// <summary>
        /// Translate the time/year intersection into a time/horizon intersection
        /// </summary>
        /// <param name="cellIs"></param>
        /// <param name="timeDim"></param>
        /// <param name="yearDim"></param>
        /// <returns></returns>
        public static string TranslateTimeYearIs(this Intersection cellIs, string timeDim, string yearDim)
        {
            string yearCoord = cellIs.GetCoordinate(yearDim);
            string timeHorizonCoord = String.Empty;

            // Ensure that this is a time/year coordinate before doing translation
            if (!yearCoord.Equals(PafAppConstants.TIME_HORIZON_DEFAULT_YEAR))
            {
                timeHorizonCoord = BuildTimeHorizonCoord(cellIs.GetCoordinate(timeDim), yearCoord);
            }

            return timeHorizonCoord;
        }

        /// <summary>
        /// Generate a time horizon coordinate from the supplied period and year
        /// </summary>
        /// <param name="period"></param>
        /// <param name="year"></param>
        /// <returns></returns>
        static public string BuildTimeHorizonCoord(string period, string year)
        {
            return year + PafAppConstants.TIME_HORIZON_MBR_DELIM + period;
        }


        /// <summary>
        /// Return the position of the time horizon delimiter in a time horizon coordinate
        /// </summary>
        /// <param name="timeHorizonCoord"></param>
        /// <returns></returns>
        internal static int FindTimeHorizonDelim(string timeHorizonCoord)
        {

            // Look for period/year delimiter. Throw an error if the delimiter is not found
            // or if it is the last character.
            int pos = timeHorizonCoord.IndexOf(PafAppConstants.TIME_HORIZON_MBR_DELIM, StringComparison.Ordinal);
            if (pos == -1 || pos > timeHorizonCoord.Length)
            {
                String errMsg = "Invalid Time Horizon coordinate [" + timeHorizonCoord + "] passed to TimeSlice object";
                throw new ArgumentException(errMsg);
            }

            return pos;
        }
    }
}
