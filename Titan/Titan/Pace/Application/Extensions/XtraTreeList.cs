﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.Nodes.Operations;

namespace Titan.Pace.Application.Extensions
{
    /// <remarks/>
    internal static class XtraTreeListExtension
    {
        /// <summary>
        /// Binds a data table to a tree list.
        /// </summary>
        /// <param name="treeList"></param>
        /// <param name="dataObject">Data object to bind (DataTable, etc)</param>
        /// <param name="keyFieldName">A string value representing the name of the field used as the unique record identifier.</param>
        /// <param name="parentFieldName">The name of the field used as an identifier of a parent record within a data source.</param>
        /// <param name="dimensionName">Only used for logging.</param>
        /// <param name="columnsToHide">List of columns to hide</param>
        /// <param name="enableFiltering">Enable filering of the TreeList</param>
        /// <param name="expandRootOnly">True to expand the root level node,  If false the tree will be set to Collapsed</param>
        public static void BindData(this TreeList treeList, object dataObject, string keyFieldName, string parentFieldName, string dimensionName = "", List<string> columnsToHide = null, bool enableFiltering = true, bool expandRootOnly = true)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            treeList.OptionsBehavior.EnableFiltering = false;
            treeList.BeginUpdate();
            treeList.BeginUnboundLoad();


            treeList.DataSource = dataObject;
            treeList.KeyFieldName = keyFieldName;
            treeList.ParentFieldName = parentFieldName;
            if (columnsToHide != null && columnsToHide.Count > 0)
            {
                foreach (string column in columnsToHide.Where(column => treeList.Columns.ColumnByFieldName(column) != null))
                {
                    treeList.Columns.ColumnByFieldName(column).Visible = false;
                }
            }
            
            treeList.EndUnboundLoad();
            treeList.OptionsBehavior.EnableFiltering = enableFiltering;
            treeList.CollapseAll();
            if (expandRootOnly)
            {
                foreach (var node in treeList.RootNodes())
                {
                    node.Expand();
                }
            }
            treeList.EndUpdate();

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("BindData: {0}, dimension: {2}, runtime: {1} (ms)", new object[] { treeList.Name, stopwatch.ElapsedMilliseconds.ToString("N0"), dimensionName });
        }

        /// <summary>
        /// Gets the root node of the TreeList.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static TreeListNode RootNode(this TreeList x)
        {
            GetRootNode op = new GetRootNode();
            x.NodesIterator.DoOperation(op);
            return op.RootNode;
        }

        /// <summary>
        /// Gets the root nodes of the TreeList.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static List<TreeListNode> RootNodes(this TreeList x)
        {
            GetRootNodes op = new GetRootNodes();
            x.NodesIterator.DoOperation(op);
            return op.RootNodes;
        }

        /// <summary>
        /// Search and check for nodes.
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="fieldNames">List of field names to search</param>
        /// <param name="values">Value to find.</param>
        /// <param name="scrollToNode">Scroll the tree the the newly checked node.</param>
        /// <param name="caseInsensitiveSearch">Perform a case insensitive search of node</param>
        /// <param name="clearCurrentSelections">If true any checked nodes will be unchecked before the new node is checked.</param>
        public static void CheckNodes(this TreeList x, ICollection<string> values, ICollection<string> fieldNames, bool scrollToNode, bool caseInsensitiveSearch = true, bool clearCurrentSelections = true)
        {
            if (values == null || values.Count == 0) return;
            if (fieldNames == null || fieldNames.Count == 0) return;

            x.BeginUpdate();

            List<TreeListNode> nodesToCheck = new List<TreeListNode>(values.Count);
            foreach (string value in values)
            {
                foreach (string fieldName in fieldNames)
                {
                    //Perform the search for nodes
                    List<TreeListNode> nodes;
                    if (caseInsensitiveSearch)
                    {
                        FindNodeByFieldCaseInsensitive op = new FindNodeByFieldCaseInsensitive(fieldName, value);
                        x.NodesIterator.DoOperation(op);
                        nodes = op.FoundNodes;
                    }
                    else
                    {
                        nodes = x.FindNodeByFieldValue(fieldName, value).ToSingleList();
                    }
                    nodesToCheck.AddRange(nodes);
                    //if not null, then do the clear, select, scroll to
                    if (nodes == null || nodes.Count <= 0) continue;
                    if (PafApp.GetLogger().IsDebugEnabled)
                        PafApp.GetLogger().DebugFormat("Found Node(s): {0}", new object[] {value});
                }
            }

            CheckNodes(x, nodesToCheck, clearCurrentSelections, scrollToNode);

            x.EndUpdate();
        }

        /// <summary>
        /// Finds nodes by text string.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="fieldNames"></param>
        /// <param name="textToFind"></param>
        /// <param name="caseInsensitiveSearch"></param>
        /// <returns></returns>
        public static List<TreeListNode> FindNodeByText(this TreeList x, List<string> fieldNames, string textToFind, bool caseInsensitiveSearch = true)
        {
            foreach (string fieldName in fieldNames)
            {
                List<TreeListNode> nodes;
                if (caseInsensitiveSearch)
                {
                    FindNodeByFieldCaseInsensitive op = new FindNodeByFieldCaseInsensitive(fieldName, textToFind);
                    x.NodesIterator.DoOperation(op);
                    nodes = op.FoundNodes;
                }
                else
                {
                    nodes = x.FindNodeByFieldValue(fieldName, textToFind).ToSingleList();
                }
                if (nodes != null && nodes.Count > 0)
                {
                    return nodes;
                }
            }
            return null;
        }

        /// <summary>
        /// Search and check for nodes.
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="fieldNames">List of field names to search</param>
        /// <param name="value">Value to find.</param>
        /// <param name="scrollToNode">Scroll the tree the the newly checked node.</param>
        /// <param name="caseInsensitiveSearch">Perform a case insensitive search of node</param>
        /// <param name="clearCurrentSelections">If true any checked nodes will be unchecked before the new node is checked.</param>
        public static void CheckNode(this TreeList x, string value, List<string> fieldNames, bool scrollToNode, bool caseInsensitiveSearch = true, bool clearCurrentSelections = true)
        {
            if (String.IsNullOrEmpty(value)) return;
            if (fieldNames == null || fieldNames.Count == 0) return;

            CheckNodes(x, value.ToSingleList(), fieldNames,scrollToNode,caseInsensitiveSearch,clearCurrentSelections);
        }

        /// <summary>
        /// Search for and select (not checks) a tree node.
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="fieldNames">List of field names to search</param>
        /// <param name="value">Value to find.</param>
        /// <param name="scrollToNode">Scroll the tree the the newly checked node.</param>
        /// <param name="caseInsensitiveSearch">Perform a case insensitive search of node</param>
        public static void SelectTreeNode(this TreeList x, List<string> fieldNames, string value, bool scrollToNode, bool caseInsensitiveSearch)
        {
            if (String.IsNullOrEmpty(value)) return;
            if (fieldNames == null || fieldNames.Count == 0) return;

            x.BeginUpdate();

            foreach (string fieldName in fieldNames)
            {
                //Perform the search for nodes
                List<TreeListNode> nodes;
                if (caseInsensitiveSearch)
                {
                    FindNodeByFieldCaseInsensitive op = new FindNodeByFieldCaseInsensitive(fieldName, value);
                    x.NodesIterator.DoOperation(op);
                    nodes = op.FoundNodes;
                }
                else
                {
                    nodes = x.FindNodeByFieldValue(fieldName, value).ToSingleList();
                }
                //if not null, then do the clear, select, scroll to
                if (nodes != null && nodes.Count > 0)
                {
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Found Node(s): {0}, attempting selection.", new object[] { value });
                    x.MakeNodesVisible(nodes);
                    x.FocusedNode = nodes[0];
                    break;
                }
            }

            x.EndUpdate();
        }


        /// <summary>
        /// Checks a tree node.
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="treeNode">List of TreeListNode to check.</param>
        /// <param name="clearCurrentSelections">If true any checked nodes will be unchecked before the new node is checked.</param>
        /// <param name="scrollToNode">Scroll the tree the the newly checked node.</param>
        public static void CheckNode(this TreeList x, TreeListNode treeNode, bool clearCurrentSelections = true, bool scrollToNode = true)
        {
            CheckNodes(x, treeNode.ToSingleList(), clearCurrentSelections, scrollToNode);
        }


        /// <summary>
        /// Check a list of tree nodes.
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="treeNodes">List of TreeListNode to check.</param>
        /// <param name="clearCurrentSelections">If true any checked nodes will be unchecked before the new node is checked.</param>
        /// <param name="scrollToNode">Scroll the tree the the newly checked node.</param>
        public static void CheckNodes(this TreeList x, TreeListNodes treeNodes, bool clearCurrentSelections, bool scrollToNode)
        {
            List<TreeListNode> nodes = new List<TreeListNode>(treeNodes.Count);
            nodes.AddRange(treeNodes.Cast<TreeListNode>());
            CheckNodes(x, nodes,clearCurrentSelections,scrollToNode);
        }

        /// <summary>
        /// Check a list of tree nodes.
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="treeNodes">List of TreeListNode to check.</param>
        /// <param name="clearCurrentSelections">If true any checked nodes will be unchecked before the new node is checked.</param>
        /// <param name="scrollToNode">Scroll the tree the the newly checked node.</param>
        public static void CheckNodes(this TreeList x, IEnumerable<TreeListNode> treeNodes, bool clearCurrentSelections, bool scrollToNode)
        {
            //if not null, then do the clear, select, scroll to
            if (treeNodes != null)
            {
                //If requested, then clear the current selections.
                if (clearCurrentSelections)
                {
                    x.ClearTreeSelections();
                }
                foreach (TreeListNode node in treeNodes)
                {
                    node.Checked = true;
                    if (scrollToNode)
                    {
                        x.MakeNodeVisible(node);
                    }
                }
            }
        }

        /// <summary>
        /// Checks all the valid descendants of a specified node.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="nodes"></param>
        public static void CheckAllDescendants(this TreeList x, TreeListNodes nodes)
        {
            CheckAllTreeNodes op = new CheckAllTreeNodes(x);
            x.NodesIterator.DoLocalOperation(op, nodes);
        }

        /// <summary>
        /// Makes visible the <see cref="T:DevExpress.XtraTreeList.Nodes.TreeListNode"/> specified via the parameter.
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="nodes">List of nodes to make visible</param>
        public static void MakeNodesVisible(this TreeList x, List<TreeListNode> nodes)
        {
            if (nodes == null || nodes.Count == 0) return;

            foreach (TreeListNode node in nodes)
            {
                x.MakeNodeVisible(node);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x">XtraTreeList</param>
        /// <param name="fieldName">Field name to search</param>
        /// <param name="value">Value to find.</param>
        public static void SetFocusNode(this TreeList x, string fieldName, string value)
        {
            if (String.IsNullOrEmpty(value)) return;
            if (String.IsNullOrEmpty(fieldName)) return;

            TreeListNode node = x.FindNodeByFieldValue(fieldName, value);
            if (node != null)
            {
               x.FocusedNode = node;
            }
        }

        /// <summary>
        /// Clears all the selections (Checked) of the TreeList.
        /// </summary>
        /// <param name="x">TreeList</param>
        public static void ClearTreeSelections(this TreeList x)
        {
            if (x == null) return;

            foreach (TreeListNode nodex in x.GetCheckedNodes())
            {
                nodex.Checked = false;
            }
        }

        /// <summary>
        /// Gets the first checked Node
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static TreeListNode GetCheckedNode(this TreeList x)
        {
            var nodex = x.GetCheckedNodes();
            if (nodex == null || nodex.Count == 0)
            {
                return null;
            }
            return nodex[0];
        }

        /// <summary>
        /// Gets the first selected Node
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static TreeListNode GetSelectedNode(this TreeList x)
        {
            var nodex = x.Selection;
            if (nodex == null || nodex.Count == 0)
            {
                return null;
            }
            return nodex[0];
        }

        /// <summary>
        /// Gets all the checked nodes
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static List<TreeListNode> GetCheckedNodes(this TreeList x)
        {
            GetCheckedNodesOperation op = new GetCheckedNodesOperation();
            x.NodesIterator.DoOperation(op);

            return op.CheckedNodes;
        }

        /// <summary>
        /// Gets all the checked nodes as a list
        /// </summary>
        /// <param name="x"></param>
        /// <param name="descColumn">Column to return text.</param>
        /// <returns></returns>
        public static List<string> GetCheckedNodes(this TreeList x, string descColumn)
        {
            var nodes = x.GetCheckedNodes();
            if (nodes == null || nodes.Count == 0)
            {
                return null;
            }

            return nodes.Select(node => node.GetDisplayText(descColumn)).ToList();
        }

        /// <summary>
        /// Gets all the checked nodes for a specified node list.
        /// </summary>
        /// <param name="x">TreeList</param>
        /// <param name="nodes">List of nodes within tree to search.</param>
        /// <returns></returns>
        public static List<TreeListNode> GetCheckedNodes(this TreeList x, TreeListNodes nodes)
        {
            //Call the operation:
            GetCheckedNodesOperation op = new GetCheckedNodesOperation();
            x.NodesIterator.DoLocalOperation(op, nodes);

            return op.CheckedNodes;
        }

        /// <summary>
        /// Gets all the checked nodes as a list
        /// </summary>
        /// <param name="x">TreeList</param>
        /// <param name="nodes">List of nodes within tree to search.</param>
        /// <param name="descColumn">Column to return text.</param>
        /// <returns></returns>
        public static List<string> GetCheckedNodes(this TreeList x, TreeListNodes nodes, string descColumn)
        {
            var nodex = x.GetCheckedNodes(nodes);
            if (nodex == null || nodex.Count == 0)
            {
                return null;
            }
            return nodex.Select(node => node.GetDisplayText(descColumn)).ToList();
        }

        /// <summary>
        /// Gets the text of the first checked node.
        /// </summary>
        /// <returns></returns>
        public static object GetCheckedNodeText(this TreeList x, string columnTextToReturn)
        {
            TreeListNode temp = x.GetCheckedNode();
            return temp.GetText(columnTextToReturn);
        }

        /// <summary>
        /// Gets the text of the first selection.
        /// </summary>
        /// <returns></returns>
        public static object GetSelectedNodeText(this TreeList x, string columnTextToReturn)
        {
            if (x.Selection == null) return null;
            TreeListNode temp = x.Selection[0];
            return temp.GetText(columnTextToReturn);
        }



        /// <summary>
        /// Gets the text of the selections.
        /// </summary>
        /// <returns></returns>
        public static List<string> GetCheckedNodesText(this TreeList x, string columnTextToReturn)
        {
            List<TreeListNode> treeNodes = x.GetCheckedNodes();
            List<string> temp = new List<string>(treeNodes.Count);
            temp.AddRange(treeNodes.Select(node => node.GetText(columnTextToReturn)));
            return temp;
        }

        /// <summary>
        /// Gets count of checked nodex.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static int CheckedNodesCount(this TreeList x)
        {
            GetCheckedNodesOperation op = new GetCheckedNodesOperation();
            x.NodesIterator.DoOperation(op);

            return op.CheckedNodeCount;
        }

        /// <summary>
        /// Gets a list of nodes using a DFS traversal.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="checkedOnly">Only add checked nodes to the list.</param>
        /// <returns></returns>
        public static List<TreeListNode> DeptFirstNodes(this TreeList x, bool checkedOnly = false)
        {
            return DepthFirst(x, checkedOnly);
        }

        /// <summary>
        /// Gets a list of nodes using a BFS traversal.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="checkedOnly">Only add checked nodes to the list.</param>
        /// <returns></returns>
        public static List<TreeListNode> BreathFirstNodes(this TreeList x, bool checkedOnly = false)
        {
            return BreathFirst(x, checkedOnly);
        }

        /// <summary>
        /// Reverses the nodes in a TreeList.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="checkedOnly">Only add checked nodes to the list.</param>
        /// <returns></returns>
        private static List<TreeListNode> Reverse(this TreeList x, bool checkedOnly = false)
        {
            List<TreeListNode> dfs = new List<TreeListNode>();
            foreach (TreeListNode child in x.Nodes)
            {
                dfs = ReverseTraverseNode(dfs, child.Nodes, checkedOnly);
                if (!checkedOnly)
                {
                    dfs.Add(child);
                }
                else if (child.Checked)
                {
                    dfs.Add(child);
                }
            }
            return dfs;
        }


        private static List<TreeListNode> ReverseTraverseNode(List<TreeListNode> dfs, TreeListNodes nodes, bool checkedOnly) 
        {
            foreach (TreeListNode child in nodes)
            {
                dfs = ReverseTraverseNode(dfs, child.Nodes, checkedOnly);
                if (!checkedOnly)
                {
                    dfs.Add(child);
                }
                else if (child.Checked)
                {
                    dfs.Add(child);
                }
            }
            return dfs;
        }

        private static List<TreeListNode> DepthFirst(this TreeList x, bool checkedOnly = true)
        {
            GetTotalNodeCount op = new GetTotalNodeCount();
            x.NodesIterator.DoOperation(op);

            List<TreeListNode> nodes = new List<TreeListNode>(op.Count);
            Stack<TreeListNode> stack = new Stack<TreeListNode>(op.Count);

            // Bang all the top nodes into the queue.
            foreach (TreeListNode top in x.Nodes)
            {
                if (!checkedOnly)
                {
                    stack.Push(top);
                }
                else if (top.Checked)
                {
                    stack.Push(top);
                }
            }

            while (stack.Count > 0)
            {
                TreeListNode node = stack.Pop();
                if (node != null)
                {

                    // Add the node to the list of nodes.
                    if (!checkedOnly)
                    {
                        nodes.Add(node);
                    }
                    else if (node.Checked)
                    {
                        nodes.Add(node);
                    }

                    if (node.Nodes != null && node.Nodes.Count > 0)
                    {
                        // Enqueue the child nodes.
                        foreach (TreeListNode child in node.Nodes)
                        {
                            if (!checkedOnly)
                            {
                                stack.Push(child);
                            }
                            else if (child.Checked)
                            {
                                stack.Push(child);
                            }
                        }
                    }
                }
            }
            return nodes;
        }

        private static List<TreeListNode> BreathFirst(this TreeList x, bool checkedOnly = true)
        {
            GetTotalNodeCount op = new GetTotalNodeCount();
            x.NodesIterator.DoOperation(op);

            List<TreeListNode> nodes = new List<TreeListNode>(op.Count);
            Queue<TreeListNode> queue = new Queue<TreeListNode>(op.Count);

            // Bang all the top nodes into the queue.
            foreach (TreeListNode top in x.Nodes)
            {
                if (!checkedOnly)
                {
                    queue.Enqueue(top);
                }
                else if (top.Checked)
                {
                    queue.Enqueue(top);
                }
            }

            while (queue.Count > 0)
            {
                TreeListNode node = queue.Dequeue();
                if (node != null)
                {
                    // Add the node to the list of nodes.
                    if (!checkedOnly)
                    {
                        nodes.Add(node);
                    }
                    else if (node.Checked)
                    {
                        nodes.Add(node);
                    }

                    if (node.Nodes != null && node.Nodes.Count > 0)
                    {
                        // Enqueue the child nodes.
                        foreach (TreeListNode child in node.Nodes)
                        {
                            if (!checkedOnly)
                            {
                                queue.Enqueue(child);
                            }
                            else if (child.Checked)
                            {
                                queue.Enqueue(child);
                            }
                        }
                    }
                }
            }

            return nodes;
        }
    }

    /// <summary>
    /// The operation class that checks all nodes in a TreeListNodes nodes
    /// </summary>
    internal class CheckAllTreeNodes : TreeListOperation
    {
        public TreeList Parent { get; set; }
        public CheckAllTreeNodes(TreeList parent)
            : base()
        {
            Parent = parent;
        }
        public override void Execute(TreeListNode node)
        {
            if (node.CheckState == CheckState.Unchecked)
            {
                node.Checked = true;
            }
        }
    }

    /// <summary>
    /// The operation class that collects checked nodes
    /// </summary>
    internal class GetCheckedNodesOperation : TreeListOperation
    {
        public List<TreeListNode> CheckedNodes;
        public int CheckedNodeCount = 0;
        public GetCheckedNodesOperation() : base()
        {
            CheckedNodeCount = 0;
            CheckedNodes = new List<TreeListNode>(20);
        }
        public override void Execute(TreeListNode node)
        {
            if (node.CheckState != CheckState.Unchecked)
            {
                CheckedNodes.Add(node);
                CheckedNodeCount++;
            }
        }
    }


    /// <summary>
    /// The operation class that collects checked nodes
    /// </summary>
    internal class GetTotalNodeCount : TreeListOperation
    {
        public int Count = 0;
        public GetTotalNodeCount() : base() { }
        public override void Execute(TreeListNode node)
        {
            Count++;
        }
    }


    /// <summary>
    /// Gets the description of the nodes from the tree.
    /// </summary>
    internal class GetDisplayText : TreeListOperation
    {
        public List<string> NodeDisplayText;
        public string DisplayColumn;
        public GetDisplayText(string displayColumn)
            : base()
        {
            NodeDisplayText = new List<string>();
            DisplayColumn = displayColumn;
        }
        public override void Execute(TreeListNode node)
        {
            NodeDisplayText.Add(node.GetDisplayText(DisplayColumn));
        }
    }


    /// <summary>
    /// The operation class that collects checked nodes
    /// </summary>
    internal class GetRootNode : TreeListOperation
    {
        public TreeListNode RootNode;
        public GetRootNode() : base() { }
        public override void Execute(TreeListNode node)
        {
            if (node.Level != 0) return;
            RootNode = node;
        }
    }

    /// <summary>
    /// The operation class that collects checked nodes
    /// </summary>
    internal class GetRootNodes : TreeListOperation
    {
        public List<TreeListNode> RootNodes;
        public GetRootNodes() : base() { RootNodes = new List<TreeListNode>(); }
        public override void Execute(TreeListNode node)
        {
            if (node.Level != 0) return;
             RootNodes.Add(node);
        }
    }


    /// <summary>
    /// Returns a node by its field value.
    /// Note this search is case insensitive
    /// </summary>
    internal class FindNodeByFieldCaseInsensitive : TreeListOperation
    {
        private readonly string _fieldName;
        private readonly string _memberName;
        public List<TreeListNode> FoundNodes;

        public FindNodeByFieldCaseInsensitive(string fieldName, string memberName)
        {
            _fieldName = fieldName;
            _memberName = memberName;
            FoundNodes = new List<TreeListNode>();
        }
        public override void Execute(TreeListNode node) {
            string nodeValue = Convert.ToString(node[_fieldName]);
            if (nodeValue.EqualsIgnoreCase(_memberName))
            {
                FoundNodes.Add(node);
            }
        }
    }
}