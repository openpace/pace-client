﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Data;
using System.Linq;

namespace Titan.Pace.Application.Extensions
{
    internal static class DataSetExtension
    {
        /// <summary>
        /// Gets a DataTable from a dataset.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static DataTable GetTable(this DataSet x, string tableName)
        {
            if (x == null || x.Tables.Count == 0) return null;
            return x.Tables.Cast<DataTable>().FirstOrDefault(dt => dt.TableName.Equals(tableName));
        }

        /// <summary>
        /// Removes a DataTable from a dataset.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public static void RemoveTable(this DataSet x, string tableName)
        {
            if (x == null || x.Tables.Count == 0) return;

            DataTable dt = x.GetTable(tableName);

            x.Tables.Remove(dt);
        }
    }
}
