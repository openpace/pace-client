﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace Titan.Pace.Application.Events
{
    /// <summary>
    /// SavedSelectionsChangedEventArgs
    /// </summary>
    internal class SavedSelectionsChangedEventArgs : EventArgs
    {
        /// <summary>
        /// User selector ID
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// Is this a clear or saved.
        /// </summary>
        public SavedCleared SavedCleared { get; private set; }
        
        /// <summary>
        /// Clear all dimensions
        /// </summary>
        public bool AllDimensions { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public List<string> Selections { get; private set; }

        public SavedSelectionsChangedEventArgs(string id, SavedCleared savedCleared = SavedCleared.Cleared, List<string> selections = null,  bool allDimensions = true)
        {
            Id = id;
            SavedCleared = savedCleared;
            AllDimensions = allDimensions;
            Selections = selections;
        }
    }
}
