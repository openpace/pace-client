﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.Application.Events
{
    /// <summary>
    /// Event args for the <see cref="Events.ViewSectionGroupChangedHandler"/> 
    /// </summary>
    internal class ViewSectionGroupChangedEventArgs : EventArgs
    {
        public string ViewName { get; set; }
        public ViewAxis ViewAxis { get; set; }
        public bool GroupEnabled { get; set; }
        public string DimensionName { get; set; }

        public ViewSectionGroupChangedEventArgs(string viewName, ViewAxis viewAxis, bool groupEnabled, string dimensionName)
        {
            ViewName = viewName;
            ViewAxis = viewAxis;
            GroupEnabled = groupEnabled;
            DimensionName = dimensionName;
        }
    }
}
