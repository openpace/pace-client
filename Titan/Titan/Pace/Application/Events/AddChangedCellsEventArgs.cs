﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;
using Titan.Palladium.Rules;

namespace Titan.Pace.Application.Events
{
    /// <summary>
    /// Custom event argument for when a user locks a cell.
    /// </summary>
    internal class AddChangedCellsEventArgs : EventArgs
    {
        public AddChangedCellsEventArgs(string sheetName, string sheetHash, Change changeType,
           ContiguousRange cellRange, object[,] cellValueArray, ProtectionMngrListTracker listTracker)
        {
            SheetName = sheetName;
            SheetHash = sheetHash;
            ChangeType = changeType;
            Cells = cellRange;
            CellValueArray = cellValueArray;
            ListTracker = listTracker;
        }

        public string SheetName;
        public string SheetHash;
        public Change ChangeType;
        public ContiguousRange Cells;
        public object[,] CellValueArray;
        public ProtectionMngrListTracker ListTracker;
    }
}
