﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.Rules;

namespace Titan.Pace.Application.Events
{
    /// <summary>
    /// Custom event argument for when a user locks a cell.
    /// </summary>
    internal class UserLockedCellEventArgs : BaseLockedCellEventArgs
    {
        public UserLockedCellEventArgs(string sheetName, string sheetHash, Cell cell, ProtectionMngrListTracker listTracker)
            : base(sheetName, sheetHash, listTracker)
        {
            Cell = cell;
        }

        public UserLockedCellEventArgs(string sheetName, string sheetHash, Range range, ProtectionMngrListTracker listTracker)
            : base(sheetName, sheetHash, listTracker)
        {
            Range = range;
        }

        public Cell Cell { get; set; }
        public Range Range { get; set; }
    }
}
