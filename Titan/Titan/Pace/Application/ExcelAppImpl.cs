#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Microsoft.Win32;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Exceptions;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Forms;
using Titan.Pace.Application.Win32;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.Rules;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Properties;
using Range=Microsoft.Office.Interop.Excel.Range;
using SimpleTrees = Titan.Pace.ExcelGridView.SimpleTrees;

namespace Titan.Pace.Application
{
    /// <summary>
    /// Implementation of "AppInterface" class.
    /// </summary>
    /// <remarks></remarks>
    internal class ExcelAppImpl : IAppInterface
    {
        [DllImport("ESSEXCLN.XLL", EntryPoint = "EssVSetGlobalOption")]
        private extern static int EssVSetGlobalOption(int item, object globalOption);

        [DllImport("HsAddin.dll", EntryPoint = "HypSetGlobalOption")]
        private extern static int HypSetGlobalOption(int vtItem, object vtGlobalOption);

        #region Private Variables
        /// <summary>
        /// The local Excel applicaiton.
        /// </summary>
        private readonly Microsoft.Office.Interop.Excel.Application _App;

        /// <summary>
        /// List to hold the buttons that are added to the toolbar.
        /// </summary>
        private readonly List<CommandBarControl> _CmdBarControls;

        /// <summary>
        /// List to hold the buttons that are added to the menubar(CommandBarPopup).
        /// </summary>
        private readonly List<CommandBarControl> _MenuBarControls;

        /// <summary>
        /// List to hold the right click buttons, so they don't go out of scope.
        /// </summary>
        private readonly List<CommandBarControl> _RightClickBarControls;

        /// <summary>
        /// Holds reference to the new button.  Used to tell if Excel is in "Edit" mode.
        /// </summary>
        private readonly CommandBarControl _NewButton = null;

        /// <summary>
        /// The Excel toolbar.
        /// </summary>
        private CommandBar _ViewToolBar;

        /// <summary>
        /// Right click menu command bar.
        /// </summary>
        private readonly CommandBar _RightClickMenu;

        /// <summary>
        /// The Excel menubar.
        /// </summary>
        private CommandBarPopup _MenuBar;

        /// <summary>
        /// Logon form control.
        /// </summary>
        private frmLogon _Logon;

        /// <summary>
        /// Role selection control.
        /// </summary>
        private readonly frmRoleSelection _Role;

        /// <summary>
        /// Role Filter control.
        /// </summary>
        private readonly frmRoleFilter _Filter;

        /// <summary>
        /// Actions pane control.
        /// </summary>
        private readonly ExcelActionsPane _Pane;

        private object _EssbaseSecondaryButtonSetting;

        private object _EssbaseDoubleClicking;

        private int? _HSVDoubleClickingButtonSetting;

        /// <summary>
        /// color utitlities
        /// </summary>
        private readonly ColorUtilities cu;

        #endregion Private Variables

        #region Constructor

        /// <summary>
        /// Constructor
        /// </summary>
        public ExcelAppImpl(Microsoft.Office.Interop.Excel.Application app)
        {
            _App = app;
            //_App = Globals.ThisWorkbook.Application;
            //Command bar variables
            _CmdBarControls = new List<CommandBarControl>();
            _MenuBarControls = new List<CommandBarControl>();
            _RightClickBarControls = new List<CommandBarControl>(20);
            _ViewToolBar = null;
            _MenuBar = null;
            //_ReplicationRightClickPopup = null;
            _CommandBars commandBars = _App.CommandBars;
            _RightClickMenu = commandBars[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.CommandBars.Cell")];

            _Pane = new ExcelActionsPane();

            _NewButton = GetButton(
                _App,
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.CommandBars.WorksheetMenuBar"), 
                23,
                false,
                true);

            //Control varaiables
            _Logon = new frmLogon();
            _Logon.UserAuthenicated += new frmLogon.RecievedUserAuthenication(PafApp.GetEventManager().userLogon_UserAuthenicated);

            _Role = new frmRoleSelection();

            _Filter = new frmRoleFilter();
            _Filter.roleFilter.FilterChanged += new RoleFilter.RoleFilter_RoleFilter(PafApp.GetEventManager().LogonEvent_InfoChanged);

            // TTN-2656
            _Role.RoleSelector.FilteredSubtotalsOrSupInvIxChanged +=
                new RoleSelection.RoleSelection_FilteredSubtotalsOrSupInvIxChanged(PafApp.GetEventManager().LogonEvent_InfoChanged);

            _Role.RoleSelector.RoleChanged +=
                new RoleSelection.RoleSelection_RoleChanged(PafApp.GetEventManager().LogonEvent_InfoChanged);

            _Role.RoleSelector.SeasonProcessChanged +=
                new RoleSelection.RoleSelection_SeasonProcessChanged(PafApp.GetEventManager().LogonEvent_InfoChanged);

            _Logon.LogonInfoChanged +=
                new frmLogon.UserLogon_ServerChanged(PafApp.GetEventManager().LogonEvent_InfoChanged);
            //_SessionLockGrid = new frmSessionLocks();
            cu = new ColorUtilities();
        }



        /// <summary>
        /// Constructor
        /// </summary>
        public ExcelAppImpl()
            : this(Globals.ThisWorkbook.Application)
        {
        }

        #endregion Constructor

        #region Public Methods
        /// <summary>
        /// Gets the SimpleTrees from the ActionsPane.
        /// </summary>
        /// <returns>A SimpleTrees structure that contains a list a PafSimpleTrees</returns>
        [DebuggerHidden]
        [Obsolete("Replaced by SimpleDimTrees", true)]
        public SimpleTrees GetSimpleTrees()
        {
            //return _Pane.GetSimpleTrees();
            return null;
        }


        public SimpleDimTrees GetPaceSimpleTrees()
        {
            return PafApp.SimpleDimTrees;
        }

        /// <summary>
        /// Enables the buttons on the right click menu.
        /// </summary>
        public void AddControlstoRightClickMenu()
        {
            //If you add any buttons to the right click menu, make sure you adjust this index accordingly.
            //Or the Begin Group line break will be placed in the wrong place.
            int microsoftButtonStart = 9;

            ResourceManager resourceManager = PafApp.GetLocalization().GetResourceManager();

            _RightClickBarControls.Add(
                MakeaNewButton(
                    _RightClickMenu,
                    resourceManager.GetString("Application.Excel.Menu.LockCell"),
                    resourceManager.GetString("Application.Excel.Menu.LockCell"),
                    225,
                    true,
                    1,
                    true,
                    "r_LockCell",
                    new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            //TTN-1561
            _RightClickBarControls.Add(
               MakeaNewButton(
                   _RightClickMenu,
                   resourceManager.GetString("Application.Excel.Menu.SessionLock"),
                   resourceManager.GetString("Application.Excel.Menu.SessionLock"),
                   0,
                   false,
                   2,
                   true,
                   "r_SessionLock",
                   new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));


            _RightClickBarControls.Add(
                MakeaNewButton(
                    _RightClickMenu,
                    resourceManager.GetString("Application.Excel.Menu.ReplicateAll"),
                    resourceManager.GetString("Application.Excel.Menu.ReplicateAll"),
                    0,
                    false,
                    3,
                    true,
                    "r_ReplicateAll",
                    new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            _RightClickBarControls.Add(
                MakeaNewButton(
                    _RightClickMenu,
                    resourceManager.GetString("Application.Excel.Menu.ReplicateExisting"),
                    resourceManager.GetString("Application.Excel.Menu.ReplicateExisting"),
                    0,
                    false,
                    4,
                    true,
                    "r_ReplicateExisting",
                    new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            _RightClickBarControls.Add(
               MakeaNewButton(
                   _RightClickMenu,
                   resourceManager.GetString("Application.Excel.Menu.UnReplicate"),
                   resourceManager.GetString("Application.Excel.Menu.UnReplicate"),
                   0,
                   false,
                   5,
                   true,
                   "r_Unreplicate",
                   new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            _RightClickBarControls.Add(
               MakeaNewButton(
                   _RightClickMenu,
                 resourceManager.GetString("Application.Excel.Menu.LiftAll"),
                 resourceManager.GetString("Application.Excel.Menu.LiftAll"),
                   0,
                   false,
                   6,
                   true,
                   "r_LiftAll",
                   new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            _RightClickBarControls.Add(
              MakeaNewButton(
                  _RightClickMenu,
                 resourceManager.GetString("Application.Excel.Menu.LiftExisting"),
                 resourceManager.GetString("Application.Excel.Menu.LiftExisting"),
                  0,
                  false,
                  7,
                  true,
                  "r_LiftExisting",
                  new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            _RightClickBarControls.Add(
             MakeaNewButton(
                 _RightClickMenu,
                 resourceManager.GetString("Application.Excel.Menu.UnLift"),
                 resourceManager.GetString("Application.Excel.Menu.UnLift"),
                 0,
                 false,
                 8,
                 false,
                 "r_Unlift",
                 new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            //TTN-877
            _RightClickBarControls.Add(
                MakeaNewButton(
                    _RightClickMenu,
                    resourceManager.GetString("Application.Excel.Menu.Screentip.PasteShape"),
                    resourceManager.GetString("Application.Excel.Menu.Screentip.PasteShape"),
                    0,
                    false,
                    9,
                    true,
                    "r_PasteShape",
                    new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler)));

            //For a BeginGroup break just below the buttons that we have added to the right click menu.
            //Index is set at top of method.
            //Fix TTN-361.
            _App.CommandBars["Cell"].Controls[microsoftButtonStart].BeginGroup = true;
        }

        /// <summary>
        /// Resets (removes) the Titan buttons from the Excel sheet right click menu.
        /// </summary>
        public void ResetRightClickMenu()
        {
            try
            {
                _RightClickMenu.Reset();
            }
            catch (Exception)
            {
                //We don't care if an exception was throw, just don't show it to the user.
            }
        }

        /// <summary>
        /// Resets the Excel right click menu.
        /// </summary>
        /// <param name="showButtons">Show or hide the buttons.</param>
        public void ShowRightClickMenuButtons(bool showButtons)
        {
            try
            {
                Stopwatch startTime = Stopwatch.StartNew();

                ResourceManager resourceManager = PafApp.GetLocalization().GetResourceManager();
                pafPlanSessionResponse sessionResponse = PafApp.GetPafPlanSessionResponse();

                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.LockCell")].Visible = showButtons;

                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.SessionLock")].Visible = showButtons;

                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.Screentip.PasteShape")].Visible = showButtons;

                //TTN-911
                //_RightClickMenu.Controls[resourceManager.
                //    GetString("Application.Excel.Menu.Screentip.PasteShape")].Visible = showButtons;


                //new code.  This looks at only the PlanSessionResponse.
                bool globalReplicateAll = sessionResponse.replicateAllEnabled;
                bool globalReplicate = sessionResponse.replicateEnabled;

                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.ReplicateAll")].Visible =
                    globalReplicateAll && showButtons;

                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.ReplicateExisting")].Visible =
                    globalReplicate && showButtons;

                if (globalReplicateAll || globalReplicate)
                {
                    _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.UnReplicate")].Visible = showButtons;
                }
                else
                {
                    _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.UnReplicate")].Visible = false;
                }



                //new code.  This looks at only the PlanSessionResponse.
                bool globalLiftAll = sessionResponse.liftAllEnabled;
                bool globalLift = sessionResponse.liftEnabled;

                if (!globalLiftAll)
                {
                    globalLiftAll = sessionResponse.liftAllEnabled;
                }

                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.LiftAll")].Visible =
                globalLiftAll && showButtons;

                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.LiftExisting")].Visible =
                globalLift && showButtons;


                if (globalLiftAll || globalLift)
                {
                    _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.UnLift")].Visible = showButtons;
                }
                else
                {
                    _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.UnLift")].Visible = false;
                }

                //TTN-1995
                _RightClickMenu.Controls[resourceManager.GetString("Application.Excel.Menu.SessionLock")].Visible = PafApp.GetPafAppSettingsHelper().GetGlobalSessionLockEnabled();

                startTime.Stop();
                PafApp.GetLogger().Info("Memory Usage: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
                PafApp.GetLogger().InfoFormat("Complete ShowRightClickMenuButtons, runtime: {0} ms", startTime.ElapsedMilliseconds.ToString("N0"));

            }
            catch (Exception)
            {
                //We don't care if an exception was throw, just don't show it to the user.
            }
        }

        /// <summary>
        /// Show or hide the Role Filter toolbar bar and menubar buttons.
        /// </summary>
        public void ShowRoleFilterToolbarButton()
        {
            bool roleFilterStatus;

            if (_Role.RoleSelector.CurrentPafPlannerConfig != null)
            {
                roleFilterStatus = _Role.RoleSelector.CurrentPafPlannerConfig.isUserFilteredUow;
            }
            else
            {
                roleFilterStatus = false;
            }

            ResourceManager resourceManager = PafApp.GetLocalization().GetResourceManager();

            _ViewToolBar.Controls[resourceManager.GetString("Application.Forms.frmRoleFilter.Caption")].Visible = roleFilterStatus;

            _MenuBar.Controls[resourceManager.GetString("Application.Excel.Menu.RoleFilter")].Visible = roleFilterStatus;

            EnableRoleFilterToolbarButton();
        }

        /// <summary>
        /// Enable or disable the Role Filter toolbar bar and menubar buttons.
        /// </summary>
        public void EnableRoleFilterToolbarButton()
        {
            bool status = true;

            if (PafApp.GetGridApp().RoleSelector.UserSelectedRole.Equals(String.Empty) ||
                PafApp.GetGridApp().RoleSelector.UserPlanTypeSpecString.Equals(String.Empty))
            {
                status = false;
            }

            try
            {
                _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Forms.frmRoleFilter.Caption")].Enabled = status;

                _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.RoleFilter")].Enabled = status;
            }
            catch(Exception)
            {
            }
        }

        /// <summary>
        /// Builds the Pace toolbar.
        /// </summary>
        public void BuildToolbar()
        {
            string toolbarName = PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Toolbar.Name");

            foreach (CommandBar commandBar in _App.CommandBars)
            {
                if (commandBar.Name == toolbarName)
                {
                    _App.CommandBars[toolbarName].Delete();
                    break;
                }
            }

            _ViewToolBar = AddToolbar(toolbarName);
            _ViewToolBar.Visible = false;

            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.Login"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.Login"),
                2126, 
                false,
                null,
                true, 
                "t_Login",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            
            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.SelectRole"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.SelectRole"), 
                2131, 
                false, 
                null, 
                false, 
                "t_SelectRole",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));

            MakeaNewButton(
                _ViewToolBar,
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Forms.frmRoleFilter.Caption"),
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Forms.frmRoleFilter.Caption"),
                602,
                false,
                null,
                true,
                "t_SelectRoleFilter",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));

            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.ShowHideTaskPane"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.ShowHideTaskPane"), 
                2555, 
                false, 
                null,
                PafApp.GetPafPlanSessionResponse() != null ? true : false, 
                "t_TaskPane",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            
            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.SaveChanges"),
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.SaveChanges"), 
                2116, 
                true, 
                null,
                true, 
                "t_SaveChanges",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            
            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.LockCell"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.LockCell"), 
                225,
                true, 
                null, 
                false, 
                "t_LockCell",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            //MakeaNewButton(
            //  _ViewToolBar,
            //  PafApp.GetLocalization().GetResourceManager().
            //      GetString("Application.Excel.Menu.SessionLock"),
            //  PafApp.GetLocalization().GetResourceManager().
            //      GetString("Application.Excel.Menu.SessionLock"),
            //  (Image)PafApp.GetLocalization().GetResourceManager().GetObject("greenlock"),
            //  false,
            //  null,
            //  true,
            //  "t_SessionLock",
            //  new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));

            //Chain 2308
            //Chain Broken 2309
            //Chain with check 4306
            //Chain with grid 
            MakeaNewButton(
             _ViewToolBar,
             PafApp.GetLocalization().GetResourceManager().
                 GetString("Application.Excel.Menu.SessionLock"),
             PafApp.GetLocalization().GetResourceManager().
                 GetString("Application.Excel.Menu.SessionLock"),
             2308,
             false,
             null,
             false,
             "t_SessionLock",
             new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));

            


            //Always show the "Undo Changes" button. (TTN-537)
            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.UndoChanges"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.ToolBar.UndoChanges"), 
                314, 
                true, 
                null,
                PafApp.GetPafPlanSessionResponse() != null ? true : false, 
                "t_UndoChanges",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            
            
            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.RefreshView"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.ToolBar.Refresh"),
                1977, 
                false,
                null,
                false, 
                "t_Refresh",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            
            MakeaNewButton(
                _ViewToolBar, 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.Calculate"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.Calculate"), 
                1000, 
                true,
                null, 
                false, 
                "t_Calculate",
                new _CommandBarButtonEvents_ClickEventHandler[] 
                {
                    PafApp.GetEventManager().CommandBarEventHandler,
                    PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler
                });

            // Build the RuleSet combo box and add it to the Pace toolbar.
            if (PafApp.GetCommandBarMngr().ShouldRuleSetComboBoxBeBuilt)
            {
                BuildRuleSetComboBox(null);
            }

            //TTN-1995
            ShowSessionLockToolbarButtons(PafApp.GetPafAppSettingsHelper().GetGlobalSessionLockEnabled());

            //MakeaNewButton(
            //_ViewToolBar,
            //"Attach Chart",
            //"Attach Chart",
            //436,
            //true,
            //null,
            //true,
            //"m_Chart",
            //new _CommandBarButtonEvents_ClickEventHandler[] 
            //{
            //    PafApp.GetEventManager().CommandBarEventHandler,
            //    PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler
            //});

            //_ViewToolBar.Visible = true;
            _ViewToolBar.Visible = !Globals.ThisWorkbook.Ribbon.Visible;
        }

        /// <summary>
        /// Set the status of the pace toolbar.
        /// </summary>
        public void SetToolbarStatus(bool toolbarStatus)
        {
            if (Globals.ThisWorkbook.Ribbon.Visible)
            {
                _ViewToolBar.Enabled = false;
                return;
            }
            _ViewToolBar.Enabled = toolbarStatus;
        }

        /// <summary>
        /// Build the ruleset combo box and add it to the Pace toolbar.
        /// </summary>
        /// <param name="before">Inserts the combo box before a control at a given position.</param>
        public void BuildRuleSetComboBox(int? before)
        {
            if (PafApp.GetPafPlanSessionResponse() != null && 
                PafApp.GetPafPlanSessionResponse().ruleSetList != null)
            {
                int ruleSetListLength = PafApp.GetPafPlanSessionResponse().ruleSetList.Length;

                //Add the Ruleset combo box if multiple measure rule sets exist
                if (ruleSetListLength > 0 && PafApp.GetPafPlanSessionResponse().ruleSetList[0] != null)
                {
                    List<string> items = new List<string>();
                    int defaultRuleSetIndex = 0;
                    int i = 1; //index is 1 based
                    foreach (string item in PafApp.GetPafPlanSessionResponse().ruleSetList)
                    {
                        items.Add(item);
                        if (item.ToUpper() == PafApp.GetPafPlanSessionResponse().defaultRuleSetName.ToUpper())
                        {
                            defaultRuleSetIndex = i;
                        }
                        i++;
                    }

                    MakeaNewDropDown(_ViewToolBar,
                         PafApp.GetLocalization().GetResourceManager().
                             GetString("Application.Excel.Menu.ChangeRuleSet"),
                         items,
                         true,
                         before,
                         ruleSetListLength == 1 ? false : true,
                         "m_ChangeRuleSet",
                         defaultRuleSetIndex,
                         110,
                         new _CommandBarComboBoxEvents_ChangeEventHandler(
                             PafApp.GetEventManager().DropDownEventHandler));
                }
            }
        }

        /// <summary>
        /// Adds the Test Harness Buttons to the Pace Toolbar
        /// </summary>
        public void BuildTestHarnessToolBarButtons()
        {
            _ViewToolBar.Visible = false;
            
            MakeaNewButton(
                _ViewToolBar,
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.LoadTest"), 
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.LoadTest"),
                186,
                true,
                null,
                true,
                "t_LoadTest",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler));
            
            MakeaNewButton(
                _ViewToolBar,
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.RecordStart"),
                PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.RecordStart"),
                184,
                false,
                null,
                true,
                "t_Record",
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler));

            if (PafApp.GetPafAuthResponse() != null && 
                PafApp.GetPafAuthResponse().securityToken != null)
                {
                    ShowTestHarnessToolbarButtons(PafApp.GetPafAuthResponse().securityToken.admin); 
                }

            if (PafApp.GetPafPlanSessionResponse() != null)
                PafApp.GetGridApp().EnableTestHarnessToolbarButtons(true);
            else
                PafApp.GetGridApp().EnableTestHarnessToolbarButtons(false);


            _ViewToolBar.Visible = true;
        }

        public void SetSessionLockButtonStatus(bool hasLocks)
        {
            return; // The Section Lock feature is not current enabled.
            if (_ViewToolBar == null) return;
            CommandBarButton button = (CommandBarButton)_ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SessionLock")];
            if (button == null) return;
            string enabled = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SessionLock.Faceid.Enabled");
            string disabled = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SessionLock.Faceid.Disabled");
            button.FaceId = Convert.ToInt32(hasLocks ? enabled : disabled);

            button = (CommandBarButton)_MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SessionLock")];
            if (button == null) return;
            button.FaceId = Convert.ToInt32(hasLocks ? enabled : disabled);


            button = (CommandBarButton)_RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SessionLock")];
            if (button == null) return;
            button.FaceId = Convert.ToInt32(hasLocks ? enabled : disabled);
        }

        /// <summary>
        /// Show or hide the extra test harness toolbar buttons on the toolbar.
        /// </summary>
        /// <param name="show">true to show the buttons, false to hide them.</param>
        public void ShowSessionLockToolbarButtons(bool show)
        {
            try
            {
                _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.SessionLock")].Visible = show;

                _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.SessionLock")].Visible = show;

            }
            catch (Exception)
            {
                //We don't care about the error.
            }
        }

        /// <summary>
        /// Show or hide the extra test harness toolbar buttons on the toolbar.
        /// </summary>
        /// <param name="show">true to show the buttons, false to hide them.</param>
        public void ShowTestHarnessToolbarButtons(bool show)
        {
            try
            {
                _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.LoadTest")].Visible = show;
                
                
                _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.RecordStart")].Visible = show;

            }
            catch (Exception)
            {
                //We don't care about the error.
            }
        }


        /// <summary>
        /// Enable or disable the extra test harness toolbar buttons on the toolbar.
        /// </summary>
        /// <param name="enable">true to enable the buttons, false to disable them.</param>
        public void EnableTestHarnessToolbarButtons(bool enable)
        {
            try
            {
                _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.LoadTest")].Enabled = enable;


                _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.RecordStart")].Enabled = enable;

            }
            catch (Exception)
            {
                //We don't care about the error.
            }
        }

        /// <summary>
        /// Removes the drop down menu for the Excel application.
        /// </summary>
        /// <param name="menuName">Name of the menu to remove.</param>
        public void DeleteMenus(string menuName)
        {
            //Delete a menu if one exists.
            try
            {
                _App.MenuBars[
                    PafApp.GetLocalization().GetResourceManager().GetString(
                    "Application.Excel.CommandBars.WorksheetMenuBar")
                    ].Menus[menuName].Delete();
            }
            catch (Exception)
            {
                //We don't care about the error, just delete the menu.
            }
        }

        /// <summary>
        /// Builds a MenuBar in the Excel "Worksheet Menu Bar"
        /// </summary>
        /// <param name="menuName">The name of the menu.</param>
        /// <param name="toolTip">The tooltip for the menubar.</param>
        public void BuildMenu(string menuName, string toolTip)
        {
            DeleteMenus(menuName);

            _MenuBar = AddMenubar(menuName, toolTip);

            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Login"), 2126, false, null, true, "m_Login", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.SelectRole"), 2131, false, null, false, "m_SelectRole", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.RoleFilter"), 602, false, null, true, "m_SelectRoleFilter", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.ShowHideTaskPane"), 2555, false, null, 
                PafApp.GetPafPlanSessionResponse() != null ? true : false, "m_TaskPane", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.SaveChanges"), 2116, true, null, true, "m_SaveChanges", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.LockCell"), 225, true, null, false, "m_LockCell", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar,PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.SessionLock"), 2308,false,null,false,"m_SessionLock", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.RefreshView"), 314, true, null, true, "m_UndoChanges", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.UndoChanges"), 1977, false, null, false, "m_Refresh", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Calculate"), 1000, true, null, false, "m_Calculate", null,
                new _CommandBarButtonEvents_ClickEventHandler[] 
                {
                    PafApp.GetEventManager().CommandBarEventHandler,
                    PafApp.GetTestHarnessManager().TestHarnessToolbarEventHandler
                }, 
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.Shortcut.Calculate"));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                 GetString("Application.Excel.Menu.Sort"), 210, true, null, false, "m_Sort", null,
                 new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            //TTN-911
            //MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
            //    GetString("Application.Excel.Menu.Screentip.PasteShape"), 9994, true, null, false, "m_PasteShape", null,
            //    new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));


            //TTN-877
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Screentip.PasteShape"), 0, true, null, false, "m_PasteShape", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));


            //Add custom menu items
            if (PafApp.GetPafPlanSessionResponse() != null && PafApp.GetPafPlanSessionResponse().customMenuDefs != null)
            {
                foreach (customMenuDef menuDef in PafApp.GetPafPlanSessionResponse().customMenuDefs)
                {
                    if (menuDef != null)
                    {
                        MakeaNewMenuButton(_MenuBar, menuDef.caption, menuDef.faceID, menuDef.beginGroup, null, menuDef.enableButton, "m_Custom", menuDef.key,
                            new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
                    }
                }
            }

            //// Jim's Assortment Hackery...
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
              GetString("Application.Excel.Menu.CreateAssortment"), 0, true, null, false, "m_CreateAssortment", null,
              new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));



            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
               GetString("Application.Excel.Menu.Options"), 4266, true, null, false, "m_Options", null,
               new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
               GetString("Application.Excel.Menu.ChangePassword"), 505, false, null, false, "m_ChangePassword", null,
               new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
               GetString("Application.Excel.Menu.Help"), 984, true, null, true, "m_Help", null,
               new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));
            MakeaNewMenuButton(_MenuBar, PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.About"), 0, false, null, true, "m_About", null,
                new _CommandBarButtonEvents_ClickEventHandler(PafApp.GetEventManager().CommandBarEventHandler));


            //TTN-1995
            ShowSessionLockToolbarButtons(PafApp.GetPafAppSettingsHelper().GetGlobalSessionLockEnabled());

            _MenuBar.Visible = !Globals.ThisWorkbook.Ribbon.Visible;
        }

        /// <summary>
        /// Enables/disables the button Roles buttons.
        /// </summary>
        /// <param name="enableRefreshButton">Enable/disable the refresh button.</param>
        public void EnableRolesButtons(bool enableRefreshButton)
        {
            _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.SelectRole")].Enabled = enableRefreshButton;

            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.SelectRole")].Enabled = enableRefreshButton;
        }

        /// <summary>
        /// Enables/disables the buttons that are specific to our views only.
        /// </summary>
        /// <param name="enableRefreshButton">Enable/disable the refresh button.</param>
        public void EnableViewSheetOnlyButtons(bool enableRefreshButton)
        {
            _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.RefreshView")].Enabled = enableRefreshButton;

            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.RefreshView")].Enabled = enableRefreshButton;
        }

        /// <summary>
        /// Enables the PasteShape buttons on the toolbar and right click menu.
        /// </summary>
        /// <remarks>Removed contents. http://63.122.66.56/jira/browse/TTN-911</remarks>
        public void EnablePasteShapeButtons1(bool enableButton)
        {
            ////Added for the menu bar
            //_MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
            //    GetString("Application.Excel.Menu.Screentip.PasteShape")].Enabled = enableButton;

            ////Added for the right click menu
            //_RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().
            //    GetString("Application.Excel.Menu.Screentip.PasteShape")].Enabled = enableButton;
        }


        /// <summary>
        /// Enables the PasteShape buttons on the toolbar and right click menu.
        /// </summary>
        /// <remarks>Removed contents. http://63.122.66.56/jira/browse/TTN-877</remarks>
        public void EnablePasteShapeButtons(bool enableButton)
        {
            //Added for the menu bar
            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Screentip.PasteShape")].Enabled = enableButton;

            //Added for the right click menu
            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Screentip.PasteShape")].Enabled = enableButton;
        }

        /// <summary>
        /// Enable or disble the hyperlink buttons.
        /// </summary>
        /// <param name="enable">enable or disable</param>
        /// <param name="buttons">buttons to disable.</param>
        public void EnableHyperlinkButton(bool enable, List<CommandBarControl> buttons)
        {
            foreach (CommandBarControl c in buttons)
            {
                try
                {
                    c.Enabled = enable;
                }
                catch (Exception ex)
                {
                    //just keep going...
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// Enable or disble the hyperlink buttons.
        /// </summary>
        /// <param name="enable">enable or disable</param>
        /// <param name="buttons">buttons to disable.</param>
        public void EnableFormulaButton(bool enable, List<CommandBarControl> buttons)
        {
            foreach (CommandBarControl c in buttons)
            {
                try
                {
                    c.Enabled = enable;
                }
                catch (Exception ex)
                {
                    //just keep going...
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// Enables the buttons on the Toolbar and Menu Bar.
        /// </summary>
        /// <param name="enableLockButton">Enable/disable the lock buttons.</param>
        public void EnableLockButtons(bool enableLockButton)
        {
            //Added for the toolbar
            _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.LockCell")].Enabled = enableLockButton;

            //Added for the menu bar
            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.LockCell")].Enabled = enableLockButton;

            //Added for the right click menu
            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager()
                .GetString("Application.Excel.Menu.LockCell")].Enabled = enableLockButton;

        }

        /// <summary>
        /// Enables the buttons on the Toolbar and Menu Bar.
        /// </summary>
        /// <param name="enable">Enable/disable the lock buttons.</param>
        public void EnableSessionLockButtons(bool enable)
        {
            if (_ViewToolBar == null) return;

            //Added for the toolbar
            _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.SessionLock")].Enabled = enable;

            //Added for the menu bar
            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.SessionLock")].Enabled = enable;

            //Added for the right click menu
            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager()
                .GetString("Application.Excel.Menu.SessionLock")].Enabled = enable;

        }

        /// <summary>
        /// Enabled/disabled the cell notes buttons.
        /// </summary>
        /// <param name="enable">Enables/disable the cell notes buttons.</param>
        /// <param name="buttons">Buttons to enable/disable.</param>
        public void EnableCellNotesButtons(bool enable, List<CommandBarControl> buttons)
        {
            foreach (CommandBarControl c in buttons)
            {
                try
                {
                    c.Enabled = enable;
                }
                catch(Exception ex)
                {
                    //just keep going...
                    Debug.WriteLine(ex.Message);
                }
            }
        }

        /// <summary>
        /// Enables the replicate/unreplicate buttons on the right click menu.
        /// </summary>
        /// <param name="enableReplicateButton">Enable/disable replication button.</param>
        /// <param name="enableUnReplicateButton">Enable/disable unreplication button.</param>
        public void EnableReplicateButtons(bool enableReplicateButton, bool enableUnReplicateButton)
        {
            bool globalReplicateAll = PafApp.GetPafPlanSessionResponse().replicateAllEnabled;
            bool globalReplicate = PafApp.GetPafPlanSessionResponse().replicateEnabled;

            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.ReplicateAll")].Enabled =
                globalReplicateAll && enableReplicateButton;

            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.ReplicateAll")].Visible = globalReplicateAll && enableReplicateButton;

            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.ReplicateExisting")].Enabled =
                globalReplicate && enableReplicateButton;

            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.ReplicateExisting")].Visible = globalReplicate && enableReplicateButton;

            if (globalReplicateAll || globalReplicate)
            {
                _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.UnReplicate")].Enabled = enableUnReplicateButton;
                _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.UnReplicate")].Visible = enableUnReplicateButton;
            }
            else
            {
                _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.UnReplicate")].Enabled = false;
                _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.UnReplicate")].Visible = false;
            }
        }

        /// <summary>
        /// Enables the replicate/unreplicate buttons on the right click menu.
        /// </summary>
        /// <param name="enablLiftButton">Enable/disable replication button.</param>
        /// <param name="enableUnLiftButton">Enable/disable unreplication button.</param>
        public void EnableLiftButtons(bool enablLiftButton, bool enableUnLiftButton)
        {
            bool globalLiftAll = PafApp.GetPafPlanSessionResponse().liftAllEnabled;
            bool globalLift = PafApp.GetPafPlanSessionResponse().liftEnabled;


            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.LiftAll")].Enabled =
                globalLiftAll && enablLiftButton;

            _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.LiftExisting")].Enabled =
                globalLift && enablLiftButton;

            if (globalLiftAll || globalLift)
            {
                _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager()
                    .GetString("Application.Excel.Menu.UnLift")].Enabled = enableUnLiftButton;
            }
            else
            {
                _RightClickMenu.Controls[PafApp.GetLocalization().GetResourceManager()
                    .GetString("Application.Excel.Menu.UnLift")].Enabled = false;
            }
        }

        /// <summary>
        /// Enables the Change Password optoion in Menu Bar.
        /// </summary>
        /// <param name="enableChangePasswordButton">Enable or disable the button.
        /// If the user is a domain/LDAP user this parm is ignored and the button is disabled.
        /// </param>
        public void EnableChangePasswordButton(bool enableChangePasswordButton)
        {
            if (_Logon.DomainUser)
            {
                enableChangePasswordButton = false;
            }

            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.ChangePassword")].Enabled = enableChangePasswordButton;
       
        }

        /// <summary>
        /// Enables or disables the save, commit, undo buttons and change ruleset combobox depending on the actions 
        /// available.
        /// </summary>
        /// <param name="areCalculationsPending">Calculate button status.</param>
        /// <param name="areAnyCalculationsPending">Used for the change ruleset dropdown.</param>
        public void EnableCommandBarControls(bool areCalculationsPending, bool areAnyCalculationsPending)
        {
            bool pafPlanSessionResp = PafApp.GetPafPlanSessionResponse() != null ? true : false;
            
            //Always show the "Refresh View" button. (TTN-537)
            _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.UndoChanges")].Enabled = pafPlanSessionResp;

            _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Calculate")].Enabled = areCalculationsPending;

            try
            {
                _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Menu.ChangeRuleSet")].Enabled = !areAnyCalculationsPending;
            }
            catch(Exception)
            {
                //there could be an error because this control does not alway exist, so just ignore it.
            }

            //Only enable the show/hide button when user is logged in. (TTN-561)
            _ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.ShowHideTaskPane")].Enabled = pafPlanSessionResp;

            //Always show the "Refresh View" button. (TTN-537) KRM
            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.RefreshView")].Enabled = pafPlanSessionResp;

            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.Calculate")].Enabled = areCalculationsPending;

            //Only enable the show/hide button when user is logged in. (TTN-561)
            _MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Excel.Menu.ShowHideTaskPane")].Enabled = pafPlanSessionResp;

            //_MenuBar.Controls[PafApp.GetLocalization().GetResourceManager().
            //    GetString("Application.Excel.Menu.CreateAssortment")].Enabled = pafPlanSessionResp;

            //custom menu items
            if (PafApp.GetPafPlanSessionResponse() != null && PafApp.GetPafPlanSessionResponse().customMenuDefs != null)
            {
                foreach (customMenuDef menuDef in PafApp.GetPafPlanSessionResponse().customMenuDefs)
                {
                    if (menuDef != null)
                    {
                        //TTN-894, only disable when calculations are pending.
                        if (areAnyCalculationsPending)
                        {
                            _MenuBar.Controls[menuDef.caption].Enabled = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Shows/Hides the custom toolbars within the application.
        /// </summary>
        /// <param name="showToolBar">Show the custom toolbar.</param>
        /// <param name="showMenuBar">Show the Menu Bar menu.</param>
        public void EnableApplicationCommandBars(bool showToolBar, bool showMenuBar)
        {
            try
            {
                if (Globals.ThisWorkbook.Ribbon.Visible)
                {
                    _ViewToolBar.Visible = false;
                    _MenuBar.Visible = false;
                    return;
                }
                _ViewToolBar.Enabled = showToolBar;
                _MenuBar.Enabled = showMenuBar;

            }
            catch (Exception)
            {
                //ignore errors
            }
        }

        /// <summary>
        /// Enables the custom class plan menu.
        /// </summary>
        /// <param name="caption">Caption of the popup menu.</param>
        /// <param name="enabled">Enable or disable the the popup menu.</param>
        public void EnableMenuButton(string caption, bool enabled)
        {
            //TTN-894
            try
            {
                _MenuBar.Controls[caption].Enabled = enabled;
            }
            catch(Exception)
            {
            }
        }

        /// <summary>
        /// Shows the task pane if it's hidden, hides the task pane if it's visible.
        /// </summary>
        public void ShowHideTaskPane()
        {
            try
            {
                bool taskPaneStatus;
                //Removed TTN-1064
                //if (_App.CommandBars[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Toolbar.TaskPane.Name")].Visible)
                //Added TTN-1064
                if (PafApp.GetEventManager().TaskPaneCommandBar.Visible)
                {
                    //TemporaryLogTTN1064("ExcelAppImpl.ShowHideTaskPane()", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), null, "user setting task pane status to false");
                    taskPaneStatus = false;
                }
                else
                {
                    //TemporaryLogTTN1064("ExcelAppImpl.ShowHideTaskPane()", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), null, "user setting task pane status to true");
                    taskPaneStatus = true;
                }

                Globals.ThisWorkbook.ActionsPane.Visible = taskPaneStatus;
                Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = taskPaneStatus;
                //Added TTN-1064
                PafApp.GetEventManager().TaskPaneCommandBar.Visible = taskPaneStatus;
                //Removed TTN-1064
                //_App.CommandBars[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Toolbar.TaskPane.Name")].Visible = taskPaneStatus;

                if (taskPaneStatus)
                {
                    //Fix Jira issue #26
                    int row = Globals.ThisWorkbook.Application.ActiveCell.Row;
                    int col = Globals.ThisWorkbook.Application.ActiveCell.Column;

                    ((Range)((Worksheet)Globals.ThisWorkbook.ActiveSheet).Cells[row, col]).Activate();
                }

                SaveTaskPaneStatus(taskPaneStatus);

                //removed TTN-1064
                //Settings.Default.TaskPaneStatus = taskPaneStatus;
                //Settings.Default.Save();
                //Settings.Default.Reload();
            }
            //Fix Jira issue #110.
            catch (COMException ex)
            {
                PafApp.GetLogger().Error(ex);
                //TemporaryLogTTN1064("ExcelAppImpl.ShowHideTaskPane()", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), ex, "error occured");
            }
        }

        /// <summary>
        /// Shows/Hides the task pane
        /// </summary>
        /// <param name="showTaskPane">true shows the task pane/false hides it</param>
        public void ShowHideTaskPane(bool showTaskPane)
        {
            try
            {
                if (showTaskPane)
                {
                    _App.DisplayDocumentActionTaskPane = Settings.Default.TaskPaneStatus;

                    //TemporaryLogTTN1064("ExcelAppImpl.ShowHideTaskPane(bool showTaskPane)", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), null, "task pane is: " + Settings.Default.TaskPaneStatus);
                }
            }
            catch (Exception)
            {
                //TemporaryLogTTN1064("ExcelAppImpl.ShowHideTaskPane(bool showTaskPane)", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), ex, "error occured");
                //ignore errors
            }
        }

        /// <summary>
        /// Shows/Hides the task pane
        /// </summary>
        /// <param name="showTaskPane">true shows the task pane/false hides it</param>
        public void SetTaskPaneStatus(bool showTaskPane)
        {
            try
            {
                _App.DisplayDocumentActionTaskPane = showTaskPane;

                //TemporaryLogTTN1064("ExcelAppImpl.SetTaskPaneStatus(bool showTaskPane)", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), null, "task pane is: " + showTaskPane);
            }
            catch (Exception)
            {
                //TemporaryLogTTN1064("ExcelAppImpl.SetTaskPaneStatus(bool showTaskPane)", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), ex, "error ocured");
                //ignore errors
            }
        }

        /// <summary>
        /// Searches the sheet to find a tag with the following value.
        /// </summary>
        /// <param name="sh">The Excel worksheet.</param>
        /// <param name="propertyName">The name of the property to find.</param>
        /// <param name="value">The property value to find.</param>
        /// <returns>True if the tag is found, false if the tag is not found.</returns>
        public bool DoesSheetContainCustomProperty(Worksheet sh, string propertyName, object value)
        {
            try
            {
                foreach (CustomProperty cp in sh.CustomProperties)
                {
                    if (cp.Name.ToUpper().Equals(propertyName.ToUpper()) &&
                        cp.Value.Equals(value))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return false;
            }
        }

        /// <summary>
        /// Searches the sheet to find a tag with the following value.
        /// </summary>
        /// <param name="sh">The Excel worksheet.</param>
        /// <param name="propertyName">The property to find.</param>
        /// <returns>True if the tag is found, false if the tag is not found.</returns>
        public bool DoesSheetContainCustomProperty(Worksheet sh, string propertyName)
        {
            try
            {
                foreach (CustomProperty cp in sh.CustomProperties)
                {
                    if (cp.Name.ToUpper().Equals(propertyName.ToUpper()))
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return false;
            }
        }

        /// <summary>
        /// Searches the activeworkbook to find a tag with the following value.
        /// </summary>
        /// <param name="propertyName">The property to find.</param>
        /// <param name="value">The value of the property.</param>
        /// <returns>The worksheet if the tag is found, null if the tag is not found.</returns>
        public Worksheet DoesWorkbookContainCustomProperty(string propertyName, object value)
        {
            try
            {
                foreach (Worksheet sheet in _App.ActiveWorkbook.Worksheets)
                {
                    foreach (CustomProperty cp in sheet.CustomProperties)
                    {
                        if (cp.Name.ToUpper().Equals(propertyName.ToUpper()) &&
                            cp.Value.Equals(value))
                        {
                            return sheet;
                        }
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// Checks the workbook to see if the worksheet already exists.
        /// </summary>
        /// <param name="wbk">Workbook to search for sheet.</param>
        /// <param name="name">Name of the sheet to look for.</param>
        /// <returns>True if the sheet exists, false if not.</returns>
        public bool DoesSheetExist(Workbook wbk, string name)
        {
            try
            {
                foreach (Worksheet sheet in wbk.Worksheets)
                {
                    if (sheet.Name.ToLower().Equals(name.ToLower()))
                        return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return false;
            }
        }

        /// <summary>
        /// Checks the workbook to see if the worksheet already exists.
        /// </summary>
        /// <param name="name">Name of the sheet to look for.</param>
        /// <returns>True if the sheet exists, false if not.</returns>
        public bool DoesSheetExist(string name)
        {
            try
            {
                foreach (Worksheet sheet in _App.ActiveWorkbook.Worksheets)
                {
                    if (sheet.Name.ToLower().Equals(name.ToLower()))
                    return true;
                }
                return false;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return false;
            }
        }

        /// <summary>
        /// Gets the format for a style.
        /// </summary>
        /// <param name="styleName">name of the sytle to return the format object.</param>
        /// <returns>a format object.</returns>
        public Format GetStyleFormat(string styleName)
        {
            Style style;

            try
            {
                style = Globals.ThisWorkbook.Styles[styleName];
            }
            catch
            {
                return null;
            }

            Format format = new Format();

            switch (style.HorizontalAlignment)
            {
                case Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft: //"left"
                    format.Alignment = "left";
                    break;
                case Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter: //"center"
                    format.Alignment = "center";
                    break;
                case Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignRight: //"right"
                    format.Alignment = "right";
                    break;
            }


            format.BgHexFillColor = cu.GetHexStrFromWin32Color(int.Parse(style.Interior.Color.ToString()));
            //format.BgHexFillColor = ColorTranslator.FromWin32((int.Parse(style.Interior.Color.ToString()))).ToArgb().ToString();

            format.Bold = style.Font.Bold.ToString();

            format.FontHexColor = cu.GetHexStrFromWin32Color(int.Parse(style.Font.Color.ToString()));
            //format.FontHexColor = ColorTranslator.FromWin32((int.Parse(style.Font.Color.ToString()))).ToArgb().ToString();

            format.FontName = style.Font.Name.ToString();

            format.Italics = style.Font.Italic.ToString();

            format.Size = double.Parse(style.Font.Size.ToString());

            format.StrikeOut = style.Font.Strikethrough.ToString();

            format.UnderLine = Boolean.FalseString;

            format.DoubleUnderline = Boolean.FalseString;

            if(style.Locked)
            {
                format.Plannable = Boolean.FalseString;
            }
            else
            {
                format.Plannable = Boolean.TrueString;
            }

            format.NumberFormat = style.NumberFormat;

            return format;
        }

        /// <summary>
        /// Searches the sheet to find a property and returns its value..
        /// </summary>
        /// <param name="sh">The Excel worksheet.</param>
        /// <param name="propertyName">The property to find.</param>
        /// <returns>Returns the value if found, null if the value is not found.</returns>
        public object GetCustomProperty(Worksheet sh, string propertyName)
        {
            try
            {
                foreach (CustomProperty cp in sh.CustomProperties)
                {
                    if (cp.Name.ToUpper().Equals(propertyName.ToUpper()))
                    {
                        return cp.Value;
                    }
                }
                return null;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// Adds a custom property to the excel worksheet.
        /// </summary>
        /// <param name="sh">The excel worksheet.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">Value of the property.</param>
        public void AddCustomProperty(Worksheet sh, string propertyName, object value)
        {
            foreach (CustomProperty cp in sh.CustomProperties)
            {
                if (cp.Name.ToUpper().Equals(propertyName.ToUpper()) &&
                    cp.Value.Equals(value))
                {
                    return;
                }
            }
            sh.CustomProperties.Add(propertyName, value);
        }

        /// <summary>
        /// Adds a custom property to the excel worksheet.
        /// </summary>
        /// <param name="sh">The excel worksheet.</param>
        /// <param name="propertyName">Name of the property.</param>
        public bool RemoveCustomProperty(Worksheet sh, string propertyName)
        {
            foreach (CustomProperty cp in sh.CustomProperties)
            {
                if (cp.Name.ToUpper().Equals(propertyName.ToUpper()))
                {
                    cp.Delete();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Creates a new worksheet with a custom view property, and the same name as the
        /// viewName parameter, with any invalid characters striped out.
        /// </summary>
        /// <param name="currentSheet">The currently active worksheet.</param>
        /// <param name="viewPropertyName">The name of the viewProperty to track the view name.</param>
        /// <param name="viewName">The name of the view to be put on the sheet.</param>
        /// <param name="sheetPassword">Password to protect the worksheet.</param>
        /// <returns>The worksheet if successful, null if not.</returns>
        public Worksheet BuildNewViewSheet(Worksheet currentSheet, string viewPropertyName,
            string viewName, string sheetPassword)
        {
            //Need to unlock the current sheet, create the new sheet, activate it, protect it, 
            //add the sheet prop then build the view.
            try
            {

                //Unprotect the current sheet.
                UnprotectSheet(currentSheet, sheetPassword);

                //TTN-633
                ////Check the rest of the sheets to see if they are protected and make sure
                ////they don't have a password.
                ////Fix Jira issue #80
                //Worksheet sht = AnySheetsProtectedWithPassword();
                //if (sht != null)
                //{
                //    ProtectSheet(currentSheet, sheetPassword);
                //    throw new Exception(
                //        String.Format(PafApp.GetLocalization().GetResourceManager().
                //            GetString("Application.Exception.UserLockedSheetExists"),
                //            sht.Name));
                //}

                //See if a sheet with the name already exists
                //Removed to Fix TTN-883 (TTN-875 fixes this issue, but normalizing the sheet name)
                //if (DoesSheetExist(viewName))
                //{
                //    throw new Exception(
                //        String.Format(PafApp.GetLocalization().GetResourceManager().
                //            GetString("Application.MessageBox.SheetExists"),
                //            viewName));
                //}

                //Create a new sheet.
                Worksheet newSheet;
                newSheet = PafApp.GetGridApp().AddNewWorksheet(viewName, true, true);
                if (newSheet == null)
                    return null;
                //Activate the new sheet.
                ActivateSheet(newSheet.Name);
                //Add the custom property, which is the name of the view that will be on the sheet.
                AddCustomProperty(newSheet, viewPropertyName, viewName);
                //Apply our formats.
                AddOurFormats(newSheet);
                ////TTN-633 (removed)
                //Protect the sheet.
                //ProtectSheet(newSheet, sheetPassword);

                return newSheet;
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// Removes all the view sheets in the workbook.
        /// </summary>
        public void RemoveViewSheets()
        {
            //Remove all the view sheets.
            foreach (Worksheet sheet in Globals.ThisWorkbook.Worksheets)
            {
                if (DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    try
                    {
                        Globals.ThisWorkbook.Application.DisplayAlerts = false;
                        UnprotectSheet(sheet, PafAppConstants.SHEET_PASSWORD);
                        PafApp.GetViewMngr().RemoveView(sheet.GetHashCode());
                        sheet.Delete();
                    }
                    catch (Exception ex)
                    {
                        PafApp.MessageBox().Show(ex);
                    }
                    finally
                    {
                        Globals.ThisWorkbook.Application.DisplayAlerts = true;
                    }
                }
            }
        }

        /// <summary>
        /// Protects the Excel worksheet.
        /// </summary>
        /// <param name="sheet">The Excel worksheet to protect.</param>
        /// <param name="password">The password to use.</param>
        /// <returns>True if the sheet was locked, false if not.</returns>
        public bool ProtectSheet(Worksheet sheet, string password)
        {
            try
            {
                if (!sheet.ProtectContents)
                {
                    //TTN-767 Changed DrawingObject to false, to allow for cell notes.
                    sheet.Protect(password, false, true, true, true, true, true, true,
                        false, false, true, false, false, false, false, false);
                    sheet.EnableOutlining = true;
                }
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Unprotects the Excel worksheet.
        /// </summary>
        /// <param name="sht">The Excel worksheet to protect.</param>
        /// <param name="password">The password to use.</param>
        /// <returns>True if the sheet was unlocked, false if not.</returns>
        /// <remarks>Warning - This method breaks Excel's Cut/Copy Mode</remarks>
        public bool UnprotectSheet(Worksheet sht, string password)
        {
            try
            {
                sht.Unprotect(password);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Unprotects all worksheets in the workbook.
        /// </summary>
        public void UnprotectWorksheets()
        {
            try
            {
                foreach (Worksheet sheet in _App.ActiveWorkbook.Worksheets)
                {
                    bool val = PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);
                    if (val)
                    {
                        if (sheet.ProtectContents)
                        {
                            UnprotectSheet(sheet, PafAppConstants.SHEET_PASSWORD);
                        }
                    }
                    else
                    {
                        if(sheet.ProtectContents)
                        {
                            if(! UnprotectSheet(sheet, PafAppConstants.SHEET_PASSWORD))
                            {
                                UnprotectSheet(sheet, String.Empty);
                            }
                        }
                    }
                }
            }
            catch
            {
                return;
            }
        }

        /// <summary>
        /// Protects all our worksheets in the workbook
        /// </summary>
        public void ProtectOurWorksheets()
        {
            try
            {
                //_App.Application.ScreenUpdating = false;

                foreach (Worksheet sheet in _App.ActiveWorkbook.Worksheets)
                {
                    bool val = PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);
                    if (val)
                    {
                        ProtectSheet(sheet, PafAppConstants.SHEET_PASSWORD);
                    }
                }
            }
            catch
            {
                return;
            }
        }

        public void HideUnbuiltWorksheet(Worksheet currentSheet)
        {
            try
            {
                foreach (Worksheet sheet in _App.ActiveWorkbook.Worksheets)
                {
                    if (!sheet.Name.EqualsIgnoreCase(currentSheet.Name))
                    {
                        if (PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet,PafAppConstants.OUR_VIEW_PROP_VALUE))
                        {
                            if (!PafApp.GetViewMngr().GetView(sheet.GetHashCode()).ViewHasBeenBuilt)
                            {
                                PafApp.GetGridApp().HideSheet(sheet.Name);
                            }
                        }
                    }
                }
            }
            catch
            {
                return;
            }
        }

        ///// <summary>
        ///// Searches the workbook for protected worksheets, and unprotects them if no password is present.
        ///// </summary>
        ///// <returns>Returns the worksheet if a protected sheet with a password is found, null if not.</returns>
        //private Worksheet AnySheetsProtectedWithPassword()
        //{
        //    try
        //    {
        //        foreach (Worksheet sheet in _App.ActiveWorkbook.Sheets)
        //        {
        //            if (sheet.ProtectContents)
        //            {
        //                if (!UnprotectSheet(sheet, ""))
        //                {
        //                    return sheet;
        //                }
        //            }
        //        }
        //        return null;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        /// <summary>
        /// Toggle screen updating
        /// </summary>
        /// <returns></returns>
        public bool ScreenUpdating
        {
            get { return _App.ScreenUpdating; }
            set { _App.ScreenUpdating = value; }
        }

        /// <summary>
        /// Get/Set if Excel will displays alerts.
        /// </summary>
        public bool DisplayAlerts
        {
            get { return _App.DisplayAlerts; }
            set { _App.DisplayAlerts = value; }
        }

        /// <summary>
        /// Turn on or off the Excel events.
        /// </summary>
        /// <param name="flag">Sets the Excel events to be on or off.</param>
        public void EnableEvents(bool flag)
        {
            _App.EnableEvents = flag;

        }

        /// <summary>
        /// Clears the grid of all data.
        /// </summary>
        /// <param name="grid">Name of the grid to clear.</param>
        public void ClearGrid(string grid)
        {
            try
            {
                //Turn events off - if not the app will hang.
                PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);

                if(Settings.Default.AutoFreezePane)
                    _App.ActiveWindow.FreezePanes = false;

                //Clear the view.
                PafApp.GetViewMngr().CurrentGrid.Clear();
                //PafApp.Clear();

                //Turn the events back on.
                PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
            }
            catch (Exception) //Catch the grid not found error.
            {
            }
        }

        /// <summary>
        /// Get a Excel.Worksheet from a hash code.
        /// </summary>
        /// <param name="hashCode">The hash code of the worksheet that should be returned.</param>
        /// <returns>A worksheet if one is found, null if no worksheet is found.</returns>
        public Worksheet GetWorksheetFromHashCode(int hashCode)
        {
            try 
            {
                foreach (Worksheet sheet in _App.ActiveWorkbook.Worksheets)
                {
                    if (sheet.GetHashCode().Equals(hashCode))
                        return sheet;
                }

                return null;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// Activates an Excel worksheet.
        /// </summary>
        /// <param name="sheetName">The name of the worksheet to activate.</param>
        /// <returns>True if activated, flase if the sheet was not activated.</returns>
        public bool ActivateSheet(string sheetName)
        {
            try
            {
                ((Worksheet)_App.ActiveWorkbook.Worksheets[sheetName]).Activate();
                return true;
            }
            catch
            {
                return false;
            }
        }


        public void HideSheet(string sheetName)
        {
            try
            {
                if (((Worksheet)_App.ActiveWorkbook.Worksheets[sheetName]).Visible != XlSheetVisibility.xlSheetHidden)
                {
                    ((Worksheet) _App.ActiveWorkbook.Worksheets[sheetName]).Visible = XlSheetVisibility.xlSheetHidden;
                }
            }
            catch(Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        public void UnhideSheet(string sheetName)
        {
            try
            {
                if (((Worksheet)_App.ActiveWorkbook.Worksheets[sheetName]).Visible != XlSheetVisibility.xlSheetVisible)
                {
                    ((Worksheet) _App.ActiveWorkbook.Worksheets[sheetName]).Visible = XlSheetVisibility.xlSheetVisible;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        /// <summary>
        /// Prompts the user if they want to calculate.  Also performs the calculation.  
        /// </summary>
        /// <param name="sheet">The worksheet to check for calculations.</param>
        /// <param name="showCancelButton">Show the cancel button.</param>
        /// <returns>The button that the user clicked (Yes-No-Cancel).</returns>
        public DialogResult Calculate(Worksheet sheet, bool showCancelButton)
        {
            if (sheet == null)
                throw new NullReferenceException(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.NullWorksheet"));
            try
            {
                //Fix TTN-448 - must check for null view reference.
                if (PafApp.GetViewMngr().CurrentView == null || PafApp.GetViewMngr().CurrentProtectionMngr == null)
                    throw new ArgumentException();

                //Check to see if calculations are pending, if so prompt for calc.
                if (PafApp.GetViewMngr().CurrentProtectionMngr.CalculationsPending())
                {
                    DialogResult res = ShowCalculationsMessage(showCancelButton, sheet.Name);
                    if (res == DialogResult.Yes)
                    {
                        PafApp.GetEventManager().EvaluateView(sheet);
                    }
                    return res;
                }
                else
                    return DialogResult.None;
            }
            catch (ArgumentException)
            {
                return DialogResult.None;
            }
        }

        /// <summary>
        /// Prompts the user if they want to calculate.  Also performs the calculation.  
        /// </summary>
        /// <param name="sheet">The worksheet to check for calculations.</param>
        /// <returns>The button that the user clicked (Yes/No/Cancel).</returns>
        public DialogResult Calculate(Worksheet sheet)
        {
            return Calculate(sheet, true);
        }

        /// <summary>
        /// Prompts the user if they want to calculate.  Also performs the calculation.  
        /// </summary>
        /// <returns>The button that the user clicked (Yes/No/Cancel).</returns>
        public DialogResult Calculate()
        {
            return Calculate((Worksheet)Globals.ThisWorkbook.ActiveSheet, true);
        }

        /// <summary>
        /// Prompts the user if they want to save their pending calculations.  
        /// </summary>
        /// <param name="showWarning">Include a warning as part of the message displayed to the user.</param>
        /// <returns>The button that the user clicked (Yes/No/Cancel).</returns>
        /// <param name="alwaysShowMessage">Always show the message box, even if there is no data to save.</param>
        /// <param name="runSilent">Run the save silently, will not prompt 
        /// the user (this parameter ignores alwaysShowMessage).</param>
        /// <remarks>Added runSilent parameter to fix TTN-973.</remarks>
        public DialogResult SaveWork(bool showWarning, bool alwaysShowMessage, bool runSilent)
        {
            Stopwatch startTime = new Stopwatch();
            // get active sheet
            Worksheet sheet = (Worksheet)Globals.ThisWorkbook.Application.ActiveWorkbook.ActiveSheet;
            DialogResult res = DialogResult.None;
            //update the cell note cache.
            PafApp.GetCellNoteMngr().CopyAndWriteAllUpdatesInsertsDeletsToClientCache(PafApp.GetViewMngr().CurrentView);
            //get the data control.
            PersistToServerSelector pss = new PersistToServerSelector();
            //get the message box buttons.
            frmMessageBox.frmMessageBoxButtons buttons = frmMessageBox.frmMessageBoxButtons.OK_CANCEL;
            //get the message to display.
            string message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.SaveWorkWithWarning");
            //get the is data waiting to be saved.
            bool isDataWaitingToBeSaved = PafApp.GetSaveWorkMngr().DataWaitingToBeSaved;
            isDataWaitingToBeSaved = PafApp.GetCellNoteMngr().IsClientCacheDirty() ? true : isDataWaitingToBeSaved;
            //check for member tags.
            isDataWaitingToBeSaved = PafApp.GetMbrTagMngr().IsMbrTagCacheDirty() ? true : isDataWaitingToBeSaved;
            //
            if(! showWarning)
            {
                buttons = frmMessageBox.frmMessageBoxButtons.YES_NO_CANCEL;
                message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.SaveWork");
            }
 
            if (isDataWaitingToBeSaved || alwaysShowMessage)
            {
                //Fix TTN-973
                if(runSilent)
                {
                    res = DialogResult.OK;
                }
                else 
                {
                    res = PafApp.MessageBox().Show(
                        message,
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SaveChanges"),
                        SystemIcons.Question,
                        true,
                        pss,
                        isDataWaitingToBeSaved,
                        true,
                        buttons);
                }

                startTime = Stopwatch.StartNew();

                if (res == DialogResult.Yes || res == DialogResult.OK)
                {
                    if (pss.SaveEssbaseData.Checked)
                    {
                        PafApp.GetSaveWorkMngr().SaveWork();
                    }

                    if (pss.SaveCellNotes.Checked)
                    {
                        SaveCellNotes(PafApp.GetCellNoteMngr());
                    }

                    if(pss.SaveMbrTags.Checked)
                    {
                        SaveMemberTags(sheet, PafApp.GetViewMngr());
                    }
                }
            }
            startTime.Stop();
            PafApp.GetLogger().Info("Memory Usage: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            PafApp.GetLogger().InfoFormat("SaveWork runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            return res;
        }

        public bool SaveMemberTags(Worksheet sheet, ViewMngr viewMngr)
        {
            string exp2 = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.MemberTagSaveFailed");
            try
            {
                simpleMemberTagData[] addedMbrTags = null;
                simpleMemberTagData[] updatedMbrTags = null;
                simpleMemberTagData[] delMbrTags = null;

                PafApp.GetMbrTagMngr().GetMbrTagData(viewMngr, out addedMbrTags, out updatedMbrTags, out delMbrTags);

                if (!PafApp.GetServiceMngr().SaveMemberTags(addedMbrTags, updatedMbrTags, delMbrTags))
                {
                    throw new Exception(exp2);
                }
                else
                {
                    Globals.ThisWorkbook.Application.ScreenUpdating = false;

                    PafApp.GetMbrTagMngr().ResetBgFillColor(viewMngr);

                    PafApp.GetViewMngr().ResetMemberTagFormatChanges(viewMngr.CurrentView);

                    PafApp.GetViewMngr().MakeAllMemberTagFormattingMngrDirtyExcept(sheet.GetHashCode());

                    PafApp.GetMbrTagMngr().MbrTagCache.Clear();
                }
                return true;
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex.Message, ex);
                throw new Exception(exp2, ex);
            }
            finally
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
            }
        }

        public void SaveCellNotes(CellNoteMngr cellNoteMngr)
        {
            string exp = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.CellNoteSaveFailed");
            try
            {
                //simpleCellNote[] noteToInsert = null;
                if (!PafApp.GetServiceMngr().SaveCellNotes(
                         cellNoteMngr.GetCellNoteInsertBucketForInsert(),
                         cellNoteMngr.GetCellNoteInsertBucketForUpdate(),
                         cellNoteMngr.GetCellNoteInsertBucketForDelete()))
                {
                    throw new Exception(exp);
                }
                else
                {
                    PafApp.GetCellNoteMngr().ClearBuckets();
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex.Message, ex);
                throw new Exception(exp, ex);
            }
        }

        /// <summary>
        /// Shows the RefreshView confirmation dialog box.
        /// </summary>
        /// <param name="sheet">Current view Excel.Worksheet.</param>
        /// <returns>The button(Yes/No) that was clicked by the user.</returns>
        public DialogResult RefreshView(Worksheet sheet)
        {
            if (sheet == null)
                throw new NullReferenceException(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.NullWorksheet"));
            try
            {
                DialogResult res = MessageBox.Show(
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.RefreshView"),
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button2);

                return res;
            }
            catch (ArgumentException)
            {
                return DialogResult.None;
            }
        }

        /// <summary>
        /// Saves the task pane status.
        /// </summary>
        /// <param name="status">Status.</param>
        public void SaveTaskPaneStatus(bool status)
        {
            //Save the current task pane status
            Settings.Default.TaskPaneStatus = status;
            Settings.Default.Save();
            Settings.Default.Reload();

            //TemporaryLogTTN1064("ExcelAppImpl.SaveTaskPaneStatus", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), null, "save task pane status: " + status);
        }


        /// <summary>
        /// Saves the current status of the task pane to disk.
        /// </summary>
        public void SaveTaskPaneCurrentStatus()
        {
            try
            {
                //Save the current task pane status
                SaveTaskPaneStatus(PafApp.GetEventManager().TaskPaneCommandBar.Visible);

                //TemporaryLogTTN1064("ExcelAppImpl.SaveTaskPaneCurrentStatus", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), null, "set task pane status: " + Settings.Default.TaskPaneStatus);
                

            }
            catch(Exception)
            {
                //just catch it...
            }
        }

        //public void TemporaryLogTTN1064(string caller, Worksheet sheet, Exception ex, string message1)
        //{
        //    try
        //    {
        //        PafApp.GetLogger().Error("=======");
        //        if (!String.IsNullOrEmpty(caller))
        //        {
        //            PafApp.GetLogger().ErrorFormat("Function: {0}", new string[] { caller });
        //        }
        //        if (sheet != null)
        //        {
        //            PafApp.GetLogger().ErrorFormat("CurrentTab: {0} ", new string[] { sheet.Name });
        //        }
        //        if (ex != null)
        //        {
        //            PafApp.GetLogger().Error(ex);
        //        }
        //        if (!String.IsNullOrEmpty(message1))
        //        {
        //            PafApp.GetLogger().ErrorFormat("Message: {0}", new string [] {message1});
        //        }
        //        if (LogonInformation != null && !String.IsNullOrEmpty(LogonInformation.UserName))
        //        {
        //            PafApp.GetLogger().ErrorFormat("UserId: {0}", new string[] { LogonInformation.UserName });
        //        }
        //        if (LogonInformation!= null && !String.IsNullOrEmpty(LogonInformation.URL))
        //        {
        //            PafApp.GetLogger().ErrorFormat("URL: {0}", new string[] { LogonInformation.URL });
        //        }
        //        if (RoleSelector != null && !String.IsNullOrEmpty(RoleSelector.UserRole))
        //        {
        //            PafApp.GetLogger().ErrorFormat("Role: {0}", new string[] { RoleSelector.UserRole});
        //        }
        //        if (RoleSelector != null && !String.IsNullOrEmpty(RoleSelector.UserSelectedPlanTypeSpecString))
        //        {
        //            PafApp.GetLogger().ErrorFormat("Season: {0}", new string[] { RoleSelector.UserSelectedPlanTypeSpecString});
        //        }
        //        if (RoleFilterForm != null && RoleFilterForm.roleFilter != null && RoleFilterForm.roleFilter.PafDimSpecs != null)
        //        {
        //            foreach (pafDimSpec dimSpec in RoleFilterForm.roleFilter.PafDimSpecs)
        //            {
        //                if (dimSpec == null) continue;
        //                StringBuilder sb = new StringBuilder();

        //                foreach(string s in dimSpec.expressionList)
        //                {
        //                    if (!String.IsNullOrEmpty(s))
        //                    {
        //                        sb.Append(s + ",");
        //                    }
        //                }
        //                if (!String.IsNullOrEmpty(sb.ToString()))
        //                {
        //                    PafApp.GetLogger().ErrorFormat("Role Filter Dimension: {0}, Expression List: {1} ",
        //                                                   new string[] {dimSpec.dimension, sb.ToString()});
        //                }
        //            }
        //        }
        //        if (PafApp.GetViewMngr() != null && PafApp.GetViewMngr().Views != null)
        //        {
        //            foreach (KeyValuePair<int, ViewStateInfo> views in PafApp.GetViewMngr().Views)
        //            {
        //                if (views.Value.GetProtectionMngr() != null)
        //                {
        //                    PafApp.GetLogger().ErrorFormat(
        //                        "View Name: {0}, IsPlannable: {1}, IsDirty: {2}, Has Calc Pending: {3}, ",
        //                        new string[]
        //                            {
        //                                views.Value.ViewName, views.Value.Plannable.ToString(),
        //                                views.Value.Dirty.ToString(),
        //                                views.Value.GetProtectionMngr().CalculationsPending().ToString()
        //                            });
        //                }
        //            }
        //        }

        //        PafApp.GetLogger().Error("=======");
        //    }
        //    catch (Exception)
        //    {
        //        //just continue...
        //    }
        //}

        /// <summary>
        /// Places an Excel freeze pane on the current worksheet.
        /// </summary>
        /// <param name="cell">Address of the cell to place the freeze pane.</param>
        /// <param name="updateScreen">Turn on Excel Screen updating before setting freeze pane.</param>
        public void SetFreezePane(CellAddress cell, bool updateScreen = true)
        {
            try
            {
                if (Settings.Default.AutoFreezePane)
                {
                    Worksheet sheet = (Worksheet)_App.ActiveSheet;

                    _App.ActiveWindow.FreezePanes = false;
                    

                    if (sheet != null)
                    {
                        //TTN-1289
                        if(updateScreen) _App.ScreenUpdating = true;

                        ((Range)sheet.Cells[cell.Row, cell.Col]).Select();
                        
                        _App.ActiveWindow.FreezePanes = true;
                    }
                }
            }
            finally
            {
                if (updateScreen)  _App.ScreenUpdating = false;
            }
        }

        /// <summary>
        /// Clears any existing Excel freeze panes.
        /// </summary>
        public void ClearExistingFreezePanes()
        {
            try
            {
                _App.ActiveWindow.FreezePanes = false;
            }
            catch 
            {
            }
        }

        /// <summary>
        /// Looks at all the values of an Excel range looking for a non numeric input.
        /// </summary>
        /// <param name="Target">An Excel range</param>
        /// <returns>True if the range has invalid input, false if the range input is ok.</returns>
        public bool RangeHasReplicationCharacters(Range Target)
        {
            try
            {
                ProtectionMngr protectionMngr = PafApp.GetViewMngr().CurrentProtectionMngr;
                foreach (Range cell in Target)
                {
                    if (! ObjectExtension.IsDouble(cell.Value2))
                    {
                        double d;
                        if (PafApp.GetPafPlanSessionResponse().replicateAllEnabled && PafApp.GetPafPlanSessionResponse().replicateEnabled)
                        {
                            if (!protectionMngr.Replication.IsExpressionValidForReplication(cell.Value2, PafAppConstants.REPLICATE_REGEX_STRING, out d))
                            {
                                return true;
                            }
                        }
                        else if (PafApp.GetPafPlanSessionResponse().replicateAllEnabled && !PafApp.GetPafPlanSessionResponse().replicateEnabled)
                        {
                            if (!protectionMngr.Replication.IsExpressionValidForReplication(cell.Value2, PafAppConstants.REPLICATE_REGEX_STRING_RA, out d))
                            {
                                return true;
                            }
                        }
                        else if (!PafApp.GetPafPlanSessionResponse().replicateAllEnabled && PafApp.GetPafPlanSessionResponse().replicateEnabled)
                        {
                            if (!protectionMngr.Replication.IsExpressionValidForReplication(cell.Value2, PafAppConstants.REPLICATE_REGEX_STRING_R, out d))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Looks at all the values of an Excel range looking for a non numeric input.
        /// </summary>
        /// <param name="Target">An Excel range</param>
        /// <returns>True if the range has invalid input, false if the range input is ok.</returns>
        public bool RangeHasLiftCharacters(Range Target)
        {
            try
            {
                ProtectionMngr protectionMngr = PafApp.GetViewMngr().CurrentProtectionMngr;
                foreach (Range cell in Target)
                {
                    if (!ObjectExtension.IsDouble(cell.Value2))
                    {
                        double d;
                        if (PafApp.GetPafPlanSessionResponse().liftAllEnabled && PafApp.GetPafPlanSessionResponse().liftEnabled)
                        {
                            if (!protectionMngr.Lift.IsExpressionValidForReplication(cell.Value2, PafAppConstants.LIFT_REGEX_STRING, out d))
                            {
                                return true;
                            }
                        }
                        else if (PafApp.GetPafPlanSessionResponse().liftAllEnabled && !PafApp.GetPafPlanSessionResponse().liftEnabled)
                        {
                            if (!protectionMngr.Lift.IsExpressionValidForReplication(cell.Value2, PafAppConstants.LIFT_REGEX_STRING_LA, out d))
                            {
                                return true;
                            }
                        }
                        else if (!PafApp.GetPafPlanSessionResponse().liftAllEnabled && PafApp.GetPafPlanSessionResponse().liftEnabled)
                        {
                            if (!protectionMngr.Lift.IsExpressionValidForReplication(cell.Value2, PafAppConstants.LIFT_REGEX_STRING_L, out d))
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception)
            {
                return true;
            }
        }

        /// <summary>
        /// Builds the inital ActionsPane.
        /// </summary>
        public void BuildActionsPane()
        {
            _Pane.BuildActionsPane();
            try
            {
                //if (PafApp.GetPafPlanSessionResponse() == null)
                //{
                //    //Removed to Fix TTN-1064
                //    //SetTaskPaneStatus(false);
                //    TemporaryLogTTN1064("ExcelAppImpl.BuildActionsPane", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), null, "Build Actions Pane, PafPlanSessionResponse is Null (1046)");
                //}
                //else if (! _App.CommandBars[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Toolbar.TaskPane.Name")].Visible)
                if (!PafApp.GetEventManager().TaskPaneCommandBar.Visible)
                {
                    SetTaskPaneStatus(true);
                }
            }
            catch (Exception)
            {
                //TemporaryLogTTN1064("ExcelAppImpl.BuildActionsPane", ((Worksheet)_App.ActiveWorkbook.ActiveSheet), ex, String.Empty);
                //just catch it...
            }
        }

        /// <summary>
        /// Shows the Logon Dialog Box, so the user can logon to Essbase.
        /// </summary>
        /// <returns>True if user is authenicated, false if not.</returns>
        public bool ShowLogonDialogBox()
        {
            NativeWindow mainWindow = new NativeWindow();
            mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

            try
            {

                if (_Logon.IsDisposed)
                {
                    _Logon = new frmLogon();
                    _Logon.UserAuthenicated += new frmLogon.RecievedUserAuthenication(PafApp.GetEventManager().userLogon_UserAuthenicated);

                }

                if (_Logon.ShowDialog(mainWindow) == DialogResult.OK)
                {
                    bool returnValue = _Logon.isUserAuthenaticated;
                    if(_Logon.ClientUpgradeRequired)
                    {
                        _Logon.Hide();
                        _Logon.Close();
                        throw new OutOfDateClientException(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.OutOfDateClient"));
                    }
                    return returnValue;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                mainWindow.ReleaseHandle();
            }
        }

        /// <summary>
        /// Shows the Cluster Dialog Box, so the user can logon to Essbase.
        /// </summary>
        /// <returns>True if user is the user click the ok button, false if not.</returns>
        public bool ShowClusterDialogBox()
        {
            NativeWindow mainWindow = new NativeWindow();
            mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);


            if (ClusterDialog == null)
            {
                ClusterDialog = new frmCreateAsst();
            }


            try
            {
                //return ClusterDialog.ShowDialog(mainWindow) == DialogResult.OK;
                if (ClusterDialog.ShowDialog(mainWindow) == DialogResult.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                mainWindow.ReleaseHandle();
            }
        }

        /// <summary>
        /// Shows the Logon Dialog Box, so the user can logon to Essbase.
        /// </summary>
        /// <returns>True if user is authenicated, false if not.</returns>
        public bool ShowSessionGridLockDialogBox()
        {
            NativeWindow mainWindow = new NativeWindow();
            mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

            if (SessionLockForm == null)
            {
                SessionLockForm = new frmSessionLocks();
            }


            Worksheet sheet = (Worksheet) Globals.ThisWorkbook.ActiveSheet;

            OlapView olapView = PafApp.GetViewMngr().CurrentOlapView;

            Cell targetCell = new Cell(sheet.Application.ActiveCell.Row, sheet.Application.ActiveCell.Column);

            try
            {
                string memberName = olapView.GetHeaderMemberName(targetCell.CellAddress.ToCell());
                Intersection currentSelection = new Intersection(olapView.DimensionPriority);
                if (!String.IsNullOrEmpty(memberName))
                {
                    String dim = olapView.GetMemberDimName(memberName);
                    currentSelection.SetCoordinate(dim, memberName);
                }
                SessionLockForm.CalculationsPending = PafApp.GetViewMngr().AreAnyCalculationsPending();
                SessionLockForm.SimpleDimTrees = PafApp.SimpleDimTrees;
                SessionLockForm.DimensionPriority = olapView.DimensionPriority;
                SessionLockForm.DefaultLockIntersection = currentSelection;

                if (SessionLockForm.ShowDialog(mainWindow) == DialogResult.OK)
                {
                    ViewMngr viewMngr = PafApp.GetViewMngr();

                    //If the session locks are null, 0, or some have been removed,
                    //then we must do a purge.
                    if (SessionLockForm.Locks == null || SessionLockForm.Locks.Count == 0 ||
                        SessionLockForm.IntersectionsRemoved || SessionLockForm.IntersectionsChanged)
                    {
                        viewMngr.GlobalLockMngr.Clear();
                        viewMngr.UpdateGlobalLockMngrs();
                        PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(0);
                        PafApp.GetEventManager().RefreshView(viewMngr.CurrentView.ViewName, sheet, PafApp.GetViewMngr().CurrentView.HasAttributes);
                        if (PafApp.GetTestHarnessManager().IsRecording)
                        {
                            PafApp.GetViewMngr().CurrentProtectionMngr.SessionLockedCell -=
                                new ProtectionMngr.ProtectionMngr_SessionLockedCell(
                                    PafApp.GetEventManager().ExcelEventMgr_SessionLockedCell);

                            //Add the new one.
                            PafApp.GetViewMngr().CurrentProtectionMngr.SessionLockedCell +=
                                new ProtectionMngr.ProtectionMngr_SessionLockedCell(
                                    PafApp.GetEventManager().ExcelEventMgr_SessionLockedCell);
                        }
                    }

                    if (SessionLockForm.Locks != null && SessionLockForm.Locks.Count > 0)
                    {
                        viewMngr.GlobalLockMngr.Clear();
                        foreach (DescendantIntersection di in SessionLockForm.Locks)
                        {
                            viewMngr.GlobalLockMngr.AddUserSessionLocksCoordinateSet(di.Descendants);
                            if (di.Parent != null && di.Parent.Dimensions != null)
                            {
                                viewMngr.GlobalLockMngr.AddUserSessionParentLocksCoordinateSet(di.Parent, di.HasAttributes);
                            }
                        }

                        Globals.ThisWorkbook.Application.ScreenUpdating = false;
                        try
                        {
                            viewMngr.CurrentProtectionMngr.AddSessionLocks(viewMngr.GlobalLockMngr, Convert.ToString(sheet.GetHashCode()), sheet.Name);
                        }
                        finally
                        {
                            Globals.ThisWorkbook.Application.ScreenUpdating = true;
                        }

                        viewMngr.UpdateGlobalLockMngrs();
                        PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(sheet.GetHashCode());
                        if (PafApp.GetViewMngr().CurrentView.HasAttributes)
                        {
                            PafApp.GetEventManager().RefreshView(viewMngr.CurrentView.ViewName, sheet, true);
                        }

                        SetSessionLockButtonStatus(true);
                        PafApp.GetLogger().Info("End Memory: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
                    }
                    else
                    {
                        SetSessionLockButtonStatus(false);
                    }
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                mainWindow.ReleaseHandle();
            }
            return true;
        }

        /// <summary>
        /// Shows the Role Selection Dialog Box.
        /// </summary>
        /// /// <returns>True if user clicked ok, false if cancel.</returns>
        public bool ShowRoleSelectionDialogBox()
        {
            NativeWindow mainWindow = new NativeWindow();
            mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

            try
            {
                if (_Role.ShowDialog(mainWindow) == DialogResult.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                mainWindow.ReleaseHandle();
            }
        }

        /// <summary>
        /// Shows the Role Filter Dialog Box.
        /// </summary>
        /// <returns>True if user clicked ok, false if cancel.</returns>
        public bool ShowRoleFilterDialogBox(bool enableMultiSelect)
        {
            NativeWindow mainWindow = new NativeWindow();
            mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

            try
            {
                _Filter.UserFilteredMultiSelect = enableMultiSelect;
                if (_Filter.ShowDialog(mainWindow) == DialogResult.OK)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            finally
            {
                mainWindow.ReleaseHandle();
            }
        }

        /// <summary>
        /// Sets the position of the Excel Task Pane.
        /// </summary>
        /// <param name="pos">
        /// Enum of the Microsoft.Office.Core.MsoBarPosition.
        /// </param>
        public void PositionTaskPane(MsoBarPosition pos)
        {
            _App.CommandBars[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Toolbar.TaskPane.Name")].Position = pos;
        }

        /// <summary>
        /// Prompts the user if they want to get a new data cache from the server.  
        /// </summary>
        /// <param name="dataSlice">Reference parameter containg the new pafDataSlice, null if no pafDataSlice is returned.</param>
        /// <param name="viewName">Name of the current view.</param>
        /// <returns>The button that the user clicked (Ok-Cancel).</returns>
        public DialogResult ResetDataCache(ref pafDataSlice dataSlice, string viewName)
        {
            DialogResult res = ShowResetDataCacheMessage();
            if (res == DialogResult.OK)
                dataSlice = PafApp.GetSaveWorkMngr().ReloadDataCache(viewName);
            else
                dataSlice = null;

            return res;

        }

        /// <summary>
        /// Prompts the user if they want to get a new data cache from the server.  
        /// </summary>
        /// <returns>The button that the user clicked (Ok/Cancel).</returns>
        public DialogResult ResetDataCache(string viewName)
        {
            DialogResult res = ShowResetDataCacheMessage();
            if (res == DialogResult.OK)
            {

                PafApp.GetSaveWorkMngr().ReloadDataCache(viewName);
            }
            return res;

        }

        public DialogResult UndoCalculation()
        {
            DialogResult result =  UndoCalculationMessage();
            if (result == DialogResult.OK)
            {
                PafApp.UndoCalculation();
            }
            return result;
        }

        /// <summary>
        /// Adds a new worksheet to the active workbook.
        /// </summary>
        /// <param name="sheetName">The name of the new sheet.</param>
        /// <param name="normalizeSheetName">If true, then the sheetName parm will have 
        /// all invalid characters removed before it is set as the sheet name.</param>
        /// <param name="addSheetToBegining">If true, the new sheet will be 
        /// added to the begining of the workbook, if false it is added to the end.</param>
        /// <returns>The worksheet if the sheet was added; null if not.</returns>
        public Worksheet AddNewWorksheet(string sheetName, bool normalizeSheetName, bool addSheetToBegining)
        {
            object missing = Missing.Value;
            object start;
            object end;

            if (addSheetToBegining)
            {
                start = _App.ActiveWorkbook.Sheets[1];
                end = missing;
            }
            else
            {
                start = missing; 
                end = _App.ActiveWorkbook.Sheets[_App.ActiveWorkbook.Sheets.Count];
            }

            try
            {
                Worksheet newSheet = (Worksheet)_App.ActiveWorkbook.Sheets.Add(
                    start,
                    end,
                    missing,
                    XlSheetType.xlWorksheet);
                
                if (normalizeSheetName)
                {
                    string nSheetName = sheetName;
                    nSheetName = MakeSheetNameUnique(nSheetName, true, false);
                    newSheet.Name = nSheetName;
                }
                else
                {
                    newSheet.Name = sheetName;
                }

                return newSheet;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// Adds a new worksheet to the active workbook.
        /// </summary>
        /// <param name="wbk">The workbook to add the sheets to.</param>
        /// <param name="sheetName">The name of the new sheet.</param>
        /// <param name="normalizeSheetName">If true, then the sheetName parm will have 
        /// all invalid characters removed before it is set as the sheet name.</param>
        /// <param name="addSheetToBegining">If true, the new sheet will be 
        /// added to the begining of the workbook, if false it is added to the end.</param>
        /// <returns>The worksheet if the sheet was added; null if not.</returns>
        public Worksheet AddNewWorksheet(Workbook wbk, string sheetName, bool normalizeSheetName, bool addSheetToBegining)
        {
            object missing = Missing.Value;
            object start;
            object end;

            if (addSheetToBegining)
            {
                start = wbk.Sheets[1];
                end = missing;
            }
            else
            {
                start = missing;
                end = wbk.Sheets[_App.ActiveWorkbook.Sheets.Count];
            }

            try
            {
                Worksheet newSheet = (Worksheet)wbk.Sheets.Add(
                    start,
                    end,
                    missing,
                    XlSheetType.xlWorksheet);

                if (normalizeSheetName)
                {
                    string nSheetName = sheetName;
                    nSheetName = MakeSheetNameUnique(nSheetName, true, false);
                    newSheet.Name = nSheetName;
                }
                else
                {
                    newSheet.Name = sheetName;
                }

                return newSheet;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// Rename a worksheet to a new name.
        /// </summary>
        /// <param name="sheet">The worksheet object to rename.</param>
        /// <param name="newName">The new name of the worksheet.</param>
        public void RenameWorksheet(Worksheet sheet, string newName)
        {
            try
            {
                sheet.Name = newName;
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Turns off gridlines, and set the display heading according to the settings files.
        /// </summary>
        /// <param name="sheet">Sheet to perform the actions.</param>
        public void AddOurFormats(Worksheet sheet)
        {
            try
            {
                ActivateSheet(sheet.Name);
                _App.ActiveWindow.DisplayGridlines = false;
                _App.ActiveWindow.DisplayHeadings = Settings.Default.AutoColumnHeadings;
            }
            catch { }

        }

        /// <summary>
        /// Deletes an Excel workbook.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        public void DeleteWorkbook(string path, string fileName)
        {
            string fileExt = VersionAsFloat >= 12 ? PafAppConstants.EXCEL12_FILE_EXTENSION : PafAppConstants.EXCEL_FILE_EXTENSION;
            string excelFile = Path.Combine(path, fileName + fileExt);

            try
            {
                File.Delete(excelFile);
            }
            catch (Exception)
            {
                PafApp.GetLogger().WarnFormat("Cannot delete file: {0}", new []{excelFile});
                throw;
            }
            
        }

        /// <summary>
        /// Creates a new Excel workbook, or opens the workbook if it already exists.
        /// </summary>
        /// <param name="path">Path to the new file.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>The Excel.Workbook object, or null if an error occurs.</returns>
        public Workbook CreateNewWorkBook(string path, string fileName)
        {
            string fileExt;
            //fix issue where test harness creates xlsx workbook, however they can't be reopened by harness.
            //So changed version number of .xlsx file extension to version 16.
            fileExt = VersionAsFloat >= 12 ? PafAppConstants.EXCEL12_FILE_EXTENSION : PafAppConstants.EXCEL_FILE_EXTENSION;

            string excelFile = Path.Combine(path, fileName + fileExt);
            object missing = Missing.Value;

            try
            {
                //Workbook ojbect
                Workbook wbk;
                //Excel applicaiton object.
                Microsoft.Office.Interop.Excel.Application ExcelObj = 
                    new Microsoft.Office.Interop.Excel.Application();

                //If the file does not already exist
                if (!File.Exists(excelFile))
                {
                    //Create the new workbook.
                    wbk = ExcelObj.Workbooks.Add(missing);
                    //Save it.
                    wbk.SaveAs(excelFile, XlFileFormat.xlOpenXMLWorkbook, missing, missing, missing, false,
                        XlSaveAsAccessMode.xlNoChange, missing, missing, missing, missing, missing);
                }
                else
                {
                    //Open the existing workbook
                    wbk = ExcelObj.Workbooks.Open(excelFile, false, false, missing, missing, missing, false, missing,
                        missing, missing, missing, missing, missing, missing, missing);
                }
                //Return it.
                return wbk;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                PafApp.GetLogger().Error(ex);
                return null;
            }
        }

        /// <summary>
        /// Get double click button setting from the registry
        /// </summary>
        /// <param name="refresh">Refresh the status of the registry value.</param>
        /// <returns>Returns true if enable, false if not, or null if not available.</returns>
        public bool? getHSVDoubleClickingButtonSetting(bool refresh)
        {
            if (HSVAddIn != null)
            {
                if (_HSVDoubleClickingButtonSetting == null || refresh)
                {
                    string xml = Registry.GetValue("HKEY_CURRENT_USER\\Software\\Hyperion Solutions\\HyperionSmartView\\Options", "CAOptionsXML", 0).ToString();
                    
                    //If the user hasn't made any changes yet to the SV options, then there will be no CAOPtionsXML reg key
                    if (xml == "0")
                    {
                        return null;
                    }
                    
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(new StringReader(xml));
                    XmlNode node = xmlDoc.SelectSingleNode("//option[@key='useDoubleClickForAdhoc' and @value='1']");

                    if (node != null)
                    {
                        _HSVDoubleClickingButtonSetting = 1;
                    }
                    else
                    {
                        _HSVDoubleClickingButtonSetting = 0;
                    }
                }

                if (_HSVDoubleClickingButtonSetting == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get secondary button setting from the registry
        /// </summary>
        /// <param name="refresh">Refresh the status of the registry value.</param>
        /// <returns>Returns true if enable, false if not, or null if not available.</returns>
        public bool? getEssbaseSecondaryButtonSetting(bool refresh)
        {
            if (EssbaseAddIn != null)
            {
                if (_EssbaseSecondaryButtonSetting == null || refresh)
                {
                    _EssbaseSecondaryButtonSetting =
                        Registry.GetValue("HKEY_CURRENT_USER\\Software\\Hyperion Solutions\\Essbase\\CSL Global Options", "RightMouse", 0);
                }

                if (_EssbaseSecondaryButtonSetting.ToString() == "1")
                {
                    return true;
                }
                else if (_EssbaseSecondaryButtonSetting.ToString() == "0")
                {
                    return false;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Get Essbase double-clicking setting
        /// </summary>
        /// <param name="refresh">Refresh the status of the registry value.</param>
        /// <returns>Returns true if enable, false if not, or null if not available.</returns>
        public bool? getEssbaseDoubleClicking(bool refresh)
        {
            if (EssbaseAddIn != null)
            {
                if (_EssbaseDoubleClicking == null || refresh)
                {
                    _EssbaseDoubleClicking =
                        Registry.GetValue("HKEY_CURRENT_USER\\Software\\Hyperion Solutions\\Essbase\\CSL Global Options", "DoubleClick", 0);
                }

                if (_EssbaseDoubleClicking.ToString() == "1")
                {
                    return true;
                }
                else if (_EssbaseDoubleClicking.ToString() == "0")
                {
                    return false;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Resets the HSV Global Options
        /// SmartView.
        /// </summary>
        public void SetHSVAddInGlobalOptions(bool turnOffAllOptions)
        {
            try
            {
                if (HSVAddIn != null)
                {
                    Worksheet ws = null;
                    bool protectionFlag = false;
                    bool inCopyMode = Globals.ThisWorkbook.Application.CutCopyMode == XlCutCopyMode.xlCopy ? true : false;

                    ws = (Worksheet)_App.ActiveWorkbook.ActiveSheet;

                    //If you are in copy mode, then don't unprotect the sheet.
                    //this will kill the copymode.
                    //TTN-1337
                    if (!inCopyMode && ws.ProtectContents)
                    {
                        //need to unprotect the sheet to set the HSV global options
                        UnprotectSheet(ws, PafAppConstants.SHEET_PASSWORD);
                        protectionFlag = true;
                    }

                    if (turnOffAllOptions)
                    {
                        HypSetGlobalOption(2, false);
                        Globals.ThisWorkbook.Ribbon.SelectOurTab();
                    }
                    else
                    {
                        bool? globalOption = getHSVDoubleClickingButtonSetting(false);
                        HypSetGlobalOption(2, globalOption);
                    }

                    //kmoos - commented out.
                    //reset the protection
                    if (protectionFlag)
                    {
                        ProtectSheet(ws, PafAppConstants.SHEET_PASSWORD);
                    }

                }
            }
            catch
            {
            }
        }

        /// <summary>
        /// Resets the Essbase Global Options
        /// Essbase Add-in
        /// </summary>
        public void SetEssbaseAddInGlobalOptions(bool turnOffAllOptions)
        {
            try
            {
                if (EssbaseAddIn != null)
                {

                    if (turnOffAllOptions)
                    {
                        EssVSetGlobalOption(1, false);
                        EssVSetGlobalOption(2, false);
                    }
                    else
                    {
                        bool? globalOption1 = getEssbaseSecondaryButtonSetting(false);
                        EssVSetGlobalOption(1, globalOption1);

                        bool? globalOption2 = getEssbaseDoubleClicking(false);
                        EssVSetGlobalOption(2, globalOption2);
                    }

                }
            }
            catch
            {
            }
        }


        /// <summary>
        /// Display Essbase AddIn
        /// </summary>
        /// <param name="showAddIn">Display the add in.</param>
        public void ShowHideEssbaseAddIn(bool showAddIn)
        {
            AddIn essbaseAddIn = EssbaseAddIn;

            if (essbaseAddIn != null)
            {
                //Ignore the error if someone removes the Essbase toolbar 
                //when the Essbase addin is installed
                try
                {
                    _App.CommandBars["E&ssbase"].Enabled = showAddIn;
                }
                catch
                {

                }
            }
        }

        /// <summary>
        /// Return a reference to the Essbase Excel Add in (if it exists)
        /// </summary>
        private AddIn EssbaseAddIn
        {
            get
            {
                foreach (AddIn addIn in _App.AddIns)
                {
                    if (addIn.Name.ToUpper() == "ESSEXCLN.XLL")
                    {
                        return addIn;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Display Hyperion Smart View AddIn
        /// </summary>
        /// <param name="showAddIn">Display the add in.</param>
        public void ShowHideHSVAddIn(bool showAddIn)
        {
            if (HSVAddIn != null)
            {
                //Ignore the error if someone removes the HSV toolbar 
                //when the HSV addin is installed
                try
                {
                    _App.CommandBars["Hyperion Smart View For Office"].Enabled = showAddIn;
                }
                catch
                {

                }

                try
                {
                    _App.MenuBars[PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.CommandBars.WorksheetMenuBar")].Menus["H&yperion"].Enabled = showAddIn;
                }
                catch
                {

                }

            }
        }

        /// <summary>
        /// Gets the edit mode status of Excel.
        /// </summary>
        /// <returns>true if Excel is in edit mode, false if not.</returns>
        public bool InEditMode()
        {
            if(_NewButton != null)
            {
                if (!(_NewButton).Enabled)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Set to true if copied or cut data is available
        /// </summary>
        /// <returns>true if in Excel cut/copy mode, false if not.</returns>
        public bool InCutCopyMode()
        {
            if (Globals.ThisWorkbook.Application.CutCopyMode == XlCutCopyMode.xlCopy ||
                Globals.ThisWorkbook.Application.CutCopyMode == XlCutCopyMode.xlCut)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Displays the Excel memory information.
        /// </summary>
        public void DisplayMemory()
        {
            try
            {
                PafApp.GetLogger().Info("Memory Used: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            }
            catch(Exception)
            {
            }
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Adds a new toolbar to Excel.
        /// </summary>
        /// <param name="toolbarName">The new toolbar name.</param>
        /// <returns>The newly created toolbar.</returns>
        private CommandBar AddToolbar(string toolbarName)
        {
            CommandBar toolBar;

            // Create a command bar for the add-in
            object missing = Missing.Value;
            toolBar = _App.CommandBars.
                Add(toolbarName, MsoBarPosition.msoBarTop, missing, true);
            toolBar.Protection = MsoBarProtection.msoBarNoCustomize;
            toolBar.Visible = true;
            return toolBar;
        }

        /// <summary>
        /// Creats a new CommandBarPopup
        /// </summary>
        /// <param name="menubarName">Name of the new bar.</param>
        /// <param name="toolTip">Tooltip associated with the bar.</param>
        /// <returns></returns>
        private CommandBarPopup AddMenubar(string menubarName, string toolTip)
        {
            object missing = Missing.Value;

            CommandBarPopup bar;

            bar = (CommandBarPopup)_App.CommandBars.ActiveMenuBar.Controls.Add(MsoControlType.msoControlPopup,
                missing, missing, _App.CommandBars.ActiveMenuBar.Controls.Count, true);

            bar.Caption = menubarName;
            bar.TooltipText = toolTip;
            //bar.Tag = tag;

            return bar;
        }

        /// <summary>
        /// Remove any invalid characters and checks the lenght of a potential excel sheet name.
        /// </summary>
        /// <param name="sheetName">The potential sheet name.</param>
        /// <returns>The normalized sheet name.</returns>
        private string NormalizeExcelSheetName(string sheetName)
        {
            try
            {
                //Excel only allows 31 chars in the tab name, 
                //so if it's longer then truncate it.
                if (sheetName.Length > PafAppConstants.MAX_EXCEL_SHEET_NAME_LEN)
                    sheetName = sheetName.Substring(0, PafAppConstants.MAX_EXCEL_SHEET_NAME_LEN);
                
                //Confusing but it removes, :, \, /, ? *, ', [, ],
                string str = Regex.Replace(sheetName, @"[:/[\\\?\*\'\]]", "");
                return str;
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return sheetName;
            }

        }

        /// <summary>
        /// Makes a worksheet name unique by adding _x at the end of the sheet name.
        /// </summary>
        /// <param name="sheetName">The name of the sheet to make unique.</param>
        /// <param name="normalizeSheetName">true to normalize the sheet before making it unique.</param>
        /// <param name="searchForNumAtEndOfShtName">true to search the end of the sheetName
        /// for an _x and removing it before trying to make it unique.</param>
        /// <returns>the new unique sheet name.</returns>
        public string MakeSheetNameUnique(string sheetName, bool normalizeSheetName, 
            bool searchForNumAtEndOfShtName)
        {
            string nSheetName;
            
            //normalize the sheet name if we have to.
            if(normalizeSheetName)
            {
                nSheetName = NormalizeExcelSheetName(sheetName);
            }
            else
            {
                nSheetName = sheetName;
            }
       
            //first do a test to see if we have to make it unique, if not then just return...
            if(!DoesSheetExist(nSheetName))
            {
                return nSheetName;
            }


            int startNum = 0;
            if(searchForNumAtEndOfShtName)
            {
                //search the sheet name for a _XX number
                int iPos = nSheetName.LastIndexOf('_');
                if(iPos > 0)
                {
                    //if we find a number or something like it.
                    string s = nSheetName.Substring(iPos + 1);
                    //make sure it's numeric.
                    if (ObjectExtension.IsDouble(s.Trim()))
                    {
                        //parse it and set it as the start number.
                        startNum = int.Parse(s);
                        //and remove it from the sheet name.
                        nSheetName = nSheetName.Substring(0, iPos);
                    }
                }
            }
            else
            {
                //if not then if the lenght if too long, shorten it so we can start to add some numbers...
                if (nSheetName.Length == PafAppConstants.MAX_EXCEL_SHEET_NAME_LEN)
                {
                    nSheetName = nSheetName.Substring(0, nSheetName.Length - 2);
                }
            }

            //now, the hard work...
            int i = startNum;
            string workSheetName;
            do
            {
                workSheetName = nSheetName + "_" + ++i;
            }
            while (DoesSheetExist(workSheetName));


            //now, if the lenght is > 31 chars we must parse it down...
            if(workSheetName.Length > PafAppConstants.MAX_EXCEL_SHEET_NAME_LEN)
            {
                //find the number of chars we have to remove.
                int charsToRemove = workSheetName.Length - PafAppConstants.MAX_EXCEL_SHEET_NAME_LEN;
                //remove the chars from the sheet name.
                string sTemp = nSheetName.Substring(0, nSheetName.Length - charsToRemove);
                //now put the sheet name and the '_' and 'i' variable back together.
                sTemp = sTemp + "_" + i;
                //set it back to the workSheetName varaible so we can return it back...
                workSheetName = sTemp;

                //it's possible that the new name already exists, 
                //so now me must recursively call this,
                //until we find a name.
                if(DoesSheetExist(workSheetName))
                {
                    return MakeSheetNameUnique(workSheetName, false, true);
                }
            }

            //finally return...
            return workSheetName;
        }

        /// <summary>
        /// Gets a button from a command bar.
        /// </summary>
        /// <param name="application">Excel Application</param>
        /// <param name="commandBarName">Name of the excel command bar the contains the control.</param>
        /// <param name="id">Id of the command bar control.</param>
        /// <param name="visible">Search only visible controls.</param>
        /// <param name="recursive">Recursivly search the command bar.</param>
        /// <returns></returns>
        private CommandBarControl GetButton(Microsoft.Office.Interop.Excel.Application application,
            string commandBarName, int id, bool visible, bool recursive)
        {
            object missing = Missing.Value;

            CommandBar wb = application.CommandBars[commandBarName];

            //newButton
            return wb.FindControl(
                missing,
                id,
                missing,
                visible,
                recursive);
        }

        /// <summary>
        /// Makes a new button and adds it to the commandbar.
        /// </summary>
        /// <param name="commandBar">The commandbar to add the button to.</param>
        /// <param name="caption">Caption of the menu item.</param>
        /// <param name="toolTip">The tool tip to display.</param>
        /// <param name="faceID">The face id to add to the menu item.</param>
        /// <param name="beginGroup">Does this item start a new group?</param>
        /// <param name="before">Insert this item before.</param>
        /// <param name="enableButton">Enable this button.</param>
        /// <param name="parameter"></param>
        /// <param name="clickHandler">Clickhandler to handle the click events.</param>
        private CommandBarButton MakeaNewButton(CommandBar commandBar, string caption, string toolTip, int faceID,
            bool beginGroup, int? before, bool enableButton, string parameter, _CommandBarButtonEvents_ClickEventHandler clickHandler)
        {
            object missing = Missing.Value;

            string toolbarName = PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Toolbar.Name");

            CommandBarButton newButton;

            newButton = (CommandBarButton)commandBar.Controls.
                Add(MsoControlType.msoControlButton, 1, missing, before != null ? before: missing, true);
            newButton.Caption = caption;
            newButton.TooltipText = toolTip;
            if (faceID > 0)
            {
                newButton.FaceId = faceID;
            }
            newButton.BeginGroup = beginGroup;
            newButton.Enabled = enableButton;
            newButton.Parameter = parameter;
            newButton.Click += clickHandler;
            if (commandBar.Name == toolbarName)
            {
                _CmdBarControls.Add(newButton);
            }

            return newButton;
        }

        private CommandBarButton MakeaNewButton(CommandBar commandBar, 
            string caption,
            string toolTip, 
            Image image,
            bool beginGroup, 
            int? before, 
            bool enableButton, 
            string parameter, 
            _CommandBarButtonEvents_ClickEventHandler clickHandler)
        {
            object missing = Missing.Value;

            string toolbarName = PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Toolbar.Name");

            CommandBarButton newButton;

            newButton = (CommandBarButton)commandBar.Controls.
                Add(MsoControlType.msoControlButton, 1, missing, before != null ? before : missing, true);
            newButton.Caption = caption;
            newButton.TooltipText = toolTip;
            newButton.Style = MsoButtonStyle.msoButtonIconAndCaption;
            newButton.BeginGroup = beginGroup;
            newButton.Enabled = enableButton;
            newButton.Parameter = parameter;

            //System.Drawing.Bitmap bb = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromFile("C:\\greenlock.png");
            Bitmap bb = (Bitmap)image;
            //bb.MakeTransparent(bb.GetPixel(1, 1));
            Clipboard.SetDataObject(bb, true);
            newButton.PasteFace();
            Clipboard.Clear();

            newButton.Click += clickHandler;
            if (commandBar.Name == toolbarName)
            {
                _CmdBarControls.Add(newButton);
            }

            return newButton;
        }

        /// <summary>
        /// Makes a new button and adds it to the commandbar.
        /// </summary>
        /// <param name="commandBar">The commandbar to add the button to.</param>
        /// <param name="caption">Caption of the menu item.</param>
        /// <param name="toolTip">The tool tip to display.</param>
        /// <param name="faceID">The face id to add to the menu item.</param>
        /// <param name="beginGroup">Does this item start a new group?</param>
        /// <param name="before">Insert this item before.</param>
        /// <param name="enableButton">Enable this button.</param>
        /// <param name="parameter"></param>
        /// <param name="clickHandler">Clickhandler to handle the click events.</param>
        private CommandBarButton MakeaNewButton(CommandBar commandBar, string caption, string toolTip, int faceID,
            bool beginGroup, int? before, bool enableButton, string parameter, _CommandBarButtonEvents_ClickEventHandler[] clickHandler)
        {
            object missing = Missing.Value;

            string toolbarName = PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Excel.Toolbar.Name");

            CommandBarButton newButton;

            newButton = (CommandBarButton)commandBar.Controls.
                Add(MsoControlType.msoControlButton, 1, missing, before != null ? before : missing, true);
            newButton.Caption = caption;
            newButton.TooltipText = toolTip;
            newButton.FaceId = faceID;
            newButton.BeginGroup = beginGroup;
            newButton.Enabled = enableButton;
            newButton.Parameter = parameter;
            foreach (_CommandBarButtonEvents_ClickEventHandler handle in clickHandler)
            {
                newButton.Click += handle;
            }

            if (commandBar.Name == toolbarName)
            {
                _CmdBarControls.Add(newButton);
            }
            return newButton;
        }


        ///<remarks>I added this method after the branch</remarks>
        /// <summary>
        /// Adds a new CommandBarComboBox to the custom MenuBar.
        /// </summary>
        /// <param name="menuBar">The menu bar to add the new menu item to.</param>
        /// <param name="caption">Caption of the menu item.</param>
        /// <param name="items">Combo Box Items</param>
        /// <param name="beginGroup">Does this item start a new group?</param>
        /// <param name="before">Insert this item before.</param>
        /// <param name="enable">Enable this button.</param>
        /// <param name="paramater">Identifies the event handler code fired.</param>
        /// <param name="defaultRuleSetIndex">The index of the default selection.</param>
        /// <param name="clickHandler">Clickhandler to handle the click events.</param>
        /// <param name="width">Width of the drop down.</param>
        private void MakeaNewDropDown(CommandBar menuBar, string caption, List<string> items,
            bool? beginGroup, int? before, bool? enable, string paramater, int defaultRuleSetIndex, int? width,
            _CommandBarComboBoxEvents_ChangeEventHandler clickHandler)
        {
            object missing = Missing.Value;

            CommandBarComboBox newComboBox;

            newComboBox = (CommandBarComboBox)menuBar.Controls.
                Add(MsoControlType.msoControlDropdown, 1, missing, before != null ? before : missing, true);

            if (caption != null)
            {
                newComboBox.Caption = caption;
            }

            foreach (string item in items)
            {
                newComboBox.AddItem(item, missing);
            }

            newComboBox.ListIndex = defaultRuleSetIndex;

            if (beginGroup != null)
            {
                newComboBox.BeginGroup = beginGroup.Value;
            }

            if (enable != null)
            {
                newComboBox.Enabled = enable.Value;
            }

            if (paramater != null)
            {
                newComboBox.Parameter = paramater;
            }

            if (width != null)
            {
                newComboBox.Width = width.Value;
            }

            newComboBox.Change += clickHandler;

            _MenuBarControls.Add(newComboBox);
        }

                ///<remarks>I renamed this method after the branch.  Was MakeaNewItem</remarks>
        /// <summary>
        /// Adds a new CommandBarButton to the custom MenuBar.
        /// </summary>
        /// <param name="menuBar">The menu bar to add the new menu item to.</param>
        /// <param name="caption">Caption of the menu item.</param>
        /// <param name="faceID">The face id to add to the menu item.</param>
        /// <param name="beginGroup">Does this item start a new group?</param>
        /// <param name="before">Insert this item before.</param>
        /// <param name="enableButton">Enable this button.</param>
        /// <param name="paramater">Identifies the event handler code fired.</param>
        /// <param name="action">The customMenuDef key</param>
        /// <param name="clickHandler">Clickhandler to handle the click events.</param>
        private void MakeaNewMenuButton(CommandBarPopup menuBar, string caption, int? faceID,
            bool? beginGroup, int? before, bool? enableButton, string paramater, string action,
            _CommandBarButtonEvents_ClickEventHandler clickHandler)
        {
            MakeaNewMenuButton(menuBar, caption, faceID, beginGroup, before, enableButton,
                paramater, action, clickHandler, null);
        }

        ///<remarks>I renamed this method after the branch.  Was MakeaNewItem</remarks>
        /// <summary>
        /// Adds a new CommandBarButton to the custom MenuBar.
        /// </summary>
        /// <param name="menuBar">The menu bar to add the new menu item to.</param>
        /// <param name="caption">Caption of the menu item.</param>
        /// <param name="faceID">The face id to add to the menu item.</param>
        /// <param name="beginGroup">Does this item start a new group?</param>
        /// <param name="before">Insert this item before.</param>
        /// <param name="enableButton">Enable this button.</param>
        /// <param name="paramater">Identifies the event handler code fired.</param>
        /// <param name="action">The customMenuDef key</param>
        /// <param name="clickHandler">Clickhandler to handle the click events.</param>
        /// <param name="shortCutString">The short cut key string</param>
        private void MakeaNewMenuButton(CommandBarPopup menuBar, string caption, int? faceID,
            bool? beginGroup, int? before, bool? enableButton, string paramater, string action,
            _CommandBarButtonEvents_ClickEventHandler clickHandler, string shortCutString)
        {

            object missing = Missing.Value;

            CommandBarButton newButton;

            newButton = (CommandBarButton)menuBar.Controls.
                Add(MsoControlType.msoControlButton, 1, missing, before != null ? before : missing, true);

            if (caption != null)
            {
                newButton.Caption = caption;
            }
            if (faceID != null)
            {
                newButton.FaceId = faceID.Value;
            }
            if (beginGroup != null)
            {
                newButton.BeginGroup = beginGroup.Value;
            }
            if (enableButton != null)
            {
                newButton.Enabled = enableButton.Value;
            }
            if (paramater != null)
            {
                newButton.Parameter = paramater;
            }
            if (! String.IsNullOrEmpty(shortCutString))
            {
                newButton.ShortcutText = shortCutString;
            }
            //Multiple buttons with the same tag cause the button click event to be fired multiple times.
            //I chose the descriptionText property to hold the action because it seemed the best substitute.
            if (action != null)
            {
                newButton.DescriptionText = action;
            }

            newButton.Click += clickHandler;

            _MenuBarControls.Add(newButton);
        }


        ///<remarks>I renamed this method after the branch.  Was MakeaNewItem</remarks>
        /// <summary>
        /// Adds a new CommandBarButton to the custom MenuBar.
        /// </summary>
        /// <param name="menuBar">The menu bar to add the new menu item to.</param>
        /// <param name="caption">Caption of the menu item.</param>
        /// <param name="faceID">The face id to add to the menu item.</param>
        /// <param name="beginGroup">Does this item start a new group?</param>
        /// <param name="before">Insert this item before.</param>
        /// <param name="enableButton">Enable this button.</param>
        /// <param name="paramater">Identifies the event handler code fired.</param>
        /// <param name="tag">The customMenuDef key</param>
        /// <param name="clickHandler">Clickhandler to handle the click events.</param>
        /// <param name="shortCutString">The short cut key string</param>
        private void MakeaNewMenuButton(CommandBarPopup menuBar, string caption, int? faceID,
            bool? beginGroup, int? before, bool? enableButton, string paramater,
            string tag, _CommandBarButtonEvents_ClickEventHandler[] clickHandler, string shortCutString)
        {

            object missing = Missing.Value;

            CommandBarButton newButton;

            newButton = (CommandBarButton)menuBar.Controls.
                Add(MsoControlType.msoControlButton, 1, missing, before != null ? before : missing, true);

            if (caption != null)
            {
                newButton.Caption = caption;
            }
            if (faceID != null)
            {
                newButton.FaceId = faceID.Value;
            }
            if (beginGroup != null)
            {
                newButton.BeginGroup = beginGroup.Value;
            }
            if (enableButton != null)
            {
                newButton.Enabled = enableButton.Value;
            }
            if (paramater != null)
            {
                newButton.Parameter = paramater;
            }
            if (tag != null)
            {
                newButton.Tag = tag;
            }
            if (!String.IsNullOrEmpty(shortCutString))
            {
                newButton.ShortcutText = shortCutString;
            }
            foreach (_CommandBarButtonEvents_ClickEventHandler handle in clickHandler)
            {
                newButton.Click += handle;
            }

            _MenuBarControls.Add(newButton);
        }

        /// <summary>
        /// Prompts the user if they want to calculate.
        /// </summary>
        /// <returns>The button that the user clicked (Yes/No/Cancel)</returns>
        private DialogResult ShowCalculationsMessage(bool showCancelButton, string sheet)
        {
            MessageBoxButtons but;
            if (showCancelButton)
                but = MessageBoxButtons.YesNoCancel;
            else
                but = MessageBoxButtons.YesNo;

            return MessageBox.Show(
                String.Format(
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.SaveCalculations"), sheet),
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                but,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button1);
        }

        /// <summary>
        /// Prompts the user if they want to get a new data cache from the server.
        /// </summary>
        /// <returns>The button that the user clicked (Ok/Cancel)</returns>
        private DialogResult ShowResetDataCacheMessage()
        {
            return MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ResetDataCache"),
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);
        }

        /// <summary>
        /// Prompts the user if they want to undo the last calculation.
        /// </summary>
        /// <returns>The button that the user clicked (Ok/Cancel)</returns>
        private DialogResult UndoCalculationMessage()
        {
            return MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.UndoCalculation"),
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Exclamation,
                MessageBoxDefaultButton.Button2);
        }

        /// <summary>
        /// Return a reference to the Essbase Excel Add in (if it exists)
        /// </summary>
        private AddIn HSVAddIn
        {
            get
            {
                foreach (AddIn addIn in _App.AddIns)
                {
                    if (addIn.Name.ToUpper() == "HSTBAR.XLA")
                    {
                        if (addIn.Installed)
                        {
                            return addIn;
                        }
                    }
                }
                return null;
            }
        }

        #endregion Private Methods

        #region Public Properties
        /// <summary>
        /// Gets the RightClick CommandBar object.
        /// </summary>
        public CommandBar RightClickMenu
        {
            [DebuggerHidden]
            get { return _RightClickMenu; }
        }

        /// <summary>
        /// Returns the UserLogon control.
        /// </summary>
        public frmLogon LogonInformation
        {
            [DebuggerHidden]
            get { return _Logon; }
        }

        /// <summary>
        /// Returns the RoleSelector control
        /// </summary>
        public RoleSelection RoleSelector
        {
            [DebuggerHidden]
            get { return _Role.RoleSelector; }
        }

        /// <summary>
        /// Role selection form.
        /// </summary>
        public frmRoleSelection RoleSelectionForm
        {
            [DebuggerHidden]
            get { return _Role; }
        }

        /// <summary>
        /// Returns the SessionLock form
        ///  </summary>
        public frmSessionLocks SessionLockForm { get; set; }

        /// <summary>
        /// Returns the ClusterDialog form
        ///  </summary>
        public frmCreateAsst ClusterDialog { get; set; }

        /// <summary>
        /// Returns the RoleFilter control
        /// </summary>
        public RoleFilter RoleFilter
        {
            [DebuggerHidden]
            get { return _Filter.roleFilter; }
        }

        /// <summary>
        /// Role Filter form.
        /// </summary>
        public frmRoleFilter RoleFilterForm
        {
            [DebuggerHidden]
            get { return _Filter; }
        }

        /// <summary>
        /// Returns the ExcelActionsPane.
        /// </summary>
        public ExcelActionsPane ActionsPane
        {
            [DebuggerHidden]
            get { return _Pane; }
        }

        /// <summary>
        /// Returns the toolbar CommandBar object.
        /// </summary>
        public CommandBar ViewToolBar
        {
            [DebuggerHidden]
			get { return _ViewToolBar; }
        }                                                                                                                        

        /// <summary>
        /// Returns the MenuBar CommandBarControl.
        /// </summary>
        public CommandBarControl ViewMenu
        {
            [DebuggerHidden]
            get { return _MenuBar; }
        }

        /// <summary>
        /// Returns a list of hash codes for the sheets.
        /// </summary>
        public List<int> Sheets
        {
            get 
            {
                //Build a list of sheets, then compare them to the GridMngr, to see if any have been deleted.
                List<int> allSheets = new List<int>(Globals.ThisWorkbook.Sheets.Count);
                //Move the sheet names to an array, and send it to the viewmngr to recon.
                for (int i = 1; i <= Globals.ThisWorkbook.Sheets.Count; i++)
                {
                    allSheets.Add(Globals.ThisWorkbook.Sheets[i].GetHashCode());
                }
                return allSheets;
            }
        }

        /// <summary>
        /// Gets the application version.
        /// </summary>
        public string Version
        {
            get { return _App.Name + " " + PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Message.Version") +": " +  _App.Version; }
        }

        /// <summary>
        /// Gets the application version as an float.
        /// </summary>
        public float VersionAsFloat
        {
            get
            {
                return float.Parse(_App.Version);
            }
        }

        #endregion Public Properties
    }
}
