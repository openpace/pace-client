#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Management;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;
using Titan.Pace.DataStructures;

namespace Titan.Pace.Application.Utilities
{
    internal static class Security
    {
        /// <summary>
        /// Encryptes the information in a PafUser object.
        /// </summary>
        /// <param name="pafUser">The paf user object to encrypt.</param>
        /// <returns>a PafLogonInfo object, or null.</returns>
        public static PafLogonInfo EncryptUser(PafLogonInfo pafUser)
        {
            try
            {
                PafLogonInfo user;
                if (pafUser == null)
                {
                    return null;
                }
                else
                {
                    user = pafUser.DeepClone();
                }

                RijndaelManaged myRijndael = new RijndaelManaged();
                myRijndael.Mode = CipherMode.CBC;
                myRijndael.Padding = PaddingMode.PKCS7;
                myRijndael.KeySize = 128;
                myRijndael.BlockSize = 128;
               
                byte[] key;
                byte[] iv;

                Random randy = new Random();
                String IV = randy.NextDouble().ToString();

                //create the key
                byte[] kBytes = Encoding.UTF8.GetBytes(PafAppConstants.SERVER_LOGON_SPEC);
                key = new byte[16];
                int len = kBytes.Length;
                if (len > key.Length)
                {
                    len = key.Length;
                }
                Array.Copy(kBytes, key, len);

                //create the IV
                byte[] iBytes = Encoding.UTF8.GetBytes(IV);
                iv = new byte[16];
                len = iBytes.Length;
                if (len > iv.Length)
                {
                    len = iv.Length;
                }
                Array.Copy(iBytes, iv, len);

                //creates encryptor
                ICryptoTransform transform = myRijndael.CreateEncryptor(key, iv);

                if (!String.IsNullOrEmpty(user.UserPassword))
                {
                    byte[] plainText = Encoding.UTF8.GetBytes(user.UserPassword);
                    byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);
                    user.UserPassword = Convert.ToBase64String(cipherBytes);
                }

                if (!String.IsNullOrEmpty(user.UserName))
                {
                    byte[] plainText = Encoding.UTF8.GetBytes(user.UserName);
                    byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);
                    user.UserName = Convert.ToBase64String(cipherBytes);
                }

                if (!String.IsNullOrEmpty(user.UserSID))
                {
                    byte[] plainText = Encoding.UTF8.GetBytes(user.UserSID);
                    byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);
                    user.UserSID = Convert.ToBase64String(cipherBytes);
                }

                if (!String.IsNullOrEmpty(user.UserDomain))
                {
                    byte[] plainText = Encoding.UTF8.GetBytes(user.UserDomain);
                    byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);
                    user.UserDomain = Convert.ToBase64String(cipherBytes);
                }

                user.IV = IV;

                return user;

            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
                throw;
            }
        }

        ///// <summary>
        ///// Encryptes the information in a PafUser object.
        ///// </summary>
        ///// <returns>a PafLogonInfo object, or null.</returns>
        //private static string EncryptString(string text)
        //{
        //    try
        //    {
        //        RijndaelManaged myRijndael = new RijndaelManaged();
        //        myRijndael.Mode = CipherMode.CBC;
        //        myRijndael.Padding = PaddingMode.PKCS7;
        //        myRijndael.KeySize = 128;
        //        myRijndael.BlockSize = 128;

        //        string encText = String.Empty;

        //        byte[] key;
        //        byte[] iv;

        //        Random randy = new Random();
        //        String IV = randy.NextDouble().ToString();

        //        //create the key
        //        byte[] kBytes = Encoding.UTF8.GetBytes(PafAppConstants.SERVER_LOGON_SPEC);
        //        key = new byte[16];
        //        int len = kBytes.Length;
        //        if (len > key.Length)
        //        {
        //            len = key.Length;
        //        }
        //        Array.Copy(kBytes, key, len);

        //        ////create the IV
        //        //byte[] iBytes = Encoding.UTF8.GetBytes(IV);
        //        //iv = new byte[16];
        //        //len = iBytes.Length;
        //        //if (len > iv.Length)
        //        //{
        //        //    len = iv.Length;
        //        //}
        //        //Array.Copy(iBytes, iv, len);


        //        //myRijndael.GenerateIV();

        //        myRijndael.Key = key;
        //        myRijndael.IV = iv;


        //        //creates encryptor
        //        ICryptoTransform transform = myRijndael.CreateEncryptor(key, iv);

        //        if (!String.IsNullOrEmpty(text))
        //        {
        //            byte[] plainText = Encoding.UTF8.GetBytes(text);
        //            byte[] cipherBytes = transform.TransformFinalBlock(plainText, 0, plainText.Length);
        //            encText = Convert.ToBase64String(cipherBytes);
        //        }

                
        //        //Debug.WriteLine("Encrypted Text: " + encText);

        //        ////string decText = DecriptString(encText, IV);

        //        //Debug.WriteLine("Decrypted Text: " + decText);

        //        return encText;

        //    }
        //    catch (Exception ex)
        //    {
        //        PafApp.GetLogger().Error(ex);
        //        throw;
        //    }
        //}

        //private static string DecriptString(string text, string IV)
        //{
        //    try
        //    {
        //        RijndaelManaged myRijndael = new RijndaelManaged();
        //        myRijndael.Mode = CipherMode.CBC;
        //        myRijndael.Padding = PaddingMode.PKCS7;
        //        myRijndael.KeySize = 128;
        //        myRijndael.BlockSize = 128;

        //        string decText;

        //        byte[] key;
        //        byte[] iv;

        //        //create the key
        //        byte[] kBytes = Encoding.UTF8.GetBytes(PafAppConstants.SERVER_LOGON_SPEC);
        //        key = new byte[16];
        //        int len = kBytes.Length;
        //        if (len > key.Length)
        //        {
        //            len = key.Length;
        //        }
        //        Array.Copy(kBytes, key, len);

        //        //create the IV
        //        byte[] iBytes = Encoding.UTF8.GetBytes(IV);
        //        iv = new byte[16];
        //        len = iBytes.Length;
        //        if (len > iv.Length)
        //        {
        //            len = iv.Length;
        //        }
        //        Array.Copy(iBytes, iv, len);

        //        //creates deccryptor
        //        ICryptoTransform transform = myRijndael.CreateDecryptor(key, iv);
        //        //decrypt
        //        byte[] encryptedData = Convert.FromBase64String(text);
        //        byte[] plainText = transform.TransformFinalBlock(encryptedData, 0, encryptedData.Length);
        //        decText = Encoding.UTF8.GetString(plainText);

        //        return decText;

        //    }
        //    catch (Exception ex)
        //    {
        //        PafApp.GetLogger().Error(ex);
        //        throw;
        //    }
        //}


        /// <summary>
        /// Gets the domain informaion for the current windows user.
        /// </summary>
        /// <param name="userNameWithDomain">the user id.</param>
        /// <param name="returnMachineDomain">if the domain name cannot be found in the user name
        /// should the machine domin be returned.</param>
        /// <returns>the users domain, or the machine domain.</returns>
        public static string GetUsersDomain(string userNameWithDomain, bool returnMachineDomain)
        {
            return ParseDomainName(userNameWithDomain, returnMachineDomain);
        }

        /// <summary>
        /// Gets the users name for the current windows user.
        /// </summary>
        /// <param name="includeDomain">Include the domain as part of the users name.</param>
        /// <returns>the users name.</returns>
        public static string GetUserName(bool includeDomain)
        {
            WindowsIdentity wi = WindowsIdentity.GetCurrent();

            //don't even look to see if there is a domain the caller wants it, so return it.
            if(includeDomain)
            {
                return wi.Name;
            }

            return ParseUserName(wi.Name);
        }

        /// <summary>
        /// Gets the users SID.
        /// </summary>
        /// <returns>the users SID.</returns>
        public static string GetUserSID()
        {
            WindowsIdentity wi = WindowsIdentity.GetCurrent();

            if (wi != null)
            {
                return wi.User.Value;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Parses the domain name off the user login name.
        /// </summary>
        /// <param name="userNameWithDomain">login name with the domain.</param>
        /// <returns>the login name.</returns>
        private static string ParseUserName(string userNameWithDomain)
        {
            string userName = String.Empty;

            int pos;
            pos = userNameWithDomain.IndexOf(PafAppConstants.ASTERISK, StringComparison.CurrentCultureIgnoreCase);
            if (pos > -1)
            {
                //userName = userNameWithDomain.Substring(0, userNameWithDomain.Length - PafAppConstants.ASTERISK.Length - pos - 1);
                userName = userNameWithDomain.Substring(0, pos);
            }
            else
            {
                pos = userNameWithDomain.IndexOf(PafAppConstants.FOWARD_SLASH, StringComparison.CurrentCultureIgnoreCase);
                if (pos > -1)
                {
                    userName = userNameWithDomain.Substring(pos + 1);
                }
            }
            return userName;
        }

        /// <summary>
        /// Parses the domain name off the user login name.
        /// </summary>
        /// <param name="userNameWithDomain">login name with the domain.</param>
        /// <param name="returnMachineDomain">if the domain name cannot be found in the user name
        /// should the machine domin be returned.</param>
        /// <returns>the domain name.</returns>
        private static string ParseDomainName(string userNameWithDomain, bool returnMachineDomain)
        {
            string domain = String.Empty;
            int pos;
            pos = userNameWithDomain.IndexOf(PafAppConstants.FOWARD_SLASH, StringComparison.CurrentCultureIgnoreCase);
            if (pos > -1)
            {
                domain = userNameWithDomain.Substring(0, pos);
            }
            else
            {
                pos = userNameWithDomain.LastIndexOf(PafAppConstants.ASTERISK, StringComparison.CurrentCultureIgnoreCase);
                if (pos > -1)
                {
                    domain = userNameWithDomain.Substring(pos + 1);
                }
            }

            try
            {
                if (String.IsNullOrEmpty(domain) && returnMachineDomain)
                {
                    ManagementObject cs;
                    using (cs = new ManagementObject("Win32_ComputerSystem.Name='" + Environment.MachineName + "'"))
                    {
                        cs.Get();
                        domain = cs["domain"].ToString();
                    }
                }
            }
            catch (Exception e)
            {
                PafApp.GetLogger().Warn("Problem getting user domain.", e);
                domain = String.Empty;
            }

            return domain;
        }
    }
}