#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Drawing.Printing;

namespace Titan.Pace.Application.Utilities
{
    /// <summary>
    /// Static class to hold general utility functions.
    /// </summary>
    internal static class Utility
    {
        /// <summary>
        /// Checks the system to see if a default system printer is installed.
        /// </summary>
        /// <returns>True if the printer is installed, false if no printer is installed.</returns>
        public static bool CheckForDefaultPrinter()
        {
            try
            {
                PrintDocument prtdoc = new PrintDocument();

                string defaultPrinter = prtdoc.PrinterSettings.PrinterName;

                if (String.IsNullOrEmpty(defaultPrinter))
                    return false;
                else
                    return true;
            }
            catch (NullReferenceException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        ///// <summary>
        ///// Inserts a string inside of another string at the first found occurence of stringToFind in stringToSearch.
        ///// </summary>
        ///// <param name="stringToFind">The string to find.</param>
        ///// <param name="stringToSearch">The string to search.</param>
        ///// <param name="insertionString">The string to insert.</param>
        //public static string StringInserter(string stringToFind, string stringToSearch, string insertionString)
        //{
        //    int pos = stringToSearch.IndexOf(stringToFind);
        //    if (pos > 0)
        //    {
        //        return stringToSearch.Insert(pos, insertionString);
        //    }
        //    else
        //    {
        //        return stringToSearch;
        //    }
        //}

        /// <summary>
        ///// Removes all the formating from the cell text minus any commas, replication information, or decimal points.
        ///// </summary>
        ///// <param name="cellText">The text of the cell, which contains the numeric format.</param>
        ///// <param name="cellFormat">The numeric format of the cell.</param>
        ///// <param name="replicationRegExStr">The regular expression string for the replication keyboard shortcuts (R or RA currently)</param>
        ///// <param name="symbolsNotToRemove">regex string of symbols not to remove</param>
        ///// <returns>The cell minus the commas, replication information, or decimal points.</returns>
        ///// <remarks>TTN-871</remarks>
        //public static string RemoveFormatInfoFromCell1(string cellText, string cellFormat, string replicationRegExStr, string symbolsNotToRemove)
        //{
        //    string returnString = cellText;
        //    Regex reggie = new Regex(symbolsNotToRemove, RegexOptions.IgnoreCase);
        //    Regex r2 = new Regex(replicationRegExStr, RegexOptions.IgnoreCase);
        //    foreach(char c in cellFormat)
        //    {
        //        if (!c.IsDouble())
        //        {
        //            if (!reggie.IsMatch(c.ToString()) && !r2.IsMatch(c.ToString()))
        //            {
        //                returnString = returnString.Replace(c.ToString(), String.Empty);
        //            }
        //        }
        //    }
        //    return returnString.Trim();
        //}

        ///// <summary>
        ///// Parses the user entered value, to determine if its valid entry.
        ///// </summary>
        ///// <param name="userEnteredValue">The string value enterd by the user.</param>
        ///// <param name="regularExpression">The regular expression used to parse the user entered value.</param>
        ///// <param name="value">The valid numeric value that is to be replicated.</param>
        ///// <returns>true if the value is value, false if not.</returns>
        //public static bool isExpressionValidForReplication(string userEnteredValue, string regularExpression, out double value)
        //{
        //    bool ret = false;
        //    bool containPercent = false;
        //    value = 0;

        //    Regex rLead = new Regex(
        //        PafApp.GetLocalization().GetResourceManager().GetString("Application.Replication.Symbols.Leading.Cells"),
        //        RegexOptions.IgnoreCase);

        //    if (userEnteredValue.Substring(0, 1).IsDouble() || rLead.IsMatch(userEnteredValue.Substring(0, 1)))
        //    {
        //        Regex reggie = new Regex(regularExpression, RegexOptions.IgnoreCase);
        //        //remove any extra stuff from the user entered value...
        //        Regex reggie2 = new Regex(@"\%", RegexOptions.IgnoreCase);

        //        if (reggie.IsMatch(userEnteredValue))
        //        {
        //            //replace the regular expression with an empty string
        //            string s = reggie.Replace(userEnteredValue, String.Empty, 1);

        //            //see if the string contains a percentage.
        //            if (reggie2.IsMatch(s))
        //            {
        //                containPercent = true;
        //                s = reggie2.Replace(s, String.Empty, 1);
        //            }

        //            if (s.Trim().IsDouble())
        //            {
        //                if (!containPercent)
        //                {
        //                    value = Double.Parse(s.Trim());
        //                }
        //                else
        //                {
        //                    value = (Double.Parse(s.Trim()) / 100);
        //                }
        //                ret = true;
        //            }
        //        }
        //    }
        //    return ret;
        //}

        ///// <summary>
        ///// Parses the user entered value, to determine if its valid entry.
        ///// </summary>
        ///// <param name="userEnteredValue">The string value enterd by the user.</param>
        ///// <param name="regularExpression">The regular expression used to parse the user entered value.</param>
        ///// <param name="value">The valid numeric value that is to be replicated.</param>
        ///// <returns>true if the value is value, false if not.</returns>
        //public static bool isExpressionValidForReplication(object userEnteredValue, string regularExpression, out double value)
        //{
        //    return isExpressionValidForReplication(userEnteredValue.ToString(), regularExpression, out value);
        //}

    }
}
