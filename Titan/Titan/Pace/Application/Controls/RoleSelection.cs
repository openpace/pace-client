#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Titan.PafService;
using Titan.Properties;

namespace Titan.Pace.Application.Controls
{
    internal partial class RoleSelection : UserControl
    {
        #region Variables
        private const int PAD = 5;
        public event RoleSelection_RoleChanged RoleChanged;
        public delegate void RoleSelection_RoleChanged(object sender, LogonEventArgs e, ref bool Cancel);

        public event RoleSelection_SeasonProcessChanged SeasonProcessChanged;
        public delegate void RoleSelection_SeasonProcessChanged(object sender, LogonEventArgs e, ref bool Cancel);

        // Events related to user changes in 'FilteredSubtotals' or 'Suppress Invalid Intersections' option (TTN-2656)
        public event RoleSelection_FilteredSubtotalsOrSupInvIxChanged FilteredSubtotalsOrSupInvIxChanged;
        public delegate void RoleSelection_FilteredSubtotalsOrSupInvIxChanged(object sender, LogonEventArgs e, ref bool Cancel);

        private string currentUser = String.Empty;

        private pafPlannerRole[] _roles;
        private pafPlannerConfig[] _plannerConfigs;

        //role, PafPlanerConfig
        private Dictionary<string,  List<pafPlannerConfig>> _pafPlannerConfigs;

        //properties
        private bool _SuppressInvalidInx = false;
        private pafPlannerRole _CurrentPafPlannerRole = null;
        private season _CurrentPafSeason = null;
        private pafPlannerConfig _CurrentPafPlannerConfig = null;
        private string _SelectedUserRole = String.Empty;
        private string _UserSelectedUserRole = String.Empty;
        private string _planTypeSpecString = String.Empty;
        private string _UserPlanTypeSpecString = String.Empty;
        private HashSet<String> _discontigRoles;
        private bool _extraControlsVisible = false;
        private bool _FilteredSubtotalsEnabled = false;         // TTN-2656
        private bool _SupInvInxEnabled = false;                 // TTN-2656
        #endregion Variables

        #region Constructor & Control Load
        /// <summary>
        /// Constructor.
        /// </summary>
        public RoleSelection()
        {
            try
            {
                _pafPlannerConfigs = new Dictionary<string, List<pafPlannerConfig>>();
                InitializeComponent();
                loadUi();
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Resets the controls.
        /// </summary>
        public void ClearControls() 
        {
            try
            {
                lstSeasonProc.Items.Clear();
                cboRole.ResetText();
                cboRole.Items.Clear();
                cmdOk.Enabled = false;
            }
            catch (Exception) { }
        }

        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                //Get the labels from the localized .resx file.
                Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                lblRoleSelection.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleSelection.RoleSelectionLabel");
                lblDims.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleSelection.ProcessSeasonLabel");
                chkSupInvIx.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.InvalidInx");
                chkFilteredTotals.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.FilteredSubtotals");

                toolTip.SetToolTip(lstSeasonProc, 
                    PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Controls.ToolTips.RoleSelection.SeasonProcess"));
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Control load event handler.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Eventargs</param>
        private void RoleSelection_Load(object sender, EventArgs e)
        {
            _SelectedUserRole = Settings.Default.LastUserRole;
            _planTypeSpecString = Settings.Default.LastUserProcess;
            _FilteredSubtotalsEnabled = Settings.Default.FilteredTotals;
            _SupInvInxEnabled = Settings.Default.SuppressInvIx;
        }
        #endregion Constructor & Control Load

        #region Methods
        /// <summary>
        /// Gets a plan type if one does not exist.
        /// </summary>
        private void getClientAuth()
        {
            _discontigRoles = new HashSet<string>();
            _extraControlsVisible = false;
            pafAuthResponse pafAuthResp = PafApp.GetPafAuthResponse();
            _roles = pafAuthResp.plannerRoles;
            _plannerConfigs = pafAuthResp.plannerConfigs;

            //Build a HashSet of discontig roles.
            if (pafAuthResp.discontigRoles != null && pafAuthResp.discontigRoles.Length > 0)
            {
                _discontigRoles.UnionWith(pafAuthResp.discontigRoles);
            }
            //_discontigRoles.Add("Division Planner");
            //Check all the Paf Planner Configs.  If we have one congig where the two checkboxs would be shown,
            //Then they must be visible for all the configs.  This is so the list box dosen't expand and collapse as the 
            //Roles change.  
            foreach (pafPlannerConfig pafPlannerConfig in _plannerConfigs.Where(pafPlannerConfig => !pafPlannerConfig.isUserFilteredUow))
            {
                if (pafPlannerConfig.isFilteredSubtotals && _discontigRoles.Contains(pafPlannerConfig.role))
                {
                    PafApp.GetLogger().InfoFormat("Planner Config || Role: {0}, Cycle: {1}, has filtered subtotals, and discontiguous security, so extra control will always be visible.", new object[] { pafPlannerConfig.role, pafPlannerConfig.cycle });
                    _extraControlsVisible = true;
                    break;
                }
                if (!pafPlannerConfig.isDataFilteredUow) continue;
                PafApp.GetLogger().InfoFormat("Planner Config || Role: {0}, Cycle: {1}, has suppress invalid intersections, so extra control will always be visible.", new object[] { pafPlannerConfig.role, pafPlannerConfig.cycle });
                _extraControlsVisible = true;
                break;
            }
        }

        /// <summary>
        /// Resets the current pafPlannerRole and currentpafseason, when the currentUser changes.
        /// </summary>
        private void setCurrentUser()
        {
            if(currentUser.Equals(String.Empty))
            {
                currentUser = PafApp.GetGridApp().LogonInformation.UserName;
            }
            else
            {
                if(!currentUser.Equals(PafApp.GetGridApp().LogonInformation.UserName))
                {
                    currentUser = PafApp.GetGridApp().LogonInformation.UserName;
                    _CurrentPafPlannerRole = null;
                    _CurrentPafSeason = null;
                }
            }
        }

        /// <summary>
        /// Procedure to add roles to the cboPlanType list box, or txtPlanType textbox.
        /// </summary>
        public void AddRoles()
        {
            try
            {
                getClientAuth();

                setCurrentUser();
                
                if (_pafPlannerConfigs != null)
                {
                     _pafPlannerConfigs.Clear();
                }


                if (_roles == null)
                    return;


                if (_plannerConfigs.Length > 0)
                {
                    foreach (pafPlannerConfig str in _plannerConfigs)
                    {
                        if (str != null)
                        {
                            if (_pafPlannerConfigs.ContainsKey(str.role))
                            {
                                List<pafPlannerConfig> tmp = _pafPlannerConfigs[str.role];
                                tmp.Add(str);
                                _pafPlannerConfigs[str.role] = tmp;
                            }
                            else
                            {
                                List<pafPlannerConfig> tmp = new List<pafPlannerConfig>();
                                tmp.Add(str);
                                _pafPlannerConfigs.Add(str.role, tmp);
                            }
                        }
                    }
                }

                if (_roles.Length > 1)
                {
                    cboRole.Visible = true;
                    txtRole.Visible = false;
                    foreach (pafPlannerRole str in _roles)
                    {
                        if (str != null)
                        {
                            cboRole.Items.Add(str.roleName);
                        }
                    }

                    if (!string.IsNullOrEmpty(_SelectedUserRole))
                    {
                        if (cboRole.Items.Contains(_SelectedUserRole))
                        {
                            int index = cboRole.Items.IndexOf(_SelectedUserRole);
                            cboRole.CausesValidation = false;
                            cboRole.BeginUpdate();
                            cboRole.SelectedIndex = index;
                            cboRole.EndUpdate();
                            cboRole.CausesValidation = true;
                        }
                    }
                }
                else
                {
                    cboRole.Visible = false;
                    txtRole.Visible = true;
                    txtRole.Text = _roles[0].roleName;
                }

                //TTN-2534 - Put back last selectons
                if (_extraControlsVisible)
                {
                    if (chkSupInvIx.Enabled)
                    {
                        chkSupInvIx.Checked = Settings.Default.SuppressInvIx;
                    }
                    if (chkFilteredTotals.Enabled)
                    {
                        chkFilteredTotals.Checked = Settings.Default.FilteredTotals;
                    }
                }

                toolTip.SetToolTip(cboRole,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.RoleSelection.Role"), 
                        new string[] { PafApp.GetGridApp().LogonInformation.UserName }));

                toolTip.SetToolTip(cmdCancel,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Cancel")));

                toolTip.SetToolTip(cmdOk,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Ok")));

            }
            catch (NullReferenceException)
            {
                PafApp.MessageBox().Show(
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.NoRolesDefined"),
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"));
            }
            catch (Exception)
            {
                return;
            }
        }

        /// <summary>
        /// Procedure to add the values to the seasons/values list box.
        /// </summary>
        public void AddProcesses()
        {
            try
            {
                getClientAuth();

                if (_roles == null)
                    return;

                if (!cboRole.Visible)
                {
                    foreach (season season in _roles[0].seasons)
                    {
                        if (!String.IsNullOrEmpty(season.id))
                        {
                            lstSeasonProc.Items.Add(season.id);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_planTypeSpecString))
                {
                    if (lstSeasonProc.Items.Contains(_planTypeSpecString))
                    {
                        int index = lstSeasonProc.Items.IndexOf(_planTypeSpecString);
                        lstSeasonProc.BeginUpdate();
                        lstSeasonProc.SelectedIndex = index;
                        lstSeasonProc_Click(null, null);
                        lstSeasonProc.EndUpdate();
                    }
                }

                //CancelEventArgs e = new CancelEventArgs();
                Validate_PafPlannerConfig(null, new CancelEventArgs());
            }
            catch (NullReferenceException)
            {
                PafApp.MessageBox().Show(
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.NoProcessesDefined"),
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"));
            }
            catch (Exception)
            {
                return;
            }
        }

        /// <summary>
        /// Method to check if the keypress was an enter key and if the ok button is enabled, 
        /// click it for the user.
        /// </summary>
        /// <param name="e">KeyEventArgs</param>
        private void ControlKeyPress(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmdOk.Enabled)
                {
                    cmdOk.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                cmdCancel.PerformClick();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <param name="season"></param>
        /// <returns></returns>
        private season GetSeason(pafPlannerRole role, string season)
        {
            return role.seasons.Where(s => !String.IsNullOrEmpty(s.id)).FirstOrDefault(s => season.ToLower().Equals(s.id.ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        private pafPlannerRole GetPafPlannerRole(string role)
        {
            return _roles.Where(r => !String.IsNullOrEmpty(r.roleName)).FirstOrDefault(r => role.ToLower().Equals(r.roleName.ToLower()));
        }

        /// <summary>
        /// Finds the current pafPlannerConfig to use.
        /// </summary>
        /// <param name="role">Current role selected by the user.</param>
        /// <param name="season">Current season/process selected by the user.</param>
        /// <returns>PafPlannerConf or null.</returns>
        public pafPlannerConfig FindPlannerConfig(string role, string season)
        {
            try
            {
                ClearControls();
                AddRoles();
                AddProcesses();

                pafPlannerRole r = GetPafPlannerRole(role);
                season s = GetSeason(r, season);

                if(r != null && s != null)
                {
                    return FindPlannerConfig(r, s);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }


        /// <summary>
        /// Finds the current pafPlannerConfig to use.
        /// </summary>
        /// <param name="role">Current role selected by the user.</param>
        /// <param name="season">Current season/process selected by the user.</param>
        /// <returns>PafPlannerConf or null.</returns>
        private pafPlannerConfig FindPlannerConfig(pafPlannerRole role, season season)
        {
            try
            {
                List<pafPlannerConfig> lst;
                if (_pafPlannerConfigs.TryGetValue(role.roleName, out lst))
                {
                    if (lst != null)
                    {
                        foreach (pafPlannerConfig ppc in lst)
                        {
                            if (role.roleName.Equals(ppc.role) && season.planCycle.Equals(ppc.cycle))
                            {
                                return ppc;
                            }
                        }
                        //in this case we are checking for the desfault instace, 
                        //the default instace will have an empty plancycle.
                        // if (lst.Count == 1)
                        foreach (pafPlannerConfig ppc in lst)
                        {
                            if (ppc.cycle == null || ppc.cycle.Equals(String.Empty))
                            {
                                return ppc;
                            }
                        }
                    }
                }
                return null;
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return null;
            }
        }

        /// <summary>
        /// Resizes the season proc list box to show or hide the ChkSupInvIx check box.
        /// </summary>
        /// <param name="showChkSupInvIx">Show the chkSupInvIx check box.</param>
        /// <param name="allowRoleFilter">Status of the Role Filter Flag.</param>
        /// <param name="allowFilteredSubtotals">Status of the Filter Subtotals Flag.</param>
        /// <param name="discontigSecurity">Does the selected role contain discontigous security</param>
        private void ResizeSeasonProcListBox(bool showChkSupInvIx, bool allowRoleFilter, bool allowFilteredSubtotals, bool discontigSecurity)
        {
            //Show controls if
            //a) allowFilteredSubtotals is true and discontigSecurity is true
            //b) allowFilteredSubtotals is true and showChkSupInvIx is enabled & selected
            //c) Both of the above.

            string role = null;
            if (_CurrentPafPlannerConfig != null) role = _CurrentPafPlannerConfig.role;

            string season = null;
            if (_CurrentPafSeason != null) season = _CurrentPafSeason.id;

            PafApp.GetLogger().InfoFormat("Role Selector || Role: {0}, Season: {1},  Invalid Inter: {2}, Role Filter: {3}, Filtered Subtotals: {4}, Discontiguous Sec: {5}",
                new object[] { role, season, showChkSupInvIx, allowRoleFilter, allowFilteredSubtotals, discontigSecurity });

            //Filtered subtotals is false, so hide everything.
            if (!allowFilteredSubtotals)
            {
                chkSupInvIx.Visible = showChkSupInvIx;
                chkSupInvIx.Enabled = showChkSupInvIx;
                chkSupInvIx.Checked = showChkSupInvIx;
                chkFilteredTotals.Visible = showChkSupInvIx;
                chkFilteredTotals.Enabled = false;
                chkFilteredTotals.Checked = false;
                RoleSelection_Resize(null, null);
                return;
            }
            //Allow role filter is true, so hide the controls as the Role Filter window will deal with this 
            if (allowRoleFilter)
            {
                chkSupInvIx.Enabled = false;
                chkFilteredTotals.Enabled = false;
                RoleSelection_Resize(null, null);
                return;
            }


            //To Reach Here
            //allowFilteredSubtotals will always be true
            //allowRoleFilter will always be false

            //At this point we just need to deal with cases A and B above.

            //Case "A" - discontigSecurity is true
            if (discontigSecurity)
            {
                chkSupInvIx.Visible = true;
                chkSupInvIx.Enabled = showChkSupInvIx;
                chkSupInvIx.Checked = showChkSupInvIx;

                chkFilteredTotals.Visible = true;
                chkFilteredTotals.Enabled = true;
                chkFilteredTotals.Checked = true;

                RoleSelection_Resize(null, null);
                return;
            }

            //Case "B" - showChkSupInvIx is true
            if (showChkSupInvIx)
            {
                chkSupInvIx.Visible = true;
                chkSupInvIx.Enabled = true;
                chkSupInvIx.Checked = true;

                chkFilteredTotals.Visible = true;
                chkFilteredTotals.Enabled = true;

                RoleSelection_Resize(null, null);

                return;
            }

            //Catch Case.  At this point showChkSupInvIx is false, and discontigSecurity is false.
            chkSupInvIx.Visible = _extraControlsVisible;
            chkSupInvIx.Enabled = false;
            chkSupInvIx.Checked = false;

            chkFilteredTotals.Visible = _extraControlsVisible;
            chkFilteredTotals.Enabled = false;
            chkFilteredTotals.Checked = false;

            RoleSelection_Resize(null, null);


        }

        /// <summary>
        /// Gets the discontigous status fo the current role.
        /// </summary>
        public bool IsRoleSecurityDiscontigous
        {
            get { return _CurrentPafPlannerRole != null && _discontigRoles.Contains(_CurrentPafPlannerRole.roleName); }
        }

        #endregion Methods

        #region Properties
        /// <summary>
        /// Gets/sets the users selected role.
        /// </summary>
        ///         
        [
        Browsable(true),
        Category("Data"),
		Description("User role."),
		DefaultValue("")
        ]
        public string UserRole
        {
            get { return _SelectedUserRole ;}
            set 
            {
                bool Cancel = false;
                if (! _SelectedUserRole.Equals(value))
                    RoleChanged(this, new LogonEventArgs(_SelectedUserRole, value, LogonEventArgs.ChangeType.Role), ref Cancel);

                if (Cancel)
                    return;

                _SelectedUserRole = value;
                _UserSelectedUserRole = value;
            }
        }

        /// <summary>
        /// Gets the selected version/season combination text, selected by the user.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("Gets the plan type text selected by the user"),
        DefaultValue("")
        ]
        public string UserSelectedPlanTypeSpecString
        {
            get { return _planTypeSpecString; }
            set 
            {
                bool Cancel = false;
                if (! _planTypeSpecString.Equals(value))
                    SeasonProcessChanged(this, new LogonEventArgs(_planTypeSpecString, value, LogonEventArgs.ChangeType.SeasonProcess), ref Cancel);

                if (Cancel)
                    return;

                _planTypeSpecString = value;
                _UserPlanTypeSpecString = value;
            }
        }

        /// <summary>
        /// Stores the checked value of the Suppress Invalid Intersections check box.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("Stores the checked value of the Suppress Invalid Intersections check box."),
        DefaultValue(false)
        ]
        public bool SuppressInvalidInx
        {
            get
            {
                return chkSupInvIx.Enabled && _SuppressInvalidInx;
            }
        }

        /// <summary>
        /// The currently selected paf planner role.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("The currently selected paf planner role."),
        DefaultValue(null)
        ]
        public pafPlannerRole CurrentPafPlannerRole
        {
            get { return _CurrentPafPlannerRole; }
            set { _CurrentPafPlannerRole = value; }
        }

        /// <summary>
        /// The currently selected paf planner config.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("The currently selected paf planner config."),
        DefaultValue(null)
        ]
        public pafPlannerConfig CurrentPafPlannerConfig
        {
            get { return _CurrentPafPlannerConfig; }
            set { _CurrentPafPlannerConfig = value; }
        }

        /// <summary>
        /// The currently selected paf planner season.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("The currently selected paf planner season."),
        DefaultValue(null)
        ]
        public season CurrentPafSeason
        {
            get { return _CurrentPafSeason; }
        }


        /// <summary>
        /// The currently selected paf planner role, the value could be null.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("The currently selected paf planner role, the value could be null."),
        DefaultValue(null)
        ]
        public string UserSelectedRole
        {
            get { return _UserSelectedUserRole; }
            set { _UserSelectedUserRole = value; }
        }


        /// <summary>
        /// The currently selected paf planner season, the value could be null.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("The currently selected paf planner season, the value could be null."),
        DefaultValue(null)
        ]
        public string UserPlanTypeSpecString
        {
            get { return _UserPlanTypeSpecString; }
            set { _UserPlanTypeSpecString = value; }
        }

        /// <summary>
        /// Stores the checked value of the Suppress Invalid Intersections check box.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("Gets the filtered Subtotal Checked Value."),
        DefaultValue(false)
        ]
        public bool FilteredSubtotals
        {
            get
            {
                return chkFilteredTotals.Enabled && chkFilteredTotals.Checked;
            }
        }

        #endregion Properties

        #region Handlers
        /// <summary>
        /// Click handeler for cmdOk, sets properties.
        /// </summary>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            try
            {
                string tempUserRole;
                string tempTypeSpec;
                bool reinitClientState = false;
                bool tempFilteredSubtotalsEnabled;
                bool tempSupInxIxEnabled;


                if (txtRole.Visible)
                {
                    tempUserRole = txtRole.Text;
                    tempTypeSpec = lstSeasonProc.SelectedItem.ToString();
                    
                }
                else
                {
                    tempUserRole = cboRole.SelectedItem.ToString();
                    tempTypeSpec = lstSeasonProc.SelectedItem.ToString();
                }

                // Get current values of Filtered Subototals & Suppress Invalid Intersection Check Boxes (TTN-2656)
                tempFilteredSubtotalsEnabled = chkFilteredTotals.Checked;
                tempSupInxIxEnabled = chkSupInvIx.Checked;

                //Get temp vars
                //If roles != then throw event
                //Check cancel value
                //if true, then exit.
                bool Cancel = false;
                if (!_SelectedUserRole.Equals(tempUserRole) && !_SelectedUserRole.Equals(""))
                {
                    RoleChanged(this, new LogonEventArgs(_SelectedUserRole, tempUserRole, LogonEventArgs.ChangeType.Role), ref Cancel);
                    reinitClientState = true;
                }

                if (!_planTypeSpecString.Equals(tempTypeSpec) && !_planTypeSpecString.Equals(""))
                {
                    SeasonProcessChanged(this, new LogonEventArgs(_planTypeSpecString, tempTypeSpec, LogonEventArgs.ChangeType.SeasonProcess), ref Cancel);
                    reinitClientState = true;
                }

                // Fire event upon a change to Filtered Subtotals or Suppress Invalid Intersections Options (TTN-2656)
                if (_extraControlsVisible && (_FilteredSubtotalsEnabled != tempFilteredSubtotalsEnabled || _SupInvInxEnabled != tempSupInxIxEnabled))
                {

                    FilteredSubtotalsOrSupInvIxChanged(this, new LogonEventArgs(tempUserRole, tempTypeSpec, LogonEventArgs.ChangeType.FilteredSubtotalsOrSupInvInx), ref Cancel);
                    reinitClientState = true;
                }

                if (Cancel)
                {
                    return;
                }
                else
                {
                    _SelectedUserRole = tempUserRole;
                    _UserSelectedUserRole = tempUserRole;
                    _planTypeSpecString = tempTypeSpec;
                    _UserPlanTypeSpecString = tempTypeSpec;

                    // Update stored values of Filtered Subtotals and Suppress Invalid Intersections options (TTN-2656)
                    if (_extraControlsVisible)
                    {
                        if (chkFilteredTotals.Enabled)
                        {
                            _FilteredSubtotalsEnabled = tempFilteredSubtotalsEnabled;
                        }
                        if (chkSupInvIx.Enabled)
                        {
                            _SupInvInxEnabled = tempSupInxIxEnabled; 
                        }
                    }
                }

                //PafApp.PopulateRoleFilter(_SelectedUserRole, _planTypeSpecString);
                //PafApp.CreatePlanningSession(_SelectedUserRole, _planTypeSpecString);

                _planTypeSpecString = lstSeasonProc.SelectedItem.ToString();

                //BuildCustomMenuDefs();

                if (!String.IsNullOrEmpty(_planTypeSpecString))
                {
                    //Set the last user id
                    Settings.Default.LastUserRole = _SelectedUserRole;
                    //Set the default property
                    Settings.Default.LastUserProcess = _planTypeSpecString;
                    //Write to disk
                    Settings.Default.Save();
                    //Reload the settings files.
                    Settings.Default.Reload();
                }

                if (_extraControlsVisible)
                {
                    if (chkFilteredTotals.Enabled)
                    {
                        Settings.Default.FilteredTotals = chkFilteredTotals.Checked;
                    }
                    if (chkSupInvIx.Enabled)
                    {
                        Settings.Default.SuppressInvIx = chkSupInvIx.Checked;
                    }
                    Settings.Default.Save();
                    Settings.Default.Reload();
                }

                //Fix for TTN-884
                //Reinitialize Client State
                if (reinitClientState == true)
                {
                    PafApp.GetReinitClientState();
                }

                //Fix issue where warning or error would cause role filter screen to to disappear.
                //DialogResult result = DialogResult.None;
                //bool keepGoing = true;
                //do
                //{
                //    result = PafApp.GetUowCalculator().CalculateUow(ParentForm,
                //       _CurrentPafPlannerConfig,
                //       PafApp.GetGridApp().LogonInformation.UserName,
                //       _SelectedUserRole,
                //     _planTypeSpecString);

                //    if (result == DialogResult.Cancel || result == DialogResult.OK ||
                //        result == DialogResult.Abort)
                //    {
                //        keepGoing = false;
                //    }


                //} while (keepGoing);


                //if (result == DialogResult.OK)
                //{
                //    BuildCustomMenuDefs();
                //}
                //else
                //{
                //    //do nothing...
                //}


                //begin old code

                //calculate uow
                PafApp.GetUowCalculator().CalculateUow(ParentForm,
                     _CurrentPafPlannerConfig,
                     PafApp.GetGridApp().LogonInformation.UserName,
                     _SelectedUserRole,
                     _planTypeSpecString);

                BuildCustomMenuDefs();

                //end old code...


            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Builds the CustomMenuDefs which are based on role/season
        /// </summary>
        public void BuildCustomMenuDefs()
        {
            try
            {
                //pmack
                //reset CustomMenuDefs which are based on role/season
                PafApp.CustomMenuDefs = null;

                //rebuild menu to pick up dynamic buttons 
                if (PafApp.GetGridApp().ViewMenu != null)
                {
                    PafApp.GetCommandBarMngr().CreateMenu();
                    PafApp.GetCommandBarMngr().EnableRolesMenuandToolbarButtons(true);
                    PafApp.GetCommandBarMngr().ShowRoleFilterToolbarButton();
                    PafApp.GetCommandBarMngr().EnableOptionsMenuButton(true);
                    PafApp.GetCommandBarMngr().EnableChangePasswordMenuButton(true);
                }
                //Reset the Pace toolbar to pick up Ruleset selection changes
                if (PafApp.GetGridApp().ViewToolBar != null)
                {
                    try
                    {
                        PafApp.GetGridApp().ViewToolBar.Controls[PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.Excel.Menu.ChangeRuleSet")].Delete(false);
                    }
                    //Will throw an error if the change ruleset combobox didn't exist.  ignore and continue.
                    catch
                    {
                    }

                    PafApp.GetCommandBarMngr().BuildRuleSetComboBox();
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Event handler for the season/Process list box click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstSeasonProc_Click(object sender, EventArgs e)
        {
            if (lstSeasonProc.SelectedIndex >= 0)
            {
                cmdOk.Enabled = true;
            }
            else
            {
                cmdOk.Enabled = false;
            }
        }

        /// <summary>
        /// Event handler for the season/Process list box double click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lstSeasonProc_DoubleClick(object sender, EventArgs e)
        {
            if (lstSeasonProc.SelectedIndex >= 0)
                cmdOk.PerformClick();
        }

        /// <summary>
        /// Event handler for when the Role selection box selected index changes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboRole_SelectedIndexChanged(object sender, EventArgs e)
        {
            lstSeasonProc.Items.Clear();

            int roleIndex = cboRole.Visible ? cboRole.SelectedIndex : 0;

            foreach (season season in _roles[roleIndex].seasons)
            {
                if (!String.IsNullOrEmpty(season.id))
                {
                    lstSeasonProc.Items.Add(season.id);
                }
            }

            //Fix Jira Bug 111.  If user is changing role, then try and reselect
            //their process/season if it exists.
            bool reslectedSeason = false;
            int seasonIndex = 0;
            if (!string.IsNullOrEmpty(_planTypeSpecString))
            {
                if (lstSeasonProc.Items.Contains(_planTypeSpecString))
                {
                    int item = lstSeasonProc.Items.IndexOf(_planTypeSpecString);
                    lstSeasonProc.BeginUpdate();
                    lstSeasonProc.SelectedIndex = item;
                    seasonIndex = lstSeasonProc.SelectedIndex;
                    lstSeasonProc.EndUpdate();
                    reslectedSeason = true;
                }
            }
            Validate_PafPlannerConfig(sender, new CancelEventArgs());
            lstSeasonProc_Click(null, null);
            bool dataFiltered = false, userFiltered = false, secDis = false, subTotals = false;
            if (reslectedSeason)
            {
                //set the role.
                _CurrentPafPlannerRole = _roles[roleIndex];
                //set the season.
                _CurrentPafSeason = _CurrentPafPlannerRole.seasons[seasonIndex];
                //null out the planner config
                _CurrentPafPlannerConfig = null;
                //set the planner conf.
                _CurrentPafPlannerConfig = FindPlannerConfig(_CurrentPafPlannerRole, _CurrentPafSeason);
                if (_CurrentPafPlannerConfig != null)
                {
                    dataFiltered = _CurrentPafPlannerConfig.isDataFilteredUow;
                    userFiltered = _CurrentPafPlannerConfig.isUserFilteredUow;
                    subTotals = _CurrentPafPlannerConfig.isFilteredSubtotals;
                    secDis = IsRoleSecurityDiscontigous;
                }
            }
            ResizeSeasonProcListBox(dataFiltered, userFiltered, subTotals, secDis);
        }

        /// <summary>
        /// Event handler for when the season/process selection box selected index changes.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">EventArgs</param>
        private void lstSeasonProc_SelectedIndexChanged(object sender, EventArgs e)
        {
            

            try
            {
                int roleIndex;
                if (cboRole.Visible)
                    roleIndex = cboRole.SelectedIndex;
                else
                    roleIndex = 0;

                int index = lstSeasonProc.SelectedIndex;

                //set the role.
                _CurrentPafPlannerRole = _roles[roleIndex];
                //set the season.
                _CurrentPafSeason = _CurrentPafPlannerRole.seasons[index];
                //null out the planner config
                _CurrentPafPlannerConfig = null;
                //set the planner conf.
                _CurrentPafPlannerConfig = FindPlannerConfig(_CurrentPafPlannerRole, _CurrentPafSeason);
                //show or hide the allow suppress invalid inx checkbox.
                if(_CurrentPafPlannerConfig == null)
                {
                    ResizeSeasonProcListBox(false, false, false, false);

                }
                else
                {
                    ResizeSeasonProcListBox(
                        _CurrentPafPlannerConfig.isDataFilteredUow,
                        _CurrentPafPlannerConfig.isUserFilteredUow,
                        _CurrentPafPlannerConfig.isFilteredSubtotals,
                        IsRoleSecurityDiscontigous);
                }

                Validate_PafPlannerConfig(sender, new CancelEventArgs());
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Validates the paf planner config
        /// </summary>
        /// <param name="sender">sender.</param>
        /// <param name="e">cancel event args.</param>
        private void Validate_PafPlannerConfig(object sender, CancelEventArgs e)
        {
            errorProvider1.Clear();

            if (_CurrentPafPlannerRole != null && _CurrentPafSeason != null)
            {
                pafPlannerConfig pafPlannerConfig = FindPlannerConfig(_CurrentPafPlannerRole, _CurrentPafSeason);

                if (pafPlannerConfig == null)
                {
                    errorProvider1.SetError(
                        lblRoleSelection,
                        String.Format(PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.Controls.RoleSelection.Exception.NoPlannerConfig"),
                            new string[] {_CurrentPafPlannerRole.roleName}));

                    e.Cancel = true;
                    cmdOk.Enabled = false;
                }
                else
                {
                    cmdOk.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Event handler for when the control resize event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void RoleSelection_Resize(object sender, EventArgs e)
        {
            chkFilteredTotals.Visible = _extraControlsVisible;
            chkSupInvIx.Visible = _extraControlsVisible;
            int extraHeight = 0;
            if (_extraControlsVisible)
            {
                extraHeight = pnlOptions.Height;
            }
            pnlList.Height = Height - pnlList.Top - extraHeight - pnlOptions.Height;
        }

        /// <summary>
        /// lstSeasonProc KeyUp event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Key event args.</param>
        private void TextControlKeyDown(object sender, KeyEventArgs e)
        {
            ControlKeyPress(e);
        }

        /// <summary>
        /// Handler for when the check value changed.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void chkSupInvIx_CheckedChanged(object sender, EventArgs e)
        {
            _SuppressInvalidInx = chkSupInvIx.Checked;

            if (_CurrentPafPlannerConfig == null) return;


            //If Filtered Subs are turned off, then disable the box - ALWAYS
            if (!_CurrentPafPlannerConfig.isFilteredSubtotals)
            {
                chkFilteredTotals.Enabled = false;
                return;
            }
            if (!IsRoleSecurityDiscontigous)
            {
                chkFilteredTotals.Enabled = _SuppressInvalidInx;
            }
        }

        /// <summary>
        /// Handler for the check change of Filtered Subtotals
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkFilteredTotals_CheckedChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the enter 
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event Args</param>
        /// <remarks>TTN-854</remarks>
        private void lstSeasonProc_Enter(object sender, EventArgs e)
        {
            if(lstSeasonProc.SelectedIndex > 0 || lstSeasonProc.Items == null || lstSeasonProc.Items.Count == 0)
            {
                return;
            }
            lstSeasonProc.SelectedIndex = 0;
        }

        #endregion Handlers


    }
    #region RoleEventArgs
    /// <summary>
    /// Custom event argument for when the user changes roles.
    /// </summary>
    internal class LogonEventArgs : EventArgs
    {
        public enum ChangeType
        {
            FilteredSubtotalsOrSupInvInx,       // TTN-2656
            Password,
            Role,
            RoleFilter,
            SeasonProcess,
            Server,
            Userid,
            Userid_Relogin,
        }

        public LogonEventArgs(string OldRole, string NewRole, ChangeType Type)
        {
            oldRole = OldRole;
            newRole = NewRole;
            type = Type;
        } 

        public string oldRole;
        public string newRole;
        public ChangeType type;
    }
    #endregion RoleEventArgs
}