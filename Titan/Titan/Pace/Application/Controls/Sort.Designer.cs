namespace Titan.Pace.Application.Controls
{
    partial class Sort
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOk = new System.Windows.Forms.Button();
            this.cboSort1 = new System.Windows.Forms.ComboBox();
            this.cboSort2 = new System.Windows.Forms.ComboBox();
            this.cboSort3 = new System.Windows.Forms.ComboBox();
            this.lblSort1 = new System.Windows.Forms.Label();
            this.lblSort2 = new System.Windows.Forms.Label();
            this.lblSort3 = new System.Windows.Forms.Label();
            this.rdoSort1Desc = new System.Windows.Forms.RadioButton();
            this.rdoSort1Asc = new System.Windows.Forms.RadioButton();
            this.grpSort1 = new System.Windows.Forms.GroupBox();
            this.grpSort2 = new System.Windows.Forms.GroupBox();
            this.rdoSort2Asc = new System.Windows.Forms.RadioButton();
            this.rdoSort2Desc = new System.Windows.Forms.RadioButton();
            this.grpSort3 = new System.Windows.Forms.GroupBox();
            this.rdoSort3Asc = new System.Windows.Forms.RadioButton();
            this.rdoSort3Desc = new System.Windows.Forms.RadioButton();
            this.grpColumnOptions = new System.Windows.Forms.GroupBox();
            this.rdoHeaders = new System.Windows.Forms.RadioButton();
            this.rdoDescriptions = new System.Windows.Forms.RadioButton();
            this.grpSort1.SuspendLayout();
            this.grpSort2.SuspendLayout();
            this.grpSort3.SuspendLayout();
            this.grpColumnOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(362, 359);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(4);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(100, 28);
            this.cmdCancel.TabIndex = 8;
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Enabled = false;
            this.cmdOk.Location = new System.Drawing.Point(254, 359);
            this.cmdOk.Margin = new System.Windows.Forms.Padding(4);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(100, 28);
            this.cmdOk.TabIndex = 7;
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cboSort1
            // 
            this.cboSort1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSort1.FormattingEnabled = true;
            this.cboSort1.Location = new System.Drawing.Point(28, 41);
            this.cboSort1.Margin = new System.Windows.Forms.Padding(4);
            this.cboSort1.Name = "cboSort1";
            this.cboSort1.Size = new System.Drawing.Size(225, 24);
            this.cboSort1.TabIndex = 0;
            this.cboSort1.SelectedIndexChanged += new System.EventHandler(this.cboSort1_SelectedIndexChanged);
            // 
            // cboSort2
            // 
            this.cboSort2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSort2.FormattingEnabled = true;
            this.cboSort2.Location = new System.Drawing.Point(28, 132);
            this.cboSort2.Margin = new System.Windows.Forms.Padding(4);
            this.cboSort2.Name = "cboSort2";
            this.cboSort2.Size = new System.Drawing.Size(225, 24);
            this.cboSort2.TabIndex = 2;
            this.cboSort2.SelectedIndexChanged += new System.EventHandler(this.cboSort2_SelectedIndexChanged);
            // 
            // cboSort3
            // 
            this.cboSort3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboSort3.FormattingEnabled = true;
            this.cboSort3.Location = new System.Drawing.Point(28, 223);
            this.cboSort3.Margin = new System.Windows.Forms.Padding(4);
            this.cboSort3.Name = "cboSort3";
            this.cboSort3.Size = new System.Drawing.Size(225, 24);
            this.cboSort3.TabIndex = 4;
            this.cboSort3.SelectedIndexChanged += new System.EventHandler(this.cboSort3_SelectedIndexChanged);
            // 
            // lblSort1
            // 
            this.lblSort1.AutoSize = true;
            this.lblSort1.Location = new System.Drawing.Point(21, 17);
            this.lblSort1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSort1.Name = "lblSort1";
            this.lblSort1.Size = new System.Drawing.Size(0, 17);
            this.lblSort1.TabIndex = 10;
            // 
            // lblSort2
            // 
            this.lblSort2.AutoSize = true;
            this.lblSort2.Location = new System.Drawing.Point(21, 108);
            this.lblSort2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSort2.Name = "lblSort2";
            this.lblSort2.Size = new System.Drawing.Size(0, 17);
            this.lblSort2.TabIndex = 11;
            // 
            // lblSort3
            // 
            this.lblSort3.AutoSize = true;
            this.lblSort3.Location = new System.Drawing.Point(21, 199);
            this.lblSort3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSort3.Name = "lblSort3";
            this.lblSort3.Size = new System.Drawing.Size(0, 17);
            this.lblSort3.TabIndex = 12;
            // 
            // rdoSort1Desc
            // 
            this.rdoSort1Desc.AutoSize = true;
            this.rdoSort1Desc.Location = new System.Drawing.Point(8, 47);
            this.rdoSort1Desc.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort1Desc.Name = "rdoSort1Desc";
            this.rdoSort1Desc.Size = new System.Drawing.Size(17, 16);
            this.rdoSort1Desc.TabIndex = 0;
            this.rdoSort1Desc.UseVisualStyleBackColor = true;
            // 
            // rdoSort1Asc
            // 
            this.rdoSort1Asc.AutoSize = true;
            this.rdoSort1Asc.Checked = true;
            this.rdoSort1Asc.Location = new System.Drawing.Point(8, 23);
            this.rdoSort1Asc.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort1Asc.Name = "rdoSort1Asc";
            this.rdoSort1Asc.Size = new System.Drawing.Size(17, 16);
            this.rdoSort1Asc.TabIndex = 0;
            this.rdoSort1Asc.TabStop = true;
            this.rdoSort1Asc.UseVisualStyleBackColor = true;
            // 
            // grpSort1
            // 
            this.grpSort1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSort1.Controls.Add(this.rdoSort1Asc);
            this.grpSort1.Controls.Add(this.rdoSort1Desc);
            this.grpSort1.Location = new System.Drawing.Point(308, 17);
            this.grpSort1.Margin = new System.Windows.Forms.Padding(4);
            this.grpSort1.Name = "grpSort1";
            this.grpSort1.Padding = new System.Windows.Forms.Padding(4);
            this.grpSort1.Size = new System.Drawing.Size(153, 84);
            this.grpSort1.TabIndex = 1;
            this.grpSort1.TabStop = false;
            // 
            // grpSort2
            // 
            this.grpSort2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSort2.Controls.Add(this.rdoSort2Asc);
            this.grpSort2.Controls.Add(this.rdoSort2Desc);
            this.grpSort2.Location = new System.Drawing.Point(308, 108);
            this.grpSort2.Margin = new System.Windows.Forms.Padding(4);
            this.grpSort2.Name = "grpSort2";
            this.grpSort2.Padding = new System.Windows.Forms.Padding(4);
            this.grpSort2.Size = new System.Drawing.Size(153, 84);
            this.grpSort2.TabIndex = 3;
            this.grpSort2.TabStop = false;
            // 
            // rdoSort2Asc
            // 
            this.rdoSort2Asc.AutoSize = true;
            this.rdoSort2Asc.Checked = true;
            this.rdoSort2Asc.Location = new System.Drawing.Point(8, 23);
            this.rdoSort2Asc.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort2Asc.Name = "rdoSort2Asc";
            this.rdoSort2Asc.Size = new System.Drawing.Size(17, 16);
            this.rdoSort2Asc.TabIndex = 0;
            this.rdoSort2Asc.TabStop = true;
            this.rdoSort2Asc.UseVisualStyleBackColor = true;
            // 
            // rdoSort2Desc
            // 
            this.rdoSort2Desc.AutoSize = true;
            this.rdoSort2Desc.Location = new System.Drawing.Point(8, 47);
            this.rdoSort2Desc.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort2Desc.Name = "rdoSort2Desc";
            this.rdoSort2Desc.Size = new System.Drawing.Size(17, 16);
            this.rdoSort2Desc.TabIndex = 0;
            this.rdoSort2Desc.UseVisualStyleBackColor = true;
            // 
            // grpSort3
            // 
            this.grpSort3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSort3.Controls.Add(this.rdoSort3Asc);
            this.grpSort3.Controls.Add(this.rdoSort3Desc);
            this.grpSort3.Location = new System.Drawing.Point(308, 199);
            this.grpSort3.Margin = new System.Windows.Forms.Padding(4);
            this.grpSort3.Name = "grpSort3";
            this.grpSort3.Padding = new System.Windows.Forms.Padding(4);
            this.grpSort3.Size = new System.Drawing.Size(153, 84);
            this.grpSort3.TabIndex = 5;
            this.grpSort3.TabStop = false;
            // 
            // rdoSort3Asc
            // 
            this.rdoSort3Asc.AutoSize = true;
            this.rdoSort3Asc.Checked = true;
            this.rdoSort3Asc.Location = new System.Drawing.Point(8, 23);
            this.rdoSort3Asc.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort3Asc.Name = "rdoSort3Asc";
            this.rdoSort3Asc.Size = new System.Drawing.Size(17, 16);
            this.rdoSort3Asc.TabIndex = 0;
            this.rdoSort3Asc.TabStop = true;
            this.rdoSort3Asc.UseVisualStyleBackColor = true;
            // 
            // rdoSort3Desc
            // 
            this.rdoSort3Desc.AutoSize = true;
            this.rdoSort3Desc.Location = new System.Drawing.Point(8, 47);
            this.rdoSort3Desc.Margin = new System.Windows.Forms.Padding(4);
            this.rdoSort3Desc.Name = "rdoSort3Desc";
            this.rdoSort3Desc.Size = new System.Drawing.Size(17, 16);
            this.rdoSort3Desc.TabIndex = 0;
            this.rdoSort3Desc.UseVisualStyleBackColor = true;
            // 
            // grpColumnOptions
            // 
            this.grpColumnOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.grpColumnOptions.Controls.Add(this.rdoHeaders);
            this.grpColumnOptions.Controls.Add(this.rdoDescriptions);
            this.grpColumnOptions.Location = new System.Drawing.Point(28, 306);
            this.grpColumnOptions.Margin = new System.Windows.Forms.Padding(4);
            this.grpColumnOptions.Name = "grpColumnOptions";
            this.grpColumnOptions.Padding = new System.Windows.Forms.Padding(4);
            this.grpColumnOptions.Size = new System.Drawing.Size(433, 44);
            this.grpColumnOptions.TabIndex = 6;
            this.grpColumnOptions.TabStop = false;
            // 
            // rdoHeaders
            // 
            this.rdoHeaders.AutoSize = true;
            this.rdoHeaders.Location = new System.Drawing.Point(208, 21);
            this.rdoHeaders.Margin = new System.Windows.Forms.Padding(4);
            this.rdoHeaders.Name = "rdoHeaders";
            this.rdoHeaders.Size = new System.Drawing.Size(17, 16);
            this.rdoHeaders.TabIndex = 0;
            this.rdoHeaders.UseVisualStyleBackColor = true;
            // 
            // rdoDescriptions
            // 
            this.rdoDescriptions.AutoSize = true;
            this.rdoDescriptions.Checked = true;
            this.rdoDescriptions.Location = new System.Drawing.Point(8, 21);
            this.rdoDescriptions.Margin = new System.Windows.Forms.Padding(4);
            this.rdoDescriptions.Name = "rdoDescriptions";
            this.rdoDescriptions.Size = new System.Drawing.Size(17, 16);
            this.rdoDescriptions.TabIndex = 0;
            this.rdoDescriptions.TabStop = true;
            this.rdoDescriptions.UseVisualStyleBackColor = true;
            this.rdoDescriptions.CheckedChanged += new System.EventHandler(this.rdoHeaders_CheckedChanged);
            // 
            // Sort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.grpColumnOptions);
            this.Controls.Add(this.grpSort3);
            this.Controls.Add(this.grpSort2);
            this.Controls.Add(this.grpSort1);
            this.Controls.Add(this.lblSort3);
            this.Controls.Add(this.lblSort2);
            this.Controls.Add(this.lblSort1);
            this.Controls.Add(this.cboSort3);
            this.Controls.Add(this.cboSort2);
            this.Controls.Add(this.cboSort1);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOk);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Sort";
            this.Size = new System.Drawing.Size(486, 406);
            this.Resize += new System.EventHandler(this.Sort_Resize);
            this.grpSort1.ResumeLayout(false);
            this.grpSort1.PerformLayout();
            this.grpSort2.ResumeLayout(false);
            this.grpSort2.PerformLayout();
            this.grpSort3.ResumeLayout(false);
            this.grpSort3.PerformLayout();
            this.grpColumnOptions.ResumeLayout(false);
            this.grpColumnOptions.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button cmdCancel;
        internal System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.ComboBox cboSort1;
        private System.Windows.Forms.ComboBox cboSort2;
        private System.Windows.Forms.ComboBox cboSort3;
        private System.Windows.Forms.Label lblSort1;
        private System.Windows.Forms.Label lblSort2;
        private System.Windows.Forms.Label lblSort3;
        private System.Windows.Forms.RadioButton rdoSort1Desc;
        private System.Windows.Forms.RadioButton rdoSort1Asc;
        private System.Windows.Forms.GroupBox grpSort1;
        private System.Windows.Forms.GroupBox grpSort2;
        private System.Windows.Forms.RadioButton rdoSort2Asc;
        private System.Windows.Forms.RadioButton rdoSort2Desc;
        private System.Windows.Forms.GroupBox grpSort3;
        private System.Windows.Forms.RadioButton rdoSort3Asc;
        private System.Windows.Forms.RadioButton rdoSort3Desc;
        private System.Windows.Forms.GroupBox grpColumnOptions;
        private System.Windows.Forms.RadioButton rdoHeaders;
        private System.Windows.Forms.RadioButton rdoDescriptions;
    }
}
