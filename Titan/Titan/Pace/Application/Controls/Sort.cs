#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;
using Titan.Properties;

namespace Titan.Pace.Application.Controls
{
    internal partial class Sort : UserControl
    {
        private string[] _Alphabet = null;
        private Dictionary<int, TupleItem> _TupleItems = null;
        private List<int> _SortPositions = new List<int>();
        private bool _IsCboSelectedIndexChanged = false;
        private bool _IsInDataRange = true;
        private const int BORDER_PAD = 21;
        private const int PAD = 15;

        /// <summary>
        /// constructor
        /// </summary>
        public Sort()
        {
            InitializeComponent();
            loadUi();
        }

        #region Methods

        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                //Reset the property to false each time the form is loaded.
                PafApp.GetViewMngr().ShowSortForm = false;

                //Build an array of tuple strings for display in the combo boxes
                OlapView olapView = PafApp.GetViewMngr().CurrentOlapView;

                //Only search for a previous sort range if only a single cell is selected
                if (PafApp.GetViewMngr().CurrentGrid.GetSelectedCellCount() == 1)
                {
                    // Get stored User Selections for a given list row positions
                    foreach (SortSelection selection in PafApp.GetViewMngr().CurrentOlapView.SortSelections)
                    {
                        //Determine if the active cell is in an existing list of row positions 
                        if (selection.ContainsPosition(PafApp.GetViewMngr().CurrentGrid.GetActiveCell().CellAddress.Row - olapView.StartCell.Row - (olapView.ColDims.Count - 1)))
                        {
                            _SortPositions = selection.SortPositions;
                            break;
                        }
                    }
                }

                //If the list of rows are being sorted for the first time
                bool areRowPositionsAlreadySorted = false;
                if (_SortPositions.Count == 0)
                {
                    // If only a single cell is selected, force generic selection of the entire
                    // data range (TTN-583)
                    if (PafApp.GetViewMngr().CurrentGrid.GetSelectedCellCount() == 1)
                    {
                        Range rng = new Range();
                        rng.AddContiguousRange(PafApp.GetViewMngr().CurrentOlapView.DataRange);
                        PafApp.GetViewMngr().CurrentGrid.Select(rng);
                    }

                    //Build the row list to contain all rows with selected cells.
                    foreach (ContiguousRange contigRange in PafApp.GetViewMngr().CurrentGrid.GetSelectedCells().ContiguousRanges)
                    {
                        foreach (int row in contigRange.Rows)
                        {
                            //Determine if every position in the selected range is in the View Section Data Range
                            if (!PafApp.GetViewMngr().CurrentOlapView.DataRange.InContiguousRange(row))
                            {
                                _IsInDataRange = false;
                                break;
                            }

                            //modified for aliases
                            int selectedPosition = row - olapView.StartCell.Row - (olapView.ColDims.Count - 1 + olapView.ColAliases);
                            if (!_SortPositions.Contains(selectedPosition))
                            {
                                _SortPositions.Add(selectedPosition);
                            }
                        }

                        //Get out of outer loop after breaking out of inner loop.
                        if (!_IsInDataRange)
                        {
                            break;
                        }
                    }
                }
                else
                {
                    areRowPositionsAlreadySorted = true;
                }

                //If every cell in the selected range is in the Data Range and more than 1 row is selected, then
                if (_IsInDataRange && _SortPositions.Count > 1)
                {
                    //Build the range
                    Range selectRange = new Range();
                    foreach (int rowPosition in _SortPositions)
                    {
                        for (int col = olapView.DataRange.TopLeft.Col; col <= olapView.DataRange.BottomRight.Col; col++)
                        {
                            //modified for aliases
                            selectRange.AddContiguousRange(new CellAddress(rowPosition + olapView.StartCell.Row + (olapView.ColDims.Count - 1 + olapView.ColAliases), col));
                        }
                    }

                    //select that range on the grid.
                    PafApp.GetViewMngr().CurrentGrid.Select(selectRange);

                    //set the flag to display the Sort Form
                    PafApp.GetViewMngr().ShowSortForm = true;

                    //encapsulate the sort row positions
                    olapView.SortPositions.Clear();
                    foreach (int rowPosition in _SortPositions)
                    {
                        olapView.SortPositions.Add(rowPosition);
                    }

                    //Encapsulate the sort range - after the sort this range is selected
                    PafApp.GetViewMngr().CurrentOlapView.SortRange = selectRange;
                    //Encapsulate the TopLeft Window anchor position - after the sort the Window is anchored on this cell
                    PafApp.GetViewMngr().CurrentOlapView.TopLeftWindowAnchor = PafApp.GetViewMngr().CurrentGrid.GetWindowPosition();
                }

                if (PafApp.GetViewMngr().ShowSortForm)
                {
                    //Get the labels from the localized .resx file
                    cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                    cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                    lblSort1.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Sort");
                    lblSort2.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortNext");
                    lblSort3.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortNext");
                    rdoSort1Asc.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortAscending");
                    rdoSort1Desc.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortDescending");
                    rdoSort2Asc.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortAscending");
                    rdoSort2Desc.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortDescending");
                    rdoSort3Asc.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortAscending");
                    rdoSort3Desc.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SortDescending");
                    rdoDescriptions.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UseColumnDescriptions");
                    rdoHeaders.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UseColumnHeaders");

                    //Build a dictionary of TupleItems to populate the SortOn Column combo boxes.
                    int position = 0;
                    int alpha = 0;
                    //This signals to the function that the alpha columns have already been built, and not longer need to be processed.
                    bool firstTimeThru = true;
                    _TupleItems = new Dictionary<int, TupleItem>();
                    foreach (string dim in olapView.ColDims.Keys)
                    {
                        // output the individual members for that column
                        foreach (string member in olapView.ColDims[dim])
                        {
                            //Only include non blanks
                            memberTagDef mbrTagDef = PafApp.GetMemberTagInfo().GetMemberTag(member);
                            if (member.ToUpper() != Settings.Default.Blank && mbrTagDef == null)
                            {
                                string aliasTable = PafApp.GetViewTreeView().GetAliasTableName(PafApp.GetViewMngr().CurrentView.ViewName, dim);

                                //string mbrName = PafApp.GetGridApp().GetSimpleTrees().GetAlias(dim, member, aliasTable);
                                string mbrName = PafApp.SimpleDimTrees.GetMemberAliasValue(dim, member, aliasTable);
                                //If an alias exists for the member then use it
                                if (mbrName == null)
                                {
                                    mbrName = member;
                                }

                                string alphaHeader = String.Empty;
                                //TTN-1301
                                if(firstTimeThru)
                                {
                                    //string alphaHeader = Alphabet[alpha + olapView.StartCell.Col + olapView.RowDims.Count - 1];
                                    //TTN-1182, Have to adjust of member or alias columns.
                                   alphaHeader = Alphabet[alpha + olapView.RowRange.BottomRight.Col];
                                }

                                TupleItem tupleItem;
                                //if (!_TupleItems.ContainsKey(position))
                                if(! _TupleItems.TryGetValue(position, out tupleItem))
                                {
                                    _TupleItems.Add(position, new TupleItem(position, mbrName, rdoSort1Asc.Checked, alphaHeader));
                                }
                                else
                                {
                                    tupleItem.TupleString += " : " + mbrName;
                                    //_TupleItems[position].TupleString = _TupleItems[position].TupleString + " : " + mbrName;
                                }
                                position++;
                            }
                            //02/07/2008 KRM - Moved into if block, and added new alpha var.
                            //position++;
                            alpha++;
                        }
                        position = 0;
                        //TTN-1301
                        firstTimeThru = false;
                    }

                    // Get stored User Selections for a given list of row positions - if they exist
                    SortSelection tempSortSelection = new SortSelection(_SortPositions);

                    foreach (SortSelection selection in PafApp.GetViewMngr().CurrentOlapView.SortSelections)
                    {
                        if (selection.Equals(tempSortSelection))
                        {
                            //This populates the combo boxes if AreColumnHeadersSelected = false
                            rdoDescriptions.Checked = selection.AreColumnHeadersSelected;

                            //Otherwise the rdoHeaders_CheckedChanged needs to be fired to populate the combo boxes
                            if (selection.AreColumnHeadersSelected)
                            {
                                rdoHeaders_CheckedChanged(new object(), new EventArgs());
                            }

                            rdoHeaders.Checked = !selection.AreColumnHeadersSelected;

                            string[] sortOnColumnSelections = selection.SortOnColumnSelections.ToArray();
                            cboSort1.Text = sortOnColumnSelections[0];

                            cboSort2.Text = sortOnColumnSelections.Length >= 2 ? sortOnColumnSelections[1] : String.Empty;

                            cboSort3.Text = sortOnColumnSelections.Length >= 3 ? sortOnColumnSelections[2] : String.Empty;


                            bool[] sortDirectionSelections = selection.SortDirectionSelections.ToArray();
                            rdoSort1Asc.Checked = sortDirectionSelections[0];
                            rdoSort1Desc.Checked = !sortDirectionSelections[0];

                            if (sortDirectionSelections.Length >= 2)
                            {
                                rdoSort2Asc.Checked = sortDirectionSelections[1];
                                rdoSort2Desc.Checked = !sortDirectionSelections[1];
                            }
                            else
                            {
                                rdoSort2Asc.Checked = true;
                                rdoSort2Desc.Checked = false;
                            }


                            if (sortDirectionSelections.Length >= 3)
                            {
                                rdoSort3Asc.Checked = sortDirectionSelections[2];
                                rdoSort3Desc.Checked = !sortDirectionSelections[2];
                            }
                            else
                            {
                                rdoSort3Asc.Checked = true;
                                rdoSort3Desc.Checked = false;
                            }

                            break;
                        }

                    }

                    //Populate Combo Boxes if the list of rows is being sorted for the first time
                    if (!areRowPositionsAlreadySorted)
                    {

                        _IsCboSelectedIndexChanged = true;

                        cboSort1.Items.Clear();
                        cboSort1.Items.Clear();
                        cboSort1.Items.Clear();

                        cboSort1.Items.Add(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"));
                        cboSort2.Items.Add(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"));
                        cboSort3.Items.Add(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"));
                        foreach (KeyValuePair<int, TupleItem> kv in _TupleItems)
                        {
                            kv.Value.DisplayDescriptions = rdoDescriptions.Checked;

                            cboSort1.Items.Add(kv.Value);
                            cboSort2.Items.Add(kv.Value);
                            cboSort3.Items.Add(kv.Value);
                        }

                        _IsCboSelectedIndexChanged = false;
                    }
                }
             }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }
        #endregion Methods

        #region Handlers

        /// <summary>
        /// Ok button click hander.
        /// </summary>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            OlapView olapView = PafApp.GetViewMngr().CurrentOlapView;

            try
            {
                Cursor = Cursors.WaitCursor; 

                //Set Flag indicating that a sort has occurred.
                olapView.IsSorted = true;

                //Turn sorting mode on
                PafApp.GetViewMngr().Sorting = true;

                //Now clear the list of rows/columns to sort by 
                olapView.SortByList.Clear();

                //Next build the list of rows/columns to sort by 
                if (cboSort1.SelectedItem != null && cboSort1.SelectedItem.ToString() != 
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"))
                {
                    SortBy sortBy = new SortBy(((TupleItem)cboSort1.SelectedItem).Position,
                        rdoSort1Asc.Checked);
                    olapView.SortByList.Add(sortBy);
                }

                if (cboSort2.SelectedItem != null && cboSort2.SelectedItem.ToString() != 
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"))
                {
                    SortBy sortBy = new SortBy(((TupleItem)cboSort2.SelectedItem).Position,
                        rdoSort2Asc.Checked);
                    olapView.SortByList.Add(sortBy);
                }

                if (cboSort3.SelectedItem != null && cboSort3.SelectedItem.ToString() != 
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"))
                {
                    SortBy sortBy = new SortBy(((TupleItem)cboSort3.SelectedItem).Position,
                        rdoSort3Asc.Checked);
                    olapView.SortByList.Add(sortBy);
                }

                //Remove any previous set of selected row positions if there is overlap with the current set of selected positions
                for (int i = PafApp.GetViewMngr().CurrentOlapView.SortSelections.Count - 1; i >= 0; i--)
                {
                    foreach (int position in _SortPositions)
                    {
                        if (PafApp.GetViewMngr().CurrentOlapView.SortSelections[i].ContainsPosition(position))
                        {
                            PafApp.GetViewMngr().CurrentOlapView.SortSelections.RemoveAt(i);
                            break;
                        }
                    }
                }

                //Encapsulate the user selections
                SortSelection newSortSelections = new SortSelection(_SortPositions);
                newSortSelections.SortOnColumnSelections.Add(cboSort1.SelectedItem != null ? cboSort1.SelectedItem.ToString() : "");
                newSortSelections.SortDirectionSelections.Add(rdoSort1Asc.Checked);
                newSortSelections.SortOnColumnSelections.Add(cboSort2.SelectedItem != null ? cboSort2.SelectedItem.ToString() : "");
                newSortSelections.SortDirectionSelections.Add(rdoSort2Asc.Checked);
                newSortSelections.SortOnColumnSelections.Add(cboSort3.SelectedItem != null ? cboSort3.SelectedItem.ToString() : "");
                newSortSelections.SortDirectionSelections.Add(rdoSort3Asc.Checked);
                newSortSelections.AreColumnHeadersSelected = rdoDescriptions.Checked;

                //Store the User Selections
                PafApp.GetViewMngr().CurrentOlapView.SortSelections.Add(newSortSelections);

                PafApp.GetViewMngr().CurrentOlapView.ServerSortOverridden = true;

                PafApp.GetViewMngr().Sort();
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                //Turn sorting mode off
                PafApp.GetViewMngr().Sorting = false;
                Cursor = Cursors.Default;  
            }
        }

        #endregion Handlers


        /// <summary>
        /// Toggle between column headers and column tuple descriptions
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void rdoHeaders_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (_IsCboSelectedIndexChanged == false)
                {
                    _IsCboSelectedIndexChanged = true;

                    PopulateCboSort(cboSort1, cboSort2.SelectedItem != null ? cboSort2.SelectedItem.ToString() : "",
                        cboSort3.SelectedItem != null ? cboSort3.SelectedItem.ToString() : "");
                    PopulateCboSort(cboSort2, cboSort1.SelectedItem != null ? cboSort1.SelectedItem.ToString() : "",
                        cboSort3.SelectedItem != null ? cboSort3.SelectedItem.ToString() : "");
                    PopulateCboSort(cboSort3, cboSort1.SelectedItem != null ? cboSort1.SelectedItem.ToString() : "",
                        cboSort2.SelectedItem != null ? cboSort2.SelectedItem.ToString() : "");

                    _IsCboSelectedIndexChanged = false;
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

      
        /// <summary>
        /// The Excel Alpha column headers from A to IV
        /// </summary>
        public string[] Alphabet
        {
            get 
            {
                if (_Alphabet == null)
                {
                    _Alphabet = new string[256];
                    string[] abc = new string[26] {"A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"};
                    int j = 0;  //resets at the end of the alphabet
                    int k = 0;  //increments every time the alphabet repeats
                    for (int i = 0; i < 256; i++)
                    {
                        if (i > 25)
                        {
                            //Prefix an alpha char in alphabetical order to the set of 26 ABC's
                            _Alphabet[i] = _Alphabet[k] + abc[j];
                        }
                        else
                        {
                            //The first set of 26 ABC's should not be prefixed.
                            _Alphabet[i] = abc[j];
                        }

                        if (j == 25)
                        {
                            j = 0;

                            //The second set of 26 ABC's should be prefixed by "A".  This makes that possible.
                            if (i > 25)
                            {
                                k++;
                            }
                        }
                        else
                        {
                            j++;
                        }
                    }

                }
                
                return _Alphabet; 
            }
        }

        private void cboSort1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_IsCboSelectedIndexChanged == false)
            {
                _IsCboSelectedIndexChanged = true;

                if (cboSort1.Text == PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"))
                {
                    cboSort1.SelectedItem = null;
                }
                EnableCmdOk();

                PopulateCboSort(cboSort2, cboSort1.SelectedItem != null ? cboSort1.SelectedItem.ToString() : "",
                    cboSort3.SelectedItem != null ? cboSort3.SelectedItem.ToString() : "");
                PopulateCboSort(cboSort3, cboSort1.SelectedItem != null ? cboSort1.SelectedItem.ToString() : "",
                    cboSort2.SelectedItem != null ? cboSort2.SelectedItem.ToString() : "");

                _IsCboSelectedIndexChanged = false;
            }
        }

        private void cboSort2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_IsCboSelectedIndexChanged == false)
            {
                _IsCboSelectedIndexChanged = true;

                if (cboSort2.Text == PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"))
                {
                    cboSort2.SelectedItem = null;
                }
                EnableCmdOk();

                PopulateCboSort(cboSort1, cboSort2.SelectedItem != null ? cboSort2.SelectedItem.ToString() : "",
                    cboSort3.SelectedItem != null ? cboSort3.SelectedItem.ToString() : "");
                PopulateCboSort(cboSort3, cboSort2.SelectedItem != null ? cboSort2.SelectedItem.ToString() : "",
                    cboSort1.SelectedItem != null ? cboSort1.SelectedItem.ToString() : "");

                _IsCboSelectedIndexChanged = false;
            }
        }

        private void cboSort3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_IsCboSelectedIndexChanged == false)
            {
                _IsCboSelectedIndexChanged = true;

                if (cboSort3.Text == PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"))
                {
                    cboSort3.SelectedItem = null;
                }
                EnableCmdOk();

                PopulateCboSort(cboSort1, cboSort3.SelectedItem != null ? cboSort3.SelectedItem.ToString() : "",
                    cboSort2.SelectedItem != null ? cboSort2.SelectedItem.ToString() : "");
                PopulateCboSort(cboSort2, cboSort3.SelectedItem != null ? cboSort3.SelectedItem.ToString() : "",
                    cboSort1.SelectedItem != null ? cboSort1.SelectedItem.ToString() : "");

                _IsCboSelectedIndexChanged = false;
            }
        }

        private void EnableCmdOk()
        {
            if (cboSort1.SelectedItem == null && cboSort2.SelectedItem == null && 
                cboSort3.SelectedItem == null)
            {
                cmdOk.Enabled = false;
            }
            else
            {
                cmdOk.Enabled = true;
            }
        }

        private void PopulateCboSort(ComboBox cbo, string compare1, string compare2)
        {
            //Populate Combo Box.
            string temp = cbo.SelectedItem != null ? cbo.SelectedItem.ToString() : "";
            bool sortFlag = false;

            cbo.Items.Clear();
            cbo.Items.Add(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.None"));

            foreach (KeyValuePair<int, TupleItem> kv in _TupleItems)
            {
                if (kv.Value.ToString() != compare1 && kv.Value.ToString() != compare2)
                {
                    if (kv.Value.ToString() == temp)
                    {
                        sortFlag = true;
                    }
                    kv.Value.DisplayDescriptions = rdoDescriptions.Checked;
                    cbo.Items.Add(kv.Value);

                    if (sortFlag)
                    {
                        cbo.Text = kv.Value.ToString();
                        sortFlag = false;
                    }
                }
            }
        }

        private void Sort_Resize(object sender, EventArgs e)
        {
            Width = grpSort1.Left + grpSort1.Width + BORDER_PAD;
            cmdCancel.Top = grpColumnOptions.Bottom + PAD;
            cmdOk.Top = cmdCancel.Top;
        }
    }

    internal class TupleItem
    {
        private int _Position;
        private string _TupleString;
        private bool _Ascending;
        private string _AlphaHeader;
        private bool _DisplayDescriptions = true;


        public TupleItem(int position, string tupleString, bool ascending)
            :this(position, tupleString, ascending, String.Empty)
        {
        }

        public TupleItem(int position, string tupleString, bool ascending, string alphaHeader)
        {
            _Position = position;
            _TupleString = tupleString;
            _Ascending = ascending;
            _AlphaHeader = alphaHeader;
        }

        /// <summary>
        /// 
        /// </summary>
        public string TupleString
        {
            get { return _TupleString; }

            set { _TupleString = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public int Position
        {
            get { return _Position; }

            set { _Position = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public string AlphaHeader
        {
            get { return _AlphaHeader; }

            set { _AlphaHeader = value; }
        }

        public bool DisplayDescriptions
        {
            get { return _DisplayDescriptions; }
            set { _DisplayDescriptions = value; }
        }

        public override string ToString()
        {
            if (DisplayDescriptions)
            {
                return TupleString;
            }
            else
            {
                return AlphaHeader;
            }
        }
    }
}
