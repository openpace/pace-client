﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Titan.Pace.Application.Extensions;
using Titan.Pace.ExcelGridView;
using Titan.PafService;

namespace Titan.Pace.Application.Controls.TreeView
{
    internal static class PaceTreeViewExtension
    {
        /// <summary>
        /// Build a new tree view.
        /// </summary>
        /// <param name="tree">A ViewTree object to be used to build the tree.</param>
        /// <param name="simpleTrees">The simple trees that contain the tree information.</param>
        /// <param name="pafTree">PafSimpleTree to represent in the control.</param>
        /// <param name="hasCheckBoxes">True to use checkboxes</param>
        /// <param name="aliasTableToDisplay">Name of the alias table to use as the default</param>
        public static void BuildTreeControl(this PaceTreeView tree, SimpleTrees simpleTrees, pafSimpleDimTree pafTree, 
            bool hasCheckBoxes, string aliasTableToDisplay)
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Starting tree build for dimension: " + pafTree.id);

            //create a tree node collection.
            TreeNodeCollection tnc;

            int idx1 = -1, idx2 = -1;

            if (hasCheckBoxes)
            {
                idx2 = 1;
                idx1 = 1;
            }


            //check to see if their is alreay a root node inserted...
            if (tree.Nodes.Count > 0)
            {
                //set the tree node collection, to the section node.
                tnc = tree.Nodes[0].Nodes;

                //string alias = simpleTrees.GetAlias(pafTree.id, pafTree.rootKey, aliasTableName);
                string member = simpleTrees.GetMember(pafTree.id, pafTree.rootKey).key;


                Dictionary<string, string> aliasValues = simpleTrees.GetAliasValues(pafTree, pafTree.rootKey, pafTree.aliasTableNames);
                tnc.Add(new PaceTreeNode(pafTree.rootKey, member, aliasValues, idx1, idx2, aliasTableToDisplay));
                //increment the tree node collection.
                tnc = tree.Nodes[0].Nodes[0].Nodes;
            }
            else
            {
                //set the collection to the top of the tree
                tnc = tree.Nodes;
                //string alias = simpleTrees.GetAlias(pafTree.id, pafTree.rootKey, aliasTableName);
                string member = simpleTrees.GetMember(pafTree.id, pafTree.rootKey).key;

                Dictionary<string, string> aliasValues = simpleTrees.GetAliasValues(pafTree, pafTree.rootKey, pafTree.aliasTableNames);
                tnc.Add(new PaceTreeNode(pafTree.rootKey, member, aliasValues, idx1, idx2, aliasTableToDisplay));

                //increment the tree node collection.
                tnc = tree.Nodes[0].Nodes;
            }

            //See if the root has any children
            string[] children = simpleTrees.GetChildren(pafTree.id, pafTree.rootKey);

            //Make sure that their are some children
            if (children != null)
            {
                foreach (string child in children)
                {
                    //See if the children have children.
                    string[] grandChildren = simpleTrees.GetChildren(pafTree.id, child);
                    //string alias = simpleTrees.GetAlias(pafTree.id, child, aliasTableName);
                    string member = simpleTrees.GetMember(pafTree.id, child).key;


                    Dictionary<string, string> aliasValues = simpleTrees.GetAliasValues(pafTree, child, pafTree.aliasTableNames);

                    if (grandChildren == null)
                    {

                        //Debug the children if necessary.
                        if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Children of \"" + child + "\" not found.");
                        ////The node to be added doesn't have any children so don't add the "Dummy" keyword,
                        ////just add the items.
                        tnc.Add(new PaceTreeNode(child, member, aliasValues, idx1, idx2, aliasTableToDisplay));


                    }
                    else
                    {
                        if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Children of \"" + child + "\" found " + String.Join(",", grandChildren));

                        tnc.Add(new PaceTreeNode(
                           child,
                           member,
                           aliasValues,
                           aliasTableToDisplay,
                           idx1,
                           idx2,
                           new[] { tree.GetDummyNode() }));
                    }

                }
            }
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Ending tree build for dimension: " + pafTree.id);
        }



        /// <summary>
        /// Sets/selects a list of tree nodes on a tree view.
        /// </summary>
        /// <param name="tree">Tree view to set the selections.</param>
        /// <param name="nodes">List of tree nodes to select.</param>
        /// <param name="expandTreeAfterSel">Expand the tree control down the the selections.</param>
        public static void SetTreeSelections(this PaceTreeView tree, List<PaceTreeNode> nodes, bool expandTreeAfterSel)
        {
            //list of the nodes to expand.
            List<TreeNode> nodesToExpand = new List<TreeNode>();
            //If the user has made so selections.
            if (nodes != null)
            {
                //Clear out the selections.
                tree.UncheckAllNodes();
                tree.ClearCheckedNodes();

                //for each of the nodes.
                foreach (PaceTreeNode node in nodes)
                {
                    if (!tree.MultiSelectTree)
                    {
                        //No multi select, so just select the single node.
                        //This does not change the sel node for a single select.
                        //This fires an event which causes cal messages to appear.
                        //TTN-1570 (change matchWholeWord to true)
                        List<PaceTreeNode> tmp = tree.Find(node.Text, true, false, true);
                        if (tmp != null && tmp.Count > 0)
                        {
                            tree.SelectNode(tmp[0], true);
                            if (expandTreeAfterSel && !nodesToExpand.Contains(tmp[0].Parent) && tmp[0].Parent != null)
                            {
                                nodesToExpand.Add(tmp[0].Parent);
                            }
                        }
                    }
                    else
                    {
                        //Multi select, so add the node to the selectednode collection.
                        //TTN-1570 (change matchWholeWord to true)
                        List<PaceTreeNode> tmp = tree.Find(node.Text, true, false, true);
                        if (tmp != null && tmp.Count > 0)
                        {
                            foreach (PaceTreeNode n in tmp)
                            {
                                tree.SelectNode(n, true);
                                if (expandTreeAfterSel && !nodesToExpand.Contains(n.Parent) && tmp[0].Parent != null)
                                {
                                    nodesToExpand.Add(n.Parent);
                                }
                            }
                        }
                    }
                }
                if (expandTreeAfterSel)
                {
                    foreach (PaceTreeNode node in nodesToExpand)
                    {
                        if (!node.IsExpanded)
                        {
                            node.ExpandNode();
                        }
                    }
                }
            }
            else //the user has not made any selection.
            {
                //Just clear everything out.
                tree.UncheckAllNodes();
                tree.ClearCheckedNodes();
            }
        }
    }
}
