﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using Titan.Palladium.DataStructures;

namespace Titan.Pace.Application.Controls.TreeView
{
    /// <summary>
    /// 
    /// </summary>
    internal class SessionLockSelectedMembers
    {
        /// <summary>
        /// List of selected members.
        /// </summary>
        public List<string> Selections { get; set; }

        /// <summary>
        /// Default Constructor.
        /// </summary>
        public SessionLockSelectedMembers()
        {
            Selections = new List<string>();
        }

        /// <summary>
        /// Creates an object from a comma delimited string.
        /// </summary>
        /// <param name="selection"></param>
        public SessionLockSelectedMembers(string selection)
        {
            if (String.IsNullOrEmpty(selection))
            {
                Selections = new List<string>();
            }
            else
            {
                Selections = selection.Split(new[] {","}, StringSplitOptions.RemoveEmptyEntries).ToList();
            }
        }

        /// <summary>
        /// Creates an object from a comma delimited string (object).
        /// </summary>
        /// <param name="selection"></param>
        public SessionLockSelectedMembers(object selection)
            : this(Convert.ToString(selection))
        {
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="selections"></param>
        public SessionLockSelectedMembers(List<string> selections)
        {
            Selections = selections;
        }

        /// <summary>
        /// Returns true if the Selections property is null or empty.
        /// </summary>
        /// <returns></returns>
        public bool SelectionEmpty()
        {
            return Selections == null || Selections.Count == 0;
        }

        /// <summary>
        /// Returns true if the Selections property contains more than one member.
        /// </summary>
        /// <returns></returns>
        public bool MultiMember()
        {
            if (Selections == null || Selections.Count == 0) return false;

            return Selections.Count > 1;
        }

        /// <summary>
        /// Return true if the specified member exists in the Selections list.
        /// </summary>
        /// <param name="memberName"></param>
        /// <returns></returns>
        public bool ContainsMember(string memberName)
        {
            if (Selections == null || Selections.Count == 0) return false;

            return Selections.Contains(memberName);
        }

        /// <summary>
        /// Returns the single (top) item from the Selections property.
        /// </summary>
        /// <returns></returns>
        public string SingleSelection()
        {
            if (Selections == null || Selections.Count == 0) return String.Empty;
            return Selections[0];
        }

        /// <summary>
        /// Generates and updates the specified list of intersections, using the Selections property.
        /// </summary>
        /// <param name="intersections"></param>
        /// <param name="dimensionPriority"></param>
        /// <param name="dimensionName">Dimension name of the member.</param>
        /// <returns></returns>
        public List<Intersection> CreateIntersections(List<Intersection> intersections, IList<string> dimensionPriority, string dimensionName)
        {
            if (intersections == null || intersections.Count == 0)
                return CreateIntersections(dimensionPriority, dimensionName);

            List<Intersection> result = new List<Intersection>(Selections.Count);
            foreach (Intersection i in intersections)
            {
                foreach (string member in Selections)
                {
                    Intersection temp = (Intersection)i.Clone();
                    temp.AddCoordinate(dimensionName, member);
                    result.Add(temp);
                }

            }

            return result;
        }

        /// <summary>
        /// Generates a list of Intersections from the Selections property.
        /// </summary>
        /// <param name="dimensionPriority"></param>
        /// <param name="dimensionName">Dimension name of the member.</param>
        /// <returns></returns>
        public List<Intersection> CreateIntersections(IList<string> dimensionPriority, string dimensionName)
        {
            List<Intersection> result = new List<Intersection>(Selections.Count);
            foreach (string member in Selections)
            {
                Intersection temp  = new Intersection(dimensionPriority);
                temp.AddCoordinate(dimensionName, member);
                result.Add(temp);
            }
            return result;
        }

        /// <summary>
        /// Overridden ToString() 
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (Selections == null || Selections.Count == 0) return String.Empty;

            return String.Join(",", Selections.ToArray());
        }
    }
}