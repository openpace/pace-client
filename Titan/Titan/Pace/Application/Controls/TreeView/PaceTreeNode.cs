﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Titan.Pace.Application.Extensions;

namespace Titan.Pace.Application.Controls.TreeView
{
    internal class PaceTreeNode : TreeNode
    {
        /// <summary>
        /// Member name.
        /// </summary>
        public string MemberName { get; set; }

        /// <summary>
        /// Currently displayed alias table.
        /// </summary>
        public string AliasTableToDisplay { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Dictionary<string, string> AliasTableMap { get; set; }

        /// <summary>
        /// Draw state for the TreeNode.
        /// </summary>
        public DrawState DrawCheckBox { get; set; }

        

        /// <summary>
        /// 
        /// </summary>
        public enum PaceTreeDimensionType 
        {
            Attr,
            Base,
        }

        public enum DrawState
        {
            Enabled,
            Disabled,
        }

        public PaceTreeNode()
        {
            DrawCheckBox = DrawState.Disabled;
            AliasTableMap = new Dictionary<string, string>();
        }

        public PaceTreeNode(string text)
        {
            Text = text;
        }

        /// <summary>
        /// Builds a new pace tree node.
        /// </summary>
        /// <param name="text">The text of the node.</param>
        /// <param name="tag">The tag of the node.</param>
        /// <param name="imageIndex">The index of the icon for the item.</param>
        /// <param name="selectedImageIndex">The index of the icon for the item, when it is selected.</param>
        /// <returns>A new tree node, null if error occurs.</returns>
        public PaceTreeNode(string text, object tag, int imageIndex, int selectedImageIndex)
        {
            Text = text;
            Name = text;
            Tag = tag;
            ImageIndex = imageIndex;
            SelectedImageIndex = selectedImageIndex;

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Adding tree node: text=" + text + ", tag=" + tag + ", ImageIndex=" + imageIndex + ", SelectedImageIndex=" + selectedImageIndex);
        }

        /// <summary>
        /// Builds a new tree node.
        /// </summary>
        /// <param name="tag">The tag of the node.</param>
        /// <param name="memberName">The member name.</param>
        /// <param name="aliasTableMap">The dictioanry of alias values for the node</param>
        /// <param name="imageIndex">The index of the icon for the item.</param>
        /// <param name="selectedImageIndex">The index of the icon for the item, when it is selected.</param>
        /// <param name="aliasTableToDisplay"></param>
        /// <returns>A new tree node, null if error occurs.</returns>
        public PaceTreeNode(object tag, string memberName, Dictionary<string, string> aliasTableMap,
            int imageIndex, int selectedImageIndex, string aliasTableToDisplay)
        {
            Tag = tag;
            MemberName = memberName;
            AliasTableMap = aliasTableMap;
            ImageIndex = imageIndex;
            SelectedImageIndex = selectedImageIndex;
            AliasTableToDisplay = aliasTableToDisplay;


            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Adding tree node: tag=" + tag + ", ImageIndex=" + imageIndex + ", SelectedImageIndex=" + selectedImageIndex);

            UpdateNodeText();

            Name = Text;
        }

        /// <summary>
        /// Builds a new tree node.
        /// </summary>
        /// <param name="tag">The tag of the node.</param>
        /// <param name="memberName">Member name.</param>
        /// <param name="aliasTableMap">The dictioanry of alias values for the node.</param>
        /// <param name="aliasTableToDisplay"></param>
        /// <returns>A new tree node, null if error occurs.</returns>
        public PaceTreeNode(object tag, string memberName, Dictionary<string, string> aliasTableMap,
            string aliasTableToDisplay)
        {
            Tag = tag;
            MemberName = memberName;
            AliasTableMap = aliasTableMap;
            AliasTableToDisplay = aliasTableToDisplay;

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Adding tree node: tag=" + tag);

            UpdateNodeText();

            Name = Text;
        }

        /// <summary>
        /// Builds a new tree node.
        /// </summary>
        /// <param name="tag">The value(or tag) of the node.</param>
        /// <param name="memberName">Member name.</param>
        /// <param name="aliasTableMap">The dictioanry of alias values for the node.</param>
        /// <param name="aliasTableToDisplay"></param>
        /// <param name="imageIndex">The index of the icon for the item.</param>
        /// <param name="selectedImageIndex">The index of the icon for the item, when it is selected.</param>
        /// <param name="children">The children of the new tree node.</param>
        /// <returns>A new tree node, null if error occurs.</returns>
        public PaceTreeNode(object tag, string memberName, Dictionary<string, string> aliasTableMap,
            string aliasTableToDisplay, int imageIndex, int selectedImageIndex, PaceTreeNode[] children)
        {
            Tag = tag;
            MemberName = memberName;
            AliasTableMap = aliasTableMap;
            AliasTableToDisplay = aliasTableToDisplay;
            ImageIndex = imageIndex;
            SelectedImageIndex = selectedImageIndex;
            Nodes.AddRange(children);

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Adding tree node: tag=" + tag);

            UpdateNodeText();

            Name = Text;
        }


        /// <summary>
        /// Builds a new tree node.
        /// </summary>
        /// <param name="tag">The tag of the node.</param>
        /// <param name="memberName">Member name.</param>
        /// <param name="aliasTableMap">The dictioanry of alias values for the node.</param>
        /// <param name="aliasTableToDisplay"></param>
        /// <param name="children">The children of the new tree node.</param>
        /// <returns>A new tree node, null if error occurs.</returns>
        public PaceTreeNode(object tag, string memberName, Dictionary<string, string> aliasTableMap,
            string aliasTableToDisplay, PaceTreeNode[] children)
        {
            Tag = tag;
            MemberName = memberName;
            AliasTableMap = aliasTableMap;
            AliasTableToDisplay = aliasTableToDisplay;
            Nodes.AddRange(children);

            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Adding tree node: tag=" + tag);

            UpdateNodeText();

            Name = Text;
        }

        /// <summary>
        /// Gets the PaceTreeView parent
        /// </summary>
        public PaceTreeView PaceTreeView 
        { 
            get
            {
                if (TreeView is PaceTreeView)
                {
                    return (PaceTreeView) TreeView;
                }
                return null;
            }
        }

        public void UpdateNodeText()
        {
            string value;
            if (AliasTableMap.TryGetValue(AliasTableToDisplay, out value))
            {
                Text = value;
            }
            else
            {
                Text = MemberName;
            }

        }

        /// <summary>
        /// Check all the children of a PaceTreeNode.
        /// </summary>
        public void CheckAllChildren()
        {
            if (!PaceTreeView.MultiSelectTree) return;
            this.Expand();
            foreach (PaceTreeNode tn in Nodes)
            {
                tn.ExpandAll();
                if (!tn.Checked)
                {
                    tn.Checked = true;
                }
                if (tn.Nodes.Count > 0)
                {
                    CheckNodes(tn.Nodes);
                }
            }
        }

        /// <summary>
        /// Returns a list of ancestor nodes for the current node.
        /// </summary>
        /// <param name="addCurrentNode">Adds the current node as the first item in the list.</param>
        /// <returns></returns>
        public IList<PaceTreeNode> GetAncestorNodes(bool addCurrentNode)
        {
            IList<PaceTreeNode> nodesWithChildren = new List<PaceTreeNode>();
            GetParentNodes(nodesWithChildren, this.Parent.ToPaceTreeNode());
            if (addCurrentNode)
            {
                nodesWithChildren.Insert(0, this);
            }
            return nodesWithChildren;
        }

        /// <summary>
        /// Check and select all children in a recursive fashion
        /// </summary>
        private void CheckNodes(TreeNodeCollection treeNode)
        {
            foreach (PaceTreeNode tn in treeNode.Cast<PaceTreeNode>().Where(tn => tn != null))
            {
                if (!tn.Checked)
                {
                    tn.Checked = true;
                }

                if (tn.Nodes.Count > 0)
                {
                    CheckNodes(tn.Nodes);
                }
            }
        }

        private static void GetParentNodes(IList<PaceTreeNode> nodesWithChildren, PaceTreeNode parentNode)
        {
            if (parentNode != null)
            {
                nodesWithChildren.Add(parentNode.ToPaceTreeNode());
                GetParentNodes(nodesWithChildren, parentNode.Parent.ToPaceTreeNode());
            }
           
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            PaceTreeNode node = (PaceTreeNode) obj;
            try
            {
                if(Text.Equals(node.Text) && MemberName.Equals(node.MemberName))
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }

        }
    }
  
}
