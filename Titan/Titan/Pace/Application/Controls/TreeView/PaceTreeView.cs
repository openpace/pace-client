﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using Titan.Pace.Application.Utilities;
using Titan.Properties;

namespace Titan.Pace.Application.Controls.TreeView
{
    internal class PaceTreeView : System.Windows.Forms.TreeView
    {
        private readonly Image _redX;

        private readonly Logging _logger;

        private const int WM_LBUTTONDBLCLK = 0x203;

        private bool IsShiftSelMode = false;

        private readonly bool _paintWithImage = true;

        private readonly Color _disabledColor;

        private readonly Color _enabledColor;

        #region GettersSetters

        /// <summary>
        /// 
        /// </summary>
        public string AliasTableToDisplay { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string DimensionName { get; set; }

        /// <summary>
        /// Get/set if this tree supports multiple selections.
        /// </summary>
        public bool MultiSelectTree { get; set; }

        private bool IsShiftDown { get; set; }

        /// <summary>
        /// The node selected (checked). In the case of a list it will be the last node in the list. If a node is unchecked it will be the previous node in tree order
        /// If no nodes are selected it will be null.
        /// </summary>
        private PaceTreeNode LastSelectedNode { get; set; }

        /// <summary>
        /// Only allow the user to make leaf level selections.
        /// </summary>
        public bool OnlyAllowLeafLevelSelections { get; set; }

        /// <summary>
        /// The type of dimension.
        /// </summary>
        public PaceTreeNode.PaceTreeDimensionType DimensionType { get; set; }

        /// <summary>
        /// Set of member name strings that cannot be selected in the tree.
        /// </summary>
        public HashSet<string> UnselectableMbrName { get; set; }

        ///// <summary>
        ///// Set of collection of tree nodes that cannot be selected.
        ///// </summary>
        //public HashSet<PaceTreeNode> UnselectableTreeNodes { get; set; }

        /// <summary>
        /// Allow the root node of the tree to be selected.
        /// </summary>
        public bool AllowRootNodeSelection { get; set; }

        /// <summary>
        /// Do not allow Ancestor Nodes to be selected.
        /// </summary>
        public bool DisallowAncestorSelections { get; set; }

        /// <summary>
        /// List of levels where nodes cannot be selected.
        /// </summary>
        public List<int> UnselectableNodeLevels { get; set; }

        private int _selectableNodeLevel;

        /// <summary>
        /// The Level where nodes in the tree can be selected.
        /// If this is set to 2, then only nodes at level 2 can be selected.
        /// </summary>
        public int SelectableNodeLevel
        {
            get { return _selectableNodeLevel; }
            set { _selectableNodeLevel = value; }
        }

        /// <summary>
        /// Cached search results.  Populated after Find() is called.
        /// </summary>
        public Dictionary<string, List<PaceTreeNode>> SearchResults { get; set; }

        /// <summary>
        /// Error message for any errors that occur during OnBeforeCheck event. 
        /// </summary>
        public string CannotSelectNodeErrorMessage { get; private set; }

        /// <summary>
        /// Error message for any errors that occur during OnBeforeCheck event. 
        /// </summary>
        public string CannotSelectNodeErrorWarning { get; private set; }

        /// <summary>
        /// Returns the first checked node, see CheckedNodes for the entire list.
        /// This is to be used for a single select tree view.
        /// </summary>
        public PaceTreeNode CheckedNode
        {
            get
            {
                List<PaceTreeNode> checkedNodes = CheckedNodes;
                if (checkedNodes != null && checkedNodes.Count > 0)
                {
                    return checkedNodes[0];
                }
                return null;
            }
        }

        /// <summary>
        /// The list of currently checked nodes in the Tree View.
        /// </summary>
        public  List<PaceTreeNode> CheckedNodes { get; protected set; }

        /// <summary>
        /// Gets the member names of all the currently checked nodes in a list
        /// </summary>
        /// <returns></returns>
        public List<string> CheckedNodesAsString()
        {
            List<string> nodes = new List<string>(CheckedNodes.Count);
            nodes.AddRange(CheckedNodes.Select(node => node.MemberName));
            return nodes;
        }

        /// <summary>
        /// Gets the Root PaceTreeNode, or null if the node does not exits.
        /// </summary>
        public PaceTreeNode RootNode 
        {
            get
            {
                if(Nodes != null && Nodes.Count > 0)
                {
                    return (PaceTreeNode) Nodes[0];
                }
                return null;
            }
        }

        public PaceTreeNode FirstSelectableNode
        {
            get
            {
                if(IsNodeSelectable(RootNode))
                {
                    return RootNode;
                }

                PaceTreeNode outNode = null;

                GetFirstSelectableTreeNode(RootNode.Nodes, ref outNode);

                return outNode;

            }
        }

        #endregion GettersSetters

        private PaceTreeNode _lastFoundNode;

        public PaceTreeView()  
        {
            Graphics graphics = CreateGraphics();
            float dpiX = graphics.DpiX;
            float dpiY = graphics.DpiY;
            if (dpiX >= 144 && dpiY >= 144)
            {
                _paintWithImage = false;
            }
            else if (dpiX >= 120 && dpiY >= 120)
            {
                _paintWithImage = false;
            }
            
            _disabledColor = Color.FromArgb(240, 238, 238);
            _enabledColor = Color.FromArgb(0, 0, 0, 0);

            UnselectableNodeLevels = new List<int>();
            UnselectableMbrName = new HashSet<string>();
            //UnselectableTreeNodes = new HashSet<PaceTreeNode>();
            DrawMode = TreeViewDrawMode.OwnerDrawText;
            SelectableNodeLevel = -1;
            DisallowAncestorSelections = false;
            AllowRootNodeSelection = false;

            DoubleBuffered = true;

            CheckBoxes = false;

            CheckedNodes = new List<PaceTreeNode>();

            _redX = Resources.redx;

            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.DoubleBuffer, true);
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.Selectable, true);
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);

            SearchResults = new Dictionary<string, List<PaceTreeNode>>(StringComparer.Ordinal);

            _logger = new Logging(MethodBase.GetCurrentMethod().DeclaringType.ToString());
        }

        //protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        //{
        //    if (keyData == (Keys.ShiftKey | Keys.Shift))
        //        this.IsShiftDown = true;
        //    else 
        //        this.IsShiftDown = false;


        //    return base.ProcessCmdKey(ref msg, keyData);
        //}       


        #region public methods

        /// <summary>
        /// Gets a tree node with the "Dummy" keyword filled it the name, tag, and text properties.
        /// </summary>
        /// <returns>a TreeNode</returns>
        public PaceTreeNode GetDummyNode()
        {
            return new PaceTreeNode("Dummy") {Name = "Dummy", Tag = "Dummy", MemberName="Dummy"};
        }

        /// <summary>
        /// Gets the lowest level node in a list of PaceTreeNodes
        /// </summary>
        /// <param name="nodes">List of nodes.</param>
        /// <returns></returns>
        public int LowestNodeLevel(List<PaceTreeNode> nodes)
        {
            int level = 0;
            if (nodes != null && nodes.Count > 0)
            {
                level = nodes[0].Level;
                int level1 = level;
                foreach (PaceTreeNode t in nodes.Where(t => t.Level > level1))
                {
                    level = t.Level;
                }
            }
            return level;
        }

        /// <summary>
        /// Gets the lowest level node in a list of PaceTreeNodes
        /// </summary>
        /// <param name="nodes">List of nodes.</param>
        /// <returns></returns>
        public PaceTreeNode LowestLevelNode(List<PaceTreeNode> nodes)
        {
            PaceTreeNode tmp = null;
            if (nodes != null && nodes.Count > 0)
            {
                tmp = nodes[0];
                int level = tmp.Level;
                int level1 = level;
                foreach (PaceTreeNode t in nodes.Where(t => t.Level > level1))
                {
                    //level = t.Level;
                    tmp = t;
                }
            }
            return tmp;
        }

        /// <summary>
        /// A modified implementation of the Microsoft TreeView.Find() method.
        /// </summary>
        /// <param name="searchText">String to search for</param>
        /// <param name="searchAliasTables">True to search in the alias tables, false to just search
        /// the text propery.</param>
        /// <param name="matchCase">True to perform a case sensative search.</param>
        /// <param name="matchWholeWord">True to match the entire word, false to return a partial match.</param>
        /// <param name="tnc">Collection of Pace Tree Nodes to search.</param>
        /// <returns>A list of found PaceTreeNode</returns>
        public List<PaceTreeNode> Find(string searchText, bool searchAliasTables, bool matchCase,
            bool matchWholeWord, TreeNodeCollection tnc)
        {

            DateTime startTime = DateTime.Now;
            List<PaceTreeNode> results = null;
            HashSet<PaceTreeNode> set = null;
            if (tnc != null)
            {


                set = Find(searchText, true, searchAliasTables, matchCase, matchWholeWord, tnc);

                if (set != null && set.Count > 0)
                {
                    results = set.ToList();
                }

                if (results != null && results.Count > 0)
                {

                    if (SearchResults.ContainsKey(searchText))
                    {
                        SearchResults.Remove(searchText);
                        SearchResults.Add(searchText, results);
                    }
                    else
                    {
                        SearchResults.Add(searchText, results);
                    }

                    _lastFoundNode = results[0];
                }

            }
            _logger.Debug("Find: " + searchText + ", runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            return results;

        }


        /// <summary>
        /// A modified implementation of the Microsoft TreeView.Find() method.
        /// </summary>
        /// <param name="searchText">String to search for</param>
        /// <param name="searchAliasTables">True to search in the alias tables, false to just search
        /// the text propery.</param>
        /// <param name="matchCase">True to perform a case sensative search.</param>
        /// <param name="matchWholeWord">True to match the entire word, false to return a partial match.</param>
        /// <returns>A list of found PaceTreeNode</returns>
        public List<PaceTreeNode> Find(string searchText, bool searchAliasTables, bool matchCase, 
            bool matchWholeWord)
        {

            return Find(searchText, searchAliasTables, matchCase, matchWholeWord, Nodes);

        }


        /// <summary>
        /// A modified implementation of the Microsoft TreeView.Find() method.
        /// </summary>
        /// <param name="searchText">String to search for</param>
        /// <param name="searchAliasTables">True to search in the alias tables, false to just search
        /// the text propery.</param>
        /// <param name="matchCase">True to perform a case sensative search.</param>
        /// <param name="matchWholeWord">True to match the entire word, false to return a partial match.</param>
        /// <param name="startNode">PaceTreeNode to start the search.</param>
        /// <returns>A list of found PaceTreeNode</returns>
        public List<PaceTreeNode> Find(string searchText, bool searchAliasTables, bool matchCase,
            bool matchWholeWord, PaceTreeNode startNode)
        {

            return Find(searchText, searchAliasTables, matchCase, matchWholeWord, startNode.Nodes);

        }

        /// <summary>
        /// Returns a list of ancestor nodes for the current node.
        /// </summary>
        /// <returns></returns>
        public IList<PaceTreeNode> GetAncestorNodes()
        {
            IList<PaceTreeNode> nodesWithChildren = new List<PaceTreeNode>();
            GetParentNodes(nodesWithChildren, this.Nodes);
            return nodesWithChildren;
        }


        /// <summary>
        /// Selectes a node in the tree view.
        /// </summary>
        /// <param name="tn">Tree node to select.</param>
        /// <param name="checkNode">Check the tree node.</param>
        public void SelectNode(TreeNode tn, bool checkNode)
        {
            if (tn == null) return;
            SelectedNode = tn;
            if (SelectedNode == null) return;
            if (checkNode != SelectedNode.Checked)
            {
                if (!SelectedNode.IsVisible)
                {
                    SelectedNode.EnsureVisible();
                }
                SelectedNode.Checked = checkNode;
            }
            SelectedNode.EnsureVisible();
        }

        /// <summary>
        /// Uncheck the checked nodes.
        /// </summary>
        public void UncheckAllNodes() {

            foreach (PaceTreeNode tn in Nodes) {
                if (tn.Checked) {
                    //this fires the before  and after check events taking care of the collection management
                    tn.Checked = false;
                }

                if (tn.Nodes.Count > 0) {
                    UncheckNodes(tn.Nodes);
                }
            }
        }



        /// <summary>
        /// Remove all the checked nodes from the _checkedNodes list. 
        /// </summary>
        public void ClearCheckedNodes()
        {
            if (CheckedNodes != null && CheckedNodes.Count > 0)
            {
                CheckedNodes.Clear();
                SelectableNodeLevel = -1;
                //UnselectableTreeNodes = null;
            }
        }

        /// <summary>
        /// Walk the tree view.
        /// </summary>
        public void TraverseTreeView()
        {
            foreach (PaceTreeNode n in Nodes)
            {
                PrintRecursive(n);
            }
        }

        /// <summary>
        /// Disable a Tree Node Collection.
        /// </summary>
        /// <param name="tnc">Array of tree nodes.</param>
        public void DisableTreeNodes(TreeNodeCollection tnc)
        {
            foreach (PaceTreeNode tn in tnc)
            {
                DisableNode(tn);

                if (tn.Nodes.Count > 0)
                {
                    DisableTreeNodes(tn.Nodes);
                }
            }
        }


        /// <summary>
        /// Disable a pace tree node.
        /// </summary>
        /// <param name="node">Pace Tree Node</param>
        private void DisableNode(PaceTreeNode node)
        {
            //TTN-2560
            //if (DimensionType == PaceTreeNode.PaceTreeDimensionType.Attr && node == RootNode)
            //{
            //    return;
            //}

            node.DrawCheckBox = PaceTreeNode.DrawState.Disabled;
        }

        /// <summary>
        /// Enable a Tree Node Collection.
        /// </summary>
        /// <param name="paceTreeNodes">Array of Pace Tree Nodes nodes.</param>
        public void EnableTreeNode(List<PaceTreeNode> paceTreeNodes)
        {
            foreach (PaceTreeNode tn in paceTreeNodes)
            {
                EnableNode(tn);

                if (tn.Nodes.Count > 0)
                {
                    EnableTreeNode(tn.Nodes);
                }
            }
        }

        /// <summary>
        /// Enable a Tree Node Collection.
        /// </summary>
        /// <param name="tnc">Array of tree nodes.</param>
        public void EnableTreeNode(TreeNodeCollection tnc)
        {
            foreach (PaceTreeNode tn in tnc)
            {
                EnableNode(tn);

                if (tn.Nodes.Count > 0)
                {
                    EnableTreeNode(tn.Nodes);
                }
            }
        }

        /// <summary>
        /// Enable the tree node.
        /// </summary>
        /// <param name="tn">Tree node</param>
        public void EnableNode(PaceTreeNode tn)
        {
            tn.DrawCheckBox = PaceTreeNode.DrawState.Enabled;
            //tn.ForeColor = nodeColor;
        }



        /// <summary>
        /// Selects all the nodes in the tree
        /// </summary>
        public void SelectAllNodes()
        {
            if (MultiSelectTree)
            {
                foreach (PaceTreeNode tn in Nodes)
                {
                    tn.ExpandAll();
                    if (!tn.Checked)
                    {
                        tn.Checked = true;
                    }
                    if (tn.Nodes.Count > 0)
                    {
                        SelectNodes(tn.Nodes);
                    }
                }
            }

        }
        
        



        #endregion public methods





        #region private methods


        /// <summary>
        /// Check and select all children in a recursive fashion
        /// </summary>
        private void SelectNodes(TreeNodeCollection treeNode)
        {
            foreach (PaceTreeNode tn in treeNode.Cast<PaceTreeNode>().Where(tn => tn != null))
            {
                if (!tn.Checked)
                {
                    tn.Checked = true;
                }

                if (tn.Nodes.Count > 0)
                {
                    SelectNodes(tn.Nodes);
                }
            }
        }


        /// <summary>
        /// Uncheck the checked nodes.
        /// </summary>
        private void UncheckNodes(TreeNodeCollection treeNode)
        {
            foreach (PaceTreeNode tn in treeNode)
            {
                if (tn.Checked)
                {
                    tn.Checked = false;
                    //CheckedNodes.Remove(tn);
                    RemoveCheckedNode(tn);
                    if (tn.Nodes.Count > 0)
                    {
                        UncheckNodes(tn.Nodes);
                    }
                }
                else
                {
                    if (tn.Nodes.Count > 0)
                    {
                        UncheckNodes(tn.Nodes);
                    }
                }
            }
        }


        private void PrintRecursive(PaceTreeNode treeNode)
        {
            if (treeNode != null)
            {
                foreach (PaceTreeNode ptn in treeNode.Nodes)
                {
                    PrintRecursive(ptn);
                }
            }
        }

        /// <summary>
        /// A modified implementation of the Microsoft TreeView.Find() method.
        /// </summary>
        /// <param name="searchText">String to search for</param>
        /// <param name="searchAllChildren">Search for all children - Recursive search.</param>
        /// <param name="matchCase">True to perform a case sensative search.</param>
        /// <param name="matchWholeWord">True to match the entire word, false to return a partial match.</param>
        /// <param name="searchAliasTables">True to search in the alias tables, false to just search
        /// the text propery.</param>
        /// <param name="tnc">Tree node collection to search.</param>
        /// <returns>A set of found PaceTreeNode</returns>
        private HashSet<PaceTreeNode> Find(string searchText,
            bool searchAllChildren,
            bool searchAliasTables,
            bool matchCase,
            bool matchWholeWord,
            TreeNodeCollection tnc)
        {
            HashSet<PaceTreeNode> res = new HashSet<PaceTreeNode>();
            //Wildcard wildcard = null;
            bool hasWildcard = false;

            if (searchText.IndexOf('*') > 0 || searchText.IndexOf('?') > 0)
            {
                hasWildcard = true;
            }



            StringComparison sc = StringComparison.OrdinalIgnoreCase;
            RegexOptions ro = RegexOptions.IgnoreCase;
            if (matchCase)
            {
                sc = StringComparison.Ordinal;
                ro = RegexOptions.None;
            }

            Find(searchText, hasWildcard, searchAllChildren, sc, matchWholeWord, ro, searchAliasTables, tnc, res);
            return res;
        }

        /// <summary>
        /// This is a modified implementation of the Microsoft TreeView.Find() method.
        /// </summary>
        /// <param name="searchText">Text to search for.</param>
        /// <param name="hasWildcard">Signals that the search text parmeter has wildcard characters.</param>
        /// <param name="searchAllChildren">Search for all children - Recursive search.</param>
        /// <param name="stringComparison">Case sensative search.</param>
        /// <param name="matchWholeWord">True to match the entire word, false to return a partial match.</param>
        /// <param name="regexOptions">Regular expression case sensative option (if necessary)</param>
        /// <param name="searchAliasTables">True to search in the alias tables, false to just search
        /// the text propery.</param>
        /// <param name="tnc">Tree node collection to search.</param>
        /// <param name="foundTreeNodes">set of tree node that have been found so far.</param>
        /// <returns>A set of found PaceTreeNode</returns>
        private HashSet<PaceTreeNode> Find(string searchText,
                                        bool hasWildcard,
                                        bool searchAllChildren,
                                        StringComparison stringComparison,
                                        bool matchWholeWord,
                                        RegexOptions regexOptions,
                                        bool searchAliasTables,
                                        TreeNodeCollection tnc,
                                        HashSet<PaceTreeNode> foundTreeNodes)
        {
            for (int i = 0; i < tnc.Count; i++)
            {
                //cast to our subclass.
                PaceTreeNode node = (PaceTreeNode)tnc[i];

                Wildcard wildcard = null;

                //just a null check.
                if (node == null)
                    break;

                //this code block looks to see if the next child node is a "Dummy" node
                //if so the tree has not been fully build, so we have to expand the next node
                //so if will be fully populated so the search can be correctly done, becuase I build the
                //tree as the user click the nodes (the correct way to build a tree :-) ).
                if (node.Nodes != null && node.Nodes.Count > 0 &&
                    node.FirstNode != null &&
                    node.FirstNode.Text.Equals("Dummy"))
                {
                    node.ExpandAll();
                }
                else if (node.Nodes != null && node.Nodes.Count > 0 &&
                    node.Nodes[0].FirstNode != null &&
                    node.Nodes[0].FirstNode.Text.Equals("Dummy"))
                {
                    node.ExpandAll();
                }

                //some debug statements.
                _logger.Debug("Searching node: " + node.Text);

                //if the search text has a wildcard, then create a wildcard object.
                if (hasWildcard)
                {
                    wildcard = new Wildcard(searchText, regexOptions);
                }



                //if you searching thru all the alias tables.
                if (searchAliasTables)
                {
                    //if three is nothing in the alias table map, then break.
                    if (node.AliasTableMap == null) break;

                    foreach (KeyValuePair<string, string> kvp in node.AliasTableMap)
                    {
                        //if (kvp.Value.IndexOf(searchText, stringComparison) >= 0)
                        if (DoSearch(kvp.Value, searchText, matchWholeWord, stringComparison, wildcard) >= 0)
                        {
                            foundTreeNodes.Add(node);
                            break;
                        }
                    }
                }
                else
                {//else just look that the text of the node.
                    //if (node.Text.IndexOf(searchText, stringComparison) >= 0)
                    if (DoSearch(node.Text, searchText, matchWholeWord, stringComparison, wildcard) >= 0)
                    {
                        foundTreeNodes.Add(node);
                        //break;
                    }
                }
                //finally just look at the member name.
                if (node.MemberName.Equals(searchText))
                {
                    foundTreeNodes.Add(node);
                    break;
                }
            }
            //if we are going recursive, the do it.
            if (searchAllChildren)
            {
                for (int i = 0; i < tnc.Count; i++)
                {
                    if (tnc[i].Nodes.Count > 0)
                    {
                        foundTreeNodes = Find(searchText,
                            hasWildcard,
                            true,
                            stringComparison,
                            matchWholeWord,
                            regexOptions,
                            searchAliasTables,
                            tnc[i].Nodes,
                            foundTreeNodes);
                    }
                }
            }

            return foundTreeNodes;
        }

        /// <summary>
        /// The actual search/find comparsion method.
        /// </summary>
        /// <param name="sourceText">Source text.</param>
        /// <param name="searchText">Text to find.</param>
        /// <param name="matchWholeWord">Should the 2 words match.</param>
        /// <param name="stringComparison">String comparions object.</param>
        /// <param name="wildcard">Regex object</param>
        /// <returns>-1 if strings don't match, >=0 if strings do match.</returns>
        private int DoSearch(string sourceText, string searchText, bool matchWholeWord,
            StringComparison stringComparison, Regex wildcard)
        {
            int i = -1;

            if (wildcard != null)
            {
                if (wildcard.IsMatch(sourceText))
                {
                    i = 1;
                }
            }
            else
            {
                if(matchWholeWord)
                {
                    if(sourceText.Equals(searchText, stringComparison))
                    {
                        i = 1;
                    }
                }
                else
                {
                    i = sourceText.IndexOf(searchText, stringComparison);
                }
            }

            if (i >= 0)
            {
                _logger.Debug("Found node: " + sourceText);
            }

            return i;

        }




        /// <summary>
        /// Swaps the checked status to two Pace tree nodes.
        /// </summary>
        /// <param name="treeNodeToUncheck">Pace tree node to uncheck.</param>
        /// <param name="treeNodeToCheck">Pace tree node to check.</param>
        private void SwapCheckedNode(IList<PaceTreeNode> treeNodeToUncheck, TreeNode treeNodeToCheck)
        {
            if (treeNodeToUncheck.Count > 1)
            {
                return;
            }

            SwapCheckedNode(treeNodeToUncheck[0], treeNodeToCheck);
        }

        /// <summary>
        /// Swaps the checked status to two Pace tree nodes.
        /// </summary>
        /// <param name="treeNodeToUncheck">Pace tree node to uncheck.</param>
        /// <param name="treeNodeToCheck">Pace tree node to check.</param>
        private static void SwapCheckedNode(TreeNode treeNodeToUncheck, TreeNode treeNodeToCheck)
        {
            if (treeNodeToUncheck != null && treeNodeToCheck != null)
            {
                treeNodeToUncheck.Checked = false;
                treeNodeToCheck.Checked = true;
            }
        }

        private bool IsNodeSelectable(PaceTreeNode node)
        {
            CannotSelectNodeErrorMessage = String.Empty;
            CannotSelectNodeErrorWarning = String.Empty;

            if (!AllowRootNodeSelection)
            {
                if (UnselectableMbrName.Contains(node.MemberName) || UnselectableNodeLevels.Contains(node.Level))
                {
                    CannotSelectNodeErrorMessage = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.CannotSelectRootLevelNode");
                    return false;
                }
            }
            if (SelectableNodeLevel != -1 && SelectableNodeLevel != node.Level)
            {
                CannotSelectNodeErrorWarning = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.MemberAncestorDes");
                return false;
            }
            if (OnlyAllowLeafLevelSelections && node.Nodes.Count > 0)
            {
                CannotSelectNodeErrorMessage = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.CanOnlySelectLeafLevelNodes");
                return false;
            }
            if (DisallowAncestorSelections && CheckedNodes.Count > 0)
            {
                //This will catch nodes selected above currently checked nodes.
                if (CheckedNodes.Select(checkedNode => checkedNode.GetAncestorNodes(true).ToList()).Any(ancestors => ancestors.Contains(node)))
                {
                    CannotSelectNodeErrorWarning = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.MemberAncestorDes");
                    return false;
                }
                //This will catch nodes select below the currently checked node.
                List<PaceTreeNode> unselNodex = node.GetAncestorNodes(false).ToList();
                if (CheckedNodes.Any(unselNodex.Contains))
                {
                    CannotSelectNodeErrorWarning = PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.MemberAncestorDes");
                    return false;
                }
            }

            return true;
        }

        private static void GetParentNodes(IList<PaceTreeNode> nodesWithChildren, TreeNodeCollection parentNodes)
        {
            foreach (PaceTreeNode node in parentNodes.Cast<PaceTreeNode>().Where(node => node.Nodes.Count > 0))
            {
                nodesWithChildren.Add(node);
                GetParentNodes(nodesWithChildren, node.Nodes);
            }
        }

        private void GetFirstSelectableTreeNode(TreeNodeCollection tnc, ref PaceTreeNode selNode)
        {
            if (selNode != null) return;
            foreach (PaceTreeNode tn in tnc.Cast<PaceTreeNode>().Where(tn => tn != null))
            {
                if (IsNodeSelectable(tn))
                {
                    selNode = tn;
                    break;
                }

                if (tn.Nodes.Count > 0)
                {
                    GetFirstSelectableTreeNode(tn.Nodes, ref selNode);
                }
            }
        }

        /// <summary>
        /// Returns a list of nodes between two specified nodes. First it determines the "first" node in the treeview
        /// collection via preorder traversal, then it returns all nodes encountered "between" the first and last nodes
        /// encountered in a preorder traversal
        /// </summary>
        /// <param name="firstNode">Pace tree node boundary.</param>
        /// <param name="secondNode">Pace tree node to check.</param>
        private void SelectNodeRange(TreeNode firstNode, TreeNode secondNode) {
            List<TreeNode> selectedNodes = new List<TreeNode>();
            bool isFinished = false, isInBounds = false;
            
            // Just let the regular click handling work, no range to select
            if (firstNode == secondNode) return;

            // begin walking the tree nodes. At the end the collection should
            // contain all nodes for selecting
           // of nodes to return. Stop when the other boundary node is found.
            foreach (TreeNode tn in Nodes) {
                if (!isFinished) {
                    AddToNodeRange(tn, firstNode, secondNode, ref selectedNodes, ref isInBounds, ref isFinished);
                }
                else {
                    break;
                }
            }

            // if !isFinished and inBounds than something went wrong
            if (!isFinished && isInBounds ) {throw new Exception("Invalid state in shift control, [" 
                + firstNode.ToString() + "], [" + secondNode.ToString() + "]");}

            // now just select the nodes in the collection

            // assume secondNode is currect selected and remote it from the collection to avoid check/uncheck double hit
            selectedNodes.Remove(secondNode);
            SelectNodeCollection(selectedNodes);

        }

        private void SelectNodeCollection(List<TreeNode> selectedNodes) {
            foreach (TreeNode tn in selectedNodes) {
                SelectNode(tn, true);
            }
        }

       /// <summary>
        /// Recursive method for walking a tree adding members to a collection if "between" the boundary nodes.
       /// </summary>
       /// <param name="nodeToCheck"></param>
       /// <param name="firstNode"></param>
       /// <param name="secondNode"></param>
       /// <param name="inBoundsList"></param>
       /// <param name="isInBounds"></param>
       /// <param name="isFinished"></param>
        private void AddToNodeRange(TreeNode nodeToCheck, TreeNode firstNode, TreeNode secondNode, ref List<TreeNode> inBoundsList, ref bool isInBounds, ref bool isFinished) {

            // Are we currently within the boundary of the check ? If so in most cases just add this node and any children recursively
            if (isInBounds) {
                // even if this is the ending boundary node it would still get added
                inBoundsList.Add(nodeToCheck);
                if (nodeToCheck == firstNode || nodeToCheck == secondNode) {
                    // this must be the end node since we are "inbounds" and got another match
                    isFinished = true;
                    return; // all done here
                }
                if (nodeToCheck.Nodes.Count > 0) {
                    // has children so recurse into them
                    foreach (TreeNode tn in nodeToCheck.Nodes) {
                        AddToNodeRange(tn, firstNode, secondNode, ref inBoundsList, ref isInBounds, ref isFinished);
                        if (isFinished) return; //the end was found inside a child, return
                    }
                }
                else {
                    // we have no children and we're in bounds so just return have already added the node
                    return;
                }
            }
            // out of bounds case
            else {
                if (nodeToCheck == firstNode || nodeToCheck == secondNode) {
                    // this must be the first node since we are ! "inbounds" and got a match
                    inBoundsList.Add(nodeToCheck);
                    isInBounds = true;
                }
                // regardless of weather we got a match, we need to recurse into the children

                if (nodeToCheck.Nodes.Count > 0) {
                    // has children so recurse into them
                    foreach (TreeNode tn in nodeToCheck.Nodes) {
                        AddToNodeRange(tn, firstNode, secondNode, ref inBoundsList, ref isInBounds, ref isFinished);
                        if (isFinished) return; //the end was found inside a child, return
                    }
                }
            }
        }

        /// <summary>
        /// Remove a checked node.
        /// </summary>
        /// <param name="node"></param>
        private void RemoveCheckedNode(PaceTreeNode node)
        {
            CheckedNodes.Remove(node);
            if (CheckedNodes.Count == 0)
            {
                SelectableNodeLevel = -1;
            }
        }

        #endregion private methods





        #region overriden methods

        [DebuggerHidden]
        protected override sealed bool DoubleBuffered
        {
            get { return base.DoubleBuffered; }
            set { base.DoubleBuffered = value; }
        }

        [DebuggerHidden]
        protected override void WndProc(ref Message m)
        {
            switch (m.Msg)
            {
                case WM_LBUTTONDBLCLK:
                    break;
                default:
                    base.WndProc(ref m);
                    break;
            }
        }

        protected override void OnBeforeCheck(TreeViewCancelEventArgs e)
        {
            PaceTreeNode node = (PaceTreeNode)e.Node;

            if (node == null) return;

            // Check for shift modifier, and attempt to select everything in between last click.
            // ShiftMode prevents this from recursively calling as all the embeded nodes are selected.
            if (ModifierKeys == Keys.Shift && MultiSelectTree && !IsShiftSelMode) {
                // MessageBox.Show("Shift is pressed");

                // Set shift mode on to stop recursing into this section
                IsShiftSelMode = true;             

                // Select all the nodes in the range between the last selected node and the current node
                SelectNodeRange(LastSelectedNode, node);

                // flip off flag for shift mode
                IsShiftSelMode = false;

            }

            if (node.Checked)
            {
                e.Cancel = false;
                _logger.Debug("PaceTreeView.OnBeforeCheck: node.Checked=true, e.Cancel=false");
            }
            else if (!IsNodeSelectable(node))
            {
                e.Cancel = true;
                _logger.Debug("PaceTreeView.OnBeforeCheck: !NodeSelectable, e.Cancel=true");
            }
            else if (CheckedNodes.Count > 0 && !this.MultiSelectTree)
            {
                SwapCheckedNode(CheckedNodes, node);
                e.Cancel = true;
                _logger.Debug("PaceTreeView.OnBeforeCheck: CheckedNodes.Count>0 && !MultiSelectTree, e.Cancel=true");
            }
            

            //KRM 4/8/2011 Replaced with unselectable code above.
            //else
            //{
            //    if (node.AliasTableMap.Count > 0)
            //    {
            //        string[] values = new string[node.AliasTableMap.Count];
            //        node.AliasTableMap.Values.CopyTo(values, 0);
            //        foreach (string v in values)
            //        {
            //            if (v.Equals(node.Text) && UnselectableNodeLevels.Contains(node.Level))
            //            {
            //                e.Cancel = true;
            //            }
            //        }
            //    }
            //}

            //don't allow disable nodes to be checked.
            if (node.DrawCheckBox == PaceTreeNode.DrawState.Disabled)
            {
                e.Cancel = true;
                return;
            }

            base.OnBeforeCheck(e);
        }

        protected override void OnAfterCheck(TreeViewEventArgs e)
        {
            PaceTreeNode node = (PaceTreeNode)e.Node;

            if (node == null) return;

            if (node.DrawCheckBox == PaceTreeNode.DrawState.Disabled)
            {
                node.Checked = false;
            }

            if (node.Checked)
            {
                CheckedNodes.Add(node);
                LastSelectedNode = node;
            }
            else
            {
                RemoveCheckedNode(node);
            }

            base.OnAfterCheck(e);

        }


        //[DebuggerHiddenAttribute]
        //protected override void OnDrawNode1(DrawTreeNodeEventArgs e)
        //{
        //    e.DrawDefault = true;
        //    base.OnDrawNode(e);

        //    PaceTreeNode node = (PaceTreeNode)e.Node;

        //    try
        //    {
        //        if (node.DrawCheckBox != PaceTreeNode.DrawState.Disabled)
        //        {
        //            if (e.Node.ForeColor == _disabledColor)
        //            {
        //                e.Node.ForeColor = _enabledColor;
        //            }
        //        }
        //        else
        //        {
        //            e.Node.ForeColor = _disabledColor;
        //            if (_paintWithImage)
        //            {
        //                e.Graphics.DrawImage(_redX, e.Bounds.Left - 34, e.Bounds.Top - 2, 18, e.Bounds.Height + 2);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        PafApp.GetLogger().Error(ex);
        //    }
        //}

        [DebuggerHidden]
        protected override void OnDrawNode(DrawTreeNodeEventArgs e)
        {
            PaceTreeNode node = (PaceTreeNode)e.Node;
            try
            {
                //Get the specific node font, if it dosen't exist get the tree font.
                Font treeFont = e.Node.NodeFont ?? e.Node.TreeView.Font;

                //The node is enabled, so just draw the text
                if (node.DrawCheckBox == PaceTreeNode.DrawState.Enabled)
                {
                    TextRenderer.DrawText(e.Graphics, e.Node.Text, treeFont, e.Bounds, _enabledColor, TextFormatFlags.GlyphOverhangPadding);
                }
                else //The node is disabled
                {
                    //Should an 'X' be drawn over the checkbox.
                    if (_paintWithImage)
                    {
                        //Draw the 'X'
                        e.Graphics.DrawImage(_redX, e.Bounds.Left - 16, e.Bounds.Top - 4, 21, e.Bounds.Height + 7);
                    }
                    //Set the color as disabled.
                    Color color = _disabledColor;
                    //If the node is selected / focues, then paint with a dark backcolor.
                    if ((e.State & TreeNodeStates.Focused) != 0) color = _enabledColor;
                    //Draw the node text
                    TextRenderer.DrawText(e.Graphics, e.Node.Text, treeFont, e.Bounds, color, TextFormatFlags.GlyphOverhangPadding);
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        [DebuggerHidden]
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            try
            {
                if (GetHashCode().Equals(obj.GetHashCode()))
                {
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion overriden methods
    }
}
