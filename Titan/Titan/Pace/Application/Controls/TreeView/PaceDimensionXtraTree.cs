﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Utils.Drawing;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Titan.Pace.Application.Extensions;

namespace Titan.Pace.Application.Controls.TreeView
{
    internal partial class PaceDimensionXtraTree : TreeList
    {
        /// <summary>
        /// Does the tree allow multiple checked nodes.
        /// </summary>
        public bool MultiCheck { get; set; }

        /// <summary>
        /// List of levels where nodes can be selected.  
        /// If this collection is blank then all levels are selectable.
        /// </summary>
        public List<int> SelectableLevels { get; set; }

        /// <summary>
        /// List of levels where nodes cannot be selected.
        /// </summary>
        public List<int> UnselectableLevels { get; set; }

        /// <summary>
        /// List of generations where nodes can be selected.
        /// If this collection is blank then all generations are selectable.
        /// </summary>
        public List<int> SelectableGenerations { get; set; }

        /// <summary>
        /// Name of the column that contains the nodes level
        /// </summary>
        public string LevelColumnName { get; set; }

        /// <summary>
        /// Name of the column that contains the node member name.
        /// </summary>
        public string MemberColumnName { get; set; }

        /// <summary>
        /// Name of the column that's displayed in the tree.
        /// </summary>
        public string DisplayColumnName { get; set; }

        /// <summary>
        /// Name of th ecolumn that contains the nodes generation.
        /// </summary>
        public string GenerationColumnName { get; set; }

        /// <summary>
        /// Name of the column that has the nodes visible status.
        /// </summary>
        public string VisibleColumnName { get; set; }

        /// <summary>
        /// List of nodes in selection order.
        /// </summary>
        public List<TreeListNode> CheckedNodesSelectionOrder { get; private set; }

        public PaceDimensionXtraTree()
        {
            MultiCheck = true;
            SelectableLevels = new List<int>(5);
            SelectableGenerations = new List<int>(5);
            UnselectableLevels = new List<int>(5);
            CheckedNodesSelectionOrder = new List<TreeListNode>(100);
            BeforeCheckNode += PaceDimensionXtraTree_BeforeCheckNode;
            CustomDrawNodeCheckBox += PaceDimensionXtraTree_CustomDrawNodeCheckBox;
            NodeChanged += PaceDimensionXtraTree_NodeChanged;    
            InitializeComponent();
        }


        /// <summary>
        /// Returns the first node on the tree that can be checked by the user.
        /// </summary>
        public TreeListNode FirstSelectableNode
        {
            get
            {
                TreeListNode root = this.RootNode();
                TreeListNode outNode = null;
                if (this.IsNodeCheckable(root))
                {
                    outNode =  root;
                }
                else
                {
                    GetFirstSelectableTreeNode(root.Nodes, ref outNode);
                }
                return outNode;
            }
        }

        /// <summary>
        /// Gets a string list of the checked nodes.
        /// </summary>
        /// <param name="inCheckedOrder">If true the order is in the order the user checked them.</param>
        /// <returns></returns>
        public List<string> GetCheckedNodes(bool inCheckedOrder = false)
        {
            if (!inCheckedOrder)
            {
                return this.GetCheckedNodes(MemberColumnName);
            }
            if (CheckedNodesSelectionOrder == null || CheckedNodesSelectionOrder.Count == 0)
            {
                return null;
            }

            return CheckedNodesSelectionOrder.GetNodesText(MemberColumnName);
        }


        /// <summary>
        /// Returns true if the generation is valid for selection.
        /// </summary>
        /// <param name="generation"></param>
        /// <returns></returns>
        public bool IsGenerationValid(int generation)
        {
            return SelectableGenerations == null || SelectableGenerations.Count <= 0 || SelectableGenerations.Contains(generation);
        }

        /// <summary>
        /// Returns true if the level is valid for selection.
        /// </summary>
        /// <param name="level"></param>
        /// <returns></returns>
        public bool IsLevelValid(int level)
        {
            return SelectableLevels == null || SelectableLevels.Count <= 0 || SelectableLevels.Contains(level);
        }

        #region Private Methods

        void PaceDimensionXtraTree_NodeChanged(object sender, NodeChangedEventArgs e)
        {
            if (e.ChangeType != NodeChangeTypeEnum.CheckedState) return;
            if (e.Node.Checked)
            {
                CheckedNodesSelectionOrder.Add(e.Node);
            }
            else
            {
                CheckedNodesSelectionOrder.Remove(e.Node);
            }
        }

        private void PaceDimensionXtraTree_CustomDrawNodeCheckBox(object sender, CustomDrawNodeCheckBoxEventArgs e)
        {
            if (UnselectableLevels.Count == 0 && SelectableLevels.Count == 0 && SelectableGenerations.Count == 0)
            {
                e.Handled = false;
                return;
            }
            if (!this.IsNodeCheckable(e.Node))
            {
                e.ObjectArgs.State = ObjectState.Disabled;
            }


        }

        private void PaceDimensionXtraTree_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            if (e.PrevState == CheckState.Checked) return;

            bool canCheck = this.IsNodeCheckable(e.Node);
            if (MultiCheck)
            {
                e.CanCheck = canCheck;
            }
            else
            {
                //TTN-2424 - If the node can be checked, then we don't need to uncheck all the nodes (since this is a single select)
                if(canCheck) UncheckAll();
                e.CanCheck = canCheck;
            }
        }

       

        private void GetFirstSelectableTreeNode(TreeListNodes tnc, ref TreeListNode selNode)
        {
            if (selNode != null) return;
            foreach (TreeListNode tn in tnc.Cast<TreeListNode>().Where(tn => tn != null))
            {
                if (this.IsNodeCheckable(tn))
                {
                    selNode = tn;
                    break;
                }

                if (tn.Nodes.Count > 0)
                {
                    GetFirstSelectableTreeNode(tn.Nodes, ref selNode);
                }
            }
        }

        #endregion Private Methods
    }
}
