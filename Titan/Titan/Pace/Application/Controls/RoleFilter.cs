#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;
using DevExpress.XtraBars;
using Titan.Pace.Application.Controls.Display;
using Titan.Pace.Application.Controls.TreeView;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Forms;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView;
using Titan.PafService;
using Titan.Properties;

namespace Titan.Pace.Application.Controls
{
    /// <summary>
    /// 
    /// </summary>
    internal partial class RoleFilter : UserControl
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);


        #region Variables

        private int _pad;
        private const int Pad1 = 3;
        //private const int Pad2 = -150;
        //private const int Pad3 = -435;
        private const string NoAliasTable = "None";
        private const int WM_SETREDRAW = 11;
        private const string Required = "*";

        private float _dpiX, _dpiY;
        private int suspendCounter = 0;
        public event RoleFilter_RoleFilter FilterChanged;
        public delegate void RoleFilter_RoleFilter(object sender, LogonEventArgs e, ref bool Cancel);

        //*******************************
        private string _user;
        private string _role;
        private string _seasonProc;

        private pafDimSpec[] _pafDimSpec;
        private pafDimSpec[] _previousPafDimSpec;
        //*******************************

        private readonly List<PaceTreeView> _dims;
        private readonly List<string> _base;
        private readonly List<string> _attr;
        private readonly SimpleTrees _roleFiltersSimpleTrees;

        /// <summary>
        /// signals that we are updating the GUI.
        /// </summary>
        private bool _updating;
        private bool _updating2;

        /// <summary>
        /// attribute, Base dictionary lookup.
        /// </summary>
        private readonly Dictionary<string, string> _attrBaseLookup;

        /// <summary>
        /// Structure to cache the user selections in.
        /// </summary>
        //private CachedTreeNodes _CachedTreeNodes;
        private readonly CacheSelectionsList<string, string, PaceTreeNode> _cachedTreeNodes;

        /// <summary>
        /// Structure to cache the user selections in.
        /// </summary>
        //private CachedTreeNodes _AttrTreeNodes;
        private readonly CacheSelectionsList<string, string, PaceTreeNode> _attrTreeNodes;

        /// <summary>
        /// Structure to cache the user selections in.
        /// </summary>
        private readonly Dictionary<string, List<PaceTreeNode>> _selectedAtrTreeNodes;

        /// <summary>
        /// Structure to chache the PafUserSelections in.
        /// </summary>
        private readonly Dictionary<string, pafDimSpec> _cachedPafDimSpec;

        /// <summary>
        /// Structure to hold the valid attributes that can be selected.
        /// </summary>
        private readonly Dictionary<string, string[]> _validAttributes;

        /// <summary>
        /// 
        /// </summary>
        private SavedFilterSelections _savedSelections;

        /// <summary>
        /// Holds the attribute dim info, needed for attribute tab disabling.
        /// </summary>
        private attributeDimInfo[] _attributeDimInfo;

        private readonly ImageList _imageList;

        private bool _suppressInvalidInxVis;

        private bool _discontigSecurity;

        private bool _filteredSubtotals;

        private bool _extraControlsVisible;

        private readonly List<AliasTableDisplay> _aliasTableNames = new List<AliasTableDisplay>();

        private string _clear;

        private string _selections;

        private string _remember;

        #endregion Variables

        #region Properties

        public bool UserFilteredMultiSelect { get; set; }

        #endregion Properties

        #region Constructor & Control Load

        /// <summary>
        /// Constructor.
        /// </summary>
        public RoleFilter()
        {
            try
            {
                _base = new List<string>();
                _dims = new List<PaceTreeView>();
                _attr = new List<string>();
                _attrBaseLookup = new Dictionary<string, string>();
                _cachedTreeNodes = new CacheSelectionsList<string, string, PaceTreeNode>();
                _attrTreeNodes = new CacheSelectionsList<string, string, PaceTreeNode>();
                _cachedPafDimSpec = new Dictionary<string, pafDimSpec>();
                _validAttributes = new Dictionary<string, string[]>();
                _selectedAtrTreeNodes = new Dictionary<string, List<PaceTreeNode>>(10);
                _roleFiltersSimpleTrees = new SimpleTrees();
                _updating = false;
                _updating2 = false;
                _imageList = new ImageList();
                InitializeComponent();
                searchControl.SearchInvoked += searchControl_SearchInvoked;
                searchControl.SearchTextModified += searchControl_SearchTextModified;
                loadUi();
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                Graphics graphics = CreateGraphics();
                _dpiX = graphics.DpiX;
                _dpiY = graphics.DpiY;
                _pad = Pad1;
                if (_dpiX >= 144 && _dpiY >= 144)
                {
                    _pad = Pad1;
                }
                else if (_dpiX >= 120 && _dpiY >= 120)
                {
                    _pad = Pad1;
                }

                //Get the labels from the localized .resx file.
                Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                cmdResetSel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.ClearSelections");
                chkSupInvIx.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.InvalidInx");
                barButtonItemFind.Caption =PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.ToolStripMenuItem.Find.Text");
                toolStripDropDownButton.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.ToolStripMenuItem.AliasTable.Text");
                chkFilteredTotals.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.FilteredSubtotals");
                _clear = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.Clear");
                _selections = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.Selections");
                _remember = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.Remember");
                barButtonItemClearAll.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.ClearAll");
                barCheckItemRememberAll.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.RememberAll");
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        public void RefrehAttributeTab(int tabIndex)
        {
            tabRoleFilter_SelectedIndexChanged(tabIndex);
        }

        public void RefrehAttributeTabs()
        {
            tabRoleFilter_SelectedIndexChanged(null, null);
        }

        /// <summary>
        /// 
        /// </summary>
        public void RebuildTreeTabs()
        {
            try
            {
                _updating = true;

                _savedSelections = PafApp.GetPafAppSettingsHelper().RoleFilterSavedSelections();

                //check to see if user has changed username, role/process, if not don't rebuild anything.  
                if(PafApp.GetGridApp().LogonInformation.UserName != _user ||
                    PafApp.GetGridApp().RoleSelector.UserRole != _role || 
                    PafApp.GetGridApp().RoleSelector.UserSelectedPlanTypeSpecString != _seasonProc)
                {
                    List<string> temp = new List<string>();
                    _user = PafApp.GetGridApp().LogonInformation.UserName;
                    //get the role
                    _role = PafApp.GetGridApp().RoleSelector.UserRole;
                    //get the season process
                    _seasonProc = PafApp.GetGridApp().RoleSelector.UserSelectedPlanTypeSpecString;
                    //get the data filter flag visible property.
                    _suppressInvalidInxVis = PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig.isDataFilteredUow;
                    //Is the current security discontigous
                    _discontigSecurity = PafApp.GetGridApp().RoleSelector.IsRoleSecurityDiscontigous;
                    //Filtered subtotals from config.
                    _filteredSubtotals = PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig.isFilteredSubtotals;
                    //reset the lists.
                    _attributeDimInfo = null;
                    _pafDimSpec = null;
                    _previousPafDimSpec = null;
                    _base.Clear();
                    _dims.Clear();
                    _attr.Clear();
                    _attrBaseLookup.Clear();
                    _aliasTableNames.Clear();
                    toolStripDropDownButton.DropDownItems.Clear();
                    //cboDisplay.Items.Clear();
                    _roleFiltersSimpleTrees.Clear();
                    tabRoleFilter.TabPages.Clear();
                    //clear the checked selecs lists
                    ClearCachedSelectionObjects();
                    cmdOk.Enabled = false;

                    //get the pafMdbProps
                    pafMdbProps pafMdbProps = PafApp.GetServiceMngr().GetPafMdbProps();

                    //do a null check, then create the dictionary.
                    if (pafMdbProps != null && pafMdbProps.cachedAttributeDims != null) 
                    {
                        for (int i = 0; i < pafMdbProps.baseDimLookupKeys.Length; i++)
                        {
                            _attrBaseLookup.Add(
                                pafMdbProps.baseDimLookupKeys[i],
                                pafMdbProps.baseDimLookupValues[i]);
                        }
                    }

                    //get the attribute dim info object from the client auth object...
                    if(PafApp.GetPafAuthResponse()!= null && PafApp.GetPafAuthResponse().attrDimInfo != null)
                    {
                        _attributeDimInfo = PafApp.GetPafAuthResponse().attrDimInfo;
                    }


                    //if the Role Filter Response if is != null and the dim tree != null then
                    //start to build the tabs and controls.
                    if (PafApp.GetPafPopulateRoleFilterResponse() != null &&
                        PafApp.GetPafPopulateRoleFilterResponse().dimTrees != null)
                    {
                        //add the base dims to the list.
                        if (PafApp.GetPafPopulateRoleFilterResponse().baseTreeNames != null)
                        {
                            _base.AddRange(PafApp.GetPafPopulateRoleFilterResponse().baseTreeNames);
                        }


                        //add the attribute dims to the list.
                        if (PafApp.GetPafPopulateRoleFilterResponse().attributeTreeNames != null)
                        {
                            temp.AddRange(PafApp.GetPafPopulateRoleFilterResponse().attributeTreeNames);
                        }

                        if (PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig.userFilterSpec != null)
                        {
                            foreach (string str in PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig.userFilterSpec)
                            {
                                if (temp.Contains(str))
                                {
                                    _attr.Add(str);
                                }
                            }
                        }

                        //build the simple tree lists.
                        foreach (pafSimpleDimTree psdt in PafApp.GetPafPopulateRoleFilterResponse().dimTrees)
                        {
                            _roleFiltersSimpleTrees.Add(psdt, psdt.id, true);
                            if (_aliasTableNames.Count <= 0)
                            {
                                _aliasTableNames.Add(new AliasTableDisplay(String.Empty, NoAliasTable));
                                foreach (string s in psdt.aliasTableNames)
                                {
                                    _aliasTableNames.Add(new AliasTableDisplay(s, s));
                                }
                            }
                        }
                        string lastAliasTable = Properties.Settings.Default.RoleFilterLastAliasTable;
                        AliasTableDisplay aliasTableDisplay = new AliasTableDisplay(lastAliasTable, lastAliasTable);
                        int i = 0, idx = 0;
                        foreach(AliasTableDisplay alias in _aliasTableNames)
                        {
                            ToolStripMenuItem b = new ToolStripMenuItem
                            {
                                Text = alias.AliasTableDisplayValue,
                                Tag = alias
                            };
                            b.Click += ToolStripButton_Click;

                            if (aliasTableDisplay.Equals(alias))
                            {
                                idx = i;
                            }

                            toolStripDropDownButton.DropDownItems.Add(b);
                            i++;
                        }

                        if(idx > 0)
                        {
                            ToolStripMenuItem senderButton =
                                (ToolStripMenuItem)toolStripDropDownButton.DropDownItems[idx];
                            senderButton.Checked = true;
                        }

                        //add the base dim tabs.
                        if (_base != null && _base.Count > 0)
                        {
                            foreach (string str in _base)
                            {
                                pafSimpleDimTree tree = _roleFiltersSimpleTrees.GetTree(str);
                                if (tree != null)
                                {
                                    CreateTab(tree);
                                }
                            }
                        }
                        //add the attributes dim tabs.
                        if (_attr != null && _attr.Count > 0)
                        {
                            foreach (string str in _attr)
                            {
                                pafSimpleDimTree tree = _roleFiltersSimpleTrees.GetTree(str);
                                if (tree != null)
                                {
                                    CreateTab(tree);
                                }
                            }
                        }
                        //resize the tab control box the hide the suppress invalid intersexion checkbox
                        ResizeTabControlBox(_suppressInvalidInxVis, _filteredSubtotals, _discontigSecurity);
                        //TTN-2534 - Put back last selectons
                        if (_extraControlsVisible)
                        {
                            if (chkSupInvIx.Enabled)
                            {
                                chkSupInvIx.Checked = Settings.Default.SuppressInvIx;
                            }
                            if (chkFilteredTotals.Enabled)
                            {
                                chkFilteredTotals.Checked = Settings.Default.FilteredTotals;
                            }
                        }
                        RoleFilter_Resize(null, null);
                    }
                }
                toolStripStatusLabel.Text = String.Empty;
                searchControl.ButtonText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Find");
                //reslect any cached nodes.
                SelectOrReselectNodes();
                cmdOk.Select();
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                _updating = false;
            }
        }

        /// <summary>
        /// Reselect nodes on the attribute tree
        /// </summary>
        /// <remarks>TTN-2145</remarks>
        public void ReselctAttributeNodes()
        {
            foreach (PaceTreeView tree in _dims)
            {
                string dimension = tree.Name;
                if (tree.DimensionType == PaceTreeNode.PaceTreeDimensionType.Base) continue;
                //Get the cached user selection.
                List<PaceTreeNode> nodes;
                _selectedAtrTreeNodes.TryGetValue(dimension, out nodes);
                //if (nodes == null || nodes.Count == 0) continue;
                if (nodes == null || nodes.Count == 0)
                {
                    List<string> roleNodes = _savedSelections.Values(tree.Name);
                    if (roleNodes != null && roleNodes.Count > 0)
                    {
                        nodes = new List<PaceTreeNode>();
                        foreach (var x in roleNodes)
                        {
                            var n = tree.Find(x, false, false, false);
                            if (n != null)
                            {
                                nodes.AddRange(n);
                            }
                            else
                            {
                                PafApp.GetLogger().WarnFormat("Cached tree selection '{0}' cannot be found in tree.", x);
                            }
                        }
                        if (nodes.Count == 0)
                        {
                            nodes = null;
                            PafApp.GetLogger().WarnFormat("Dimension '{0}' no cached tree selections can be found.  No selections will be set.", tree.Name);
                        }
                    }
                }
                if (nodes != null && nodes.Count > 0)
                {
                    int index = tabRoleFilter.SelectedIndex;
                    foreach (TabPage v in tabRoleFilter.TabPages)
                    {
                        if (v.Text.Equals(dimension))
                        {
                            index = v.TabIndex;
                            break;
                        }

                    }
                    RefrehAttributeTab(tabRoleFilter.TabPages[index].TabIndex);
                    List<PaceTreeNode> enabledNodes = new List<PaceTreeNode>();
                    foreach (var node in nodes) 
                    {
                        if (node.DrawCheckBox == PaceTreeNode.DrawState.Enabled)
                        {
                            enabledNodes.Add(node);
                        }
                        else
                        {
                            PafApp.GetLogger().WarnFormat("Saved node '{0}' cannot be checked because it's not a valid attribute at this time.", node.Text);
                        }
                    }
                    //tree.EnableTreeNode(nodes);
                    SelectNodes(tree, enabledNodes);
                    foreach (var n in enabledNodes)
                    {
                        _attrTreeNodes.CacheSelection(dimension, dimension, n);
                    }
                    CacheUserSelections(tree, false);
                    DisableAttributeTabs(tree.Name);
                }
            }
            PaceTreeView currentTree = (PaceTreeView)tabRoleFilter.TabPages[tabRoleFilter.SelectedIndex].Controls[0];
            if (currentTree.DimensionType == PaceTreeNode.PaceTreeDimensionType.Base) return;
            int currentidx = tabRoleFilter.SelectedIndex;
            tabRoleFilter.SelectedIndex = 0;
            tabRoleFilter.SelectedIndex = currentidx;
        }

        private string GetStringDisplayFormat()
        {
            ToolStripMenuItem senderButton = null;
            foreach (ToolStripMenuItem mi in toolStripDropDownButton.DropDownItems)
            {
                if (!mi.Checked) continue;
                senderButton = mi;
                break;
            }
            if (senderButton != null)
            {
                AliasTableDisplay atd = (AliasTableDisplay) senderButton.Tag;
                return atd.AliasTable;
            }
            return String.Empty;
        }

        /// <summary>
        /// Set the valid attributes in the cache after getting the information from the server.
        /// </summary>
        private void SetAttributes(PaceTreeView tree)
        {
            try
            {
                //get the base dimensions
                string baseDim = _attrBaseLookup[tree.Name];
                //create an empty pafDimSpec.
                pafDimSpec[] pafDimSpec = GetAttrPafDimSpec(tree.Name, baseDim);
                //get the base tree.
                PaceTreeView baseTree = GetTreeView(baseDim);

                //TTN-1645
                string[] checkNodeNames = baseTree.CheckedNodes.Select(treeNode => treeNode.MemberName).ToArray();


                pafValidAttrResponse resp = null;
                if (checkNodeNames.Length > 0)
                {
                    //prepare the service call.
                    resp = PafServiceMngr.GetValidAttributeMembers(
                        PafApp.ClientId,
                        PafApp.GetPafAuthResponse().securityToken.sessionToken,
                        baseDim,
                        checkNodeNames,
                        tree.Name,
                        pafDimSpec);
                }

                if (resp != null && resp.members != null)
                {
                    //create a blank list.
                    List<string> lst = new List<string>();
                    //add all the attributes to the list.
                    lst.AddRange(resp.members);
                    //remove any existing selections.
                    _validAttributes.Remove(tree.Name);
                    //add the item to the cache
                    _validAttributes.AddOrUpdate(tree.Name, lst.ToArray());
                }
                else
                {
                    _validAttributes.Remove(tree.Name);
                }
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Clears all the cached selection objects
        /// </summary>
        private void ClearCachedSelectionObject(string dimension)
        {
            //clear the checked selecs lists
            _cachedTreeNodes.RemoveSelection(dimension);
            _cachedPafDimSpec.Remove(dimension);
            _attrTreeNodes.RemoveSelection(dimension);
            _validAttributes.Remove(dimension);
            _selectedAtrTreeNodes.Remove(dimension);
        }

        /// <summary>
        /// Clears all the cached selection objects
        /// </summary>
        private void ClearCachedSelectionObjects()
        {
            //clear the checked selecs lists
            _cachedTreeNodes.Clear();
            _cachedPafDimSpec.Clear();
            _attrTreeNodes.Clear();
            _validAttributes.Clear();
            _selectedAtrTreeNodes.Clear();
        }

        /// <summary>
        /// Get an pafDimSpec array of attribute selections.
        /// </summary>
        /// <param name="attrDim">The currently selected attribute tab.</param>
        /// <param name="attrBaseDim">Name of the associated base dimension.</param>
        /// <returns>an array of pafDimSpec attribute selections</returns>
        private pafDimSpec[] GetAttrPafDimSpec(string attrDim, string attrBaseDim)
        {
            List<pafDimSpec> pafDimSpec = new List<pafDimSpec>();
            foreach(string attr in _attr)
            {
                if(attr != attrDim)
                {
                    //get the cached selections from the list.
                    pafDimSpec pdc = _cachedPafDimSpec.ValueOrDefault(attr);
                    if(pdc.expressionList.Length > 0)
                    {
                        string str = _attrBaseLookup[pdc.dimension];
                        //if the base dims don't match then don't add it to the list.
                        if (str == attrBaseDim)
                        {
                            pafDimSpec.Add(pdc);
                        }
                    }
                }
            }
            if(pafDimSpec.Count > 0)
            {
                return pafDimSpec.ToArray();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Creates a tab control tab.
        /// </summary>
        /// <param name="tree"></param>
        private void CreateTab(pafSimpleDimTree tree)
        {
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;
            //create a tab page.
            TabPage page = new TabPage(tree.id);
            //add an '*' to the base dims.
            if (_base.Contains(tree.id))
            {
                page.Text = tree.id + Required;
            }
            //create a tree view.
            PaceTreeView mwTree = new PaceTreeView();

            //set the name of the control == to the tree id.
            mwTree.Name = tree.id;
            //set the dimension name.
            mwTree.DimensionName = tree.id;
            //when the tree loses focus don't let the sel nodes loose color.
            mwTree.HideSelection = false;
            //set the tag == to the dim type (base or attribute)
            if (_base.Contains(tree.id))
            {
                //set is as an base dim type
                mwTree.DimensionType = PaceTreeNode.PaceTreeDimensionType.Base;
                mwTree.CheckBoxes = true;
                mwTree.DrawMode = TreeViewDrawMode.Normal;
                mwTree.UnselectableMbrName.Add(tree.id);
                mwTree.DisallowAncestorSelections = UserFilteredMultiSelect;
                mwTree.MultiSelectTree = UserFilteredMultiSelect;
                mwTree.AllowRootNodeSelection = true;
                if (debugEnabled) PafApp.GetLogger().DebugFormat("CreateTab UOW, Dimension (base): {0}", new object[] { tree.id });
                if (debugEnabled) PafApp.GetLogger().DebugFormat("CreateTab UOW, DisallowAncestorSelections: {0}", new[] { UserFilteredMultiSelect });
                if (debugEnabled) PafApp.GetLogger().DebugFormat("CreateTab UOW, MultiSelectTree: {0}", new[] { UserFilteredMultiSelect });
            }
            else
            {
                //set is as an attribute dim type
                mwTree.DimensionType = PaceTreeNode.PaceTreeDimensionType.Attr; 
                //multi select.
                mwTree.CheckBoxes = true;
                mwTree.MultiSelectTree = true;
                mwTree.AllowRootNodeSelection = true;
                //mwTree.ImageList = _imageList;
                if (debugEnabled) PafApp.GetLogger().DebugFormat("CreateTab UOW, Dimension (attbr): {0}", new object[] { tree.id });
                if (debugEnabled) PafApp.GetLogger().DebugFormat("CreateTab UOW, DisallowAncestorSelections: {0}", new[] { false });
                if (debugEnabled) PafApp.GetLogger().DebugFormat("CreateTab UOW, MultiSelectTree: {0}", new[] { true });
            }
            //add the before expand event hander, for adding the nodes on a dynamic basis.
            mwTree.BeforeExpand += mwTree_BeforeExpand;
            //add the before select event handler
            //mwTree.BeforeSelect += new TreeViewCancelEventHandler(mwTree_BeforeSelect);
            //add the after tree select event hander.  checks for valid selections.
            //mwTree.AfterSelect += new TreeViewEventHandler(mwTree_AfterSelect);
            //event handler, handle before a node is checked.
            mwTree.BeforeCheck += mwTree_BeforeCheck;
            mwTree.AfterCheck += mwTree_AfterCheck;
            mwTree.MouseUp += mwTree_MouseUp;
            //set it to fill.
            mwTree.Dock = DockStyle.Fill;
            //add the tree view to the tab page.
            page.Controls.Add(mwTree);
            //add the tab page to the tab control
            tabRoleFilter.TabPages.Add(page);
            //set the alias display mode
            mwTree.AliasTableToDisplay = GetStringDisplayFormat();
            //set the default alias table name.
            //aliasTable = PafApp.GetPafAppSettingsHelper().GetGlobalAliasTableName(tree.id);
            //priRowColFormat = PafApp.GetPafAppSettingsHelper().GetPrimaryRowColumnFormat(tree.id);
           
            //build the trees.
            //TTN-1747
            mwTree.BuildTreeControl(_roleFiltersSimpleTrees, tree, mwTree.CheckBoxes, GetStringDisplayFormat());
        

            //add the tree control to the list.
            _dims.Add(mwTree);
        }




        #endregion Constructor & Control Load

        #region Properties



        /// <summary>
        /// Gets the role.
        /// </summary>
        ///         
        [
        Browsable(true),
        Category("Data"),
        Description("Gets the Role"),
        DefaultValue(false)
        ]
        public string Role
        {
            get { return _role; }
        }


        /// <summary>
        /// Gets the SeasonProcess.
        /// </summary>
        ///         
        [
        Browsable(true),
        Category("Data"),
        Description("Gets the SeasonProcess"),
        DefaultValue(false)
        ]
        public string SeasonProcess
        {
            get { return _seasonProc; }
        }


        /// <summary>
        /// Gets/sets the invalid intersections checkbox visibility.
        /// </summary>
        ///         
        [
        Browsable(true),
        Category("Data"),
        Description("Gets Filtered Subtotals checkbox checked status"),
        DefaultValue(false)
        ]
        public bool FilteredSubtotalsChk
        {
            get { return chkFilteredTotals.Enabled && chkFilteredTotals.Checked; }
        }

        /// <summary>
        /// Gets/sets the invalid intersections checkbox checked status..
        /// </summary>
        ///         
        [
        Browsable(true),
        Category("Data"),
        Description("Gets/sets the invalid intersections checkbox checked status."),
        DefaultValue(false)
        ]
        public bool SuppressInvalidInxChk
        {
            get { return chkSupInvIx.Enabled && chkSupInvIx.Checked; }
            set { chkSupInvIx.Checked = value; }
        }

        /// <summary>
        /// Gets/sets the paf dim specs
        /// </summary>
        ///         
        [
        Browsable(true),
        Category("Data"),
        Description("Gets/sets the paf dim specs"),
        DefaultValue(null)
        ]
        public pafDimSpec[] PafDimSpecs
        {
            get { return _pafDimSpec; }
            set { _pafDimSpec = value; }
        }

        public string User
        {
            get { return _user; }
            set { _user = value; }
        }
        #endregion Properties

        #region Methods

        /// <summary>
        /// Searches for the MWTreeView.
        /// </summary>
        /// <param name="dimensionName">Dimension of the tree control to search for.</param>
        /// <returns>the MWTreeView if found, null if not found.</returns>
        private PaceTreeView GetTreeView(string dimensionName)
        {
            foreach(PaceTreeView tree in _dims)
            {
                if(tree.Name == dimensionName)
                {
                    return tree;
                }
            }
            return null;
        }

        /// <summary>
        /// Reselect any cached nodes.
        /// </summary>
        /// <param name="dimension">Optional.  If specified only the specified dimension will be reset.</param>
        private void SelectOrReselectNodes(string dimension = null)
        {
            try
            {
                foreach (PaceTreeView tree in _dims)
                {
                    if (tree.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Attr)) continue;

                    if (!String.IsNullOrEmpty(dimension))
                    {
                        if (!tree.DimensionName.Equals(dimension)) continue;
                    }

                    List<PaceTreeNode> nodes = _cachedTreeNodes.GetCachedSelections(tree.Name, tree.Name);
                    if (nodes == null && PafApp.GetPafAppSettingsHelper().PersistRoleFilterSelectionsEnabled())
                    {
                        List<string> roleNodes = _savedSelections.Values(tree.Name);
                        if (roleNodes != null && roleNodes.Count > 0)
                        {
                            nodes = new List<PaceTreeNode>();
                            foreach (var x in roleNodes)
                            {
                                var n = tree.Find(x, false, false, true);
                                if (n != null)
                                {
                                    nodes.AddRange(n);
                                }
                                else 
                                {
                                    PafApp.GetLogger().WarnFormat("Saved tree selection '{0}' cannot be found in tree.", x);
                                }
                            }
                        }
                        if (nodes == null || nodes.Count == 0)
                        {
                            nodes = null;
                            tree.CollapseAll();
                            PafApp.GetLogger().WarnFormat("Dimension '{0}' no cached tree selections can be found.  Legacy default selections will be set.", tree.Name);
                        }
                    }

                    SelectNodes(tree, nodes);
                }
            }
            catch(Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Check nodes on the tree.
        /// </summary>
        /// <param name="tree"></param>
        /// <param name="nodes"></param>
        private void SelectNodes(PaceTreeView tree, List<PaceTreeNode> nodes)
        {
            //If the user has made so selections.
            if (nodes != null)
            {
                //Clear out the selections.
                tree.UncheckAllNodes();
                tree.ClearCheckedNodes();
                PaceTreeNode level = tree.LowestLevelNode(nodes);
                //for each of the nodes.
                foreach (PaceTreeNode node in nodes.Where(node => node != null))
                {
                    tree.SelectNode(node, tree.CheckBoxes);
                }
                tree.CollapseAll();
                if (level != null)
                {
                    level.EnsureVisible();
                    if (level.Level == 0)
                    {
                        level.Expand();
                    }
                }
            }
            else //the user has not made any selection.
            {
                //Just clear everything out.;
                tree.UncheckAllNodes();
                tree.ClearCheckedNodes();

                if (tree.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Base))
                {
                    //TTN-1747
                    PaceTreeNode firstNode = tree.FirstSelectableNode;
                    if (firstNode != null)
                    {
                        tree.SelectNode(firstNode, true);
                        firstNode.Expand();
                    }
                }
                else
                {
                    //TTN-1208
                    tree.Nodes[0].Expand();
                }
            }
        }


        /// <summary>
        /// Resizes the tab control panel to show or hide the ChkSupInvIx check box.
        /// </summary>
        /// <param name="showChkSupInvIx">Show the chkSupInvIx check box.</param>
        /// <param name="allowFilteredSubtotals">Status of the Filter Subtotals Flag.</param>
        /// <param name="discontigSecurity">Does the selected role contain discontigous security</param>
        private void ResizeTabControlBox(bool showChkSupInvIx, bool allowFilteredSubtotals, bool discontigSecurity)
        {
            //Show controls if
            //a) allowFilteredSubtotals is true
            //b) allowFilteredSubtotals is true and discontigSecurity is true
            //b) allowFilteredSubtotals is true and showChkSupInvIx is enabled & selected
            //c) Both of the above.

            PafApp.GetLogger().InfoFormat("Role Filter ||   Role: {0}, Season: {1}, Invalid Inter: {2}, Filtered Subtotals: {3}, Discontiguous Sec: {4}",
                new object[] { _role, _seasonProc, showChkSupInvIx, allowFilteredSubtotals, discontigSecurity });

            //Filtered Subtotals Trumps All, so Hide the Controls and Exit
            if (!allowFilteredSubtotals)
            {
                chkSupInvIx.Visible = showChkSupInvIx;
                chkSupInvIx.Enabled = showChkSupInvIx;
                chkSupInvIx.Checked = showChkSupInvIx;
                chkFilteredTotals.Visible = showChkSupInvIx;
                chkFilteredTotals.Enabled = false;
                chkFilteredTotals.Checked = false;
                _extraControlsVisible = showChkSupInvIx;
                RoleFilter_Resize(null, null);
                return;
            }

            //To Reach Here
            //allowFilteredSubtotals will always be true

            //On the Role Filter we don't care about the discontigSecurity flag.  If allowFilteredSubtotals then
            //show the controls, however set the status of Invalid Itx to the showChkSupInvIx flag.

            chkSupInvIx.Visible = true;
            chkSupInvIx.Enabled = showChkSupInvIx;
            chkSupInvIx.Checked = showChkSupInvIx;

            chkFilteredTotals.Visible = true;
            chkFilteredTotals.Enabled = true;
            chkFilteredTotals.Checked = true;

            _extraControlsVisible = true;
            RoleFilter_Resize(null, null);


            ////KRM - Old Code - This code follows the discontigSecurity flag, which we only want on the RoleSelector.
            ////Case "A" - discontigSecurity is true
            //if (discontigSecurity)
            //{
            //    chkSupInvIx.Visible = true;
            //    chkSupInvIx.Enabled = showChkSupInvIx;
            //    chkSupInvIx.Checked = showChkSupInvIx;

            //    chkFilteredTotals.Visible = true;
            //    chkFilteredTotals.Enabled = true;
            //    chkFilteredTotals.Checked = true;

            //    _extraControlsVisible = true;
            //    RoleFilter_Resize(null, null);
            //    return;
            //}

            ////Case "B" - showChkSupInvIx is true
            //if (showChkSupInvIx)
            //{
            //    chkSupInvIx.Visible = true;
            //    chkSupInvIx.Enabled = true;
            //    chkSupInvIx.Checked = true;

            //    chkFilteredTotals.Visible = true;
            //    chkFilteredTotals.Enabled = true;

            //    _extraControlsVisible = true;
            //    RoleFilter_Resize(null, null);
            //    return;
            //}


            ////Catch Case.  At this point showChkSupInvIx is false, and discontigSecurity is false, allowFilteredSubtotals is true
            //chkSupInvIx.Visible = false;
            //chkSupInvIx.Enabled = false;
            //chkSupInvIx.Checked = false;

            //chkFilteredTotals.Visible = false;
            //chkFilteredTotals.Enabled = false;
            //chkFilteredTotals.Checked = false;

            //_extraControlsVisible = false;
            //RoleFilter_Resize(null, null);
        }

        private pafDimSpec CacheUserSelections(PaceTreeView tree, bool cacheTreeNodes)
        {
            _cachedPafDimSpec.Remove(tree.Name);
            _cachedTreeNodes.RemoveSelection(tree.Name);
            //create a new paf dim spec.
            pafDimSpec pafDimSpec = new pafDimSpec();
            PaceTreeNode[] nodes;
            pafDimSpec.dimension = tree.Name;

            //User can only sel one item, so just get it and set it.
            if (!tree.CheckBoxes)
            {
                pafDimSpec.expressionList = new [] { tree.SelectedNode.Tag.ToString() };
                //Set the tree node array size
                nodes = new PaceTreeNode[tree.Nodes.Count];
                nodes[0] = (PaceTreeNode) tree.SelectedNode;
            }
            else //User can sel mul. items.
            {
                pafDimSpec.expressionList = new string[tree.CheckedNodes.Count];
                nodes = new PaceTreeNode[tree.CheckedNodes.Count];

                int i = 0;
                foreach(PaceTreeNode node in tree.CheckedNodes)
                {
                    pafDimSpec.expressionList[i] = node.Tag.ToString();
                    nodes[i] = node;
                    i++;
                }
            }
            //Cache the nodes.
            if (cacheTreeNodes)
            {
                _cachedTreeNodes.CacheSelection(pafDimSpec.dimension, pafDimSpec.dimension, nodes);
            }
            if (tree.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Attr))
            {
                _attrTreeNodes.CacheSelection(pafDimSpec.dimension, pafDimSpec.dimension, nodes);
            }
            //cache the user selections...
            _cachedPafDimSpec.AddOrUpdate(pafDimSpec.dimension, pafDimSpec);

            return pafDimSpec;
        }

        /// <summary>
        /// Checks all the tree views for a valid selection.  Takes care of enabling/disabling the
        /// ok button if the critera is not meet.
        /// </summary>
        private bool CheckOkButtonStatus()
        {
            int minSelection = 0;
            foreach (PaceTreeView tree in _dims)
            {
                if (tree.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Base))
                {
                    //User can only sel one item, so just get it and set it.
                    if (!tree.CheckBoxes)
                    {
                        //Set the tree node array size
                        TreeNode[] nodes = new TreeNode[] {tree.SelectedNode};
                        if (nodes.Length == 1)
                        {
                            if(nodes[0] != null)
                            {
                                minSelection++;
                            }
                        }
                    }
                    else //User can sel mul. items.
                    {
                        //Get the count of nodes into the hash.
                        int checkNodeCnt = tree.CheckedNodes.Count;
                        if (checkNodeCnt > 0)
                        {
                            minSelection++;
                        }
                    }
                }
            }
            if (minSelection != _base.Count)
            {
                return false;
            }

            return true;
        }

        private void ClearSearchFilters()
        {

            for (int i = 0; i < tabRoleFilter.TabPages.Count; i++)
            {
                PaceTreeView tree = (PaceTreeView)tabRoleFilter.TabPages[i].Controls[0];
                tree.SearchResults.Clear();
            }

        }

        private bool Find(string textToFind, bool searchAliasTables, bool matchCase,
            bool matchWholeWord, bool autoCheckMember)
        {
            //bool matchCase = find.MatchCase;
            bool ret = true;
            if (!String.IsNullOrEmpty(textToFind))
            {
                //Get the dimension tree
                PaceTreeView tree = (PaceTreeView)tabRoleFilter.TabPages[tabRoleFilter.SelectedIndex].Controls[0];
                TreeNode[] selectedNodes = new[] { tree.SelectedNode };
                List<PaceTreeNode> results = null;

                //look for a cached search.
                if (tree.SearchResults.TryGetValue(textToFind, out results))
                {
                    //make sure there is a selected node, this should never happen, but
                    //put a null check anyways.
                    if (selectedNodes != null && selectedNodes.Length > 0)
                    {
                        //get the current positon.
                        int i = results.IndexOf((PaceTreeNode)selectedNodes[0]);

                        //make sure we don't try to select beyond the index.
                        if (i >= 0 && i + 1 < results.Count)
                        {
                            //select the next index.
                            tree.SelectNode(results[i + 1], autoCheckMember);
                            tree.Select();
                            return true;
                        }
                        else
                        {
                            //if we are at the end restart at the begining.
                            tree.SelectNode(results[0], autoCheckMember);
                            tree.Select();
                            return true;
                        }

                    }

                }
                else if (selectedNodes != null && selectedNodes.Length > 0)
                {
                    //if you current selection is not at the end of the branch, then start the 
                    //search from the current position.
                    if (selectedNodes[0] != null && selectedNodes[0].Nodes != null && selectedNodes[0].Nodes.Count > 0)
                    {
                        results = tree.Find(textToFind, searchAliasTables, matchCase, matchWholeWord, (PaceTreeNode)selectedNodes[0]);
                    }
                    else //the position was at the end of a branch, so start the search at the root node.
                    {
                        results = tree.Find(textToFind, searchAliasTables, matchCase, matchWholeWord);
                    }
                }
                else //no selection, so start at the root node.
                {
                    results = tree.Find(textToFind, searchAliasTables, matchCase, matchWholeWord);

                }

                if (results != null && results.Count > 0)
                {
                    tree.SelectNode(results[0], autoCheckMember);
                }
                else
                {
                    ret = false;
                }
                tree.Select();
            }
            return ret;
        }

        public void SuspendDrawing()
        {
            if (suspendCounter == 0)
            {
                SendMessage(this.Handle, WM_SETREDRAW, false, 0);
            }
            suspendCounter++;
        }

        public void ResumeDrawing()
        {
            suspendCounter--;
            if (suspendCounter == 0)
            {
                SendMessage(this.Handle, WM_SETREDRAW, true, 0);
                Refresh();
            }
        }

        #endregion Methods

        #region Handlers

// ReSharper disable InconsistentNaming
        private void searchControl_SearchTextModified(object sender, SearchEventArgs e)

        {
            if(String.IsNullOrEmpty(e.SearchText))
            {

                toolStripStatusLabel.Text = String.Empty;
                //searchControl.ButtonText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Find");
            }
        }

        private void searchControl_SearchInvoked(object sender, SearchEventArgs e)
        {
            string textToFind = e.SearchText;
            bool matchCase = Settings.Default.Find_MatchCase;
            bool autoCheckMember = Settings.Default.Find_AutoCheckMember;
            bool searchAliasTables = Settings.Default.Find_SearchAliasTables;
            bool matchWholeWord = Settings.Default.Find_MatchWholeWord;
            bool results = false;
            if (!String.IsNullOrEmpty(textToFind))
            {
                results = Find(textToFind, searchAliasTables, matchCase, matchWholeWord, autoCheckMember);
            }
            if (!results)
            {
                toolStripStatusLabel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Find.Message.NoMembersFound");
                searchControl.ButtonText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Find");
            }
            else
            {
                toolStripStatusLabel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Find.Message.SearchNextNode");
                searchControl.ButtonText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Next");
            }
        }

        private void mwTree_MouseUp(object sender, MouseEventArgs e)
        {
            //Get the dimension tree
            PaceTreeView tree = (PaceTreeView)sender;

            if (e.Button == MouseButtons.Right && tree != null)
            {

                TabPage page = (TabPage) tree.Parent;

                string dimName = page.Text;
                if (dimName.Substring(dimName.Length - 1, 1).Equals(Required))
                {
                    dimName = dimName.Substring(0, dimName.Length - 1);
                }


                barButtonItemFind.Enabled = false;
                barButtonItemClear.Caption = _clear + " " + dimName +  " " + _selections;
                barButtonItemClear.Tag = dimName;
                barCheckItemSelections.Caption = _remember + " " + dimName + " " + _selections;
                barCheckItemSelections.Tag = dimName;
                barCheckItemRememberAll.Tag = dimName;
                barCheckItemSelections.Tag = dimName;

                Point clickPoint = new Point(e.X, e.Y);
                TreeNode clickNode = tree.GetNodeAt(clickPoint);
                if (clickNode == null) return;
                // Convert from Tree coordinates to Screen coordinates
                Point screenPoint = tree.PointToScreen(clickPoint);
                // Convert from Screen coordinates to Form coordinates
                //Point formPoint = PointToClient(screenPoint);

                barButtonItemFind.Enabled = true;


                //contextMenuStrip.Show(this, formPoint);
                popupMenu.ShowPopup(screenPoint);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mwTree_AfterCheck(object sender, TreeViewEventArgs e)
        {
            cmdOk.Enabled = CheckOkButtonStatus();

            PaceTreeNode node = (PaceTreeNode)e.Node;

            HandleNodeMouseClick(node);

            //Get the dimension tree
            PaceTreeView t = (PaceTreeView)sender;
            if (t.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Attr))
            {
                CacheUserSelections(t, false);
                DisableAttributeTabs(t.Name);
            }
        }


        private void mwTree_BeforeCheck(object sender, TreeViewCancelEventArgs e)
        {
            //Get the dimension tree
            PaceTreeView tree = (PaceTreeView)sender;

            if(e.Cancel)
            {

                MessageBoxButtons mbb = MessageBoxButtons.OK;
                bool warning = false;
                string message = tree.CannotSelectNodeErrorMessage;
                DialogResult result = DialogResult.OK;
                if(String.IsNullOrEmpty(message))
                {
                    warning = true;
                }
                else
                {
                    result = MessageBox.Show(message,
                                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                                mbb,
                                MessageBoxIcon.Information);
                }

                if (result == DialogResult.OK && warning)
                {
                    tree.UncheckAllNodes();
                    e.Cancel = false;
                }
                else
                {
                    return;
                }
            }
        }

        /// <summary>
        /// Event handler for when a user clicks a node in the view tree.
        /// </summary>
        /// <param name="treeNode">The tree node that was clicked by the user.</param>
        private void HandleNodeMouseClick(PaceTreeNode treeNode)
        {
            //if this tab is an attribue tab then just return...
            PaceTreeView treeView = (PaceTreeView) treeNode.TreeView;
            if (treeView.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Attr))
            {
                return;
            }

            foreach(KeyValuePair<string, string> kvp in _attrBaseLookup)
            {
                if (treeNode.TreeView.Name == kvp.Value)
                {
                    PaceTreeView tree = GetTreeView(kvp.Key);
                    if(tree != null)
                    {
                        //Clear out any stored out attribute slections.
                        _cachedTreeNodes.RemoveSelection(tree.Name);
                        _cachedPafDimSpec.Remove(tree.Name);
                        _attrTreeNodes.RemoveSelection(tree.Name);
                        _validAttributes.Remove(tree.Name);
                        //Just clear everything out.
                        tree.UncheckAllNodes();
                        
                    }
                }
            }
            ResetAllTabs();
        }

        /// <summary>
        /// Before expand event handler.  Replaces the "Dummy" nodes with the real nodes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mwTree_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            PaceTreeView tree = (PaceTreeView)sender;
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Start expanding tree dimension: " + tree.Name);

            try
            {
                tree.BeginUpdate();

                //If the first node is not the "Dummy" node, then exit.
                //This stops us from adding nodes that have already been added.
                if (!e.Node.FirstNode.Text.Equals("Dummy"))
                {
                    return;
                }

                //Ok, we have the root level node text (which is the dim name), so we need to
                //get the children of e.Node.Tag, which is the curretnly selected node.
                string[] children = _roleFiltersSimpleTrees.GetChildren(tree.Name, e.Node.Tag.ToString());

                //Remove that "Dummy" node off the tree control.
                e.Node.FirstNode.Remove();

                pafSimpleDimTree simpleDimTree = _roleFiltersSimpleTrees.GetTree(tree.Name);

                //Make sure that their are some children
                if (children != null)
                {
                    foreach (string child in children)
                    {
                        //See if the children have children.
                        string[] grandChildren = _roleFiltersSimpleTrees.GetChildren(tree.Name, child);
                        string member = _roleFiltersSimpleTrees.GetMember(tree.Name, child).key;

                        Dictionary<string, string> aliasValues = _roleFiltersSimpleTrees.GetAliasValues(simpleDimTree, child, simpleDimTree.aliasTableNames);


                        if (grandChildren == null)
                        {
                            //Debug the children if necessary.
                            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Children of \"" + child + "\" not found.");

                            //The node to be added doesn't have any children so don't add the "Dummy" keyword,
                            //just add the items.
                            e.Node.Nodes.Add(new PaceTreeNode(
                                    child,
                                    member,
                                    aliasValues,
                                    GetStringDisplayFormat()));


                        }
                        else
                        {
                            //Debug the children if necessary.
                            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Children of \"" + child + "\" found " + String.Join(",", grandChildren));

                            //The node has children so add the "Dummy" node.
                            //The "Dummy" keyword is added as a child so the "+" appears next to the node.
                            e.Node.Nodes.Add(new PaceTreeNode(
                                child,
                                member,
                                aliasValues,
                                GetStringDisplayFormat(),
                                new[] { new PaceTreeNode("Dummy") }
                                ));

                        }
                    }
                }
            }
            finally
            {
                tree.EndUpdate();
                if (tree.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Attr))
                {
                    DisableAttributeTreeNodes(tree, e.Node.Nodes);
                }
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Ending expanding tree dimension: " + tree.Name);
            }
        }

        /// <summary>
        /// Disable the attribute tree nodes.
        /// </summary>
        /// <param name="tree">The attribute tree view.</param>
        /// <param name="tnc">The attribute tree node collection.</param>
        private void DisableAttributeTreeNodes(PaceTreeView tree, TreeNodeCollection tnc)
        {
            string[] selections = _validAttributes.ValueOrDefault(tree.Name);
            //disable the tree nodes
            tree.DisableTreeNodes(tnc);

            if (selections != null)
            {
                foreach (string str in selections)
                {
                    List<PaceTreeNode> tn = tree.Find(str, true, false, true);

                    if (tn != null && tn.Count > 0)
                    {
                        foreach(PaceTreeNode p in tn)
                        {
                            //TreeViewUtilities.EnableNode(p, tabRoleFilter.ForeColor);
                            tree.EnableNode(p);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Resets all the tab pages to the default.
        /// </summary>
        private void ResetAllTabs()
        {
            if(tabRoleFilter.TabCount > 0)
            {
                foreach(TabPage page in tabRoleFilter.TabPages)
                {
                    page.Enabled = true;
                    page.ToolTipText = "";
                }
            }
        }

        /// <summary>
        /// Handles tab disabling of attributes tabs.
        /// </summary>
        /// <param name="dimName">Name of the attribute dimension.</param>
        private void DisableAttributeTabs(string dimName)
        {
            List<PaceTreeNode> selections = _attrTreeNodes.GetCachedSelections(dimName, dimName);
            if (selections == null || selections.Count == 0)
            {
                return;
            }

            attributeDimInfo attrDimInfo = null;
            foreach (attributeDimInfo adi in _attributeDimInfo)
            {
                if (dimName == adi.dimName)
                {
                    attrDimInfo = adi;
                }
            }
            
            if (attrDimInfo != null)
            {
                //set the base level.
                int baseLevel = attrDimInfo.baseDimMappingLevel;

                //loop thur the levels and disable the tabs that arent at the same level.
                foreach (attributeDimInfo adi in _attributeDimInfo)
                {
                    if (adi.baseDimMappingLevel == baseLevel)
                    {
                        PaceTreeView t = GetTreeView(adi.dimName);
                        if (t != null)
                        {
                            TabPage tp = (TabPage) t.Parent;
                            tp.Enabled = true;
                            tp.ToolTipText = "";
                        }
                    }
                    else
                    {
                        PaceTreeView t = GetTreeView(adi.dimName);
                        if (t != null)
                        {
                            TabPage tp = (TabPage) t.Parent;
                            tp.Enabled = false;
                            tp.ToolTipText =
                                PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.AttrBaseMbrLevelDontMatch");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Resize event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event Args.</param>
        private void RoleFilter_Resize(object sender, EventArgs e)
        {
            int extraCtrlHeight = 0;
            if (_extraControlsVisible)
            {
                extraCtrlHeight = pnlOptions.Height;
            }

            pnlTabs.Height = Height - pnlTabs.Top - extraCtrlHeight - pnlButtons.Height - statusStrip.Height - _pad;
        }

        /// <summary>
        /// Ok button event handler.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event Args.</param>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            //clear the checked selecs lists
            _cachedTreeNodes.Clear();
            _cachedPafDimSpec.Clear();
            _selectedAtrTreeNodes.Clear();
            //store the old sel in a tmp var.
            pafDimSpec[] tmp = _previousPafDimSpec;
            //move paf dim spec to the previous
            _previousPafDimSpec = _pafDimSpec;
            //create a new paf dim spec.
            _pafDimSpec = new pafDimSpec[_dims.Count];
            //create a list to hold the pafdimspec
            List<pafDimSpec> pafDimSpec = new List<pafDimSpec>();

            foreach (PaceTreeView tree in _dims)
            {
                bool isAttributeDim = false;
                if (tree.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Base))
                {
                    pafDimSpec.Add(CacheUserSelections(tree, true));
                }
                else
                {
                    isAttributeDim = true;
                    pafDimSpec.Add(CacheUserSelections(tree, false));
                    List<PaceTreeNode> nodes = _attrTreeNodes.GetCachedSelections(tree.Name, tree.Name);
                    if (nodes != null && nodes.Count > 0)
                    {
                        if (!_selectedAtrTreeNodes.ContainsKey(tree.Name))
                        {
                            _selectedAtrTreeNodes.Add(tree.Name, nodes);
                        }
                        else
                        {
                            _selectedAtrTreeNodes[tree.Name] = nodes;
                        }
                    }
                }
                if (_savedSelections.DimensionIgnored(tree.DimensionName)) continue;
                if (tree.CheckedNodes.Count > 0)
                {
                    _savedSelections.AddKeyValue(tree.DimensionName, tree.CheckedNodesAsString(), true);
                }
                else
                {
                    // Remove any pre-existing selections for any unselected attribute dimensions (TTN-2654)
                    if (isAttributeDim)
                    {
                        _savedSelections.RemoveKeyValue(tree.DimensionName);
                    }
                }
            }

            //converts the list to an array.
            _pafDimSpec = pafDimSpec.ToArray();

            if(_pafDimSpec != _previousPafDimSpec)
            {
                bool Cancel = false;
                if (FilterChanged != null)
                {
                    FilterChanged(this, 
                        new LogonEventArgs(_role, _seasonProc, LogonEventArgs.ChangeType.RoleFilter), 
                        ref Cancel);

                    if (Cancel)
                    {
                        //put the old variables back.
                        _pafDimSpec = _previousPafDimSpec;
                        _previousPafDimSpec = tmp;
                        cmdCancel.PerformClick();
                    }
                }
            }


            if (_extraControlsVisible)
            {
                if (chkFilteredTotals.Enabled)
                {
                    Settings.Default.FilteredTotals = chkFilteredTotals.Checked;
                }
                if (chkSupInvIx.Enabled)
                {
                    Settings.Default.SuppressInvIx = chkSupInvIx.Checked;
                }
                Settings.Default.Save();
                Settings.Default.Reload();
            }

            PafApp.GetPafAppSettingsHelper().SaveRoleFilterSavedSelections(_savedSelections);

            //Disable sticky selections
            _cachedTreeNodes.Clear();
            _attrTreeNodes.Clear();
            _selectedAtrTreeNodes.Clear();
            _cachedPafDimSpec.Clear();
        }

        /// <summary>
        /// Occurs when the selected index changes.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event args</param>
        private void tabRoleFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            tabRoleFilter_SelectedIndexChanged();
        }

        /// <summary>
        /// Occurs when the selected index changes.
        /// </summary>
        /// <param name="tabIndex"></param>
        private void tabRoleFilter_SelectedIndexChanged(int? tabIndex = null)
        {
            if (_updating)
            {
                return;
            }

            //Debug.WriteLine("Current Tab: " + tabRoleFilter.SelectedIndex);

            if (tabIndex == null)
            {
                tabIndex = tabRoleFilter.SelectedIndex;
            }

            PaceTreeView tree = (PaceTreeView)tabRoleFilter.TabPages[tabIndex.Value].Controls[0];


            if (!tree.AliasTableToDisplay.Equals(GetStringDisplayFormat()))
            {
                try
                {
                    tree.BeginUpdate();
                    tree.Nodes.ChangeTreeNodeText(GetStringDisplayFormat());
                }
                finally
                {
                    tree.EndUpdate();
                }
            }

            if (!tree.AliasTableToDisplay.Equals(GetStringDisplayFormat()))
            {
                tree.AliasTableToDisplay = GetStringDisplayFormat();
            }


            if (tree.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Attr))
            {
                foreach (PaceTreeView at in _dims)
                {
                    if (at.DimensionType.Equals(PaceTreeNode.PaceTreeDimensionType.Attr))
                    {
                        if (at.Name != tree.Name)
                        {
                            CacheUserSelections(at, false);
                        }
                    }
                }
                //set the valid attributes in the cache.
                SetAttributes(tree);
                //disable the attribute tree nodes.
                DisableAttributeTreeNodes(tree, tree.Nodes);
                //disable attribute tabs.
                DisableAttributeTabs(tree.Name);
                //force a repaint.
                tree.Refresh();
                //Expand the root node
                if (tree.RootNode != null)
                {
                    tree.RootNode.Expand();
                }
            }
        }

        /// <summary>
        /// Handler for the reset handler button.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event Args.</param>
        private void cmdResetSel_Click(object sender, EventArgs e)
        {
            DialogResult res = MessageBox.Show(
                PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ResetFilteredSelections"),
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button1);

            if (res == DialogResult.OK)
            {
                //clear the checked selecs lists
                ClearCachedSelectionObjects();
                //reslect any cached nodes.
                SelectOrReselectNodes();
                //reset the tab pages.
                ResetAllTabs();
                //need to simulate callback to refresh attribute refresh
                tabRoleFilter_SelectedIndexChanged(null, null);

            }
        }

        private void cboDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabRoleFilter.TabCount <= 0) return;

            PaceTreeView tree = (PaceTreeView)tabRoleFilter.TabPages[tabRoleFilter.SelectedIndex].Controls[0];

            string displayFormat = GetStringDisplayFormat();

            if (!tree.AliasTableToDisplay.Equals(displayFormat))
            {
                try
                {
                    tree.BeginUpdate();
                    tree.Nodes.ChangeTreeNodeText(displayFormat);
                }
                finally
                {
                    tree.EndUpdate();
                }
            }
            if (!tree.AliasTableToDisplay.Equals(GetStringDisplayFormat()))
            {
                tree.AliasTableToDisplay = GetStringDisplayFormat();
            }

            Settings.Default.RoleFilterLastAliasTable = displayFormat;
            Settings.Default.Save();
            Settings.Default.Reload();
        }


        private void findToolStripMenuItem_Click(object sender, EventArgs e)
        {

            bool preMatchCase = Settings.Default.Find_MatchCase;
            frmFind find = new frmFind();

            if (find.ShowDialog(this) == DialogResult.OK)
            {
                string textToFind = find.SearchText;
                bool matchCase = find.MatchCase;
                bool searchAliasTables = find.SearchAliasTables;
                bool autoCheckMember = find.AutoCheckMember;
                bool matchWholeWord = find.MatchWholeWord;
                if (!String.IsNullOrEmpty(textToFind))
                {
                    if (preMatchCase != matchCase)
                    {
                        ClearSearchFilters();
                    }
                    Find(textToFind, searchAliasTables, matchCase, matchWholeWord, autoCheckMember);
                }
            }
            else
            {
                if (preMatchCase != find.MatchCase)
                {
                    ClearSearchFilters();
                }
            }
        }

        void ToolStripButton_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem senderButton = (ToolStripMenuItem)sender;
            foreach (ToolStripMenuItem mi in toolStripDropDownButton.DropDownItems)
            {
                if (mi == senderButton)
                {
                    mi.Checked = true;
                    mi.CheckState = CheckState.Checked;
                }
                else
                {
                    mi.Checked = false;
                }
            }
            cboDisplay_SelectedIndexChanged(null, null);

        }

        private void chkSupInvIx_CheckedChanged(object sender, EventArgs e)
        {
            if (_filteredSubtotals && !_discontigSecurity)
            {
                //chkFilteredTotals.Enabled = chkSupInvIx.Checked;
            }
        }

        private void popupMenu_BeforePopup(object sender, CancelEventArgs e)
        {
            try
            {
                _updating2 = true;

                bool status = PafApp.GetPafAppSettingsHelper().PersistRoleFilterSelectionsEnabled();
                BarItemVisibility vis = BarItemVisibility.Always;
                if (!status)
                {
                    vis = BarItemVisibility.Never;
                }

                string dimName = barButtonItemClear.Tag.ToString();

                //per Carolyn this button should always be available
                //barButtonItemClear.Enabled = _savedSelections.SavedSelectionsDictionary.ContainsKey(dimName);
                barButtonItemClear.Enabled = true;
                //barButtonItemClearAll.Enabled = _savedSelections.SavedSelectionsDictionary.Count > 0;
                barButtonItemClearAll.Enabled = true;
                //barCheckItemRememberAll.Enabled = true;
                //barCheckItemRememberAll.Checked = _savedSelections.DimensionsToIgnore.Count == 0 && !_savedSelections.IgnoreAllDimensions;
                barCheckItemRememberAll.Enabled = _savedSelections.DimensionsToIgnore.Count > 0 || _savedSelections.IgnoreAllDimensions;
                barCheckItemSelections.Enabled = true;
                barCheckItemSelections.Checked = (!_savedSelections.DimensionIgnored(dimName) && !_savedSelections.IgnoreAllDimensions);

                barButtonItemClear.Visibility = vis;
                barButtonItemClearAll.Visibility = vis;
                barCheckItemRememberAll.Visibility = vis;
                barCheckItemSelections.Visibility = vis;
            }
            finally
            {
                _updating2 = false;
            }
            
        }

        private void barButtonItemFind_ItemClick(object sender, ItemClickEventArgs e)
        {
            bool preMatchCase = Settings.Default.Find_MatchCase;
            frmFind find = new frmFind();

            if (find.ShowDialog(this) == DialogResult.OK)
            {
                string textToFind = find.SearchText;
                bool matchCase = find.MatchCase;
                bool searchAliasTables = find.SearchAliasTables;
                bool autoCheckMember = find.AutoCheckMember;
                bool matchWholeWord = find.MatchWholeWord;
                if (!String.IsNullOrEmpty(textToFind))
                {
                    if (preMatchCase != matchCase)
                    {
                        ClearSearchFilters();
                    }
                    Find(textToFind, searchAliasTables, matchCase, matchWholeWord, autoCheckMember);
                }
            }
            else
            {
                if (preMatchCase != find.MatchCase)
                {
                    ClearSearchFilters();
                }
            }
        }

        private void barButtonItemClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            string dim = e.Item.Tag.ToString();
            if (String.IsNullOrEmpty(dim)) return;

            _savedSelections.RemoveKeyValue(dim);
            PafApp.GetPafAppSettingsHelper().SaveRoleFilterSavedSelections(_savedSelections);

            barCheckItemSelections.Checked = false;

            PafApp.GetLogger().InfoFormat("Clearing saved role filter selection for dimension: {0}", dim);
        }

        private void barButtonItemClearAll_ItemClick(object sender, ItemClickEventArgs e)
        {

            _savedSelections.ClearAllKeyValues();
            //Add all ignored dims.
            _savedSelections.ClearAllIgnoredDimensions();
            _savedSelections.IgnoreAllDimensions = true;
            ClearCachedSelectionObjects();
            SelectOrReselectNodes();

            PafApp.GetPafAppSettingsHelper().SaveRoleFilterSavedSelections(_savedSelections);
            PafApp.GetLogger().Info("Clearing all saved role filter selections and setting dimensions remembered state to false");
        }

        private void barCheckItemSelections_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_updating2) return;

            string dim = e.Item.Tag.ToString();
            if (String.IsNullOrEmpty(dim)) return;

            BarCheckItem item = (BarCheckItem) e.Item;

            if (!item.Checked)
            {
                _savedSelections.RemoveKeyValue(dim);
                _savedSelections.AddIgnoredDimension(dim);
                ClearCachedSelectionObject(dim);
                SelectOrReselectNodes(dim);
            }
            else
            {
                _savedSelections.RemoveIgnoredDimension(dim);
                if (_savedSelections.IgnoreAllDimensions)
                {
                    _savedSelections.IgnoreAllDimensions = false;
                    foreach (var s in _base.Where(s => !s.Equals(dim)))
                    {
                        _savedSelections.AddIgnoredDimension(s);
                    }
                    foreach (var s in _attr.Where(s => !s.Equals(dim)))
                    {
                        _savedSelections.AddIgnoredDimension(s);
                    }
                }
            }
            PafApp.GetPafAppSettingsHelper().SaveRoleFilterSavedSelections(_savedSelections);

            PafApp.GetLogger().InfoFormat("Setting dimension: {0} remembered state to: {1}", dim, item.Checked);
        }

        private void barCheckItemRememberAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (_updating2) return;

            _savedSelections.ClearAllIgnoredDimensions();
            _savedSelections.IgnoreAllDimensions = false;

            PafApp.GetPafAppSettingsHelper().SaveRoleFilterSavedSelections(_savedSelections);
            PafApp.GetLogger().InfoFormat("Setting all dimensions remembered state to true");
        }

        /// <summary>
        /// Dead Code.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void barCheckItemRememberAll_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_updating2) return;

            string dim = e.Item.Tag.ToString();
            if (String.IsNullOrEmpty(dim)) return;

            BarCheckItem item = (BarCheckItem)e.Item;

            if (!item.Checked)
            {
                _savedSelections.ClearAllKeyValues();
                //Add all ignored dims.
                _savedSelections.ClearAllIgnoredDimensions();
                _savedSelections.IgnoreAllDimensions = true;
                ClearCachedSelectionObjects();
            }
            else
            {
                _savedSelections.ClearAllIgnoredDimensions();
                _savedSelections.IgnoreAllDimensions = false;
            }
            PafApp.GetPafAppSettingsHelper().SaveRoleFilterSavedSelections(_savedSelections);
            PafApp.GetLogger().InfoFormat("Setting all dimensions remembered state to: {0}", item.Checked);

        }

        private void tabRoleFilter_MouseClick(object sender, MouseEventArgs e)
        {
            TabControl tabControl = (TabControl) sender;

            
            if (e.Button != MouseButtons.Right || tabControl == null)
            {
                return;
            }

            TabPage page = tabControl.SelectedTab;

            string dimName = page.Text;
            if (dimName.Substring(dimName.Length - 1, 1).Equals(Required))
            {
                dimName = dimName.Substring(0, dimName.Length - 1);
            }


            barButtonItemFind.Enabled = false;
            barButtonItemClear.Caption = _clear + " " + dimName + " " + _selections;
            barButtonItemClear.Tag = dimName;
            barCheckItemSelections.Caption = _remember + " " + dimName + " " + _selections;
            barCheckItemSelections.Tag = dimName;
            barCheckItemRememberAll.Tag = dimName;
            barCheckItemSelections.Tag = dimName;

            Point clickPoint = new Point(e.X, e.Y);
            Point screenPoint = page.PointToScreen(clickPoint);

            popupMenu.ShowPopup(screenPoint);
        }

  

        // ReSharper restore InconsistentNaming
        #endregion Handlers

    }
}