#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    /// <summary>
    /// 
    /// </summary>
    internal partial class PersistToServerSelector : UserControl
    {
        /// <summary>
        /// Signals if the data should be saved to essbase.
        /// </summary>
        public CheckBox SaveEssbaseData;

        /// <summary>
        /// Signals if the cell notes should be saved.
        /// </summary>
        public CheckBox SaveCellNotes;

        private GroupBox groupBox1;
        private Panel panel1;

        /// <summary>
        /// Signals if the member tags should be saved.
        /// </summary>
        public CheckBox SaveMbrTags;
        private const int PAD = 6;

        /// <summary>
        /// Constructor.
        /// </summary>
        public PersistToServerSelector()
        {
            InitializeComponent();
            loadUi();
        }

        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                bool s = false;
                bool s2 = false;
                bool s3 = false;

                SaveEssbaseData.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SaveChanges");
                SaveCellNotes.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SaveCellNotes");
                SaveMbrTags.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.Menu.SaveMbrTags");

                if (PafApp.GetSaveWorkMngr().DataWaitingToBeSaved)
                {
                    s = true;
                }
                if(PafApp.GetCellNoteMngr().IsClientCacheDirty())
                {
                    s2 = true;
                }
                if (PafApp.GetMbrTagMngr().IsMbrTagCacheDirty())
                {
                    s3 = true;
                }

                SaveEssbaseData.Enabled = s;
                SaveEssbaseData.Checked = s;
                SaveCellNotes.Enabled = s2;
                SaveCellNotes.Checked = s2;
                SaveMbrTags.Enabled = s3;
                SaveMbrTags.Checked = s3;
                PersistToServerSelector_Resize(null, null);
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Realods the ui components.
        /// </summary>
        public void ReloadUi()
        {
            loadUi();
        }

        /// <summary>
        /// Control resize event handler.
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args.</param>
        private void PersistToServerSelector_Resize(object sender, EventArgs e)
        {
            SaveEssbaseData.Left = PAD;
            SaveCellNotes.Left = PAD;
            SaveMbrTags.Left = PAD;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event args</param>
        private void SaveMbrTags_CheckedChanged(object sender, EventArgs e)
        {
            errorProvider1.Clear();

            CheckBox c = (CheckBox) sender;

            if (PafApp.GetViewMngr().CurrentProtectionMngr.CalculationsPending())
            {
                errorProvider1.SetError(c,
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.PersistToServerSelector.Exception.Calculate"));
                c.Enabled = false;
                c.Checked = false;
            }
        }
    }
}