#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Threading;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    internal partial class ChangePassword : UserControl
    {

        private string _username;

        private string _oldPassword;

        private string _newPassword = null;

        //default is 6, can be overwritten from server
        private int _minNewPasswordLength = 6;

        //default is 16, can be overwritten from server
        private int _maxNewPasswordLength = 16;

        /// <summary>
        /// Constructs user control
        /// </summary>
        public ChangePassword()
        {
            InitializeComponent();
            loadUi();
        }

        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                //Get the labels from the localized .resx file.
                Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                lblUserId.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ChangePassword.UserNameLabelText");
                lblOldPassword.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ChangePassword.OldPasswordLabelText");
                lblNewPassword.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ChangePassword.NewPasswordLabelText");
                lblConfirmNewPassword.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ChangePassword.ConfirmNewPasswordLabelText");

            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Set/Get the Username
        /// </summary>
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;

                if (!String.IsNullOrEmpty(_username))
                {
                    txtUserName.Text = _username;
                }

            }
        }

        /// <summary>
        /// Set/Get the Old Password
        /// </summary>
        public string OldPassword
        {
            get { return _oldPassword; }
            set
            {
                _oldPassword = value;

                if (!String.IsNullOrEmpty(_oldPassword))
                {
                    txtOldPassword.Text = _oldPassword;
                }
            }
        }

        /// <summary>
        /// Set/Get the min new password length
        /// </summary>
        public int? MinNewPasswordLength
        {
            get { return _minNewPasswordLength; }
            set
            {
                if (value != null)
                {
                    _minNewPasswordLength = (int)value;
                }
            }
        }

        /// <summary>
        /// Set/Get the max new password length
        /// </summary>
        public int? MaxNewPasswordLength
        {
            get { return _maxNewPasswordLength; }
            set
            {
                if (value != null)
                {
                    _maxNewPasswordLength = (int)value;
                }
            }
        }

        /// <summary>
        /// Get the min new password length
        /// </summary>
        public string NewPassword
        {
            get { return _newPassword; }
        }
        
        /// <summary>
        /// Called when user control is loading
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChangePassword_Load(object sender, EventArgs e)
        {
            //Seed the userid/password boxes, if properties are set.
            if (!String.IsNullOrEmpty(_username))
            {
                txtUserName.Text = _username;
            }

            if (!String.IsNullOrEmpty(_oldPassword))
            {
                txtOldPassword.Text = _oldPassword;
            }

        }

        /// <summary>
        /// return true when form data is valid.
        /// </summary>
        private Boolean isFormDataValid()
        {
            lblError.Text = "";
            
            string newPassword1 = txtNewPassword1.Text.Trim();
            string newPassword2 = txtNewPassword2.Text.Trim();

            //if length val passes but passwords don't match
            if ( newPassword1.Length >= _minNewPasswordLength && newPassword1.Length <= _maxNewPasswordLength && ! newPassword1.Equals(newPassword2) )
            {
                lblError.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ChangePassword.ErrorMessage.PasswordsDontMatch");
                return false;
            }

            //if any password 1 or 2 are out of range
            if (newPassword1.Length < _minNewPasswordLength || newPassword1.Length > _maxNewPasswordLength
                || newPassword2.Length < _minNewPasswordLength || newPassword2.Length > _maxNewPasswordLength)
            {
                lblError.Text = String.Format(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ChangePassword.ErrorMessage.PasswordRangeError"), _minNewPasswordLength, _maxNewPasswordLength);
                return false;
            }

            //if password 1 and 2 don't match
            if (!newPassword1.Equals(newPassword2))
            {
                lblError.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ChangePassword.ErrorMessage.PasswordsDontMatch");
                return false;
            }

            return true;

        }

        /// <summary>
        /// update the state of the ok button
        /// </summary>
        private void updateOkButtonState()
        {
            cmdOk.Enabled = isFormDataValid();
        }

        /// <summary>
        /// Called when text changes in the old password field.  Updates Ok button state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtOldPassword_TextChanged(object sender, EventArgs e)
        {
            updateOkButtonState();
        }

        /// <summary>
        /// Called when text changes in the new password 1 field.  Updates Ok button state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNewPassword1_TextChanged(object sender, EventArgs e)
        {
            updateOkButtonState();
        }

        /// <summary>
        /// Called when text changes in the new password 2 field.  Updates Ok button state.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNewPassword2_TextChanged(object sender, EventArgs e)
        {
            updateOkButtonState();
        }

        /// <summary>
        /// Control KeyDown Handler.
        /// </summary>
        private void TextControlKeyDown(object sender, KeyEventArgs e)
        {
            ControlKeyPress(e);
        }

        /// <summary>
        /// Method to check if the keypress was an enter key and if the ok button is enabled, 
        /// click it for the user.
        /// </summary>
        /// <param name="e">KeyEventArgs</param>
        private void ControlKeyPress(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmdOk.Enabled)
                {
                    cmdOk.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                cmdCancel.PerformClick();
            }
        }

        /// <summary>
        /// Called when Ok Button clicked.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            _oldPassword = txtOldPassword.Text.Trim();
            _newPassword = txtNewPassword1.Text.Trim();
        }

        /// <summary>
        /// Called when focus is brought to new password 1 text box.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNewPassword1_Enter(object sender, EventArgs e)
        {
            updateOkButtonState();
        }
    }
}
