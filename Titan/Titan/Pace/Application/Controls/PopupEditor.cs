﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Menu;
using DevExpress.XtraTreeList.Nodes;
using Titan.Pace.Application.Controls.TreeView;
using Titan.Pace.Application.Extensions;

namespace Titan.Pace.Application.Controls
{
    /// <remarks/>
    internal partial class PopupEditor : UserControl
    {
        private string LevelColumnName { get; set; }
        private string GenerationColumnName { get; set; }
        private List<ComboBoxDisplay> AliasTableNames { get; set; }
        private string MemberNameColumnName { get; set; }
        private string MemberKeyColumnName { get; set; }
        private string ParentKeyColumnName { get; set; }
        private SessionLockSelectedMembers DefaultMemberNames { get; set; }
        private List<int> LevelsAllowedForSelection { get; set; }
        private readonly DataTable _dataTable;

        /// <remarks/>
        public PopupEditor(DataTable results, string dimName, string memberKeyColumnName, string parentKeyColumnName, string levelColumnName, string generationColumnName,
            string memberNameColumnName, List<ComboBoxDisplay> aliasTableNames, SessionLockSelectedMembers defaultMemberNames, List<int> levelsAllowedForSelection)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            InitializeComponent();
            _dataTable = results;
            cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
            cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
            txtSearch.Properties.NullValuePrompt = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.PopupEditor.Search.Tooltip");
            txtMemberSearch.Properties.NullValuePrompt = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.PopupEditor.MemberSearch.Tooltip");
            barButtonUncheckAll.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ClearAllCheckboxes.Text");
            barButtonCheckAll.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SelectAllCheckboxes.Text");
            barButtonCheckAllChildren.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SelectAllChildren.Text");
            barButtonCheckAllDesc.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SelectAllDescendants.Text");
            aliasTable.ToolTip = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.AliasTable.ToolTip");
            
            treeList1.BeginUpdate();
            treeList1.BeginUnboundLoad();

            LevelColumnName = levelColumnName;
            GenerationColumnName = generationColumnName;
            MemberNameColumnName = memberNameColumnName;
            AliasTableNames = aliasTableNames;
            MemberKeyColumnName = memberKeyColumnName;
            ParentKeyColumnName = parentKeyColumnName;
            LevelsAllowedForSelection = levelsAllowedForSelection;
            treeList1.DataSource = _dataTable;
            treeList1.KeyFieldName = MemberKeyColumnName;
            treeList1.ParentFieldName = ParentKeyColumnName;
            treeList1.Columns.ColumnByFieldName(LevelColumnName).Visible = false;
            treeList1.Columns.ColumnByFieldName(GenerationColumnName).Visible = false;
            DefaultMemberNames = defaultMemberNames;
            OriginalMemberNames = defaultMemberNames;

            treeList1.EndUnboundLoad();

            if (!DefaultMemberNames.SelectionEmpty())
            {
                SetSelections(DefaultMemberNames);
            }

            aliasTable.Properties.Items.Clear();
            foreach (ComboBoxDisplay alias in AliasTableNames)
            {
                aliasTable.Properties.Items.Add(alias);
            }
            aliasTable.SelectedIndex = 0;

            treeList1.EndUpdate();

            treeList1.AfterCheckNode += new NodeEventHandler(treeList1_AfterCheckNode);

            cmdOk.Enabled = treeList1.GetCheckedNodes().Count != 0;

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("PopupEditor: {0} runtime: {1} (ms)", new object[] { dimName, stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

        /// <summary>
        /// Original Members that were set in the control
        /// </summary>
        public SessionLockSelectedMembers OriginalMemberNames { get; set; }

        private void treeList1_AfterCheckNode(object sender, NodeEventArgs e)
        {
            SetOkButtonStatus();
        }

        private void SetOkButtonStatus()
        {
            cmdOk.Enabled = treeList1.GetCheckedNodes().Count != 0;
        }

        /// <summary>
        /// Sets the selections on the TreeView
        /// </summary>
        /// <param name="memberNames"></param>
        public void SetSelections(SessionLockSelectedMembers memberNames)
        {
            if (memberNames.MultiMember())
            {
                SetMemberSelections(memberNames);
            }
            else
            {
                SetMemberSelection(memberNames);
            }
        }

        /// <summary>
        /// Sets the selection on the TreeView
        /// </summary>
        /// <param name="memberNames"></param>
        private void SetMemberSelection(SessionLockSelectedMembers memberNames)
        {
            treeList1.ClearTreeSelections();

            treeList1.CheckNode(memberNames.SingleSelection(), AliasTableNames.Select(x => x.Value).ToList(), true, true, false);

            TreeListNode node = treeList1.GetCheckedNode();

            TreeListNode rootNode = treeList1.RootNode();

            if (rootNode == null) return;

            if (node == rootNode)
            {
                if (!node.Expanded)
                {
                    node.Expanded = true;
                }
            }
            else
            {
                treeList1.MakeNodeVisible(rootNode);
            }
        }

        /// <summary>
        /// Sets the selections on the TreeView
        /// </summary>
        /// <param name="memberNames"></param>
        private void SetMemberSelections(SessionLockSelectedMembers memberNames)
        {
            treeList1.ClearTreeSelections();
            int i = 0;
            bool scrollToNode = false;
            foreach (string node in memberNames.Selections)
            {
                if ((i + 1) == memberNames.Selections.Count) scrollToNode = true;
                treeList1.CheckNode(node, AliasTableNames.Select(x => x.Value).ToList(), scrollToNode, true, false);
                i++;
            }
            TreeListNode rootNode = treeList1.RootNode();
            treeList1.MakeNodeVisible(rootNode);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public SessionLockSelectedMembers GetSelections()
        {
            List<string> nodes = treeList1.GetCheckedNodesText(MemberNameColumnName);
            List<string> text = new List<string>(nodes.Count);
            text.AddRange(nodes);
            return new SessionLockSelectedMembers(text);
        }

        #region Event Handlers
        // ReSharper disable InconsistentNaming


        private void treeList1_BeforeCheckNode(object sender, global::DevExpress.XtraTreeList.CheckNodeEventArgs e)
        {
            if(LevelsAllowedForSelection == null || LevelsAllowedForSelection.Count == 0) return;
            //do this to cancel.
            //e.CanCheck = false;
            int level = Convert.ToInt32(e.Node.GetValue(LevelColumnName));
            if(!LevelsAllowedForSelection.Contains(level))
            {
                e.CanCheck = false;
            }
        }

        private void textEdit1_KeyUp(object sender, KeyEventArgs e)
        {
            treeList1.CheckNode(txtMemberSearch.Text, AliasTableNames.Select(x => x.Value).ToList(), true, true, true);
            SetOkButtonStatus();
        }

        private void PopupEditor_Load(object sender, EventArgs e)
        {
            txtMemberSearch.Focus();
        }

        private void txtSearch_KeyUp(object sender, KeyEventArgs e)
        {
            treeList1.FilterNodes();
        }
        private void aliasTable_SelectedIndexChanged(object sender, EventArgs e)
        {
            treeList1.FilterNodes();
        }
        private void cmdOk_Click(object sender, EventArgs e)
        {
            popupContainerControl1.OwnerEdit.ClosePopup();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            popupContainerControl1.OwnerEdit.CancelPopup();
        }

        private void barButtonUncheckAll_ItemClick(object sender, global::DevExpress.XtraBars.ItemClickEventArgs e)
        {
            treeList1.UncheckAll();
        }

        private void treeList1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            if (e.Menu.MenuType == TreeListMenuType.Node)
            {
                TreeList tl = (TreeList)sender;
                popupMenu1.ShowPopup(tl.PointToScreen(e.Point));
            }
        }

        private void treeList1_FilterNode(object sender, FilterNodeEventArgs e)
        {
            if (String.IsNullOrEmpty(txtSearch.Text)) return;
            if (aliasTable.SelectedItem == null) return;
            if (aliasTable.SelectedIndex == -1) return;

            ComboBoxDisplay displayObject = (ComboBoxDisplay) aliasTable.SelectedItem;
            string aliasTableName = displayObject.Value;

            TreeList treeList = e.Node.TreeList;
            if (treeList != null)
            {
                treeList.ExpandAll();
            }

            Trace.WriteLine(e.Node[aliasTableName].ToString());
            if (!e.Node[aliasTableName].ToString().ToLower().Contains(txtSearch.Text.ToLower()))
            {
                e.Node.Visible = false;
                e.Handled = true;
            }

        }

        private void barCheckAllChildren_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeListNode node = treeList1.FocusedNode;
            if (node != null && node.HasChildren)
            {
                node.CheckAllChildren();
            }
        }

        private void barButtonItem1_ItemClick(object sender, ItemClickEventArgs e)
        {
            treeList1.CheckAll();
        }

        private void barButtonItem2_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeListNode node = treeList1.FocusedNode;
            if (node != null && node.HasChildren)
            {
                treeList1.CheckAllDescendants(node.Nodes);
            }
        }

  
        // ReSharper restore InconsistentNaming
        #endregion Event Handlers
    }
}
