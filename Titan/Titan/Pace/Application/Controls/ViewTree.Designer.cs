using DevExpress.XtraTreeList;

namespace Titan.Pace.Application.Controls
{
    partial class ViewTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.subpane = new Subpane();
            this.TreeList = new global::DevExpress.XtraTreeList.TreeList();
            this.toolTipController1 = new global::DevExpress.Utils.ToolTipController(this.components);
            this.barManager1 = new global::DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new global::DevExpress.XtraBars.BarDockControl();
            this.barButtonItemAutoFilter = new global::DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new global::DevExpress.XtraBars.PopupMenu(this.components);
            this.subpane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // subpane
            // 
            this.subpane.AutoSize = true;
            this.subpane.BackColor = System.Drawing.Color.Transparent;
            this.subpane.Controls.Add(this.TreeList);
            this.subpane.DefaultExpandedHeight = 200;
            this.subpane.Dock = System.Windows.Forms.DockStyle.Top;
            this.subpane.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subpane.GradientColorBegin = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.GradientColorEnd = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.HighlightColor = System.Drawing.SystemColors.ControlText;
            this.subpane.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.subpane.IsAnimated = false;
            this.subpane.Location = new System.Drawing.Point(0, 0);
            this.subpane.Name = "subpane";
            this.subpane.Padding = new System.Windows.Forms.Padding(0);
            this.subpane.PanelColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.subpane.Size = new System.Drawing.Size(179, 198);
            this.subpane.TabIndex = 17;
            this.subpane.TabStop = true;
            this.subpane.Tag = "";
            this.subpane.Text = "Subpane";
            this.subpane.BeforeExpanding += new System.ComponentModel.CancelEventHandler(this.subpane_BeforeExpanding);
            this.subpane.AfterExpand += new System.EventHandler(this.subpane_AfterExpand);
            // 
            // TreeList
            // 
            this.TreeList.Appearance.FocusedCell.BackColor = System.Drawing.Color.WhiteSmoke;
            this.TreeList.Appearance.FocusedCell.Options.UseBackColor = true;
            this.TreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeList.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeList.Location = new System.Drawing.Point(0, 21);
            this.TreeList.Name = "TreeList";
            this.TreeList.OptionsBehavior.Editable = false;
            this.TreeList.OptionsBehavior.EnableFiltering = true;
            this.TreeList.OptionsFilter.FilterMode = global::DevExpress.XtraTreeList.FilterMode.Smart;
            this.TreeList.OptionsView.ShowColumns = false;
            this.TreeList.OptionsView.ShowHorzLines = false;
            this.TreeList.OptionsView.ShowIndicator = false;
            this.TreeList.OptionsView.ShowVertLines = false;
            this.TreeList.Size = new System.Drawing.Size(179, 177);
            this.TreeList.TabIndex = 1;
            this.TreeList.ToolTipController = this.toolTipController1;
            this.TreeList.PopupMenuShowing += new global::DevExpress.XtraTreeList.PopupMenuShowingEventHandler(this.treeView1_PopupMenuShowing);
            this.TreeList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeView1_KeyDown);
            // 
            // toolTipController1
            // 
            this.toolTipController1.GetActiveObjectInfo += new global::DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowItemAnimatedHighlighting = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new global::DevExpress.XtraBars.BarItem[] {
            this.barButtonItemAutoFilter});
            this.barManager1.MaxItemId = 1;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(179, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 203);
            this.barDockControlBottom.Size = new System.Drawing.Size(179, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 203);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(179, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 203);
            // 
            // barButtonItemAutoFilter
            // 
            this.barButtonItemAutoFilter.Caption = "Show auto filter";
            this.barButtonItemAutoFilter.Id = 0;
            this.barButtonItemAutoFilter.Name = "barButtonItemAutoFilter";
            this.barButtonItemAutoFilter.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAutoFilter_ItemClick);
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new global::DevExpress.XtraBars.LinkPersistInfo[] {
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAutoFilter)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // ViewTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.subpane);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DoubleBuffered = true;
            this.Name = "ViewTree";
            this.Size = new System.Drawing.Size(179, 203);
            this.subpane.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        #endregion

        public Subpane subpane;
        internal TreeList TreeList;
        private global::DevExpress.XtraBars.BarManager barManager1;
        private global::DevExpress.XtraBars.BarDockControl barDockControlTop;
        private global::DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private global::DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private global::DevExpress.XtraBars.BarDockControl barDockControlRight;
        private global::DevExpress.XtraBars.PopupMenu popupMenu1;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemAutoFilter;
        private global::DevExpress.Utils.ToolTipController toolTipController1;

    }
}
