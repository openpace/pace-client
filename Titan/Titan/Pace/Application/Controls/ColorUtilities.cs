#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    /// <summary>
    /// Utility to test for colors.
    /// </summary>
    internal partial class ColorUtilities : UserControl
    {
        private readonly char[] hexDigits = {
         '0', '1', '2', '3', '4', '5', '6', '7',
         '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

        private readonly Dictionary<string, string> _colorToHexStringHash;
        private readonly Dictionary<string, Color> _hexStringToColor;
        private readonly Dictionary<string, int> _win32Color;
        private readonly Dictionary<int, string> _hexStrFromWin32Color;

        /// <summary>
        /// 
        /// </summary>
        public ColorUtilities()
        {
            _colorToHexStringHash = new Dictionary<string, string>();
            _hexStringToColor = new Dictionary<string, Color>();
            _win32Color = new Dictionary<string, int>();
            _hexStrFromWin32Color = new Dictionary<int, string>();
            InitializeComponent();
        }

        /// <summary>
        /// Gets the win32 int structure from an hex color string.
        /// </summary>
        /// <param name="hexColorString">An hex color string.</param>
        /// <returns>a color in a win32 int structure.</returns>
        public int GetWin32Color(string hexColorString)
        {
            try
            {

                int val;
                if (_win32Color.TryGetValue(hexColorString, out val))
                {
                    return val;
                }

                int i = ColorTranslator.ToWin32(HexStringToColor(hexColorString));


                _win32Color.Add(hexColorString, i);

                return i;
            }
            catch(Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// Gets a hex color string from a win32 color.
        /// </summary>
        /// <param name="win32Color">Win32 color to convert.</param>
        /// <returns>a hex string.</returns>
        public string GetHexStrFromWin32Color(int win32Color)
        {
            string val = String.Empty;
            if (_hexStrFromWin32Color.TryGetValue(win32Color, out val))
            {
                return val;
            }

            Color c = ColorTranslator.FromWin32(win32Color);

            string color = ColorToHexString(c);

            _hexStrFromWin32Color.Add(win32Color, color);

            return color;
        }

        

        /// <summary>
        /// Converts a color structure to a hex string.
        /// </summary>
        /// <param name="color">Color structure</param>
        /// <returns>Hex color string.</returns>
        public string ColorToHexString(Color color)
        {
            string val = null;
            if(_colorToHexStringHash.TryGetValue(color.Name, out val))
            {
                return val;
            }

            byte[] bytes = new byte[3];
            bytes[0] = color.R;
            bytes[1] = color.G;
            bytes[2] = color.B;
            char[] chars = new char[bytes.Length * 2];
            for (int i = 0; i < bytes.Length; i++)
            {
                int b = bytes[i];
                chars[i * 2] = hexDigits[b >> 4];
                chars[i * 2 + 1] = hexDigits[b & 0xF];
            }

            string hex = new string(chars);

            _colorToHexStringHash.Add(color.Name, hex);

            return hex;
        }

        /// <summary>
        /// Gets a color structure from an hex color string.
        /// </summary>
        /// <param name="hexColor">An hex color string.</param>
        /// <returns>A color structure.</returns>
        public Color HexStringToColor(string hexColor)
        {
            Color val;
            if (_hexStringToColor.TryGetValue(hexColor, out val))
            {
                return val;
            }


            string hc = ExtractHexDigits(hexColor);
            if (hc.Length != 6)
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("hexColor is not exactly 6 digits.");
                return Color.Empty;
            }
            string r = hc.Substring(0, 2);
            string g = hc.Substring(2, 2);
            string b = hc.Substring(4, 2);
            Color color;
            try
            {
                int ri
                   = Int32.Parse(r, System.Globalization.NumberStyles.HexNumber);
                int gi
                   = Int32.Parse(g, System.Globalization.NumberStyles.HexNumber);
                int bi
                   = Int32.Parse(b, System.Globalization.NumberStyles.HexNumber);
                color = Color.FromArgb(ri, gi, bi);
            }
            catch
            {
                // you can choose whether to throw an exception
                //throw new ArgumentException("Conversion failed.");
                return Color.Empty;
            }

            _hexStringToColor.Add(hexColor, color);

            return color;
        }
        /// <summary>
        /// Extract only the hex digits from a string.
        /// </summary>
        private static string ExtractHexDigits(IEnumerable<char> input)
        {
            // remove any characters that are not digits (like #)
            Regex isHexDigit
               = new Regex("[abcdefABCDEF\\d]+", RegexOptions.Compiled);
            string newnum = "";
            foreach (char c in input)
            {
                if (isHexDigit.IsMatch(c.ToString()))
                    newnum += c.ToString();
            }
            return newnum;
        }


        /// <summary>
        /// Validates that a color can be set to the backcolor of a textbox.
        /// </summary>
        /// <param name="c">Color to be set.</param>
        /// <returns>true if the color can be set, false if the color cannot be set.</returns>
        public bool ValidateBackColor(Color c)
        {
            try
            {
                txtColor.BackColor = c;
                return true;
            }
            catch(Exception)
            {
                return false;
            }
        }
    }
}
