#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    internal partial class ViewOptions : UserControl
    {
        #region Variables

        /// <summary>
        /// Custom event for the subpane before expand.  Occurs before the subpane is expanded.
        /// </summary>
        public event Subpane_BeforeExpand SubpaneBeforeExpand;

        /// <summary>
        /// Custom event for the subpane after expand.  Occurs after the subpane has been expanded.
        /// </summary>
        public event Subpane_AfterExpand SubpaneAfterExpand;

        /// <summary>
        /// Custom event for when the row check box state changes.
        /// </summary>
        public event Chk_RowCheckChange ChkBoxRowCheckChange;

        /// <summary>
        /// Represents the method that will handle the Chk_RowCheckChange event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="state"></param>
        public delegate void Chk_RowCheckChange(object sender, EventArgs e, bool state);

        /// <summary>
        /// Custom event for when the column check box state changes.
        /// </summary>
        public event Chk_ColumnCheckChange ChkBoxColumnCheckChange;

        /// <summary>
        /// Represents the method that will handle the Chk_ColumnCheckChange event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="state"></param>
        public delegate void Chk_ColumnCheckChange(object sender, EventArgs e, bool state);

        /// <summary>
        /// Represents the method that will handle the SubpaneBeforeExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_BeforeExpand(object sender, CancelEventArgs e, string controlName);

        /// <summary>
        /// Represents the method that will handle the SubpaneAfterExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_AfterExpand(object sender, EventArgs e, string controlName);

        private bool _AllowUpdates = false;

        #endregion Variables

        #region Properties
        /// <summary>
        /// Gets/sets the visible property of the control.
        /// </summary>
        public bool ControlIsVisible
        {
            get { return this.subpane.Visible; }
            set { this.subpane.Visible = value;  }
        }
        #endregion Properties

        #region Constructor & Control Load

        /// <summary>
        /// Constructor.
        /// </summary>
        public ViewOptions()
        {
            InitializeComponent();

            this.subpane.AfterCollapse += new EventHandler(subpane1_AfterCollapse);
            this.subpane.AfterExpand += new EventHandler(subpane1_AfterExpand);
            this.chkColumn.CheckedChanged += new EventHandler(chkColumn_CheckedChanged);
            this.chkRows.CheckedChanged += new EventHandler(chkRows_CheckedChanged);
        }

        
        #endregion Constructor & Control Load
 
        #region Events


        /// <summary>
        /// Event handler for the tree view before expanding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_BeforeExpanding(object sender, CancelEventArgs e)
        {
            if(this.SubpaneBeforeExpand != null)
                this.SubpaneBeforeExpand(sender, e, this.Name);
        }

        /// <summary>
        /// Event handler for the tree view after expand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_AfterExpand(object sender, EventArgs e)
        {
            if (this.SubpaneAfterExpand != null)
                this.SubpaneAfterExpand(sender, e, this.Name);
        }

        #endregion Events

        #region Subpane Code
        #region Fields
        private event EventHandler afterExpandEvent;
        private event EventHandler afterCollapseEvent;
        #endregion        

        #region Properties
        public bool IsExpanded
        {
            get
            {
                return this.subpane.IsExpanded;
            }
        }
        #endregion

        #region Public Methods
        public void Expand()
        {
            this.subpane.Expand();
        }

        public void Collapse()
        {
            this.subpane.Collapse();
        }

        public void BeginUpdate()
        {
            _AllowUpdates = false;
        }

        public void EndUpdate()
        {
            _AllowUpdates = true;
        }

        #endregion

        #region Events

        public event EventHandler AfterExpand
        {
            add
            {
                this.afterExpandEvent += value;
            }
            remove
            {
                this.afterExpandEvent -= value;
            }
        }

        public event EventHandler AfterCollapse
        {
            add
            {
                this.afterCollapseEvent += value;
            }
            remove
            {
                this.afterCollapseEvent -= value;
            }
        }

        #endregion

        #region Event Firing Methods
        private void AfterExpandEvent()
        {
            if (this.afterExpandEvent != null)
                this.afterExpandEvent(this, EventArgs.Empty);
        }

        private void AfterCollapseEvent()
        {
            if (this.afterCollapseEvent != null)
                this.afterCollapseEvent(this, EventArgs.Empty);
        }
        #endregion

        #region Event Handlers

        private void subpane1_AfterCollapse(object sender, EventArgs e)
        {
            this.AfterCollapseEvent();
        }

        private void subpane1_AfterExpand(object sender, EventArgs e)
        {
            this.AfterExpandEvent();
        }

        private void chkRows_CheckedChanged(object sender, EventArgs e)
        {
            if (_AllowUpdates)
            {
                if (ChkBoxRowCheckChange != null)
                {
                    ChkBoxRowCheckChange(sender, e, chkRows.Checked);
                }
            }
        }

        private void chkColumn_CheckedChanged(object sender, EventArgs e)
        {
            if (_AllowUpdates)
            {
                if (ChkBoxColumnCheckChange != null)
                {
                    ChkBoxColumnCheckChange(sender, e, chkColumn.Checked);
                }
            }
        }

        #endregion Event Handlers

        #endregion Subpane Code
    }
}