﻿//***************************************************************************
//
//         Copyright (c) Microsoft Corporation. All rights reserved.
//
//    This code sample is provided "AS IS" without warranty of any kind.
//
//***************************************************************************
namespace Titan.Pace.Application.Controls
{
    partial class Subpane
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            // 
            // Subpane
            // 
            this.Name = "Subpane";

        }

        #endregion
    }
}
