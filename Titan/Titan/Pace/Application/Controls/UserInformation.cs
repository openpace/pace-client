#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    internal partial class UserInformation : UserControl
    {
        #region Variables

        /// <summary>
        /// Custom event for the tree view before expand.  Occurs before the view tree is expanded.
        /// </summary>
#pragma warning disable 67
        public event ViewTree_TreeViewCancelEvent BeforeExpand;


        /// <summary>
        /// Custom event for the tree view before collapse.  Occurs before the view tree is collapsed.
        /// </summary>
        public event ViewTree_TreeViewBeforeCollapseEvent BeforeCollapse;
        
        /// <summary>
        /// Custom event for the tree view before select.  Occurs before the view tree is selected.
        /// </summary>
        public event ViewTree_TreeViewBeforeSelectEvent BeforeSelect;

        /// <summary>
        /// Custom event for the tree view double click.  Occurs when the view tree is double clicked.
        /// </summary>
        public event ViewTree_TreeViewOnDoubleClick DblClick;

        /// <summary>
        /// Custom event for the subpane before expand.  Occurs before the subpane is expanded.
        /// </summary>
        public event Subpane_BeforeExpand SubpaneBeforeExpand;

        /// <summary>
        /// Custom event for the subpane after expand.  Occurs after the subpane has been expanded.
        /// </summary>
        public event Subpane_AfterExpand SubpaneAfterExpand;
#pragma warning restore 67

        /// <summary>
        /// Represents the method that will handle the tree view BeforeExpand event. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">Name of the control.</param>
        public delegate void ViewTree_TreeViewCancelEvent(object sender, TreeViewCancelEventArgs e, string controlName);

        /// <summary>
        /// Represents the method that will handle the tree view BeforeCollapse event. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">Name of the control.</param>
        public delegate void ViewTree_TreeViewBeforeCollapseEvent(object sender, TreeViewCancelEventArgs e, string controlName);
        
        /// <summary>
        /// Represents the method that will handle the tree view BeforeCollapse event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="currNode">The currently selected Node, null if one is not selected.</param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void ViewTree_TreeViewBeforeSelectEvent(object sender, TreeNode currNode, TreeViewCancelEventArgs e, string controlName);

        /// <summary>
        /// Represents the method that will handle the tree view DblClick event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="currNode">The currently selected Node, null if one is not selected.</param>
        public delegate void ViewTree_TreeViewOnDoubleClick(object sender, EventArgs e, TreeNode currNode);

        /// <summary>
        /// Represents the method that will handle the SubpaneBeforeExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_BeforeExpand(object sender, CancelEventArgs e, string controlName);

        /// <summary>
        /// Represents the method that will handle the SubpaneAfterExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_AfterExpand(object sender, EventArgs e, string controlName);
        #endregion Variables

        #region Properties
        /// <summary>
        /// Gets/sets the visible property of the control.
        /// </summary>
        public bool ControlIsVisible
        {
            get { return this.subpane.Visible; }
            set { this.subpane.Visible = value;  }
        }
        #endregion Properties

        #region Constructor & Control Load

        /// <summary>
        /// Constructor.
        /// </summary>
        public UserInformation()
        {
            InitializeComponent();

            this.subpane.AfterCollapse += new EventHandler(subpane1_AfterCollapse);
            this.subpane.AfterExpand += new EventHandler(subpane1_AfterExpand);

            Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();

            lblPlanTypeLabel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.ActionsPane.Header.RoleLabel");
            lblSeasonCaptionLabel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.ActionsPane.Header.SeasonLabel");

            subpane.DefaultExpandedHeight = Height;
        }
        #endregion Constructor & Control Load
 
        #region Events


        /// <summary>
        /// Event handler for the tree view before expanding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_BeforeExpanding(object sender, CancelEventArgs e)
        {
            if(this.SubpaneBeforeExpand != null)
                this.SubpaneBeforeExpand(sender, e, this.Name);
        }

        /// <summary>
        /// Event handler for the tree view after expand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_AfterExpand(object sender, EventArgs e)
        {
            if (this.SubpaneAfterExpand != null)
                this.SubpaneAfterExpand(sender, e, this.Name);
        }

        /// <summary>
        /// Event handler for the tree view before collapse event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treeView1_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
        {
            if (this.BeforeCollapse != null)
                this.BeforeCollapse(sender, e, this.Name);
        }


        #endregion Events

        #region Subpane Code
        #region Fields
        private event EventHandler afterExpandEvent;
        private event EventHandler afterCollapseEvent;
        #endregion        

        #region Properties
        public bool IsExpanded
        {
            get
            {
                return this.subpane.IsExpanded;
            }
        }

        /// <summary>
        /// Get/set the user id caption.
        /// </summary>
        public string UserId
        {
            get { return lblUserId.Text; }
            set { lblUserId.Text = value; }
        }

        /// <summary>
        /// Get/set the season/process caption.
        /// </summary>
        public string SeasonCaption
        {
            get { return lblSeasonCaption.Text; }
            set { lblSeasonCaption.Text = value; }
        }

        /// <summary>
        /// Get/set the plantype caption.
        /// </summary>
        public string PlanType
        {
            get { return lblPlanType.Text; }
            set { lblPlanType.Text = value; }
        }

        #endregion

        #region Public Methods
        public void Expand()
        {
            this.subpane.Expand();
        }

        public void Collapse()
        {
            this.subpane.Collapse();
        }
        #endregion

        #region Events

        public event EventHandler AfterExpand
        {
            add
            {
                this.afterExpandEvent += value;
            }
            remove
            {
                this.afterExpandEvent -= value;
            }
        }

        public event EventHandler AfterCollapse
        {
            add
            {
                this.afterCollapseEvent += value;
            }
            remove
            {
                this.afterCollapseEvent -= value;
            }
        }

        #endregion

        #region Event Firing Methods
        private void AfterExpandEvent()
        {
            if (this.afterExpandEvent != null)
                this.afterExpandEvent(this, EventArgs.Empty);
        }

        private void AfterCollapseEvent()
        {
            if (this.afterCollapseEvent != null)
                this.afterCollapseEvent(this, EventArgs.Empty);
        }
        #endregion

        #region Event Handlers

        void subpane1_AfterCollapse(object sender, EventArgs e)
        {
            this.AfterCollapseEvent();
        }

        void subpane1_AfterExpand(object sender, EventArgs e)
        {
            this.AfterExpandEvent();
        }
        #endregion

 

        #endregion Subpane Code
    }
}