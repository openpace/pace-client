namespace Titan.Pace.Application.Controls
{
    partial class RoleFilter
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RoleFilter));
            this.pnlTabs = new System.Windows.Forms.Panel();
            this.tabRoleFilter = new System.Windows.Forms.TabControl();
            this.findToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.toolStripDropDownButton = new System.Windows.Forms.ToolStripDropDownButton();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.pnlRoleInformation = new System.Windows.Forms.Panel();
            this.searchControl = new SearchControl();
            this.pnlOptions = new System.Windows.Forms.Panel();
            this.chkFilteredTotals = new System.Windows.Forms.CheckBox();
            this.chkSupInvIx = new System.Windows.Forms.CheckBox();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.cmdResetSel = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOk = new System.Windows.Forms.Button();
            this.popupMenu = new global::DevExpress.XtraBars.PopupMenu(this.components);
            this.barButtonItemFind = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemClear = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemClearAll = new global::DevExpress.XtraBars.BarButtonItem();
            this.barCheckItemSelections = new global::DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemRememberAll = new global::DevExpress.XtraBars.BarButtonItem();
            this.barManager1 = new global::DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new global::DevExpress.XtraBars.BarDockControl();
            this.barCheckItem1 = new global::DevExpress.XtraBars.BarCheckItem();
            this.pnlTabs.SuspendLayout();
            this.statusStrip.SuspendLayout();
            this.pnlRoleInformation.SuspendLayout();
            this.pnlOptions.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            this.SuspendLayout();
            // 
            // pnlTabs
            // 
            this.pnlTabs.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlTabs.Controls.Add(this.tabRoleFilter);
            this.pnlTabs.Location = new System.Drawing.Point(2, 38);
            this.pnlTabs.Name = "pnlTabs";
            this.pnlTabs.Size = new System.Drawing.Size(353, 382);
            this.pnlTabs.TabIndex = 0;
            // 
            // tabRoleFilter
            // 
            this.tabRoleFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabRoleFilter.Location = new System.Drawing.Point(0, 0);
            this.tabRoleFilter.Multiline = true;
            this.tabRoleFilter.Name = "tabRoleFilter";
            this.tabRoleFilter.SelectedIndex = 0;
            this.tabRoleFilter.ShowToolTips = true;
            this.tabRoleFilter.Size = new System.Drawing.Size(353, 382);
            this.tabRoleFilter.TabIndex = 0;
            this.tabRoleFilter.SelectedIndexChanged += new System.EventHandler(this.tabRoleFilter_SelectedIndexChanged);
            this.tabRoleFilter.MouseClick += new System.Windows.Forms.MouseEventHandler(this.tabRoleFilter_MouseClick);
            // 
            // findToolStripMenuItem
            // 
            this.findToolStripMenuItem.Name = "findToolStripMenuItem";
            this.findToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F)));
            this.findToolStripMenuItem.Size = new System.Drawing.Size(127, 22);
            this.findToolStripMenuItem.Tag = "Find";
            this.findToolStripMenuItem.Click += new System.EventHandler(this.findToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(155, 22);
            this.toolStripMenuItem2.Text = "toolStripMenuItem2";
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "find.png");
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripDropDownButton,
            this.toolStripStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 506);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(358, 22);
            this.statusStrip.TabIndex = 27;
            this.statusStrip.Text = "statusStrip1";
            // 
            // toolStripDropDownButton
            // 
            this.toolStripDropDownButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripDropDownButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripDropDownButton.Image")));
            this.toolStripDropDownButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripDropDownButton.Name = "toolStripDropDownButton";
            this.toolStripDropDownButton.Size = new System.Drawing.Size(77, 20);
            this.toolStripDropDownButton.Text = "Alias Table";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // pnlRoleInformation
            // 
            this.pnlRoleInformation.AutoSize = true;
            this.pnlRoleInformation.Controls.Add(this.searchControl);
            this.pnlRoleInformation.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlRoleInformation.Location = new System.Drawing.Point(0, 0);
            this.pnlRoleInformation.Name = "pnlRoleInformation";
            this.pnlRoleInformation.Size = new System.Drawing.Size(358, 34);
            this.pnlRoleInformation.TabIndex = 1;
            // 
            // searchControl
            // 
            this.searchControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.searchControl.AutoSize = true;
            this.searchControl.ButtonText = "";
            this.searchControl.Location = new System.Drawing.Point(4, 3);
            this.searchControl.Margin = new System.Windows.Forms.Padding(4);
            this.searchControl.Name = "searchControl";
            this.searchControl.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.searchControl.SearchText = "";
            this.searchControl.Size = new System.Drawing.Size(351, 28);
            this.searchControl.TabIndex = 22;
            // 
            // pnlOptions
            // 
            this.pnlOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlOptions.Controls.Add(this.chkFilteredTotals);
            this.pnlOptions.Controls.Add(this.chkSupInvIx);
            this.pnlOptions.Location = new System.Drawing.Point(0, 425);
            this.pnlOptions.Name = "pnlOptions";
            this.pnlOptions.Size = new System.Drawing.Size(358, 46);
            this.pnlOptions.TabIndex = 29;
            // 
            // chkFilteredTotals
            // 
            this.chkFilteredTotals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFilteredTotals.AutoEllipsis = true;
            this.chkFilteredTotals.Location = new System.Drawing.Point(3, 2);
            this.chkFilteredTotals.Name = "chkFilteredTotals";
            this.chkFilteredTotals.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.chkFilteredTotals.Size = new System.Drawing.Size(347, 22);
            this.chkFilteredTotals.TabIndex = 30;
            this.chkFilteredTotals.UseVisualStyleBackColor = true;
            // 
            // chkSupInvIx
            // 
            this.chkSupInvIx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSupInvIx.AutoEllipsis = true;
            this.chkSupInvIx.Location = new System.Drawing.Point(3, 21);
            this.chkSupInvIx.Name = "chkSupInvIx";
            this.chkSupInvIx.Padding = new System.Windows.Forms.Padding(0, 6, 0, 0);
            this.chkSupInvIx.Size = new System.Drawing.Size(347, 22);
            this.chkSupInvIx.TabIndex = 29;
            this.chkSupInvIx.UseVisualStyleBackColor = true;
            this.chkSupInvIx.CheckedChanged += new System.EventHandler(this.chkSupInvIx_CheckedChanged);
            // 
            // pnlButtons
            // 
            this.pnlButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlButtons.Controls.Add(this.cmdResetSel);
            this.pnlButtons.Controls.Add(this.cmdCancel);
            this.pnlButtons.Controls.Add(this.cmdOk);
            this.pnlButtons.Location = new System.Drawing.Point(0, 472);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(358, 31);
            this.pnlButtons.TabIndex = 30;
            // 
            // cmdResetSel
            // 
            this.cmdResetSel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdResetSel.AutoSize = true;
            this.cmdResetSel.Location = new System.Drawing.Point(0, 5);
            this.cmdResetSel.Name = "cmdResetSel";
            this.cmdResetSel.Size = new System.Drawing.Size(75, 23);
            this.cmdResetSel.TabIndex = 36;
            this.cmdResetSel.UseVisualStyleBackColor = true;
            this.cmdResetSel.Click += new System.EventHandler(this.cmdResetSel_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(282, 5);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 35;
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Enabled = false;
            this.cmdOk.Location = new System.Drawing.Point(201, 5);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 34;
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new global::DevExpress.XtraBars.LinkPersistInfo[] {
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemFind),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemClear, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemClearAll),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemSelections, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemRememberAll)});
            this.popupMenu.Manager = this.barManager1;
            this.popupMenu.Name = "popupMenu";
            this.popupMenu.BeforePopup += new System.ComponentModel.CancelEventHandler(this.popupMenu_BeforePopup);
            // 
            // barButtonItemFind
            // 
            this.barButtonItemFind.Caption = "Find...";
            this.barButtonItemFind.Id = 0;
            this.barButtonItemFind.ItemShortcut = new global::DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.barButtonItemFind.Name = "barButtonItemFind";
            this.barButtonItemFind.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemFind_ItemClick);
            // 
            // barButtonItemClear
            // 
            this.barButtonItemClear.Caption = "_";
            this.barButtonItemClear.Id = 2;
            this.barButtonItemClear.Name = "barButtonItemClear";
            this.barButtonItemClear.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemClear_ItemClick);
            // 
            // barButtonItemClearAll
            // 
            this.barButtonItemClearAll.Caption = "_";
            this.barButtonItemClearAll.Id = 3;
            this.barButtonItemClearAll.Name = "barButtonItemClearAll";
            this.barButtonItemClearAll.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemClearAll_ItemClick);
            // 
            // barCheckItemSelections
            // 
            this.barCheckItemSelections.Caption = "_";
            this.barCheckItemSelections.Id = 4;
            this.barCheckItemSelections.Name = "barCheckItemSelections";
            this.barCheckItemSelections.CheckedChanged += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemSelections_CheckedChanged);
            // 
            // barCheckItemRememberAll
            // 
            this.barCheckItemRememberAll.Caption = "_";
            this.barCheckItemRememberAll.Id = 5;
            this.barCheckItemRememberAll.Name = "barCheckItemRememberAll";
            this.barCheckItemRememberAll.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemRememberAll_ItemClick);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new global::DevExpress.XtraBars.BarItem[] {
            this.barButtonItemFind,
            this.barCheckItem1,
            this.barButtonItemClear,
            this.barButtonItemClearAll,
            this.barCheckItemSelections,
            this.barCheckItemRememberAll});
            this.barManager1.MaxItemId = 6;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(358, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 528);
            this.barDockControlBottom.Size = new System.Drawing.Size(358, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 528);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(358, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 528);
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "barCheckItem1";
            this.barCheckItem1.Id = 1;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // RoleFilter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.pnlTabs);
            this.Controls.Add(this.pnlRoleInformation);
            this.Controls.Add(this.pnlOptions);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DoubleBuffered = true;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "RoleFilter";
            this.Size = new System.Drawing.Size(358, 528);
            this.Resize += new System.EventHandler(this.RoleFilter_Resize);
            this.pnlTabs.ResumeLayout(false);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.pnlRoleInformation.ResumeLayout(false);
            this.pnlRoleInformation.PerformLayout();
            this.pnlOptions.ResumeLayout(false);
            this.pnlButtons.ResumeLayout(false);
            this.pnlButtons.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel pnlTabs;
        private System.Windows.Forms.TabControl tabRoleFilter;
        //private System.Windows.Forms.ComboBox cboDisplay;
        private System.Windows.Forms.ToolStripMenuItem findToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.Panel pnlRoleInformation;
        private SearchControl searchControl;
        private System.Windows.Forms.ToolStripDropDownButton toolStripDropDownButton;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
        private System.Windows.Forms.Panel pnlOptions;
        private System.Windows.Forms.CheckBox chkFilteredTotals;
        private System.Windows.Forms.CheckBox chkSupInvIx;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button cmdResetSel;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdOk;
        private global::DevExpress.XtraBars.PopupMenu popupMenu;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemFind;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemClear;
        private global::DevExpress.XtraBars.BarManager barManager1;
        private global::DevExpress.XtraBars.BarDockControl barDockControlTop;
        private global::DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private global::DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private global::DevExpress.XtraBars.BarDockControl barDockControlRight;
        private global::DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemClearAll;
        private global::DevExpress.XtraBars.BarCheckItem barCheckItemSelections;
        private global::DevExpress.XtraBars.BarButtonItem barCheckItemRememberAll;
    }
}
