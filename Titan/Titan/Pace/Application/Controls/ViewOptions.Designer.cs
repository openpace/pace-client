namespace Titan.Pace.Application.Controls
{
    internal partial class ViewOptions
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private readonly System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.subpane = new Subpane();
            this.grpSuppressZero = new System.Windows.Forms.GroupBox();
            this.chkColumn = new System.Windows.Forms.CheckBox();
            this.chkRows = new System.Windows.Forms.CheckBox();
            this.subpane.SuspendLayout();
            this.grpSuppressZero.SuspendLayout();
            this.SuspendLayout();
            // 
            // subpane
            // 
            this.subpane.AutoSize = true;
            this.subpane.BackColor = System.Drawing.Color.Transparent;
            this.subpane.Controls.Add(this.grpSuppressZero);
            this.subpane.DefaultExpandedHeight = 200;
            this.subpane.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.subpane.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subpane.GradientColorBegin = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.GradientColorEnd = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.HighlightColor = System.Drawing.SystemColors.ControlText;
            this.subpane.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.subpane.IsAnimated = false;
            this.subpane.Location = new System.Drawing.Point(0, 0);
            this.subpane.Name = "subpane";
            this.subpane.Padding = new System.Windows.Forms.Padding(0);
            this.subpane.PanelColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.subpane.Size = new System.Drawing.Size(128, 107);
            this.subpane.TabIndex = 17;
            this.subpane.TabStop = true;
            this.subpane.Tag = "";
            this.subpane.Text = "_";
            this.subpane.BeforeExpanding += new System.ComponentModel.CancelEventHandler(this.subpane_BeforeExpanding);
            this.subpane.AfterExpand += new System.EventHandler(this.subpane_AfterExpand);
            // 
            // grpSuppressZero
            // 
            this.grpSuppressZero.BackColor = System.Drawing.Color.Transparent;
            this.grpSuppressZero.Controls.Add(this.chkColumn);
            this.grpSuppressZero.Controls.Add(this.chkRows);
            this.grpSuppressZero.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpSuppressZero.Location = new System.Drawing.Point(0, 21);
            this.grpSuppressZero.Name = "grpSuppressZero";
            this.grpSuppressZero.Size = new System.Drawing.Size(128, 86);
            this.grpSuppressZero.TabIndex = 0;
            this.grpSuppressZero.TabStop = false;
            this.grpSuppressZero.Text = "_";
            // 
            // chkColumn
            // 
            this.chkColumn.AutoSize = true;
            this.chkColumn.Location = new System.Drawing.Point(6, 44);
            this.chkColumn.Name = "chkColumn";
            this.chkColumn.Size = new System.Drawing.Size(33, 17);
            this.chkColumn.TabIndex = 1;
            this.chkColumn.Text = "_";
            this.chkColumn.UseVisualStyleBackColor = true;
            this.chkColumn.CheckedChanged += new System.EventHandler(this.chkColumn_CheckedChanged);
            // 
            // chkRows
            // 
            this.chkRows.AutoSize = true;
            this.chkRows.Location = new System.Drawing.Point(6, 21);
            this.chkRows.Name = "chkRows";
            this.chkRows.Size = new System.Drawing.Size(33, 17);
            this.chkRows.TabIndex = 0;
            this.chkRows.Text = "_";
            this.chkRows.UseVisualStyleBackColor = true;
            this.chkRows.CheckedChanged += new System.EventHandler(this.chkRows_CheckedChanged);
            // 
            // ViewOptions
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.subpane);
            this.Name = "ViewOptions";
            this.Size = new System.Drawing.Size(128, 107);
            this.subpane.ResumeLayout(false);
            this.grpSuppressZero.ResumeLayout(false);
            this.grpSuppressZero.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Subpane subpane;
        public System.Windows.Forms.CheckBox chkColumn;
        public System.Windows.Forms.CheckBox chkRows;
        public System.Windows.Forms.GroupBox grpSuppressZero;
    }
}
