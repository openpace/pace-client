﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    internal partial class SearchControl : UserControl
    {
        public event SearchButtonClicked SearchInvoked;
        public delegate void SearchButtonClicked(object sender, SearchEventArgs e);

        public event SearchTextChanged SearchTextModified;
        public delegate void SearchTextChanged(object sender, SearchEventArgs e);

        private string _nullText;

        public string SearchText
        {
            get
            {
                return txtFindMember.Text; 
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    txtFindMember.Text = value;
                }
            }
        }

        public string ButtonText
        {
            get
            {
                return cmdFind.Text;
            }
            set
            {
                if (!String.IsNullOrEmpty(value))
                {
                    cmdFind.Text = value;
                }
            }
        }

        public SearchControl()
        {
            _nullText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Search.PromptText");
            InitializeComponent();
            cmdFind.Left = txtFindMember.Right;
            cmdFind.Top = txtFindMember.Top;
            txtFindMember.Properties.NullValuePrompt = _nullText;
        }

        private void cmdFind_Resize(object sender, EventArgs e)
        {
            cmdFind.Left = txtFindMember.Right;
            cmdFind.Top = txtFindMember.Top;
        }

        private void cmdFind_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                cmdFind.PerformClick();
            }
            else
            {
                SearchTextModified(this, new SearchEventArgs(txtFindMember.Text));
                //toolStripStatusLabel1.Text = String.Empty;
                //cmdFind.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Find");
            }
        }

        private void cmdFind_Click(object sender, EventArgs e)
        {
            Search();
        }

        private void Search()
        {
            SearchInvoked(this, new SearchEventArgs(txtFindMember.Text));
        }

    }
    #region SearchEventArgs
    /// <summary>
    /// Custom event argument for when the user changes roles.
    /// </summary>
    internal class SearchEventArgs : EventArgs
    {

        public SearchEventArgs(string searchText)
        {
            SearchText = searchText;
        }

        public string SearchText;
    }
    #endregion SearchEventArgs
}
