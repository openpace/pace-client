﻿namespace Titan.Pace.Application.Controls
{
    internal partial class PopupEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.popupContainerControl1 = new global::DevExpress.XtraEditors.PopupContainerControl();
            this.aliasTable = new global::DevExpress.XtraEditors.ComboBoxEdit();
            this.barManager1 = new global::DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new global::DevExpress.XtraBars.BarDockControl();
            this.treeList1 = new global::DevExpress.XtraTreeList.TreeList();
            this.barButtonUncheckAll = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonCheckAll = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonCheckAllChildren = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonCheckAllDesc = new global::DevExpress.XtraBars.BarButtonItem();
            this.panelControl1 = new global::DevExpress.XtraEditors.PanelControl();
            this.cmdCancel = new global::DevExpress.XtraEditors.SimpleButton();
            this.cmdOk = new global::DevExpress.XtraEditors.SimpleButton();
            this.txtSearch = new global::DevExpress.XtraEditors.TextEdit();
            this.txtMemberSearch = new global::DevExpress.XtraEditors.TextEdit();
            this.popupMenu1 = new global::DevExpress.XtraBars.PopupMenu(this.components);
            this.bar1 = new global::DevExpress.XtraBars.Bar();
            this.bar2 = new global::DevExpress.XtraBars.Bar();
            this.bar3 = new global::DevExpress.XtraBars.Bar();
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).BeginInit();
            this.popupContainerControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.aliasTable.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).BeginInit();
            this.treeList1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberSearch.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // popupContainerControl1
            // 
            this.popupContainerControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.popupContainerControl1.Controls.Add(this.aliasTable);
            this.popupContainerControl1.Controls.Add(this.panelControl1);
            this.popupContainerControl1.Controls.Add(this.txtSearch);
            this.popupContainerControl1.Controls.Add(this.txtMemberSearch);
            this.popupContainerControl1.Controls.Add(this.treeList1);
            this.popupContainerControl1.Location = new System.Drawing.Point(0, 0);
            this.popupContainerControl1.Name = "popupContainerControl1";
            this.popupContainerControl1.Size = new System.Drawing.Size(609, 347);
            this.popupContainerControl1.TabIndex = 8;
            // 
            // aliasTable
            // 
            this.aliasTable.EditValue = "Column to search";
            this.aliasTable.Location = new System.Drawing.Point(498, 299);
            this.aliasTable.MenuManager = this.barManager1;
            this.aliasTable.Name = "aliasTable";
            this.aliasTable.Properties.Buttons.AddRange(new global::DevExpress.XtraEditors.Controls.EditorButton[] {
            new global::DevExpress.XtraEditors.Controls.EditorButton(global::DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.aliasTable.Properties.Items.AddRange(new object[] {
            "test",
            "test3"});
            this.aliasTable.Properties.TextEditStyle = global::DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.aliasTable.Size = new System.Drawing.Size(105, 20);
            this.aliasTable.TabIndex = 11;
            this.aliasTable.ToolTip = "_";
            this.aliasTable.SelectedIndexChanged += new System.EventHandler(this.aliasTable_SelectedIndexChanged);
            // 
            // barManager1
            // 
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this.treeList1;
            this.barManager1.Items.AddRange(new global::DevExpress.XtraBars.BarItem[] {
            this.barButtonUncheckAll,
            this.barButtonCheckAll,
            this.barButtonCheckAllChildren,
            this.barButtonCheckAllDesc});
            this.barManager1.MaxItemId = 4;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(609, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 277);
            this.barDockControlBottom.Size = new System.Drawing.Size(609, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 277);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(609, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 277);
            // 
            // treeList1
            // 
            this.treeList1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.treeList1.Controls.Add(this.barDockControlLeft);
            this.treeList1.Controls.Add(this.barDockControlRight);
            this.treeList1.Controls.Add(this.barDockControlBottom);
            this.treeList1.Controls.Add(this.barDockControlTop);
            this.treeList1.Location = new System.Drawing.Point(0, 21);
            this.treeList1.Name = "treeList1";
            this.treeList1.OptionsBehavior.Editable = false;
            this.treeList1.OptionsBehavior.EnableFiltering = true;
            this.treeList1.OptionsFilter.FilterMode = global::DevExpress.XtraTreeList.FilterMode.Smart;
            this.treeList1.OptionsMenu.EnableColumnMenu = false;
            this.treeList1.OptionsMenu.EnableFooterMenu = false;
            this.treeList1.OptionsMenu.ShowAutoFilterRowItem = false;
            this.treeList1.OptionsView.ShowCheckBoxes = true;
            this.treeList1.OptionsView.ShowHorzLines = false;
            this.treeList1.OptionsView.ShowIndicator = false;
            this.treeList1.OptionsView.ShowVertLines = false;
            this.treeList1.Size = new System.Drawing.Size(609, 277);
            this.treeList1.TabIndex = 1;
            this.treeList1.BeforeCheckNode += new global::DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeList1_BeforeCheckNode);
            this.treeList1.PopupMenuShowing += new global::DevExpress.XtraTreeList.PopupMenuShowingEventHandler(this.treeList1_PopupMenuShowing);
            this.treeList1.FilterNode += new global::DevExpress.XtraTreeList.FilterNodeEventHandler(this.treeList1_FilterNode);
            // 
            // barButtonUncheckAll
            // 
            this.barButtonUncheckAll.Caption = "_";
            this.barButtonUncheckAll.Id = 0;
            this.barButtonUncheckAll.Name = "barButtonUncheckAll";
            this.barButtonUncheckAll.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonUncheckAll_ItemClick);
            // 
            // barButtonCheckAll
            // 
            this.barButtonCheckAll.Caption = "_";
            this.barButtonCheckAll.Id = 1;
            this.barButtonCheckAll.Name = "barButtonCheckAll";
            this.barButtonCheckAll.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonCheckAllChildren
            // 
            this.barButtonCheckAllChildren.Caption = "_";
            this.barButtonCheckAllChildren.Id = 2;
            this.barButtonCheckAllChildren.Name = "barButtonCheckAllChildren";
            this.barButtonCheckAllChildren.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barCheckAllChildren_ItemClick);
            // 
            // barButtonCheckAllDesc
            // 
            this.barButtonCheckAllDesc.Caption = "_";
            this.barButtonCheckAllDesc.Id = 3;
            this.barButtonCheckAllDesc.Name = "barButtonCheckAllDesc";
            this.barButtonCheckAllDesc.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // panelControl1
            // 
            this.panelControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panelControl1.Controls.Add(this.cmdCancel);
            this.panelControl1.Controls.Add(this.cmdOk);
            this.panelControl1.Location = new System.Drawing.Point(0, 321);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(609, 26);
            this.panelControl1.TabIndex = 10;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.Location = new System.Drawing.Point(554, 3);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(50, 20);
            this.cmdCancel.TabIndex = 9;
            this.cmdCancel.Text = "_";
            this.cmdCancel.Click += new System.EventHandler(this.cmdCancel_Click);
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.Location = new System.Drawing.Point(498, 3);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(50, 20);
            this.cmdOk.TabIndex = 10;
            this.cmdOk.Text = "_";
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearch.Location = new System.Drawing.Point(0, 299);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(492, 20);
            this.txtSearch.TabIndex = 2;
            this.txtSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtSearch_KeyUp);
            // 
            // txtMemberSearch
            // 
            this.txtMemberSearch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMemberSearch.Location = new System.Drawing.Point(0, 0);
            this.txtMemberSearch.Name = "txtMemberSearch";
            this.txtMemberSearch.Size = new System.Drawing.Size(609, 20);
            this.txtMemberSearch.TabIndex = 0;
            this.txtMemberSearch.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textEdit1_KeyUp);
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new global::DevExpress.XtraBars.LinkPersistInfo[] {
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonCheckAll),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonUncheckAll),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonCheckAllChildren, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonCheckAllDesc)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            // 
            // bar1
            // 
            this.bar1.BarName = "Tools";
            this.bar1.DockCol = 0;
            this.bar1.DockStyle = global::DevExpress.XtraBars.BarDockStyle.Top;
            this.bar1.Text = "Tools";
            // 
            // bar2
            // 
            this.bar2.BarName = "Main menu";
            this.bar2.DockCol = 0;
            this.bar2.DockStyle = global::DevExpress.XtraBars.BarDockStyle.Top;
            this.bar2.OptionsBar.MultiLine = true;
            this.bar2.OptionsBar.UseWholeRow = true;
            this.bar2.Text = "Main menu";
            // 
            // bar3
            // 
            this.bar3.BarName = "Status bar";
            this.bar3.CanDockStyle = global::DevExpress.XtraBars.BarCanDockStyle.Bottom;
            this.bar3.DockCol = 0;
            this.bar3.DockStyle = global::DevExpress.XtraBars.BarDockStyle.Bottom;
            this.bar3.OptionsBar.AllowQuickCustomization = false;
            this.bar3.OptionsBar.DrawDragBorder = false;
            this.bar3.OptionsBar.UseWholeRow = true;
            this.bar3.Text = "Status bar";
            // 
            // PopupEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.popupContainerControl1);
            this.Name = "PopupEditor";
            this.Size = new System.Drawing.Size(609, 347);
            this.Load += new System.EventHandler(this.PopupEditor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.popupContainerControl1)).EndInit();
            this.popupContainerControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.aliasTable.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treeList1)).EndInit();
            this.treeList1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtMemberSearch.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private global::DevExpress.XtraTreeList.TreeList treeList1;
        /// <remarks/>
        public global::DevExpress.XtraEditors.PopupContainerControl popupContainerControl1;
        private global::DevExpress.XtraEditors.TextEdit txtMemberSearch;
        private global::DevExpress.XtraEditors.TextEdit txtSearch;
        private global::DevExpress.XtraEditors.PanelControl panelControl1;
        private global::DevExpress.XtraEditors.SimpleButton cmdCancel;
        private global::DevExpress.XtraEditors.SimpleButton cmdOk;
        private global::DevExpress.XtraBars.BarManager barManager1;
        private global::DevExpress.XtraBars.Bar bar1;
        private global::DevExpress.XtraBars.Bar bar2;
        private global::DevExpress.XtraBars.Bar bar3;
        private global::DevExpress.XtraBars.BarDockControl barDockControlTop;
        private global::DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private global::DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private global::DevExpress.XtraBars.BarDockControl barDockControlRight;
        private global::DevExpress.XtraBars.PopupMenu popupMenu1;
        private global::DevExpress.XtraBars.BarButtonItem barButtonUncheckAll;
        private global::DevExpress.XtraEditors.ComboBoxEdit aliasTable;
        private global::DevExpress.XtraBars.BarButtonItem barButtonCheckAll;
        private global::DevExpress.XtraBars.BarButtonItem barButtonCheckAllChildren;
        private global::DevExpress.XtraBars.BarButtonItem barButtonCheckAllDesc;
    }
}
