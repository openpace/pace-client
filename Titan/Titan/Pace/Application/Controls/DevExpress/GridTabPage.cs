﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid;
using DevExpress.XtraTab;
using GridView = DevExpress.XtraGrid.Views.Grid.GridView;

namespace Titan.Pace.Application.Controls.DevExpress
{
    internal class GridTabPage : XtraTabPage
    {
        public GridView GridView { get; private set; }
        public GridControl Grid { get; private set; }
        public string QueryString { get; private set; }
        public object Table { get; private set; }

        public void Bind()
        {
            Grid.BeginUpdate();
            //Table.DefaultView.RowFilter = QueryString;
            Grid.DataSource = Table;
            Grid.EndUpdate();
        }

        public GridTabPage(string text, string queryString, object table)
        {
            Grid = new GridControl();
            GridView = new GridView();
            QueryString = queryString;
            Table = table;

            SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(GridView)).BeginInit();

            Name = text;
            Text = text;
            Size = new System.Drawing.Size(790, 412);
            Controls.Add(Grid);
            Grid.Text = text;
            Grid.Dock = DockStyle.Fill;
            Grid.Location = new System.Drawing.Point(0, 0);
            Grid.LookAndFeel.SkinName = "Metropolis";
            Grid.MainView = GridView;
            Grid.Name = text;
            Grid.Size = new System.Drawing.Size(790, 412);
            Grid.ViewCollection.AddRange(new global::DevExpress.XtraGrid.Views.Base.BaseView[] {
            GridView});

            GridView.GridControl = Grid;
            GridView.Name = "gridView" + text;
            GridView.OptionsBehavior.AllowAddRows = DefaultBoolean.False;
            GridView.OptionsBehavior.AllowDeleteRows = DefaultBoolean.False;
            GridView.OptionsBehavior.Editable = false;
            GridView.OptionsView.EnableAppearanceEvenRow = true;
            GridView.OptionsView.EnableAppearanceOddRow = true;
            GridView.OptionsView.ShowIndicator = false;

            Bind();

            ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(GridView)).EndInit();
        }
    }
}
