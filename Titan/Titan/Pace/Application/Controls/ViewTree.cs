#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.ComponentModel;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.ViewInfo;

namespace Titan.Pace.Application.Controls
{
    internal partial class ViewTree : UserControl
    {
        #region Variables
        /// <summary>
        /// Custom event for the subpane before expand.  Occurs before the subpane is expanded.
        /// </summary>
        public event Subpane_BeforeExpand SubpaneBeforeExpand;

        /// <summary>
        /// Custom event for the subpane after expand.  Occurs after the subpane has been expanded.
        /// </summary>
        public event Subpane_AfterExpand SubpaneAfterExpand;

        /// <summary>
        /// Represents the method that will handle the SubpaneBeforeExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_BeforeExpand(object sender, CancelEventArgs e, string controlName);

        /// <summary>
        /// Represents the method that will handle the SubpaneAfterExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_AfterExpand(object sender, EventArgs e, string controlName);

        public string _id;
        public string _dimension;

        #endregion Variables

        #region Properties
        /// <summary>
        /// Gets/sets the visible property of the control.
        /// </summary>
        public bool ControlIsVisible
        {
            get { return subpane.Visible; }
            set { subpane.Visible = value;  }
        }

        public string Dimension
        {
            get { return _dimension; }
            set { _dimension = value; }
        }

        public string Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string DescriptionColumn { get; set; }

        public string DynamicColumn { get; set; }

        public string SelectableColumn { get; set; }

        #endregion Properties

        #region Constructor & Control Load

        /// <summary>
        /// Constructor.
        /// </summary>
        public ViewTree()
        {
            InitializeComponent();

            subpane.AfterCollapse += subpane1_AfterCollapse;
            subpane.AfterExpand += subpane1_AfterExpand;
            barButtonItemAutoFilter.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ShowAutoFilterRow.Text");
        }
        #endregion Constructor & Control Load
 
        #region Events

        /// <summary>
        /// Event handler for the tree view before expanding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_BeforeExpanding(object sender, CancelEventArgs e)
        {
            if(this.SubpaneBeforeExpand != null)
                this.SubpaneBeforeExpand(sender, e, this.Name);
        }

        /// <summary>
        /// Event handler for the tree view after expand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_AfterExpand(object sender, EventArgs e)
        {
            if (this.SubpaneAfterExpand != null)
                this.SubpaneAfterExpand(sender, e, this.Name);
        }
        #endregion Events

        #region Subpane Code
        #region Fields
        private event EventHandler afterExpandEvent;
        private event EventHandler afterCollapseEvent;
        #endregion        

        #region Properties
        public bool IsExpanded
        {
            get
            {
                return this.subpane.IsExpanded;
            }
        }
        #endregion

        #region Public Methods
        public void Expand()
        {
            this.subpane.Expand();
        }

        public void Collapse()
        {
            this.subpane.Collapse();
        }
        #endregion

        #region Events

        public event EventHandler AfterExpand
        {
            add
            {
                this.afterExpandEvent += value;
            }
            remove
            {
                this.afterExpandEvent -= value;
            }
        }

        public event EventHandler AfterCollapse
        {
            add
            {
                this.afterCollapseEvent += value;
            }
            remove
            {
                this.afterCollapseEvent -= value;
            }
        }

        #endregion

        #region Event Firing Methods
        private void AfterExpandEvent()
        {
            if (this.afterExpandEvent != null)
                this.afterExpandEvent(this, EventArgs.Empty);
        }

        private void AfterCollapseEvent()
        {
            if (this.afterCollapseEvent != null)
                this.afterCollapseEvent(this, EventArgs.Empty);
        }
        #endregion

        #region Event Handlers

        void subpane1_AfterCollapse(object sender, EventArgs e)
        {
            this.AfterCollapseEvent();
        }

        void subpane1_AfterExpand(object sender, EventArgs e)
        {
            this.AfterExpandEvent();
        }
        #endregion

        #endregion Subpane Code

        private void treeView1_PopupMenuShowing(object sender, global::DevExpress.XtraTreeList.PopupMenuShowingEventArgs e)
        {
            TreeList tl = (TreeList)sender;
            System.Drawing.Point clickPoint = new System.Drawing.Point(e.Point.X, e.Point.Y);
            TreeListHitInfo ht = tl.CalcHitInfo(clickPoint);
            popupMenu1.ShowPopup(tl.PointToScreen(e.Point));
        }

        private void barButtonItemAutoFilter_ItemClick(object sender, global::DevExpress.XtraBars.ItemClickEventArgs e)
        {
            if (TreeList.OptionsView.ShowAutoFilterRow)
            {
                barButtonItemAutoFilter.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ShowAutoFilterRow.Text");
                TreeList.OptionsView.ShowAutoFilterRow = false;
            }
            else
            {
                barButtonItemAutoFilter.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.HideAutoFilterRow.Text");
                TreeList.OptionsView.ShowAutoFilterRow = true;
            }
        }

        private void treeView1_KeyDown(object sender, KeyEventArgs e)
        {
            Keys CtrlShiftG = Keys.Control | Keys.F;
            if ((e.KeyData & CtrlShiftG) == CtrlShiftG)
            {
                barButtonItemAutoFilter.PerformClick();
            }
        }

        private void toolTipController1_GetActiveObjectInfo(object sender, global::DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (!(e.SelectedControl is TreeList)) return;

            TreeList tree = (TreeList)e.SelectedControl;

            TreeListHitInfo hit = tree.CalcHitInfo(e.ControlMousePosition);

            if (hit.HitInfoType == HitInfoType.Cell)
            {
                TreeListNode node = hit.Node;

                object cellInfo = new TreeListCellToolTipInfo(node, hit.Column, null);

                StringBuilder sb = new StringBuilder();
                if (!String.IsNullOrWhiteSpace(DescriptionColumn))
                {
                    object value = node[DescriptionColumn];
                    if (value != null && !String.IsNullOrWhiteSpace(value.ToString()))
                    {
                        sb.Append(DescriptionColumn).Append(": ").Append(value).AppendLine();
                    }
                }

                //if (!String.IsNullOrWhiteSpace(DynamicColumn))
                //{
                //    object value = node[SelectableColumn];
                //    if (value != null)
                //    {
                //        if ((bool) value)
                //        {
                //            object dynamic = node[DynamicColumn];
                //            sb.Append(DynamicColumn).Append(": ").Append(dynamic).AppendLine();
                //        }
                //    }
                //}

                if (sb.Length > 0)
                {
                    e.Info = new global::DevExpress.Utils.ToolTipControlInfo(cellInfo, sb.ToString());
                }
            }
        }
    }
}