namespace Titan.Pace.Application.Controls
{
    internal partial class PersistToServerSelector
    {

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.SaveMbrTags = new System.Windows.Forms.CheckBox();
            this.SaveCellNotes = new System.Windows.Forms.CheckBox();
            this.SaveEssbaseData = new System.Windows.Forms.CheckBox();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.panel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.groupBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(398, 92);
            this.panel1.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.SaveMbrTags);
            this.groupBox1.Controls.Add(this.SaveCellNotes);
            this.groupBox1.Controls.Add(this.SaveEssbaseData);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(398, 92);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            // 
            // SaveMbrTags
            // 
            this.SaveMbrTags.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveMbrTags.AutoSize = true;
            this.SaveMbrTags.Location = new System.Drawing.Point(17, 65);
            this.SaveMbrTags.Name = "SaveMbrTags";
            this.SaveMbrTags.Size = new System.Drawing.Size(80, 17);
            this.SaveMbrTags.TabIndex = 5;
            this.SaveMbrTags.Text = "checkBox2";
            this.SaveMbrTags.UseVisualStyleBackColor = true;
            // 
            // SaveCellNotes
            // 
            this.SaveCellNotes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveCellNotes.AutoSize = true;
            this.SaveCellNotes.Location = new System.Drawing.Point(17, 42);
            this.SaveCellNotes.Name = "SaveCellNotes";
            this.SaveCellNotes.Size = new System.Drawing.Size(80, 17);
            this.SaveCellNotes.TabIndex = 4;
            this.SaveCellNotes.Text = "checkBox2";
            this.SaveCellNotes.UseVisualStyleBackColor = true;
            // 
            // SaveEssbaseData
            // 
            this.SaveEssbaseData.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.SaveEssbaseData.AutoSize = true;
            this.SaveEssbaseData.Location = new System.Drawing.Point(17, 19);
            this.SaveEssbaseData.Name = "SaveEssbaseData";
            this.SaveEssbaseData.Size = new System.Drawing.Size(80, 17);
            this.SaveEssbaseData.TabIndex = 3;
            this.SaveEssbaseData.Text = "checkBox1";
            this.SaveEssbaseData.UseVisualStyleBackColor = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // PersistToServerSelector
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.panel1);
            this.Name = "PersistToServerSelector";
            this.Size = new System.Drawing.Size(398, 92);
            this.Resize += new System.EventHandler(this.PersistToServerSelector_Resize);
            this.panel1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.ComponentModel.IContainer components;
    }
}
