namespace Titan.Pace.Application.Controls
{
    partial class Header
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            global::DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new global::DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.lblHeadingText = new System.Windows.Forms.Label();
            this.ValidationEditor = new global::DevExpress.XtraEditors.TextEdit();
            this.dxValidationProvider1 = new global::DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ValidationEditor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // lblHeadingText
            // 
            this.lblHeadingText.AutoSize = true;
            this.lblHeadingText.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblHeadingText.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHeadingText.Location = new System.Drawing.Point(0, 0);
            this.lblHeadingText.Name = "lblHeadingText";
            this.lblHeadingText.Size = new System.Drawing.Size(0, 13);
            this.lblHeadingText.TabIndex = 4;
            this.lblHeadingText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ValidationEditor
            // 
            this.ValidationEditor.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ValidationEditor.EditValue = "Test";
            this.ValidationEditor.Location = new System.Drawing.Point(0, 0);
            this.ValidationEditor.Name = "ValidationEditor";
            this.ValidationEditor.Properties.AppearanceReadOnly.BackColor = System.Drawing.SystemColors.Control;
            this.ValidationEditor.Properties.AppearanceReadOnly.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ValidationEditor.Properties.AppearanceReadOnly.Options.UseBackColor = true;
            this.ValidationEditor.Properties.AppearanceReadOnly.Options.UseFont = true;
            this.ValidationEditor.Properties.BorderStyle = global::DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.ValidationEditor.Properties.ReadOnly = true;
            this.ValidationEditor.Size = new System.Drawing.Size(135, 18);
            this.ValidationEditor.TabIndex = 5;
            conditionValidationRule1.ConditionOperator = global::DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "This value is not valid";
            conditionValidationRule1.ErrorType = global::DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning;
            this.dxValidationProvider1.SetValidationRule(this.ValidationEditor, conditionValidationRule1);
            // 
            // Header
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.ValidationEditor);
            this.Controls.Add(this.lblHeadingText);
            this.Name = "Header";
            this.Size = new System.Drawing.Size(135, 21);
            this.Load += new System.EventHandler(this.Header_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ValidationEditor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblHeadingText;
        private global::DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private global::DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1;
        public global::DevExpress.XtraEditors.TextEdit ValidationEditor;
    }
}
