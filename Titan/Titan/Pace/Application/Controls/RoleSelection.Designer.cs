namespace Titan.Pace.Application.Controls
{
    partial class RoleSelection
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtRole = new System.Windows.Forms.TextBox();
            this.cboRole = new System.Windows.Forms.ComboBox();
            this.lblDims = new System.Windows.Forms.Label();
            this.lblRoleSelection = new System.Windows.Forms.Label();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.pnlOptions = new System.Windows.Forms.Panel();
            this.chkSupInvIx = new System.Windows.Forms.CheckBox();
            this.chkFilteredTotals = new System.Windows.Forms.CheckBox();
            this.pnlButtons = new System.Windows.Forms.Panel();
            this.cmdOk = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.pnlList = new System.Windows.Forms.Panel();
            this.lstSeasonProc = new System.Windows.Forms.ListBox();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.pnlOptions.SuspendLayout();
            this.pnlButtons.SuspendLayout();
            this.pnlList.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtRole
            // 
            this.txtRole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRole.Location = new System.Drawing.Point(3, 20);
            this.txtRole.Name = "txtRole";
            this.txtRole.ReadOnly = true;
            this.txtRole.Size = new System.Drawing.Size(281, 20);
            this.txtRole.TabIndex = 0;
            this.txtRole.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextControlKeyDown);
            this.txtRole.Validating += new System.ComponentModel.CancelEventHandler(this.Validate_PafPlannerConfig);
            // 
            // cboRole
            // 
            this.cboRole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRole.FormattingEnabled = true;
            this.cboRole.Location = new System.Drawing.Point(3, 20);
            this.cboRole.Name = "cboRole";
            this.cboRole.Size = new System.Drawing.Size(281, 21);
            this.cboRole.TabIndex = 0;
            this.cboRole.Visible = false;
            this.cboRole.SelectedIndexChanged += new System.EventHandler(this.cboRole_SelectedIndexChanged);
            this.cboRole.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextControlKeyDown);
            // 
            // lblDims
            // 
            this.lblDims.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblDims.AutoSize = true;
            this.lblDims.Location = new System.Drawing.Point(3, 44);
            this.lblDims.Name = "lblDims";
            this.lblDims.Size = new System.Drawing.Size(31, 13);
            this.lblDims.TabIndex = 15;
            this.lblDims.Text = "____";
            // 
            // lblRoleSelection
            // 
            this.lblRoleSelection.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRoleSelection.AutoSize = true;
            this.lblRoleSelection.Location = new System.Drawing.Point(3, 4);
            this.lblRoleSelection.Name = "lblRoleSelection";
            this.lblRoleSelection.Size = new System.Drawing.Size(25, 13);
            this.lblRoleSelection.TabIndex = 16;
            this.lblRoleSelection.Text = "___";
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // pnlOptions
            // 
            this.pnlOptions.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlOptions.Controls.Add(this.chkSupInvIx);
            this.pnlOptions.Controls.Add(this.chkFilteredTotals);
            this.pnlOptions.Location = new System.Drawing.Point(3, 366);
            this.pnlOptions.Name = "pnlOptions";
            this.pnlOptions.Size = new System.Drawing.Size(279, 39);
            this.pnlOptions.TabIndex = 19;
            // 
            // chkSupInvIx
            // 
            this.chkSupInvIx.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSupInvIx.Location = new System.Drawing.Point(2, 20);
            this.chkSupInvIx.Name = "chkSupInvIx";
            this.chkSupInvIx.Size = new System.Drawing.Size(274, 17);
            this.chkSupInvIx.TabIndex = 19;
            this.chkSupInvIx.Text = "_";
            this.chkSupInvIx.UseVisualStyleBackColor = true;
            this.chkSupInvIx.CheckedChanged += new System.EventHandler(this.chkSupInvIx_CheckedChanged);
            // 
            // chkFilteredTotals
            // 
            this.chkFilteredTotals.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFilteredTotals.Location = new System.Drawing.Point(2, 3);
            this.chkFilteredTotals.Name = "chkFilteredTotals";
            this.chkFilteredTotals.Size = new System.Drawing.Size(274, 17);
            this.chkFilteredTotals.TabIndex = 20;
            this.chkFilteredTotals.Text = "_";
            this.chkFilteredTotals.UseVisualStyleBackColor = true;
            // 
            // pnlButtons
            // 
            this.pnlButtons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlButtons.Controls.Add(this.cmdOk);
            this.pnlButtons.Controls.Add(this.cmdCancel);
            this.pnlButtons.Location = new System.Drawing.Point(3, 408);
            this.pnlButtons.Name = "pnlButtons";
            this.pnlButtons.Size = new System.Drawing.Size(279, 29);
            this.pnlButtons.TabIndex = 20;
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Enabled = false;
            this.cmdOk.Location = new System.Drawing.Point(122, 3);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 4;
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.CausesValidation = false;
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(203, 3);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // pnlList
            // 
            this.pnlList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlList.Controls.Add(this.lstSeasonProc);
            this.pnlList.Location = new System.Drawing.Point(3, 60);
            this.pnlList.Name = "pnlList";
            this.pnlList.Size = new System.Drawing.Size(281, 303);
            this.pnlList.TabIndex = 21;
            // 
            // lstSeasonProc
            // 
            this.lstSeasonProc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstSeasonProc.FormattingEnabled = true;
            this.lstSeasonProc.IntegralHeight = false;
            this.lstSeasonProc.Location = new System.Drawing.Point(0, 0);
            this.lstSeasonProc.Name = "lstSeasonProc";
            this.lstSeasonProc.Size = new System.Drawing.Size(281, 303);
            this.lstSeasonProc.TabIndex = 3;
            this.lstSeasonProc.DoubleClick += new System.EventHandler(this.lstSeasonProc_DoubleClick);
            this.lstSeasonProc.Enter += new System.EventHandler(this.lstSeasonProc_Enter);
            this.lstSeasonProc.SelectedIndexChanged += new System.EventHandler(this.lstSeasonProc_SelectedIndexChanged);
            this.lstSeasonProc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.TextControlKeyDown);
            this.lstSeasonProc.Click += new System.EventHandler(this.lstSeasonProc_Click);
            // 
            // RoleSelection
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnlList);
            this.Controls.Add(this.pnlButtons);
            this.Controls.Add(this.txtRole);
            this.Controls.Add(this.cboRole);
            this.Controls.Add(this.lblRoleSelection);
            this.Controls.Add(this.lblDims);
            this.Controls.Add(this.pnlOptions);
            this.DoubleBuffered = true;
            this.Name = "RoleSelection";
            this.Size = new System.Drawing.Size(285, 437);
            this.Load += new System.EventHandler(this.RoleSelection_Load);
            this.Resize += new System.EventHandler(this.RoleSelection_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.pnlOptions.ResumeLayout(false);
            this.pnlButtons.ResumeLayout(false);
            this.pnlList.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRole;
        private System.Windows.Forms.ComboBox cboRole;
        private System.Windows.Forms.Label lblDims;
        private System.Windows.Forms.Label lblRoleSelection;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Panel pnlOptions;
        private System.Windows.Forms.CheckBox chkSupInvIx;
        private System.Windows.Forms.CheckBox chkFilteredTotals;
        private System.Windows.Forms.Panel pnlButtons;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Panel pnlList;
        private System.Windows.Forms.ListBox lstSeasonProc;

    }
}
