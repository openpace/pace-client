﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace Titan.Pace.Application.Controls.Display
{
    internal class AliasTableDisplay
    {
        public string AliasTable { get; set; }

        public string AliasTableDisplayValue { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="aliasTable">The actual alias table.</param>
        /// <param name="aliasTableDisplayValue">The alias table to display in the dropdown.</param>
        public AliasTableDisplay(string aliasTable, string aliasTableDisplayValue)
        {
            AliasTable = aliasTable;
            AliasTableDisplayValue = aliasTableDisplayValue;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if(obj == null)
            {
                return false;
            }

            AliasTableDisplay o;

            try
            {
                o = (AliasTableDisplay)obj;
            }
            catch (Exception)
            {
                return false;
            }
            

            if (AliasTable.ToLower().Equals(o.AliasTable.ToLower()) && 
                AliasTableDisplayValue.ToLower().Equals(o.AliasTableDisplayValue.ToLower()))
            {
                return true;
            }
            return false;
        }

        public override string ToString()
        {
            return AliasTableDisplayValue;
        }
    }
}
