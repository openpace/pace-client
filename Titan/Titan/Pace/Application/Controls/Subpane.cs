﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
#region Using directives

using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using Titan.Pace.Application.Controls.Microsoft;

#endregion

namespace Titan.Pace.Application.Controls
{
    [ToolboxBitmap(typeof(Panel))]
    internal partial class Subpane : Panel
    {
        #region Constants
        ///// <summary>
        ///// Radius of the curved corners.
        ///// </summary>
        private const int curveRadius = 3;

        /// <summary>
        /// Diameter of the collapase button.
        /// </summary>
        private const int COLLAPSE_BUTTON_DIAMETER = 16;

        /// <summary>
        /// Speed to animate the expanding/collapsing of the panel.
        /// </summary>
        private const int animateSpeed = 5;

        /// <summary>
        /// Amount of space to pad the title bar caption with.
        /// </summary>
        private const int titlePadding = 8;

        /// <summary>
        /// Mode of the header's gradient effect.
        /// </summary>
        private const LinearGradientMode headerGradientMode = LinearGradientMode.Horizontal;
        #endregion

        #region Fields
        #region Header Fields
        /// <summary>
        /// Color to start the title bar gradient with.
        /// </summary>
        private Color headerBeginGradientColor;

        /// <summary>
        /// Color to end the title bar gradient with.
        /// </summary>
        private Color headerEndGradientColor;

        /// <summary>
        /// Fore Color of the title bar when active.
        /// </summary>
        private Color headerTextHighlightColor;

        /// <summary>
        /// Text of the title bar.
        /// </summary>
        private string headerText;

        /// <summary>
        /// Specifies is the title bar is currently active.
        /// </summary>
        private bool isHeaderSelected;
        #endregion

        #region Panel Fields
        /// <summary>
        /// Specifies if the Subpane is currently expanded or not.
        /// </summary>
        private bool isExpanded;

        /// <summary>
        /// Specifies if the expanding/collapsing of the subpane
        /// should be animated or not.
        /// </summary>
        private bool isAnimated;


        /// <summary>
        /// A cached copy of whether the panel should be AutoSized or
        /// not.  This is needed so that when the AutoSize property
        /// can be reset when the panel is expanded.
        /// </summary>
        private bool isAutosized;

        /// <summary>
        /// A cached copy of the fully height of the panel.  This
        /// is needed so that the panel can be reset to its original
        /// height when it is expanded.
        /// </summary>
        private int fullyExpandedHeight;

        /// <summary>
        /// The default expanded height of the subpane.
        /// </summary>
        private int defaultExpandedHeight; //Fix TTN-258

        /// <summary>
        /// A cached copy of the panel's cursor.  This is needed 
        /// so that when the title bar is not selected, the original
        /// cursor can be used again.
        /// </summary>
        private Cursor currentCursor;

        /// <summary>
        /// The background color for the panel.
        /// </summary>
        private Color backColor;
        #endregion
        #endregion

        #region Events
        private CancelEventHandler beforeExpandEvent;
        private CancelEventHandler beforeCollapseEvent;
        private EventHandler afterExpandEvent;
        private EventHandler afterCollapseEvent;
        #endregion

        #region Constructors
        public Subpane()
        {
            InitializeComponent();
            
            // Set the styles that the control supports.
            this.SetStyle(ControlStyles.CacheText, true);
            this.SetStyle(ControlStyles.ContainerControl, true);
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.SetStyle(ControlStyles.ResizeRedraw, true);
            this.SetStyle(ControlStyles.Selectable, true);
            this.SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            this.SetStyle(ControlStyles.UserPaint, true);
            this.SetStyle(ControlStyles.UseTextForAccessibility, true);

            // Initialize default values.
            this.GradientColorBegin = ProfessionalColors.MenuStripGradientBegin;
            this.GradientColorEnd = ProfessionalColors.MenuStripGradientEnd;
            this.HighlightColor = this.ForeColor;
            this.Text = this.Name;
            this.isHeaderSelected = false;

            this.isExpanded = true;
            this.isAnimated = IsUserMenuScroll();
            this.isAutosized = base.AutoSize;
            this.fullyExpandedHeight = this.defaultExpandedHeight;// base.Height;
            this.currentCursor = this.Cursor;
            this.PanelColor = ProfessionalColors.MenuStripGradientEnd;
            this.TabStop = true;

            // Set the top padding for the title bar.
            base.Padding = new Padding(0, this.Font.Height + titlePadding, 0, 0);
            base.BackColor = Color.Transparent;
        }
        #endregion

        #region Overrides
        #region Properties
        /// <summary>
        /// The text to show on the titlebar of the Subpane.
        /// </summary>
        /// <value>The titlebar's text.</value>
        [Browsable(true),
        Category("Appearance"),
        DefaultValue(""),
        LocalizedDescription("TextDescription", "WordSubpaneExample.SubpaneResources"),
        Localizable(true)]
        public override string Text
        {
            get
            {
                return this.headerText;
            }

            set
            {
                this.headerText = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// The default expanded height of the subpane.
        /// </summary>
        /// <value>The height of the subpane.</value>
        [Browsable(true),
        Category("Layout"),
        DefaultValue("200"),
        LocalizedDescription("The default expanded height of the subpane.", "SubpaneResources"),
        Localizable(true)]
        public int DefaultExpandedHeight
        {
            get
            {
                return this.defaultExpandedHeight;
            }

            set
            {
                this.defaultExpandedHeight = value;
            }
        }

        public new Padding Padding
        {
            get
            {
                Padding padding = base.Padding;
                return new Padding(padding.Left, padding.Top - (this.Font.Height + titlePadding), padding.Right, padding.Bottom);
            }
            set
            {
                base.Padding = new Padding(value.Left, value.Top + this.Font.Height + titlePadding, value.Right, value.Bottom);
            }
        }

        /// <summary>
        /// Specifies if the Subpane should automatically size itself
        /// to fit all controls.
        /// </summary>
        /// <value>Subpane automatically size itself.</value>
        public override bool AutoSize
        {
            // Override the AutoSize property so that if 
            // the value is changed when the Subpane is collapsed
            // the cached copy of the property can be updated.
            // When the Subpane is subsequently expanded, the 
            // new value for AutoSize will be used.
            get
            {
                return this.isAutosized;
            }
            set
            {
                this.isAutosized = value;
                if (this.IsExpanded)
                {
                    base.AutoSize = this.isAutosized;
                }
            }
        }

        [Browsable(false),
        EditorBrowsable(System.ComponentModel.EditorBrowsableState.Never)]
        public override Color BackColor
        {
            get
            {
                return base.BackColor;
            }

            set
            {
                return;
            }
        }
        #endregion

        #region Methods
        /// <summary>
        /// Raises the MouseMove event.
        /// </summary>
        /// <param name="e">A MouseEventArgs that contains the event data.</param>
        protected override void OnMouseMove(MouseEventArgs e)
        {
            //Debug.Assert(e != null);
            base.OnMouseMove(e);
            Cursor.Current = this.currentCursor;

            // Only update values if they aren't already correct
            // to prevent unnecessary repaints.  Otherwise, there
            // might be some flickering.

            if ((this.isHeaderSelected) && (e.Y > this.Font.Height + titlePadding))
            {
                this.isHeaderSelected = false;
                this.currentCursor = this.Cursor;
                this.Invalidate();
                return;
            }

            if ((!this.isHeaderSelected) && (e.Y <= this.Font.Height + titlePadding))
            {
                this.currentCursor = Cursors.Hand;
                this.isHeaderSelected = true;
                this.Invalidate();
                return;
            }
        }


        /// <summary>
        /// Raises the MouseDown event.
        /// </summary>
        /// <param name="e">A MouseEventArgs that contains the event data.</param>
        protected override void OnMouseDown(MouseEventArgs e)
        {
            //Debug.Assert(e != null);

            // If the click was in the title bar
            // then the subpane state should be toggled.
            if (e.Button != MouseButtons.Right)
            {
                if (e.Y <= this.Font.Height + titlePadding)
                {
                    this.ToggleExpansion();
                }
            }

            base.OnMouseDown(e);
        }


        /// <summary>
        /// Raises the MouseLeave event.
        /// </summary>
        /// <param name="e">An EventArgs that contains event data.</param>
        protected override void OnMouseLeave(EventArgs e)
        {
            //Debug.Assert(e != null);

            // Update the state of the title bar.
            this.isHeaderSelected = false;
            base.OnMouseLeave(e);
            this.Invalidate();
        }

        /// <summary>
        /// Raises the GotFocus event.
        /// </summary>
        /// <param name="e">An EventArgs that contains event data.</param>
        protected override void OnGotFocus(EventArgs e)
        {
            //Debug.Assert(e != null);

            // Invalidate the control so that the title 
            // bar can be redrawn with focus.
            base.OnGotFocus(e);
            this.Invalidate();
        }

        /// <summary>
        /// Raises the LostFocus event.
        /// </summary>
        /// <param name="e">An EventArgs that contains event data.</param>
        protected override void OnLostFocus(EventArgs e)
        {
            //Debug.Assert(e != null);

            // Invalidate the control so that the title
            // bar can be redrawn without focus.
            base.OnLostFocus(e);
            this.Invalidate();
        }


        /// <summary>
        /// Raises the Enter event.
        /// </summary>
        /// <param name="e">An EventArgs that contains the event data.</param>
        protected override void OnEnter(EventArgs e)
        {
            base.OnEnter(e);

            // If the Subpane is not expanded, then
            // just focus on the Subpane itself and 
            // not any of the child controls.
            if (!this.IsExpanded)
                this.Focus();
        }

        /// <summary>
        /// Processes a dialog key.
        /// </summary>
        /// <param name="keyData">One of the Keys values that represents the key to process.</param>
        /// <returns>True is the key was processed by the control; otherwise false.</returns>
        protected override bool ProcessDialogKey(Keys keyData)
        {
            // If the key was space or return, and the control already has focus,
            // toggle the current state of the subpane.  Otherwise, let the panel
            // handle the key.
            if (this.Focused && (keyData == Keys.Space || keyData == Keys.Return))
            {
                this.ToggleExpansion();
                return true;
            }

            // If the Tab key was pressed and the Subpane is not expanded,
            // then move to the next control after the last item in the Subpane's
            // tab sequence.
            if (!this.IsExpanded && (keyData == Keys.Tab))
            {
                Control lastControl = this.GetLastControl();
                Control topLevelParent = this.GetTopLevelParent();
                if (topLevelParent != null)
                    return topLevelParent.SelectNextControl(lastControl, true, true, true, true);
            }


            // If the Tab and the Shift key were pressed and the Subpane is 
            // not expanded, then move to the previous control before the first item 
            // in the Subpane's tab sequence.
            if (!this.IsExpanded && (keyData == (Keys.Tab & Keys.Shift)))
            {
                Control lastControl = this.GetLastControl();
                Control topLevelParent = this.GetTopLevelParent();
                if (topLevelParent != null)
                    return topLevelParent.SelectNextControl(lastControl, false, true, true, true);
            }

            return base.ProcessDialogKey(keyData);
        }


        /// <summary>
        /// Performs the work of setting the specified bounds of this control.
        /// </summary>
        /// <param name="x">
        /// The new Left property value of the control.
        /// </param>
        /// <param name="y">
        /// The new Top property value of the control.
        /// </param>
        /// <param name="width">
        /// The new Width property value of the control.
        /// </param>
        /// <param name="height">
        /// The new Height property value of the control.
        /// </param>
        /// <param name="specified">
        /// A bitwise combination of the BoundsSpecified values.
        /// </param>
        protected override void SetBoundsCore(int x, int y, int width, int height, BoundsSpecified specified)
        {
            // Save the latest height as the fully expanded height.
            if ((specified & BoundsSpecified.Height) == BoundsSpecified.Height)
                this.fullyExpandedHeight = height;
            if (this.IsExpanded)
            {
                // The Subpane is already expanded, so just 
                // use the latest height value.
                base.SetBoundsCore(x, y, width, height, specified);
            }
            else
            {
                // The Subpane is collapsed, so use the current height value.
                // When the Subpane is expanded next, it will expand out to the
                // newly set height value.
                //Debug.Assert(!this.DesignMode);
                base.SetBoundsCore(x, y, width, this.Height, specified);
            }
        }

        /// <summary>
        /// Raises the Paint event.
        /// </summary>
        /// <param name="e">A PaintEventArgs that contains the event data.</param>
        protected override void OnPaint(PaintEventArgs e)
        {
            //Debug.Assert(e != null);

            base.OnPaint(e);
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            this.DrawBackground(e.Graphics);
            this.DrawHeader(e.Graphics);
        }
        #endregion
        #endregion

        #region Properties
        #region Helper Properties
        private TextFormatFlags FormatFlags
        {
            get
            {
                return (TextFormatFlags.SingleLine | TextFormatFlags.EndEllipsis |
                    TextFormatFlags.VerticalCenter | TextFormatFlags.ExternalLeading);
            }
        }
        #endregion

        #region Header Properties
        /// <summary>
        /// The color to start the titlebar's gradient with.
        /// </summary>
        /// <value>Titlebar's start gradient color.</value>
        public Color GradientColorBegin
        {
            get
            {
                return this.headerBeginGradientColor;
            }
            set
            {
                this.headerBeginGradientColor = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// The color to end the titlebar's gradient with.
        /// </summary>
        /// <value>Titlebar's end gradient color.</value>
        public Color GradientColorEnd
        {
            get
            {
                return this.headerEndGradientColor;
            }
            set
            {
                this.headerEndGradientColor = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// The ForeColor of the titlebar when it has mouse focus.
        /// </summary>
        /// <value>Titlebar's ForeColor when it has mouse focus.</value>
        public Color HighlightColor
        {
            get
            {
                return this.headerTextHighlightColor;
            }
            set
            {
                this.headerTextHighlightColor = value;
                this.Invalidate();
            }
        }

        #endregion

        #region Panel Properties
        /// <summary>
        /// The background color of the collapsible region of the Subpane.
        /// </summary>
        /// <value>Background color of the collapsible region of the Subpane.</value>
        public Color PanelColor
        {
            get
            {
                return this.backColor;
            }
            set
            {
                this.backColor = value;
                this.Invalidate();
            }
        }

        /// <summary>
        /// Specifies is the Subpane is currently expanded or not.
        /// </summary>
        /// <value>True if the Subpane currently expanded.</value>
        public bool IsExpanded
        {
            get
            {
                return this.isExpanded;
            }
        }

        /// <summary>
        /// Specifies if the expanding and collapsing 
        /// the Subpane should be animated.
        /// </summary>
        /// <value>Animate the expanding and collapsing of the Subpane.</value>
        public bool IsAnimated
        {
            get
            {
                return this.isAnimated;
            }
            set
            {
                this.isAnimated = value;
            }
        }
        //public int CurveRadius
        //{
        //    get { return curveRadius; }
        //    set { curveRadius = value; }
        //}
        private new BorderStyle BorderStyle
        {
            get
            {
                return base.BorderStyle;
            }
        }

        /// <summary>
        /// Internal property accessor to set the Subpane's state.
        /// </summary>
        /// <value>True if the Subpane is current expanded.</value>
        private bool IsExpandedInternal
        {
            set
            {
                this.isExpanded = value;
                this.Invalidate();
            }
        }
        #endregion
        #endregion

        #region Events
        public event CancelEventHandler BeforeExpanding
        {
            add
            {
                this.beforeExpandEvent += value;
            }
            remove
            {
                this.beforeExpandEvent -= value;
            }
        }

        public event CancelEventHandler BeforeCollapsing
        {
            add
            {
                this.beforeCollapseEvent += value;
            }
            remove
            {
                this.beforeCollapseEvent -= value;
            }
        }


        public event EventHandler AfterExpand
        {
            add
            {
                this.afterExpandEvent += value;
            }
            remove
            {
                this.afterExpandEvent -= value;
            }
        }
        public event EventHandler AfterCollapse
        {
            add
            {
                this.afterCollapseEvent += value;
            }
            remove
            {
                this.afterCollapseEvent -= value;
            }
        }
        #endregion

        #region Event Firing Methods
        private bool BeforeExpandingEvent()
        {
            if (this.beforeExpandEvent != null)
            {
                CancelEventArgs eventArgs = new CancelEventArgs(false);
                this.beforeExpandEvent(this, eventArgs);
                return eventArgs.Cancel;
            }
            return false;
        }

        private void AfterExpandingEvent()
        {
            if (this.afterExpandEvent != null)
            {
                this.afterExpandEvent(this, new EventArgs());
            }
        }

        private bool BeforeCollapsingEvent()
        {
            if (this.beforeCollapseEvent != null)
            {
                CancelEventArgs eventArgs = new CancelEventArgs(false);
                this.beforeCollapseEvent(this, eventArgs);
                return eventArgs.Cancel;
            }
            return false;
        }

        private void AfterCollapsingEvent()
        {
            if (this.afterCollapseEvent != null)
            {
                this.afterCollapseEvent(this, new EventArgs());
            }
        }
        #endregion

        #region Expand/Collapse Methods
        /// <summary>
        /// Expands the Subpane if it is not already expanded.
        /// </summary>
        public void Expand()
        {
            if (this.IsExpanded || !this.Enabled)
                return;

            this.Expand(this.IsAnimated);
            this.Refresh();
        }

        /// <summary>
        /// Expands the Subpane with or without animation.
        /// </summary>
        /// <param name="animate">
        /// Whether to animate the Subpane expansion or not.
        /// </param>
        private void Expand(bool animate)
        {
            // Fire the BeforeExpanding event.
            if (this.BeforeExpandingEvent())
                return;

            // Set the Subpane to be expanded.  This will ensure that
            // the latest height will be used while the control grows.
            //Debug.Assert(!this.IsExpanded);
            this.IsExpandedInternal = true;

            // Suspend the layout of the parent so that different combinations
            // of control docking will not effect the Subpane expansion.
            this.Parent.SuspendLayout();

            //Fix (TTN-258)
            if (this.fullyExpandedHeight == this.Height)
                this.fullyExpandedHeight = this.defaultExpandedHeight;

            int expandedHeight = this.fullyExpandedHeight;

            if (animate)
            {
                // Since the Subpane expansion is animated, grow the height
                // of the control by the animate speed and redraw it.
                while (this.Height < expandedHeight)
                {
                    this.Height = Math.Min(this.Height + animateSpeed, expandedHeight);
                    this.Refresh();
                }
            }
            else
            {
                // The Subpane expansion is not animated, so just set the height
                // to the correct amount and redraw.
                this.Height = expandedHeight;
                this.Refresh();
            }

            // Reset the AutoSize property for the SubPane to the correct value
            // now that the control is fully expanded.
            this.AutoSize = this.isAutosized;
            this.fullyExpandedHeight = expandedHeight;
            this.Parent.ResumeLayout(true);
            this.AfterExpandingEvent();
        }

        /// <summary>
        /// Collapses the Subpane if is is not already collapsed.
        /// </summary>
        public void Collapse()
        {
            if (!this.IsExpanded || !this.Enabled)
                return;

            this.Collapse(this.IsAnimated);
        }

        /// <summary>
        /// Collapses the Subpane with or without animation.
        /// </summary>
        /// <param name="animate">
        /// Whether to animate the Subpane collapsing or not.
        /// </param>
        private void Collapse(bool animate)
        {
            // Fire the BeforeCollapsing event.
            if (this.BeforeCollapsingEvent())
                return;

            // Set the Subpane to be expanded.  This will ensure that
            // the latest height will be used while the control grows.
            //Debug.Assert(this.IsExpanded);

            // Suspend the layout of the parent so that different combinations
            // of control docking will not effect the Subpane expansion.
            this.Parent.SuspendLayout();

            // Cache a copy of the full height of the Subpane.
            int expandedHeight = this.fullyExpandedHeight;

            // Turn off AutoSize for the SubPane if it is currently
            // enabled during the Collapse state of the SubPane.
            this.isAutosized = this.AutoSize;
            this.AutoSize = false;

            if (animate)
            {
                // Since the Subpane collapse is animated, shrink the height
                // of the control by the animate speed and redraw it.
                while (this.Height > this.Font.Height + titlePadding)
                {

                    this.Height = Math.Max(this.Height - animateSpeed,
                        this.Font.Height + titlePadding);
                    this.Refresh();
                }
            }
            else
            {
                // The Subpane expansion is not animated, so just set the height
                // to the correct amount and redraw.
                this.Height = this.Font.Height + titlePadding;
                this.Refresh();
            }

            // Collapse is done, so set the Expanded state 
            // of the Subpane to false.
            this.IsExpandedInternal = false;

            // Save what the fully expanded height of the control should
            // be for when the Subpane is expanded next.
            this.fullyExpandedHeight = expandedHeight;

            this.Parent.ResumeLayout(true);

            this.AfterCollapsingEvent();
        }
        #endregion

        #region Helper Methods
        /// <summary>
        /// Draws the background for the subpane.
        /// </summary>
        /// <param name="graphics">The graphics to use to paint the background.</param>
        private void DrawBackground(Graphics graphics)
        {
            //Debug.Assert(graphics != null);

            // If the subpane is not expanded, there is no background to be 
            // painted, so skip it.
            if (this.isExpanded)
            {
                RectangleF background = new RectangleF(0, this.Font.Height + titlePadding, this.Width,
                    this.Height - (this.Font.Height + titlePadding));
                graphics.FillRectangle(new SolidBrush(this.PanelColor), background);
            }
        }

        /// <summary>
        /// Draws the title bar for the subpane.
        /// </summary>
        /// <param name="graphics">The graphics to use to paint the title bar.</param>
        private void DrawHeader(Graphics graphics)
        {
            //Debug.Assert(graphics != null);

            Rectangle header = new Rectangle(0, 0, this.Width,
                this.Font.Height + titlePadding);
            GraphicsPath path = GetPath(header);
            graphics.FillPath(new LinearGradientBrush(header, this.headerBeginGradientColor,
                this.headerEndGradientColor, headerGradientMode), path);

            // Add the text to the title bar.
            header = new Rectangle(titlePadding, 0, this.Width - (titlePadding * 2),
                this.Font.Height + titlePadding);
            this.DrawHeaderText(graphics, header);
        }

        /// <summary>
        /// Draws the title bar text for the subpane.
        /// </summary>
        /// <param name="graphics">The graphics to use to paint the text for the title bar.</param>
        /// <param name="rectangle">The rectangle to paint the text in.</param>
        private void DrawHeaderText(Graphics graphics, Rectangle rectangle)
        {
            //Debug.Assert(graphics != null);
            //Debug.Assert(rectangle != null);

            // Add the title bar caption.
            this.DrawString(graphics, this.headerText, this.Font, rectangle, FormatFlags | TextFormatFlags.Left);

            // If the subpane has focus, add a rectangle around the caption.
            ////if (this.Focused)
            ////{
            ////    Size headerSize = TextRenderer.MeasureText(graphics, this.headerText,
            ////        this.Font, rectangle.Size, FormatFlags | TextFormatFlags.Left);
            ////    Rectangle header = new Rectangle(0, 0, headerSize.Width + (titlePadding * 2),
            ////        this.Font.Height + titlePadding);
            ////    graphics.DrawRectangle(new Pen(this.ForeColor), header);
            ////}

            // Draw the up/down arrows for the title bar.
            //////this.DrawArrows(graphics, rectangle);
        }

        /// <summary>
        /// Draws the up/down arrows for the title bar.
        /// </summary>
        /// <param name="graphics">The graphics to use to paint the arrows for the title bar.</param>
        /// <param name="rectangle">The rectangle to pain the text in.</param>
        ////private void DrawArrows(Graphics graphics, Rectangle rectangle)
        ////{
        ////    if (this.IsExpanded)
        ////        this.DrawString(graphics, Convert.ToString(Convert.ToChar(0xC8)),
        ////            new Font("Wingdings", this.Font.Size), rectangle, FormatFlags | TextFormatFlags.Right);
        ////    else
        ////        this.DrawString(graphics, Convert.ToString(Convert.ToChar(0xCA)),
        ////            new Font("Wingdings", this.Font.Size), rectangle, FormatFlags | TextFormatFlags.Right);
        ////}
        private void DrawArrows(Graphics graphics, Rectangle rectangle)
        {
            if (this.IsExpanded)
                this.DrawString(graphics, Convert.ToString(Convert.ToChar(0x70)),
                    new Font("Wingdings 3", this.Font.Size), rectangle, FormatFlags | TextFormatFlags.Right);
            else
                this.DrawString(graphics, Convert.ToString(Convert.ToChar(0x71)),
                    new Font("Wingdings 3", this.Font.Size), rectangle, FormatFlags | TextFormatFlags.Right);
        }

        /// <summary>
        /// Paints a string for the control.
        /// </summary>
        /// <param name="graphics">The graphics to use to paint the arrows for the title bar.</param>
        /// <param name="text">The text to paint.</param>
        /// <param name="font">The font to paint the the text in.</param>
        /// <param name="rectangle">The rectangle to pain the text in.</param>
        /// <param name="flags">The format to use when painting the text.</param>
        private void DrawString(Graphics graphics, string text, Font font, Rectangle rectangle, TextFormatFlags flags)
        {
            // Determine which brush to use.
            Color fontColor = Color.Empty;
            if (!this.isHeaderSelected)
                fontColor = this.ForeColor;
            else
                fontColor = this.headerTextHighlightColor;

            TextRenderer.DrawText(graphics, text, font, rectangle, fontColor, flags);

            DrawCollapseButton(graphics);
        }

        
        private Color colorCollapseButtonChevrons()
        {
            return this.ForeColor;
        }

        private Color colorCollapseButtonBorder()
        {
            if (VisualStyleRenderer.IsSupported)
            {
                VisualStyleRenderer vsr;
                vsr = new VisualStyleRenderer(VisualStyleElement.ExplorerBar.NormalGroupHead.Normal);
                return vsr.GetColor(ColorProperty.BorderColorHint);
            }
            else
                return Color.Black;//Color.CadetBlue;
        }

        private void DrawCollapseButton(Graphics g)//, Rectangle rectangle)
        {
            System.Drawing.Drawing2D.SmoothingMode sm;
            Rectangle buttonRect;
            bool reverse;
            Point baseLoc;
            int top;
            Point pt;
            Pen p;

            reverse = false;// MiscFunctions.GetRightToLeftValue(this.m_buddyFrame);

            //
            // Set up a nice anti-aliased pen with which we will draw this.
            //
            sm = g.SmoothingMode;
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            p = new Pen(colorCollapseButtonBorder(), 1.5f);

            //
            // figure out where to draw the circle.
            // 
            top = 0;//this.Height - this.Font.Height;
            if (reverse == false)
            {
                buttonRect = new Rectangle(this.Width - COLLAPSE_BUTTON_DIAMETER - 6, ((int)(top + (this.Font.Height - COLLAPSE_BUTTON_DIAMETER) / 2 + 4)), COLLAPSE_BUTTON_DIAMETER, COLLAPSE_BUTTON_DIAMETER);

                baseLoc = new Point(this.Width - COLLAPSE_BUTTON_DIAMETER - 6, ((int)(top + (this.Font.Height - COLLAPSE_BUTTON_DIAMETER) / 2 + 4)));

            }
            else
            {
                buttonRect = new Rectangle(5, ((int)(top + (this.Font.Height - COLLAPSE_BUTTON_DIAMETER) / 2 - 1)), COLLAPSE_BUTTON_DIAMETER, COLLAPSE_BUTTON_DIAMETER);

                baseLoc = new Point(5, ((int)(top + (this.Font.Height - COLLAPSE_BUTTON_DIAMETER) / 2 - 1)));
            }

            //g.FillEllipse(new SolidBrush(this.m_buddyFrame.CaptionBlend.StartColor), buttonRect);
            g.FillEllipse(new SolidBrush(Color.White), buttonRect);
            g.DrawEllipse(p, buttonRect);
            p.Dispose();

            //
            // now draw the little chevrons
            //
            if (! this.IsExpanded)
            {
                p = new Pen(colorCollapseButtonChevrons(), 1.5f);
                pt = new Point(((int)(baseLoc.X + (COLLAPSE_BUTTON_DIAMETER / 2))), ((int)(baseLoc.Y + (COLLAPSE_BUTTON_DIAMETER / 2))));
                g.DrawLine(p, pt, new Point(pt.X - 3, pt.Y - 3));
                g.DrawLine(p, pt, new Point(pt.X + 3, pt.Y - 3));
                pt = new Point(((int)(baseLoc.X + (COLLAPSE_BUTTON_DIAMETER / 2))), ((int)(baseLoc.Y + (COLLAPSE_BUTTON_DIAMETER / 2) + 4)));
                g.DrawLine(p, pt, new Point(pt.X - 3, pt.Y - 3));
                g.DrawLine(p, pt, new Point(pt.X + 3, pt.Y - 3));

            }
            else
            {
                p = new Pen(colorCollapseButtonChevrons(), 1.5f);
                pt = new Point(((int)(baseLoc.X + (COLLAPSE_BUTTON_DIAMETER / 2))), ((int)(baseLoc.Y + (COLLAPSE_BUTTON_DIAMETER / 2) - 4)));
                g.DrawLine(p, pt, new Point(pt.X - 3, pt.Y + 3));
                g.DrawLine(p, pt, new Point(pt.X + 3, pt.Y + 3));
                pt = new Point(((int)(baseLoc.X + (COLLAPSE_BUTTON_DIAMETER / 2))), ((int)(baseLoc.Y + (COLLAPSE_BUTTON_DIAMETER / 2))));
                g.DrawLine(p, pt, new Point(pt.X - 3, pt.Y + 3));
                g.DrawLine(p, pt, new Point(pt.X + 3, pt.Y + 3));
            }

            //
            // Restore this
            //
            g.SmoothingMode = sm;
        }
        /// <summary>
        /// Toggles the Expansion state of the Subpane.
        /// </summary>
        private void ToggleExpansion()
        {
            if (this.IsExpanded)
                this.Collapse();
            else
                this.Expand();
        }

        /// <summary>
        /// Gets the Top Level Parent for this Subpane.
        /// </summary>
        /// <returns>The top level parent for the current subpane.</returns>
        private Control GetTopLevelParent()
        {
            Control parent = this.Parent;
            while (parent != null && parent.Parent != null)
            {
                parent = parent.Parent;
            }
            return parent;
        }

        /// <summary>
        /// Gets the last item in this Subpane's Tab sequence.
        /// </summary>
        /// <returns>The last control in this subpane's tab sequence.</returns>
        private Control GetLastControl()
        {
            Control control = this;
            int maxTabIndex = this.TabIndex;
            foreach (Control c in this.Controls)
            {
                if (c.TabStop && c.TabIndex > maxTabIndex)
                {
                    control = c;
                    maxTabIndex = c.TabIndex;
                }
            }
            return control;
        }
        #endregion

        #region Static Helper Methods
        /// <summary>
        /// Create a GraphicsPath for the rectangle with its upper corners rounded.
        /// </summary>
        /// <param name="rect">The rectangle to create a GraphicsPath for.</param>
        /// <returns>A GraphicsPath for the rectangle with its upper corners rounded</returns>
        private static GraphicsPath GetPath(RectangleF rect)
        {
            //Debug.Assert(rect != null);

            GraphicsPath path = new GraphicsPath();
            path.StartFigure();
            path.AddArc(new RectangleF(rect.X, rect.Y, 2 * curveRadius,
                2 * curveRadius), 180, 90);
            path.AddLine(new PointF(rect.X + curveRadius, rect.Y),
                new PointF(rect.X + rect.Width - curveRadius, rect.Y));
            path.AddArc(new RectangleF(rect.X + rect.Width - 2 * curveRadius,
                rect.Y, 2 * curveRadius, 2 * curveRadius), 270, 90);
            path.AddLine(new PointF(rect.X + rect.Width, rect.Y + curveRadius),
                new PointF(rect.X + rect.Width, rect.Y + rect.Height));
            path.AddLine(new PointF(rect.X + rect.Width, rect.Y + rect.Height),
                new PointF(rect.X, rect.Y + rect.Height));
            path.AddLine(new PointF(rect.X, rect.Y + rect.Height - curveRadius),
                new PointF(rect.X, rect.Y + curveRadius));
            path.CloseFigure();
            return path;
        }

        /// <summary>
        /// Determines if the current user's preferences are set for scrolling menus.
        /// </summary>
        /// <returns>True if the current user's menus are scroll animated; otherwise false.</returns>
        private static bool IsUserMenuScroll()
        {
            bool isMenuAnimationSet = false;
            NativeMethods.SystemParametersInfo(NativeMethods.SPI_GETMENUANIMATION,
                0, ref isMenuAnimationSet, 0);
            if (isMenuAnimationSet)
            {
                bool isMenuFadeAnimation = false;
                NativeMethods.SystemParametersInfo(NativeMethods.SPI_GETMENUFADE,
                    0, ref isMenuFadeAnimation, 0);
                return !isMenuFadeAnimation;
            }
            return false;
        }
        #endregion
    }
}
