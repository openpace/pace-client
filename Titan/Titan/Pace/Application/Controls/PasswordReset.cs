#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Threading;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    internal partial class PasswordReset : UserControl
    {

        private string _username;
               
        #region Constructor & Control Load

        public PasswordReset()
        {
            InitializeComponent();
            loadUi();
        }

        #endregion Constructor & Control Load

        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                //Get the labels from the localized .resx file.
                Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                lblUserId.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserLogon.UsernameLabelText");


            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Control KeyDown Handler.
        /// </summary>
        private void TextControlKeyDown(object sender, KeyEventArgs e)
        {
            ControlKeyPress(e);
        }

        /// <summary>
        /// Method to check if the keypress was an enter key and if the ok button is enabled, 
        /// click it for the user.
        /// </summary>
        /// <param name="e">KeyEventArgs</param>
        private void ControlKeyPress(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmdOk.Enabled)
                {
                    cmdOk.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                cmdCancel.PerformClick();
            }
        }

        /// <summary>
        /// Called when text changes in user name field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtUserid_TextChanged(object sender, EventArgs e)
        {

            string userId = txtUserid.Text.Trim();

            if (!String.IsNullOrEmpty(userId))
            {
                cmdOk.Enabled = true;

            }
            else
            {
                cmdOk.Enabled = false;
            }
        }

        /// <summary>
        /// Called when user clicks OK Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            _username = txtUserid.Text.Trim();
        }

        /// <summary>
        /// Set/get username
        /// </summary>
        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;

                if (!String.IsNullOrEmpty(_username))
                {

                    txtUserid.Text = _username;
                    cmdOk.Enabled = true;

                }

            }
        }

    }
}
