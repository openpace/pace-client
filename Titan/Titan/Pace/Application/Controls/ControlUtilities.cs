#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    internal partial class ControlUtilities : UserControl
    {
        public ControlUtilities()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Create a size structure that will fit the text string.
        /// </summary>
        /// <param name="text">text to figure out a size structure of for</param>
        /// <param name="textboxLeft">Left location of the text box</param>
        /// <param name="textboxTop">Top location of the text box</param>
        /// <param name="textboxHeight">Height of the text box</param>
        /// <param name="fontName">Name of the font used in the text box</param>
        /// <param name="fontSize">Size of the font used in the text box</param>
        /// <returns>a size structure with the size that will fit the text</returns>
        public Size GetTextBoxSize(string text, double textboxLeft, double textboxTop,
            double textboxHeight, string fontName, float fontSize)
        {
            Size sz = new Size();
            try
            {
                //double screenWidth = Screen.PrimaryScreen.Bounds.Width;
                //double screenHeight = Screen.PrimaryScreen.Bounds.Height;
                Font stringFont = new Font(fontName, fontSize);

                //double maxWidth = screenWidth - textboxLeft;
                //double maxHeigth = screenHeight - (textboxTop + textboxHeight);

                string[] s = text.Split(
                    new string[] { PafAppConstants.NL },
                    StringSplitOptions.RemoveEmptyEntries);

                if (s.Length > 0)
                {
                    //get the max line length.
                    int maxLineLen = 0;
                    int maxLen = 0;
                    for (int i = 0; i < s.Length; i++)
                    {
                        if (s[i].Trim().Length > maxLen)
                        {
                            maxLen = s[i].Length;
                            maxLineLen = i;
                        }
                    }

                    SizeF stringSize;
                    using (Graphics graphics = txtTextBox.CreateGraphics())
                    {
                        stringSize = graphics.MeasureString(s[maxLineLen], stringFont);
                    }

                    sz.Width = (int)Math.Round(stringSize.Width, 0);
                    sz.Height = (int)Math.Round(stringSize.Height * s.Length, 0);
                }
                else
                {
                    sz.Width = PafAppConstants.DEFAULT_CELL_NOTE_WIDTH;
                    sz.Height = PafAppConstants.DEFAULT_CELL_NOTE_HEIGHT;
                }
            }
            catch (Exception)
            {
                sz.Width = PafAppConstants.DEFAULT_CELL_NOTE_WIDTH;
                sz.Height = PafAppConstants.DEFAULT_CELL_NOTE_HEIGHT;
            }
            return sz;
        }
    }
}
