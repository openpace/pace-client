#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using DevExpress.XtraTreeList.ViewInfo;
using Titan.Pace.Application.Controls.TreeView;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Base.Mdb;
using Titan.Pace.DataStructures;
using FilterCondition = DevExpress.XtraTreeList.FilterCondition;

namespace Titan.Pace.Application.Controls
{
    internal partial class DimensionTree : UserControl
    {
        #region Variables

        private const int ImagePad = 5;
        private const int DefaultImageWidth = 12 + ImagePad;
        private TreeListNode _savedFocused;
        private string _promptString;
        private string _memeberNameLabel;
        private bool _hasGroupings;
        private string _clear;
        private string _selections;
        private string _save;
        private string _update;


        public TreeListViewState TreeListState { get; set; }

        /// <summary>
        /// Custom event for the subpane before expand.  Occurs before the subpane is expanded.
        /// </summary>
        public event Subpane_BeforeExpand SubpaneBeforeExpand;

        /// <summary>
        /// Custom event for the subpane after expand.  Occurs after the subpane has been expanded.
        /// </summary>
        public event Subpane_AfterExpand SubpaneAfterExpand;

        /// <summary>
        /// Custom event for the changing of the saved selections.
        /// </summary>
        public event SavedSelectionsChanged SavedSelectionsChangedEvent;
        
        /// <summary>
        /// Represents the method that will handle the SubpaneBeforeExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_BeforeExpand(object sender, CancelEventArgs e, string controlName);

        /// <summary>
        /// Represents the method that will handle the SubpaneAfterExpand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="controlName">The name of the control.</param>
        public delegate void Subpane_AfterExpand(object sender, EventArgs e, string controlName);

        /// <summary>
        /// Represents the method that will handle when the saved selections are updated.
        /// </summary>
        /// <param name="sender"></param>
        public delegate void SavedSelectionsChanged(object sender, SavedSelectionsChangedEventArgs e);

        #endregion Variables

        #region Properties
        /// <summary>
        /// Gets/sets the visible property of the control.
        /// </summary>
        public bool ControlIsVisible
        {
            get { return subpane.Visible; }
            set { subpane.Visible = value;  }
        }

        /// <summary>
        /// Name of the dimension
        /// </summary>
        public string DimensionName { get; set; }

        /// <summary>
        /// User selection id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Is the dimension is a base dim.
        /// </summary>
        public bool BaseDimension { get; set; }

        /// <summary>
        /// Level zero only.
        /// </summary>
        public bool ZeroLevelOnly { get; set; }

        /// <summary>
        /// List of alias tables.
        /// </summary>
        public List<string> AliasTableNames { get; set; }

        /// <summary>
        /// True to set a filter on the tree.
        /// </summary>
        public bool FilterTree { get; set; }

        /// <summary>
        /// Name of the column to display in the tree.
        /// </summary>
        public string DisplayColumn { get; set; }

        /// <summary>
        /// True if the show auto rows is enabled
        /// </summary>
        public bool UserFilteredEnabled { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string MemberNameColumn { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string GenerationColumnName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LevelColumnName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string VisibleColumnName { get; set; }

        /// <summary>
        /// List of generation info objects
        /// </summary>
        public List<DimLevelGenInfo> GenInfo { get; set; }

        /// <summary>
        /// List of level info objects
        /// </summary>
        public List<DimLevelGenInfo> LevelInfo { get; set; }

        /// <summary>
        /// Enable the level right click menu option.
        /// </summary>
        public bool EnableLevelRightClick { get; set; }

        /// <summary>
        /// Enable the generation right click menu option.
        /// </summary>
        public bool EnableGenerationRightClick { get; set; }

        /// <summary>
        /// parent first value (sent from server)
        /// </summary>
        public bool ParentFirst { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public SavedFilterSelections SavedSelections { get; set; }

        /// <summary>
        /// Text of the error
        /// </summary>
        public string ErrorDisplayText
        {
            get { return ErrorIndicator.HeaderText; }
            set { ErrorIndicator.HeaderText = value; }
            
        }

        /// <summary>
        /// Text of the error popup
        /// </summary>
        public string ErrorPopupText
        {
            get { return ErrorIndicator.ErrorPopupText; }
            set { ErrorIndicator.ErrorPopupText = value; }

        }

        /// <summary>
        /// Icon for the error popup
        /// </summary>
        public ErrorType ErrorPopupIcon
        {
            get { return ErrorIndicator.ErrorIcon; }
            set { ErrorIndicator.ErrorIcon = value; }

        }

        /// <summary>
        /// True if this user selection is tied to a view with groups
        /// </summary>
        public bool HasGroupings
        {
            get { return _hasGroupings; }
            set
            {
                if (_hasGroupings == value) return;
                _hasGroupings = value;
                PerformGroupValidation();
            }
        }

        /// <summary>
        /// Axis location of the user selector.
        /// </summary>
        public ViewAxis ViewAxis { get; set; }

        /// <summary>
        /// Is Parent First checked.
        /// </summary>
        public bool IsParentFirst
        {
            get
            {
                //If the item is hidden, then return true which mimicks legacy functionality
                return barCheckItemParentFirst.Visibility == BarItemVisibility.Never || barCheckItemParentFirst.Checked;
            }
        }

        /// <summary>
        /// ToolTip to display when not hovering over a node.
        /// </summary>
        public string PromptString
        {
            get { return _promptString; }
            set
            {
                _promptString = value;
                toolTipController2.SetToolTip(subpane, _promptString);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public bool IsExpanded
        {
            get
            {
                return subpane.IsExpanded;
            }
        }

        #endregion Properties

        #region Private

        private void AddBarItem(BarManager barManager, BarSubItem parent, string caption, object tag, bool isLevel)
        {
            BarItem item = new BarButtonItem(barManager, caption);
            item.Tag = tag;
            if (isLevel)
            {
                item.ItemClick += item_ItemClick_Level;
            }
            else
            {
                item.ItemClick += item_ItemClick_Generation;
            } 
            parent.AddItem(item);
        }


        #endregion Private

        #region Constructor & Control Load

        /// <summary>
        /// Constructor.
        /// </summary>
        public DimensionTree()
        {
            InitializeComponent();

            subpane.AfterCollapse += subpane1_AfterCollapse;
            subpane.AfterExpand += subpane1_AfterExpand;
            TreeList.AfterCheckNode += TreeList_AfterCheckNode;
            barButtonItemClearAll.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ClearAllCheckboxes.Text");
            barButtonItemSelectAll.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SelectAllCheckboxes.Text");
            barButtonItemAllDes.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SelectAllDescendants.Text");
            barButtonItemAllChildren.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SelectAllChildren.Text");
            barButtonItemAllGrandChildren.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.SelectAllGrandchildren.Text");
            barButtonItemAutoFilter.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ShowAutoFilterRow.Text");
            barButtonItemAllAnc.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Ancestors.Text");
            barSubItemCheckGeneration.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.CheckGeneration.Text");
            barSubItemCheckLevel.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.CheckLevel.Text");
            barCheckItemParentFirst.Caption =PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ParentFirst.Text");
            //barButtonItemUpdateSelection.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.ClearAll");
            _clear = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.Clear");
            _selections = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.RoleFilter.Selections");
            _save = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Save");
            _update = PafApp.GetLocalization().GetResourceManager().GetString("Application.Update");
            SetErrorBannerStatus(false);
        }


        #endregion Constructor & Control Load
 
        #region Events

        /// <summary>
        /// Performs the tree validation and enable warning if necessary.
        /// </summary>
        private void PerformGroupValidation()
        {
            var tree = TreeList;
            if (tree.CheckedNodesCount() < 2 || !HasGroupings || ViewAxis == ViewAxis.Page)
            {
                if (!ErrorIndicator.Visible) return;
                SetErrorBannerStatus(false);
                return;
            }

            SetErrorBannerStatus(HasGroupings);
        }

        /// <summary>
        /// Event to fire after a node is checked in the tree list.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeList_AfterCheckNode(object sender, NodeEventArgs e)
        {
            PerformGroupValidation();
        }

        /// <summary>
        /// Event handler for the tree view before expanding.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_BeforeExpanding(object sender, CancelEventArgs e)
        {
            if(SubpaneBeforeExpand != null)
                SubpaneBeforeExpand(sender, e, this.Name);
        }

        /// <summary>
        /// Event handler for the tree view after expand event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane_AfterExpand(object sender, EventArgs e)
        {
            if (SubpaneAfterExpand != null)
                SubpaneAfterExpand(sender, e, this.Name);
        }

        private void barButtonItemAutoFilter_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (TreeList.OptionsView.ShowAutoFilterRow)
            {
                barButtonItemAutoFilter.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ShowAutoFilterRow.Text");
                if (!FilterTree)
                {
                    TreeList.OptionsBehavior.EnableFiltering = false;
                }
                TreeList.OptionsView.ShowAutoFilterRow = false;
                TreeList.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Never;
            }
            else
            {
                barButtonItemAutoFilter.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.HideAutoFilterRow.Text");
                TreeList.OptionsBehavior.EnableFiltering = true;
                TreeList.OptionsView.ShowAutoFilterRow = true;
                TreeList.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Default;
            }
            UserFilteredEnabled = TreeList.OptionsView.ShowAutoFilterRow;
        }

        private void barButtonItemClearAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeList.UncheckAllValid();
            ValidateTree();
            if (SavedSelectionsChangedEvent != null)
            {
                SavedSelectionsChangedEvent(this, new SavedSelectionsChangedEventArgs(Id, SavedCleared.Cleared, allDimensions: false));
            }
        }

        private void barButtonItemSelectAll_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeList.CheckAllValid();
            ValidateTree();
        }

        private void barButtonItemAllDes_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeList.CheckAllValidDescendants(_savedFocused.Nodes);
            ValidateTree();
        }

        private void barButtonItemAllChildren_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeList.CheckAllValidChildren(_savedFocused.Nodes);
            ValidateTree();
        }

        private void barButtonItemAllGrandChildren_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeList.CheckAllValidGrandChildren(_savedFocused.Nodes);
            ValidateTree();
        }

        private void barButtonItemAllAnc_ItemClick(object sender, ItemClickEventArgs e)
        {
            TreeList.CheckAllValidAncestors(_savedFocused);
            ValidateTree();
        }

        private void popupMenu1_BeforePopup(object sender, CancelEventArgs e)
        {
            bool multiSelect = TreeList.MultiCheck;
            bool hasDescendant = false;
            bool hasChildren = false;
            bool hasParent = false;
            bool savedFoucs = false;
            DataTable dataTable = (DataTable)TreeList.DataSource;

            if (_savedFocused != null)
            {
                savedFoucs = true;
                hasDescendant = TreeList.HasVisibleDescendant(_savedFocused.Nodes);
                hasChildren = TreeList.HasVisibleChild(_savedFocused.Nodes);
                hasParent = TreeList.HasVisibleAncestor(_savedFocused);
            }

            //TTN-2549
            bool status = PafApp.GetPafAppSettingsHelper().PersistUserSelectionSelectionsEnabled();
            BarItemVisibility vis = BarItemVisibility.Always;
            if (!status)
            {
                vis = BarItemVisibility.Never;
            }

            string id = String.Join("", Id.Take(8));
            if (Id.Length > 8)
            {
                id = String.Format("{0}{1}", id.Trim(), "...");
            }

            barButtonItemClear.Caption = String.Format("{0} {1} ({2}) {3}", _clear, DimensionName, id, _selections);
            barButtonItemSaveSelections.Caption = String.Format("{0} {1} ({2}) {3}", _save, DimensionName, id, _selections);
            barButtonItemUpdateSelection.Caption = String.Format("{0} {1} ({2}) {3}", _update, DimensionName, id, _selections);

            barButtonItemClear.Enabled = SavedSelections.SavedSelectionsDictionary.ContainsKey(Id);
            //barButtonItemUpdateSelection.Enabled = SavedSelections.SavedSelectionsDictionary.Count > 0;
            barButtonItemUpdateSelection.Enabled = true;
            barButtonItemSaveSelections.Enabled = true;

            barButtonItemClear.Visibility = vis;
            barButtonItemUpdateSelection.Visibility = vis;
            barButtonItemSaveSelections.Visibility = vis;

            //END TTN-2549


            //Set ParentFirst to visible only if the item is multi select & not level zero only
            barCheckItemParentFirst.Visibility = (multiSelect && !ZeroLevelOnly) ? BarItemVisibility.Always : BarItemVisibility.Never;
            //Set the ParentFirst to checked.
            barCheckItemParentFirst.Checked = (ParentFirst && multiSelect);

            barButtonItemSelectAll.Enabled = multiSelect;
            barButtonItemAllDes.Enabled = (multiSelect && hasDescendant);
            barButtonItemAllChildren.Enabled = (multiSelect && hasChildren);
            barButtonItemAllAnc.Enabled = (multiSelect && hasParent);

            
            barSubItemCheckGeneration.Visibility = EnableGenerationRightClick ? BarItemVisibility.Always : BarItemVisibility.Never;
            barSubItemCheckLevel.Visibility = EnableLevelRightClick ? BarItemVisibility.Always : BarItemVisibility.Never;

            barSubItemCheckGeneration.ClearLinks();
            barSubItemCheckLevel.ClearLinks();

            if (savedFoucs && EnableGenerationRightClick)
            {
                int generation = Convert.ToInt32(_savedFocused.GetValue(GenerationColumnName));
                var genResults = (from x in dataTable.AsEnumerable()
                                  where x.Field<byte>(VisibleColumnName) == 1 && x.Field<int>(GenerationColumnName) > generation
                                  select new { Level = x.Field<int>(GenerationColumnName) }).Distinct().OrderByDescending(x => x.Level).ToList().Select(v => v.Level).ToList();

                if (GenInfo != null)
                {
                    foreach (var info in GenInfo)
                    {
                        if (!TreeList.IsGenerationValid(info.Number)) continue;
                        if (!genResults.Contains(info.Number)) continue;
                        AddBarItem(barManager1, barSubItemCheckGeneration, info.Name, info.Number, false);
                    }
                }
                else
                {
                    foreach (var result in genResults)
                    {
                        if (!TreeList.IsGenerationValid(result)) continue;
                        AddBarItem(barManager1, barSubItemCheckGeneration, Convert.ToString(result), result, false);
                    }
                }

                if (barSubItemCheckGeneration.ItemLinks.Count == 0)barSubItemCheckGeneration.Visibility = BarItemVisibility.Never;
            }


            if (savedFoucs && EnableLevelRightClick)
            {
                int level = Convert.ToInt32(_savedFocused.GetValue(LevelColumnName));
                var levelResults = (from x in dataTable.AsEnumerable()
                                    where x.Field<byte>(VisibleColumnName) == 1 && x.Field<int>(LevelColumnName) < level
                                    select new {Level = x.Field<int>(LevelColumnName)}).Distinct().OrderByDescending(x => x.Level).ToList().Select(v => v.Level).ToList();

                if (LevelInfo != null)
                {
                    foreach (var info in LevelInfo)
                    {
                        if (!TreeList.IsLevelValid(info.Number)) continue;
                        if (!levelResults.Contains(info.Number)) continue;
                        AddBarItem(barManager1, barSubItemCheckLevel, info.Name, info.Number, true);
                    }
                }
                else
                {
                    foreach (var result in levelResults)
                    {
                        if (!TreeList.IsLevelValid(result)) continue;
                        AddBarItem(barManager1, barSubItemCheckLevel, Convert.ToString(result), result, true);
                    }
                }
                if (barSubItemCheckLevel.ItemLinks.Count == 0)barSubItemCheckLevel.Visibility = BarItemVisibility.Never;
            }
        }

        private void item_ItemClick_Level(object sender, ItemClickEventArgs e)
        {
            int level = Convert.ToInt32(e.Item.Tag);
            TreeList.CheckAllNodesAtLevel(_savedFocused.Nodes, LevelColumnName, level);
            ValidateTree();
        }

        private void item_ItemClick_Generation(object sender, ItemClickEventArgs e)
        {
            int generation = Convert.ToInt32(e.Item.Tag);
            TreeList.CheckAllNodesAtGeneration(_savedFocused.Nodes, GenerationColumnName, generation);
            ValidateTree();

        }

        private void treeView1_KeyDown(object sender, KeyEventArgs e)
        {
            Keys CtrlShiftG = Keys.Control | Keys.F;
            if ((e.KeyData & CtrlShiftG) == CtrlShiftG)
            {
                barButtonItemAutoFilter.PerformClick();
            }
        }

        private void barCheckItemParentFirst_ItemClick(object sender, ItemClickEventArgs e)
        {
            BarCheckItem item = (BarCheckItem) e.Item;
            ParentFirst = item.Checked;
        }

        private void barButtonItemSaveSelections_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (SavedSelectionsChangedEvent != null)
            {
                SavedSelectionsChangedEvent(this, new SavedSelectionsChangedEventArgs(Id, SavedCleared.Saved, TreeList.GetCheckedNodes(), false));
            }
        }

        private void barButtonItemClear_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (SavedSelectionsChangedEvent != null)
            {
                SavedSelectionsChangedEvent(this, new SavedSelectionsChangedEventArgs(Id, SavedCleared.Cleared, allDimensions: false));
            }
        }

        private void barButtonItemUpdateSelection_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (SavedSelectionsChangedEvent != null)
            {
                SavedSelectionsChangedEvent(this, new SavedSelectionsChangedEventArgs(Id, SavedCleared.Updated, TreeList.GetCheckedNodes(), false));
            }
        }

        private void toolTipController1_GetActiveObjectInfo(object sender, global::DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventArgs e)
        {
            if (!(e.SelectedControl is TreeList)) return;

            if (popupMenu1.Visible) return;

            if (AliasTableNames == null || AliasTableNames.Count == 0) return;

            TreeList tree = (TreeList)e.SelectedControl;

            TreeListHitInfo hit = tree.CalcHitInfo(e.ControlMousePosition);

            if (hit.HitInfoType == HitInfoType.Cell)
            {
                TreeListNode node = hit.Node;

                object cellInfo = new TreeListCellToolTipInfo(node, hit.Column, null);

                if (String.IsNullOrWhiteSpace(_memeberNameLabel))
                {
                    _memeberNameLabel = MemberNameColumn.AddSpacesToSentence();
                }

                StringBuilder sb = new StringBuilder();
                if (DisplayColumn != MemberNameColumn)
                {
                    sb.Append(_memeberNameLabel).Append(": ").Append(node[MemberNameColumn]).AppendLine();
                }


                foreach (string table in AliasTableNames)
                {
                    if (table == DisplayColumn) continue;
                    sb.Append(table).Append(": ").Append(node[table]).AppendLine();
                }

                e.Info = new global::DevExpress.Utils.ToolTipControlInfo(cellInfo, sb.ToString());

            }
            else
            {
                e.Info = new global::DevExpress.Utils.ToolTipControlInfo(e.SelectedControl, PromptString);
            }
        }
        

        #endregion Events

        #region Public

        /// <summary>
        /// Validates the checked nodes.
        /// </summary>
        public void ValidateTree()
        {
            PerformGroupValidation();
        }

        /// <summary>
        /// Applies a filter based on the "Visible" Column
        /// </summary>
        public void ApplyFilter()
        {
            if (FilterTree)
            {
                TreeList.OptionsBehavior.EnableFiltering = true;
                TreeList.FilterConditions.Clear();
                //TTN-2385
                TreeList.ExpandAll();
                //TreeList.ActiveFilterEnabled = true;
                //TreeList.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Default;
                //TreeList.AddFilter(new BinaryOperator(VisibleColumnName, 1));
                TreeList.FilterConditions.Add(new FilterCondition(FilterConditionEnum.Equals, TreeList.Columns[VisibleColumnName], 0));
            }
            else
            {
                TreeList.FilterConditions.Clear();
                TreeList.OptionsView.ShowFilterPanelMode = ShowFilterPanelMode.Default;
            }
        }

        private void SetErrorBannerStatus(bool visible)
        {
            
            if (visible)
            {
                ErrorDisplayText = String.Empty;
                ErrorIndicator.PerformValidation();
                int imageWidth = DefaultImageWidth;
                if (ErrorIndicator.ValidationEditor.ErrorIcon != null)
                {
                    imageWidth = ErrorIndicator.ValidationEditor.ErrorIcon.Width + ImagePad;
                }
                TreeList.Dock = DockStyle.None;
                TreeList.Left = imageWidth;
                TreeList.Width = Width - imageWidth;
                TreeList.Anchor = (((((AnchorStyles.Top | AnchorStyles.Bottom) | AnchorStyles.Left) | AnchorStyles.Right)));
                //ErrorIndicator.Visible = true;
                //ErrorIndicator.Anchor = ((((AnchorStyles.Top | AnchorStyles.Left) | AnchorStyles.Right)));

            }
            else
            {
                ErrorDisplayText =  "_____";
                ErrorIndicator.PerformValidation();
                //ErrorIndicator.Visible = false;
                TreeList.Dock = DockStyle.Fill;
            }
        }


        #endregion Public

        #region Subpane Code
        #region Fields
        private event EventHandler afterExpandEvent;
        private event EventHandler afterCollapseEvent;
        #endregion        

        #region Public Methods
        public void Expand()
        {
            subpane.Expand();
        }

        public void Collapse()
        {
            subpane.Collapse();
        }
        #endregion

        #region Events

        public event EventHandler AfterExpand
        {
            add
            {
                afterExpandEvent += value;
            }
            remove
            {
                afterExpandEvent -= value;
            }
        }

        public event EventHandler AfterCollapse
        {
            add
            {
                afterCollapseEvent += value;
            }
            remove
            {
                afterCollapseEvent -= value;
            }
        }

        #endregion

        #region Event Firing Methods
        private void AfterExpandEvent()
        {
            if (this.afterExpandEvent != null)
                this.afterExpandEvent(this, EventArgs.Empty);
        }

        private void AfterCollapseEvent()
        {
            if (this.afterCollapseEvent != null)
                this.afterCollapseEvent(this, EventArgs.Empty);
        }
        #endregion

        #region Event Handlers

        void subpane1_AfterCollapse(object sender, EventArgs e)
        {
            AfterCollapseEvent();
        }

        void subpane1_AfterExpand(object sender, EventArgs e)
        {
            AfterExpandEvent();
        }


        private void subpane_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button != MouseButtons.Right) return;

            Point clickPoint = new Point(e.X, e.Y);
            Point screenPoint = subpane.PointToScreen(clickPoint);
            _savedFocused = null;
            popupMenu1.ShowPopup(screenPoint);
        }

        private void treeView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            TreeList tl = (TreeList)sender;
            Point clickPoint = new Point(e.Point.X, e.Point.Y);
            TreeListHitInfo ht = tl.CalcHitInfo(clickPoint);
            _savedFocused = ht.Node;
            popupMenu1.ShowPopup(tl.PointToScreen(e.Point));
        }
        #endregion Event Handlers

        

        #endregion Subpane Code


    }
}