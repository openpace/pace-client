﻿using DevExpress.XtraEditors;

namespace Titan.Pace.Application.Controls
{
    partial class SearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SearchControl));
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.cmdFind = new System.Windows.Forms.Button();
            this.txtFindMember = new global::DevExpress.XtraEditors.TextEdit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFindMember.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "Find.png");
            // 
            // cmdFind
            // 
            this.cmdFind.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdFind.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.cmdFind.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFind.ForeColor = System.Drawing.SystemColors.Control;
            this.cmdFind.Image = global::Titan.Properties.Resources.Find;
            this.cmdFind.Location = new System.Drawing.Point(158, 0);
            this.cmdFind.Margin = new System.Windows.Forms.Padding(0);
            this.cmdFind.Name = "cmdFind";
            this.cmdFind.Size = new System.Drawing.Size(22, 20);
            this.cmdFind.TabIndex = 29;
            this.cmdFind.UseVisualStyleBackColor = true;
            this.cmdFind.Click += new System.EventHandler(this.cmdFind_Click);
            this.cmdFind.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmdFind_KeyDown);
            this.cmdFind.Resize += new System.EventHandler(this.cmdFind_Resize);
            // 
            // txtFindMember
            // 
            this.txtFindMember.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFindMember.Location = new System.Drawing.Point(0, 0);
            this.txtFindMember.Name = "txtFindMember";
            this.txtFindMember.Properties.NullValuePromptShowForEmptyValue = true;
            this.txtFindMember.Size = new System.Drawing.Size(155, 20);
            this.txtFindMember.TabIndex = 30;
            this.txtFindMember.KeyDown += new System.Windows.Forms.KeyEventHandler(this.cmdFind_KeyDown);
            // 
            // SearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.txtFindMember);
            this.Controls.Add(this.cmdFind);
            this.Name = "SearchControl";
            this.Size = new System.Drawing.Size(184, 20);
            ((System.ComponentModel.ISupportInitialize)(this.txtFindMember.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private TextEdit txtFindMember;
        private System.Windows.Forms.Button cmdFind;
        private System.Windows.Forms.ImageList imageList1;
    }
}
