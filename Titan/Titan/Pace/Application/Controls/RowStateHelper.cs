#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;

namespace Titan.Pace.Application.Controls
{
    internal class RowStateHelper : Component
    {
        private readonly AppearanceObject _appearanceDisabledRow;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public AppearanceObject AppearanceDisabledRow
        {
            get { return _appearanceDisabledRow; }
        }

        private global::DevExpress.XtraGrid.Views.Grid.GridView _gridView;
        public global::DevExpress.XtraGrid.Views.Grid.GridView GridView
        {
            get { return _gridView; }
            set { UnSubscribeEvents(value); _gridView = value; SubscribeEvents(value); }
        }


        private readonly List<long> _disabledRows;

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public List<long> DisabledRows
        {
            get { return _disabledRows; }
        }


        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public string KeyColumnName { get; set; }

        [DesignerSerializationVisibility(DesignerSerializationVisibility.Content)]
        public string EnabledColumnName { get; set; }


        public RowStateHelper()
        {
            KeyColumnName = String.Empty;
            EnabledColumnName = String.Empty;
            _disabledRows = new List<long>();
            _appearanceDisabledRow = new AppearanceObject();
            AppearanceDisabledRow.Changed += _AppearanceDisabledRow_Changed;
        }

        private void UnSubscribeEvents(global::DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            view.RowCellStyle -= view_RowCellStyle;
            view.ShowingEditor -= view_ShowingEditor;

        }
        private void SubscribeEvents(global::DevExpress.XtraGrid.Views.Grid.GridView view)
        {
            view.RowCellStyle += view_RowCellStyle;
            view.ShowingEditor += view_ShowingEditor;
        }

        /// <summary>
        /// Add a disabled row.
        /// </summary>
        /// <param name="rowId"></param>
        public void DisableRow(long rowId)
        {
            DisabledRows.Add(rowId);
        }

        /// <summary>
        /// Gets disabled status of a row.
        /// </summary>
        /// <param name="rowId"></param>
        /// <returns></returns>
        public bool IsRowDisabled(long rowId)
        {
            return DisabledRows.Contains(rowId);
        }

        /// <summary>
        /// Enables a row (removes index from collection)
        /// </summary>
        /// <param name="rowId"></param>
        public void EnableRow(long rowId)
        {
            while (IsRowDisabled(rowId))
            {
                DisabledRows.Remove(rowId);
            }
        }


        private void view_ShowingEditor(object sender, CancelEventArgs e)
        {
            global::DevExpress.XtraGrid.Views.Grid.GridView gridView = (global::DevExpress.XtraGrid.Views.Grid.GridView)sender;
            var row = (DataRowView)gridView.GetRow(gridView.FocusedRowHandle);
            if (row == null) return;
            long key = Convert.ToInt64(row[KeyColumnName]);
            GridColumn gridColumn = gridView.FocusedColumn;
            if (gridColumn.Name.Equals(EnabledColumnName))
            {
                e.Cancel = true;
            }
            //else
            //{
            //    e.Cancel = IsRowDisabled(key);
            //}
        }

        private void view_RowCellStyle(object sender, RowCellStyleEventArgs e)
        {
            Console.WriteLine(DateTime.Now.Millisecond);
            if (!GridView.IsRowSelected(e.RowHandle))
            {
                var row = (DataRowView)GridView.GetRow(e.RowHandle);
                if (row == null) return;
                long key = Convert.ToInt64(row[KeyColumnName]);
                if (IsRowDisabled(key))
                {
                    e.Appearance.Assign(AppearanceDisabledRow);
                }
            }
        }

        private void _AppearanceDisabledRow_Changed(object sender, EventArgs e)
        {
            if (GridView == null) return;
            foreach (int t in DisabledRows)
            {
                GridView.RefreshRow(t);
            }
        }
    }
}
