using DevExpress.XtraBars;
using Titan.Pace.Application.Controls.TreeView;

namespace Titan.Pace.Application.Controls
{
    partial class DimensionTree
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DimensionTree));
            this.subpane = new Subpane();
            this.TreeList = new PaceDimensionXtraTree();
            this.toolTipController1 = new global::DevExpress.Utils.ToolTipController(this.components);
            this.ErrorIndicator = new Header();
            this.barManager1 = new global::DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new global::DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new global::DevExpress.XtraBars.BarDockControl();
            this.barButtonItemAutoFilter = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSelectAll = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemClearAll = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAllDes = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAllChildren = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAllGrandChildren = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemAllAnc = new global::DevExpress.XtraBars.BarButtonItem();
            this.barSubItem1 = new global::DevExpress.XtraBars.BarSubItem();
            this.barCheckItemParentFirst = new global::DevExpress.XtraBars.BarCheckItem();
            this.barSubItemCheckLevel = new global::DevExpress.XtraBars.BarSubItem();
            this.barSubItemCheckGeneration = new global::DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemSaveSelections = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemClear = new global::DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemUpdateSelection = new global::DevExpress.XtraBars.BarButtonItem();
            this.popupMenu1 = new global::DevExpress.XtraBars.PopupMenu(this.components);
            this.toolTipController2 = new global::DevExpress.Utils.ToolTipController(this.components);
            this.subpane.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // subpane
            // 
            this.subpane.AutoSize = true;
            this.subpane.BackColor = System.Drawing.Color.Transparent;
            this.subpane.Controls.Add(this.TreeList);
            this.subpane.Controls.Add(this.ErrorIndicator);
            this.subpane.DefaultExpandedHeight = 200;
            this.subpane.Dock = System.Windows.Forms.DockStyle.Top;
            this.subpane.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subpane.GradientColorBegin = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.GradientColorEnd = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.HighlightColor = System.Drawing.SystemColors.ControlText;
            this.subpane.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.subpane.IsAnimated = false;
            this.subpane.Location = new System.Drawing.Point(0, 0);
            this.subpane.Name = "subpane";
            this.subpane.Padding = new System.Windows.Forms.Padding(0);
            this.subpane.PanelColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.subpane.Size = new System.Drawing.Size(179, 198);
            this.subpane.TabIndex = 17;
            this.subpane.TabStop = true;
            this.subpane.Tag = "";
            this.subpane.Text = "Subpane";
            this.subpane.BeforeExpanding += new System.ComponentModel.CancelEventHandler(this.subpane_BeforeExpanding);
            this.subpane.AfterExpand += new System.EventHandler(this.subpane_AfterExpand);
            this.subpane.MouseDown += new System.Windows.Forms.MouseEventHandler(this.subpane_MouseDown);
            // 
            // TreeList
            // 
            this.TreeList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TreeList.DisplayColumnName = null;
            this.TreeList.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeList.GenerationColumnName = null;
            this.TreeList.LevelColumnName = null;
            this.TreeList.Location = new System.Drawing.Point(0, 24);
            this.TreeList.MemberColumnName = null;
            this.TreeList.MultiCheck = true;
            this.TreeList.Name = "TreeList";
            this.TreeList.OptionsBehavior.Editable = false;
            this.TreeList.OptionsBehavior.EnableFiltering = true;
            this.TreeList.OptionsFilter.FilterMode = global::DevExpress.XtraTreeList.FilterMode.Smart;
            this.TreeList.OptionsView.ShowCheckBoxes = true;
            this.TreeList.OptionsView.ShowColumns = false;
            this.TreeList.OptionsView.ShowHorzLines = false;
            this.TreeList.OptionsView.ShowIndicator = false;
            this.TreeList.OptionsView.ShowVertLines = false;
            this.TreeList.SelectableGenerations = ((System.Collections.Generic.List<int>)(resources.GetObject("TreeList.SelectableGenerations")));
            this.TreeList.SelectableLevels = ((System.Collections.Generic.List<int>)(resources.GetObject("TreeList.SelectableLevels")));
            this.TreeList.Size = new System.Drawing.Size(179, 174);
            this.TreeList.TabIndex = 1;
            this.TreeList.ToolTipController = this.toolTipController1;
            this.TreeList.UnselectableLevels = ((System.Collections.Generic.List<int>)(resources.GetObject("TreeList.UnselectableLevels")));
            this.TreeList.VisibleColumnName = null;
            this.TreeList.PopupMenuShowing += new global::DevExpress.XtraTreeList.PopupMenuShowingEventHandler(this.treeView1_PopupMenuShowing);
            this.TreeList.KeyDown += new System.Windows.Forms.KeyEventHandler(this.treeView1_KeyDown);
            // 
            // toolTipController1
            // 
            this.toolTipController1.GetActiveObjectInfo += new global::DevExpress.Utils.ToolTipControllerGetActiveObjectInfoEventHandler(this.toolTipController1_GetActiveObjectInfo);
            // 
            // ErrorIndicator
            // 
            this.ErrorIndicator.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ErrorIndicator.AutoSize = true;
            this.ErrorIndicator.ErrorIcon = global::DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning;
            this.ErrorIndicator.ErrorPopupText = "";
            this.ErrorIndicator.HeaderText = "Test";
            this.ErrorIndicator.Location = new System.Drawing.Point(0, 24);
            this.ErrorIndicator.Name = "ErrorIndicator";
            this.ErrorIndicator.Size = new System.Drawing.Size(179, 17);
            this.ErrorIndicator.TabIndex = 2;
            // 
            // barManager1
            // 
            this.barManager1.AllowCustomization = false;
            this.barManager1.AllowMoveBarOnToolbar = false;
            this.barManager1.AllowQuickCustomization = false;
            this.barManager1.AllowShowToolbarsPopup = false;
            this.barManager1.DockControls.Add(this.barDockControlTop);
            this.barManager1.DockControls.Add(this.barDockControlBottom);
            this.barManager1.DockControls.Add(this.barDockControlLeft);
            this.barManager1.DockControls.Add(this.barDockControlRight);
            this.barManager1.Form = this;
            this.barManager1.Items.AddRange(new global::DevExpress.XtraBars.BarItem[] {
            this.barButtonItemAutoFilter,
            this.barButtonItemSelectAll,
            this.barButtonItemClearAll,
            this.barButtonItemAllDes,
            this.barButtonItemAllChildren,
            this.barButtonItemAllGrandChildren,
            this.barButtonItemAllAnc,
            this.barSubItem1,
            this.barCheckItemParentFirst,
            this.barSubItemCheckLevel,
            this.barSubItemCheckGeneration,
            this.barButtonItem1,
            this.barButtonItemSaveSelections,
            this.barButtonItemClear,
            this.barButtonItemUpdateSelection});
            this.barManager1.MaxItemId = 15;
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(179, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 203);
            this.barDockControlBottom.Size = new System.Drawing.Size(179, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 203);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(179, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 203);
            // 
            // barButtonItemAutoFilter
            // 
            this.barButtonItemAutoFilter.Caption = "Show auto filter";
            this.barButtonItemAutoFilter.Id = 0;
            this.barButtonItemAutoFilter.ItemShortcut = new global::DevExpress.XtraBars.BarShortcut((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F));
            this.barButtonItemAutoFilter.Name = "barButtonItemAutoFilter";
            this.barButtonItemAutoFilter.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAutoFilter_ItemClick);
            // 
            // barButtonItemSelectAll
            // 
            this.barButtonItemSelectAll.Caption = "Select all";
            this.barButtonItemSelectAll.Id = 1;
            this.barButtonItemSelectAll.Name = "barButtonItemSelectAll";
            this.barButtonItemSelectAll.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSelectAll_ItemClick);
            // 
            // barButtonItemClearAll
            // 
            this.barButtonItemClearAll.Caption = "Clear all";
            this.barButtonItemClearAll.Id = 2;
            this.barButtonItemClearAll.Name = "barButtonItemClearAll";
            this.barButtonItemClearAll.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemClearAll_ItemClick);
            // 
            // barButtonItemAllDes
            // 
            this.barButtonItemAllDes.Caption = "Select all Descendants";
            this.barButtonItemAllDes.Id = 3;
            this.barButtonItemAllDes.Name = "barButtonItemAllDes";
            this.barButtonItemAllDes.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAllDes_ItemClick);
            // 
            // barButtonItemAllChildren
            // 
            this.barButtonItemAllChildren.Caption = "Select all Children";
            this.barButtonItemAllChildren.Id = 4;
            this.barButtonItemAllChildren.Name = "barButtonItemAllChildren";
            this.barButtonItemAllChildren.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAllChildren_ItemClick);
            // 
            // barButtonItemAllGrandChildren
            // 
            this.barButtonItemAllGrandChildren.Caption = "Select all Grandchildren";
            this.barButtonItemAllGrandChildren.Id = 5;
            this.barButtonItemAllGrandChildren.Name = "barButtonItemAllGrandChildren";
            this.barButtonItemAllGrandChildren.Visibility = global::DevExpress.XtraBars.BarItemVisibility.Never;
            this.barButtonItemAllGrandChildren.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAllGrandChildren_ItemClick);
            // 
            // barButtonItemAllAnc
            // 
            this.barButtonItemAllAnc.Caption = "Select all ancestors";
            this.barButtonItemAllAnc.Id = 6;
            this.barButtonItemAllAnc.Name = "barButtonItemAllAnc";
            this.barButtonItemAllAnc.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemAllAnc_ItemClick);
            // 
            // barSubItem1
            // 
            this.barSubItem1.Caption = "Check All";
            this.barSubItem1.Id = 7;
            this.barSubItem1.Name = "barSubItem1";
            // 
            // barCheckItemParentFirst
            // 
            this.barCheckItemParentFirst.Caption = "Parent";
            this.barCheckItemParentFirst.Checked = true;
            this.barCheckItemParentFirst.Id = 8;
            this.barCheckItemParentFirst.Name = "barCheckItemParentFirst";
            this.barCheckItemParentFirst.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItemParentFirst_ItemClick);
            // 
            // barSubItemCheckLevel
            // 
            this.barSubItemCheckLevel.Caption = "Level";
            this.barSubItemCheckLevel.Id = 9;
            this.barSubItemCheckLevel.Name = "barSubItemCheckLevel";
            // 
            // barSubItemCheckGeneration
            // 
            this.barSubItemCheckGeneration.Caption = "Generation";
            this.barSubItemCheckGeneration.Id = 10;
            this.barSubItemCheckGeneration.LinksPersistInfo.AddRange(new global::DevExpress.XtraBars.LinkPersistInfo[] {
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1)});
            this.barSubItemCheckGeneration.Name = "barSubItemCheckGeneration";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "barButtonItem1";
            this.barButtonItem1.Id = 11;
            this.barButtonItem1.Name = "barButtonItem1";
            // 
            // barButtonItemSaveSelections
            // 
            this.barButtonItemSaveSelections.Caption = "_";
            this.barButtonItemSaveSelections.Id = 12;
            this.barButtonItemSaveSelections.Name = "barButtonItemSaveSelections";
            this.barButtonItemSaveSelections.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemSaveSelections_ItemClick);
            // 
            // barButtonItemClear
            // 
            this.barButtonItemClear.Caption = "_";
            this.barButtonItemClear.Id = 13;
            this.barButtonItemClear.Name = "barButtonItemClear";
            this.barButtonItemClear.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemClear_ItemClick);
            // 
            // barButtonItemUpdateSelection
            // 
            this.barButtonItemUpdateSelection.Caption = "_";
            this.barButtonItemUpdateSelection.Id = 14;
            this.barButtonItemUpdateSelection.Name = "barButtonItemUpdateSelection";
            this.barButtonItemUpdateSelection.ItemClick += new global::DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemUpdateSelection_ItemClick);
            // 
            // popupMenu1
            // 
            this.popupMenu1.LinksPersistInfo.AddRange(new global::DevExpress.XtraBars.LinkPersistInfo[] {
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAutoFilter),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemSelectAll, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAllDes),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAllAnc),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAllChildren),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemAllGrandChildren),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barSubItemCheckGeneration, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barSubItemCheckLevel),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemClearAll, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemParentFirst, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemSaveSelections, true),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemUpdateSelection),
            new global::DevExpress.XtraBars.LinkPersistInfo(this.barButtonItemClear)});
            this.popupMenu1.Manager = this.barManager1;
            this.popupMenu1.Name = "popupMenu1";
            this.popupMenu1.BeforePopup += new System.ComponentModel.CancelEventHandler(this.popupMenu1_BeforePopup);
            // 
            // toolTipController2
            // 
            this.toolTipController2.AutoPopDelay = 500;
            // 
            // DimensionTree
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.subpane);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.DoubleBuffered = true;
            this.Name = "DimensionTree";
            this.Size = new System.Drawing.Size(179, 203);
            this.subpane.ResumeLayout(false);
            this.subpane.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        #endregion

        public Subpane subpane;
        public PaceDimensionXtraTree TreeList;
        private global::DevExpress.XtraBars.BarManager barManager1;
        private global::DevExpress.XtraBars.BarDockControl barDockControlTop;
        private global::DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private global::DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private global::DevExpress.XtraBars.BarDockControl barDockControlRight;
        private global::DevExpress.XtraBars.PopupMenu popupMenu1;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemAutoFilter;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemSelectAll;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemClearAll;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemAllDes;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemAllChildren;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemAllGrandChildren;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItemAllAnc;
        private global::DevExpress.Utils.ToolTipController toolTipController1;
        private global::DevExpress.Utils.ToolTipController toolTipController2;
        private global::DevExpress.XtraBars.BarSubItem barSubItem1;
        private global::DevExpress.XtraBars.BarCheckItem barCheckItemParentFirst;
        private global::DevExpress.XtraBars.BarSubItem barSubItemCheckLevel;
        private global::DevExpress.XtraBars.BarSubItem barSubItemCheckGeneration;
        private global::DevExpress.XtraBars.BarButtonItem barButtonItem1;
        public Header ErrorIndicator;
        private BarButtonItem barButtonItemSaveSelections;
        private BarButtonItem barButtonItemClear;
        private BarButtonItem barButtonItemUpdateSelection;

    }
}
