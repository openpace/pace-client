﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Runtime.InteropServices;

namespace Titan.Pace.Application.Controls.Microsoft
{
    #region LocalizedDescription Class
    /// <summary>
    /// Provides methods to localize the description text for properties.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Event, AllowMultiple = false, Inherited = true)]
    internal sealed class LocalizedDescriptionAttribute :
        System.ComponentModel.DescriptionAttribute
    {

        /// <summary>
        /// The resource table from the current assembly that 
        /// contains the localized description text.
        /// </summary>
        private string resourceTable;

        /// <summary>
        /// Creates a new instance of the LocalizedDescription attribute.
        /// </summary>
        /// <param name="description">The description for the property.</param>
        public LocalizedDescriptionAttribute(string description)
            : base(description)
        {
            this.resourceTable = String.Empty;
        }

        /// <summary>
        /// Creates a new instance of the LocalizedDescription attribute.
        /// </summary>
        /// <param name="description">
        /// The name of the entry that contains the  
        /// description in the resource table.
        /// </param>
        /// <param name="table">
        /// The resource table from the current assembly that 
        /// contains the localized description text.
        /// </param>
        public LocalizedDescriptionAttribute(string description, string table)
            : this(description)
        {
            this.resourceTable = table;
        }

        /// <summary>
        /// The resource table from the current assembly that 
        /// contains the localized description text.
        /// </summary>
        /// <value>
        /// Resource table from the current assembly that
        /// contains the localized description text.
        /// </value>
        public string Table
        {
            get
            {
                return this.resourceTable;
            }
            set
            {
                this.resourceTable = value;
            }
        }

        /// <summary>
        /// The localized description text for the property 
        /// associated with this attribute.
        /// </summary>
        /// <value>
        /// The localized description text for the property 
        /// associated with this attribute.
        /// </value>
        public override string Description
        {
            get
            {
                System.Resources.ResourceManager resourceManager =
                    new System.Resources.ResourceManager(this.resourceTable,
                    System.Reflection.Assembly.GetExecutingAssembly());
                try
                {
                    return resourceManager.GetString(this.DescriptionValue);
                }
                catch (InvalidOperationException)
                {
                    return this.DescriptionValue;
                }
                catch (System.Resources.MissingManifestResourceException)
                {
                    return this.DescriptionValue;
                }
            }
        }
    }
    #endregion

    internal static class NativeMethods
    {
        internal const int SPI_GETMENUANIMATION = 0x1002;
        internal const int SPI_GETMENUFADE = 0x1012;

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        internal static extern int
            SystemParametersInfo(int uAction, int uParam,
            ref bool lpvParam, int fuWinIni);

    }
}

