namespace Titan.Pace.Application.Controls
{
    partial class UserInformation
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.subpane = new Subpane();
            this.grpUserInformation = new System.Windows.Forms.GroupBox();
            this.lblUserId = new System.Windows.Forms.Label();
            this.lblUserIdLabel = new System.Windows.Forms.Label();
            this.lblSeasonCaption = new System.Windows.Forms.Label();
            this.lblSeasonCaptionLabel = new System.Windows.Forms.Label();
            this.lblPlanType = new System.Windows.Forms.Label();
            this.lblPlanTypeLabel = new System.Windows.Forms.Label();
            this.subpane.SuspendLayout();
            this.grpUserInformation.SuspendLayout();
            this.SuspendLayout();
            // 
            // subpane
            // 
            this.subpane.AutoSize = true;
            this.subpane.BackColor = System.Drawing.Color.Transparent;
            this.subpane.Controls.Add(this.grpUserInformation);
            this.subpane.DefaultExpandedHeight = 130;
            this.subpane.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.subpane.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subpane.GradientColorBegin = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.GradientColorEnd = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.HighlightColor = System.Drawing.SystemColors.ControlText;
            this.subpane.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.subpane.IsAnimated = false;
            this.subpane.Location = new System.Drawing.Point(0, 0);
            this.subpane.Name = "subpane";
            this.subpane.Padding = new System.Windows.Forms.Padding(0);
            this.subpane.PanelColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(243)))), ((int)(((byte)(247)))));
            this.subpane.Size = new System.Drawing.Size(179, 138);
            this.subpane.TabIndex = 17;
            this.subpane.TabStop = true;
            this.subpane.Tag = "";
            this.subpane.Text = "User Information";
            this.subpane.BeforeExpanding += new System.ComponentModel.CancelEventHandler(this.subpane_BeforeExpanding);
            this.subpane.AfterExpand += new System.EventHandler(this.subpane_AfterExpand);
            // 
            // grpUserInformation
            // 
            this.grpUserInformation.Controls.Add(this.lblUserId);
            this.grpUserInformation.Controls.Add(this.lblUserIdLabel);
            this.grpUserInformation.Controls.Add(this.lblSeasonCaption);
            this.grpUserInformation.Controls.Add(this.lblSeasonCaptionLabel);
            this.grpUserInformation.Controls.Add(this.lblPlanType);
            this.grpUserInformation.Controls.Add(this.lblPlanTypeLabel);
            this.grpUserInformation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpUserInformation.Location = new System.Drawing.Point(0, 21);
            this.grpUserInformation.Name = "grpUserInformation";
            this.grpUserInformation.Size = new System.Drawing.Size(179, 117);
            this.grpUserInformation.TabIndex = 1;
            this.grpUserInformation.TabStop = false;
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserId.Location = new System.Drawing.Point(15, 23);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(0, 12);
            this.lblUserId.TabIndex = 27;
            // 
            // lblUserIdLabel
            // 
            this.lblUserIdLabel.AutoSize = true;
            this.lblUserIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUserIdLabel.Location = new System.Drawing.Point(4, 10);
            this.lblUserIdLabel.Name = "lblUserIdLabel";
            this.lblUserIdLabel.Size = new System.Drawing.Size(50, 13);
            this.lblUserIdLabel.TabIndex = 26;
            this.lblUserIdLabel.Text = "User ID";
            // 
            // lblSeasonCaption
            // 
            this.lblSeasonCaption.AutoSize = true;
            this.lblSeasonCaption.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeasonCaption.Location = new System.Drawing.Point(17, 90);
            this.lblSeasonCaption.Name = "lblSeasonCaption";
            this.lblSeasonCaption.Size = new System.Drawing.Size(0, 12);
            this.lblSeasonCaption.TabIndex = 25;
            // 
            // lblSeasonCaptionLabel
            // 
            this.lblSeasonCaptionLabel.AutoSize = true;
            this.lblSeasonCaptionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSeasonCaptionLabel.Location = new System.Drawing.Point(6, 77);
            this.lblSeasonCaptionLabel.Name = "lblSeasonCaptionLabel";
            this.lblSeasonCaptionLabel.Size = new System.Drawing.Size(0, 13);
            this.lblSeasonCaptionLabel.TabIndex = 24;
            // 
            // lblPlanType
            // 
            this.lblPlanType.AutoSize = true;
            this.lblPlanType.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlanType.Location = new System.Drawing.Point(17, 56);
            this.lblPlanType.Name = "lblPlanType";
            this.lblPlanType.Size = new System.Drawing.Size(0, 12);
            this.lblPlanType.TabIndex = 23;
            // 
            // lblPlanTypeLabel
            // 
            this.lblPlanTypeLabel.AutoSize = true;
            this.lblPlanTypeLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlanTypeLabel.Location = new System.Drawing.Point(6, 43);
            this.lblPlanTypeLabel.Name = "lblPlanTypeLabel";
            this.lblPlanTypeLabel.Size = new System.Drawing.Size(0, 13);
            this.lblPlanTypeLabel.TabIndex = 22;
            // 
            // UserInformation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.Controls.Add(this.subpane);
            this.DoubleBuffered = true;
            this.Name = "UserInformation";
            this.Size = new System.Drawing.Size(179, 138);
            this.subpane.ResumeLayout(false);
            this.grpUserInformation.ResumeLayout(false);
            this.grpUserInformation.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Subpane subpane;
        private System.Windows.Forms.GroupBox grpUserInformation;
        private System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.Label lblSeasonCaption;
        private System.Windows.Forms.Label lblSeasonCaptionLabel;
        private System.Windows.Forms.Label lblPlanType;
        private System.Windows.Forms.Label lblPlanTypeLabel;
        public System.Windows.Forms.Label lblUserIdLabel;

    }
}
