#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Threading;
using System.Windows.Forms;

namespace Titan.Pace.Application.Controls
{
    internal partial class Header : UserControl
    {
        public Header()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Control load event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Header_Load(object sender, EventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
        }
        /// <summary>
        /// Gets/sets the text in the box. 
        /// </summary>
        public string HeaderText
        {
            get { return ValidationEditor.Text; }
            set { ValidationEditor.Text = value; }
        }

        /// <summary>
        /// Error icon for the popup
        /// </summary>
        public global::DevExpress.XtraEditors.DXErrorProvider.ErrorType ErrorIcon
        {
            get { return dxValidationProvider1.GetValidationRule(ValidationEditor).ErrorType; }
            set { dxValidationProvider1.GetValidationRule(ValidationEditor).ErrorType = value; }
        }

        /// <summary>
        /// Text to be displayed in the popup
        /// </summary>
        public string ErrorPopupText
        {
            get { return dxValidationProvider1.GetValidationRule(ValidationEditor).ErrorText; }
            set { dxValidationProvider1.GetValidationRule(ValidationEditor).ErrorText = value; }
        }

        /// <summary>
        /// Perform the validation for the box.
        /// </summary>
        public void PerformValidation()
        {
            dxValidationProvider1.Validate();
        }

    }
}
