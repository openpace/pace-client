#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Forms;
using Titan.Pace.Data;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;
using Range=Microsoft.Office.Interop.Excel.Range;

namespace Titan.Pace.Application
{
    internal interface IAppInterface
    {

        #region Methods
        /// <summary>
        /// Enables the buttons on the right click menu.
        /// </summary>
        void AddControlstoRightClickMenu();

        /// <summary>
        /// Adds a new worksheet to the active workbook.
        /// </summary>
        /// <param name="sheetName">The name of the new sheet.</param>
        /// <param name="normalizeSheetName">If true, then the sheetName parm will have 
        /// all invalid characters removed before it is set as the sheet name.</param>
        /// <param name="addSheetToBegining">If true, the new sheet will be 
        /// added to the begining of the workbook, if false it is added to the end.</param>
        /// <returns>The worksheet if the sheet was added; null if not.</returns>
        Worksheet AddNewWorksheet(string sheetName, bool normalizeSheetName, bool addSheetToBegining);

        /// <summary>
        /// Adds a new worksheet to the active workbook.
        /// </summary>
        /// <param name="wbk">The workbook to add the sheets to.</param>
        /// <param name="sheetName">The name of the new sheet.</param>
        /// <param name="normalizeSheetName">If true, then the sheetName parm will have 
        /// all invalid characters removed before it is set as the sheet name.</param>
        /// <param name="addSheetToBegining">If true, the new sheet will be 
        /// added to the begining of the workbook, if false it is added to the end.</param>
        /// <returns>The worksheet if the sheet was added; null if not.</returns>
        Worksheet AddNewWorksheet(Workbook wbk, string sheetName, bool normalizeSheetName, bool addSheetToBegining);

        /// <summary>
        /// Rename a worksheet to a new name.
        /// </summary>
        /// <param name="sheet">The worksheet object to rename.</param>
        /// <param name="newName">The new name of the worksheet.</param>
        void RenameWorksheet(Worksheet sheet, string newName);

        /// <summary>
        /// Resets (removes) the Titan buttons from the Excel sheet right click menu.
        /// </summary>
        void ResetRightClickMenu();

        /// <summary>
        /// Resets the Excel right click menu.
        /// </summary>
        /// <param name="showbuttons">Show or hide the buttons.</param>
        void ShowRightClickMenuButtons(bool showbuttons);

        /// <summary>
        /// Builds the Excel toolbar, and right click menu.
        /// </summary>
        void BuildToolbar();

        /// <summary>
        /// Set the status of the pace toolbar.
        /// </summary>
        void SetToolbarStatus(bool toolbarStatus);

        /// <summary>
        /// Build the ruleset combo box and add it to the Pace toolbar.
        /// </summary>
        /// <param name="before">Inserts the combo box before a control at a given position.</param>
        void BuildRuleSetComboBox(int? before);

        /// <summary>
        /// Builds the Excel toolbar for the Test Harness.
        /// </summary>
        void BuildTestHarnessToolBarButtons();

        /// <summary>
        /// Sets the face id of the session lock button.
        /// </summary>
        /// <param name="hasLocks"></param>
        void SetSessionLockButtonStatus(bool hasLocks);

        /// <summary>
        /// Show or hide the extra test harness toolbar buttons on the toolbar.
        /// </summary>
        /// <param name="show">true to show the buttons, false to hide them.</param>
        void ShowTestHarnessToolbarButtons(bool show);

        /// <summary>
        /// Show or hide the Role Filter toolbar bar and menubar buttons.
        /// </summary>
        void ShowRoleFilterToolbarButton();

        /// <summary>
        /// Enable or disable the extra test harness toolbar buttons on the toolbar.
        /// </summary>
        /// <param name="enable">true to enable the buttons, false to disable them.</param>
        void EnableTestHarnessToolbarButtons(bool enable);

        /// <summary>
        /// Removes the drop down menu for the Excel application.
        /// </summary>
        /// <param name="menuName">Name of the menu to remove.</param>
        void DeleteMenus(string menuName);

        /// <summary>
        /// Builds a MenuBar in the Excel "Worksheet Menu Bar"
        /// </summary>
        /// <param name="menuName">The name of the menu.</param>
        /// <param name="toolTip">The tooltip for the menubar.</param>
        void BuildMenu(string menuName, string toolTip);

        /// <summary>
        /// Enables/disables the button Roles buttons.
        /// </summary>
        /// <param name="enableRefreshButton">Enable/disable the refresh button.</param>
        void EnableRolesButtons(bool enableRefreshButton);

        /// <summary>
        /// Enables/disables the buttons that are specific to our views only.
        /// </summary>
        /// <param name="enableRefreshButton">Enable/disable the refresh button.</param>
        void EnableViewSheetOnlyButtons(bool enableRefreshButton);

        /// <summary>
        /// Enables the PasteShape buttons on the toolbar and right click menu.
        /// </summary>
        /// <remarks>Removed contents. http://63.122.66.56/jira/browse/TTN-911</remarks>
        void EnablePasteShapeButtons(bool enableButton);

        /// <summary>
        /// Enable or disable the Role Filter toolbar bar and menubar buttons.
        /// </summary>
        void EnableRoleFilterToolbarButton();

        /// <summary>
        /// Enabled/disables the cell notes buttons.
        /// </summary>
        /// <param name="enable">Enables/disable the cell notes buttons.</param>
        /// <param name="buttons">Buttons to enable/disable.</param>
        void EnableCellNotesButtons(bool enable, List<CommandBarControl> buttons);

        /// <summary>
        /// Enable or disble the hyperlink buttons.
        /// </summary>
        /// <param name="enable">enable or disable</param>
        /// <param name="buttons">buttons to disable.</param>
        void EnableHyperlinkButton(bool enable, List<CommandBarControl> buttons);

        /// <summary>
        /// Enable or disble the formula buttons.
        /// </summary>
        /// <param name="enable">enable or disable</param>
        /// <param name="buttons">buttons to disable.</param>
        void EnableFormulaButton(bool enable, List<CommandBarControl> buttons);

        /// <summary>
        /// Enables the buttons on the Toolbar and Menu Bar.
        /// </summary>
        /// <param name="enableLockButton">Enable/disable the lock buttons.</param>
        void EnableLockButtons(bool enableLockButton);

        /// <summary>
        /// Enables the buttons on the Toolbar and Menu Bar.
        /// </summary>
        /// <param name="enable">Enable/disable the lock buttons.</param>
        void EnableSessionLockButtons(bool enable);

        /// <summary>
        /// Enables the replicate/unreplicate buttons on the right click menu.
        /// </summary>
        /// <param name="enableReplicateButton">Enable/disable replication button.</param>
        /// <param name="enableUnReplicateButton">Enable/disable unreplication button.</param>
        void EnableReplicateButtons(bool enableReplicateButton, bool enableUnReplicateButton);

        /// <summary>
        /// Enables the lift/unlift buttons on the right click menu.
        /// </summary>
        /// <param name="enableLiftButton">Enable/disable lift button.</param>
        /// <param name="enableUnLiftButton">Enable/disable unlift button.</param>
        void EnableLiftButtons(bool enableLiftButton, bool enableUnLiftButton);

        /// <summary>
        /// Enables the Change Password optoion in Menu Bar.
        /// </summary>
        /// <param name="enableChangePasswordButton">Enable or disable the button.
        /// If the user is a domain/LDAP user this parm is ignored and the button is disabled.
        /// </param>
        void EnableChangePasswordButton(bool enableChangePasswordButton);

        /// <summary>
        /// Shows/Hides the custom toolbars within the application.
        /// </summary>
        /// <param name="showToolBar">Show the custom toolbar.</param>
        /// <param name="showMenuBar">Show the Menu Bar menu.</param>
        void EnableApplicationCommandBars(bool showToolBar, bool showMenuBar);

        /// <summary>
        /// Enables or disables the save, commit, undo buttons and change ruleset combobox depending on the actions 
        /// available.
        /// </summary>
        /// <param name="areCalculationsPending">Calculate button status.</param>
        /// <param name="areAnyCalculationsPending">Used for the change ruleset dropdown.</param>
        void EnableCommandBarControls(bool areCalculationsPending, bool areAnyCalculationsPending);

        /// <summary>
        /// Enables or disables a menu button
        /// </summary>
        /// <param name="caption">Button caption.</param>
        /// <param name="enabled">Button enabled status.</param>
        void EnableMenuButton(string caption, bool enabled);

        /// <summary>
        /// Turn on or off the Excel events.
        /// </summary>
        /// <param name="flag">Sets the Excel events to be on or off.</param>
        void EnableEvents(bool flag);

        /// <summary>
        /// Looks at all the values of an Excel range looking for a non numeric input.
        /// </summary>
        /// <param name="Target">An Excel range</param>
        /// <returns>True if the range has invalid input, false if the range input is ok.</returns>
        bool RangeHasReplicationCharacters(Range Target);

        /// <summary>
        /// Looks at all the values of an Excel range looking for a non numeric input.
        /// </summary>
        /// <param name="Target">An Excel range</param>
        /// <returns>True if the range has invalid input, false if the range input is ok.</returns>
        bool RangeHasLiftCharacters(Range Target);

        /// <summary>
        /// Saves the task pane status.
        /// </summary>
        /// <param name="status">Status.</param>
        void SaveTaskPaneStatus(bool status);

        /// <summary>
        /// Saves the current status of the task pane to disk.
        /// </summary>
        void SaveTaskPaneCurrentStatus();

        /// <summary>
        /// Places an Excel freeze pane on the current worksheet.
        /// </summary>
        /// <param name="cell">Address of the cell to place the freeze pane.</param>
        /// <param name="updateScreen">Turn on Excel Screen updating before setting freeze pane.</param>
        void SetFreezePane(CellAddress cell, bool updateScreen = true);

        /// <summary>
        /// Clears any existing Excel freeze panes.
        /// </summary>
        void ClearExistingFreezePanes();

        /// <summary>
        /// Activates an Excel worksheet.
        /// </summary>
        /// <param name="sheetName">The name of the worksheet to activate.</param>
        /// <returns>True if activated, flase if the sheet was not activated.</returns>
        bool ActivateSheet(string sheetName);

        /// <summary>
        /// Hides an Excel worksheet.
        /// </summary>
        /// <param name="sheetName">The name of the worksheet to activate.</param>
        /// <returns>True if activated, flase if the sheet was not activated.</returns>
        void HideSheet(string sheetName);

        /// <summary>
        /// Unhides an Excel worksheet.
        /// </summary>
        /// <param name="sheetName">The name of the worksheet to activate.</param>
        /// <returns>True if activated, flase if the sheet was not activated.</returns>
        void UnhideSheet(string sheetName);

        /// <summary>
        /// Get a Excel.Worksheet from a hash code.
        /// </summary>
        /// <param name="hashCode">The hash code of the worksheet that should be returned.</param>
        /// <returns>A worksheet if one is found, null if no worksheet is found.</returns>
        Worksheet GetWorksheetFromHashCode(int hashCode);


        /// <summary>
        /// Creates a new worksheet with a custom view property, and the same name as the
        /// viewName parameter, with any invalid characters striped out.
        /// </summary>
        /// <param name="currentSheet">The currently active worksheet.</param>
        /// <param name="viewPropertyName">The name of the viewProperty to track the view name.</param>
        /// <param name="viewName">The name of the view to be put on the sheet.</param>
        /// <param name="sheetPassword">Password to protect the worksheet.</param>
        /// <returns>The worksheet if successful, null if not.</returns>
        Worksheet BuildNewViewSheet(Worksheet currentSheet, string viewPropertyName,
            string viewName, string sheetPassword);

        /// <summary>
        /// Searches the sheet to find a tag with the following value.
        /// </summary>
        /// <param name="sh">The Excel worksheet.</param>
        /// <param name="property">The property to find.</param>
        /// <param name="value">The value of the property.</param>
        /// <returns>True if the tag is found, false if the tag is not found.</returns>
        bool DoesSheetContainCustomProperty(Worksheet sh, string property, object value);

        /// <summary>
        /// Searches the sheet to find a tag with the following value.
        /// </summary>
        /// <param name="sh">The Excel worksheet.</param>
        /// <param name="propertyName">The property to find.</param>
        /// <returns>True if the tag is found, false if the tag is not found.</returns>
        bool DoesSheetContainCustomProperty(Worksheet sh, string propertyName);

        /// <summary>
        /// Checks the workbook to see if the worksheet already exists.
        /// </summary>
        /// <param name="wbk">Workbook to search for sheet.</param>
        /// <param name="name">Name of the sheet to look for.</param>
        /// <returns>True if the sheet exists, false if not.</returns>
        bool DoesSheetExist(Workbook wbk, string name);

        /// <summary>
        /// Checks the workbook to see if the worksheet already exists.
        /// </summary>
        /// <param name="sheetName">Name of the sheet to look for.</param>
        /// <returns>True if the sheet exists, false if not.</returns>
        bool DoesSheetExist(string sheetName);

        /// <summary>
        /// Searches the activeworkbook to find a tag with the following value.
        /// </summary>
        /// <param name="propertyName">The property to find.</param>
        /// <param name="value">The value of the property.</param>
        /// <returns>The worksheet if the tag is found, null if the tag is not found.</returns>
        Worksheet DoesWorkbookContainCustomProperty(string propertyName, object value);

        /// <summary>
        /// Adds a custom property to the excel worksheet.
        /// </summary>
        /// <param name="sh">The excel worksheet.</param>
        /// <param name="propertyName">Name of the property.</param>
        /// <param name="value">Value of the property.</param>
        void AddCustomProperty(Worksheet sh, string propertyName, object value);

        /// <summary>
        /// Adds a custom property to the excel worksheet.
        /// </summary>
        /// <param name="sh">The excel worksheet.</param>
        /// <param name="propertyName">Name of the property.</param>
        bool RemoveCustomProperty(Worksheet sh, string propertyName);

        /// <summary>
        /// Gets the format for a style.
        /// </summary>
        /// <param name="styleName">name of the sytle to return the format object.</param>
        /// <returns>a format object.</returns>
        Format GetStyleFormat(string styleName);

        /// <summary>
        /// Searches the sheet to find a property and returns its value.
        /// </summary>
        /// <param name="sh">The Excel worksheet.</param>
        /// <param name="propertyName">The property to find.</param>
        /// <returns>Returns the value if found, null if the value is not found.</returns>
        object GetCustomProperty(Worksheet sh, string propertyName);

        /// <summary>
        /// Turns off gridlines, and set the display heading according to the settings files.
        /// </summary>
        /// <param name="sheet">Sheet to perform the actions.</param>
        void AddOurFormats(Worksheet sheet);

        /// <summary>
        /// Creates a new Excel workbook, or opens the workbook if it already exists.
        /// </summary>
        /// <param name="path">Path to the new file.</param>
        /// <param name="fileName">The file name.</param>
        /// <returns>The Excel.Workbook object, or null if an error occurs.</returns>
        Workbook CreateNewWorkBook(string path, string fileName);

        /// <summary>
        /// Deletes an Excel workbook.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        void DeleteWorkbook(string path, string fileName);

        /// <summary>
        /// Makes a worksheet name unique by adding _x at the end of the sheet name.
        /// </summary>
        /// <param name="sheetName">The name of the sheet to make unique.</param>
        /// <param name="normalizeSheetName">true to normalize the sheet before making it unique.</param>
        /// <param name="searchForNumAtEndOfShtName">true to search the end of the sheetName
        /// for an _x and removing it before trying to make it unique.</param>
        /// <returns>the new unique sheet name.</returns>
        string MakeSheetNameUnique(string sheetName, bool normalizeSheetName,
                                   bool searchForNumAtEndOfShtName);

        /// <summary>
        /// Removes all the view sheets in the workbook.
        /// </summary>
        void RemoveViewSheets();

        /// <summary>
        /// Protects the Excel worksheet.
        /// </summary>
        /// <param name="sht">The Excel worksheet to protect.</param>
        /// <param name="password">The password to use.</param>
        /// <returns>True if the sheet was locked, false if not.</returns>
        bool ProtectSheet(Worksheet sht, string password);

        /// <summary>
        /// Unprotects the Excel worksheet.
        /// </summary>
        /// <param name="sht">The Excel worksheet to protect.</param>
        /// <param name="password">The password to use.</param>
        /// <returns>True if the sheet was unlocked, false if not.</returns>
        bool UnprotectSheet(Worksheet sht, string password);

        ///// <summary>
        ///// Protects all our worksheets in the workbook
        ///// </summary>
        //void UnprotectOurWorksheets();

        /// <summary>
        /// Unprotects all worksheets in the workbook.
        /// </summary>
        void UnprotectWorksheets();

        /// <summary>
        /// Unprotects all our worksheets in the workbook
        /// </summary>
        void ProtectOurWorksheets();

        /// <summary>
        /// Hides any dynamic worksheets that have not been built.
        /// </summary>
        /// <param name="currentSheet">The currently selected worksheet.</param>
        void HideUnbuiltWorksheet(Worksheet currentSheet);

        /// <summary>
        /// Displays the Excel memory information.
        /// </summary>
        void DisplayMemory();

        /// <summary>
        /// Set the options of hyperion smart view add in.
        /// </summary>
        /// <param name="turnOffAllOptions">Sets options to false</param>
        void SetHSVAddInGlobalOptions(bool turnOffAllOptions);

         /// <summary>
        /// Resets the Essbase Global Options
        /// </summary>
        /// <param name="turnOffAllOptions">Sets options to false</param>
        void SetEssbaseAddInGlobalOptions(bool turnOffAllOptions);

        /// <summary>
        /// Get Hyperion Smart View double-clicking setting
        /// </summary>
        /// <returns>Returns true if enable, false if not, or null if not available.</returns>
        bool? getHSVDoubleClickingButtonSetting(bool refresh);

        /// <summary>
        /// Get secondary button setting from the registry
        /// </summary>
        /// <param name="refresh">Refresh the status of the registry value.</param>
        /// <returns>Returns true if enable, false if not, or null if not available.</returns>
        bool? getEssbaseSecondaryButtonSetting(bool refresh);

        /// <summary>
        /// Get Essbase double-clicking setting
        /// </summary>
        /// <param name="refresh">Refresh the status of the registry value.</param>
        /// <returns>Returns true if enable, false if not, or null if not available.</returns>
        bool? getEssbaseDoubleClicking(bool refresh);

        /// <summary>
        /// Display Essbase AddIn
        /// </summary>
        /// <param name="showAddIn">Display the add in.</param>
        void ShowHideEssbaseAddIn(bool showAddIn);

        /// <summary>
        /// Display Essbase AddIn
        /// </summary>
        /// <param name="showAddIn">Display the add in.</param>
        void ShowHideHSVAddIn(bool showAddIn);

        /// <summary>
        /// Set to true if copied or cut data is available
        /// </summary>
        /// <returns>true if in Excel cut/copy mode, false if not.</returns>
        bool InCutCopyMode();

        /// <summary>
        /// Gets the edit mode status of Excel.
        /// </summary>
        /// <returns>true if Excel is in edit mode, false if not.</returns>
        bool InEditMode();

        /// <summary>
        /// Save cell notes.
        /// </summary>
        /// <param name="cellNoteMngr"></param>
        void SaveCellNotes(CellNoteMngr cellNoteMngr);

        /// <summary>
        /// Save member tag data.
        /// </summary>
        /// <param name="sheet"></param>
        /// <param name="viewMngr"></param>
        /// <returns></returns>
        bool SaveMemberTags(Worksheet sheet, ViewMngr viewMngr);

        #endregion Methods

        #region Task Pane

        //Task Pane//

        /// <summary>
        /// Builds the inital ActionsPane.
        /// </summary>
        void BuildActionsPane();

        /// <summary>
        /// Gets the SimpleTrees from the ActionsPane.
        /// </summary>
        /// <returns>A SimpleTrees structure that contains a list a PafSimpleTrees</returns>
        [Obsolete("Replaced by SimpleDimTrees", false)]
        SimpleTrees GetSimpleTrees();

        /// <summary>
        /// Gets the SimpleTrees.
        /// </summary>
        /// <returns>A SimpleTrees structure that contains a list a PafSimpleTrees</returns>
        SimpleDimTrees GetPaceSimpleTrees();

        /// <summary>
        /// Sets the position of the Excel Task Pane.
        /// </summary>
        /// <param name="pos">
        /// Enum of the Microsoft.Office.Core.MsoBarPosition.
        /// </param>
        void PositionTaskPane(MsoBarPosition pos);

        /// <summary>
        /// Shows the task pane if it's hidden, hides the task pane if it's visible.
        /// </summary>
        void ShowHideTaskPane();

        /// <summary>
        /// Shows/Hides the task pane
        /// </summary>
        /// <param name="showTaskPane">true shows the task pane/false hides it</param>
        void SetTaskPaneStatus(bool showTaskPane);

        /// <summary>
        /// Shows/hides the task pane
        /// </summary>
        /// <param name="showTaskPane">true shows the task pane/false hised it</param>
        void ShowHideTaskPane(bool showTaskPane);

        #endregion Task Pane

        #region Actions
        //Actions//

        /// <summary>
        /// Prompts the user if they want to calculate.  Also performs the calculation.  
        /// </summary>
        /// <param name="sheet">The worksheet to check for calculations.</param>
        /// <param name="showCancelButton">Show the cancel button.</param>
        /// <returns>The button that the user clicked (Yes/No/Cancel).</returns>
        DialogResult Calculate(Worksheet sheet, bool showCancelButton);

        /// <summary>
        /// Prompts the user if they want to calculate.  Also performs the calculation.  
        /// </summary>
        /// <returns>The button that the user clicked (Yes/No/Cancel).</returns>
        DialogResult Calculate(Worksheet sheet);

        /// <summary>
        /// Prompts the user if they want to calculate.  Also performs the calculation.  
        /// </summary>
        /// <returns>The button that the user clicked (Yes/No/Cancel).</returns>
        DialogResult Calculate();

        /// <summary>
        /// Prompts the user if they want to save their pending calculations.  
        /// </summary>
        /// <param name="showWarning">Include a warning as part of the message displayed to the user.</param>
        /// <returns>The button that the user clicked (Yes/No/Cancel).</returns>
        /// <param name="alwaysShowMessage">Always show the message box, even if there is no data to save.</param>
        /// <param name="runSilent">Run the save silently, will not prompt 
        /// the user (this parameter ignores alwaysShowMessage).</param>
        DialogResult SaveWork(bool showWarning, bool alwaysShowMessage, bool runSilent);

        /// <summary>
        /// Shows the RefreshView confirmation dialog box.
        /// </summary>
        /// <param name="sheet">Current view Excel.Worksheet.</param>
        /// <returns>The button(Yes/No) that was clicked by the user.</returns>
        DialogResult RefreshView(Worksheet sheet);

        /// <summary>
        /// Clears the grid of all data.
        /// </summary>
        /// <param name="grid">Name of the grid to clear.</param>
        void ClearGrid(string grid);

        /// <summary>
        /// Prompts the user if they want to save their pending calculations.
        /// </summary>
        /// <param name="dataSlice">Reference parameter containg the new pafDataSlice, null if no pafDataSlice is returned.</param>
        /// <param name="viewName">Name of the current view.</param>
        /// <returns>The button that the user clicked (Ok/Cancel).</returns>
        DialogResult ResetDataCache(ref pafDataSlice dataSlice, string viewName);

        /// <summary>
        /// Prompts the user if they want to save their pending calculations.
        /// </summary>
        /// <returns>The button that the user clicked (Ok/Cancel).</returns>
        DialogResult ResetDataCache(string viewName);

        /// <summary>
        /// Undo the last user calculation.
        /// </summary>
        /// <returns></returns>
        DialogResult UndoCalculation();
        #endregion Actions

        #region Dialog Boxes
        //Dialog boxes//

        /// <summary>
        /// Shows the Logon Dialog Box, so the user can logon to Essbase.
        /// </summary>
        /// <returns>True if user is authenicated, false if not.</returns>
        bool ShowLogonDialogBox();


        /// <summary>
        /// Shows the Cluster Dialog Box, so the user can logon to Essbase.
        /// </summary>
        /// <returns>True if user is the user click the ok button, false if not.</returns>
        bool ShowClusterDialogBox();

        /// <summary>
        /// Shows the Session Lock Dialog Box.
        /// </summary>
        /// <returns></returns>
        bool ShowSessionGridLockDialogBox();

        /// <summary>
        /// Shows the Role Selection Dialog Box.
        /// </summary>
        /// /// <returns>True if user clicked ok, false if cancel.</returns>
        bool ShowRoleSelectionDialogBox();

        /// <summary>
        /// Shows the Role Filter Dialog Box.
        /// </summary>
        /// <returns>True if user clicked ok, false if cancel.</returns>
        bool ShowRoleFilterDialogBox(bool enableMultiSelect);

        #endregion Dialog Boxes

        #region Property Getters

        //Getters//

        /// <summary>
        /// Toggle screen updating
        /// </summary>
        bool ScreenUpdating { get; set;}
        
        /// <summary>
        /// Get/Set if Excel will displays alerts.
        /// </summary>
        bool DisplayAlerts { get; set;}

        /// <summary>
        /// Returns the RoleSelector control
        /// </summary>
        RoleSelection RoleSelector { get; }

        /// <summary>
        /// Role selection form.
        /// </summary>
        frmRoleSelection RoleSelectionForm  { get;}

        /// <summary>
        /// Role Filter form.
        /// </summary>
        frmRoleFilter RoleFilterForm { get;}

        /// <summary>
        /// Returns the RoleFilter control
        /// </summary>
        RoleFilter RoleFilter { get; }

        /// <summary>
        /// Returns the UserLogon control.
        /// </summary>
        frmLogon LogonInformation { get; }

        /// <summary>
        /// Returns the SessionLock form
        ///  </summary>
        frmSessionLocks SessionLockForm { get; set; }

        /// <summary>
        /// Returns CreateAssortment form
        /// </summary>
        frmCreateAsst ClusterDialog { get; set; }

        /// <summary>
        /// Returns the ExcelActionsPane.
        /// </summary>
        ExcelActionsPane ActionsPane { get;}

        /// <summary>
        /// Returns the toolbar CommandBar object.
        /// </summary>
        CommandBar ViewToolBar { get; }

        /// <summary>
        /// Returns the MenuBar CommandBarControl.
        /// </summary>
        CommandBarControl ViewMenu { get;}

        /// <summary>
        /// Gets the RightClick CommandBar object.
        /// </summary>
        CommandBar RightClickMenu { get; }

        /// <summary>
        /// Gets the application version.
        /// </summary>
        string Version { get; }

        /// <summary>
        /// Gets the application version as an float.
        /// </summary>
        float VersionAsFloat { get;}

        /// <summary>
        /// Returns a list of hash codes for the sheets.
        /// </summary>
        List<int> Sheets { get; }


        //void TemporaryLogTTN1064(string caller, Worksheet sheet, Exception ex, string message1);

        #endregion Property Getters
    }
}
