using Titan.Pace.Application.Controls;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._changePassword = new ChangePassword();
            this.SuspendLayout();
            // 
            // _changePassword
            // 
            this._changePassword.Dock = System.Windows.Forms.DockStyle.Fill;
            this._changePassword.Location = new System.Drawing.Point(0, 0);
            this._changePassword.Margin = new System.Windows.Forms.Padding(4);
            this._changePassword.MaxNewPasswordLength = 16;
            this._changePassword.MinNewPasswordLength = 6;
            this._changePassword.Name = "_changePassword";
            this._changePassword.OldPassword = null;
            this._changePassword.Size = new System.Drawing.Size(370, 234);
            this._changePassword.TabIndex = 0;
            this._changePassword.Username = null;
            this._changePassword.Load += new System.EventHandler(this._changePassword_Load);
            // 
            // frmChangePassword
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.ClientSize = new System.Drawing.Size(370, 234);
            this.Controls.Add(this._changePassword);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmChangePassword";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.ResumeLayout(false);

        }

        internal ChangePassword _changePassword;

        #endregion
    }
}