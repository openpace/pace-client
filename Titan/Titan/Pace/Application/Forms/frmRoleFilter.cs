#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Threading;
using System.Windows.Forms;
using Titan.Properties;

namespace Titan.Pace.Application.Forms
{
    /// <summary>
    /// Role Filter Form.
    /// </summary>
    internal partial class frmRoleFilter : Form
    {
        public bool UserFilteredMultiSelect
        {
            get
            {
                return roleFilter != null && roleFilter.UserFilteredMultiSelect;
            }
            set
            {
                if(roleFilter != null)
                {
                    roleFilter.UserFilteredMultiSelect = value;
                }
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public frmRoleFilter()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Handler for the form load event.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event args.</param>
        private void frmRoleFilter_Load(object sender, System.EventArgs e)
        {
            Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
            Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmRoleFilter.Caption");
            Icon = Resources.PIcon;
        }

        /// <summary>
        /// Occurs when the forms is shown.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        private void frmRoleFilter_Shown(object sender, System.EventArgs e)
        {
            try
            {
                roleFilter.SuspendDrawing();
                roleFilter.RebuildTreeTabs();
                roleFilter.RefrehAttributeTabs();
                //TTN-2144
                roleFilter.ReselctAttributeNodes();
                string role =
                    PafApp.GetLocalization()
                          .GetResourceManager()
                          .GetString("Application.Excel.ActionsPane.Header.RoleLabel");
                string process =
                    PafApp.GetLocalization()
                          .GetResourceManager()
                          .GetString("Application.Controls.RoleSelection.ProcessSeasonLabel");
                Text = role + " " + roleFilter.Role + ", " + process + " " + roleFilter.SeasonProcess;
            }
            finally
            {
                roleFilter.ResumeDrawing();
            }
        }
    }
}