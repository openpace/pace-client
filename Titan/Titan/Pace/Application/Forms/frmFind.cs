﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Windows.Forms;

namespace Titan.Pace.Application.Forms
{
    /// <remarks/>
    public partial class frmFind : Form
    {
        /// <summary>
        /// Search text entered by the user.
        /// </summary>
        public string SearchText { get; set; }

        /// <remarks/>
        public bool MatchCase { get; set; }

        /// <remarks/>
        public bool AutoCheckMember { get; set; }

        /// <remarks/>
        public bool SearchAliasTables { get; set; }

        /// <remarks/>
        public bool MatchWholeWord { get; set; }

        /// <remarks/>
        public frmFind()
        {
            InitializeComponent();
            SetDefaults();
            this.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Find.Text");
            lblSearchTxt.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Find.Label.lblSearchTxt.Text");
            cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Find");
            cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
            chkMatchCase.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Find.Label.ChkMatchCase.Text");
            chkAutoCheckMember.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Find.Label.ChkAutoCheckMember.Text");
            chkSearchAliasTables.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Find.Label.ChkSearchAliasTables.Text");
            txtSearchText.Focus();

        }

        private void SetDefaults()
        {
            if (!String.IsNullOrEmpty(Properties.Settings.Default.Find_LastSearchEntry))
            {
                txtSearchText.Text = Properties.Settings.Default.Find_LastSearchEntry;
            }

            chkMatchCase.Checked = Properties.Settings.Default.Find_MatchCase;
            chkAutoCheckMember.Checked = Properties.Settings.Default.Find_AutoCheckMember;
            chkSearchAliasTables.Checked = Properties.Settings.Default.Find_SearchAliasTables;
            chkMatchWholeWord.Checked = Properties.Settings.Default.Find_MatchWholeWord;

        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            SearchText = txtSearchText.Text;
            MatchCase = chkMatchCase.Checked;
            AutoCheckMember = chkAutoCheckMember.Checked;
            SearchAliasTables = chkSearchAliasTables.Checked;
            MatchWholeWord = chkMatchWholeWord.Checked;

            Properties.Settings.Default.Find_LastSearchEntry = SearchText;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
        }

        private void txtSearchText_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode == Keys.Enter || e.KeyCode == Keys.Return)
            {
                cmdOk.PerformClick();
            }
        }

        private void chkMatchCase_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            Properties.Settings.Default.Find_MatchCase = cb.Checked;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
        }

        private void chkAutoCheckMember_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            Properties.Settings.Default.Find_AutoCheckMember = cb.Checked;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
        }

        private void chkSearchAliasTables_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            Properties.Settings.Default.Find_SearchAliasTables = cb.Checked;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
        }

        private void frmFind_Load(object sender, EventArgs e)
        {

        }

        private void chkMatchWholeWord_CheckedChanged(object sender, EventArgs e)
        {
            CheckBox cb = (CheckBox)sender;

            Properties.Settings.Default.Find_MatchWholeWord = cb.Checked;
            Properties.Settings.Default.Save();
            Properties.Settings.Default.Reload();
        }  
    }
}
