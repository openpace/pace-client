#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Titan.Properties;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmAboutBox2 : Form
    {
        private Bitmap backgroundImage = null;

        public frmAboutBox2()
        {
            InitializeComponent();

            //  Initialize the AboutBox to display the product information from the assembly information.
            //  Change assembly information settings for your application through either:
            //  - Project->Properties->Application->Assembly Information
            //  - AssemblyInfo.cs


            Icon = Resources.PIcon;
            backgroundImage = Resources.client_login;
            Color backColor = backgroundImage.GetPixel(1, 1);
            backgroundImage.MakeTransparent();



            this.Icon = (System.Drawing.Icon)
                Resources.PIcon;

            this.okButton.Text = PafApp.GetLocalization().
                    GetResourceManager().GetString("Application.Button.OK");

            this.Text = String.Format(
                PafApp.GetLocalization().GetResourceManager().GetString("About.About") + " {0}",
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"));

            this.labelProductName.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");

            this.labelVersion.Text = String.Format(PafApp.GetLocalization().GetResourceManager().
                GetString("Application.Message.Version") + ": {0}", AssemblyVersion);
            
            this.labelCopyright.Text = AssemblyCopyright;
            
            this.labelCompanyName.Text = AssemblyCompany;

            groupBoxInformation.Text = PafApp.GetLocalization().GetResourceManager().GetString("About.ProgramInfo");

            labelServerVersion.Text = PafApp.GetLocalization().GetResourceManager().GetString("About.ServerVersion") + ": " + PafApp.ServerVersion;

            object[] objClient = new object[2];
            objClient[0] = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            objClient[1] = PafApp.GetGridApp().Version;

            labelClientVersion.Text = String.Format(PafApp.GetLocalization().GetResourceManager().
                GetString("About.ClientVersion") + ": {0}\r\n{1}\r\n\r\n", objClient);

            labelExcelVersion.Text = PafApp.GetGridApp().Version;

            string[] text =
                PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.Support").Split(new [] {"\r\n"}, StringSplitOptions.RemoveEmptyEntries);

            groupBoxSupport.Text = text[0];

            linkLabel1.Text =  text[1];

            //labelSupportPhone.Text = text[2];     // Disabled for Pace Rebranding (TTN-2666)

            //object[] obj = new object[4];
            //obj[0] = PafApp.ServerVersion;
            //obj[1] = System.Reflection.Assembly.GetExecutingAssembly().GetName().Version;
            //obj[2] = PafApp.GetGridApp().Version;
            //obj[3] = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.Support");

            //this.textBoxDescription.Text = String.Format(PafApp.GetLocalization().GetResourceManager().
            //    GetString("About.ServerVersion") + ": {0}\r\n" +  PafApp.GetLocalization().GetResourceManager().
            //    GetString("About.ClientVersion") + ": {1}\r\n{2}\r\n\r\n{3}", obj);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            //  Do nothing here!
        }
        [DebuggerHidden]
        protected override void OnPaintBackground(PaintEventArgs e)
        {

            //// Create a Bitmap object from an image file.
            //Bitmap myBitmap = new Bitmap(@"C:\Users\administrator\Documents\Titan\Titan\Pace\Localization\Resources\client_login_600-430.png");
            //Bitmap myBitmap = (Bitmap)PafApp.GetLocalization().GetResourceManager().GetObject("client_login_600-430");
            Bitmap myBitmap = backgroundImage;

            // Draw myBitmap to the screen.
            e.Graphics.DrawImage(myBitmap, 0, 0, myBitmap.Width,
                myBitmap.Height);

            //Color backColor = myBitmap.GetPixel(1, 1);

            //// Make the default transparent color transparent for myBitmap.
            //myBitmap.MakeTransparent(backColor);

            // Draw the transparent bitmap to the screen.
            e.Graphics.DrawImage(myBitmap, myBitmap.Width, 0,
                myBitmap.Width, myBitmap.Height);

        }

        #region Assembly Attribute Accessors

        public string AssemblyTitle
        {
            get
            {
                // Get all Title attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                // If there is at least one Title attribute
                if (attributes.Length > 0)
                {
                    // Select the first one
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    // If it is not an empty string, return it
                    if (titleAttribute.Title != "")
                        return titleAttribute.Title;
                }
                // If there was no Title attribute, or if the Title attribute was the empty string, return the .exe name
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                // Get all Description attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                // If there aren't any Description attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Description attribute, return its value
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                // Get all Product attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                // If there aren't any Product attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Product attribute, return its value
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                // Get all Copyright attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                // If there aren't any Copyright attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Copyright attribute, return its value
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                // Get all Company attributes on this assembly
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                // If there aren't any Company attributes, return an empty string
                if (attributes.Length == 0)
                    return "";
                // If there is a Company attribute, return its value
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        private void frmAboutBox2_Click(object sender, EventArgs e)
        {
            this.okButton.PerformClick();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            System.Diagnostics.Process.Start("mailto:" + linkLabel1.Text);
        }

        private void label_Click(object sender, EventArgs e)
        {
            Control c = (Control) sender;
            c.Focus();
        }

        private void label_KeyDown(object sender, System.Windows.Forms.KeyEventArgs e)
        {
            Label label = (Label) sender;
            if (label.ContainsFocus && e.Control && e.KeyCode == Keys.C)
            {
                Clipboard.SetText(label.Text);
            }

        }
    }
}
