namespace Titan.Pace.Application.Forms
{
    partial class frmOptions
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBoxColors = new System.Windows.Forms.GroupBox();
            this.txtMemberTagNoteColor = new System.Windows.Forms.TextBox();
            this.lblMemberTagNoteColor = new System.Windows.Forms.Label();
            this.txtSystemLockColor = new System.Windows.Forms.TextBox();
            this.txtForwardPlannableProtectedColor = new System.Windows.Forms.TextBox();
            this.txtUserLockColor = new System.Windows.Forms.TextBox();
            this.txtProtectedColor = new System.Windows.Forms.TextBox();
            this.txtNonPlannableProtectedColor = new System.Windows.Forms.TextBox();
            this.lblUserLockColor = new System.Windows.Forms.Label();
            this.lblProtectedColor = new System.Windows.Forms.Label();
            this.lblSystemLockColor = new System.Windows.Forms.Label();
            this.lblForwardPlannableProtectedColor = new System.Windows.Forms.Label();
            this.lblNonPlannableProtectedColor = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBoxTheme = new System.Windows.Forms.GroupBox();
            this.comboBoxSkin = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxProgramSettings = new System.Windows.Forms.GroupBox();
            this.chkBOH = new System.Windows.Forms.CheckBox();
            this.cboLanguage = new System.Windows.Forms.ComboBox();
            this.lblLanguage = new System.Windows.Forms.Label();
            this.groupBoxFormattingSettings = new System.Windows.Forms.GroupBox();
            this.chkAutoFitHeaderRows = new System.Windows.Forms.CheckBox();
            this.chkAutoFitColumHeaderRowHeight = new System.Windows.Forms.CheckBox();
            this.chkAutoColumnHeading = new System.Windows.Forms.CheckBox();
            this.chkAutoPrintRange = new System.Windows.Forms.CheckBox();
            this.chkShowFreezePane = new System.Windows.Forms.CheckBox();
            this.chkShowPageMembers = new System.Windows.Forms.CheckBox();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.cmdOk = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.groupBoxColors.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.groupBoxTheme.SuspendLayout();
            this.groupBoxProgramSettings.SuspendLayout();
            this.groupBoxFormattingSettings.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxColors
            // 
            this.groupBoxColors.Controls.Add(this.txtMemberTagNoteColor);
            this.groupBoxColors.Controls.Add(this.lblMemberTagNoteColor);
            this.groupBoxColors.Controls.Add(this.txtSystemLockColor);
            this.groupBoxColors.Controls.Add(this.txtForwardPlannableProtectedColor);
            this.groupBoxColors.Controls.Add(this.txtUserLockColor);
            this.groupBoxColors.Controls.Add(this.txtProtectedColor);
            this.groupBoxColors.Controls.Add(this.txtNonPlannableProtectedColor);
            this.groupBoxColors.Controls.Add(this.lblUserLockColor);
            this.groupBoxColors.Controls.Add(this.lblProtectedColor);
            this.groupBoxColors.Controls.Add(this.lblSystemLockColor);
            this.groupBoxColors.Controls.Add(this.lblForwardPlannableProtectedColor);
            this.groupBoxColors.Controls.Add(this.lblNonPlannableProtectedColor);
            this.groupBoxColors.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBoxColors.Location = new System.Drawing.Point(4, 4);
            this.groupBoxColors.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxColors.Name = "groupBoxColors";
            this.groupBoxColors.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxColors.Size = new System.Drawing.Size(315, 372);
            this.groupBoxColors.TabIndex = 0;
            this.groupBoxColors.TabStop = false;
            // 
            // txtMemberTagNoteColor
            // 
            this.txtMemberTagNoteColor.Location = new System.Drawing.Point(12, 315);
            this.txtMemberTagNoteColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtMemberTagNoteColor.Name = "txtMemberTagNoteColor";
            this.txtMemberTagNoteColor.ReadOnly = true;
            this.txtMemberTagNoteColor.Size = new System.Drawing.Size(72, 20);
            this.txtMemberTagNoteColor.TabIndex = 16;
            this.txtMemberTagNoteColor.TabStop = false;
            // 
            // lblMemberTagNoteColor
            // 
            this.lblMemberTagNoteColor.AutoSize = true;
            this.lblMemberTagNoteColor.Location = new System.Drawing.Point(8, 295);
            this.lblMemberTagNoteColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblMemberTagNoteColor.Name = "lblMemberTagNoteColor";
            this.lblMemberTagNoteColor.Size = new System.Drawing.Size(0, 13);
            this.lblMemberTagNoteColor.TabIndex = 17;
            // 
            // txtSystemLockColor
            // 
            this.txtSystemLockColor.Location = new System.Drawing.Point(12, 149);
            this.txtSystemLockColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtSystemLockColor.Name = "txtSystemLockColor";
            this.txtSystemLockColor.ReadOnly = true;
            this.txtSystemLockColor.Size = new System.Drawing.Size(72, 20);
            this.txtSystemLockColor.TabIndex = 3;
            this.txtSystemLockColor.TabStop = false;
            this.txtSystemLockColor.DoubleClick += new System.EventHandler(this.txtSystemLockColor_DoubleClick);
            // 
            // txtForwardPlannableProtectedColor
            // 
            this.txtForwardPlannableProtectedColor.Location = new System.Drawing.Point(12, 262);
            this.txtForwardPlannableProtectedColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtForwardPlannableProtectedColor.Name = "txtForwardPlannableProtectedColor";
            this.txtForwardPlannableProtectedColor.ReadOnly = true;
            this.txtForwardPlannableProtectedColor.Size = new System.Drawing.Size(72, 20);
            this.txtForwardPlannableProtectedColor.TabIndex = 4;
            this.txtForwardPlannableProtectedColor.TabStop = false;
            // 
            // txtUserLockColor
            // 
            this.txtUserLockColor.Location = new System.Drawing.Point(12, 39);
            this.txtUserLockColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserLockColor.Name = "txtUserLockColor";
            this.txtUserLockColor.ReadOnly = true;
            this.txtUserLockColor.Size = new System.Drawing.Size(72, 20);
            this.txtUserLockColor.TabIndex = 5;
            this.txtUserLockColor.TabStop = false;
            // 
            // txtProtectedColor
            // 
            this.txtProtectedColor.Location = new System.Drawing.Point(12, 94);
            this.txtProtectedColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtProtectedColor.Name = "txtProtectedColor";
            this.txtProtectedColor.ReadOnly = true;
            this.txtProtectedColor.Size = new System.Drawing.Size(72, 20);
            this.txtProtectedColor.TabIndex = 9;
            this.txtProtectedColor.TabStop = false;
            // 
            // txtNonPlannableProtectedColor
            // 
            this.txtNonPlannableProtectedColor.Location = new System.Drawing.Point(12, 207);
            this.txtNonPlannableProtectedColor.Margin = new System.Windows.Forms.Padding(4);
            this.txtNonPlannableProtectedColor.Name = "txtNonPlannableProtectedColor";
            this.txtNonPlannableProtectedColor.ReadOnly = true;
            this.txtNonPlannableProtectedColor.Size = new System.Drawing.Size(72, 20);
            this.txtNonPlannableProtectedColor.TabIndex = 10;
            this.txtNonPlannableProtectedColor.TabStop = false;
            // 
            // lblUserLockColor
            // 
            this.lblUserLockColor.AutoSize = true;
            this.lblUserLockColor.Location = new System.Drawing.Point(8, 20);
            this.lblUserLockColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblUserLockColor.Name = "lblUserLockColor";
            this.lblUserLockColor.Size = new System.Drawing.Size(0, 13);
            this.lblUserLockColor.TabIndex = 11;
            // 
            // lblProtectedColor
            // 
            this.lblProtectedColor.AutoSize = true;
            this.lblProtectedColor.Location = new System.Drawing.Point(8, 74);
            this.lblProtectedColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblProtectedColor.Name = "lblProtectedColor";
            this.lblProtectedColor.Size = new System.Drawing.Size(0, 13);
            this.lblProtectedColor.TabIndex = 12;
            // 
            // lblSystemLockColor
            // 
            this.lblSystemLockColor.AutoSize = true;
            this.lblSystemLockColor.Location = new System.Drawing.Point(8, 129);
            this.lblSystemLockColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblSystemLockColor.Name = "lblSystemLockColor";
            this.lblSystemLockColor.Size = new System.Drawing.Size(0, 13);
            this.lblSystemLockColor.TabIndex = 13;
            // 
            // lblForwardPlannableProtectedColor
            // 
            this.lblForwardPlannableProtectedColor.AutoSize = true;
            this.lblForwardPlannableProtectedColor.Location = new System.Drawing.Point(8, 242);
            this.lblForwardPlannableProtectedColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblForwardPlannableProtectedColor.Name = "lblForwardPlannableProtectedColor";
            this.lblForwardPlannableProtectedColor.Size = new System.Drawing.Size(0, 13);
            this.lblForwardPlannableProtectedColor.TabIndex = 14;
            // 
            // lblNonPlannableProtectedColor
            // 
            this.lblNonPlannableProtectedColor.AutoSize = true;
            this.lblNonPlannableProtectedColor.Location = new System.Drawing.Point(8, 187);
            this.lblNonPlannableProtectedColor.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblNonPlannableProtectedColor.Name = "lblNonPlannableProtectedColor";
            this.lblNonPlannableProtectedColor.Size = new System.Drawing.Size(0, 13);
            this.lblNonPlannableProtectedColor.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBoxTheme);
            this.tabPage1.Controls.Add(this.groupBoxProgramSettings);
            this.tabPage1.Controls.Add(this.groupBoxFormattingSettings);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage1.Size = new System.Drawing.Size(323, 380);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBoxTheme
            // 
            this.groupBoxTheme.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxTheme.Controls.Add(this.comboBoxSkin);
            this.groupBoxTheme.Controls.Add(this.label1);
            this.groupBoxTheme.Location = new System.Drawing.Point(4, 312);
            this.groupBoxTheme.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxTheme.Name = "groupBoxTheme";
            this.groupBoxTheme.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxTheme.Size = new System.Drawing.Size(312, 52);
            this.groupBoxTheme.TabIndex = 4;
            this.groupBoxTheme.TabStop = false;
            this.groupBoxTheme.Visible = false;
            // 
            // comboBoxSkin
            // 
            this.comboBoxSkin.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxSkin.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxSkin.FormattingEnabled = true;
            this.comboBoxSkin.Location = new System.Drawing.Point(8, 16);
            this.comboBoxSkin.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxSkin.Name = "comboBoxSkin";
            this.comboBoxSkin.Size = new System.Drawing.Size(296, 21);
            this.comboBoxSkin.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 20);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(0, 13);
            this.label1.TabIndex = 5;
            // 
            // groupBoxProgramSettings
            // 
            this.groupBoxProgramSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxProgramSettings.Controls.Add(this.chkBOH);
            this.groupBoxProgramSettings.Controls.Add(this.cboLanguage);
            this.groupBoxProgramSettings.Controls.Add(this.lblLanguage);
            this.groupBoxProgramSettings.Location = new System.Drawing.Point(4, 188);
            this.groupBoxProgramSettings.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxProgramSettings.Name = "groupBoxProgramSettings";
            this.groupBoxProgramSettings.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxProgramSettings.Size = new System.Drawing.Size(312, 116);
            this.groupBoxProgramSettings.TabIndex = 1;
            this.groupBoxProgramSettings.TabStop = false;
            // 
            // chkBOH
            // 
            this.chkBOH.AutoSize = true;
            this.chkBOH.Location = new System.Drawing.Point(8, 73);
            this.chkBOH.Margin = new System.Windows.Forms.Padding(4);
            this.chkBOH.Name = "chkBOH";
            this.chkBOH.Size = new System.Drawing.Size(15, 14);
            this.chkBOH.TabIndex = 4;
            this.chkBOH.UseVisualStyleBackColor = true;
            // 
            // cboLanguage
            // 
            this.cboLanguage.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLanguage.FormattingEnabled = true;
            this.cboLanguage.Location = new System.Drawing.Point(8, 39);
            this.cboLanguage.Margin = new System.Windows.Forms.Padding(4);
            this.cboLanguage.Name = "cboLanguage";
            this.cboLanguage.Size = new System.Drawing.Size(176, 21);
            this.cboLanguage.TabIndex = 6;
            // 
            // lblLanguage
            // 
            this.lblLanguage.AutoSize = true;
            this.lblLanguage.Location = new System.Drawing.Point(8, 20);
            this.lblLanguage.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLanguage.Name = "lblLanguage";
            this.lblLanguage.Size = new System.Drawing.Size(0, 13);
            this.lblLanguage.TabIndex = 5;
            // 
            // groupBoxFormattingSettings
            // 
            this.groupBoxFormattingSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBoxFormattingSettings.Controls.Add(this.chkAutoFitHeaderRows);
            this.groupBoxFormattingSettings.Controls.Add(this.chkAutoFitColumHeaderRowHeight);
            this.groupBoxFormattingSettings.Controls.Add(this.chkAutoColumnHeading);
            this.groupBoxFormattingSettings.Controls.Add(this.chkAutoPrintRange);
            this.groupBoxFormattingSettings.Controls.Add(this.chkShowFreezePane);
            this.groupBoxFormattingSettings.Controls.Add(this.chkShowPageMembers);
            this.groupBoxFormattingSettings.Location = new System.Drawing.Point(4, 4);
            this.groupBoxFormattingSettings.Margin = new System.Windows.Forms.Padding(4);
            this.groupBoxFormattingSettings.Name = "groupBoxFormattingSettings";
            this.groupBoxFormattingSettings.Padding = new System.Windows.Forms.Padding(4);
            this.groupBoxFormattingSettings.Size = new System.Drawing.Size(312, 177);
            this.groupBoxFormattingSettings.TabIndex = 0;
            this.groupBoxFormattingSettings.TabStop = false;
            // 
            // chkAutoFitHeaderRows
            // 
            this.chkAutoFitHeaderRows.AutoSize = true;
            this.chkAutoFitHeaderRows.Location = new System.Drawing.Point(8, 145);
            this.chkAutoFitHeaderRows.Margin = new System.Windows.Forms.Padding(4);
            this.chkAutoFitHeaderRows.Name = "chkAutoFitHeaderRows";
            this.chkAutoFitHeaderRows.Size = new System.Drawing.Size(15, 14);
            this.chkAutoFitHeaderRows.TabIndex = 8;
            this.chkAutoFitHeaderRows.UseVisualStyleBackColor = true;
            // 
            // chkAutoFitColumHeaderRowHeight
            // 
            this.chkAutoFitColumHeaderRowHeight.AutoSize = true;
            this.chkAutoFitColumHeaderRowHeight.Location = new System.Drawing.Point(8, 121);
            this.chkAutoFitColumHeaderRowHeight.Margin = new System.Windows.Forms.Padding(4);
            this.chkAutoFitColumHeaderRowHeight.Name = "chkAutoFitColumHeaderRowHeight";
            this.chkAutoFitColumHeaderRowHeight.Size = new System.Drawing.Size(15, 14);
            this.chkAutoFitColumHeaderRowHeight.TabIndex = 7;
            this.chkAutoFitColumHeaderRowHeight.UseVisualStyleBackColor = true;
            // 
            // chkAutoColumnHeading
            // 
            this.chkAutoColumnHeading.AutoSize = true;
            this.chkAutoColumnHeading.Location = new System.Drawing.Point(8, 96);
            this.chkAutoColumnHeading.Margin = new System.Windows.Forms.Padding(4);
            this.chkAutoColumnHeading.Name = "chkAutoColumnHeading";
            this.chkAutoColumnHeading.Size = new System.Drawing.Size(15, 14);
            this.chkAutoColumnHeading.TabIndex = 3;
            this.chkAutoColumnHeading.UseVisualStyleBackColor = true;
            // 
            // chkAutoPrintRange
            // 
            this.chkAutoPrintRange.AutoSize = true;
            this.chkAutoPrintRange.Location = new System.Drawing.Point(8, 71);
            this.chkAutoPrintRange.Margin = new System.Windows.Forms.Padding(4);
            this.chkAutoPrintRange.Name = "chkAutoPrintRange";
            this.chkAutoPrintRange.Size = new System.Drawing.Size(15, 14);
            this.chkAutoPrintRange.TabIndex = 2;
            this.chkAutoPrintRange.UseVisualStyleBackColor = true;
            // 
            // chkShowFreezePane
            // 
            this.chkShowFreezePane.AutoSize = true;
            this.chkShowFreezePane.Location = new System.Drawing.Point(8, 47);
            this.chkShowFreezePane.Margin = new System.Windows.Forms.Padding(4);
            this.chkShowFreezePane.Name = "chkShowFreezePane";
            this.chkShowFreezePane.Size = new System.Drawing.Size(15, 14);
            this.chkShowFreezePane.TabIndex = 1;
            this.chkShowFreezePane.UseVisualStyleBackColor = true;
            // 
            // chkShowPageMembers
            // 
            this.chkShowPageMembers.AutoSize = true;
            this.chkShowPageMembers.Location = new System.Drawing.Point(8, 22);
            this.chkShowPageMembers.Margin = new System.Windows.Forms.Padding(4);
            this.chkShowPageMembers.Name = "chkShowPageMembers";
            this.chkShowPageMembers.Size = new System.Drawing.Size(15, 14);
            this.chkShowPageMembers.TabIndex = 0;
            this.chkShowPageMembers.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.groupBoxColors);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Margin = new System.Windows.Forms.Padding(4);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(4);
            this.tabPage2.Size = new System.Drawing.Size(323, 380);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(331, 406);
            this.tabControl1.TabIndex = 1;
            // 
            // colorDialog1
            // 
            this.colorDialog1.AllowFullOpen = false;
            this.colorDialog1.SolidColorOnly = true;
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(172, 416);
            this.cmdOk.Margin = new System.Windows.Forms.Padding(4);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 9;
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(255, 416);
            this.cmdCancel.Margin = new System.Windows.Forms.Padding(4);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 10;
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // frmOptions
            // 
            this.AcceptButton = this.cmdOk;
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.cmdCancel;
            this.ClientSize = new System.Drawing.Size(337, 452);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.cmdCancel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmOptions";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.groupBoxColors.ResumeLayout(false);
            this.groupBoxColors.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.groupBoxTheme.ResumeLayout(false);
            this.groupBoxTheme.PerformLayout();
            this.groupBoxProgramSettings.ResumeLayout(false);
            this.groupBoxProgramSettings.PerformLayout();
            this.groupBoxFormattingSettings.ResumeLayout(false);
            this.groupBoxFormattingSettings.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxColors;
        private System.Windows.Forms.TextBox txtMemberTagNoteColor;
        private System.Windows.Forms.Label lblMemberTagNoteColor;
        private System.Windows.Forms.TextBox txtSystemLockColor;
        private System.Windows.Forms.TextBox txtForwardPlannableProtectedColor;
        private System.Windows.Forms.TextBox txtUserLockColor;
        private System.Windows.Forms.TextBox txtProtectedColor;
        private System.Windows.Forms.TextBox txtNonPlannableProtectedColor;
        private System.Windows.Forms.Label lblUserLockColor;
        private System.Windows.Forms.Label lblProtectedColor;
        private System.Windows.Forms.Label lblSystemLockColor;
        private System.Windows.Forms.Label lblForwardPlannableProtectedColor;
        private System.Windows.Forms.Label lblNonPlannableProtectedColor;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBoxProgramSettings;
        private System.Windows.Forms.CheckBox chkBOH;
        private System.Windows.Forms.ComboBox cboLanguage;
        private System.Windows.Forms.Label lblLanguage;
        private System.Windows.Forms.GroupBox groupBoxFormattingSettings;
        private System.Windows.Forms.CheckBox chkAutoFitHeaderRows;
        private System.Windows.Forms.CheckBox chkAutoFitColumHeaderRowHeight;
        private System.Windows.Forms.CheckBox chkAutoColumnHeading;
        private System.Windows.Forms.CheckBox chkAutoPrintRange;
        private System.Windows.Forms.CheckBox chkShowFreezePane;
        private System.Windows.Forms.CheckBox chkShowPageMembers;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.ColorDialog colorDialog1;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.GroupBox groupBoxTheme;
        private System.Windows.Forms.ComboBox comboBoxSkin;
        private System.Windows.Forms.Label label1;


    }
}