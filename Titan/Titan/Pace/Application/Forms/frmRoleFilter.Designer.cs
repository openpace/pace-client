using Titan.Pace.Application.Controls;

namespace Titan.Pace.Application.Forms
{
    /// <summary>
    /// 
    /// </summary>
    partial class frmRoleFilter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        protected internal RoleFilter roleFilter;

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.roleFilter = new RoleFilter();
            this.SuspendLayout();
            // 
            // roleFilter
            // 
            this.roleFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.roleFilter.Location = new System.Drawing.Point(0, 0);
            this.roleFilter.Margin = new System.Windows.Forms.Padding(4);
            this.roleFilter.Name = "roleFilter";
            this.roleFilter.Size = new System.Drawing.Size(363, 510);
            this.roleFilter.SuppressInvalidInxChk = true;
            this.roleFilter.TabIndex = 0;
            this.roleFilter.User = null;
            // 
            // frmRoleFilter
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(363, 510);
            this.Controls.Add(this.roleFilter);
            this.DoubleBuffered = true;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmRoleFilter";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmRoleFilter_Load);
            this.Shown += new System.EventHandler(this.frmRoleFilter_Shown);
            this.ResumeLayout(false);

        }

        #endregion

    }
}