#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Windows.Forms;
using Titan.Properties;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmSort : Form
    {
        public frmSort()
        {
            try
            {
                InitializeComponent();
                this.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Sort");
                this.Icon = Resources.PIcon;        
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void ShowDialogifDataRangeSelected()
        {
            try
            {
                if (PafApp.GetViewMngr().ShowSortForm == true)
                {
                    this.ShowDialog();
                }
                else  //No data rows selected
                {
                    MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.SelectRangeFirst"), 
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        System.Windows.Forms.MessageBoxButtons.OK,
                        System.Windows.Forms.MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        private void frmSort_Load(object sender, EventArgs e)
        {

        }

        private void sort1_Load(object sender, EventArgs e)
        {

        }
    }
}