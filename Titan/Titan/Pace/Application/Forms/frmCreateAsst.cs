﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows.Forms;
using DevExpress.Data;
using DevExpress.Data.PivotGrid;
using DevExpress.Utils;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using DevExpress.XtraPivotGrid;
using DevExpress.XtraTab;
using DevExpress.XtraTab.ViewInfo;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Titan.Pace.Application.Controls.DevExpress;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.Pace.Statistics;
using Titan.PafService;
using Titan.Palladium;
using Titan.Palladium.DataStructures;
using Cursors = System.Windows.Forms.Cursors;
using MouseEventArgs = System.Windows.Forms.MouseEventArgs;

namespace Titan.Pace.Application.Forms
{

    internal partial class frmCreateAsst : XtraForm
    {
        [DllImport("user32.dll")]
        static extern short GetKeyState(int nVirtKey);

        
        private readonly SimpleDimTrees _simpleDimTrees;
        private readonly string _memberKeyColumnName;
        private readonly string _parentKeyColumnName;
        private readonly string _levelColumnName;
        private readonly string _generationColumnName;
        private readonly List<string> _levelGenColumns;
        private bool _locationSelected;
        private bool _productSelected;
        private readonly string _memberNameColumnName;
        private readonly string _versionDimension;
        private readonly string _measureDimension;
        private readonly string _timeDimension;
        private readonly string _yearsDimension;
        private readonly string _planTypeDimension;
        private readonly string _planVersion;
        private readonly Clustering _clusterDataset;
        private const string TimeHorizonDim = PafAppConstants.TIME_HORIZON_DIM;
        private const string ProductDim = "Product";
        private const string LocationDim = "Location";
        private const string ClusterDim = "Cluster";
        private const string StorePrefix = "Store";
        private DataTable _pivotData;
        private DataTable _pivotGridData;
        private clusteredResultSetResponse _resultSet;
        private readonly string _saveToPath;
        private bool _firstRun = true;
        private List<string> _measures;
        private TreeListNode _savedMeasuresNode;
        private Dictionary<int, double> _aggregateData;
        private bool _uiUpdating = false;
        private bool _customCluster = false;
        private bool _customClusterSpecified = false;
        private string[] _axisPriority = null;
        private Dictionary<string, string>  _fixedDims = new Dictionary<string, string>(10);
        private const string NA = "N/A";

        public frmCreateAsst()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            InitializeComponent();

            Icon = Properties.Resources.PIcon;

            _locationSelected = false;
            _productSelected = false;
            _simpleDimTrees = PafApp.SimpleDimTrees;
            _levelGenColumns = new List<string>(2);
            _levelColumnName = _simpleDimTrees.LevelColumName;
            _generationColumnName = _simpleDimTrees.GenerationColumName;
            _levelGenColumns.Add(_levelColumnName);
            _levelGenColumns.Add(_generationColumnName);
            _memberKeyColumnName = _simpleDimTrees.MemberKeyColumn;
            _parentKeyColumnName = _simpleDimTrees.ParentKeyColum;
            _memberNameColumnName = _simpleDimTrees.MemberColumName;
            _planVersion = PafApp.PlanningVersion;
            _versionDimension = PafApp.VersionDimension;
            _measureDimension = PafApp.MeasureDimension;
            _timeDimension = PafApp.TimeDimension;
            _yearsDimension = PafApp.YearsDimension;
            _planTypeDimension = PafApp.PlanTypeDimension;
            _saveToPath = System.Windows.Forms.Application.UserAppDataPath;
            // Create a file that the application will store user specific data in.

            string fileName = Path.Combine(_saveToPath, "cluster.xml");
            PafApp.GetLogger().InfoFormat("Loading cluster dataset from: {0}", new object[]{fileName});
            _clusterDataset = new Clustering();
            if (File.Exists(fileName))
            {
                _clusterDataset.EnforceConstraints = false;
                _clusterDataset.ReadXml(fileName, XmlReadMode.Auto);
                _clusterDataset.EnforceConstraints = true;
                //Time
                List<string> time = _clusterDataset.GetSettingsNames(TimeHorizonDim);
                comboBoxEditTime.AddSelectionItems(time, String.Empty);
                //Measures
                List<string> measures = _clusterDataset.GetSettingsNames(_measureDimension);
                comboBoxEditMeasures.AddSelectionItems(measures, String.Empty);
                //Product
                List<string> product = _clusterDataset.GetSettingsNames(ProductDim);
                comboBoxEditProducts.AddSelectionItems(product, String.Empty);
                //Location
                List<string> location = _clusterDataset.GetSettingsNames(LocationDim);
                comboBoxEditLocations.AddSelectionItems(location, String.Empty);
            }

            DataTable dataTable = _simpleDimTrees.ToTreeListDataTable(TimeHorizonDim);

            timeTreeList.BindData(dataTable, _memberKeyColumnName, _parentKeyColumnName, TimeHorizonDim,_levelGenColumns, true, true);
            var result = PafApp.CreateAssortment();
            string[] ruleSets = result.ruleSetNames;
            string defaultRuleset = result.defaultRuleSetName;
            comboBoxEditRuleSet.Properties.Items.Clear();
            foreach (string rule in ruleSets)
            {
                if (String.IsNullOrWhiteSpace(rule)) continue;
                comboBoxEditRuleSet.Properties.Items.Add(rule);
            }
            int defaultIdx = comboBoxEditRuleSet.Properties.Items.IndexOf(defaultRuleset);
            comboBoxEditRuleSet.SelectedIndex = defaultIdx;



            CreateHierarchyTab(ProductDim, prodAttTreeList, prodTreeList);
            CreateHierarchyTab(LocationDim, locAttTreeList, locTreeList);
            measuresTreeList.Columns.Clear();
            measuresTreeList.ClearNodes();
            CreateHierarchyTab(_measureDimension, null, measuresTreeList);

            //var gc = measuresTreeList.Columns.ColumnByFieldName("Default");
            //FilterCondition fc = new FilterCondition(FilterConditionEnum.Contains, gc, "Sales");
            //measuresTreeList.FilterConditions.Add(fc);

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("frmCreateAsst runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Gets cluster results from the server and displays then on a Pivot Grid and DataGrid.
        /// </summary>
        /// <param name="adHoc"></param>
        /// <remarks>This is for a demo and needs to be refactored.</remarks>
        private bool GetClusterResults(bool adHoc)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool result = true;
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;
            Cursor = Cursors.WaitCursor;
            try
            {
                int numberOfClusters = Convert.ToInt32(spinEditClusters.Value);

                List<string>[] timePeriods = GetTimePeriods();
                if (timePeriods == null) return false;

                List<TreeListNode> nodes = measuresTreeList.GetCheckedNodes();
                List<String> serviceMeasures = new List<string>(nodes.Count);
                serviceMeasures.AddRange(nodes.Select(n => TreeListNodeExtension.GetText(n, _memberNameColumnName)));

                IEnumerable<clusteredResultSetSaveRequestEntry> clusterKeysTemp = GetClusters();
                List<clusterResultSetRequestEntry> clusterKeys = new List<clusterResultSetRequestEntry>();
                if (_customCluster && clusterKeysTemp != null)
                {
                    foreach (var v in clusterKeysTemp)
                    {
                        clusterResultSetRequestEntry entry = new clusterResultSetRequestEntry();
                        entry.key = v.key;
                        entry.value = Convert.ToInt32(v.value);
                        entry.valueSpecified = true;
                        clusterKeys.Add(entry);
                    }
                }

                _resultSet = PafApp.GetClusteredResult(txtClusterLabel.Text,
                                          pafDimSpecExtension.CreateNew(_timeDimension, timePeriods[1]),
                                          pafDimSpecExtension.CreateNew(_yearsDimension, timePeriods[0]),
                                          pafDimSpecExtension.CreateNew(_measureDimension, serviceMeasures),
                                          pafDimSpecExtension.CreateNew(_versionDimension, _planVersion),
                                          numberOfClusters,
                                          Convert.ToInt32(spinEditNumIterations.Value),
                                          comboBoxEditRuleSet.SelectedItem.ToString(),
                                          clusterKeys.ToArray());


                List<clusteredResultSetResponseEntry> clusterNumbers = _resultSet.clusters.ToList();
                pivotGridCluster.DataSource = null;

                //Create the datatable structure.
                _pivotData = new DataTable { TableName = txtClusterLabel.Text };
                _pivotData.Columns.Add(ClusterDim, typeof(string));
                _pivotData.Columns.Add(ProductDim, typeof(string));
                _pivotData.Columns.Add(LocationDim, typeof(string));
                _pivotData.Columns.Add(_yearsDimension, typeof(string));
                _pivotData.Columns.Add(_timeDimension, typeof(string));
                _pivotData.Columns.Add(_versionDimension, typeof(string));
                _measures = new List<string>(_resultSet.measures.Length);
                foreach (string m in _resultSet.measures)
                {
                    _pivotData.Columns.Add(m, typeof(double));
                    _measures.Add(m);
                }

                if (String.IsNullOrEmpty(PafAppConstants.ELEMENT_DELIM))
                {
                    PafAppConstants.ELEMENT_DELIM = PafAppConstants.ELEMENT_DELIM_DEFAULT;
                }

                if (String.IsNullOrEmpty(PafAppConstants.GROUP_DELIM))
                {
                    PafAppConstants.GROUP_DELIM = PafAppConstants.GROUP_DELIM_DEFAULT;
                }

                Intersection[] aggregateCoords = _resultSet.aggregateCoords.ToIntersections();
                Intersection[] levelZeroCoords = _resultSet.level0Coords.ToIntersections();

                Stopwatch stopwatch2 = Stopwatch.StartNew();
                if (aggregateCoords != null && aggregateCoords.Length > 0)
                {
                    List<string> temp = new List<string>();
                    temp.AddRange(aggregateCoords[0].AxisSequence);
                    _axisPriority = temp.ToArray();
                    _aggregateData = new Dictionary<int, double>(aggregateCoords.Length);
                    var data = _resultSet.aggregateData.data;
                    PafApp.GetLogger().InfoFormat("Processing: {0} intersections.", aggregateCoords.Length);
                    int count = 0;
                    foreach (Intersection i in aggregateCoords)
                    {
                        double? d = data[count];
                        if (d.HasValue)
                        {
                            _aggregateData.Add(i.UniqueID, data[count].GetValueOrDefault());
                            if (debugEnabled)
                            {
                                PafApp.GetLogger().DebugFormat("Server Intersection: {0}, has data point: {1}", i.ToShortString(), data[count].GetValueOrDefault());
                            }
                        }
                        count++;
                    }
                }
                else
                {
                    return false;
                }
                stopwatch2.Stop();
                PafApp.GetLogger().InfoFormat("GetClusterResults-Fill AggData runtime: {0} (ms)", stopwatch2.ElapsedMilliseconds.ToString("N0"));


                #region Old Datatable
                ////============Old Datatable population===========
                ////Insert the values (not measure) into the datatable
                //foreach (string location in _resultSet.dimToCluster)
                //{
                //    foreach (string product in _resultSet.dimToMeasure)
                //    {
                //        foreach (string time in _resultSet.time)
                //        {
                //            foreach (string year in _resultSet.years)
                //            {
                //                int clusterNum = clusterNumbers.Where(x => x.key.Equals(location)).Select(r => r.value).FirstOrDefault();
                //                DataRow dr = _pivotData.NewRow();
                //                dr[ClusterDim] = String.Format("{0} {1}", new object[] {ClusterDim, clusterNum});
                //                dr[ProductDim] = product;
                //                dr[LocationDim] = location;
                //                dr[_yearsDimension] = year.Trim();
                //                dr[_timeDimension] = time.Trim();
                //                dr[_versionDimension] = _resultSet.version[0];
                //                _pivotData.Rows.Add(dr);
                //            }
                //        }
                //    }
                //}


                ////Update measure values in datatable
                //List<string> locations = new List<string>(_resultSet.data.Count());
                //locations.AddRange(_resultSet.data.Select(sr => sr.id));
                //int locationCount = 0;
                //foreach (string location in locations)
                //{
                //    string product = "";
                //    string timePer = "";
                //    string measure = "";
                //    //string year = "";
                //    int j = 0;
                //    foreach (var v in _resultSet.header.items)
                //    {
                //        //Parse the headers to get Time, Product, Measure
                //        List<string> headers = v.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).ToList();

                //        if (product == headers[0] && measure == headers[1] && timePer == headers[2]) continue;// && year == headers[3]) continue;

                //        //Get the prod, measure, time from the header string
                //        product = headers[0].Trim();
                //        measure = headers[1].Trim();
                //        timePer = headers[2].Trim();
                //        //year = headers[3].Trim();
                //        //Set the prod, mease, time to temporary vare - for LINQ
                //        var queryProduct = product;
                //        var queryLocation = location;
                //        var queryTime = timePer;
                //        //var queryYear = year;
                //        //Perform the query.
                //        var values = _pivotData.AsEnumerable().Where(c =>
                //            c.Field<string>(ProductDim).Equals(queryProduct) 
                //            && c.Field<string>(LocationDim).Equals(queryLocation) 
                //            && c.Field<string>(_timeDimension).Equals(queryTime)
                //            //&& c.Field<string>(_yearsDimension).Equals(queryYear)
                //            ).ToList();

                //        //Get the data row for the location
                //        stringRow sr = _resultSet.data[locationCount];
                //        //PafApp.GetLogger().InfoFormat("{0}, Cluster {1}:  {2}", new object[]{sr.id, sr.key, sr.items[j]});
                //        //Look thru the query (should only have one result) and update the measure values.
                //        foreach (DataRow dr in values)
                //        {
                //            //PafApp.GetLogger().InfoFormat("Query: {0}, {1}, {2}, {3} - Returned: {4}, {5}, {6}, {7}", new [] { queryLocation, queryProduct, queryTime, queryYear, dr[LocationDim], dr[ProductDim], dr[_timeDimension], dr[_yearsDimension] });
                //            //Get the data from the data array
                //            double value = Convert.ToDouble(sr.items[j]);
                //            //PafApp.GetLogger().InfoFormat("{0}: {1}", new object[] { measure, value });
                //            //Update the measure value in the data table.
                //            dr[measure] = value;
                //        }
                //        //PafApp.GetLogger().Info(_pivotData.ToStringBuilder());
                //        j++;
                //    }
                //    //PafApp.GetLogger().Info(_pivotData.ToStringBuilder());
                //    locationCount++;
                //}

                ////PafApp.GetLogger().Info(_pivotData.ToStringBuilder());

                ////============Old Datatable population===========
                #endregion Old Datatable

                stopwatch2 = Stopwatch.StartNew();
                if (_aggregateData != null)
                {
                    _fixedDims.Clear();
                    _fixedDims.Add(_yearsDimension, _resultSet.years[0]);
                    _fixedDims.Add(_versionDimension, _resultSet.version[0]);
                    _fixedDims.Add(_timeDimension, _timeDimension);
                    _fixedDims.Add(_planTypeDimension, _planTypeDimension);


                    Intersection temp = new Intersection(_axisPriority);
                    temp.SetCoordinate(_yearsDimension, _resultSet.years[0]);
                    temp.SetCoordinate(_timeDimension, _timeDimension);
                    temp.SetCoordinate(_versionDimension, _resultSet.version[0]);

                    foreach (string location in _resultSet.dimToCluster)
                    {
                        var clusterNum = clusterNumbers.Where(x => x.key.Equals(location)).Select(x => x.value).FirstOrDefault();
                        string cluster = String.Format("{0} {1}", new object[] {ClusterDim, clusterNum});
                        foreach (string product in _resultSet.dimToMeasure)
                        {
                            DataRow dr = _pivotData.NewRow();
                            dr[ClusterDim] = cluster;
                            temp.SetCoordinate(ClusterDim, cluster.RemoveSpaces());

                            dr[ProductDim] = product;
                            temp.SetCoordinate(ProductDim, product);

                            dr[LocationDim] = location;
                            temp.SetCoordinate(LocationDim, location);

                            foreach (string measure in _resultSet.measures)
                            {
                                temp.SetCoordinate(_measureDimension, measure);
                                double d;
                                if (_aggregateData != null && _aggregateData.TryGetValue(temp.UniqueID, out d))
                                {
                                    dr[measure] = d;
                                }
                            }
                            _pivotData.Rows.Add(dr);
                        }
                    }
                }
                stopwatch2.Stop();
                PafApp.GetLogger().InfoFormat("GetClusterResults-Fill AggDataTable runtime: {0} (ms)", stopwatch2.ElapsedMilliseconds.ToString("N0"));

                List<string> locations = _pivotData.AsEnumerable().Select(c => c.Field<string>(LocationDim)).Distinct().ToList();

                if (adHoc)
                {
                    DoKmeans(_pivotData, _measures, locations, numberOfClusters, Convert.ToInt32(spinEditNumIterations.Value));
                    IEnumerable<clusteredResultSetSaveRequestEntry> x = GetClusters();
                    _customCluster = true;
                    GetClusterResults(false);
                    _customCluster = false;
                    return true;
                }

                //=========Create the pivot table.
                stopwatch2 = Stopwatch.StartNew();
                pivotGridCluster.Fields.Clear();
                pivotGridCluster.Fields.ClearAndDispose();
                pivotGridCluster.DataSource = _pivotData;
                PivotGridField fieldCluster = CreatePivotColumn(ClusterDim, PivotArea.RowArea, true);
                PivotGridField fieldLocation = CreatePivotColumn(LocationDim, PivotArea.RowArea, true);
                PivotGridField fieldProduct = CreatePivotColumn(ProductDim, PivotArea.RowArea, true);
                PivotGridField fieldYear = CreatePivotColumn(_yearsDimension, PivotArea.FilterArea);
                PivotGridField fieldTime = CreatePivotColumn(_timeDimension, PivotArea.FilterArea);
                PivotGridField fieldVersion = CreatePivotColumn(_versionDimension, PivotArea.FilterArea);

                pivotGridCluster.Fields.AddRange(new[] { fieldCluster, fieldLocation, fieldProduct, fieldYear, fieldTime, fieldVersion });
                foreach (PivotGridField fieldMeasure in _resultSet.measures.Select(m => new PivotGridField(m, PivotArea.DataArea)))
                {
                    string format = _clusterDataset.GetMemberFormat(fieldMeasure.FieldName);
                    fieldMeasure.CellFormat.FormatType = FormatType.Numeric;
                    fieldMeasure.CellFormat.FormatString = !String.IsNullOrWhiteSpace(format) ? format : "N2";
                    fieldMeasure.SummaryType = PivotSummaryType.Custom;
                    pivotGridCluster.Fields.Add(fieldMeasure);
                }

                fieldCluster.AreaIndex = 0;
                fieldLocation.AreaIndex = 1;
                fieldProduct.AreaIndex = 2;
                stopwatch2.Stop();
                PafApp.GetLogger().InfoFormat("GetClusterResults-Pivot Grid Data Bind runtime: {0} (ms)", stopwatch2.ElapsedMilliseconds.ToString("N0"));


                //Create a summary datatable for the Data tab 
                stopwatch2 = Stopwatch.StartNew();
                List<string> aliasColumns = new List<string>();
                string memberName = _memberNameColumnName;
                _pivotGridData =  new DataTable { TableName = txtClusterLabel.Text };
                DataTable aliasTableValues = _simpleDimTrees.ToMeberAliasDataTable(LocationDim, true);
                _pivotGridData.Columns.Add(ClusterDim, typeof(string));
                _pivotGridData.Columns.Add(LocationDim, typeof(string));
                foreach (string m in _measures)
                {
                    _pivotGridData.Columns.Add(m, typeof(double));
                }
                foreach (DataColumn dc in aliasTableValues.Columns.Cast<DataColumn>().Where(dc => !dc.ColumnName.Equals(memberName)))
                {
                    aliasColumns.Add(dc.ColumnName);
                    _pivotGridData.Columns.Add(dc.ColumnName, typeof(string));
                }
                foreach (string location in locations)
                {
                    //Set the prod, mease, time to temporary vare - for LINQ
                    var queryLocation = location;
                    //Perform the query.
                    var currentCluster = _pivotData.AsEnumerable().Where(c => c.Field<string>(LocationDim).Equals(queryLocation)).Select(x =>  x.Field<string>(ClusterDim)).FirstOrDefault();
                    var aliasValues = aliasTableValues.AsEnumerable().FirstOrDefault(x => x.Field<string>(memberName).Equals(queryLocation));

                    List<string> dimTracker = new List<string>(_axisPriority.Length);
                    dimTracker.Clear();
                    dimTracker.AddRange(_axisPriority);
                    dimTracker.Remove(_measureDimension);

                    Intersection temp = new Intersection(_axisPriority);

                    temp.SetCoordinate(LocationDim, location);
                    dimTracker.Remove(LocationDim);

                    temp.SetCoordinate(ClusterDim, currentCluster.RemoveSpaces());
                    dimTracker.Remove(ClusterDim);

                    temp.SetCoordinate(ProductDim, ProductDim);
                    dimTracker.Remove(ProductDim);

                    temp.SetCoordinate(_planTypeDimension, _planTypeDimension);
                    dimTracker.Remove(_planTypeDimension);

                    foreach (var key in _fixedDims)
                    {
                        temp.SetCoordinate(key.Key, key.Value);
                        dimTracker.Remove(key.Key);
                    }

                    if (dimTracker.Count > 0)
                    {
                        foreach (string s in dimTracker.Where(s => s != _measureDimension))
                        {
                            temp.SetCoordinate(s, s);
                        }
                    }

                    DataRow newDr = _pivotGridData.NewRow();
                    newDr[ClusterDim] = currentCluster;
                    newDr[LocationDim] = location;
                    foreach (string m in _measures)
                    {
                        temp.SetCoordinate(_measureDimension, m);
                        double d;
                        if (_aggregateData.TryGetValue(temp.UniqueID, out d))
                        {
                            if(debugEnabled) PafApp.GetLogger().InfoFormat("Summary Intersection: {0}, has value: {1}", temp.ToShortString(), d);
                        }
                        else
                        {
                            PafApp.GetLogger().WarnFormat("Summary Intersection: {0}, no value found.", temp.ToShortString());
                        }
                        newDr[m] = d;
                    }
                    foreach (string at in aliasColumns)
                    {
                        newDr[at] = aliasValues[at];
                    }
                    _pivotGridData.Rows.Add(newDr);

                }
                stopwatch2.Stop();
                PafApp.GetLogger().InfoFormat("GetClusterResults-Create Summary Table runtime: {0} (ms)", stopwatch2.ElapsedMilliseconds.ToString("N0"));

                //====Bind and customize Pivot Data tab================
                gridControlPivotData.DataSource = null;
                gridViewPivotData.Columns.Clear();
                gridControlPivotData.BindData(_pivotGridData);
                foreach (GridColumn c in _measures.Select(m => gridViewPivotData.Columns.ColumnByFieldName(m)).Where(c => c != null))
                {
                    string format = _clusterDataset.GetMemberFormat(c.FieldName);
                    c.DisplayFormat.FormatType = FormatType.Numeric;
                    c.DisplayFormat.FormatString = !String.IsNullOrWhiteSpace(format) ? format : "N2";

                }
                GridColumn columnLocation = gridViewPivotData.Columns.ColumnByFieldName(LocationDim);
                if (columnLocation != null)
                {
                    columnLocation.SortMode = ColumnSortMode.Custom;
                }
                GridColumn columnCluster = gridViewPivotData.Columns.ColumnByFieldName(ClusterDim);
                if (columnCluster != null)
                {
                    columnCluster.SortMode = ColumnSortMode.Custom;
                    UpdatePivotDataGridComboItems();
                    columnCluster.ColumnEdit = repositoryItemComboBox2;
                }

                foreach (GridColumn gc in aliasColumns.Select(at => gridViewPivotData.Columns.ColumnByFieldName(at)))
                {
                    if (!gc.FieldName.Equals(PafAppConstants.ALIAS_DEFAULT_TABLE_NAME))
                    {
                        gc.Visible = false;
                    }
                    else
                    {
                        gc.VisibleIndex = 2;
                    }
                }


                if (gridViewPivotData.SortInfo.Count == 0)
                {
                    gridViewPivotData.SortInfo.ClearAndAddRange(
                        new []
                            {
                                new GridColumnSortInfo(columnCluster, ColumnSortOrder.Ascending),
                                new GridColumnSortInfo(columnLocation, ColumnSortOrder.Ascending)
                            }
                        );
                }
                //========End Pivot Table Creation
            }

            catch (Exception ex)
            {
                result = false;
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                Cursor = Cursors.Default;
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("GetClusterResults runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
            return result;
        }

        void pivotGridCluster_CustomSummary(object sender, PivotGridCustomSummaryEventArgs e)
        {
            Intersection temp = new Intersection(_axisPriority, _axisPriority);
            string cluster = "", location = "";
            foreach (var key in _fixedDims)
            {
                temp.SetCoordinate(key.Key, key.Value);
            }

            PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
            if (ds.RowCount > 0)
            {
                PivotDrillDownDataRow row = ds[0];
                cluster = Convert.ToString(row[ClusterDim]);
                cluster = cluster.RemoveSpaces();
                location = Convert.ToString(row[LocationDim]);
            }
            else
            {
                e.CustomValue = "N/A";
                PafApp.GetLogger().InfoFormat("No rows requested.");
            }


            if (e.DataField != null)
            {
                //PafApp.GetLogger().InfoFormat("Data Field: {0}", e.DataField.FieldName);
                temp.SetCoordinate(_measureDimension, e.DataField.FieldName.RemoveSpaces());
            }

            //if (e.FieldName != null)
            //{
            //    //PafApp.GetLogger().InfoFormat("Field Name: {0}", e.FieldName);
            //}

            if (e.ColumnField != null)
            {
                temp.SetCoordinate(e.ColumnField.FieldName, e.ColumnFieldValue.ToString().RemoveSpaces());
            }

            if (e.RowField != null)
            {
                temp.SetCoordinate(e.RowField.FieldName, e.RowFieldValue.ToString().RemoveSpaces());
                String root = _simpleDimTrees.GetRootMemberName(LocationDim);
                switch (e.RowField.FieldName)
                {
                    case ProductDim:
                        temp.SetCoordinate(LocationDim, location);
                        temp.SetCoordinate(ClusterDim, cluster);
                        //TTN-2211
                        if (root.EqualsIgnoreCase(location))
                        {
                            temp.SetCoordinate(LocationDim, LocationDim);
                        }
                        break;
                    case LocationDim:
                        temp.SetCoordinate(ClusterDim, cluster);
                        //TTN-2211
                        if (root.EqualsIgnoreCase(location))
                        {
                            temp.SetCoordinate(LocationDim, LocationDim);
                        }
                        break;
                }
            }

            double d;
            if (_aggregateData != null && _aggregateData.TryGetValue(temp.UniqueID, out d))
            {
                e.CustomValue = d;
                if(PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().DebugFormat("Pivot grid requesting Intersection: {0}, value found: {1}", temp.ToShortString(), d);
            }
            else
            {
                e.CustomValue = NA;
                PafApp.GetLogger().WarnFormat("Pivot grid requesting Intersection: {0}, no value found.", temp.ToShortString());
            }
        }

        /// <summary>
        /// Creates a PivotGridColumn
        /// </summary>
        /// <param name="fieldName">Data table field</param>
        /// <param name="pivotArea">Location of the column</param>
        /// <param name="customSort">True to enable a custom sorter.</param>
        private PivotGridField CreatePivotColumn(string fieldName, PivotArea pivotArea, bool customSort = false)
        {
            PivotGridField field = new PivotGridField(fieldName, pivotArea);
            if(customSort) field.SortMode = PivotSortMode.Custom;
            field.Options.AllowDrag = DefaultBoolean.False;
            field.Options.AllowEdit = false;
            field.Options.ShowInCustomizationForm = false;
            field.Options.ShowInPrefilter = false;
            field.Options.AllowDragInCustomizationForm = DefaultBoolean.False;

            return field;
        }

        /// <summary>
        /// Runs a K-Means cluster assortment on a data table.
        /// </summary>
        /// <param name="dt">DataTable</param>
        /// <param name="measures">List of measure</param>
        /// <param name="locations">List of locations</param>
        /// <param name="numClusters">Num of clusters to break into</param>
        /// <param name="numOfIterations">Num of iterations to run.</param>
        private void DoKmeans(DataTable dt, ICollection<string> measures, ICollection<string> locations, int numClusters, int numOfIterations)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            List<List<double>> outer = new List<List<double>>();
            Dictionary<int, string> rowMap = new Dictionary<int, string>(locations.Count);
            Dictionary<string, int> storeMap = new Dictionary<string, int>(locations.Count);
            int j = 0;
            foreach (string location in locations)
            {
                //Set the prod, mease, time to temporary vare - for LINQ
                var queryLocation = location;
                //Perform the query.
                var values = dt.AsEnumerable().Where(c => c.Field<string>(LocationDim).Equals(queryLocation)).ToList();
                Dictionary<string, double> locationValues = measures.ToDictionary(m => m, m => 0.0);
                //Sum the data rows.
                foreach (DataRow dr in values)
                {
                    foreach (string m in measures)
                    {
                        double val = locationValues[m];
                        double data = Convert.ToDouble(dr[m]);
                        locationValues[m] = val + data;
                    }
                }
                rowMap.Add(j, location);
                storeMap.Add(location, j);
                List<double> inner = new List<double>(measures.Count);
                inner.AddRange(locationValues.Select(k => k.Value));
                outer.Add(inner);
                j++;
            }
            double[][] rawData = new double[outer.Count][];
            for (int i = 0; i < rawData.Length; i++)
            {
                rawData[i] = outer[i].ToArray();
            }

            Stopwatch stopwatch1 = Stopwatch.StartNew();
            int[] clustering = KMeans.Cluster(rawData, numClusters, measures.Count, numOfIterations);
            stopwatch1.Stop();
            PafApp.GetLogger().InfoFormat("KMeans algorithm runtime: {0} (ms)", new object[] { stopwatch1.ElapsedMilliseconds.ToString("N0") });

            var storeCluster = (from s in _pivotData.AsEnumerable()
                                 group s by new
                                 {
                                     Cluster = s.Field<string>(ClusterDim),
                                     Store = s.Field<string>(LocationDim)
                                 }
                                     into g
                                     select new
                                     {
                                         g.Key.Cluster,
                                         g.Key.Store,
                                     }).ToList();

            foreach (var storeRow in storeCluster)
            {
                string store = storeRow.Store;
                int storeIndex;
                storeMap.TryGetValue(store, out storeIndex);
                int newCluster = clustering[storeIndex] + 1;
                string cluster = String.Format("{0} {1}", new object[] { ClusterDim, newCluster });
                var values = dt.AsEnumerable().Where(c => c.Field<string>(LocationDim).Equals(store)).ToList();
                foreach (var dr in values)
                {
                    dr[ClusterDim] = cluster;
                }

            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("DoKmeans runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Updates the "Results" grid for Locations and Products.
        /// </summary>
        /// <param name="dimension">Dim Name.</param>
        /// <param name="baseTree">Base member XtraTreeList </param>
        /// <param name="attribTree">Attribute member XtraTreeList</param>
        /// <param name="dxDataGrid">Results data grid.</param>
        /// <param name="level">Tree Level</param>
        /// <returns></returns>
        private bool UpdateResultGrid(string dimension, TreeList baseTree, TreeList attribTree, GridControl dxDataGrid, int level)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            bool result = false;

            //Get the checked base tree nodes.
            List<string> checkedNodes = baseTree.GetCheckedNodes(_memberNameColumnName);
            pafDimSpec dimSpec = pafDimSpecExtension.CreateNew(dimension);
            if (checkedNodes != null && checkedNodes.Count > 0)
            {
                dimSpec.expressionList = checkedNodes.ToArray();
            }

            //Add root level nodes.
            List<pafDimSpec> attribSpecs = (from TreeListNode attrRoot in attribTree.Nodes 
                                            where attrRoot.Checked 
                                            select attrRoot.GetDisplayText(_memberNameColumnName) 
                                            into nodeName 
                                            select pafDimSpecExtension.CreateNew(nodeName, nodeName.ToSingleList<string>())).ToList();
            //Get the checked attribute tree nodes
            foreach (TreeListNode attrRoot in attribTree.Nodes)
            {
                List<string> attrSels = attribTree.GetCheckedNodes(attrRoot.Nodes, _memberNameColumnName);
                if (attrSels != null && attrSels.Count > 0)
                {
                    attribSpecs.Add(pafDimSpecExtension.CreateNew(attrRoot.GetDisplayText(_memberNameColumnName), attrSels));
                }
            }

            paceResultSetResponse resultSet = PafApp.GetFilteredResultSetResponse(attribSpecs, dimSpec, level);

            if (resultSet != null && resultSet.data != null)
            {
                DataTable table = new DataTable();


                List<string> aliasColumns = new List<string>();
                string memberName = _simpleDimTrees.Members.MemberNameColumn.ColumnName;
                DataTable aliasTableValues = _simpleDimTrees.ToMeberAliasDataTable(dimension, true);

                foreach (String hdr in resultSet.header.items)
                {
                    table.Columns.Add(hdr, typeof (string));
                }
                foreach (DataColumn dc in aliasTableValues.Columns.Cast<DataColumn>().Where(dc => !dc.ColumnName.Equals(memberName)))
                {
                    aliasColumns.Add(dc.ColumnName);
                    table.Columns.Add(dc.ColumnName, typeof(string));
                }

                foreach (stringRow r in resultSet.data)
                {
                    List<object> rowValues = new List<object>(r.items);
                    var aliasValues = aliasTableValues.AsEnumerable().FirstOrDefault(x => x.Field<string>(memberName).Equals(rowValues[0]));
                    if (aliasValues != null)
                    {
                        rowValues.AddRange(aliasColumns.Select(at => aliasValues[at]));
                    }
                    table.LoadDataRow(rowValues.ToArray(), true);
                }
                int count = table.Rows.Count;
                dxDataGrid.DataSource = table;

                GridView gv = (GridView)dxDataGrid.Views[0];
                foreach (GridColumn gc in aliasColumns.Select(at => gv.Columns.ColumnByFieldName(at)))
                {
                    if (!gc.FieldName.Equals(PafAppConstants.ALIAS_DEFAULT_TABLE_NAME))
                    {
                        gc.Visible = false;
                    }
                    else
                    {
                        gc.VisibleIndex = 1;
                    }
                }


                switch (dimension)
                {
                    case ProductDim:
                        result = true;
                        break;
                    case LocationDim:
                        if (spinEditClusters.Value > count)
                        {
                            spinEditClusters.Value = count;
                        }
                        spinEditClusters.Properties.MaxValue = count;
                        result = true;
                        break;
                }
            }
            else
            {
                dxDataGrid.DataSource = null;
            }
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("UpdateResultGrid runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
            return result;
        }

        /// <summary>
        /// Returns an array of Time Horizon members
        /// Null if an error occured, Index 0 has years, Index 1 has time
        /// </summary>
        /// <returns>Null if an error occured, Index 0 has years, Index 1 has time</returns>
        private List<string>[] GetTimePeriods()
        {

            List<TreeListNode> timeHorNodes = timeTreeList.GetCheckedNodes();
            if (timeHorNodes == null) return null;

            //Get list of all time periods
            int currentLevel = Convert.ToInt32(timeHorNodes[0].GetValue(_levelColumnName));
            var timeMembers = _simpleDimTrees.GetMembersAtLevel(TimeHorizonDim, currentLevel);

            List<string> timeSliceYears = new List<string>();
            List<string> timeSliceTime = new List<string>();
            List<string>[] ret = new List<string>[2];

            if (timeHorNodes.Count == 1)
            {
                TimeSlice ts = new TimeSlice(timeHorNodes[0].GetText(_memberNameColumnName));
                timeSliceYears.Add(ts.Year);
                timeSliceTime.Add(ts.Period);
            }
            else
            {
                string firstNode = timeHorNodes[0].GetText(_memberNameColumnName);
                string lastNode = timeHorNodes[1].GetText(_memberNameColumnName);
                List<string> tMembers = new List<string>();
                bool running = false;
                foreach (var node in timeMembers)
                {
                    if (node.Equals(firstNode) || running)
                    {
                        running = true;
                        tMembers.Add(node);
                    }
                    if (node.Equals(lastNode))
                    {
                        break;
                    }
                }
                foreach (TimeSlice t in tMembers.Select(x => new TimeSlice(x)))
                {
                    if (!timeSliceTime.Contains(t.Period)) timeSliceTime.Add(t.Period);
                    if (!timeSliceYears.Contains(t.Year)) timeSliceYears.Add(t.Year);
                }
            }
            ret[0] = timeSliceYears;
            ret[1] = timeSliceTime;
            return ret;
        }

        /// <summary>
        /// Create a hierarchy (Location or Product) tab.
        /// </summary>
        /// <param name="dim">Dimension Name</param>
        /// <param name="tlattr"></param>
        /// <param name="tldim"></param>
        private void CreateHierarchyTab(String dim, TreeList tlattr, TreeList tldim)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            if (tlattr != null)
            {
                tlattr.OptionsSelection.MultiSelect = true;
                tlattr.OptionsBehavior.Editable = false;

                //Attributes
                List<string> attributes = _simpleDimTrees.GetAssociatedAttributeNames(dim);
                DataTable dt = _simpleDimTrees.ToTreeListDataTable(attributes);
                tlattr.BindData(dt, _memberKeyColumnName, _parentKeyColumnName, dim, _levelGenColumns);
            }

            //Base Dim
            tldim.OptionsSelection.MultiSelect = true;
            tldim.OptionsBehavior.Editable = false;
            tldim.BindData(_simpleDimTrees.ToTreeListDataTable(dim), _memberKeyColumnName, _parentKeyColumnName, dim, _levelGenColumns);

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("CreateHierarchyTab: {0}, runtime: {1} (ms)", new object[] { dim, stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

        /// <summary>
        /// Runs a validation and sets the Cluster Algorthim tab state.
        /// </summary>
        /// <returns></returns>
        public bool CheckFormState()
        {
            dxValidationProvider1.Validate();
            bool valid;
            if (_productSelected && _locationSelected
                && !String.IsNullOrEmpty(txtClusterLabel.Text)
                && timeTreeList.GetCheckedNodes().Count > 0
                && measuresTreeList.GetCheckedNodes().Count > 0)
            {
                tabCluster.PageEnabled = true;
                valid = true;
            }
            else
            {
                tabCluster.PageEnabled = false;
                valid = false;
            }
            return valid;
        }

        /// <summary>
        /// Custom code to Sort the "ClusterDim" column.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns></returns>
        private int? SortClusterColumn(object value1, object value2)
        {
            try
            {
                string str1 = value1.ToString().Replace(ClusterDim, String.Empty).Trim();
                int intValue1;
                if (!int.TryParse(str1, out intValue1))
                {
                    return null;
                }

                string str2 = value2.ToString().Replace(ClusterDim, String.Empty).Trim();
                int intValue2;
                if (!int.TryParse(str2, out intValue2))
                {
                    return null;
                }

                return Comparer<Int32>.Default.Compare(intValue1, intValue2);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Custom code to Sort the "LocationDim" column.
        /// </summary>
        /// <param name="value1"></param>
        /// <param name="value2"></param>
        /// <returns></returns>
        private int? SortLocationColumn(object value1, object value2)
        {
            try
            {
                if (! value1.ToString().Contains("Store")) return null;

                string str1 = value1.ToString().Replace(StorePrefix, String.Empty).Trim();
                int intValue1;
                if (!int.TryParse(str1, out intValue1))
                {
                    return null;
                }

                string str2 = value2.ToString().Replace(StorePrefix, String.Empty).Trim();
                int intValue2;
                if (!int.TryParse(str2, out intValue2))
                {
                    return null;
                }


                //int intValue1 = Convert.ToInt32(value1.ToString().Replace(StorePrefix, String.Empty).Trim());
                //int intValue2 = Convert.ToInt32(value2.ToString().Replace(StorePrefix, String.Empty).Trim());
                return Comparer<Int32>.Default.Compare(intValue1, intValue2);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Saves dimension selections to xml
        /// </summary>
        private void SaveClusterDataset()
        {
            string fileName = Path.Combine(System.Windows.Forms.Application.UserAppDataPath, "cluster.xml");
            PafApp.GetLogger().InfoFormat("Saving cluster dataset to: {0}", new object[] { fileName });
            _clusterDataset.WriteXml(fileName);

            _clusterDataset.Clear();
            _clusterDataset.EnforceConstraints = false;
            PafApp.GetLogger().InfoFormat("Loading cluster dataset from: {0}", new object[] { fileName });
            _clusterDataset.ReadXml(fileName, XmlReadMode.Auto);
            _clusterDataset.EnforceConstraints = true;

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimension"></param>
        /// <param name="treeList"></param>
        /// <param name="combo"></param>
        private void SaveDimensionsSelections(string dimension, TreeList treeList, ComboBoxEdit combo)
        {
            int dimKey = _clusterDataset.AddDimension(dimension);
            int setKey = _clusterDataset.GetSettingKey(dimKey, combo.Text);
            //Key Exists
            if (setKey > -1)
            {
                _clusterDataset.RemoveSelections(setKey, dimKey);
            }

            setKey = _clusterDataset.AddSettings(dimKey, combo.Text, -1);
            List<TreeListNode> nodes = treeList.GetCheckedNodes();
            foreach (TreeListNode node in nodes)
            {
                _clusterDataset.AddSelection(setKey, dimKey, node.GetSimpleTreeText(_simpleDimTrees));
            }
        }

        private void SaveAttributeDimensionsSelections(TreeList treeList, ComboBoxEdit combo)
        {
            List<TreeListNode> nodes = treeList.GetCheckedNodes();
            foreach (TreeListNode node in nodes)
            {
                TreeListNode parent = node.RootNode;
                string dimName = parent.GetSimpleTreeText(_simpleDimTrees);
                int dimKey = _clusterDataset.AddDimension(dimName);
                int setKey = _clusterDataset.AddSettings(dimKey, combo.Text, -1);
                _clusterDataset.AddSelection(setKey, dimKey, node.GetSimpleTreeText(_simpleDimTrees));
            }
        }

        private void SelectSavedMembers(TreeList tree, IEnumerable<string> selections)
        {
            tree.UncheckAll();
            foreach (string s in selections)
            {
                tree.CheckNode(s, _memberNameColumnName.ToSingleList(), false, true, false);
            }
            List<TreeListNode> selectedNode = tree.GetCheckedNodes();
            if (selectedNode != null && selectedNode.Count > 0)
            {
                tree.MakeNodesVisible(selectedNode);
            }
        }

        private void SelectSaveMembersWithAttributes(string dimensionName, TreeList baseDim, TreeList attributeDims, ComboBoxEdit comboBox)
        {
            if (String.IsNullOrEmpty(comboBox.SelectedText))
            {
                baseDim.UncheckAll();
                attributeDims.UncheckAll();
            }
            List<string> settings = _clusterDataset.GetSelections(dimensionName, comboBox.SelectedText);
            if (settings != null && settings.Count > 0)
            {
                SelectSavedMembers(baseDim, settings);
            }

            List<TreeListNode> rootNodes = attributeDims.RootNodes();
            foreach (TreeListNode node in rootNodes)
            {
                string dim = node.GetSimpleTreeText(_simpleDimTrees);
                settings = _clusterDataset.GetSelections(dim, comboBox.SelectedText);
                if (settings != null && settings.Count > 0)
                {
                    SelectSavedMembers(attributeDims, settings);
                }
            }
        }



        private void btnSave_Click(object sender, EventArgs e)
        {
            string fileName = Path.Combine(System.Windows.Forms.Application.UserAppDataPath, "cluster.xml");
            PafApp.GetLogger().InfoFormat("Saving cluster dataset to: {0}", new object[] { fileName });
            _clusterDataset.WriteXml(fileName);

            IEnumerable<clusteredResultSetSaveRequestEntry> clusterKeys = GetClusters();

            pafResponse pafResponse = null;

            try
            {
               pafResponse = PafApp.SaveClusteredResultSet(txtClusterLabel.Text,
               LocationDim,
               _resultSet.dimToCluster,
               ProductDim,
               _resultSet.dimToMeasure,
               _resultSet.measures,
               _resultSet.time,
               _resultSet.version,
               _resultSet.years,
               clusterKeys,
               _resultSet.header);

                //XtraMessageBox.Show(pafResponse.responseMsg, null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                MessageBox.Show(pafResponse.responseMsg,
                                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                                MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                PaceMessageBox.Show(this, ex, MessageBoxButtons.OK);
                //PafApp.MessageBox().Show(ex);
            }
           
            Hide();
        }

        private IEnumerable<clusteredResultSetSaveRequestEntry> GetClusters()
        {
            if (_pivotData == null) return null;
            var values = _pivotData.AsEnumerable().Select(c => new
            {
                cluster = c.Field<string>(ClusterDim),
                location = c.Field<string>(LocationDim)
            }).Distinct().ToList();
            List<clusteredResultSetSaveRequestEntry> clusterKeys = new List<clusteredResultSetSaveRequestEntry>();
            foreach (var value in values)
            {
                clusteredResultSetSaveRequestEntry item = new clusteredResultSetSaveRequestEntry();
                item.key = value.location;
                int? cluster = ParseClusterName(value.cluster);
                item.value = Convert.ToString(cluster.GetValueOrDefault());
                clusterKeys.Add(item);
            }

            return clusterKeys;
        }

        private static string GetDrillDownTabText(PivotCellEventArgs e)
        {
            StringBuilder stb = new StringBuilder();
            stb.Append("Drill: ");
            if (e.ColumnField != null)
                stb.Append(e.GetFieldValue(e.ColumnField)).Append(", ");
            if (e.RowField != null)
                stb.Append(e.GetFieldValue(e.RowField)).Append(", ");
            
            stb.Append(e.DataField);
            return stb.ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tab">Pass either tabLocResults or tabProdResults</param>
        private void SelectHierarchyTab(XtraTabPage tab)
        {
            if (tab == tabLocResults)
            {
                _locationSelected = UpdateResultGrid(LocationDim, locTreeList, locAttTreeList, gridLocationResults, Convert.ToInt32(spinEditLocationLevel.Value));
            }
            if (tab == tabProdResults)
            {
                _productSelected = UpdateResultGrid(ProductDim, prodTreeList, prodAttTreeList, gridProductResults, Convert.ToInt32(spinEditProductLevel.Value));
            }
            CheckFormState();
        }


        private void UpdatePivotDataGridComboItems()
        {
            repositoryItemComboBox2.Items.Clear();
            var clusters = _pivotData.AsEnumerable().Select(c => new
            {
                cluster = c.Field<string>(ClusterDim)
            }).OrderBy(c => c.cluster).Distinct().ToList();

            foreach (var s in clusters)
            {
                repositoryItemComboBox2.Items.Add(s.cluster);
            }
        }

        /// <summary>
        /// Update the cluster name in (PivotData) for a list of datarows.
        /// </summary>
        /// <param name="dataRows">DataRows to update</param>
        /// <param name="newCluster">New Cluster name.</param>
        /// <param name="updateDataRowView">Update the attached dataRows or just the pivotGridData</param>
        private void UpdateSelectedRowsClusterName(IEnumerable<DataRow> dataRows, string newCluster, bool updateDataRowView)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            string cluster = newCluster.Trim();

            foreach (var selRow in dataRows)
            {
                //Update the pivot data - data table
                if(updateDataRowView) selRow[ClusterDim] = cluster;
                //Now update the main data table (it's possible that each location has multiple records, so query them)
                var queryLocation = selRow[LocationDim];
                var values = (_pivotData.AsEnumerable().Where(c => c.Field<string>(LocationDim).Equals(queryLocation))).ToList();

                foreach (DataRow dr in values)
                {
                    dr[ClusterDim] = cluster;
                    dr.AcceptChanges();
                }

            }
            gridViewPivotData.RefreshData();
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("UpdateSelectedRowsClusterName, runtime: {0} (ms)", new object[] {  stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

        /// <summary>
        /// Renames the ClusterName in the pivotData and pivotGridData
        /// </summary>
        /// <param name="oldCluster"></param>
        /// <param name="newCluster"></param>
        private void RenameCluster(string oldCluster, string newCluster)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            string cluster = newCluster.Trim();
            var dataRows = (_pivotGridData.AsEnumerable().Where(c => c.Field<string>(ClusterDim).Equals(oldCluster))).ToList();

            UpdateSelectedRowsClusterName(dataRows, cluster, true);

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("RenameCluster, runtime: {0} (ms)", new object[] {  stopwatch.ElapsedMilliseconds.ToString("N0") });
        }

        #region Event Handlers
        // ReSharper disable InconsistentNaming



        private void btnGenerateClusters_Click(object sender, EventArgs e)

        {
            List<XtraTabPage> items = new List<XtraTabPage>();
            foreach (XtraTabPage t in xtraTabControlCluster.TabPages)
            {
                if (t is GridTabPage)
                {
                    items.Add(t);
                }
            }
            foreach (var tab in items)
            {
                xtraTabControlCluster.TabPages.Remove(tab);
            }

            string pivotLayout = Path.Combine(_saveToPath, "layout.xml");
            string pivotCollapsedLayout = Path.Combine(_saveToPath, "collapsedlayout.xml");
            if (!_firstRun)
            {
                PafApp.GetLogger().InfoFormat("Saving pivot layout to: {0}", new object[] { pivotLayout });
                PafApp.GetLogger().InfoFormat("Saving collapsed layout layout to {0}", new object[] { pivotCollapsedLayout });
                pivotGridCluster.SaveLayoutToXml(pivotLayout, OptionsLayoutBase.FullLayout);
                pivotGridCluster.SaveCollapsedStateToFile(pivotCollapsedLayout);
            }


            bool result = GetClusterResults(comboBoxEditAlgorthim.SelectedIndex > 0);

            btnSave.Enabled = _pivotData != null;

            if (result)
            {
                File.Delete(pivotLayout);
                File.Delete(pivotCollapsedLayout);
                if (!_firstRun)
                {
                    //PafApp.GetLogger().InfoFormat("Loading pivot layout from: {0}", new object[] { pivotLayout });
                    //PafApp.GetLogger().InfoFormat("Loading collapsed layout layout from {0}", new object[] { pivotCollapsedLayout });
                    //pivotGridCluster.RestoreLayoutFromXml(pivotLayout, OptionsLayoutBase.FullLayout);
                    //pivotGridCluster.LoadCollapsedStateFromFile(pivotCollapsedLayout);
                }
                _firstRun = false;
            }
        }

        private void treeList1_BeforeCheckNode(object sender, CheckNodeEventArgs e)
        {
            if (e.State == CheckState.Unchecked) return;

            TreeListNode node = timeTreeList.GetCheckedNode();
            if (node != null)
            {
                int currentLevel = Convert.ToInt32(node.GetValue(_levelColumnName));
                int level = Convert.ToInt32(e.Node.GetValue(_levelColumnName));
                List<string> checkedNodes = timeTreeList.GetCheckedNodes(_memberNameColumnName);
                if (checkedNodes.Count >= 2 || currentLevel != level)
                {
                    e.CanCheck = false;
                }
            }
        }

        private void xtraTabControl1_MouseHover(object sender, EventArgs e)
        {
            CheckFormState();
        }

        private void btnMeasuresSave_Click(object sender, EventArgs e)
        {
            string item = comboBoxEditMeasures.SelectedItem.ToString();
            SaveDimensionsSelections(_measureDimension, measuresTreeList, comboBoxEditMeasures);
            SaveClusterDataset();
            List<string> location = _clusterDataset.GetSettingsNames(_measureDimension);
            comboBoxEditMeasures.AddSelectionItems(location, item);
        }

        private void btnTimeSave_Click(object sender, EventArgs e)
        {
            string item = comboBoxEditTime.SelectedItem.ToString();
            SaveDimensionsSelections(TimeHorizonDim, timeTreeList, comboBoxEditTime);
            SaveClusterDataset();
            List<string> location = _clusterDataset.GetSettingsNames(TimeHorizonDim);
            comboBoxEditTime.AddSelectionItems(location, item);
        }

        private void btnLocationsSave_Click(object sender, EventArgs e)
        {
            string item = comboBoxEditLocations.SelectedItem.ToString();
            SaveAttributeDimensionsSelections(locAttTreeList, comboBoxEditLocations);
            SaveDimensionsSelections(LocationDim, locTreeList, comboBoxEditLocations);
            SaveClusterDataset();
            List<string> location = _clusterDataset.GetSettingsNames(LocationDim);
            comboBoxEditLocations.AddSelectionItems(location, item);
        }

        private void btnProductsSave_Click(object sender, EventArgs e)
        {
            string item = comboBoxEditProducts.SelectedItem.ToString();
            SaveAttributeDimensionsSelections(prodAttTreeList, comboBoxEditProducts);
            SaveDimensionsSelections(ProductDim, prodTreeList, comboBoxEditProducts);
            SaveClusterDataset();
            List<string> product = _clusterDataset.GetSettingsNames(ProductDim);
            comboBoxEditProducts.AddSelectionItems(product, item);
        }

        private void comboBoxEditProducts_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectSaveMembersWithAttributes(ProductDim, prodTreeList, prodAttTreeList, comboBoxEditProducts);
            SelectHierarchyTab(tabProdResults);
        }

        private void comboBoxEditLocations_SelectedIndexChanged(object sender, EventArgs e)
        {
            SelectSaveMembersWithAttributes(LocationDim, locTreeList, locAttTreeList, comboBoxEditLocations);
            SelectHierarchyTab(tabLocResults);
        }

        private void comboBoxEditTime_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(comboBoxEditTime.SelectedText))
            {
                timeTreeList.UncheckAll();
            }


            List<string> settings = _clusterDataset.GetSelections(TimeHorizonDim, comboBoxEditTime.SelectedText);
            if (settings != null && settings.Count > 0)
            {
                SelectSavedMembers(timeTreeList, settings);
            }
            CheckFormState();
        }

        private void comboBoxEditMeasures_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(comboBoxEditMeasures.SelectedText))
            {
                measuresTreeList.UncheckAll();
            }
            List<string> settings = _clusterDataset.GetSelections(_measureDimension, comboBoxEditMeasures.SelectedText);
            if (settings != null && settings.Count > 0)
            {
                SelectSavedMembers(measuresTreeList, settings);
            }
            CheckFormState();
        }

        private void xtraTabControlCluster_CloseButtonClick(object sender, EventArgs e)
        {
            XtraTabControl tabControl = sender as XtraTabControl;
            if (tabControl == null) return;
            ClosePageButtonEventArgs arg = e as ClosePageButtonEventArgs;
            if (arg == null) return;
            XtraTabPage page = (XtraTabPage)arg.Page;
            if (page == xtraTabPageAdHoc || page == xtraTabPagePivot) return;
            page.PageVisible = false;
            //if (!String.IsNullOrEmpty(_pivotData.DefaultView.RowFilter))
            //{
            //    _pivotData.DefaultView.RowFilter = String.Empty;
            //}
            pivotGridCluster.RefreshData();
        }

        private void xtraTabControl1_Selected(object sender, TabPageEventArgs e)
        {
            if (e.Page == tabCluster)
            {
                spinEditClusters.Properties.MinValue = 1;
            }
            if (_pivotData != null)
            {
                pivotGridCluster.RefreshData();
            }
        }

        private void AfterCheckNodeCheckState(object sender, NodeEventArgs e)
        {
            _firstRun = true;
            CheckFormState();
            _customCluster = false;
        }

        private void txtClusterLabel_KeyPress(object sender, KeyPressEventArgs e)
        {
            CheckFormState();
        }

        private void pivotGridControl2_CellDoubleClick(object sender, PivotCellEventArgs e)
        {
            PivotDrillDownDataSource ds = e.CreateDrillDownDataSource();
            if (ds == null || ds.RowCount == 0)
            {
                XtraMessageBox.Show("DrillDown operation returned no rows", null, MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            GridTabPage newTab = new GridTabPage(GetDrillDownTabText(e), String.Empty, ds);
            xtraTabControlCluster.TabPages.Add(newTab);
            xtraTabControlCluster.SelectedTabPage = newTab;
            foreach (GridColumn c in _measures.Select(m => newTab.GridView.Columns.ColumnByFieldName(m)).Where(c => c != null))
            {
                c.DisplayFormat.FormatType = FormatType.Numeric;
                c.DisplayFormat.FormatString = "N0";
            }

        }

        private void xtraTabControlCluster_SelectedPageChanging(object sender, TabPageChangingEventArgs e)
        {
            string pivotLayout = Path.Combine(_saveToPath, "layout.xml");
            string pivotCollapsedLayout = Path.Combine(_saveToPath, "collapsedlayout.xml");
            if (e.PrevPage == xtraTabPagePivot)
            {
                PafApp.GetLogger().InfoFormat("Saving pivot layout to: {0}", new object[] { pivotLayout });
                PafApp.GetLogger().InfoFormat("Saving collapsed layout layout to {0}", new object[] { pivotCollapsedLayout });

                pivotGridCluster.SaveLayoutToXml(pivotLayout, OptionsLayoutBase.FullLayout);
                pivotGridCluster.SaveCollapsedStateToFile(pivotCollapsedLayout);
            }

            if (e.Page == xtraTabPagePivot)
            {
                if (_pivotData != null)
                {
                    if (_customClusterSpecified)
                    {
                        btnGenerateClusters.PerformClick();
                        _customClusterSpecified = false;
                    }
                    else
                    {
                        pivotGridCluster.RefreshData();
                    }

                    if (File.Exists(pivotLayout))
                    {
                        PafApp.GetLogger().InfoFormat("Loading pivot layout from: {0}", new object[] { pivotLayout });
                        pivotGridCluster.RestoreLayoutFromXml(pivotLayout, OptionsLayoutBase.FullLayout);
                    }
                    if (File.Exists(pivotCollapsedLayout))
                    {
                        PafApp.GetLogger().InfoFormat("Loading collapsed layout layout from {0}", new object[] { pivotCollapsedLayout });
                        pivotGridCluster.LoadCollapsedStateFromFile(pivotCollapsedLayout);
                    }
                }
            }
            else if (e.Page == xtraTabPageAdHoc)
            {
                if (_pivotData != null)
                {
                    if (!String.IsNullOrEmpty(_pivotData.DefaultView.RowFilter))
                    {
                        _pivotData.DefaultView.RowFilter = String.Empty;
                    }
                }
            }
            else
            {
                GridTabPage page = (GridTabPage)e.Page;
                page.Bind();
            }
        }

        private int? ParseClusterName(string cluster)
        {
            string value = cluster.Replace(ClusterDim, "").Trim();

            if (String.IsNullOrEmpty(value))
            {
                return null;
            }

            int clusterNum;
            if (!int.TryParse(value, out clusterNum))
            {
                return null;
            }
            else
            {
                return clusterNum;
            }
        }


        private void gridViewPivotData_InvalidValueException(object sender, InvalidValueExceptionEventArgs e)
        {
            e.ExceptionMode = ExceptionMode.NoAction;
            GridView view = sender as GridView;
            if (view == null) return;
            view.HideEditor();
        }

        private void gridViewPivotData_ValidatingEditor(object sender, BaseContainerValidateEditorEventArgs e)
        {
            GridView view = sender as GridView;
            if (view == null) return;
            if (view.FocusedColumn.FieldName != ClusterDim) return;

            int? cluster = ParseClusterName(e.Value.ToString());

            if (cluster == null)
            {
                e.Valid = false;
            }
            else
            {
                e.Value = String.Format("{0} {1}", new object[] { ClusterDim, cluster });   
            }
        }

        private void gridViewPivotData_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            //Get the processed row and the new Cluster Value
            //Get the Store# 
            //QUery for that store number and update the cluster.

            string value = e.Value.ToString();

            DataRowView rows = (DataRowView)gridViewPivotData.GetRow(e.RowHandle);

            UpdateSelectedRowsClusterName(rows.ToDataRows(), value, false);
            UpdatePivotDataGridComboItems();
            _customCluster = true;
            _customClusterSpecified = true;
        }

        private void gridViewPivotData_ShowingEditor(object sender, System.ComponentModel.CancelEventArgs e)
        {
            GridView view = sender as GridView;

            if (view == null) return;

            if (view.FocusedColumn.FieldName != ClusterDim)
            {
                e.Cancel = true;
            }
        }

        private void xtraTabControlClusterAlgorthim_Selecting(object sender, TabPageCancelEventArgs e)
        {
            if (e.Page == tabCluster)
            {
                if (!CheckFormState())
                {
                    e.Cancel = true;
                }
            }
        }

        private void gridViewPivotData_CustomColumnSort(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e)
        {
            try
            {
                int? result;
                switch (e.Column.FieldName)
                {
                    case ClusterDim:
                        result = SortClusterColumn(e.Value1, e.Value2);
                        break;
                    case LocationDim:
                        result = SortLocationColumn(e.Value1, e.Value2);
                        break;
                    default:
                        return;
                }

                if (result == null)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                    e.Result = result.GetValueOrDefault();
                }
            }
            catch (Exception)
            {
                e.Handled = false;
            }
        }

        private void pivotGridCluster_CustomFieldSort(object sender, PivotGridCustomFieldSortEventArgs e)
        {
            try
            {
                int? result;

                switch (e.Field.FieldName)
                {
                    case ClusterDim:
                        result = SortClusterColumn(e.Value1, e.Value2);
                        break;
                    case LocationDim:
                        result = SortLocationColumn(e.Value1, e.Value2);
                        break;
                    default:
                        return;
                }
                if (result == null)
                {
                    e.Handled = false;
                }
                else
                {
                    e.Handled = true;
                    e.Result = result.GetValueOrDefault();
                }
            }
            catch (Exception)
            {
                e.Handled = false;
            }
        }

        private void prodTreeList_AfterCheckNode(object sender, NodeEventArgs e)
        {
            SelectHierarchyTab(tabProdResults);
        }

        private void locTreeList_AfterCheckNode(object sender, NodeEventArgs e)
        {

            SelectHierarchyTab(tabLocResults);
        }

        private void gridViewPivotData_PopupMenuShowing(object sender, DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;
            string clusterToSkip = String.Empty;

            int[] rowHandles = gridViewPivotData.GetSelectedRows();
            if (rowHandles.Length == 1)
            {
                DataRowView dr = (DataRowView) (gridViewPivotData.GetRow(rowHandles[0]));
                clusterToSkip = Convert.ToString(dr[ClusterDim]);
                barRenameCluster.Enabled = true;
                barEditClusterName.Enabled = true;
                barEditClusterName.EditValue = null;
                barEditClusterName.Caption = "\"" + clusterToSkip  + "\" to: " ;
            }
            else
            {
                barRenameCluster.Enabled = false;
                barEditClusterName.Enabled = false;
            }

            var clusters = _pivotData.AsEnumerable().Select(c => new
            {
                cluster = c.Field<string>(ClusterDim)
            }).OrderBy(c => c.cluster).Distinct().ToList();


            for (int i = barItemMoveTo.ItemLinks.Count - 1; i >= 0; i--)
            {
                barItemMoveTo.ItemLinks.RemoveAt(i);
            }
           

            foreach (var cluster in clusters)
            {
                if (cluster.cluster == clusterToSkip) continue;
                string clusterString = cluster.cluster;
                var button = new BarButtonItem(barManager, clusterString) {Tag = cluster.cluster};
                button.ItemClick += (button_ItemClick);
                barItemMoveTo.AddItem(button);
            }
            

            BarEditItem editItem = new BarEditItem(barManager);
            editItem.Caption = "Custom Cluster: ";
            editItem.Width = 150;
            editItem.Edit = repositoryItemTextEditCustomCluster;
            editItem.EditValueChanged += (editItem_EditValueChanged);
            editItem.Edit = repositoryItemTextEditCustomCluster;
            editItem.Visibility = BarItemVisibility.Never;
            barItemMoveTo.AddItem(editItem).BeginGroup = true;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);

                if (hitInfo.InRow)
                {
                    view.FocusedRowHandle = hitInfo.RowHandle;

                    popupMenu.ShowPopup(barManager, gridControlPivotData.PointToScreen(hitInfo.HitPoint));

                }
            }
        }

        private void editItem_EditValueChanged(object sender, EventArgs e)
        {
            var edit = (BarEditItem) sender;
            string newCluster = edit.EditValue.ToString();

            int[] rowHandles = gridViewPivotData.GetSelectedRows();
            List<DataRowView> dataRows = rowHandles.Select(rowHandle => (DataRowView)(gridViewPivotData.GetRow(rowHandle))).ToList();

            UpdateSelectedRowsClusterName(dataRows.ToDataRows(), newCluster, true);
            UpdatePivotDataGridComboItems();

        }

        private void barEditClusterName_EditValueChanged(object sender, EventArgs e)
        {
            var edit = (BarEditItem)sender;
            if (edit.EditValue == null) return;
            string newClusterName = edit.EditValue.ToString();
            int[] rowHandles = gridViewPivotData.GetSelectedRows();
            List<DataRowView> dataRows = rowHandles.Select(rowHandle => (DataRowView)(gridViewPivotData.GetRow(rowHandle))).ToList();
            if (dataRows.Count == 0) return;

            RenameCluster(dataRows[0].Row[ClusterDim].ToString().Trim(), newClusterName);
            UpdatePivotDataGridComboItems();
        }


        public void button_ItemClick(object sender, ItemClickEventArgs e)
        {
            string value = e.Item.Tag.ToString();
            int[] rowHandles = gridViewPivotData.GetSelectedRows();
            List<DataRowView> dataRows = rowHandles.Select(rowHandle => (DataRowView)(gridViewPivotData.GetRow(rowHandle))).ToList();
            UpdateSelectedRowsClusterName(dataRows.ToDataRows(), value, true);
        }

        private void pivotGridCluster_MouseMove(object sender, MouseEventArgs e)
        {
            PivotGridHitInfo hitInfo = pivotGridCluster.CalcHitInfo(e.Location);
            if (hitInfo.HitTest == PivotGridHitTest.Cell && hitInfo.CellInfo != null)
            {
                bool isPressed = Convert.ToBoolean(GetKeyState(0xA0) & 0x8000);
                if (!isPressed)
                {
                    memberTooltipController.HideHint();
                    return;
                }

                var cellInfo = hitInfo.CellInfo;
                PivotGridField[] rows = cellInfo.GetRowFields();
                PivotGridField[] cols = cellInfo.GetColumnFields();
                PivotSummaryDataSource source = cellInfo.CreateSummaryDataSource();

                StringBuilder sb = new StringBuilder();
                for (int r = 0; r < 1; r++)
                {
                    int c = 0;
                    foreach (PivotGridField row in rows)
                    {
                        sb.Append(row.FieldName).Append(": ").Append(source.GetValue(r, c)).AppendLine("");
                        c++;
                    }
                    foreach (PivotGridField col in cols)
                    {
                        sb.Append(col.FieldName).Append(": ").Append(source.GetValue(r, col.FieldName)).AppendLine("");
                    }
                }
                if (cellInfo != null && cellInfo.DataField != null && cellInfo.Value != null)
                {
                    var valueString = Convert.ToInt32(cellInfo.Value).ToString("N0");
                    sb.Append(cellInfo.DataField.FieldName)
                      .Append(": ")
                      .Append(valueString)
                      .AppendLine("");
                }
                if (sb.Length > 0)
                {
                    //Point pt = e.Location;
                    //Point pt2 = pivotGridCluster.PointToClient(tabCluster.Location);
                    //pt2.Y = tabCluster.Height;
                    Point pt = pivotGridCluster.PointToScreen(e.Location);
                    //Point pt = pivotGridCluster.PointToScreen(pt2);
                    //pt.Offset(-5, -5);
                    memberTooltipController.ShowHint(sb.ToString(), pt);
                    return;
                }
            }
            memberTooltipController.HideHint();
        }

        private void spinEditProductLevel_ValueChanged(object sender, EventArgs e)
        {
            SelectHierarchyTab(tabProdResults);
        }

        private void spinEditLocationLevel_ValueChanged(object sender, EventArgs e)
        {
            SelectHierarchyTab(tabLocResults);
        }

        private void treeList_FilterNode(object sender, FilterNodeEventArgs e)
        {
            if (e.Node == null) return;

            TreeList treeList = e.Node.TreeList;
            
            if (treeList != null)
            {
                treeList.ExpandAll();
                e.Handled = false;
            }
        }

        private void measuresTreeList_MouseUp(object sender, MouseEventArgs e)
        {
            TreeList tree = sender as TreeList;
            if (tree == null) return;
            if (e.Button == MouseButtons.Right && ModifierKeys == Keys.None && tree.State == TreeListState.Regular)
            {
                Point pt = tree.PointToClient(MousePosition);

                TreeListHitInfo info = tree.CalcHitInfo(pt);

                if (info.HitInfoType == HitInfoType.Cell)
                {
                    _savedMeasuresNode = info.Node;

                    tree.FocusedNode = info.Node;

                    popupMenuNodes.ShowPopup(MousePosition);
                }
            }
        }

        private void barCheckItem_CheckedChanged(object sender, ItemClickEventArgs e)
        {
            if (_uiUpdating) return;
            string measure = _savedMeasuresNode.GetDisplayText(_memberNameColumnName);
            string formatName = Convert.ToString(e.Item.Tag);
            BarCheckItem item = e.Item as BarCheckItem;
            if (item == null) return;
            if (!item.Checked) return;
            _clusterDataset.AddOrUpdateMemberFormat(measure, formatName);
            SaveClusterDataset();
        }

        private void popupMenuNodes_BeforePopup(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                TreeList tree = measuresTreeList;
                if (tree == null) return;
                if (ModifierKeys == Keys.None && tree.State == TreeListState.Regular)
                {
                    _uiUpdating = true;
                    barCheckItemPercent.Checked = false;
                    barCheckItemNumeric.Checked = false;
                    barCheckItemPercent4.Checked = false;
                    barCheckItemNumeric4.Checked = false;

                    Point pt = tree.PointToClient(MousePosition);
                    TreeListHitInfo info = tree.CalcHitInfo(pt);

                    if (info.HitInfoType == HitInfoType.Cell)
                    {
                        string measure = info.Node.GetDisplayText(_memberNameColumnName);
                        string format = _clusterDataset.GetMemberFormat(measure);

                        if (!String.IsNullOrWhiteSpace(format))
                        {
                            if (barCheckItemPercent.Tag.Equals(format))
                            {
                                barCheckItemPercent.Checked = true;
                            }
                            else if (barCheckItemPercent4.Tag.Equals(format))
                            {
                                barCheckItemPercent4.Checked = true;
                            }
                            else if (barCheckItemNumeric4.Tag.Equals(format))
                            {
                                barCheckItemNumeric4.Checked = true;
                            }
                            else
                            {
                                barCheckItemNumeric.Checked = true;
                            }
                        }
                        else
                        {
                            barCheckItemNumeric.Checked = true;
                        }
                    }
                }
            }
            finally
            {
                _uiUpdating = false;
            }
        }

        // ReSharper restore InconsistentNaming
        #endregion Event Handlers
    }
}