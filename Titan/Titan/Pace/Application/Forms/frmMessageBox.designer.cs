using Titan.Pace.Application.Controls;

namespace Titan.Pace.Application.Forms
{
    partial class frmMessageBox
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdOk = new System.Windows.Forms.Button();
            this.picIcon = new System.Windows.Forms.PictureBox();
            this.lblMessage = new System.Windows.Forms.TextBox();
            this.panelOk = new System.Windows.Forms.Panel();
            this.panelYesNo = new System.Windows.Forms.Panel();
            this.cmdNo = new System.Windows.Forms.Button();
            this.cmdCancel2 = new System.Windows.Forms.Button();
            this.cmdYes = new System.Windows.Forms.Button();
            this.panelOkCancel = new System.Windows.Forms.Panel();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOk2 = new System.Windows.Forms.Button();
            this.subpane = new Subpane();
            this.txtStackTrace = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).BeginInit();
            this.panelOk.SuspendLayout();
            this.panelYesNo.SuspendLayout();
            this.panelOkCancel.SuspendLayout();
            this.subpane.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(3, 3);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 3;
            this.cmdOk.Text = "_";
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // picIcon
            // 
            this.picIcon.Location = new System.Drawing.Point(3, 12);
            this.picIcon.Name = "picIcon";
            this.picIcon.Size = new System.Drawing.Size(43, 32);
            this.picIcon.TabIndex = 4;
            this.picIcon.TabStop = false;
            // 
            // lblMessage
            // 
            this.lblMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lblMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblMessage.Location = new System.Drawing.Point(52, 12);
            this.lblMessage.Multiline = true;
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.ReadOnly = true;
            this.lblMessage.Size = new System.Drawing.Size(472, 54);
            this.lblMessage.TabIndex = 5;
            // 
            // panelOk
            // 
            this.panelOk.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelOk.Controls.Add(this.cmdOk);
            this.panelOk.Location = new System.Drawing.Point(255, 63);
            this.panelOk.Name = "panelOk";
            this.panelOk.Size = new System.Drawing.Size(81, 27);
            this.panelOk.TabIndex = 7;
            // 
            // panelYesNo
            // 
            this.panelYesNo.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelYesNo.Controls.Add(this.cmdNo);
            this.panelYesNo.Controls.Add(this.cmdCancel2);
            this.panelYesNo.Controls.Add(this.cmdYes);
            this.panelYesNo.Location = new System.Drawing.Point(3, 63);
            this.panelYesNo.Name = "panelYesNo";
            this.panelYesNo.Size = new System.Drawing.Size(243, 27);
            this.panelYesNo.TabIndex = 9;
            // 
            // cmdNo
            // 
            this.cmdNo.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdNo.DialogResult = System.Windows.Forms.DialogResult.No;
            this.cmdNo.Location = new System.Drawing.Point(84, 3);
            this.cmdNo.Name = "cmdNo";
            this.cmdNo.Size = new System.Drawing.Size(75, 23);
            this.cmdNo.TabIndex = 9;
            this.cmdNo.Text = "_";
            this.cmdNo.UseVisualStyleBackColor = true;
            // 
            // cmdCancel2
            // 
            this.cmdCancel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel2.Location = new System.Drawing.Point(165, 3);
            this.cmdCancel2.Name = "cmdCancel2";
            this.cmdCancel2.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel2.TabIndex = 8;
            this.cmdCancel2.Text = "_";
            this.cmdCancel2.UseVisualStyleBackColor = true;
            // 
            // cmdYes
            // 
            this.cmdYes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdYes.DialogResult = System.Windows.Forms.DialogResult.Yes;
            this.cmdYes.Location = new System.Drawing.Point(3, 3);
            this.cmdYes.Name = "cmdYes";
            this.cmdYes.Size = new System.Drawing.Size(75, 23);
            this.cmdYes.TabIndex = 7;
            this.cmdYes.Text = "_";
            this.cmdYes.UseVisualStyleBackColor = true;
            // 
            // panelOkCancel
            // 
            this.panelOkCancel.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.panelOkCancel.Controls.Add(this.cmdCancel);
            this.panelOkCancel.Controls.Add(this.cmdOk2);
            this.panelOkCancel.Location = new System.Drawing.Point(339, 63);
            this.panelOkCancel.Name = "panelOkCancel";
            this.panelOkCancel.Size = new System.Drawing.Size(162, 27);
            this.panelOkCancel.TabIndex = 10;
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(84, 1);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 8;
            this.cmdCancel.Text = "_";
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdOk2
            // 
            this.cmdOk2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk2.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk2.Location = new System.Drawing.Point(3, 1);
            this.cmdOk2.Name = "cmdOk2";
            this.cmdOk2.Size = new System.Drawing.Size(75, 23);
            this.cmdOk2.TabIndex = 7;
            this.cmdOk2.Text = "_";
            this.cmdOk2.UseVisualStyleBackColor = true;
            // 
            // subpane
            // 
            this.subpane.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.subpane.BackColor = System.Drawing.Color.Transparent;
            this.subpane.CausesValidation = false;
            this.subpane.Controls.Add(this.txtStackTrace);
            this.subpane.DefaultExpandedHeight = 0;
            this.subpane.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.subpane.ForeColor = System.Drawing.SystemColors.ControlText;
            this.subpane.GradientColorBegin = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.GradientColorEnd = System.Drawing.SystemColors.GradientActiveCaption;
            this.subpane.HighlightColor = System.Drawing.SystemColors.ControlText;
            this.subpane.ImeMode = System.Windows.Forms.ImeMode.Off;
            this.subpane.IsAnimated = false;
            this.subpane.Location = new System.Drawing.Point(3, 96);
            this.subpane.Name = "subpane";
            this.subpane.Padding = new System.Windows.Forms.Padding(0);
            this.subpane.PanelColor = System.Drawing.SystemColors.Control;
            this.subpane.Size = new System.Drawing.Size(521, 127);
            this.subpane.TabIndex = 1;
            this.subpane.TabStop = true;
            this.subpane.Text = "Subpane";
            this.subpane.AfterExpand += new System.EventHandler(this.subpane1_AfterExpand);
            this.subpane.AfterCollapse += new System.EventHandler(this.subpane1_AfterCollapse);
            // 
            // txtStackTrace
            // 
            this.txtStackTrace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.txtStackTrace.BackColor = System.Drawing.Color.White;
            this.txtStackTrace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStackTrace.Location = new System.Drawing.Point(0, 20);
            this.txtStackTrace.Multiline = true;
            this.txtStackTrace.Name = "txtStackTrace";
            this.txtStackTrace.ReadOnly = true;
            this.txtStackTrace.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtStackTrace.Size = new System.Drawing.Size(518, 107);
            this.txtStackTrace.TabIndex = 0;
            // 
            // frmMessageBox
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(526, 225);
            this.Controls.Add(this.panelOk);
            this.Controls.Add(this.panelOkCancel);
            this.Controls.Add(this.panelYesNo);
            this.Controls.Add(this.picIcon);
            this.Controls.Add(this.subpane);
            this.Controls.Add(this.lblMessage);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmMessageBox";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.MessageBox_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picIcon)).EndInit();
            this.panelOk.ResumeLayout(false);
            this.panelYesNo.ResumeLayout(false);
            this.panelOkCancel.ResumeLayout(false);
            this.subpane.ResumeLayout(false);
            this.subpane.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtStackTrace;
        private Subpane subpane;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.PictureBox picIcon;
        private System.Windows.Forms.TextBox lblMessage;
        private System.Windows.Forms.Panel panelOk;
        private System.Windows.Forms.Panel panelYesNo;
        private System.Windows.Forms.Button cmdNo;
        private System.Windows.Forms.Button cmdCancel2;
        private System.Windows.Forms.Button cmdYes;
        private System.Windows.Forms.Panel panelOkCancel;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Button cmdOk2;
    }
}