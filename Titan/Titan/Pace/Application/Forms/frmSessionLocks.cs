﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Data.Filtering;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraGrid.Views.Grid.ViewInfo;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Controls.TreeView;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.Palladium;
using Titan.Palladium.DataStructures;
using Titan.Properties;
using Clipboard = System.Windows.Clipboard;
using DataFormats = System.Windows.DataFormats;
using IDataObject = System.Windows.IDataObject;
using Point = System.Drawing.Point;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmSessionLocks : XtraForm
    {
        private bool _building = false;
        private readonly HashSet<int> _changedStatus;
        private readonly HashSet<int> _removedStatus;
        private readonly List<long> _notInUow;
        private string IdColumnName { get; set; }
        private string SetColumnName { get; set; }
        private string LevelColumnName { get; set; }
        private string GenerationColumnName { get; set; }
        private Dictionary<string, List<string>> DimensionAliasTables { get; set; }
        private string MemberNameColumnName { get; set; }
        private string MemberKeyColumnName { get; set; }
        private string ParentKeyColumnName { get; set; }
        private string VersionRoot { get; set; }
        private string VersionDimension { get; set; }
        private string MeasureRoot { get; set; }
        private string MeasureDimension { get; set; }
        private string AppId { get; set; }
        private const string UOW = PafAppConstants.UOW_STRING; 
        private PopupEditor _popupEditor;
        private readonly Dictionary<string, DataTable> _treeTables;
        private readonly List<int> _validLevels;
        private DataSet _gridDataSet;
        private DataTable _gridDataTable = new DataTable();
        private DataSet _snapshotDataSet;
        private readonly string _appTitle;
        private string[] _dimensionPriority;

        /// <summary>
        /// The Dimensions in Priority Order
        /// </summary>
		public string[] DimensionPriority { get; set; }
        // public string[] DimensionPriority
        // {
            // get { return _dimensionPriority; }
            // set
            // {
                // List<string> dimensionPriority = new List<string>();
                // List<string>  dp = SimpleDimTrees.GetDimensionNames(false);
                // foreach (string s in value)
                // {
                    // if (dp.Contains(s))
                    // {
                        // dimensionPriority.Add(s);
                    // }
                // }


                // _dimensionPriority = dimensionPriority.ToArray(); 
            // }
        // }

        /// <summary>
        /// Gets/Sets if there are any calculations pending.
        /// </summary>
        public bool CalculationsPending { get; set; }

        /// <summary>
        /// The default intersection to use when the form reload.
        /// Note the property is cleared when the Form is hidden.
        /// </summary>
        public Intersection DefaultLockIntersection { get; set; }

        /// <summary>
        /// SimpleDimTrees data object.
        /// </summary>
        public SimpleDimTrees SimpleDimTrees { get; set; }

        /// <summary>
        /// List of locks returned from the server.
        /// </summary>
        public List<DescendantIntersection> Locks { get; set; }

        /// <summary>
        /// Signals if any Intersections have been changed from Set to Unset.
        /// </summary>
        public bool IntersectionsRemoved { get; private set; }

        /// <summary>
        /// Signals if any Intersections have been modified.
        /// </summary>
        public bool IntersectionsChanged { get; private set; }

        /// <summary>
        /// Constructor
        /// </summary>
        public frmSessionLocks()
        {
            _building = true;
            _treeTables = new Dictionary<string, DataTable>(15);
            _validLevels = new List<int> { 0 };
            _notInUow = new List<long>(5);
            Locks = new List<DescendantIntersection>(5);
            _changedStatus = new HashSet<int>();
            _removedStatus = new HashSet<int>();
            InitializeComponent();
            Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.Caption");
            chkShowAttributes.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.ShowAllAttributes.Caption");
            cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
            cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
            _appTitle = PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");
            barButtonCopyRow.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Copy");
            barButtonPasteRow.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Paste");
            barButtonDeleteRow.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Delete");
            chkShowAttributes.Properties.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.chkShowAttributes.Caption");
            chkShowAttributes.ToolTip = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.chkShowAttributes.Tooltip");
            barCheckShowAttributes.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.chkShowAttributes.Caption");
            barCheckHideInvalid.Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.HideInvalid.Caption");
            marqueeProgressBarControl1.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.ProgressBarMessage");
            marqueeProgressBarControl1.EditValue = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.ProgressBarMessage");
            gridControl1.EmbeddedNavigator.Buttons.Remove.Hint = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.EmbeddedNavigator.Remove.Hint");
            gridControl1.EmbeddedNavigator.Buttons.Append.Hint = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.EmbeddedNavigator.Add.Hint");
            gridControl1.EmbeddedNavigator.TextStringFormat = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.EmbeddedNavigator.Hint");
            //PafApp.GetDescendantsCompleted += new PafApp.PafServiceGetDescendantsCompleted(PafApp_GetDescendantsCompleted);
            //PafApp.GetDescendantsError += new PafApp.PafServiceGetDescendantsError(PafApp_GetDescendantsError);
            _building = false;
        }

        private void PafApp_GetDescendantsError(object sender, PafWebserviceErrorEventArgs eventArgs)
        {

            //MessageBox.Show(eventArgs.Message,_appTitle,MessageBoxButtons.OK,MessageBoxIcon.Error);
            PafApp.MessageBox().Show(eventArgs.Exception);
            Invoke((MethodInvoker)delegate()
            {
                SetControlStatus(true);
                marqueeProgressBarControl1.Visible = false;
                DialogResult = DialogResult.Cancel;
                Hide();
                Cursor = Cursors.Default;
            });

        }

        private void PafApp_GetDescendantsCompleted(object sender, GetDescendantsCompletedEventArgs eventArgs)
        {

            Locks = eventArgs.DescendantsIntersections;
            //Use Invoke to avoid the Cross-Threaded operation exception.
            Invoke((MethodInvoker)delegate()
            {
                SetControlStatus(true);
                marqueeProgressBarControl1.Visible = false;
                DialogResult = DialogResult.OK;
                Hide();
                Cursor = Cursors.Default;
            });
            PafApp.GetLogger().Info("PafApp_GetDescendantsCompleted - frmSessionLocks hide.");
        }


        /// <summary>
        /// Event that fires when form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSessionLocks_Load(object sender, EventArgs e)
        {
            _building = true;
            IntersectionsRemoved = false;
            IntersectionsChanged = false;
            _changedStatus.Clear();
            _removedStatus.Clear();
            //Load the main object
            LoadSimpleDimTreeVars();
            //Set the key name in the RowStateHelper
            LockedIntersections.KeyColumnName = IdColumnName;
            LockedIntersections.EnabledColumnName = SetColumnName;
            //Create the dimension trees
            CreatePopupDimTrees();
            //create the datatable
            Settings.Default.Reload();
            DataTable dtTemp = Settings.Default.SessionLocks.GetTable(AppId);
            List<string> tableDims = SimpleDimTrees.GetDimensionNames();
            if (dtTemp != null)
            {
                bool isValid = true;
                foreach (DataColumn dc in dtTemp.Columns)
                {
                    if (dc.ColumnName.Equals(IdColumnName)) continue;
                    if (dc.ColumnName.Equals(SetColumnName)) continue;
                    
                    if (!tableDims.Contains(dc.ColumnName))
                    {
                        isValid = false;
                        break;
                    }
                }

                if (isValid)
                {
                    foreach (string s in tableDims)
                    {
                        if (dtTemp.GetDataColumn(s) == null)
                        {
                            isValid = false;
                            break;
                        }
                    }
                }

                if (!isValid)
                {
                    Settings.Default.SessionLocks.Tables.Remove(AppId);
                    Settings.Default.Save();
                    Settings.Default.Reload();
                }

            }
            if (dtTemp != null)
            {
                //Load the dataset from the settings file
                _gridDataSet = Settings.Default.SessionLocks;
                //Set the table to the dataset table.
                _gridDataTable = dtTemp;
                if(! ValidateTableDimensionality(_gridDataTable))
                {
                    PafApp.GetLogger().Warn("Project dimensionality differs from table dimensionality.  Your saved session locks will be reset.");
                    _gridDataTable = SimpleDimTrees.ToGridDataTable(AppId);
                }
                //create a copy, in case the user clicks cancel.
                _snapshotDataSet = _gridDataSet.Copy();
            }
            else
            {
                _gridDataSet = Settings.Default.SessionLocks ?? new DataSet("SessionLocks");
                _gridDataTable = SimpleDimTrees.ToGridDataTable(AppId);
                _gridDataSet.Tables.Add(_gridDataTable);
            }
            //TODO this is temp
            Intersection temp = new Intersection(DimensionPriority);
            //foreach (string s in DefaultLockIntersection.AxisSequence)
            for (int i = 0; i < DefaultLockIntersection.AxisSequence.Length; i++ )
            {
                string dim = DefaultLockIntersection.AxisSequence[i];
                if (temp.ContainsAxis(dim))
                {
                    temp.SetCoordinate(dim, DefaultLockIntersection.Coordinates[i]);
                }
            }
            DefaultLockIntersection = temp;
            //Add the default lock intersection
            AddDataRow(DefaultLockIntersection, false, true);
            //Remove Duplicates.
            _gridDataTable = RemoveDuplicates(_gridDataTable);
            //Add disabled rows to the collections
            ValidateMembers();
            //Bind the data to the grid.
            BindData(_gridDataTable);
            //Hide the attribute columns
            chkShowAttributes.Checked = Settings.Default.SessionLockHideAttributes;
            barCheckShowAttributes.Checked = chkShowAttributes.Checked;
            ShowHideAttributeColumns(chkShowAttributes.Checked);
            if (Settings.Default.SessionLockHeight > 0 && Settings.Default.SessionLockWidth > 0)
            {
                Height = Settings.Default.SessionLockHeight;
                Width = Settings.Default.SessionLockWidth;
            }
            if (Settings.Default.SessionLockLocation != new Point(0, 0))
            {
                StartPosition = FormStartPosition.Manual;
                Location = Settings.Default.SessionLockLocation;
            }
            else
            {
                StartPosition = FormStartPosition.CenterParent;
            }

            PafApp.GetDescendantsCompleted -= PafApp_GetDescendantsCompleted;
            PafApp.GetDescendantsCompleted += PafApp_GetDescendantsCompleted;
            
            PafApp.GetDescendantsError -= PafApp_GetDescendantsError;
            PafApp.GetDescendantsError += PafApp_GetDescendantsError;

            _building = false;
        }

        private string ClipboardData()
        {
            IDataObject iData = Clipboard.GetDataObject();
            if (iData == null)
            {
                return "";
            }

            if (iData.GetDataPresent(DataFormats.Text))
            {
                return (string) iData.GetData(DataFormats.Text);
            }
            return "";
        }

        private bool ValidateDeleteRow(long rowId)
        {
            DataRow row = gridView1.GetDataRow(gridView1.FocusedRowHandle);

            bool isSet;
            try
            {
                isSet = Convert.ToBoolean(row[SetColumnName]);
            }
            catch (Exception) 
            {
                //The data is bad, so allow the user to delete it.
                return true;
            }

            if (IsRowDisabled(rowId)) return false;
            if (isSet) return false;

            return true;
        }

        private bool ValidateCopyRow(long rowId)
        {
            return !IsRowDisabled(rowId);
        }

        private List<long> GetSelectedRowIds()
        {

            int[] rowHandles = gridView1.GetSelectedRows();

            List<long> rowIdx = new List<long>(rowHandles.Length);
            foreach (int rowHandle in rowHandles)
            {
                var row = (DataRowView) gridView1.GetRow(rowHandle);
                rowIdx.Add(Convert.ToInt64(row[IdColumnName]));
            }
            return rowIdx;
        }

        private bool IsRowDisabled(long rowId)
        {
            return LockedIntersections.IsRowDisabled(rowId) && !_notInUow.Contains(rowId);
        }

        private bool ValidateClipboardData()
        {
            string[] data = ClipboardData().Split(new[] {'\n'}, StringSplitOptions.RemoveEmptyEntries);

            if (data.Length < 1) return false;

            //first row are column heading.
            List<string> headings = data[0].Split(new [] { '\r', '\x09' }, StringSplitOptions.RemoveEmptyEntries).ToList();

            //Validate that the column headings exist in the grid.
            if (gridView1.Columns.Cast<GridColumn>().Where(dc => dc.Visible).Any(dc => !headings.Contains(dc.Caption)))
            {
                return false;
            }
            int setPos = headings.IndexOf(SetColumnName);
            if (setPos >= 0)
            {
                headings.RemoveAt(setPos);
            }

            for (int i = 1; i < data.Length; i++)
            {
                List<string> rowData = data[i].Split(new[] { '\r', '\x09' }, StringSplitOptions.RemoveEmptyEntries).ToList();
                if (setPos >= 0)
                {
                    rowData.RemoveAt(setPos);
                }

                for (int j = 0; j < rowData.Count; j++)
                {
                    if (j >= _gridDataTable.Columns.Count) return false;
                    try
                    {
                        // ReSharper disable ReturnValueOfPureMethodIsNotUsed
                        Convert.ChangeType(rowData[j], _gridDataTable.Columns[headings[j]].DataType);
                        // ReSharper restore ReturnValueOfPureMethodIsNotUsed
                    }
                    catch (Exception ex)
                    {
                        PafApp.GetLogger().Warn("ValidateClipboard, threw exception: ", ex);
                        return false;
                    } 
                }
            }
            return true;
        }

        /// <summary>
        /// Loads the SimpleDimTrees object variables
        /// </summary>
        private void LoadSimpleDimTreeVars()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            IdColumnName = SimpleDimTrees.LockStatus.IdColumn.ColumnName;
            SetColumnName = SimpleDimTrees.LockStatus.SetColumn.ColumnName;
            LevelColumnName = SimpleDimTrees.LevelColumName;
            GenerationColumnName = SimpleDimTrees.GenerationColumName;
            MemberNameColumnName = SimpleDimTrees.MemberColumName;
            MemberKeyColumnName = SimpleDimTrees.MemberKeyColumn;
            ParentKeyColumnName = SimpleDimTrees.ParentKeyColum;
            DimensionAliasTables = new Dictionary<string, List<string>>(10);

            foreach(string dim in SimpleDimTrees.GetDimensionNames())
            {
                List<string> s = new List<string> { MemberNameColumnName };
                s.AddRange(SimpleDimTrees.GetAliasTables(dim, false));
                DimensionAliasTables.Add(dim, s);
            }

           

            VersionRoot = PafApp.PlanningVersion;
            VersionDimension = PafApp.VersionDimension;
            MeasureDimension = PafApp.MeasureDimension;
            MeasureRoot = PafApp.MeasureRoot;
            AppId = PafApp.ApplicationId;

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("LoadSimpleDimTrees runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        private void SaveFormLocation()
        {
            if (_building) return;
            if (!Visible) return;
            Settings.Default.SessionLockLocation = Location;
            Settings.Default.Save();
            Settings.Default.Reload();
        }
    
        /// <summary>
        /// Creates the individual trees used to ptopulate the popup controls.
        /// This only builds the Base Dimensions.  Attribute dimensions will be lazy loaded.
        /// </summary>
        private void CreatePopupDimTrees()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            _treeTables.Clear();
            foreach (var dim in SimpleDimTrees.GetDimensionNames().Where(dim => SimpleDimTrees.IsDimensionBase(dim)))
            {
                CreatePopupDimTree(dim);
            }
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("CreatePopupDimTrees runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Creates the individual tree used to ptopulate the popup controls.
        /// </summary>
        /// <param name="dimName">Name of the dimension to build.</param>
        private void CreatePopupDimTree(string dimName)
        {
            if (!_treeTables.ContainsKey(dimName))
            {
                Stopwatch stopwatch = Stopwatch.StartNew();

                DataTable dimTable = SimpleDimTrees.ToTreeListDataTable(dimName);
                if (dimTable == null) return;
                _treeTables.Add(dimName, dimTable);
                stopwatch.Stop();
                PafApp.GetLogger().InfoFormat("CreatePopupDimTree: {0}, runtime: {1} (ms)", new object[] { dimName, stopwatch.ElapsedMilliseconds.ToString("N0") });
            }
        }

        /// <summary>
        /// Remove duplicate rows in the DataTable.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <returns></returns>
        private DataTable RemoveDuplicates(DataTable dataTable)
        {
            List<string> dims = new List<string> {IdColumnName, SetColumnName};

            return dataTable.RemoveDuplicateRows(dims);
        }

        /// <summary>
        /// Validate that the array of dims match the data columns
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        private bool ValidateTableDimensionality(DataTable dt)
        {
            if (dt == null) return false;
            List<string> tableDims = SimpleDimTrees.GetDimensionNames();
            bool isValid = true;

            foreach (DataColumn dc in dt.Columns)
            {
                if (dc.ColumnName.Equals(IdColumnName)) continue;
                if (dc.ColumnName.Equals(SetColumnName)) continue;

                if (!tableDims.Contains(dc.ColumnName))
                {
                    PafApp.GetLogger().Warn("Project Dimension: \"" + dc.ColumnName + "\" does not exist in the table.");
                    isValid = false;
                    break;
                }
            }

            if (isValid)
            {
                foreach (string s in tableDims.Where(s => dt.GetDataColumn(s) == null))
                {
                    PafApp.GetLogger().Warn("Table Dimension: \"" + s + "\" does not exist in the project.");
                    isValid = false;
                    break;
                }
            }

            return isValid;

        }

        /// <summary>
        /// Creates a new data row from an Intersection.
        /// If the row exists, then it is NOT added.
        /// </summary>
        /// <param name="intersection">Intersection to add.</param>
        /// <param name="enableRow">Enable (Set) the row</param>
        /// <param name="checkForExisting">Check for an existing row/intersection</param>
        private void AddDataRow(Intersection intersection, bool enableRow, bool checkForExisting)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            if (intersection == null) return;

            if (checkForExisting)
            {
                //Reformat the the intersection (replace Not Inialized with UOW).
                foreach (string dim in from dim in intersection.AxisSequence
                                       let coordValue = intersection.GetCoordinate(dim)
                                       where coordValue.Equals(Intersection.NotIntialized)
                                       select dim)
                {
                    intersection.SetCoordinate(dim, UOW);
                }
                //Clone an intersection, as to not change the original
                Intersection tempInter = intersection.DeepClone();
                //Resolve UOW to actual member name.
                Intersection resolvedInter = ResolveIntersection(tempInter);
                //Find if the Intersection already exists.
                if (_gridDataTable.ContainsIntersection(resolvedInter))
                {
                    return;
                }
            }

            _gridDataTable.BeginInit();
            _gridDataTable.BeginLoadData();
            if (_gridDataTable.DataSet != null) _gridDataTable.DataSet.EnforceConstraints = false;

            DataRow dr = _gridDataTable.NewRow();
            dr[SetColumnName] = enableRow;
            foreach (string dim in intersection.AxisSequence)
            {
                string coordValue = intersection.GetCoordinate(dim);
                dr[dim] = coordValue;
            }
            _gridDataTable.Rows.Add(dr);

            _gridDataTable.EndInit();
            _gridDataTable.EndLoadData();
            _gridDataTable.AcceptChanges();
            if (_gridDataTable.DataSet != null) _gridDataTable.DataSet.AcceptChanges();

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("AddDataRow runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        private void SetPopupButtonStatus(bool copyButton, bool pasteButton)
        {
            barButtonCopyRow.Enabled = copyButton;
            barButtonPasteRow.Enabled = pasteButton;

        }

        /// <summary>
        /// Validate the members.  If an invalid member is found, then the row index is added
        /// to the Disabled Rows collection.
        /// </summary>
        private void ValidateMembers()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            LockedIntersections.DisabledRows.Clear();
            _notInUow.Clear();

            long id = 0;
            foreach (DataRow dr in _gridDataTable.Rows)
            {
                foreach (DataColumn dc in _gridDataTable.Columns)
                {
                    //If the columns are non dim cols, continue
                    if (dc.ColumnName.Equals(IdColumnName))
                    {
                        id = Convert.ToInt64(dr[IdColumnName]);
                        continue;
                    }
                    //Get the value from the set column,
                    //if calcs are pending and the row is set, then don't allow the user to change it.
                    if (dc.ColumnName.Equals(SetColumnName))
                    {
                        bool setValue = Convert.ToBoolean(dr[SetColumnName]);
                        if (setValue && CalculationsPending)
                        {
                            LockedIntersections.DisableRow(id);
                            _notInUow.Add(id);
                            break;
                        }
                        continue;
                    }
                    //Get the resolved member name.
                    string value = ResolveMemberName(dr[dc.ColumnName], dc.ColumnName);
                    //If the string is null, then contineu
                    if (String.IsNullOrEmpty(value)) continue;
                    //Check for multi members...
                    string[] members = value.Split(new [] {","} , StringSplitOptions.RemoveEmptyEntries);
                    //Validate the member(s)
                    bool validMember = members.All(member => SimpleDimTrees.GetMemberKey(member, dc.ColumnName) > 0);
                    //if the member(s) are valid, then continue.
                    if (validMember) continue;
                    //if the member name has a key it's valid, continue
                    //if (SimpleDimTrees.GetMemberKey(value, dc.ColumnName) > 0) continue;
                    //No key found, disable the row, and break to next row
                    dr[SetColumnName] = false;
                    LockedIntersections.DisableRow(id);
                    _notInUow.Add(id);
                    break;
                }
            }

            if(_gridDataTable.GetChanges() != null)
            {
                _gridDataTable.AcceptChanges();
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ValidateMembers runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Saves the DataTable (gridView bind) to the SimpleDimTress object.
        /// </summary>
        /// <param name="clearLockTables"></param>
        private void SaveSessionLocksTable(bool clearLockTables)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            if (_gridDataTable.GetChanges() != null)
            {
                _gridDataTable.AcceptChanges();
            }

            if (clearLockTables)
            {
                SimpleDimTrees.ClearLockStatusTable();
            }

            foreach (DataRow dr in _gridDataTable.Rows)
            {
                bool isSet = false;
                if (dr[SetColumnName] != DBNull.Value)
                {
                    isSet = Convert.ToBoolean(dr[SetColumnName]);
                }

                IEnumerable<Intersection> intersections = BlowOutMultiMembers(dr);

                foreach (Intersection intersection in intersections)
                {
                    long lockKey = SimpleDimTrees.AddLockStatus(isSet, 0, true);
                    foreach (DataColumn dc in _gridDataTable.Columns)
                    {
                        if (dc.ColumnName.Equals(SetColumnName) || dc.ColumnName.Equals(IdColumnName)) continue;
                        //string value = Convert.ToString(dr[dc.ColumnName]);
                        string value = intersection.GetCoordinate(dc.ColumnName);
                        if (!String.IsNullOrEmpty(value))
                        {
                            SimpleDimTrees.AddLockIntersection(dc.ColumnName, value, lockKey);
                        }
                    }
                }
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("SaveSessionLocks runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Resolves all "UOW" to the actual member name in the DataTable.
        /// </summary>
        private void ResolveMembersInTable()
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            if (_gridDataTable.GetChanges() != null)
            {
                _gridDataTable.AcceptChanges();
            }

            foreach (DataRow dr in _gridDataTable.Rows)
            {
                foreach (DataColumn dc in _gridDataTable.Columns)
                {
                    if (dc.ColumnName.Equals(IdColumnName)) continue;
                    if (dc.ColumnName.Equals(SetColumnName))
                    {
                        var obj = dr[dc.ColumnName];
                        if (obj == DBNull.Value)
                        {
                            dr[dc.ColumnName] = false;
                        }
                    }
                    if (!SimpleDimTrees.IsDimensionSynthetic(dc.ColumnName))
                    {
                        string value = ResolveMemberName(dr[dc.ColumnName], dc.ColumnName);
                        if (!String.IsNullOrEmpty(value))
                        {
                            dr[dc.ColumnName] = value;
                        }
                    }
                    else
                    {
                        SimpleDimTrees.MembersRow root = SimpleDimTrees.GetRoot(dc.ColumnName);
                        string value = ResolveMemberName(dr[dc.ColumnName], dc.ColumnName);
                        if (value.Contains(root.MemberName))
                        {
                            List<string> members = SimpleDimTrees.GetSyntheticMembers(dc.ColumnName);
                            if (members != null && members.Count > 0)
                            {
                                dr[dc.ColumnName] = String.Join(",", members.ToArray());
                            }
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(value))
                            {
                                dr[dc.ColumnName] = value;
                            }
                        }
                    }
                }
            }

            if (_gridDataTable.GetChanges() != null)
            {
                _gridDataTable.AcceptChanges();
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("ResolveMembersInTable runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }


        /// <summary>
        /// Blows out a DataRow (with multiple selections) to multiple DataRows.
        /// </summary>
        private IEnumerable<Intersection> BlowOutMultiMembers(DataRow row)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            DataRow dr = row;
            List<Intersection> results = new List<Intersection>();
            foreach (DataColumn dc in _gridDataTable.Columns)
            {
                if (dc.ColumnName.Equals(SetColumnName) || dc.ColumnName.Equals(IdColumnName)) continue;
                SessionLockSelectedMembers members = new SessionLockSelectedMembers(dr[dc.ColumnName]);
                if (members.MultiMember())
                {
                    results = members.CreateIntersections(results, DimensionPriority, dc.ColumnName);
                }
            }

            if (results.Count == 0)
            {
                results.Add(new Intersection(DimensionPriority));
            }

            FillIntersectionCoordFromDataRow(results, dr);

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("BlowOutMultiMembers runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));

            return results;
        }


        /// <summary>
        /// Fills in Intersection Coordinates from a DataRow.
        /// </summary>
        /// <param name="intersection"></param>
        /// <param name="dr"></param>
        private void FillIntersectionCoordFromDataRow(List<Intersection> intersection, DataRow dr)
        {
            foreach (DataColumn dc in _gridDataTable.Columns)
            {
                if (dc.ColumnName.Equals(SetColumnName) || dc.ColumnName.Equals(IdColumnName) ) continue;

                SessionLockSelectedMembers members = new SessionLockSelectedMembers(dr[dc.ColumnName]);

                if (members.SelectionEmpty()) continue;

                if (members.MultiMember()) continue;

                foreach (Intersection i in intersection)
                {
                    i.AddCoordinate(dc.ColumnName, members.SingleSelection());
                }
            }
        }

        private void BindData(DataTable dataTable)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            gridControl1.BeginUpdate();
            gridView1.BeginUpdate();
            gridView1.BeginDataUpdate();

            gridControl1.DataSource = dataTable;
            gridView1.Columns[IdColumnName].Visible = false;

            //Remove the stupid auto caption generation that DevExp does
            foreach (GridColumn column in gridView1.Columns)
            {
                column.Caption = column.FieldName;
            }

            gridView1.EndDataUpdate();
            gridView1.EndUpdate();
            gridControl1.EndUpdate();

            int currentRowHandle = gridView1.GetVisibleRowHandle(0);
            while (currentRowHandle != DevExpress.XtraGrid.GridControl.InvalidRowHandle)
            {
                var row = (DataRowView)gridView1.GetRow(currentRowHandle);
                if (!LockedIntersections.IsRowDisabled(Convert.ToInt64(row[IdColumnName])))
                {
                    break;
                }
                currentRowHandle = gridView1.GetNextVisibleRow(currentRowHandle);
            }
            
            gridView1.ClearSelection();

            gridView1.SelectRow(currentRowHandle);

            gridView1.FocusedRowHandle = currentRowHandle;

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("GridDataBind runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        private void ShowHideAttributeColumns(bool hidden)
        {
            foreach (GridColumn gc in gridView1.Columns)
            {
                string columnName = gc.FieldName;
                long key = SimpleDimTrees.GetDimensionKey(columnName, false);
                //if the column is Set or is a base dimension.  Continue.
                if (key < 0) continue;
                if (SimpleDimTrees.IsDimensionBase(key)) continue;
                //default to hidden.
                bool show = false;
                if (hidden)
                {
                    if (_gridDataTable.Rows.Cast<DataRow>().Any(dr => dr[columnName] != DBNull.Value))
                    {
                        show = true;
                    }
                }
                else
                {
                    show = true;
                }
                gc.Visible = show;
            }
        }

        /// <summary>
        /// Validate a Member name for a dimension.
        /// </summary>
        /// <param name="dimensionName"></param>
        /// <param name="memberNames"></param>
        /// <returns></returns>
        private bool ValidateCellValue(string dimensionName, SessionLockSelectedMembers memberNames)
        {
            //Ignore the "Set" column
            if (dimensionName.Equals(SetColumnName)) return true;
            //Ignore the "ID" column
            if (dimensionName.Equals(IdColumnName)) return true;
            //Ignore the member name if it is "UOW" because this is auto resolved.
            if (memberNames.ContainsMember(UOW)) return true;
            //If the dimension name is null/blank return false.
            if (String.IsNullOrEmpty(dimensionName)) return false;

            //If this is an attribute tree, then only
            //do the validate if a member has been specified.
            if (!SimpleDimTrees.IsDimensionBase(dimensionName))
            {
                foreach (string memberName in memberNames.Selections)
                {
                    if (!String.IsNullOrEmpty(memberName))
                    {
                        if (SimpleDimTrees.GetMemberKey(memberName, dimensionName) < 0)
                        {
                            return false;
                        }
                    }
                }
                return true;
            }

            //If the member name is null, and in this case for a base dimension (which is required) return false, 
            if (memberNames.SelectionEmpty()) return false;

            //Finally Validate the member name.
            foreach (string member in memberNames.Selections)
            {
                if (SimpleDimTrees.GetMemberKey(member, dimensionName) < 0)
                {
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Prompts user to delete currently selected rows.
        /// </summary>
        private void DeleteRows()
        {
            //int rowHandle = gridView1.FocusedRowHandle;
            List<long> rowIds = GetSelectedRowIds();

            bool result = rowIds.Count > 1 ? ConfirmRowsDelete() : ConfirmRowDelete();

            if (result)
            {
                return;
            }

            foreach (long rowId in rowIds)
            {
                LockedIntersections.DisabledRows.Remove(rowId);
                _notInUow.Remove(rowId);
            }

            gridView1.DeleteSelectedRows();

            
        }


        /// <summary>
        /// Prompts user to delete currently selected row.
        /// </summary>
        private bool ConfirmRowDelete()
        {
            return MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.ConfirmRowDelete"),
                                   _appTitle,
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question) != DialogResult.Yes;
        }

        /// <summary>
        /// Prompts user to delete currently selected row.
        /// </summary>
        private bool ConfirmRowsDelete()
        {
            return MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.ConfirmRowsDelete"),
                                   _appTitle,
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question) != DialogResult.Yes;
        }

        private bool IsRowSet(int rowHandle)
        {
            DataRow row = gridView1.GetDataRow(rowHandle);
            return row != null && Convert.ToBoolean(row[SetColumnName]);
        }

        #region Event Handlers
        // ReSharper disable InconsistentNaming

        private void cmdOk_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor = Cursors.WaitCursor;

                //Save the current location.
                SaveFormLocation();

                //Resolve "UOW" in the gridView DataTable.
                ResolveMembersInTable();

                //Set the modified flags.
                if (_removedStatus.Count > 0)
                {
                    IntersectionsRemoved = true;
                }

                foreach (int i in _changedStatus)
                {
                    if (IsRowSet(i))
                    {
                        IntersectionsChanged = true;
                        break;
                    }
                }

                //Create multiple row where the user selected multiple members.
                //KRM we no longer do this. Multiple members are kept on a single row.
                //BlowOutMultiMembers();

                //remove any duplicates.
                RemoveDuplicates(_gridDataTable);

                //Save to the SimpleDimTrees object.
                SaveSessionLocksTable(true);

                //List<long> parents = SimpleDimTrees.GetParentWhereAllChildrenAreLocked("Location");

                if (Locks == null)
                {
                    Locks = new List<DescendantIntersection>(10);
                }
                else
                {
                    Locks.Clear();
                }



                List<long> lockid = SimpleDimTrees.GetLockIds(true);
                foreach (long id in lockid)
                {
                    DescendantIntersection di = new DescendantIntersection(SimpleDimTrees.GetCoordinate(id, DimensionPriority));
                    di.HasAttributes = SimpleDimTrees.HasAttributes(id);
                    Locks.Add(di);
                }

                //Serialize the datatable to the settings file.
                Settings.Default.SessionLocks = _gridDataSet;
                Settings.Default.Save();
                Settings.Default.Reload();

                DefaultLockIntersection = null;

                //SimpleDimTrees.Save(@"c:\", @"SimpleDimTrees.xml");

                //If the user set the intersections, then perform the service call.
                if (Locks.Count > 0)
                {
                    //Locks = PafApp.GetDescendants(Locks);
                    PafApp.GetDescendants(Locks);
                    SetControlStatus(false);
                    marqueeProgressBarControl1.Visible = true;
                    DialogResult = DialogResult.None;
                }
                else
                {
                    Hide();

                    PafApp.GetLogger().Info("cmdOk_Click - frmSessionLocks hide.");

                    Cursor = Cursors.Default;
                }

            }
            finally
            {
                //Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// This Event fires when the user clicks on the cell.  
        /// This Function does all the necessary "stuff" to setup the popup control.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridView1_CustomRowCellEditForEditing(object sender, CustomRowCellEditEventArgs e)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            string dimensionName = e.Column.FieldName;
            long dimensionKey = SimpleDimTrees.GetDimensionKey(dimensionName, false);

            if (dimensionKey <= 0) return;

            DataTable dt;
            if (!_treeTables.ContainsKey(dimensionName))
            {
                CreatePopupDimTree(dimensionName);
            }
            if (_treeTables.TryGetValue(dimensionName, out dt))
            {

                List<int> allowedSelections = null;
                if (SimpleDimTrees.GetDimensionLevelZeroOnly(dimensionName))
                {
                    allowedSelections = _validLevels;
                }

                //Get the last selected memberName or the first selectable node.
                string mbrName = ResolveMemberName(e.CellValue, dimensionName);
                SessionLockSelectedMembers members = new SessionLockSelectedMembers(mbrName);

                List<string> tables;
                if(!DimensionAliasTables.TryGetValue(dimensionName, out tables))
                {
                    tables = MemberNameColumnName.ToSingleList();
                }

                List<ComboBoxDisplay> tempTables = new List<ComboBoxDisplay>();
                foreach (string table in tables)
                {
                    tempTables.Add(new ComboBoxDisplay(table.AddSpacesToSentence(), table));

                }


                _popupEditor = new PopupEditor(dt, dimensionName, MemberKeyColumnName, ParentKeyColumnName, LevelColumnName, GenerationColumnName, MemberNameColumnName, tempTables, members, allowedSelections)
                    {Tag = dimensionName};
            }
            else
            {
                PafApp.MessageBox().Show("Could not find data table, something really bad just happened.", PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"));
                return;
            }

            _popupEditor.popupContainerControl1.Tag = dimensionName;
            repositoryItemPopupContainerEdit.PopupControl = _popupEditor.popupContainerControl1;
            repositoryItemPopupContainerEdit.ShowPopupCloseButton = false;
            repositoryItemPopupContainerEdit.CloseOnOuterMouseClick = true;
            //repositoryItemPopupContainerEdit.ContextMenu
            e.RepositoryItem = repositoryItemPopupContainerEdit;

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("gridView1_CustomRowCellEditForEditing runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        private void SetControlStatus(bool status)
        {
            gridControl1.Enabled = status;
            cmdCancel.Enabled = status;
            cmdOk.Enabled = status;
            chkShowAttributes.Enabled = status;
        }

        /// <summary>
        /// Resolved the "UOW" references in the Intersection.
        /// Note: This may not return a valid intersection.  If multiple members are found for a Dimension, they are comma seperated.
        /// This is done becuase that's how they are stored in the DataTable.
        /// Use this Intersection for comparison [DataTable.ContainsIntersection(Intersection)]
        /// </summary>
        /// <param name="intersection"></param>
        /// <returns></returns>
        private Intersection ResolveIntersection(Intersection intersection)
        {
            Intersection result = new Intersection(intersection.AxisSequence);
            foreach (string dim in intersection.AxisSequence)
            {
                long dimensionKey = SimpleDimTrees.GetDimensionKey(dim, false);
                bool isBase = SimpleDimTrees.IsDimensionBase(dimensionKey);
                string coord = intersection.GetCoordinate(dim);

                //If is Attribute and blank, then just return blank
                if (!isBase && String.IsNullOrEmpty(coord)) return null;


                if (String.IsNullOrEmpty(coord) || coord.Equals(UOW))
                {
                    if (dim.Equals(MeasureDimension))
                    {
                        coord = MeasureRoot;
                    }
                    else if (dim.Equals(VersionDimension))
                    {
                        coord = VersionRoot;
                    }
                    else
                    {
                        if (!SimpleDimTrees.IsDimensionSynthetic(dimensionKey))
                        {
                            int maxMbrLevel = SimpleDimTrees.GetMemberMaxLevel(dimensionKey, true);
                            SimpleDimTrees.MembersRow firstMember = SimpleDimTrees.GetFirstMember(dimensionKey, maxMbrLevel);
                            if (firstMember != null)
                            {
                                coord = firstMember.MemberName;
                            }
                        }
                        else
                        {
                            List<string> members = SimpleDimTrees.GetSyntheticMembers(dimensionKey);
                            if (members != null && members.Count > 0)
                            {
                                coord = String.Join(",", members.ToArray());
                            }
                        }
                    }
                }
                result.SetCoordinate(dim, coord);
            }
            return result;
        }

        /// <summary>
        /// Resolves the "UOW" member to the appropiate dimension member.
        /// </summary>
        /// <param name="cellValue"></param>
        /// <param name="dimensionName"></param>
        /// <returns></returns>
        private string ResolveMemberName(object cellValue, string dimensionName)
        {
            long dimensionKey = SimpleDimTrees.GetDimensionKey(dimensionName, false);
            bool isBase = SimpleDimTrees.IsDimensionBase(dimensionKey);


            
            if (!isBase && cellValue == null) return null;


            //Get the last selected memberName or the first selectable node.
            string mbrName = Convert.ToString(cellValue);

            //If is Attribute and blank, then just return blank
            if (!isBase && String.IsNullOrEmpty(mbrName)) return null;


            if (String.IsNullOrEmpty(mbrName) || mbrName.Equals(UOW))
            {
                if (dimensionName.Equals(MeasureDimension))
                {
                    mbrName = MeasureRoot;
                }
                else if (dimensionName.Equals(VersionDimension))
                {
                    mbrName = VersionRoot;
                }
                else
                {
                    if (!SimpleDimTrees.IsDimensionSynthetic(dimensionKey))
                    {
                        int maxMbrLevel = SimpleDimTrees.GetMemberMaxLevel(dimensionKey, true);
                        SimpleDimTrees.MembersRow firstMember = SimpleDimTrees.GetFirstMember(dimensionKey, maxMbrLevel);
                        if (firstMember != null)
                        {
                            mbrName = firstMember.MemberName;
                        }
                    }
                    else
                    {
                        List<string> members = SimpleDimTrees.GetSyntheticMembers(dimensionKey);
                        if (members != null && members.Count > 0)
                        {
                            mbrName = String.Join(",", members.ToArray());
                        }
                    }
                }
            }
            return mbrName;
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            //Save the current location.
            SaveFormLocation();
            //Restore the previous data table
            Settings.Default.SessionLocks = _snapshotDataSet;
            Settings.Default.Save();
            Settings.Default.Reload();
            //Reset the default intersection.
            DefaultLockIntersection = null;
            //Close Me
            Close();
        }

        private void chkShowAttributes_CheckedChanged(object sender, EventArgs e)
        {
            CheckEdit box = (CheckEdit)sender;
            barCheckShowAttributes.Checked = box.Checked;
            ShowHideAttributeColumns(box.Checked);
            Settings.Default.SessionLockHideAttributes = box.Checked;
            Settings.Default.Save();
            Settings.Default.Reload();
        }

        /// <summary>
        /// This event fires when the user closes the popup control.
        /// Need to handle updating the cell, etc.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void repositoryItemPopupContainerEdit_QueryResultValue(object sender, QueryResultValueEventArgs e)
        {
            //object node = _popupEditor.GetSelections();

            SessionLockSelectedMembers nodes = _popupEditor.GetSelections();

            e.Value = nodes.ToString();

        }

        private void repositoryItemPopupContainerEdit_CloseUp(object sender, CloseUpEventArgs e)
        {
            
            ////This code handles when the user clicks off the popup editor.
            //if (e.AcceptValue)
            //{
            //    if (e.CloseMode == PopupCloseMode.Immediate)
            //    {
            //        //Get the old value back and set it.
            //        SessionLockSelectedMembers nodes = _popupEditor.OriginalMemberNames;
            //        _popupEditor.SetSelections(nodes);
            //        e.Value = nodes.ToString();
            //        return;
            //    }
            //}

            PopupContainerEdit popupContainerEditor = sender as PopupContainerEdit;
            if (popupContainerEditor !=  null)
            {
                SessionLockSelectedMembers members = new SessionLockSelectedMembers(e.Value);
                _popupEditor.SetSelections(members);
            }
        }

        private void gridView1_PopupMenuShowing(object sender, PopupMenuShowingEventArgs e)
        {
            GridView view = sender as GridView;

            if (view != null)
            {
                GridHitInfo hitInfo = view.CalcHitInfo(e.Point);

                if (hitInfo.InRow)
                {
                    SetPopupButtonStatus(true, true);

                    view.FocusedRowHandle = hitInfo.RowHandle;

                    popupMenu1.ShowPopup(barManager1, gridControl1.PointToScreen(hitInfo.HitPoint));

                }
                else if(hitInfo.HitTest == GridHitTest.EmptyRow)
                {
                    SetPopupButtonStatus(false, false);
                    
                    view.FocusedRowHandle = hitInfo.RowHandle;

                    popupMenu1.ShowPopup(barManager1, gridControl1.PointToScreen(hitInfo.HitPoint));
                }
            }
        }

        private void barButtonCopyRow_ItemClick(object sender, ItemClickEventArgs e)
        {
            gridView1.CopyToClipboard();
        }

        private void popupMenu1_BeforePopup(object sender, System.ComponentModel.CancelEventArgs e)
        {
            List<long> rowIds = GetSelectedRowIds();

            bool allowDelete = true;
            bool allowCopy = true;
            foreach (long rowId in rowIds)
            {
                if (!ValidateDeleteRow(rowId))
                {
                    allowDelete = false;
                }
                if (!ValidateCopyRow(rowId))
                {
                    allowCopy = false;
                }
                if (!allowCopy && !allowDelete) break;
            }
            barButtonPasteRow.Enabled = ValidateClipboardData();
            barButtonDeleteRow.Enabled = allowDelete;
            //If you can't delete it, then you can't copy it.
            //becuase it's disabled for some reason.
            barButtonCopyRow.Enabled = allowCopy;
        }

        private void barButtonPasteRow_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            string[] data = ClipboardData().Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            if (data.Length < 1) return;

            List<string> headings = data[0].Split(new [] { '\r', '\x09' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            int setPos = headings.IndexOf(SetColumnName);
            if (setPos >= 0)
            {
                headings.RemoveAt(setPos);
            }

            Intersection heading = new Intersection(headings.ToArray());
            for (int i = 1; i < data.Count(); i++)
            {
                Intersection temp = heading.DeepClone();
                List<string> coord = data[i].Split(new[] {'\r', '\x09'}, StringSplitOptions.None).ToList();
                if (setPos >= 0)
                {
                    coord.RemoveAt(setPos);
                }
                temp.SetCoordinates(coord.ToArray());
                AddDataRow(temp, false, false);
            }
        }

        private void frmSessionLocks_Resize(object sender, EventArgs e)
        {
            if (_building) return;
            Settings.Default.SessionLockHeight = Height;
            Settings.Default.SessionLockWidth = Width;
            Settings.Default.Save();
            Settings.Default.Reload();
        }

        private void gridView1_ValidateRow(object sender, DevExpress.XtraGrid.Views.Base.ValidateRowEventArgs e)
        {
            GridView view = sender as GridView;

            if(view == null) return;
            view.ClearColumnErrors();
            long rowId = Convert.ToInt64(view.GetRowCellValue(e.RowHandle, IdColumnName));

            foreach(GridColumn column in view.Columns)
            {

                object rowValue = view.GetRowCellValue(e.RowHandle, column);
                
                SessionLockSelectedMembers members = new SessionLockSelectedMembers(Convert.ToString(rowValue));

                if (rowValue == null || !ValidateCellValue(column.FieldName, members))
                {
                    e.Valid = false;
                    view.SetColumnError(column, PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.Exception.RowValidationCellMessage"));
                }
            }
            //Add invalid row stuff here.
            if (e.Valid && _notInUow.Contains(rowId))
            {
                _notInUow.Remove(rowId);
                LockedIntersections.EnableRow(rowId);
            }
        }

        private void gridView1_InvalidRowException(object sender, DevExpress.XtraGrid.Views.Base.InvalidRowExceptionEventArgs e)
        {
            e.ErrorText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.Exception.RowValidation");
            e.WindowCaption = _appTitle;
            e.ExceptionMode = ExceptionMode.DisplayError;
        }

        private void gridView1_RowUpdated(object sender, DevExpress.XtraGrid.Views.Base.RowObjectEventArgs e)
        {
            _gridDataTable.AcceptChanges();
        }

        private void gridView1_RowClick(object sender, RowClickEventArgs e)
        {
            List<long> rowIds = GetSelectedRowIds();

            bool allowDelete = true;
            foreach (long rowId in rowIds)
            {
                if (!ValidateDeleteRow(rowId))
                {
                    allowDelete = false;
                    break;
                }
            }
 

            //int rowIndex = GetSelectedRowIndex();

            ControlNavigator navigator = gridControl1.EmbeddedNavigator;
            navigator.Buttons.BeginUpdate();
            try
            {
                //navigator.Buttons.Remove.Enabled = ValidateDeleteRow(rowIndex);
                navigator.Buttons.Remove.Enabled = allowDelete;
            }
            finally
            {
                navigator.Buttons.EndUpdate();
            }
        }

        private void barButtonDeleteRow_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            DeleteRows();
        }

        private void gridView1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Delete)// && e.Modifiers == Keys.Control)
            {
                DeleteRows();
            }
        }

        private void gridControl1_EmbeddedNavigator_ButtonClick(object sender, NavigatorButtonClickEventArgs e)
        {
            if (e.Button.ButtonType == NavigatorButtonType.Remove)
            {
                e.Handled = true;
                DeleteRows();
            }
        }

        private void gridView1_CellValueChanging(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            if (e.Column.FieldName.Equals(IdColumnName)) return;

            if (e.Column.FieldName.Equals(SetColumnName))
            {
                if (!Convert.ToBoolean(e.Value))
                {
                    _removedStatus.Add(e.RowHandle);
                }
                else
                {
                    _removedStatus.Remove(e.RowHandle);
                }
            }
            else
            {
                _changedStatus.Add(e.RowHandle);
            }

        }

        private void barCheckHideInvalid_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            BarCheckItem item = e.Item as BarCheckItem;
            if (item == null) return;

            if (item.Checked)
            {
                gridView1.ActiveFilter.Clear();
                List<string> keys = new List<string>();
                for (int i = 0; i < gridView1.DataRowCount; i++)
                {
                    long key = Convert.ToInt64(gridView1.GetRowCellValue(i, gridView1.Columns[IdColumnName]));
                    if (LockedIntersections.IsRowDisabled(key))
                    {
                        keys.Add(Convert.ToString(key));
                    }
                }
                string filterString = String.Format("NOT [{0}] IN ({1})", IdColumnName, String.Join(",", keys.ToArray()));
                gridView1.ActiveFilterString = filterString;
            }
            else
            {
                gridView1.ActiveFilter.Clear();
            }
        }

        private void barCheckShowAttributes_CheckedChanged(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            //chkShowAttributes.Checked = e.Item.
        }

        private void gridView1_CustomFilterDisplayText(object sender, ConvertEditValueEventArgs e)
        {
            UnaryOperator filter = (UnaryOperator)e.Value;
            if (filter == null) return;

            if (filter.OperatorType != UnaryOperatorType.Not) return;

            InOperator inOperator = (InOperator) filter.Operand;

            if (inOperator == null) return;

            if (inOperator.LeftOperand.LegacyToString() != String.Format("[{0}]", IdColumnName)) return;
            e.Value = new OperandValue(PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmSessionLock.chkShowAttributes.HiddenRowsFilterMessage"));
            e.Handled = true;
        }

        private void gridView1_ColumnFilterChanged(object sender, EventArgs e)
        {
            if (gridView1.ActiveFilter.Criteria == null)
            {
                barCheckHideInvalid.Checked = false;
            }
        }

        private void barCheckShowAttributes_ItemClick(object sender, ItemClickEventArgs e)
        {
            BarCheckItem checkItem = (BarCheckItem) e.Item;
            if (checkItem == null) return;
            chkShowAttributes.Checked = checkItem.Checked;
        }

        private void frmSessionLocks_FormClosing(object sender, FormClosingEventArgs e)
        {
            PafApp.GetDescendantsCompleted -= PafApp_GetDescendantsCompleted;
            PafApp.GetDescendantsError -= PafApp_GetDescendantsError;
        }

        // ReSharper restore InconsistentNaming
        #endregion Event Handlers
    }
}