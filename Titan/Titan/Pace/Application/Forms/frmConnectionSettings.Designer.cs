﻿namespace Titan.Pace.Application.Forms
{
    /// <remarks/>
    partial class frmConnectionSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.grpBoxProxy = new System.Windows.Forms.GroupBox();
            this.chkUseProxy = new DevExpress.XtraEditors.CheckEdit();
            this.cmdOk = new System.Windows.Forms.Button();
            this.cmdCancel = new System.Windows.Forms.Button();
            this.lblTimeOut = new System.Windows.Forms.Label();
            this.grpBoxEvaluationTimeout = new System.Windows.Forms.GroupBox();
            this.spinEditEvalTimeout = new DevExpress.XtraEditors.SpinEdit();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.grpBoxClearServerList = new System.Windows.Forms.GroupBox();
            this.cmdClearServerUrlList = new System.Windows.Forms.Button();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.grpBoxConnectionTimeout = new System.Windows.Forms.GroupBox();
            this.spinEditConnectionTimeout = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditRetry = new DevExpress.XtraEditors.SpinEdit();
            this.lblRetry = new System.Windows.Forms.Label();
            this.lblConnectionTimeout = new System.Windows.Forms.Label();
            this.listViewServerConnections = new System.Windows.Forms.ListView();
            this.cmdRemoveServerUrl = new System.Windows.Forms.Button();
            this.grpBoxProxy.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chkUseProxy.Properties)).BeginInit();
            this.grpBoxEvaluationTimeout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEvalTimeout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.grpBoxClearServerList.SuspendLayout();
            this.grpBoxConnectionTimeout.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditConnectionTimeout.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditRetry.Properties)).BeginInit();
            this.SuspendLayout();
            // 
            // grpBoxProxy
            // 
            this.grpBoxProxy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxProxy.Controls.Add(this.chkUseProxy);
            this.grpBoxProxy.Location = new System.Drawing.Point(12, 12);
            this.grpBoxProxy.Name = "grpBoxProxy";
            this.grpBoxProxy.Size = new System.Drawing.Size(526, 50);
            this.grpBoxProxy.TabIndex = 0;
            this.grpBoxProxy.TabStop = false;
            this.grpBoxProxy.Text = "_";
            // 
            // chkUseProxy
            // 
            this.chkUseProxy.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.chkUseProxy.Location = new System.Drawing.Point(6, 19);
            this.chkUseProxy.Name = "chkUseProxy";
            this.chkUseProxy.Properties.Caption = "_";
            this.chkUseProxy.Size = new System.Drawing.Size(514, 19);
            this.chkUseProxy.TabIndex = 0;
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(357, 320);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 2;
            this.cmdOk.Text = "_";
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(472, 320);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 3;
            this.cmdCancel.Text = "_";
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // lblTimeOut
            // 
            this.lblTimeOut.AutoSize = true;
            this.lblTimeOut.Location = new System.Drawing.Point(6, 27);
            this.lblTimeOut.Name = "lblTimeOut";
            this.lblTimeOut.Size = new System.Drawing.Size(13, 13);
            this.lblTimeOut.TabIndex = 6;
            this.lblTimeOut.Text = "_";
            // 
            // grpBoxEvaluationTimeout
            // 
            this.grpBoxEvaluationTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxEvaluationTimeout.Controls.Add(this.spinEditEvalTimeout);
            this.grpBoxEvaluationTimeout.Controls.Add(this.lblTimeOut);
            this.grpBoxEvaluationTimeout.Location = new System.Drawing.Point(387, 68);
            this.grpBoxEvaluationTimeout.Name = "grpBoxEvaluationTimeout";
            this.grpBoxEvaluationTimeout.Size = new System.Drawing.Size(151, 50);
            this.grpBoxEvaluationTimeout.TabIndex = 1;
            this.grpBoxEvaluationTimeout.TabStop = false;
            this.grpBoxEvaluationTimeout.Text = "_";
            // 
            // spinEditEvalTimeout
            // 
            this.spinEditEvalTimeout.EditValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spinEditEvalTimeout.Location = new System.Drawing.Point(94, 20);
            this.spinEditEvalTimeout.Name = "spinEditEvalTimeout";
            this.spinEditEvalTimeout.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.True;
            this.spinEditEvalTimeout.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditEvalTimeout.Properties.DisplayFormat.FormatString = "N0";
            this.spinEditEvalTimeout.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditEvalTimeout.Properties.EditFormat.FormatString = "N0";
            this.spinEditEvalTimeout.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditEvalTimeout.Properties.IsFloatValue = false;
            this.spinEditEvalTimeout.Properties.Mask.EditMask = "N0";
            this.spinEditEvalTimeout.Properties.MaxValue = new decimal(new int[] {
            25,
            0,
            0,
            0});
            this.spinEditEvalTimeout.Properties.MinValue = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.spinEditEvalTimeout.Size = new System.Drawing.Size(41, 20);
            this.spinEditEvalTimeout.TabIndex = 12;
            this.spinEditEvalTimeout.EditValueChanging += new DevExpress.XtraEditors.Controls.ChangingEventHandler(this.spinEditEvalTimeout_EditValueChanging);
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // grpBoxClearServerList
            // 
            this.grpBoxClearServerList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxClearServerList.Controls.Add(this.listViewServerConnections);
            this.grpBoxClearServerList.Controls.Add(this.cmdRemoveServerUrl);
            this.grpBoxClearServerList.Controls.Add(this.cmdClearServerUrlList);
            this.grpBoxClearServerList.Location = new System.Drawing.Point(12, 124);
            this.grpBoxClearServerList.Name = "grpBoxClearServerList";
            this.grpBoxClearServerList.Size = new System.Drawing.Size(526, 175);
            this.grpBoxClearServerList.TabIndex = 4;
            this.grpBoxClearServerList.TabStop = false;
            this.grpBoxClearServerList.Text = "_";
            // 
            // cmdClearServerUrlList
            // 
            this.cmdClearServerUrlList.Location = new System.Drawing.Point(9, 146);
            this.cmdClearServerUrlList.Name = "cmdClearServerUrlList";
            this.cmdClearServerUrlList.Size = new System.Drawing.Size(115, 23);
            this.cmdClearServerUrlList.TabIndex = 0;
            this.cmdClearServerUrlList.Text = "_";
            this.cmdClearServerUrlList.UseVisualStyleBackColor = true;
            this.cmdClearServerUrlList.Click += new System.EventHandler(this.cmdClearServerUrlList_Click);
            // 
            // grpBoxConnectionTimeout
            // 
            this.grpBoxConnectionTimeout.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpBoxConnectionTimeout.Controls.Add(this.spinEditConnectionTimeout);
            this.grpBoxConnectionTimeout.Controls.Add(this.spinEditRetry);
            this.grpBoxConnectionTimeout.Controls.Add(this.lblRetry);
            this.grpBoxConnectionTimeout.Controls.Add(this.lblConnectionTimeout);
            this.grpBoxConnectionTimeout.Location = new System.Drawing.Point(12, 68);
            this.grpBoxConnectionTimeout.Name = "grpBoxConnectionTimeout";
            this.grpBoxConnectionTimeout.Size = new System.Drawing.Size(304, 50);
            this.grpBoxConnectionTimeout.TabIndex = 5;
            this.grpBoxConnectionTimeout.TabStop = false;
            this.grpBoxConnectionTimeout.Text = "_";
            // 
            // spinEditConnectionTimeout
            // 
            this.spinEditConnectionTimeout.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditConnectionTimeout.Location = new System.Drawing.Point(94, 20);
            this.spinEditConnectionTimeout.Name = "spinEditConnectionTimeout";
            this.spinEditConnectionTimeout.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditConnectionTimeout.Properties.DisplayFormat.FormatString = "N0";
            this.spinEditConnectionTimeout.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditConnectionTimeout.Properties.EditFormat.FormatString = "N0";
            this.spinEditConnectionTimeout.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditConnectionTimeout.Properties.IsFloatValue = false;
            this.spinEditConnectionTimeout.Properties.Mask.EditMask = "N0";
            this.spinEditConnectionTimeout.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinEditConnectionTimeout.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditConnectionTimeout.Size = new System.Drawing.Size(41, 20);
            this.spinEditConnectionTimeout.TabIndex = 11;
            this.spinEditConnectionTimeout.ValueChanged += new System.EventHandler(this.spinEditConnectionTimeout_ValueChanged);
            this.spinEditConnectionTimeout.Validating += new System.ComponentModel.CancelEventHandler(this.spinEditConnectionTimeout_Validating);
            // 
            // spinEditRetry
            // 
            this.spinEditRetry.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditRetry.Location = new System.Drawing.Point(246, 20);
            this.spinEditRetry.Name = "spinEditRetry";
            this.spinEditRetry.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.spinEditRetry.Properties.DisplayFormat.FormatString = "N0";
            this.spinEditRetry.Properties.DisplayFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditRetry.Properties.EditFormat.FormatString = "N0";
            this.spinEditRetry.Properties.EditFormat.FormatType = DevExpress.Utils.FormatType.Numeric;
            this.spinEditRetry.Properties.IsFloatValue = false;
            this.spinEditRetry.Properties.Mask.EditMask = "N0";
            this.spinEditRetry.Properties.MaxValue = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.spinEditRetry.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditRetry.Size = new System.Drawing.Size(41, 20);
            this.spinEditRetry.TabIndex = 10;
            // 
            // lblRetry
            // 
            this.lblRetry.AutoSize = true;
            this.lblRetry.Location = new System.Drawing.Point(162, 27);
            this.lblRetry.Name = "lblRetry";
            this.lblRetry.Size = new System.Drawing.Size(13, 13);
            this.lblRetry.TabIndex = 9;
            this.lblRetry.Text = "_";
            // 
            // lblConnectionTimeout
            // 
            this.lblConnectionTimeout.AutoSize = true;
            this.lblConnectionTimeout.Location = new System.Drawing.Point(6, 27);
            this.lblConnectionTimeout.Name = "lblConnectionTimeout";
            this.lblConnectionTimeout.Size = new System.Drawing.Size(13, 13);
            this.lblConnectionTimeout.TabIndex = 7;
            this.lblConnectionTimeout.Text = "_";
            // 
            // listViewServerConnections
            // 
            this.listViewServerConnections.Location = new System.Drawing.Point(9, 29);
            this.listViewServerConnections.Name = "listViewServerConnections";
            this.listViewServerConnections.Size = new System.Drawing.Size(511, 106);
            this.listViewServerConnections.TabIndex = 2;
            this.listViewServerConnections.UseCompatibleStateImageBehavior = false;
            this.listViewServerConnections.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.listViewServerConnections_ItemChecked);
            this.listViewServerConnections.ItemDrag += new System.Windows.Forms.ItemDragEventHandler(this.listViewServerConnections_ItemDrag);
            this.listViewServerConnections.DragDrop += new System.Windows.Forms.DragEventHandler(this.listViewServerConnections_DragDrop);
            this.listViewServerConnections.DragEnter += new System.Windows.Forms.DragEventHandler(this.listViewServerConnections_DragEnter);
            // 
            // cmdRemoveServerUrl
            // 
            this.cmdRemoveServerUrl.Location = new System.Drawing.Point(405, 146);
            this.cmdRemoveServerUrl.Name = "cmdRemoveServerUrl";
            this.cmdRemoveServerUrl.Size = new System.Drawing.Size(115, 23);
            this.cmdRemoveServerUrl.TabIndex = 1;
            this.cmdRemoveServerUrl.Text = "_";
            this.cmdRemoveServerUrl.UseVisualStyleBackColor = true;
            this.cmdRemoveServerUrl.Click += new System.EventHandler(this.cmdRemoveServerUrl_Click);
            // 
            // frmConnectionSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(550, 348);
            this.Controls.Add(this.grpBoxConnectionTimeout);
            this.Controls.Add(this.grpBoxClearServerList);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOk);
            this.Controls.Add(this.grpBoxEvaluationTimeout);
            this.Controls.Add(this.grpBoxProxy);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmConnectionSettings";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "_";
            this.grpBoxProxy.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chkUseProxy.Properties)).EndInit();
            this.grpBoxEvaluationTimeout.ResumeLayout(false);
            this.grpBoxEvaluationTimeout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditEvalTimeout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.grpBoxClearServerList.ResumeLayout(false);
            this.grpBoxConnectionTimeout.ResumeLayout(false);
            this.grpBoxConnectionTimeout.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditConnectionTimeout.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditRetry.Properties)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpBoxProxy;
        private DevExpress.XtraEditors.CheckEdit chkUseProxy;
        private System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.Button cmdCancel;
        private System.Windows.Forms.Label lblTimeOut;
        private System.Windows.Forms.GroupBox grpBoxEvaluationTimeout;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.GroupBox grpBoxClearServerList;
        private System.Windows.Forms.Button cmdClearServerUrlList;
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.GroupBox grpBoxConnectionTimeout;
        private System.Windows.Forms.Label lblConnectionTimeout;
        private System.Windows.Forms.Label lblRetry;
        private DevExpress.XtraEditors.SpinEdit spinEditRetry;
        private DevExpress.XtraEditors.SpinEdit spinEditConnectionTimeout;
        private DevExpress.XtraEditors.SpinEdit spinEditEvalTimeout;
        private System.Windows.Forms.ListView listViewServerConnections;
        private System.Windows.Forms.Button cmdRemoveServerUrl;
    }
}