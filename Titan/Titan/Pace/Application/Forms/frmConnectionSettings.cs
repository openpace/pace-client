﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Titan.Pace.Application.Extensions;
using Titan.Properties;

namespace Titan.Pace.Application.Forms
{
    /// <remarks/>
    public partial class frmConnectionSettings : Form
    {
        #region Constructor & Control Load
        private readonly int _currentEvalTimeout;
        private readonly int _currentTimeout;
        private readonly int _currentRetry;
        private string _valueError;
        private string _urlStrings;
        private string _urlDefaultString;
        /// <remarks/>
        public frmConnectionSettings()
        {
            InitializeComponent();

            loadUi();

            _currentEvalTimeout = Settings.Default.ClientTimeout;
            _currentTimeout = Settings.Default.ServerURLTimeout;
            _currentRetry = Settings.Default.ServerURLRetryAttempts;

            chkUseProxy.Checked = Settings.Default.UseProxySettings;

            if (_currentEvalTimeout.Equals(-1))
            {
                spinEditEvalTimeout.EditValue = null;
            }
            else
            {
                spinEditEvalTimeout.EditValue = ConvertMillisecondsToMinutes(_currentEvalTimeout);
            }

            spinEditConnectionTimeout.EditValue = ConvertMillisecondsToSeconds(_currentTimeout);

            spinEditRetry.Value = _currentRetry;
        }

                /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.Caption");

                _valueError = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.Value.Error");

                //Get the labels from the localized .resx file.
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");

                grpBoxProxy.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.GroupBox.Proxy.Text");
                grpBoxEvaluationTimeout.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.GroupBox.EvaluationTimeout.Text");
                grpBoxConnectionTimeout.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.GroupBox.ConnectionTimeout.Text");
                grpBoxClearServerList.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.GroupBox.ServerList.Text");

                chkUseProxy.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.CheckBox.UseProxy.Text");

                lblConnectionTimeout.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.Label.TimeoutSec.Text");
                lblTimeOut.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.Label.Timeout.Text");
                lblRetry.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.Label.Retry.Text");

                cmdClearServerUrlList.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.Button.ClearServerList.Text");
                cmdRemoveServerUrl.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.Button.ServerRemove.Text");

                listViewServerConnections.FullRowSelect = true;
                listViewServerConnections.GridLines = true;
                listViewServerConnections.Scrollable = true;
                listViewServerConnections.Items.Clear();
                listViewServerConnections.View = View.Details;
                listViewServerConnections.HeaderStyle = ColumnHeaderStyle.None;
                listViewServerConnections.AllowDrop = true;
                listViewServerConnections.CheckBoxes = true;

                ColumnHeader header1 = new ColumnHeader();

                // Set the text, alignment and width for each column header.
                header1.Text = "Server Connection URL";
                header1.TextAlign = HorizontalAlignment.Left;
                header1.Width = listViewServerConnections.Width - 5;

                // Add the headers to the ListView control.
                listViewServerConnections.Columns.Add(header1);

                //Split the string of comma delimited url(s) and add it to the combo box.
                _urlStrings = Settings.Default.WebServiceUrlList;
                _urlDefaultString = Settings.Default.WebServiceUrlDefault;

                string[] temp =  Settings.Default.WebServiceUrlList.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                var items = listViewServerConnections.Items;
                foreach (string value in temp)
                {
                    ListViewItem item = new ListViewItem(value);
                    if (value == _urlDefaultString)
                    {
                        item.Checked = true;
                    }
                    items.Add(item);
                }

                //Set all the tool tips.
                toolTip.SetToolTip(cmdCancel,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Cancel")));

                toolTip.SetToolTip(cmdOk,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Ok")));

                toolTip.SetToolTip(chkUseProxy,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.CheckBox.UseProxy.Tooltip")));

                toolTip.SetToolTip(lblTimeOut,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Label.Timeout.Tooltip")));

                toolTip.SetToolTip(spinEditEvalTimeout,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Label.Timeout.Tooltip")));

                toolTip.SetToolTip(spinEditConnectionTimeout,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Label.ConnectionTimeout.Tooltip")));

                toolTip.SetToolTip(lblConnectionTimeout,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Label.ConnectionTimeout.Tooltip")));

                toolTip.SetToolTip(spinEditRetry,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Label.Retry.Tooltip")));

                toolTip.SetToolTip(lblRetry,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Label.Retry.Tooltip")));

                toolTip.SetToolTip(cmdClearServerUrlList,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Button.ClearServerList.Tooltip")));

                toolTip.SetToolTip(cmdRemoveServerUrl,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.Button.RemoveServer.Tooltip")));

                toolTip.SetToolTip(listViewServerConnections,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ConnectionSettings.ListView.ServerList.Tooltip")));

                //toolTip.SetToolTip(chkShowPageMembers,
                //    String.Format(PafApp.GetLocalization().GetResourceManager().
                //    GetString("Application.Controls.ToolTips.Options.ShowPageMembers")));

                //toolTip.SetToolTip(txtLanguage,
                //    String.Format(PafApp.GetLocalization().GetResourceManager().
                //    GetString("Application.Controls.ToolTips.Options.Language")));
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }
        #endregion Constructor & Control Load


        private decimal ConvertMillisecondsToMinutes(int milliseconds)
        {
            double d = TimeSpan.FromMilliseconds(milliseconds).TotalMinutes;
            return Convert.ToDecimal(d);
        }

        private decimal ConvertMillisecondsToSeconds(int milliseconds)
        {
            double d = TimeSpan.FromMilliseconds(milliseconds).TotalSeconds;
            return Convert.ToDecimal(d);
        }

        private void cmdOk_Click(object sender, EventArgs e)
        {
            int numericValue = -1;
            if (spinEditEvalTimeout.EditValue != null && !String.IsNullOrWhiteSpace(spinEditEvalTimeout.EditValue.ToString()))
            {
                numericValue = Convert.ToInt32(spinEditEvalTimeout.EditValue);
            }
            int numericValue2 = Convert.ToInt32(spinEditConnectionTimeout.EditValue);
            double evalTimeout = TimeSpan.FromMinutes(Convert.ToDouble(numericValue)).TotalMilliseconds;
            double timeout = TimeSpan.FromSeconds(Convert.ToDouble(numericValue2)).TotalMilliseconds;

            Settings.Default.ClientTimeout = Convert.ToInt32(evalTimeout);

            if (!numericValue2.Equals(0))
            {
                Settings.Default.ServerURLTimeout = Convert.ToInt32(timeout);
            }

            Settings.Default.ServerURLRetryAttempts = Convert.ToInt32(spinEditRetry.Value);

            Settings.Default.UseProxySettings = chkUseProxy.Checked;
            Settings.Default.WebServiceUrlList = _urlStrings;
            Settings.Default.WebServiceUrlDefault = _urlDefaultString;
            Settings.Default.Save();
            Settings.Default.Reload();

        }

        private void cmdClearServerUrlList_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.ClearServer.Warning"), 
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"), 
                MessageBoxButtons.OKCancel, 
                MessageBoxIcon.Information, 
                MessageBoxDefaultButton.Button2) == DialogResult.OK)
            {
                _urlStrings = String.Empty;
                _urlDefaultString = String.Empty;
                listViewServerConnections.Items.Clear();
            }
        }

        private void spinEditConnectionTimeout_Validating(object sender, System.ComponentModel.CancelEventArgs e)
        {
            object evalTimeout = spinEditConnectionTimeout.EditValue;
            int result;
            if (!evalTimeout.IsInteger(out result) || result == 0)
            {
                errorProvider1.SetError(spinEditConnectionTimeout, _valueError);
                cmdOk.Enabled = false;
            }
            else
            {
                errorProvider1.Clear();
                cmdOk.Enabled = true;
            }
        }

        private void spinEditConnectionTimeout_ValueChanged(object sender, EventArgs e)
        {
            lblConnectionTimeout.Focus();
            spinEditConnectionTimeout.Focus();
        }

        private void spinEditEvalTimeout_EditValueChanging(object sender, DevExpress.XtraEditors.Controls.ChangingEventArgs e)
        {
            if (e.NewValue == null) return;
            if (String.IsNullOrWhiteSpace(e.NewValue.ToString())) return;
            int newValue = Convert.ToInt32(e.NewValue);
            if (newValue == 0 || newValue < -1)
            {
                e.Cancel = true;
            }
        }

        private void cmdRemoveServerUrl_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ConnectionSettings.RemoveServer.Warning"),
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Information,
                MessageBoxDefaultButton.Button2) == DialogResult.OK)
            {
                List<string> urls = new List<string>(_urlStrings.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries));
                Boolean found = false;
                foreach (ListViewItem item in listViewServerConnections.SelectedItems)
                {
                    if (found)
                        break;
                    foreach (string url in urls)
                    {
                        if (url == item.Text)
                        {
                            urls.Remove(item.Text);
                            _urlDefaultString = _urlDefaultString.Replace(item.Text, "");

                            listViewServerConnections.Items.Remove(item);

                            found = true;
                            break;
                        }
                    }
                }

                _urlStrings = "," + string.Join(",", urls.ToArray());

                if (_urlDefaultString == String.Empty && listViewServerConnections.Items.Count > 0)
                {
                    ListViewItem temp = listViewServerConnections.Items[0];
                    _urlDefaultString = temp.Text;
                    temp.Checked = true;
                }
            }
        }

        private void listViewServerConnections_ItemDrag(object sender, ItemDragEventArgs e)
        {
            listViewServerConnections.DoDragDrop(listViewServerConnections.SelectedItems, DragDropEffects.Move);
        }

        private void listViewServerConnections_DragEnter(object sender, DragEventArgs e)
        {
            int len = e.Data.GetFormats().Length - 1;
            int i;
            for (i = 0; i <= len; i++)
            {
                if (e.Data.GetFormats()[i].Equals("System.Windows.Forms.ListView+SelectedListViewItemCollection"))
                {
                    //The data from the drag source is moved to the target.	
                    e.Effect = DragDropEffects.Move;
                }
            }
        }

        private void listViewServerConnections_DragDrop(object sender, DragEventArgs e)
        {
            //Return if the items are not selected in the ListView control.
            if (listViewServerConnections.SelectedItems.Count == 0)
            {
                return;
            }
            //Returns the location of the mouse pointer in the ListView control.
            Point cp = listViewServerConnections.PointToClient(new Point(e.X, e.Y));
            //Obtain the item that is located at the specified location of the mouse pointer.
            ListViewItem dragToItem = listViewServerConnections.GetItemAt(cp.X, cp.Y);
            if (dragToItem == null)
            {
                return;
            }
            //Obtain the index of the item at the mouse pointer.
            int dragIndex = dragToItem.Index;
            ListViewItem[] sel = new ListViewItem[listViewServerConnections.SelectedItems.Count];
            for (int i = 0; i <= listViewServerConnections.SelectedItems.Count - 1; i++)
            {
                sel[i] = listViewServerConnections.SelectedItems[i];
            }
            for (int i = 0; i < sel.GetLength(0); i++)
            {
                //Obtain the ListViewItem to be dragged to the target location.
                ListViewItem dragItem = sel[i];
                int itemIndex = dragIndex;
                if (itemIndex == dragItem.Index)
                {
                    return;
                }
                if (dragItem.Index < itemIndex)
                    itemIndex++;
                else
                    itemIndex = dragIndex + i;
                //Insert the item at the mouse pointer.
                ListViewItem insertItem = (ListViewItem)dragItem.Clone();
                listViewServerConnections.Items.Insert(itemIndex, insertItem);
                //Removes the item from the initial location while 
                //the item is moved to the new location.
                listViewServerConnections.Items.Remove(dragItem);
            }

            _urlStrings = ",";
            foreach (ListViewItem lvi in listViewServerConnections.Items)
            {
                _urlStrings = _urlStrings + lvi.Text + ",";
            }
        }

        private void listViewServerConnections_ItemChecked(object sender, ItemCheckedEventArgs e)
        {
            if (e.Item.Checked)
            {
                foreach (ListViewItem item in listViewServerConnections.Items)
                {
                    if (item != e.Item)
                    {
                        item.Checked = false;
                    }
                }
                _urlDefaultString = e.Item.Text;
            }
        }
    }
}
