#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Drawing;
using System.Windows.Forms;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmChangePassword : Form
    {
        
        private string _username = null;

        private string _oldPassword = null;

        private int? _minNewPasswordLenth = null;

        private int? _maxNewPasswordLenth = null;

        public frmChangePassword()
        {
            InitializeComponent();
            Graphics graphics = CreateGraphics();
            float dpiX = graphics.DpiX;
            float dpiY = graphics.DpiY;
            if (dpiX >= 144 && dpiY >= 144)
            {
                ClientSize = new Size(500, 375);
            }
            else if (dpiX >= 120 && dpiY >= 120)
            {
                ClientSize = new Size(400, 285);
            }
            else
            {
                ClientSize = new Size(370, 234);
            }

        }
        
        public frmChangePassword(string username) : this()
        {
            _username = username;
        }

        public frmChangePassword(string username, string oldPassword) : this(username)
        {
            _oldPassword = oldPassword;
        }

        public frmChangePassword(string username, string oldPassword, int? minNewPasswordLength, int? maxNewPasswordLength) : this(username, oldPassword)
        {
            
            _minNewPasswordLenth = minNewPasswordLength;
            _maxNewPasswordLenth = maxNewPasswordLength;
        }

        private void _changePassword_Load(object sender, EventArgs e)
        {

            _changePassword.Username = _username;
            _changePassword.OldPassword = _oldPassword;
            _changePassword.MinNewPasswordLength = _minNewPasswordLenth;
            _changePassword.MaxNewPasswordLength = _maxNewPasswordLenth;
            Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmChangePassword.Caption");


            
        }
        
    }
}
