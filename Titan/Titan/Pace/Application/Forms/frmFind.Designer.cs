﻿namespace Titan.Pace.Application.Forms
{
    partial class frmFind
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdCancel = new System.Windows.Forms.Button();
            this.cmdOk = new System.Windows.Forms.Button();
            this.lblSearchTxt = new System.Windows.Forms.Label();
            this.txtSearchText = new System.Windows.Forms.TextBox();
            this.chkMatchCase = new System.Windows.Forms.CheckBox();
            this.chkAutoCheckMember = new System.Windows.Forms.CheckBox();
            this.chkSearchAliasTables = new System.Windows.Forms.CheckBox();
            this.chkMatchWholeWord = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // cmdCancel
            // 
            this.cmdCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cmdCancel.Location = new System.Drawing.Point(273, 103);
            this.cmdCancel.Name = "cmdCancel";
            this.cmdCancel.Size = new System.Drawing.Size(75, 23);
            this.cmdCancel.TabIndex = 5;
            this.cmdCancel.UseVisualStyleBackColor = true;
            // 
            // cmdOk
            // 
            this.cmdOk.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.cmdOk.Location = new System.Drawing.Point(192, 103);
            this.cmdOk.Name = "cmdOk";
            this.cmdOk.Size = new System.Drawing.Size(75, 23);
            this.cmdOk.TabIndex = 4;
            this.cmdOk.UseVisualStyleBackColor = true;
            this.cmdOk.Click += new System.EventHandler(this.cmdOk_Click);
            // 
            // lblSearchTxt
            // 
            this.lblSearchTxt.AutoSize = true;
            this.lblSearchTxt.Location = new System.Drawing.Point(2, 6);
            this.lblSearchTxt.Name = "lblSearchTxt";
            this.lblSearchTxt.Size = new System.Drawing.Size(13, 13);
            this.lblSearchTxt.TabIndex = 7;
            this.lblSearchTxt.Text = "_";
            // 
            // txtSearchText
            // 
            this.txtSearchText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSearchText.Location = new System.Drawing.Point(5, 22);
            this.txtSearchText.Name = "txtSearchText";
            this.txtSearchText.Size = new System.Drawing.Size(343, 20);
            this.txtSearchText.TabIndex = 1;
            this.txtSearchText.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtSearchText_KeyDown);
            // 
            // chkMatchCase
            // 
            this.chkMatchCase.AutoSize = true;
            this.chkMatchCase.Location = new System.Drawing.Point(5, 48);
            this.chkMatchCase.Name = "chkMatchCase";
            this.chkMatchCase.Size = new System.Drawing.Size(32, 17);
            this.chkMatchCase.TabIndex = 2;
            this.chkMatchCase.Text = "_";
            this.chkMatchCase.UseVisualStyleBackColor = true;
            this.chkMatchCase.CheckedChanged += new System.EventHandler(this.chkMatchCase_CheckedChanged);
            // 
            // chkAutoCheckMember
            // 
            this.chkAutoCheckMember.AutoSize = true;
            this.chkAutoCheckMember.Checked = true;
            this.chkAutoCheckMember.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoCheckMember.Location = new System.Drawing.Point(5, 68);
            this.chkAutoCheckMember.Name = "chkAutoCheckMember";
            this.chkAutoCheckMember.Size = new System.Drawing.Size(32, 17);
            this.chkAutoCheckMember.TabIndex = 3;
            this.chkAutoCheckMember.Text = "_";
            this.chkAutoCheckMember.UseVisualStyleBackColor = true;
            this.chkAutoCheckMember.CheckedChanged += new System.EventHandler(this.chkAutoCheckMember_CheckedChanged);
            // 
            // chkSearchAliasTables
            // 
            this.chkSearchAliasTables.AutoSize = true;
            this.chkSearchAliasTables.Location = new System.Drawing.Point(5, 89);
            this.chkSearchAliasTables.Name = "chkSearchAliasTables";
            this.chkSearchAliasTables.Size = new System.Drawing.Size(32, 17);
            this.chkSearchAliasTables.TabIndex = 8;
            this.chkSearchAliasTables.Text = "_";
            this.chkSearchAliasTables.UseVisualStyleBackColor = true;
            this.chkSearchAliasTables.CheckedChanged += new System.EventHandler(this.chkSearchAliasTables_CheckedChanged);
            // 
            // chkMatchWholeWord
            // 
            this.chkMatchWholeWord.AutoSize = true;
            this.chkMatchWholeWord.Location = new System.Drawing.Point(5, 109);
            this.chkMatchWholeWord.Name = "chkMatchWholeWord";
            this.chkMatchWholeWord.Size = new System.Drawing.Size(113, 17);
            this.chkMatchWholeWord.TabIndex = 9;
            this.chkMatchWholeWord.Text = "Match whole word";
            this.chkMatchWholeWord.UseVisualStyleBackColor = true;
            this.chkMatchWholeWord.CheckedChanged += new System.EventHandler(this.chkMatchWholeWord_CheckedChanged);
            // 
            // frmFind
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 129);
            this.Controls.Add(this.chkMatchWholeWord);
            this.Controls.Add(this.chkSearchAliasTables);
            this.Controls.Add(this.chkAutoCheckMember);
            this.Controls.Add(this.chkMatchCase);
            this.Controls.Add(this.txtSearchText);
            this.Controls.Add(this.lblSearchTxt);
            this.Controls.Add(this.cmdCancel);
            this.Controls.Add(this.cmdOk);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFind";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "_";
            this.Load += new System.EventHandler(this.frmFind_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.Button cmdCancel;
        internal System.Windows.Forms.Button cmdOk;
        private System.Windows.Forms.Label lblSearchTxt;
        private System.Windows.Forms.TextBox txtSearchText;
        private System.Windows.Forms.CheckBox chkMatchCase;
        private System.Windows.Forms.CheckBox chkAutoCheckMember;
        private System.Windows.Forms.CheckBox chkSearchAliasTables;
        private System.Windows.Forms.CheckBox chkMatchWholeWord;
    }
}