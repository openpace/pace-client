#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.Skins;
using Titan.Properties;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmOptions : Form
    {
        private string _CurrentGrid = "";
        private const int PAD = 5;

        #region Constructor & Control Load
        public frmOptions()
        {
            InitializeComponent();
            loadUi();
            //            this.ClientSize = new System.Drawing.Size(337, 452);
            Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmOptions.Caption");
            Icon = Resources.PIcon;
        }
        /// <summary>
        /// Gets/sets the name of the current grid(sheet).
        /// </summary>
        public string CurrentGrid
        {
            get { return _CurrentGrid; }
            set { _CurrentGrid = value; }
        }


        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                //Get the labels from the localized .resx file.
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                 
                tabControl1.TabPages[0].Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.UserSettingsGroupTitle");
                tabControl1.TabPages[1].Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.ColorSettingsGroupTitle");

                chkShowPageMembers.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.ShowPageMembers");
                chkShowPageMembers.Checked = Settings.Default.ShowPageMembers;

                chkShowFreezePane.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.AutoFreezePane");
                chkShowFreezePane.Checked = Settings.Default.AutoFreezePane;

                chkAutoPrintRange.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.AutoPrintRange");
                chkAutoPrintRange.Checked = Settings.Default.AutoPrintRange;

                chkAutoColumnHeading.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.AutoColumnHeadings");
                chkAutoColumnHeading.Checked = Settings.Default.AutoColumnHeadings;

                chkBOH.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.BOHToggle");
                chkBOH.Checked = Settings.Default.BOHProtection;

                chkAutoFitColumHeaderRowHeight.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Controls.Options.AutoFitColumHeaderRowHeight");
                chkAutoFitColumHeaderRowHeight.Checked = Settings.Default.AutoFitColumnHeaderRowHeight;


                chkAutoFitHeaderRows.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Controls.Options.AutoFitHeaderRows");
                chkAutoFitHeaderRows.Checked = Settings.Default.AutoFitColumnHeaderRowHeight;

                lblLanguage.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.LanguageLabel");

                lblUserLockColor.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.UserLockColor");
                txtUserLockColor.BackColor = PafApp.GetPafAppSettingsHelper().GetUserLockColor();

                lblProtectedColor.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.ProtectedColor");
                txtProtectedColor.BackColor = PafApp.GetPafAppSettingsHelper().GetProtectedColor();

                lblSystemLockColor.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.SystemLockColor");
                txtSystemLockColor.BackColor = PafApp.GetPafAppSettingsHelper().GetSystemLockColor();

                lblNonPlannableProtectedColor.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.NonPlannableProtectedColor");
                txtNonPlannableProtectedColor.BackColor =
                    PafApp.GetPafAppSettingsHelper().GetNonPlannableProtectedColor();

                lblForwardPlannableProtectedColor.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.ForwardPlannableProtectedColor");
                txtForwardPlannableProtectedColor.BackColor =
                    PafApp.GetPafAppSettingsHelper().GetForwardPlannableProtectedColor();

                lblMemberTagNoteColor.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.MemberTagModifiedColor");
                txtMemberTagNoteColor.BackColor = PafApp.GetPafAppSettingsHelper().GetMemberTagChangedColor();

                string languagePacks = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.LanguagePackComboBox.Items");
                string[] languagePacksArray = languagePacks.Split(',');

                groupBoxFormattingSettings.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Controls.Options.GroupBox.UiSettings.Text");

                groupBoxProgramSettings.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Controls.Options.GroupBox.Settings.Text");

                cboLanguage.Items.AddRange(languagePacksArray);
                int index = cboLanguage.Items.IndexOf(Settings.Default.Language);
                cboLanguage.CausesValidation = false;
                cboLanguage.BeginUpdate();
                cboLanguage.SelectedIndex = index;
                cboLanguage.EndUpdate();
                cboLanguage.CausesValidation = true;

                //Set all the tool tips.
                toolTip.SetToolTip(cmdCancel,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Cancel")));

                toolTip.SetToolTip(cmdOk,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Ok")));

                toolTip.SetToolTip(txtForwardPlannableProtectedColor,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.Color"), new string[] {
                        PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Controls.Options.ForwardPlannableProtectedColor")}));

                toolTip.SetToolTip(txtNonPlannableProtectedColor,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.Color"), new string[] {
                        PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Controls.Options.NonPlannableProtectedColor")}));

                toolTip.SetToolTip(txtProtectedColor,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.Color"), new string[] {
                        PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Controls.Options.ProtectedColor")}));

                toolTip.SetToolTip(txtSystemLockColor,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.Color"), new string[] {
                        PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Controls.Options.SystemLockColor")}));

                toolTip.SetToolTip(txtUserLockColor,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.Color"), new string[] {
                        PafApp.GetLocalization().GetResourceManager().
                        GetString("Application.Controls.Options.UserLockColor")}));

                toolTip.SetToolTip(chkBOH,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.BOHProtection")));

                toolTip.SetToolTip(chkAutoColumnHeading,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.RowColumnHeadings")));

                toolTip.SetToolTip(chkAutoFitColumHeaderRowHeight,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.AutoFitColumnHeadings")));


                toolTip.SetToolTip(chkAutoFitHeaderRows,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.AutoFitHeaderRows")));

                toolTip.SetToolTip(chkAutoPrintRange,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.AutoPrintRange")));

                toolTip.SetToolTip(chkShowFreezePane,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.SetFreezePane")));

                toolTip.SetToolTip(chkShowPageMembers,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.ShowPageMembers")));

                toolTip.SetToolTip(cboLanguage,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Options.Language")));

                int defaultIndex = 0;
                foreach (SkinContainer cnt in SkinManager.Default.Skins)
                {
                    int idx = comboBoxSkin.Items.Add(cnt.SkinName);
                    if (UserLookAndFeel.Default.SkinName == cnt.SkinName)
                    {
                        defaultIndex = idx;
                    }
                }
                comboBoxSkin.SelectedIndex = defaultIndex;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }
        #endregion Constructor & Control Load

        #region Handlers

        /// <summary>
        /// Event handler for the "OK" button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (!chkShowFreezePane.Checked && Settings.Default.AutoFreezePane)
            {
                DialogResult dr = MessageBox.Show(this,
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.ClearExistingFreezePane"),
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                if (dr == DialogResult.Yes)
                    PafApp.GetGridApp().ClearExistingFreezePanes();
            }
            try
            {
                if (!chkAutoPrintRange.Checked && Settings.Default.AutoPrintRange)
                {
                    DialogResult dr = MessageBox.Show(this,
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.Options.ClearExistingPrintAreas"),
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2);

                    if (dr == DialogResult.Yes)
                        PafApp.GetViewMngr().CurrentGrid.ClearPrintRange();
                }
            }
            catch { }
            Settings.Default.ShowPageMembers = chkShowPageMembers.Checked;
            Settings.Default.AutoFreezePane = chkShowFreezePane.Checked;
            Settings.Default.AutoPrintRange = chkAutoPrintRange.Checked;
            Settings.Default.AutoFitColumnHeaderRowHeight = chkAutoFitColumHeaderRowHeight.Checked;
            Settings.Default.AutoFitHeaderRows = chkAutoFitHeaderRows.Checked;

            //show/hide the column headings
            Settings.Default.AutoColumnHeadings = chkAutoColumnHeading.Checked;

            Settings.Default.AutoFitColumnHeaderRowHeight = chkAutoFitColumHeaderRowHeight.Checked;

            if (PafApp.GetViewMngr().Grids != null && PafApp.GetViewMngr().Grids.Count > 0 && PafApp.GetViewMngr().CurrentGrid != null)
            {
                PafApp.GetViewMngr().CurrentGrid.SetColumnHeader(chkAutoColumnHeading.Checked);
            }

            if (groupBoxTheme.Visible)
            {
                Settings.Default.Theme = comboBoxSkin.SelectedItem.ToString();
                UserLookAndFeel.Default.SkinName = SkinManager.Default.GetValidSkinName(Settings.Default.Theme);
            }

            Settings.Default.BOHProtection = chkBOH.Checked;
            Settings.Default.Language = cboLanguage.Text;
            Settings.Default.Save();
            Settings.Default.Reload();
        }

        /// <summary>
        /// Event handler for the form resize.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Options_Resize(object sender, EventArgs e)
        {
            tabControl1.Height = Height - cmdCancel.Height - tabControl1.Top - PAD;
        }
        #endregion Handlers

        private void txtSystemLockColor_DoubleClick(object sender, EventArgs e)
        {
            groupBoxTheme.Visible = !groupBoxTheme.Visible;
        }
    }
}