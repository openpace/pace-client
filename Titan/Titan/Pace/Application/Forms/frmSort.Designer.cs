using Titan.Pace.Application.Controls;

namespace Titan.Pace.Application.Forms
{
    partial class frmSort
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sort1 = new Sort();
            this.SuspendLayout();
            // 
            // sort1
            // 
            this.sort1.BackColor = System.Drawing.SystemColors.Control;
            this.sort1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sort1.Location = new System.Drawing.Point(0, 0);
            this.sort1.Margin = new System.Windows.Forms.Padding(4);
            this.sort1.Name = "sort1";
            this.sort1.Size = new System.Drawing.Size(385, 333);
            this.sort1.TabIndex = 0;
            // 
            // frmSort
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(385, 333);
            this.Controls.Add(this.sort1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSort";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Load += new System.EventHandler(this.frmSort_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private Sort sort1;









    }
}