using System.Threading;
using Titan.Pace.Application.Controls;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmPasswordReset
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this._passwordReset = new PasswordReset();
            this.SuspendLayout();
            // 
            // _passwordReset
            // 
            this._passwordReset.Dock = System.Windows.Forms.DockStyle.Fill;
            this._passwordReset.Location = new System.Drawing.Point(0, 0);
            this._passwordReset.Margin = new System.Windows.Forms.Padding(4);
            this._passwordReset.Name = "_passwordReset";
            this._passwordReset.Size = new System.Drawing.Size(292, 155);
            this._passwordReset.TabIndex = 0;
            this._passwordReset.Username = null;
            // 
            // frmPasswordReset
            // 
            this.ClientSize = new System.Drawing.Size(292, 155);
            this.Controls.Add(this._passwordReset);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmPasswordReset";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            
            this.Load += new System.EventHandler(this.frmPasswordReset_Load);
            this.ResumeLayout(false);

            //Get the labels from the localized .resx file.
            Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
            this.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmPasswordReset.Caption");

        }

        #endregion

        internal PasswordReset _passwordReset;
               
    }
}