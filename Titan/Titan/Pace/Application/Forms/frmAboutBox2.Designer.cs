namespace Titan.Pace.Application.Forms
{
    partial class frmAboutBox2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelProductName = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.labelCompanyName = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.labelServerVersion = new System.Windows.Forms.Label();
            this.labelClientVersion = new System.Windows.Forms.Label();
            this.labelExcelVersion = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.labelSupportPhone = new System.Windows.Forms.Label();
            this.groupBoxSupport = new System.Windows.Forms.GroupBox();
            this.groupBoxInformation = new System.Windows.Forms.GroupBox();
            this.groupBoxSupport.SuspendLayout();
            this.groupBoxInformation.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelProductName
            // 
            this.labelProductName.AutoSize = true;
            this.labelProductName.BackColor = System.Drawing.Color.Transparent;
            this.labelProductName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelProductName.ForeColor = System.Drawing.Color.White;
            this.labelProductName.Location = new System.Drawing.Point(189, 14);
            this.labelProductName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelProductName.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelProductName.Name = "labelProductName";
            this.labelProductName.Size = new System.Drawing.Size(94, 16);
            this.labelProductName.TabIndex = 19;
            this.labelProductName.Text = "Product Name";
            this.labelProductName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelProductName.UseMnemonic = false;
            this.labelProductName.Visible = false;
            this.labelProductName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelProductName.Click += new System.EventHandler(this.label_Click);
            // 
            // labelVersion
            // 
            this.labelVersion.AutoSize = true;
            this.labelVersion.BackColor = System.Drawing.Color.Transparent;
            this.labelVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelVersion.ForeColor = System.Drawing.Color.White;
            this.labelVersion.Location = new System.Drawing.Point(189, 41);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(54, 16);
            this.labelVersion.TabIndex = 0;
            this.labelVersion.Text = "Version";
            this.labelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelVersion.UseMnemonic = false;
            this.labelVersion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelVersion.Click += new System.EventHandler(this.label_Click);
            // 
            // labelCopyright
            // 
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.BackColor = System.Drawing.Color.Transparent;
            this.labelCopyright.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCopyright.ForeColor = System.Drawing.Color.White;
            this.labelCopyright.Location = new System.Drawing.Point(189, 97);
            this.labelCopyright.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelCopyright.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(65, 16);
            this.labelCopyright.TabIndex = 21;
            this.labelCopyright.Text = "Copyright";
            this.labelCopyright.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCopyright.UseMnemonic = false;
            this.labelCopyright.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelCopyright.Click += new System.EventHandler(this.label_Click);
            // 
            // labelCompanyName
            // 
            this.labelCompanyName.AutoSize = true;
            this.labelCompanyName.BackColor = System.Drawing.Color.Transparent;
            this.labelCompanyName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCompanyName.ForeColor = System.Drawing.Color.White;
            this.labelCompanyName.Location = new System.Drawing.Point(189, 70);
            this.labelCompanyName.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelCompanyName.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelCompanyName.Name = "labelCompanyName";
            this.labelCompanyName.Size = new System.Drawing.Size(106, 16);
            this.labelCompanyName.TabIndex = 22;
            this.labelCompanyName.Text = "Company Name";
            this.labelCompanyName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelCompanyName.UseMnemonic = false;
            this.labelCompanyName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelCompanyName.Click += new System.EventHandler(this.label_Click);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.BackColor = System.Drawing.Color.Transparent;
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.okButton.Location = new System.Drawing.Point(195, 55);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(1, 1);
            this.okButton.TabIndex = 0;
            this.okButton.Text = "&OK";
            this.okButton.UseVisualStyleBackColor = false;
            // 
            // labelServerVersion
            // 
            this.labelServerVersion.AutoSize = true;
            this.labelServerVersion.BackColor = System.Drawing.Color.Transparent;
            this.labelServerVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelServerVersion.ForeColor = System.Drawing.Color.White;
            this.labelServerVersion.Location = new System.Drawing.Point(9, 25);
            this.labelServerVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelServerVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelServerVersion.Name = "labelServerVersion";
            this.labelServerVersion.Size = new System.Drawing.Size(97, 16);
            this.labelServerVersion.TabIndex = 24;
            this.labelServerVersion.Text = "Server Version";
            this.labelServerVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelServerVersion.UseMnemonic = false;
            this.labelServerVersion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelServerVersion.Click += new System.EventHandler(this.label_Click);
            // 
            // labelClientVersion
            // 
            this.labelClientVersion.AutoSize = true;
            this.labelClientVersion.BackColor = System.Drawing.Color.Transparent;
            this.labelClientVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelClientVersion.ForeColor = System.Drawing.Color.White;
            this.labelClientVersion.Location = new System.Drawing.Point(9, 55);
            this.labelClientVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelClientVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelClientVersion.Name = "labelClientVersion";
            this.labelClientVersion.Size = new System.Drawing.Size(90, 16);
            this.labelClientVersion.TabIndex = 25;
            this.labelClientVersion.Text = "Client Version";
            this.labelClientVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelClientVersion.UseMnemonic = false;
            this.labelClientVersion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelClientVersion.Click += new System.EventHandler(this.label_Click);
            // 
            // labelExcelVersion
            // 
            this.labelExcelVersion.AutoSize = true;
            this.labelExcelVersion.BackColor = System.Drawing.Color.Transparent;
            this.labelExcelVersion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelExcelVersion.ForeColor = System.Drawing.Color.White;
            this.labelExcelVersion.Location = new System.Drawing.Point(9, 82);
            this.labelExcelVersion.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelExcelVersion.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelExcelVersion.Name = "labelExcelVersion";
            this.labelExcelVersion.Size = new System.Drawing.Size(90, 16);
            this.labelExcelVersion.TabIndex = 26;
            this.labelExcelVersion.Text = "Excel Version";
            this.labelExcelVersion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelExcelVersion.UseMnemonic = false;
            this.labelExcelVersion.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelExcelVersion.Click += new System.EventHandler(this.label_Click);
            // 
            // linkLabel1
            // 
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.LinkColor = System.Drawing.Color.White;
            this.linkLabel1.Location = new System.Drawing.Point(6, 25);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(69, 16);
            this.linkLabel1.TabIndex = 27;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "linkLabel1";
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // labelSupportPhone
            // 
            this.labelSupportPhone.AutoSize = true;
            this.labelSupportPhone.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelSupportPhone.ForeColor = System.Drawing.Color.White;
            this.labelSupportPhone.Location = new System.Drawing.Point(6, 56);
            this.labelSupportPhone.Margin = new System.Windows.Forms.Padding(6, 0, 3, 0);
            this.labelSupportPhone.MaximumSize = new System.Drawing.Size(0, 17);
            this.labelSupportPhone.Name = "labelSupportPhone";
            this.labelSupportPhone.Size = new System.Drawing.Size(0, 16);
            this.labelSupportPhone.TabIndex = 29;
            this.labelSupportPhone.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.labelSupportPhone.UseMnemonic = false;
            this.labelSupportPhone.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            this.labelSupportPhone.Click += new System.EventHandler(this.label_Click);
            // 
            // groupBoxSupport
            // 
            this.groupBoxSupport.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxSupport.Controls.Add(this.linkLabel1);
            this.groupBoxSupport.Controls.Add(this.labelSupportPhone);
            this.groupBoxSupport.ForeColor = System.Drawing.Color.White;
            this.groupBoxSupport.Location = new System.Drawing.Point(192, 284);
            this.groupBoxSupport.Name = "groupBoxSupport";
            this.groupBoxSupport.Size = new System.Drawing.Size(374, 69);
            this.groupBoxSupport.TabIndex = 30;
            this.groupBoxSupport.TabStop = false;
            this.groupBoxSupport.Text = "groupBox1";
            this.groupBoxSupport.Click += new System.EventHandler(this.label_Click);
            this.groupBoxSupport.KeyDown += new System.Windows.Forms.KeyEventHandler(this.label_KeyDown);
            // 
            // groupBoxInformation
            // 
            this.groupBoxInformation.BackColor = System.Drawing.Color.Transparent;
            this.groupBoxInformation.Controls.Add(this.labelServerVersion);
            this.groupBoxInformation.Controls.Add(this.labelClientVersion);
            this.groupBoxInformation.Controls.Add(this.labelExcelVersion);
            this.groupBoxInformation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBoxInformation.ForeColor = System.Drawing.Color.White;
            this.groupBoxInformation.Location = new System.Drawing.Point(192, 141);
            this.groupBoxInformation.Name = "groupBoxInformation";
            this.groupBoxInformation.Size = new System.Drawing.Size(374, 110);
            this.groupBoxInformation.TabIndex = 31;
            this.groupBoxInformation.TabStop = false;
            this.groupBoxInformation.Text = "groupBox1";
            this.groupBoxInformation.Click += new System.EventHandler(this.frmAboutBox2_Click);
            // 
            // frmAboutBox2
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Inherit;
            this.BackColor = System.Drawing.Color.White;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelButton = this.okButton;
            this.ClientSize = new System.Drawing.Size(600, 412);
            this.Controls.Add(this.groupBoxInformation);
            this.Controls.Add(this.groupBoxSupport);
            this.Controls.Add(this.labelProductName);
            this.Controls.Add(this.labelVersion);
            this.Controls.Add(this.labelCopyright);
            this.Controls.Add(this.labelCompanyName);
            this.Controls.Add(this.okButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmAboutBox2";
            this.Padding = new System.Windows.Forms.Padding(9);
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Click += new System.EventHandler(this.frmAboutBox2_Click);
            this.groupBoxSupport.ResumeLayout(false);
            this.groupBoxSupport.PerformLayout();
            this.groupBoxInformation.ResumeLayout(false);
            this.groupBoxInformation.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

       

        #endregion

        private System.Windows.Forms.Label labelProductName;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelCompanyName;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Label labelServerVersion;
        private System.Windows.Forms.Label labelClientVersion;
        private System.Windows.Forms.Label labelExcelVersion;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Label labelSupportPhone;
        private System.Windows.Forms.GroupBox groupBoxSupport;
        private System.Windows.Forms.GroupBox groupBoxInformation;
    }
}
