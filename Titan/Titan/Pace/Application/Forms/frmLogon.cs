#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Windows.Forms;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Exceptions;
using Titan.Pace.Application.Utilities;
using Titan.Pace.DataStructures;
using Titan.PafService;
using Titan.Properties;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmLogon : Form
    {
        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, Int32 wMsg, bool wParam, Int32 lParam);

        private const int WM_SETREDRAW = 11;

        private bool _isLoading = false;

        #region Variables
        private string _userid = "";
        private string _password = "";
        private string _selectedUrl = "";
        private bool _logonValid = false;
        private bool _initialLogin = true;
        private bool _clientUpgradeRequired = false;
        private bool _IsDomainUser = false;
        private const int PAD = 7;
        private Bitmap backgroundImage = null;

        public event UserLogon_ServerChanged LogonInfoChanged;
        public delegate void UserLogon_ServerChanged(object sender, LogonEventArgs e, ref bool Cancel);

        public event RecievedUserAuthenication UserAuthenicated;
        public delegate void RecievedUserAuthenication(object sender, string userId);
        #endregion Variables

        #region Constructor & Control Load
        public frmLogon()
        {
            InitializeComponent();
            loadUi();
        }        

        /// <summary>
        /// Event handler for the form closing event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogon_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this.DialogResult != DialogResult.Cancel && !this.isUserAuthenaticated
                && !this.ClientUpgradeRequired)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Event handler for the form load event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogon_Load(object sender, EventArgs e)
        {

            Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Forms.frmLogon.Caption");
            Icon = (Icon) Resources.PIcon;

            backgroundImage = Resources.client_login;
            Color backColor = backgroundImage.GetPixel(1, 1);
            backgroundImage.MakeTransparent();

            this.SuspendDrawing();

            //Seed the userid/password boxes, if properties are set.);
            if (!String.IsNullOrEmpty(_userid))
                txtUserid.Text = _userid;

            if (!String.IsNullOrEmpty(_password))
                txtPassword.Text = _password;

            //Make sure its not null
            if (!String.IsNullOrEmpty(Settings.Default.WebServiceUrlList) && cboServer.Items != null && cboServer.Items.Count == 0)
            {
                //Make sure the box is clear.
                cboServer.ResetText();
                cboServer.Items.Clear();

                //Split the string of comma delimited url(s) and add it to the combo box.
                cboServer.Items.AddRange(Settings.Default.WebServiceUrlList.Split(",".ToCharArray(),
                    StringSplitOptions.RemoveEmptyEntries));

                //Set the default
                if (cboServer.Items.Count > 1)
                    cboServer.SelectedText = Settings.Default.WebServiceUrlDefault;
                else
                    cboServer.SelectedIndex = 0;

                //Sort the list.
                //cboServer.Sorted = true;

            }

            //Enable/disable the "Login as a Different User" checkbox
            SetLoginDiffUserCheckboxState(cboServer.Text);

            //enable password reset label link based on if client password reset is enabled on server
            enablePasswordResetLabelLink(isClientPasswordResetEnabled());

            
            this.ResumeDrawing();
        }

        #endregion Constructor & Control Load



        #region Methods
        /// <summary>
        /// Loads the UI values from the Localalized .resx file.
        /// </summary>
        private void loadUi()
        {
            try
            {
                _isLoading = true;

                //Get the labels from the localized .resx file.
                Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
                cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
                cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
                cmdSettings.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Settings");
                lblUserId.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserLogon.UsernameLabelText");
                lblPassword.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserLogon.PasswordLabelText");
                lblServerUrl.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserLogon.ServerUrlLabelText");
                //Use this for betas/RC
                //lblVersion.Text = PafApp.GetLocalization().GetResourceManager().GetString("About.ClientVersion") + " " + "2.8.4.0 - RC2";
                //Use This.
                lblVersion.Text = PafApp.GetLocalization().GetResourceManager().GetString("About.ClientVersion") + " " + Assembly.GetExecutingAssembly().GetName().Version;
                resetPasswordLinkLabel.Text =PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserLogon.ResetPasswordLinkLabelText");
                chkLoginDiffUsr.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserLogon.LoginAsDifferentUserText");
                chkLoginDiffUsr.Checked = Settings.Default.LoginAsDifferentUser;

                toolTip.SetToolTip(txtUserid,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.UserLogon.Userid")));
                toolTip.SetToolTip(txtPassword,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.UserLogon.Password")));
                toolTip.SetToolTip(cboServer,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.UserLogon.Server")));
                toolTip.SetToolTip(cmdCancel,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Cancel")));
                toolTip.SetToolTip(cmdOk,
                    String.Format(PafApp.GetLocalization().GetResourceManager().
                    GetString("Application.Controls.ToolTips.Buttons.Ok")));

                resetPasswordLinkLabel.Top = lblPassword.Top;

                _isLoading = false;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                _isLoading = false;
            }
        }
        private int suspendCounter = 0;
        [DebuggerHidden]
        private void SuspendDrawing()
        {
            if (suspendCounter == 0)
                SendMessage(this.Handle, WM_SETREDRAW, false, 0);
            suspendCounter++;
        }
        [DebuggerHidden]
        private void ResumeDrawing()
        {
            suspendCounter--;
            if (suspendCounter == 0)
            {
                SendMessage(this.Handle, WM_SETREDRAW, true, 0);
                this.Refresh();
            }
        }
        [DebuggerHidden]
        protected override void OnPaint(PaintEventArgs e)
        {
            //  Do nothing here!
        }
        [DebuggerHidden]
        protected override void OnPaintBackground(PaintEventArgs e)
        {

            //// Create a Bitmap object from an image file.
            //Bitmap myBitmap = new Bitmap(@"C:\Users\administrator\Documents\Titan\Titan\Pace\Localization\Resources\client_login_600-430.png");
            //Bitmap myBitmap = (Bitmap)PafApp.GetLocalization().GetResourceManager().GetObject("client_login_600-430");
            Bitmap myBitmap = backgroundImage;

            // Draw myBitmap to the screen.
            e.Graphics.DrawImage(myBitmap, 0, 0, myBitmap.Width,
                myBitmap.Height);

            //Color backColor = myBitmap.GetPixel(1, 1);

            //// Make the default transparent color transparent for myBitmap.
            //myBitmap.MakeTransparent(backColor);

            // Draw the transparent bitmap to the screen.
            e.Graphics.DrawImage(myBitmap, myBitmap.Width, 0,
                myBitmap.Width, myBitmap.Height);

        }

        /// <summary>
        /// Init the actual authentication with the server, if valid set logon flag to true.
        /// </summary>
        /// <param name="userid">The userid to be sent to the server.</param>
        /// <param name="passwordHash">The password to be sent to the server.</param>
        /// <param name="url">The url of the server.</param>
        private void SetInfo(string userid, string passwordHash, string url)
        {
            Cursor = Cursors.WaitCursor;

            authMode currentAuthMode = PafApp.GetAuthMode(url);

            try
            {
                bool Cancel = false;
                _clientUpgradeRequired = false;
                //ttn-981

                if (!String.IsNullOrEmpty(_selectedUrl) && !_selectedUrl.Equals(url))
                {
                    if (!_initialLogin)
                    {
                        FireLogonChangedEvent(new LogonEventArgs(_selectedUrl, url,
                                                  LogonEventArgs.ChangeType.Server), ref Cancel);
                    }
                }
                else if (!_userid.Equals(userid) || !String.IsNullOrEmpty(_userid))
                {
                    if (!_initialLogin)
                    {
                        //if the old and new userid's match, then fire the relogin event
                        //with the userid_relogin changetype, so the message can be customized.
                        if (_userid.ToLowerInvariant().Equals(userid.ToLowerInvariant()))
                        {
                            FireLogonChangedEvent(new LogonEventArgs(_userid, userid,
                                                      LogonEventArgs.ChangeType.Userid_Relogin), ref Cancel);
                        }
                        else
                        {
                            FireLogonChangedEvent(new LogonEventArgs(_userid, userid,
                                                      LogonEventArgs.ChangeType.Userid), ref Cancel);
                        }
                    }
                }
                else if (!_password.Equals(passwordHash) || !String.IsNullOrEmpty(_password))
                {
                    if (!_initialLogin)
                    {
                        FireLogonChangedEvent(new LogonEventArgs(_password, passwordHash,
                                                  LogonEventArgs.ChangeType.Password), ref Cancel);
                    }
                }


                PafLogonInfo user = EncryptUser(userid, passwordHash, currentAuthMode);

                if (Cancel)
                {
                    _logonValid = false;
                    return;
                }
                else
                {
                    _userid = userid;
                    _selectedUrl = url;
                    _initialLogin = false;
                }

                if (String.IsNullOrEmpty(Settings.Default.WebServiceUrlDefault))
                {
                    Settings.Default.WebServiceUrlDefault = _selectedUrl;
                }

                //If the item in not part of the list then add it.
                if (!Settings.Default.WebServiceUrlList.Contains(_selectedUrl))
                {
                    if (String.IsNullOrEmpty(Settings.Default.WebServiceUrlList))
                        Settings.Default.WebServiceUrlList += _selectedUrl;
                    else
                        Settings.Default.WebServiceUrlList += "," + _selectedUrl;
                }


                //Logoff the Server if already logged on
                //PafApp.EndPlanningSession();

                //Set the last user id
                Settings.Default.LastUserName = _userid;
                //Set the default property
                //Settings.Default.WebServiceUrlDefault = _selectedUrl;
                //Set the web service URL
                Settings.Default.Titan_PafService_PafService = _selectedUrl;
                //Set the setting
                Settings.Default.LoginAsDifferentUser = chkLoginDiffUsr.Checked;
                //Write to disk
                Settings.Default.Save();
                //Reload the settings files.
                Settings.Default.Reload();

                try
                {
                    //Logon to the server.
                    //PafApp.SendClientAuth(userid, passwordHash, "", "", "", "");
                    PafApp.SendClientAuth(user.UserName, passwordHash, user.UserDomain, user.IV, user.UserPassword, user.UserSID);
                    PafApp.GetChangedCellMngr().ReloadDefaultColors();
                }
                //if user needs to change password, this exception will be thrown
                catch (ChangePasswordException)
                {

                    //create window from and open
                    frmChangePassword changePasswordForm = new frmChangePassword(userid, passwordHash, PafApp.MinNewPasswordLength, PafApp.MaxNewPasswordLength);
                    DialogResult dialogResult = changePasswordForm.ShowDialog();

                    //if result was ok, try to change password
                    if (dialogResult == DialogResult.OK)
                    {
                        //string oldPassword = changePasswordForm._changePassword.OldPassword;
                        string newPassword = changePasswordForm._changePassword.NewPassword;

                        PafApp.ChangePasswordRequest(_userid, _password, newPassword);

                        //set login info using new password
                        PafApp.GetGridApp().LogonInformation.UserPassword = newPassword;

                        //TTN-1359
                        user = EncryptUser(userid, newPassword, currentAuthMode);

                        //TTN-1043
                        //PafApp.SendClientAuth(_userid, newPassword, "", "", "", "");
                        //TTN-1359
                        PafApp.SendClientAuth(user.UserName, newPassword, user.UserDomain, user.IV, user.UserPassword, user.UserSID);
                    }
                    //if canceled....throw exception explaining to user that they must change password.
                    else if (dialogResult == DialogResult.Cancel)
                    {
                        throw new Exception(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.PasswordChangeCanceled"));

                    }

                }

                _logonValid = true;

                //Fire the user authenicated event.
                if (UserAuthenicated != null)
                {
                    UserAuthenicated(this, userid);
                }
            }
            catch (SecurityException ex)
            {
                _logonValid = false;
                MessageBox.Show(this, ex.Message, PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"), MessageBoxButtons.OK, MessageBoxIcon.Error);

                //enable password reset label link based on if client password reset is enabled on server
                enablePasswordResetLabelLink(isClientPasswordResetEnabled());
            }
            catch (OutOfDateClientException)
            {
                _clientUpgradeRequired = true;
                _logonValid = false;
            }
            catch (Exception ex)
            {
                _logonValid = false;
                PafApp.MessageBox().Show(ex);

                //enable password reset label link based on if client password reset is enabled on server
                enablePasswordResetLabelLink(isClientPasswordResetEnabled());
            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }

        /// <summary>
        /// Create an encryped paf logon object.
        /// </summary>
        /// <param name="userid">Current User Id</param>
        /// <param name="passwordHash">An unencryped password.</param>
        /// <param name="currentAuthMode">The current auth mode.</param>
        /// <returns>An Encryped PafLogonInfo object</returns>
        /// <remarks>ReFarkased to Fix TTN-1359</remarks>
        private PafLogonInfo EncryptUser(string userid, string passwordHash, authMode currentAuthMode)
        {
            PafLogonInfo user = new PafLogonInfo(userid, passwordHash, null, null, null);

            //if the auth mode is mixed and the password is empty, then we have to send special characters.
            if (currentAuthMode == authMode.mixedMode && !chkLoginDiffUsr.Checked)
            {
                if (userid.Contains(PafAppConstants.FOWARD_SLASH) || userid.Contains(PafAppConstants.ASTERISK))
                {
                    user.UserSID = Security.GetUserSID();
                    user.UserDomain = null;
                    passwordHash = null;
                    _password = null;
                    _IsDomainUser = true;
                }
            }
            else if (currentAuthMode == authMode.mixedMode && chkLoginDiffUsr.Checked)
            {
                if (userid.Contains(PafAppConstants.FOWARD_SLASH) || userid.Contains(PafAppConstants.ASTERISK))
                {
                    user.UserSID = null;
                    user.UserDomain = Security.GetUsersDomain(userid, true);
                    passwordHash = null;
                    _IsDomainUser = true;
                    user = Security.EncryptUser(user);
                }
                else
                {
                    user.UserSID = null;
                    user.UserDomain = null;// Security.GetUsersDomain(userid, true);
                    _password = passwordHash;
                    _IsDomainUser = false;
                    user = Security.EncryptUser(user);
                }
            }
            if (currentAuthMode == authMode.nativeMode)
            {
                user.UserSID = null;
                user.UserDomain = null;
                _password = passwordHash;
                user = Security.EncryptUser(user);
            }
            return user;
        }

        /// <summary>
        /// Fires the LogonChangedEvent.
        /// </summary>
        /// <param name="e">LogonEventArgs parameters.</param>
        /// <param name="Cancel">Cancel this action.</param>
        private void FireLogonChangedEvent(LogonEventArgs e, ref bool Cancel)
        {
            if (LogonInfoChanged != null)
            {
                LogonInfoChanged(this, e, ref Cancel);
            }
        }

        /// <summary>
        /// Method to check if the keypress was an enter key and if the ok button is enabled, 
        /// click it for the user.
        /// </summary>
        /// <param name="e">KeyEventArgs</param>
        private void ControlKeyPress(KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (cmdOk.Enabled)
                {
                    cmdOk.PerformClick();
                }
            }
            else if (e.KeyCode == Keys.Escape)
            {
                cmdCancel.PerformClick();
            }
        }
        #endregion Methods

        #region Properties
        /// <summary>
        /// Get or set the username in the control.
        /// </summary>
        /// 
        [
		Browsable(true),
		Category("Data"),
		Description("The username to be populated in the userid box."),
		DefaultValue("")
		]
        public string UserName
        {
            set
            { 
                bool Cancel = false;
                if (!_userid.Equals(value))
                {
                    FireLogonChangedEvent(new LogonEventArgs(_userid, value,
                                                LogonEventArgs.ChangeType.Userid), ref Cancel);
                }

                if (Cancel)
                    return;

                _userid = value ;
                txtUserid.Text = _userid;
            }
            get { return _userid;}
        }

        /// <summary>
        /// Get or set the password in the control.
        /// </summary>.
        [
        Browsable(true),
        Category("Data"),
        Description("The password to be populated in the user password box."),
        DefaultValue("")
        ]
        public string UserPassword
        {
            set
            {
                bool Cancel = false;
                if (!_password.Equals(value))
                {
                    FireLogonChangedEvent(new LogonEventArgs(_password, value,
                                                LogonEventArgs.ChangeType.Password), ref Cancel);
                }

                if (Cancel)
                    return;

                _password = value;
                txtPassword.Text = _password;
            }
            get { return _password;}
        }

        /// <summary>
        /// Get or set the url in the control.
        /// </summary>
        [
        Browsable(true),
        Category("Data"),
        Description("The server URL to be populated in the server url box."),
        DefaultValue("")
        ]
        public string URL
        {
            set
            {
                bool Cancel = false;
                if (! _selectedUrl.Equals(value))
                {
                    FireLogonChangedEvent(new LogonEventArgs(_selectedUrl, value,
                                                LogonEventArgs.ChangeType.Server), ref Cancel);
                }

                if (Cancel)
                    return;

                _selectedUrl  = value;
                cboServer.Text = _selectedUrl;
            }
            get { return _selectedUrl; }
        }

        /// <summary>
        /// Get information on wheather or not the user has been authenticated.
        /// </summary>
        [
        Browsable(false),
        Category("Data"),
        Description("Returns if the user has been authenicated.."),
        DefaultValue("")
        ]
        public bool isUserAuthenaticated
        {
            get { return _logonValid; }
        }

        /// <summary>
        /// Gets whether or not a client upgrade is required to work with the server.
        /// </summary>
        [
        Browsable(false),
        Category("Data"),
        Description("Returns if a client upgrade is required."),
        DefaultValue("false")
        ]
        public bool ClientUpgradeRequired
        {
            get { return _clientUpgradeRequired; }
        }


        /// <summary>
        /// Gets whether or not the current user is a domain/LDAP user.
        /// </summary>
        public bool DomainUser
        {
            get { return _IsDomainUser; }
        }

        # endregion Properties

        #region Handlers
        /// <summary>
        /// Ok button click hander.
        /// </summary>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            if (cboServer.SelectedItem == null)
            {
                //Fix TTN-1373, TTN-1195 - Add trim()
                SetInfo(txtUserid.Text.Trim(), txtPassword.Text.Trim(), cboServer.Text.Trim());
            }
            else
            {
                //Fix TTN-1373, TTN-1195 - Add trim()
                SetInfo(txtUserid.Text.Trim(), txtPassword.Text.Trim(), cboServer.SelectedItem.ToString().Trim());
            }
        }

        /// <summary>
        /// Control KeyDown Handler.
        /// </summary>
        private void TextControlKeyDown(object sender, KeyEventArgs e)
        {
            ControlKeyPress(e);
        }

        /// <summary>
        /// When user clicks reset password link label
        /// </summary>
        private void resetPasswordLinkLabel_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {

            //set current url
            setPafServerUrl();

            try
            {
                //init a connection to server
                PafApp.Init();

                //get text from userid field if there
                frmPasswordReset _frmPasswordReset = new frmPasswordReset(txtUserid.Text);

                //open password reset dialog
                DialogResult dialogResult = _frmPasswordReset.ShowDialog(this);

                //if result is ok
                if (dialogResult == DialogResult.OK)
                {

                    //get username from password reset window form's user control
                    string usernameToReset = _frmPasswordReset._passwordReset.Username;

                    //if username is not null or empty
                    if (!String.IsNullOrEmpty(usernameToReset))
                    {

                        //trim
                        usernameToReset = usernameToReset.Trim();

                        //set user id field from user name to reset
                        txtUserid.Text = usernameToReset;

                        //perform password reset
                        PafApp.RequestPasswordReset(usernameToReset);

                        

                    }


                }

            }
            catch (Exception ex)
            {

                MessageBox.Show(this, ex.Message, PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"), MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            

        }

        #endregion Handlers

        /// <summary>
        /// Called when selected index changes on cboServer drop down
        /// </summary>
        private void cboServer_SelectedIndexChanged(object sender, EventArgs e)
        {

            //enable password reset label link based on if client password reset is enabled on server
            enablePasswordResetLabelLink(isClientPasswordResetEnabled());

        }

        /// <summary>
        /// enable/disables password reset label link
        /// </summary>
        private void enablePasswordResetLabelLink(bool enable)
        {

            resetPasswordLinkLabel.Visible = enable;


        }

        /// <summary>
        /// Does an init on the server and returns if client password reset is enabled on server
        /// </summary>
        private bool isClientPasswordResetEnabled()
        {

            setPafServerUrl();

            try
            {
                PafApp.Init();
                               
            }
            catch (Exception)
            {
                //do nothing
            }

            //If the checkbox is visible, then it's native or mixed mode
            if (chkLoginDiffUsr.Visible)
            {
                if (!chkLoginDiffUsr.Checked)
                {
                    return false;
                }
                return PafApp.ClientPasswordResetEnabled;
            }
            else
            {
                return PafApp.ClientPasswordResetEnabled;
            }
        }

        /// <summary>
        /// Sets teh paf service url base on the current value in cboServer drop down
        /// </summary>
        private void setPafServerUrl()
        {
            string _url;

            //if none are selected, get current text
            if (cboServer.SelectedItem == null)
            {
                _url = cboServer.Text;
            }
            else
            {
                _url = cboServer.SelectedItem.ToString();
            }

            //Set the default property
            Settings.Default.Titan_PafService_PafService = _url;

            //save 
            Settings.Default.Save();

            //reload 
            Settings.Default.Reload();
        }



        private void cboServer_Leave(object sender, EventArgs e)
        {
            //enable password reset label link based on if client password reset is enabled on server
            enablePasswordResetLabelLink(isClientPasswordResetEnabled());

            if (cmdOk.Focused)
            {
                cmdOk.PerformClick();
            }
            else if (cmdCancel.Focused)
            {
                cmdCancel.PerformClick();
            }

            //Enable/disable the "Login as a Different User" checkbox
            SetLoginDiffUserCheckboxState(cboServer.Text);

        }

        /// <summary>
        /// Sets the domain informaion in the login box.
        /// </summary>
        private void SetDominInformation(bool controlStatus)
        {
            txtUserid.Text = Security.GetUserName(true);
            _userid = txtUserid.Text;
            txtPassword.Text = String.Empty;
            txtUserid.Enabled = controlStatus;
            txtPassword.Enabled = controlStatus;
        }

        /// <summary>
        /// Sets the user name to the last logged in user
        /// </summary>
        private void SetLastUserInfo()
        {
            txtUserid.Text = Settings.Default.LastUserName;
            _userid = txtUserid.Text;
            txtPassword.Text = String.Empty;
            txtUserid.Enabled = true;
            txtPassword.Enabled = true;
        }

        private void SetLoginDiffUserCheckboxState(string serverUrl)
        {
            switch (PafApp.GetAuthMode(serverUrl))
            {
                case authMode.mixedMode:
                    chkLoginDiffUsr.Visible = true;
                    if (chkLoginDiffUsr.Checked)
                    {
                        SetLastUserInfo();
                    }
                    else
                    {
                        SetDominInformation(false);
                    }
                    break;
                case authMode.nativeMode:
                    chkLoginDiffUsr.Visible = false;
                    SetLastUserInfo();
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkLoginDiffUsr_CheckedChanged(object sender, EventArgs e)
        {
            if(chkLoginDiffUsr.Checked)
            {
                SetLastUserInfo();
            }
            else
            {
                SetDominInformation(false);
            }

            if (!_isLoading)
            {
                enablePasswordResetLabelLink(isClientPasswordResetEnabled());
            }

            //Set the setting
            Settings.Default.LoginAsDifferentUser = chkLoginDiffUsr.Checked;
            //Write to disk
            Settings.Default.Save();
        }

        private void cmdSettings_Click(object sender, EventArgs e)
        {
            //string before = cboServer.Text;

            frmConnectionSettings f = new frmConnectionSettings();
            DialogResult dr = f.ShowDialog(this);
            f.Dispose();

            //TTN-1384 - Make sure URL list is cleared when button is clicked on
            //connection setting form.
            //cboServer.ResetText();
            cboServer.Items.Clear();
            if (!String.IsNullOrEmpty(Settings.Default.WebServiceUrlList))
            {
                //Split the string of comma delimited url(s) and add it to the combo box.
                cboServer.Items.AddRange(Settings.Default.WebServiceUrlList.Split(",".ToCharArray(),
                    StringSplitOptions.RemoveEmptyEntries));

                //Resume the selected text, if it still is in the list
                //cboServer.Text = before;

                //Sort the list.
                //cboServer.Sorted = true;
            }
        }
    }
}