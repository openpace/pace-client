﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using System.Windows.Forms;
using Titan.Pace.Application.Win32;

namespace Titan.Pace.Application.Forms
{
    public partial class HelpRequestor : Form
    {
        public string BaseUrl { get; private set; }
        public string UrlPage { get; private set; }
        public string FullUrl { get; private set; }
        public string Message { get; set; }
        public bool ShowHelpButton { get; set; }
        public MessageBoxButtons Buttons { get; set; }
        public MessageBoxIcon BoxIcon { get; set; }
        public MessageBoxDefaultButton DefaultButton { get; set; }

        public HelpRequestor(string message, MessageBoxButtons buttons, MessageBoxIcon icon, string baseUrl = null, string urlPage = null, 
             bool showHelpButton = false, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1)
        {
            InitializeComponent();
            Tag = message;
            Message = message;
            Buttons = buttons;
            BoxIcon = icon;
            DefaultButton = defaultButton;
            ShowHelpButton = showHelpButton;
            BaseUrl = baseUrl;
            UrlPage = urlPage;
            if (!String.IsNullOrWhiteSpace(baseUrl) && !String.IsNullOrWhiteSpace(urlPage))
            {
                Uri uri = new Uri(new Uri(BaseUrl), urlPage);
                FullUrl = uri.ToString();
            }
        }

        #region Public

        public DialogResult ShowMessage()
        {

            SetupRequestor();

            NativeWindow mainWindow = new NativeWindow();
            mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);
            Show(mainWindow);

            DialogResult r = MessageBox.Show(
                Message,
                PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                Buttons,
                BoxIcon,
                DefaultButton,
                0,
                ShowHelpButton);

            DestroyRequestor();

            return r;
        }

        private void SetupRequestor()
        {
            if (!ShowHelpButton) return;
            HelpRequested += HelpRequster_HelpRequested;
        }

        private void DestroyRequestor()
        {
            if (!ShowHelpButton) return;
            HelpRequested -= HelpRequster_HelpRequested;
        }

        #endregion Public

        #region Private 

        private void HelpRequster_HelpRequested(object sender, HelpEventArgs hlpevent)
        {
            Win32Utils.OpenDefaultBrowser(FullUrl);
            hlpevent.Handled = true;
        }

        #endregion Private 

    }
}
