﻿namespace Titan.Pace.Application.Forms
{
    partial class frmCreateAsst
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule conditionValidationRule1 = new DevExpress.XtraEditors.DXErrorProvider.ConditionValidationRule();
            this.gridView3 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn6 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn7 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridProductResults = new DevExpress.XtraGrid.GridControl();
            this.gridViewProductResults = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridView4 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.gridColumn8 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn9 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.panelControl2 = new DevExpress.XtraEditors.PanelControl();
            this.xtraTabControlClusterAlgorthim = new DevExpress.XtraTab.XtraTabControl();
            this.tabTime = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl6 = new DevExpress.XtraLayout.LayoutControl();
            this.btnTimeSave = new DevExpress.XtraEditors.SimpleButton();
            this.comboBoxEditTime = new DevExpress.XtraEditors.ComboBoxEdit();
            this.timeTreeList = new DevExpress.XtraTreeList.TreeList();
            this.layoutControlGroup8 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlSavedTime = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem25 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem26 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabProducts = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl2 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControlProdBlank = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControlProducts = new DevExpress.XtraTab.XtraTabControl();
            this.tabProdProducts = new DevExpress.XtraTab.XtraTabPage();
            this.prodTreeList = new DevExpress.XtraTreeList.TreeList();
            this.tabProdAttr = new DevExpress.XtraTab.XtraTabPage();
            this.prodAttTreeList = new DevExpress.XtraTreeList.TreeList();
            this.tabProdResults = new DevExpress.XtraTab.XtraTabPage();
            this.spinEditProductLevel = new DevExpress.XtraEditors.SpinEdit();
            this.btnProductsSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControlSavedProducts = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditProducts = new DevExpress.XtraEditors.ComboBoxEdit();
            this.lcgProducts = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem9 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem8 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemProducts = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabLocations = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControlLocations = new DevExpress.XtraLayout.LayoutControl();
            this.spinEditLocationLevel = new DevExpress.XtraEditors.SpinEdit();
            this.btnLocationsSave = new DevExpress.XtraEditors.SimpleButton();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.xtraTabControlLocations = new DevExpress.XtraTab.XtraTabControl();
            this.tabLocLocations = new DevExpress.XtraTab.XtraTabPage();
            this.locTreeList = new DevExpress.XtraTreeList.TreeList();
            this.tabLocAttr = new DevExpress.XtraTab.XtraTabPage();
            this.locAttTreeList = new DevExpress.XtraTreeList.TreeList();
            this.tabLocResults = new DevExpress.XtraTab.XtraTabPage();
            this.gridLocationResults = new DevExpress.XtraGrid.GridControl();
            this.gridViewLocationResults = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.comboBoxEditLocations = new DevExpress.XtraEditors.ComboBoxEdit();
            this.labelSavedLocations = new DevExpress.XtraEditors.LabelControl();
            this.lcgLocations = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem10 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup3 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemLocationBlank = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemLocations = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup2 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItemLocationLabel = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemSavedLocations = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem15 = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabMeasures = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl4 = new DevExpress.XtraLayout.LayoutControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditRuleSet = new DevExpress.XtraEditors.ComboBoxEdit();
            this.barManager = new DevExpress.XtraBars.BarManager(this.components);
            this.barDockControlTop = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlBottom = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlLeft = new DevExpress.XtraBars.BarDockControl();
            this.barDockControlRight = new DevExpress.XtraBars.BarDockControl();
            this.barItemMoveTo = new DevExpress.XtraBars.BarSubItem();
            this.barCheckItem1 = new DevExpress.XtraBars.BarCheckItem();
            this.barEditItemCustomCluster = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEditCustomCluster = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barRenameCluster = new DevExpress.XtraBars.BarSubItem();
            this.barEditClusterName = new DevExpress.XtraBars.BarEditItem();
            this.repositoryItemTextEditClusterName = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.barCheckItemPercent = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemNumeric = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemNumeric4 = new DevExpress.XtraBars.BarCheckItem();
            this.barCheckItemPercent4 = new DevExpress.XtraBars.BarCheckItem();
            this.btnMeasuresSave = new DevExpress.XtraEditors.SimpleButton();
            this.measuresTreeList = new DevExpress.XtraTreeList.TreeList();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.labelSavedMeasures = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditMeasures = new DevExpress.XtraEditors.ComboBoxEdit();
            this.layoutControlGroup4 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem16 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem17 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem18 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem19 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItemRuleSet = new DevExpress.XtraLayout.LayoutControlItem();
            this.emptySpaceItem2 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItemMeasuresSpace = new DevExpress.XtraLayout.LayoutControlItem();
            this.tabCluster = new DevExpress.XtraTab.XtraTabPage();
            this.layoutControl5 = new DevExpress.XtraLayout.LayoutControl();
            this.xtraTabControlCluster = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPagePivot = new DevExpress.XtraTab.XtraTabPage();
            this.pivotGridCluster = new DevExpress.XtraPivotGrid.PivotGridControl();
            this.memberTooltipController = new DevExpress.Utils.ToolTipController(this.components);
            this.xtraTabPageAdHoc = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlPivotData = new DevExpress.XtraGrid.GridControl();
            this.gridViewPivotData = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridView6 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.comboBoxEditAlgorthim = new DevExpress.XtraEditors.ComboBoxEdit();
            this.spinEditNumIterations = new DevExpress.XtraEditors.SpinEdit();
            this.spinEditClusters = new DevExpress.XtraEditors.SpinEdit();
            this.btnGenerateClusters = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlGroup7 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.emptySpaceItem1 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.layoutControlItem23 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem27 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem28 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem22 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem21 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem29 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem12 = new DevExpress.XtraLayout.LayoutControlItem();
            this.simpleButton2 = new DevExpress.XtraEditors.SimpleButton();
            this.panelControl1 = new DevExpress.XtraEditors.PanelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            this.txtClusterLabel = new DevExpress.XtraEditors.TextEdit();
            this.panelControl3 = new DevExpress.XtraEditors.PanelControl();
            this.btnCancel = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layWizButtons = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlGroup5 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup6 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup9 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlGroup10 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.popupMenu = new DevExpress.XtraBars.PopupMenu(this.components);
            this.popupMenuNodes = new DevExpress.XtraBars.PopupMenu(this.components);
            this.emptySpaceItem3 = new DevExpress.XtraLayout.EmptySpaceItem();
            this.dxValidationProvider1 = new DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProductResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).BeginInit();
            this.panelControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlClusterAlgorthim)).BeginInit();
            this.xtraTabControlClusterAlgorthim.SuspendLayout();
            this.tabTime.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).BeginInit();
            this.layoutControl6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTime.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSavedTime)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).BeginInit();
            this.tabProducts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).BeginInit();
            this.layoutControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlProducts)).BeginInit();
            this.xtraTabControlProducts.SuspendLayout();
            this.tabProdProducts.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prodTreeList)).BeginInit();
            this.tabProdAttr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.prodAttTreeList)).BeginInit();
            this.tabProdResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditProductLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProducts.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProducts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            this.tabLocations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLocations)).BeginInit();
            this.layoutControlLocations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLocationLevel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlLocations)).BeginInit();
            this.xtraTabControlLocations.SuspendLayout();
            this.tabLocLocations.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.locTreeList)).BeginInit();
            this.tabLocAttr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.locAttTreeList)).BeginInit();
            this.tabLocResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLocationResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLocationResults)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLocations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgLocations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLocationBlank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLocations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLocationLabel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSavedLocations)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).BeginInit();
            this.tabMeasures.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).BeginInit();
            this.layoutControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditRuleSet.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCustomCluster)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditClusterName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.measuresTreeList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMeasures.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRuleSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMeasuresSpace)).BeginInit();
            this.tabCluster.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).BeginInit();
            this.layoutControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlCluster)).BeginInit();
            this.xtraTabControlCluster.SuspendLayout();
            this.xtraTabPagePivot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridCluster)).BeginInit();
            this.xtraTabPageAdHoc.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPivotData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPivotData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditAlgorthim.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumIterations.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditClusters.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).BeginInit();
            this.panelControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClusterLabel.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).BeginInit();
            this.panelControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layWizButtons)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuNodes)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).BeginInit();
            this.SuspendLayout();
            // 
            // gridView3
            // 
            this.gridView3.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn6,
            this.gridColumn7});
            this.gridView3.GridControl = this.gridProductResults;
            this.gridView3.Name = "gridView3";
            // 
            // gridColumn6
            // 
            this.gridColumn6.Caption = "gridColumn6";
            this.gridColumn6.Name = "gridColumn6";
            this.gridColumn6.Visible = true;
            this.gridColumn6.VisibleIndex = 0;
            // 
            // gridColumn7
            // 
            this.gridColumn7.Caption = "gridColumn7";
            this.gridColumn7.Name = "gridColumn7";
            this.gridColumn7.Visible = true;
            this.gridColumn7.VisibleIndex = 1;
            // 
            // gridProductResults
            // 
            this.gridProductResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridProductResults.Location = new System.Drawing.Point(0, 0);
            this.gridProductResults.MainView = this.gridViewProductResults;
            this.gridProductResults.Name = "gridProductResults";
            this.gridProductResults.Size = new System.Drawing.Size(826, 416);
            this.gridProductResults.TabIndex = 0;
            this.gridProductResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProductResults,
            this.gridView4,
            this.gridView3});
            // 
            // gridViewProductResults
            // 
            this.gridViewProductResults.GridControl = this.gridProductResults;
            this.gridViewProductResults.Name = "gridViewProductResults";
            this.gridViewProductResults.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewProductResults.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewProductResults.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewProductResults.OptionsView.EnableAppearanceOddRow = true;
            // 
            // gridView4
            // 
            this.gridView4.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.gridColumn8,
            this.gridColumn9});
            this.gridView4.GridControl = this.gridProductResults;
            this.gridView4.Name = "gridView4";
            // 
            // gridColumn8
            // 
            this.gridColumn8.Caption = "gridColumn8";
            this.gridColumn8.Name = "gridColumn8";
            this.gridColumn8.Visible = true;
            this.gridColumn8.VisibleIndex = 0;
            // 
            // gridColumn9
            // 
            this.gridColumn9.Caption = "gridColumn9";
            this.gridColumn9.Name = "gridColumn9";
            this.gridColumn9.Visible = true;
            this.gridColumn9.VisibleIndex = 1;
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.panelControl2);
            this.layoutControl1.Controls.Add(this.simpleButton2);
            this.layoutControl1.Controls.Add(this.panelControl1);
            this.layoutControl1.Controls.Add(this.panelControl3);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.HiddenItems.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem3});
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(930, 203, 248, 404);
            this.layoutControl1.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl1.Root = this.layoutControlGroup1;
            this.layoutControl1.Size = new System.Drawing.Size(982, 591);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // panelControl2
            // 
            this.panelControl2.Controls.Add(this.xtraTabControlClusterAlgorthim);
            this.panelControl2.Location = new System.Drawing.Point(12, 96);
            this.panelControl2.Name = "panelControl2";
            this.panelControl2.Size = new System.Drawing.Size(958, 483);
            this.panelControl2.TabIndex = 5;
            // 
            // xtraTabControlClusterAlgorthim
            // 
            this.xtraTabControlClusterAlgorthim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlClusterAlgorthim.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Left;
            this.xtraTabControlClusterAlgorthim.HeaderOrientation = DevExpress.XtraTab.TabOrientation.Horizontal;
            this.xtraTabControlClusterAlgorthim.Location = new System.Drawing.Point(2, 2);
            this.xtraTabControlClusterAlgorthim.Name = "xtraTabControlClusterAlgorthim";
            this.xtraTabControlClusterAlgorthim.SelectedTabPage = this.tabTime;
            this.xtraTabControlClusterAlgorthim.Size = new System.Drawing.Size(954, 479);
            this.xtraTabControlClusterAlgorthim.TabIndex = 0;
            this.xtraTabControlClusterAlgorthim.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabTime,
            this.tabProducts,
            this.tabLocations,
            this.tabMeasures,
            this.tabCluster});
            this.xtraTabControlClusterAlgorthim.Selecting += new DevExpress.XtraTab.TabPageCancelEventHandler(this.xtraTabControlClusterAlgorthim_Selecting);
            this.xtraTabControlClusterAlgorthim.Selected += new DevExpress.XtraTab.TabPageEventHandler(this.xtraTabControl1_Selected);
            this.xtraTabControlClusterAlgorthim.MouseHover += new System.EventHandler(this.xtraTabControl1_MouseHover);
            // 
            // tabTime
            // 
            this.tabTime.Controls.Add(this.layoutControl6);
            this.tabTime.Font = new System.Drawing.Font("Tahoma", 8.25F);
            this.tabTime.Name = "tabTime";
            this.tabTime.Size = new System.Drawing.Size(852, 473);
            this.tabTime.Text = "Time Horizon";
            // 
            // layoutControl6
            // 
            this.layoutControl6.Controls.Add(this.btnTimeSave);
            this.layoutControl6.Controls.Add(this.comboBoxEditTime);
            this.layoutControl6.Controls.Add(this.timeTreeList);
            this.layoutControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl6.Location = new System.Drawing.Point(0, 0);
            this.layoutControl6.Name = "layoutControl6";
            this.layoutControl6.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(719, 398, 250, 350);
            this.layoutControl6.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl6.Root = this.layoutControlGroup8;
            this.layoutControl6.Size = new System.Drawing.Size(852, 473);
            this.layoutControl6.TabIndex = 0;
            this.layoutControl6.Text = "layoutControl6";
            // 
            // btnTimeSave
            // 
            this.btnTimeSave.Location = new System.Drawing.Point(312, 12);
            this.btnTimeSave.MaximumSize = new System.Drawing.Size(75, 0);
            this.btnTimeSave.Name = "btnTimeSave";
            this.btnTimeSave.Size = new System.Drawing.Size(75, 22);
            this.btnTimeSave.StyleController = this.layoutControl6;
            this.btnTimeSave.TabIndex = 6;
            this.btnTimeSave.Text = "Save";
            this.btnTimeSave.Click += new System.EventHandler(this.btnTimeSave_Click);
            // 
            // comboBoxEditTime
            // 
            this.comboBoxEditTime.Location = new System.Drawing.Point(81, 12);
            this.comboBoxEditTime.Name = "comboBoxEditTime";
            this.comboBoxEditTime.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditTime.Size = new System.Drawing.Size(227, 20);
            this.comboBoxEditTime.StyleController = this.layoutControl6;
            this.comboBoxEditTime.TabIndex = 4;
            this.comboBoxEditTime.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditTime_SelectedIndexChanged);
            // 
            // timeTreeList
            // 
            this.timeTreeList.Location = new System.Drawing.Point(12, 38);
            this.timeTreeList.Name = "timeTreeList";
            this.timeTreeList.OptionsBehavior.Editable = false;
            this.timeTreeList.OptionsBehavior.EnableFiltering = true;
            this.timeTreeList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.Smart;
            this.timeTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.timeTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.timeTreeList.OptionsView.ShowAutoFilterRow = true;
            this.timeTreeList.OptionsView.ShowCheckBoxes = true;
            this.timeTreeList.OptionsView.ShowHorzLines = false;
            this.timeTreeList.OptionsView.ShowIndicator = false;
            this.timeTreeList.OptionsView.ShowVertLines = false;
            this.timeTreeList.Size = new System.Drawing.Size(828, 423);
            this.timeTreeList.TabIndex = 5;
            this.timeTreeList.BeforeCheckNode += new DevExpress.XtraTreeList.CheckNodeEventHandler(this.treeList1_BeforeCheckNode);
            this.timeTreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.AfterCheckNodeCheckState);
            this.timeTreeList.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.treeList_FilterNode);
            // 
            // layoutControlGroup8
            // 
            this.layoutControlGroup8.CustomizationFormText = "layoutControlGroup8";
            this.layoutControlGroup8.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup8.GroupBordersVisible = false;
            this.layoutControlGroup8.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlSavedTime,
            this.layoutControlItem25,
            this.layoutControlItem26});
            this.layoutControlGroup8.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup8.Name = "layoutControlGroup8";
            this.layoutControlGroup8.Size = new System.Drawing.Size(852, 473);
            this.layoutControlGroup8.Text = "layoutControlGroup8";
            this.layoutControlGroup8.TextVisible = false;
            // 
            // layoutControlSavedTime
            // 
            this.layoutControlSavedTime.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlSavedTime.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlSavedTime.Control = this.comboBoxEditTime;
            this.layoutControlSavedTime.CustomizationFormText = "Saved Time";
            this.layoutControlSavedTime.Location = new System.Drawing.Point(0, 0);
            this.layoutControlSavedTime.MaxSize = new System.Drawing.Size(300, 26);
            this.layoutControlSavedTime.MinSize = new System.Drawing.Size(300, 26);
            this.layoutControlSavedTime.Name = "layoutControlSavedTime";
            this.layoutControlSavedTime.Size = new System.Drawing.Size(300, 26);
            this.layoutControlSavedTime.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlSavedTime.Text = "Saved Time";
            this.layoutControlSavedTime.TextSize = new System.Drawing.Size(66, 13);
            // 
            // layoutControlItem25
            // 
            this.layoutControlItem25.Control = this.timeTreeList;
            this.layoutControlItem25.CustomizationFormText = "layoutControlItem25";
            this.layoutControlItem25.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem25.Name = "layoutControlItem25";
            this.layoutControlItem25.Size = new System.Drawing.Size(832, 427);
            this.layoutControlItem25.Text = "layoutControlItem25";
            this.layoutControlItem25.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem25.TextToControlDistance = 0;
            this.layoutControlItem25.TextVisible = false;
            // 
            // layoutControlItem26
            // 
            this.layoutControlItem26.Control = this.btnTimeSave;
            this.layoutControlItem26.CustomizationFormText = "layoutControlItem26";
            this.layoutControlItem26.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem26.MaxSize = new System.Drawing.Size(486, 26);
            this.layoutControlItem26.MinSize = new System.Drawing.Size(486, 26);
            this.layoutControlItem26.Name = "layoutControlItem26";
            this.layoutControlItem26.Size = new System.Drawing.Size(532, 26);
            this.layoutControlItem26.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem26.Text = "layoutControlItem26";
            this.layoutControlItem26.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem26.TextToControlDistance = 0;
            this.layoutControlItem26.TextVisible = false;
            // 
            // tabProducts
            // 
            this.tabProducts.Controls.Add(this.layoutControl2);
            this.tabProducts.Name = "tabProducts";
            this.tabProducts.Size = new System.Drawing.Size(856, 494);
            this.tabProducts.Text = "Products";
            // 
            // layoutControl2
            // 
            this.layoutControl2.Controls.Add(this.labelControlProdBlank);
            this.layoutControl2.Controls.Add(this.xtraTabControlProducts);
            this.layoutControl2.Controls.Add(this.spinEditProductLevel);
            this.layoutControl2.Controls.Add(this.btnProductsSave);
            this.layoutControl2.Controls.Add(this.labelControlSavedProducts);
            this.layoutControl2.Controls.Add(this.comboBoxEditProducts);
            this.layoutControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl2.Location = new System.Drawing.Point(0, 0);
            this.layoutControl2.Name = "layoutControl2";
            this.layoutControl2.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(1084, 58, 250, 350);
            this.layoutControl2.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl2.Root = this.lcgProducts;
            this.layoutControl2.Size = new System.Drawing.Size(856, 494);
            this.layoutControl2.TabIndex = 0;
            this.layoutControl2.Text = "layoutControlProducts";
            // 
            // labelControlProdBlank
            // 
            this.labelControlProdBlank.Location = new System.Drawing.Point(481, 12);
            this.labelControlProdBlank.Name = "labelControlProdBlank";
            this.labelControlProdBlank.Size = new System.Drawing.Size(118, 22);
            this.labelControlProdBlank.StyleController = this.layoutControl2;
            this.labelControlProdBlank.TabIndex = 13;
            // 
            // xtraTabControlProducts
            // 
            this.xtraTabControlProducts.Location = new System.Drawing.Point(12, 38);
            this.xtraTabControlProducts.Name = "xtraTabControlProducts";
            this.xtraTabControlProducts.SelectedTabPage = this.tabProdProducts;
            this.xtraTabControlProducts.Size = new System.Drawing.Size(832, 444);
            this.xtraTabControlProducts.TabIndex = 11;
            this.xtraTabControlProducts.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabProdAttr,
            this.tabProdProducts,
            this.tabProdResults});
            // 
            // tabProdProducts
            // 
            this.tabProdProducts.Controls.Add(this.prodTreeList);
            this.tabProdProducts.Name = "tabProdProducts";
            this.tabProdProducts.Size = new System.Drawing.Size(826, 416);
            this.tabProdProducts.Text = "Products";
            // 
            // prodTreeList
            // 
            this.prodTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prodTreeList.Location = new System.Drawing.Point(0, 0);
            this.prodTreeList.Name = "prodTreeList";
            this.prodTreeList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.Smart;
            this.prodTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.prodTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.prodTreeList.OptionsView.ShowAutoFilterRow = true;
            this.prodTreeList.OptionsView.ShowCheckBoxes = true;
            this.prodTreeList.OptionsView.ShowIndicator = false;
            this.prodTreeList.Size = new System.Drawing.Size(826, 416);
            this.prodTreeList.TabIndex = 0;
            this.prodTreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.prodTreeList_AfterCheckNode);
            this.prodTreeList.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.treeList_FilterNode);
            // 
            // tabProdAttr
            // 
            this.tabProdAttr.Controls.Add(this.prodAttTreeList);
            this.tabProdAttr.Name = "tabProdAttr";
            this.tabProdAttr.Size = new System.Drawing.Size(826, 416);
            this.tabProdAttr.Text = "Attributes";
            // 
            // prodAttTreeList
            // 
            this.prodAttTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.prodAttTreeList.Location = new System.Drawing.Point(0, 0);
            this.prodAttTreeList.Name = "prodAttTreeList";
            this.prodAttTreeList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.Smart;
            this.prodAttTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.prodAttTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.prodAttTreeList.OptionsView.ShowAutoFilterRow = true;
            this.prodAttTreeList.OptionsView.ShowCheckBoxes = true;
            this.prodAttTreeList.OptionsView.ShowIndicator = false;
            this.prodAttTreeList.Size = new System.Drawing.Size(826, 416);
            this.prodAttTreeList.TabIndex = 0;
            this.prodAttTreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.prodTreeList_AfterCheckNode);
            this.prodAttTreeList.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.treeList_FilterNode);
            // 
            // tabProdResults
            // 
            this.tabProdResults.Controls.Add(this.gridProductResults);
            this.tabProdResults.Name = "tabProdResults";
            this.tabProdResults.Size = new System.Drawing.Size(826, 416);
            this.tabProdResults.Tag = "results";
            this.tabProdResults.Text = "Results";
            // 
            // spinEditProductLevel
            // 
            this.spinEditProductLevel.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditProductLevel.Location = new System.Drawing.Point(743, 12);
            this.spinEditProductLevel.Name = "spinEditProductLevel";
            this.spinEditProductLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditProductLevel.Properties.IsFloatValue = false;
            this.spinEditProductLevel.Properties.Mask.EditMask = "N00";
            this.spinEditProductLevel.Properties.MaxValue = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.spinEditProductLevel.Size = new System.Drawing.Size(81, 20);
            this.spinEditProductLevel.StyleController = this.layoutControl2;
            this.spinEditProductLevel.TabIndex = 12;
            this.spinEditProductLevel.ValueChanged += new System.EventHandler(this.spinEditProductLevel_ValueChanged);
            // 
            // btnProductsSave
            // 
            this.btnProductsSave.Location = new System.Drawing.Point(416, 12);
            this.btnProductsSave.Name = "btnProductsSave";
            this.btnProductsSave.Size = new System.Drawing.Size(61, 22);
            this.btnProductsSave.StyleController = this.layoutControl2;
            this.btnProductsSave.TabIndex = 10;
            this.btnProductsSave.Text = "Save";
            this.btnProductsSave.Click += new System.EventHandler(this.btnProductsSave_Click);
            // 
            // labelControlSavedProducts
            // 
            this.labelControlSavedProducts.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControlSavedProducts.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelControlSavedProducts.Location = new System.Drawing.Point(12, 12);
            this.labelControlSavedProducts.Name = "labelControlSavedProducts";
            this.labelControlSavedProducts.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.labelControlSavedProducts.Size = new System.Drawing.Size(121, 22);
            this.labelControlSavedProducts.StyleController = this.layoutControl2;
            this.labelControlSavedProducts.TabIndex = 11;
            this.labelControlSavedProducts.Text = "Saved Products";
            // 
            // comboBoxEditProducts
            // 
            this.comboBoxEditProducts.Location = new System.Drawing.Point(137, 12);
            this.comboBoxEditProducts.Name = "comboBoxEditProducts";
            this.comboBoxEditProducts.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditProducts.Size = new System.Drawing.Size(275, 20);
            this.comboBoxEditProducts.StyleController = this.layoutControl2;
            this.comboBoxEditProducts.TabIndex = 9;
            this.comboBoxEditProducts.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditProducts_SelectedIndexChanged);
            // 
            // lcgProducts
            // 
            this.lcgProducts.CustomizationFormText = "layoutControlGroup2";
            this.lcgProducts.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgProducts.GroupBordersVisible = false;
            this.lcgProducts.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem4,
            this.layoutControlItem7,
            this.layoutControlItem9,
            this.layoutControlItem8,
            this.layoutControlItemProducts,
            this.layoutControlItem5});
            this.lcgProducts.Location = new System.Drawing.Point(0, 0);
            this.lcgProducts.Name = "lcgProducts";
            this.lcgProducts.Size = new System.Drawing.Size(856, 494);
            this.lcgProducts.Text = "lcgProducts";
            this.lcgProducts.TextVisible = false;
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.xtraTabControlProducts;
            this.layoutControlItem4.CustomizationFormText = "layoutControlItem4";
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(836, 448);
            this.layoutControlItem4.Text = "layoutControlItem4";
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextToControlDistance = 0;
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.labelControlSavedProducts;
            this.layoutControlItem7.CustomizationFormText = "layoutControlItem7";
            this.layoutControlItem7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem7.MaxSize = new System.Drawing.Size(125, 26);
            this.layoutControlItem7.MinSize = new System.Drawing.Size(125, 26);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(125, 26);
            this.layoutControlItem7.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem7.Text = "layoutControlItem7";
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextToControlDistance = 0;
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem9
            // 
            this.layoutControlItem9.Control = this.btnProductsSave;
            this.layoutControlItem9.CustomizationFormText = "layoutControlItem9";
            this.layoutControlItem9.Location = new System.Drawing.Point(404, 0);
            this.layoutControlItem9.MaxSize = new System.Drawing.Size(65, 26);
            this.layoutControlItem9.MinSize = new System.Drawing.Size(65, 26);
            this.layoutControlItem9.Name = "layoutControlItem9";
            this.layoutControlItem9.Size = new System.Drawing.Size(65, 26);
            this.layoutControlItem9.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem9.Text = "layoutControlItem9";
            this.layoutControlItem9.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem9.TextToControlDistance = 0;
            this.layoutControlItem9.TextVisible = false;
            // 
            // layoutControlItem8
            // 
            this.layoutControlItem8.Control = this.comboBoxEditProducts;
            this.layoutControlItem8.CustomizationFormText = "layoutControlItem8";
            this.layoutControlItem8.Location = new System.Drawing.Point(125, 0);
            this.layoutControlItem8.MaxSize = new System.Drawing.Size(279, 26);
            this.layoutControlItem8.MinSize = new System.Drawing.Size(279, 26);
            this.layoutControlItem8.Name = "layoutControlItem8";
            this.layoutControlItem8.Size = new System.Drawing.Size(279, 26);
            this.layoutControlItem8.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem8.Text = "layoutControlItem8";
            this.layoutControlItem8.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem8.TextToControlDistance = 0;
            this.layoutControlItem8.TextVisible = false;
            // 
            // layoutControlItemProducts
            // 
            this.layoutControlItemProducts.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemProducts.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemProducts.Control = this.spinEditProductLevel;
            this.layoutControlItemProducts.CustomizationFormText = "Product Clustering Level";
            this.layoutControlItemProducts.Location = new System.Drawing.Point(591, 0);
            this.layoutControlItemProducts.MaxSize = new System.Drawing.Size(225, 26);
            this.layoutControlItemProducts.MinSize = new System.Drawing.Size(195, 26);
            this.layoutControlItemProducts.Name = "layoutControlItemProducts";
            this.layoutControlItemProducts.Size = new System.Drawing.Size(245, 26);
            this.layoutControlItemProducts.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemProducts.Text = "Product Clustering Level";
            this.layoutControlItemProducts.TextSize = new System.Drawing.Size(137, 13);
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.labelControlProdBlank;
            this.layoutControlItem5.CustomizationFormText = "layoutControlItem5";
            this.layoutControlItem5.Location = new System.Drawing.Point(469, 0);
            this.layoutControlItem5.MaxSize = new System.Drawing.Size(122, 26);
            this.layoutControlItem5.MinSize = new System.Drawing.Size(122, 26);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(122, 26);
            this.layoutControlItem5.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem5.Text = "layoutControlItem5";
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextToControlDistance = 0;
            this.layoutControlItem5.TextVisible = false;
            // 
            // tabLocations
            // 
            this.tabLocations.Controls.Add(this.layoutControlLocations);
            this.tabLocations.Name = "tabLocations";
            this.tabLocations.Size = new System.Drawing.Size(856, 494);
            this.tabLocations.Text = "Locations";
            // 
            // layoutControlLocations
            // 
            this.layoutControlLocations.Controls.Add(this.spinEditLocationLevel);
            this.layoutControlLocations.Controls.Add(this.btnLocationsSave);
            this.layoutControlLocations.Controls.Add(this.labelControl8);
            this.layoutControlLocations.Controls.Add(this.xtraTabControlLocations);
            this.layoutControlLocations.Controls.Add(this.comboBoxEditLocations);
            this.layoutControlLocations.Controls.Add(this.labelSavedLocations);
            this.layoutControlLocations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControlLocations.Location = new System.Drawing.Point(0, 0);
            this.layoutControlLocations.Name = "layoutControlLocations";
            this.layoutControlLocations.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(962, 383, 431, 357);
            this.layoutControlLocations.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControlLocations.Root = this.lcgLocations;
            this.layoutControlLocations.Size = new System.Drawing.Size(856, 494);
            this.layoutControlLocations.TabIndex = 0;
            this.layoutControlLocations.Text = "layoutControlLocations";
            // 
            // spinEditLocationLevel
            // 
            this.spinEditLocationLevel.EditValue = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.spinEditLocationLevel.Location = new System.Drawing.Point(783, 12);
            this.spinEditLocationLevel.Name = "spinEditLocationLevel";
            this.spinEditLocationLevel.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditLocationLevel.Properties.IsFloatValue = false;
            this.spinEditLocationLevel.Properties.Mask.EditMask = "N00";
            this.spinEditLocationLevel.Properties.MaxValue = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.spinEditLocationLevel.Size = new System.Drawing.Size(61, 20);
            this.spinEditLocationLevel.StyleController = this.layoutControlLocations;
            this.spinEditLocationLevel.TabIndex = 17;
            this.spinEditLocationLevel.ValueChanged += new System.EventHandler(this.spinEditLocationLevel_ValueChanged);
            // 
            // btnLocationsSave
            // 
            this.btnLocationsSave.Location = new System.Drawing.Point(425, 12);
            this.btnLocationsSave.Name = "btnLocationsSave";
            this.btnLocationsSave.Size = new System.Drawing.Size(63, 22);
            this.btnLocationsSave.StyleController = this.layoutControlLocations;
            this.btnLocationsSave.TabIndex = 13;
            this.btnLocationsSave.Text = "Save";
            this.btnLocationsSave.Click += new System.EventHandler(this.btnLocationsSave_Click);
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.labelControl8.Location = new System.Drawing.Point(492, 12);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.labelControl8.Size = new System.Drawing.Size(143, 22);
            this.labelControl8.StyleController = this.layoutControlLocations;
            this.labelControl8.TabIndex = 15;
            this.labelControl8.Visible = false;
            // 
            // xtraTabControlLocations
            // 
            this.xtraTabControlLocations.Location = new System.Drawing.Point(12, 38);
            this.xtraTabControlLocations.Name = "xtraTabControlLocations";
            this.xtraTabControlLocations.SelectedTabPage = this.tabLocLocations;
            this.xtraTabControlLocations.Size = new System.Drawing.Size(832, 444);
            this.xtraTabControlLocations.TabIndex = 16;
            this.xtraTabControlLocations.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.tabLocAttr,
            this.tabLocLocations,
            this.tabLocResults});
            // 
            // tabLocLocations
            // 
            this.tabLocLocations.Controls.Add(this.locTreeList);
            this.tabLocLocations.Name = "tabLocLocations";
            this.tabLocLocations.Size = new System.Drawing.Size(826, 416);
            this.tabLocLocations.Text = "Locations";
            // 
            // locTreeList
            // 
            this.locTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.locTreeList.Location = new System.Drawing.Point(0, 0);
            this.locTreeList.Name = "locTreeList";
            this.locTreeList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.Smart;
            this.locTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.locTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.locTreeList.OptionsView.ShowAutoFilterRow = true;
            this.locTreeList.OptionsView.ShowCheckBoxes = true;
            this.locTreeList.OptionsView.ShowIndicator = false;
            this.locTreeList.Size = new System.Drawing.Size(826, 416);
            this.locTreeList.TabIndex = 0;
            this.locTreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.locTreeList_AfterCheckNode);
            this.locTreeList.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.treeList_FilterNode);
            // 
            // tabLocAttr
            // 
            this.tabLocAttr.Controls.Add(this.locAttTreeList);
            this.tabLocAttr.Name = "tabLocAttr";
            this.tabLocAttr.Size = new System.Drawing.Size(826, 416);
            this.tabLocAttr.Text = "Attributes";
            // 
            // locAttTreeList
            // 
            this.locAttTreeList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.locAttTreeList.Location = new System.Drawing.Point(0, 0);
            this.locAttTreeList.Name = "locAttTreeList";
            this.locAttTreeList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.Smart;
            this.locAttTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.locAttTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.locAttTreeList.OptionsView.ShowAutoFilterRow = true;
            this.locAttTreeList.OptionsView.ShowCheckBoxes = true;
            this.locAttTreeList.OptionsView.ShowIndicator = false;
            this.locAttTreeList.Size = new System.Drawing.Size(826, 416);
            this.locAttTreeList.TabIndex = 0;
            this.locAttTreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.locTreeList_AfterCheckNode);
            this.locAttTreeList.FilterNode += new DevExpress.XtraTreeList.FilterNodeEventHandler(this.treeList_FilterNode);
            // 
            // tabLocResults
            // 
            this.tabLocResults.Controls.Add(this.gridLocationResults);
            this.tabLocResults.Name = "tabLocResults";
            this.tabLocResults.Size = new System.Drawing.Size(826, 416);
            this.tabLocResults.Tag = "results";
            this.tabLocResults.Text = "Results";
            // 
            // gridLocationResults
            // 
            this.gridLocationResults.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLocationResults.Location = new System.Drawing.Point(0, 0);
            this.gridLocationResults.MainView = this.gridViewLocationResults;
            this.gridLocationResults.Name = "gridLocationResults";
            this.gridLocationResults.Size = new System.Drawing.Size(826, 416);
            this.gridLocationResults.TabIndex = 0;
            this.gridLocationResults.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLocationResults});
            // 
            // gridViewLocationResults
            // 
            this.gridViewLocationResults.GridControl = this.gridLocationResults;
            this.gridViewLocationResults.Name = "gridViewLocationResults";
            this.gridViewLocationResults.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewLocationResults.OptionsView.EnableAppearanceOddRow = true;
            // 
            // comboBoxEditLocations
            // 
            this.comboBoxEditLocations.Location = new System.Drawing.Point(142, 12);
            this.comboBoxEditLocations.Name = "comboBoxEditLocations";
            this.comboBoxEditLocations.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditLocations.Size = new System.Drawing.Size(279, 20);
            this.comboBoxEditLocations.StyleController = this.layoutControlLocations;
            this.comboBoxEditLocations.TabIndex = 12;
            this.comboBoxEditLocations.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditLocations_SelectedIndexChanged);
            // 
            // labelSavedLocations
            // 
            this.labelSavedLocations.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelSavedLocations.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelSavedLocations.Location = new System.Drawing.Point(12, 12);
            this.labelSavedLocations.Name = "labelSavedLocations";
            this.labelSavedLocations.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.labelSavedLocations.Size = new System.Drawing.Size(126, 22);
            this.labelSavedLocations.StyleController = this.layoutControlLocations;
            this.labelSavedLocations.TabIndex = 14;
            this.labelSavedLocations.Text = "Saved Locations";
            // 
            // lcgLocations
            // 
            this.lcgLocations.CustomizationFormText = "Root";
            this.lcgLocations.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.lcgLocations.GroupBordersVisible = false;
            this.lcgLocations.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem10,
            this.layoutControlGroup3,
            this.layoutControlGroup2});
            this.lcgLocations.Location = new System.Drawing.Point(0, 0);
            this.lcgLocations.Name = "Root";
            this.lcgLocations.Size = new System.Drawing.Size(856, 494);
            this.lcgLocations.Text = "Root";
            this.lcgLocations.TextVisible = false;
            // 
            // layoutControlItem10
            // 
            this.layoutControlItem10.Control = this.xtraTabControlLocations;
            this.layoutControlItem10.CustomizationFormText = "layoutControlItem10";
            this.layoutControlItem10.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem10.Name = "layoutControlItem10";
            this.layoutControlItem10.Size = new System.Drawing.Size(836, 448);
            this.layoutControlItem10.Text = "layoutControlItem10";
            this.layoutControlItem10.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem10.TextToControlDistance = 0;
            this.layoutControlItem10.TextVisible = false;
            // 
            // layoutControlGroup3
            // 
            this.layoutControlGroup3.CustomizationFormText = "layoutControlGroup3";
            this.layoutControlGroup3.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutControlGroup3.GroupBordersVisible = false;
            this.layoutControlGroup3.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemLocationBlank,
            this.layoutControlItemLocations});
            this.layoutControlGroup3.Location = new System.Drawing.Point(480, 0);
            this.layoutControlGroup3.Name = "layoutControlGroup3";
            this.layoutControlGroup3.Size = new System.Drawing.Size(356, 26);
            this.layoutControlGroup3.Text = "layoutControlGroup3";
            this.layoutControlGroup3.TextLocation = DevExpress.Utils.Locations.Left;
            // 
            // layoutControlItemLocationBlank
            // 
            this.layoutControlItemLocationBlank.Control = this.labelControl8;
            this.layoutControlItemLocationBlank.CustomizationFormText = "layoutControlItem11";
            this.layoutControlItemLocationBlank.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemLocationBlank.MinSize = new System.Drawing.Size(100, 17);
            this.layoutControlItemLocationBlank.Name = "layoutControlItemLocationBlank";
            this.layoutControlItemLocationBlank.Size = new System.Drawing.Size(147, 26);
            this.layoutControlItemLocationBlank.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLocationBlank.Text = "layoutControlItemLocationBlank";
            this.layoutControlItemLocationBlank.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLocationBlank.TextToControlDistance = 0;
            this.layoutControlItemLocationBlank.TextVisible = false;
            // 
            // layoutControlItemLocations
            // 
            this.layoutControlItemLocations.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemLocations.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemLocations.Control = this.spinEditLocationLevel;
            this.layoutControlItemLocations.CustomizationFormText = "Location Clustering Level";
            this.layoutControlItemLocations.Location = new System.Drawing.Point(147, 0);
            this.layoutControlItemLocations.MaxSize = new System.Drawing.Size(250, 30);
            this.layoutControlItemLocations.MinSize = new System.Drawing.Size(209, 26);
            this.layoutControlItemLocations.Name = "layoutControlItemLocations";
            this.layoutControlItemLocations.Size = new System.Drawing.Size(209, 26);
            this.layoutControlItemLocations.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLocations.Text = "Location Clustering Level";
            this.layoutControlItemLocations.TextSize = new System.Drawing.Size(141, 13);
            // 
            // layoutControlGroup2
            // 
            this.layoutControlGroup2.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup2.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutControlGroup2.GroupBordersVisible = false;
            this.layoutControlGroup2.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItemLocationLabel,
            this.layoutControlItemSavedLocations,
            this.layoutControlItem15});
            this.layoutControlGroup2.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup2.Name = "layoutControlGroup2";
            this.layoutControlGroup2.Size = new System.Drawing.Size(480, 26);
            this.layoutControlGroup2.Text = "layoutControlGroup2";
            this.layoutControlGroup2.TextLocation = DevExpress.Utils.Locations.Left;
            // 
            // layoutControlItemLocationLabel
            // 
            this.layoutControlItemLocationLabel.Control = this.labelSavedLocations;
            this.layoutControlItemLocationLabel.CustomizationFormText = "layoutControlItemLocationLabel";
            this.layoutControlItemLocationLabel.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItemLocationLabel.MaxSize = new System.Drawing.Size(130, 26);
            this.layoutControlItemLocationLabel.MinSize = new System.Drawing.Size(130, 26);
            this.layoutControlItemLocationLabel.Name = "layoutControlItemLocationLabel";
            this.layoutControlItemLocationLabel.Size = new System.Drawing.Size(130, 26);
            this.layoutControlItemLocationLabel.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemLocationLabel.Text = "layoutControlItemLocationLabel";
            this.layoutControlItemLocationLabel.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemLocationLabel.TextToControlDistance = 0;
            this.layoutControlItemLocationLabel.TextVisible = false;
            // 
            // layoutControlItemSavedLocations
            // 
            this.layoutControlItemSavedLocations.Control = this.comboBoxEditLocations;
            this.layoutControlItemSavedLocations.CustomizationFormText = "layoutControlItemSavedLocations";
            this.layoutControlItemSavedLocations.Location = new System.Drawing.Point(130, 0);
            this.layoutControlItemSavedLocations.MaxSize = new System.Drawing.Size(283, 26);
            this.layoutControlItemSavedLocations.MinSize = new System.Drawing.Size(283, 26);
            this.layoutControlItemSavedLocations.Name = "layoutControlItemSavedLocations";
            this.layoutControlItemSavedLocations.Size = new System.Drawing.Size(283, 26);
            this.layoutControlItemSavedLocations.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItemSavedLocations.Text = "layoutControlItemSavedLocations";
            this.layoutControlItemSavedLocations.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemSavedLocations.TextToControlDistance = 0;
            this.layoutControlItemSavedLocations.TextVisible = false;
            // 
            // layoutControlItem15
            // 
            this.layoutControlItem15.Control = this.btnLocationsSave;
            this.layoutControlItem15.CustomizationFormText = "layoutControlItemLocationSaveBtn";
            this.layoutControlItem15.Location = new System.Drawing.Point(413, 0);
            this.layoutControlItem15.MaxSize = new System.Drawing.Size(67, 26);
            this.layoutControlItem15.MinSize = new System.Drawing.Size(67, 26);
            this.layoutControlItem15.Name = "layoutControlItem15";
            this.layoutControlItem15.Size = new System.Drawing.Size(67, 26);
            this.layoutControlItem15.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem15.Text = "layoutControlItemLocationSaveBtn";
            this.layoutControlItem15.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem15.TextToControlDistance = 0;
            this.layoutControlItem15.TextVisible = false;
            // 
            // tabMeasures
            // 
            this.tabMeasures.Controls.Add(this.layoutControl4);
            this.tabMeasures.Name = "tabMeasures";
            this.tabMeasures.Size = new System.Drawing.Size(856, 494);
            this.tabMeasures.Text = "Measures";
            // 
            // layoutControl4
            // 
            this.layoutControl4.Controls.Add(this.labelControl5);
            this.layoutControl4.Controls.Add(this.comboBoxEditRuleSet);
            this.layoutControl4.Controls.Add(this.btnMeasuresSave);
            this.layoutControl4.Controls.Add(this.measuresTreeList);
            this.layoutControl4.Controls.Add(this.labelSavedMeasures);
            this.layoutControl4.Controls.Add(this.comboBoxEditMeasures);
            this.layoutControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl4.Location = new System.Drawing.Point(0, 0);
            this.layoutControl4.Name = "layoutControl4";
            this.layoutControl4.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(962, 267, 451, 350);
            this.layoutControl4.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl4.Root = this.layoutControlGroup4;
            this.layoutControl4.Size = new System.Drawing.Size(856, 494);
            this.layoutControl4.TabIndex = 0;
            this.layoutControl4.Text = "layoutControl4";
            // 
            // labelControl5
            // 
            this.labelControl5.Location = new System.Drawing.Point(466, 12);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(169, 13);
            this.labelControl5.StyleController = this.layoutControl4;
            this.labelControl5.TabIndex = 19;
            // 
            // comboBoxEditRuleSet
            // 
            this.comboBoxEditRuleSet.Location = new System.Drawing.Point(689, 12);
            this.comboBoxEditRuleSet.MenuManager = this.barManager;
            this.comboBoxEditRuleSet.Name = "comboBoxEditRuleSet";
            this.comboBoxEditRuleSet.Properties.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.comboBoxEditRuleSet.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditRuleSet.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditRuleSet.Size = new System.Drawing.Size(155, 20);
            this.comboBoxEditRuleSet.StyleController = this.layoutControl4;
            this.comboBoxEditRuleSet.TabIndex = 18;
            // 
            // barManager
            // 
            this.barManager.AllowCustomization = false;
            this.barManager.AllowMoveBarOnToolbar = false;
            this.barManager.AllowQuickCustomization = false;
            this.barManager.AllowShowToolbarsPopup = false;
            this.barManager.DockControls.Add(this.barDockControlTop);
            this.barManager.DockControls.Add(this.barDockControlBottom);
            this.barManager.DockControls.Add(this.barDockControlLeft);
            this.barManager.DockControls.Add(this.barDockControlRight);
            this.barManager.Form = this;
            this.barManager.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.barItemMoveTo,
            this.barCheckItem1,
            this.barEditItemCustomCluster,
            this.barRenameCluster,
            this.barEditClusterName,
            this.barCheckItemPercent,
            this.barCheckItemNumeric,
            this.barCheckItemNumeric4,
            this.barCheckItemPercent4});
            this.barManager.MaxItemId = 11;
            this.barManager.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEditCustomCluster,
            this.repositoryItemTextEditClusterName});
            // 
            // barDockControlTop
            // 
            this.barDockControlTop.CausesValidation = false;
            this.barDockControlTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.barDockControlTop.Location = new System.Drawing.Point(0, 0);
            this.barDockControlTop.Size = new System.Drawing.Size(982, 0);
            // 
            // barDockControlBottom
            // 
            this.barDockControlBottom.CausesValidation = false;
            this.barDockControlBottom.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.barDockControlBottom.Location = new System.Drawing.Point(0, 591);
            this.barDockControlBottom.Size = new System.Drawing.Size(982, 0);
            // 
            // barDockControlLeft
            // 
            this.barDockControlLeft.CausesValidation = false;
            this.barDockControlLeft.Dock = System.Windows.Forms.DockStyle.Left;
            this.barDockControlLeft.Location = new System.Drawing.Point(0, 0);
            this.barDockControlLeft.Size = new System.Drawing.Size(0, 591);
            // 
            // barDockControlRight
            // 
            this.barDockControlRight.CausesValidation = false;
            this.barDockControlRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.barDockControlRight.Location = new System.Drawing.Point(982, 0);
            this.barDockControlRight.Size = new System.Drawing.Size(0, 591);
            // 
            // barItemMoveTo
            // 
            this.barItemMoveTo.Alignment = DevExpress.XtraBars.BarItemLinkAlignment.Left;
            this.barItemMoveTo.Caption = "Move to";
            this.barItemMoveTo.Id = 0;
            this.barItemMoveTo.Name = "barItemMoveTo";
            // 
            // barCheckItem1
            // 
            this.barCheckItem1.Caption = "Cluster 1";
            this.barCheckItem1.Id = 1;
            this.barCheckItem1.Name = "barCheckItem1";
            // 
            // barEditItemCustomCluster
            // 
            this.barEditItemCustomCluster.Caption = "xxxxxxx";
            this.barEditItemCustomCluster.Edit = this.repositoryItemTextEditCustomCluster;
            this.barEditItemCustomCluster.Hint = "ggggg";
            this.barEditItemCustomCluster.Id = 2;
            this.barEditItemCustomCluster.Name = "barEditItemCustomCluster";
            this.barEditItemCustomCluster.Width = 150;
            // 
            // repositoryItemTextEditCustomCluster
            // 
            this.repositoryItemTextEditCustomCluster.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTextEditCustomCluster.AutoHeight = false;
            this.repositoryItemTextEditCustomCluster.MaxLength = 250;
            this.repositoryItemTextEditCustomCluster.Name = "repositoryItemTextEditCustomCluster";
            // 
            // barRenameCluster
            // 
            this.barRenameCluster.Caption = "Rename";
            this.barRenameCluster.Id = 3;
            this.barRenameCluster.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barEditClusterName)});
            this.barRenameCluster.Name = "barRenameCluster";
            this.barRenameCluster.Visibility = DevExpress.XtraBars.BarItemVisibility.Never;
            // 
            // barEditClusterName
            // 
            this.barEditClusterName.Caption = "barEditClusterName";
            this.barEditClusterName.Edit = this.repositoryItemTextEditClusterName;
            this.barEditClusterName.Id = 4;
            this.barEditClusterName.Name = "barEditClusterName";
            this.barEditClusterName.Width = 150;
            this.barEditClusterName.EditValueChanged += new System.EventHandler(this.barEditClusterName_EditValueChanged);
            // 
            // repositoryItemTextEditClusterName
            // 
            this.repositoryItemTextEditClusterName.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemTextEditClusterName.AutoHeight = false;
            this.repositoryItemTextEditClusterName.MaxLength = 250;
            this.repositoryItemTextEditClusterName.Name = "repositoryItemTextEditClusterName";
            // 
            // barCheckItemPercent
            // 
            this.barCheckItemPercent.Caption = "Percent (2)";
            this.barCheckItemPercent.Id = 6;
            this.barCheckItemPercent.Name = "barCheckItemPercent";
            this.barCheckItemPercent.Tag = "P2";
            this.barCheckItemPercent.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItem_CheckedChanged);
            // 
            // barCheckItemNumeric
            // 
            this.barCheckItemNumeric.Caption = "Numeric (2)";
            this.barCheckItemNumeric.Checked = true;
            this.barCheckItemNumeric.Id = 7;
            this.barCheckItemNumeric.Name = "barCheckItemNumeric";
            this.barCheckItemNumeric.Tag = "N2";
            this.barCheckItemNumeric.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItem_CheckedChanged);
            // 
            // barCheckItemNumeric4
            // 
            this.barCheckItemNumeric4.Caption = "Numeric (4)";
            this.barCheckItemNumeric4.Id = 8;
            this.barCheckItemNumeric4.Name = "barCheckItemNumeric4";
            this.barCheckItemNumeric4.Tag = "N4";
            this.barCheckItemNumeric4.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItem_CheckedChanged);
            // 
            // barCheckItemPercent4
            // 
            this.barCheckItemPercent4.Caption = "Percent (4)";
            this.barCheckItemPercent4.Id = 9;
            this.barCheckItemPercent4.Name = "barCheckItemPercent4";
            this.barCheckItemPercent4.Tag = "P4";
            this.barCheckItemPercent4.CheckedChanged += new DevExpress.XtraBars.ItemClickEventHandler(this.barCheckItem_CheckedChanged);
            // 
            // btnMeasuresSave
            // 
            this.btnMeasuresSave.Location = new System.Drawing.Point(397, 12);
            this.btnMeasuresSave.Name = "btnMeasuresSave";
            this.btnMeasuresSave.Size = new System.Drawing.Size(65, 22);
            this.btnMeasuresSave.StyleController = this.layoutControl4;
            this.btnMeasuresSave.TabIndex = 16;
            this.btnMeasuresSave.Text = "Save";
            this.btnMeasuresSave.Click += new System.EventHandler(this.btnMeasuresSave_Click);
            // 
            // measuresTreeList
            // 
            this.measuresTreeList.Location = new System.Drawing.Point(12, 38);
            this.measuresTreeList.Name = "measuresTreeList";
            this.measuresTreeList.OptionsBehavior.Editable = false;
            this.measuresTreeList.OptionsBehavior.EnableFiltering = true;
            this.measuresTreeList.OptionsFilter.FilterMode = DevExpress.XtraTreeList.FilterMode.Smart;
            this.measuresTreeList.OptionsSelection.MultiSelect = true;
            this.measuresTreeList.OptionsView.EnableAppearanceEvenRow = true;
            this.measuresTreeList.OptionsView.EnableAppearanceOddRow = true;
            this.measuresTreeList.OptionsView.ShowAutoFilterRow = true;
            this.measuresTreeList.OptionsView.ShowCheckBoxes = true;
            this.measuresTreeList.OptionsView.ShowIndicator = false;
            this.measuresTreeList.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox1,
            this.repositoryItemCheckEdit1});
            this.measuresTreeList.Size = new System.Drawing.Size(832, 419);
            this.measuresTreeList.TabIndex = 4;
            this.measuresTreeList.AfterCheckNode += new DevExpress.XtraTreeList.NodeEventHandler(this.AfterCheckNodeCheckState);
            this.measuresTreeList.MouseUp += new System.Windows.Forms.MouseEventHandler(this.measuresTreeList_MouseUp);
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AllowNullInput = DevExpress.Utils.DefaultBoolean.False;
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "WP",
            "LY"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            this.repositoryItemComboBox1.NullText = "WP";
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Caption = "Check";
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            this.repositoryItemCheckEdit1.NullStyle = DevExpress.XtraEditors.Controls.StyleIndeterminate.Unchecked;
            // 
            // labelSavedMeasures
            // 
            this.labelSavedMeasures.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelSavedMeasures.Appearance.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Near;
            this.labelSavedMeasures.Location = new System.Drawing.Point(14, 12);
            this.labelSavedMeasures.Name = "labelSavedMeasures";
            this.labelSavedMeasures.Padding = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.labelSavedMeasures.Size = new System.Drawing.Size(124, 22);
            this.labelSavedMeasures.StyleController = this.layoutControl4;
            this.labelSavedMeasures.TabIndex = 17;
            this.labelSavedMeasures.Text = "Saved Measures";
            // 
            // comboBoxEditMeasures
            // 
            this.comboBoxEditMeasures.Location = new System.Drawing.Point(142, 12);
            this.comboBoxEditMeasures.Name = "comboBoxEditMeasures";
            this.comboBoxEditMeasures.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditMeasures.Size = new System.Drawing.Size(251, 20);
            this.comboBoxEditMeasures.StyleController = this.layoutControl4;
            this.comboBoxEditMeasures.TabIndex = 15;
            this.comboBoxEditMeasures.SelectedIndexChanged += new System.EventHandler(this.comboBoxEditMeasures_SelectedIndexChanged);
            // 
            // layoutControlGroup4
            // 
            this.layoutControlGroup4.CustomizationFormText = "Root";
            this.layoutControlGroup4.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup4.GroupBordersVisible = false;
            this.layoutControlGroup4.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem16,
            this.layoutControlItem17,
            this.layoutControlItem18,
            this.layoutControlItem19,
            this.layoutControlItemRuleSet,
            this.emptySpaceItem2,
            this.layoutControlItemMeasuresSpace});
            this.layoutControlGroup4.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup4.Name = "Root";
            this.layoutControlGroup4.Size = new System.Drawing.Size(856, 494);
            this.layoutControlGroup4.Text = "Root";
            this.layoutControlGroup4.TextVisible = false;
            // 
            // layoutControlItem16
            // 
            this.layoutControlItem16.Control = this.measuresTreeList;
            this.layoutControlItem16.CustomizationFormText = "layoutControlItem16";
            this.layoutControlItem16.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem16.Name = "layoutControlItem16";
            this.layoutControlItem16.Size = new System.Drawing.Size(836, 423);
            this.layoutControlItem16.Text = "layoutControlItem16";
            this.layoutControlItem16.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem16.TextToControlDistance = 0;
            this.layoutControlItem16.TextVisible = false;
            // 
            // layoutControlItem17
            // 
            this.layoutControlItem17.Control = this.labelSavedMeasures;
            this.layoutControlItem17.CustomizationFormText = "layoutControlItem17";
            this.layoutControlItem17.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem17.MaxSize = new System.Drawing.Size(130, 26);
            this.layoutControlItem17.MinSize = new System.Drawing.Size(130, 26);
            this.layoutControlItem17.Name = "layoutControlItem17";
            this.layoutControlItem17.Padding = new DevExpress.XtraLayout.Utils.Padding(4, 2, 2, 2);
            this.layoutControlItem17.Size = new System.Drawing.Size(130, 26);
            this.layoutControlItem17.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem17.Text = "layoutControlItem17";
            this.layoutControlItem17.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem17.TextToControlDistance = 0;
            this.layoutControlItem17.TextVisible = false;
            // 
            // layoutControlItem18
            // 
            this.layoutControlItem18.Control = this.comboBoxEditMeasures;
            this.layoutControlItem18.CustomizationFormText = "layoutControlItem18";
            this.layoutControlItem18.Location = new System.Drawing.Point(130, 0);
            this.layoutControlItem18.MaxSize = new System.Drawing.Size(255, 26);
            this.layoutControlItem18.MinSize = new System.Drawing.Size(255, 26);
            this.layoutControlItem18.Name = "layoutControlItem18";
            this.layoutControlItem18.Size = new System.Drawing.Size(255, 26);
            this.layoutControlItem18.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem18.Text = "layoutControlItem18";
            this.layoutControlItem18.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem18.TextToControlDistance = 0;
            this.layoutControlItem18.TextVisible = false;
            // 
            // layoutControlItem19
            // 
            this.layoutControlItem19.Control = this.btnMeasuresSave;
            this.layoutControlItem19.CustomizationFormText = "layoutControlItem19";
            this.layoutControlItem19.Location = new System.Drawing.Point(385, 0);
            this.layoutControlItem19.MaxSize = new System.Drawing.Size(69, 26);
            this.layoutControlItem19.MinSize = new System.Drawing.Size(69, 26);
            this.layoutControlItem19.Name = "layoutControlItem19";
            this.layoutControlItem19.Size = new System.Drawing.Size(69, 26);
            this.layoutControlItem19.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem19.Text = "layoutControlItem19";
            this.layoutControlItem19.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem19.TextToControlDistance = 0;
            this.layoutControlItem19.TextVisible = false;
            // 
            // layoutControlItemRuleSet
            // 
            this.layoutControlItemRuleSet.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItemRuleSet.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItemRuleSet.BestFitWeight = 50;
            this.layoutControlItemRuleSet.Control = this.comboBoxEditRuleSet;
            this.layoutControlItemRuleSet.CustomizationFormText = "Rule Set";
            this.layoutControlItemRuleSet.Location = new System.Drawing.Point(627, 0);
            this.layoutControlItemRuleSet.Name = "layoutControlItemRuleSet";
            this.layoutControlItemRuleSet.Size = new System.Drawing.Size(209, 26);
            this.layoutControlItemRuleSet.Text = "Rule Set";
            this.layoutControlItemRuleSet.TextSize = new System.Drawing.Size(47, 13);
            // 
            // emptySpaceItem2
            // 
            this.emptySpaceItem2.AllowHotTrack = false;
            this.emptySpaceItem2.CustomizationFormText = "emptySpaceItem2";
            this.emptySpaceItem2.Location = new System.Drawing.Point(0, 449);
            this.emptySpaceItem2.Name = "emptySpaceItem2";
            this.emptySpaceItem2.Size = new System.Drawing.Size(836, 25);
            this.emptySpaceItem2.Text = "emptySpaceItem2";
            this.emptySpaceItem2.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItemMeasuresSpace
            // 
            this.layoutControlItemMeasuresSpace.Control = this.labelControl5;
            this.layoutControlItemMeasuresSpace.CustomizationFormText = "layoutControlItem6";
            this.layoutControlItemMeasuresSpace.Location = new System.Drawing.Point(454, 0);
            this.layoutControlItemMeasuresSpace.Name = "layoutControlItemMeasuresSpace";
            this.layoutControlItemMeasuresSpace.Size = new System.Drawing.Size(173, 26);
            this.layoutControlItemMeasuresSpace.Text = "layoutControlItemMeasuresSpace";
            this.layoutControlItemMeasuresSpace.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItemMeasuresSpace.TextToControlDistance = 0;
            this.layoutControlItemMeasuresSpace.TextVisible = false;
            // 
            // tabCluster
            // 
            this.tabCluster.Controls.Add(this.layoutControl5);
            this.tabCluster.Name = "tabCluster";
            this.tabCluster.Size = new System.Drawing.Size(856, 494);
            this.tabCluster.Text = "Cluster Algorithm";
            // 
            // layoutControl5
            // 
            this.layoutControl5.Controls.Add(this.xtraTabControlCluster);
            this.layoutControl5.Controls.Add(this.labelControl4);
            this.layoutControl5.Controls.Add(this.labelControl2);
            this.layoutControl5.Controls.Add(this.comboBoxEditAlgorthim);
            this.layoutControl5.Controls.Add(this.spinEditNumIterations);
            this.layoutControl5.Controls.Add(this.spinEditClusters);
            this.layoutControl5.Controls.Add(this.btnGenerateClusters);
            this.layoutControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl5.Location = new System.Drawing.Point(0, 0);
            this.layoutControl5.Name = "layoutControl5";
            this.layoutControl5.OptionsCustomizationForm.DesignTimeCustomizationFormPositionAndSize = new System.Drawing.Rectangle(191, 287, 551, 481);
            this.layoutControl5.OptionsView.UseDefaultDragAndDropRendering = false;
            this.layoutControl5.Root = this.layoutControlGroup7;
            this.layoutControl5.Size = new System.Drawing.Size(856, 494);
            this.layoutControl5.TabIndex = 12;
            this.layoutControl5.Text = "layoutControl5";
            // 
            // xtraTabControlCluster
            // 
            this.xtraTabControlCluster.ClosePageButtonShowMode = DevExpress.XtraTab.ClosePageButtonShowMode.InActiveTabPageHeaderAndOnMouseHover;
            this.xtraTabControlCluster.HeaderButtons = ((DevExpress.XtraTab.TabButtons)((DevExpress.XtraTab.TabButtons.Close | DevExpress.XtraTab.TabButtons.Default)));
            this.xtraTabControlCluster.HeaderButtonsShowMode = DevExpress.XtraTab.TabButtonShowMode.WhenNeeded;
            this.xtraTabControlCluster.Location = new System.Drawing.Point(7, 33);
            this.xtraTabControlCluster.Name = "xtraTabControlCluster";
            this.xtraTabControlCluster.SelectedTabPage = this.xtraTabPagePivot;
            this.xtraTabControlCluster.Size = new System.Drawing.Size(842, 454);
            this.xtraTabControlCluster.TabIndex = 18;
            this.xtraTabControlCluster.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPagePivot,
            this.xtraTabPageAdHoc});
            this.xtraTabControlCluster.SelectedPageChanging += new DevExpress.XtraTab.TabPageChangingEventHandler(this.xtraTabControlCluster_SelectedPageChanging);
            this.xtraTabControlCluster.CloseButtonClick += new System.EventHandler(this.xtraTabControlCluster_CloseButtonClick);
            // 
            // xtraTabPagePivot
            // 
            this.xtraTabPagePivot.Controls.Add(this.pivotGridCluster);
            this.xtraTabPagePivot.Name = "xtraTabPagePivot";
            this.xtraTabPagePivot.Size = new System.Drawing.Size(836, 426);
            this.xtraTabPagePivot.Text = "Pivot";
            // 
            // pivotGridCluster
            // 
            this.pivotGridCluster.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pivotGridCluster.Location = new System.Drawing.Point(0, 0);
            this.pivotGridCluster.Name = "pivotGridCluster";
            this.pivotGridCluster.OptionsCustomization.AllowCustomizationForm = false;
            this.pivotGridCluster.OptionsCustomization.AllowDrag = false;
            this.pivotGridCluster.OptionsCustomization.AllowDragInCustomizationForm = false;
            this.pivotGridCluster.OptionsCustomization.AllowEdit = false;
            this.pivotGridCluster.OptionsCustomization.AllowFilter = false;
            this.pivotGridCluster.OptionsData.AutoExpandGroups = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridCluster.OptionsFilterPopup.AllowContextMenu = false;
            this.pivotGridCluster.OptionsView.GroupFieldsInCustomizationWindow = false;
            this.pivotGridCluster.OptionsView.ShowColumnGrandTotalHeader = false;
            this.pivotGridCluster.OptionsView.ShowColumnGrandTotals = false;
            this.pivotGridCluster.OptionsView.ShowColumnHeaders = false;
            this.pivotGridCluster.OptionsView.ShowColumnTotals = false;
            this.pivotGridCluster.OptionsView.ShowDataHeaders = false;
            this.pivotGridCluster.OptionsView.ShowRowGrandTotalHeader = false;
            this.pivotGridCluster.Size = new System.Drawing.Size(836, 426);
            this.pivotGridCluster.TabIndex = 13;
            this.pivotGridCluster.ToolTipController = this.memberTooltipController;
            this.pivotGridCluster.CustomSummary += new DevExpress.XtraPivotGrid.PivotGridCustomSummaryEventHandler(this.pivotGridCluster_CustomSummary);
            this.pivotGridCluster.CustomFieldSort += new DevExpress.XtraPivotGrid.PivotGridCustomFieldSortEventHandler(this.pivotGridCluster_CustomFieldSort);
            this.pivotGridCluster.CellDoubleClick += new DevExpress.XtraPivotGrid.PivotCellEventHandler(this.pivotGridControl2_CellDoubleClick);
            this.pivotGridCluster.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pivotGridCluster_MouseMove);
            // 
            // xtraTabPageAdHoc
            // 
            this.xtraTabPageAdHoc.Controls.Add(this.gridControlPivotData);
            this.xtraTabPageAdHoc.Name = "xtraTabPageAdHoc";
            this.xtraTabPageAdHoc.Size = new System.Drawing.Size(836, 426);
            this.xtraTabPageAdHoc.Text = "Pivot Data";
            // 
            // gridControlPivotData
            // 
            this.gridControlPivotData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlPivotData.Location = new System.Drawing.Point(0, 0);
            this.gridControlPivotData.MainView = this.gridViewPivotData;
            this.gridControlPivotData.Name = "gridControlPivotData";
            this.gridControlPivotData.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboBox2});
            this.gridControlPivotData.Size = new System.Drawing.Size(836, 426);
            this.gridControlPivotData.TabIndex = 1;
            this.gridControlPivotData.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewPivotData,
            this.gridView6});
            // 
            // gridViewPivotData
            // 
            this.gridViewPivotData.GridControl = this.gridControlPivotData;
            this.gridViewPivotData.Name = "gridViewPivotData";
            this.gridViewPivotData.OptionsBehavior.AllowAddRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPivotData.OptionsBehavior.AllowDeleteRows = DevExpress.Utils.DefaultBoolean.False;
            this.gridViewPivotData.OptionsSelection.MultiSelect = true;
            this.gridViewPivotData.OptionsView.EnableAppearanceEvenRow = true;
            this.gridViewPivotData.OptionsView.EnableAppearanceOddRow = true;
            this.gridViewPivotData.OptionsView.ShowIndicator = false;
            this.gridViewPivotData.PopupMenuShowing += new DevExpress.XtraGrid.Views.Grid.PopupMenuShowingEventHandler(this.gridViewPivotData_PopupMenuShowing);
            this.gridViewPivotData.ShowingEditor += new System.ComponentModel.CancelEventHandler(this.gridViewPivotData_ShowingEditor);
            this.gridViewPivotData.CellValueChanged += new DevExpress.XtraGrid.Views.Base.CellValueChangedEventHandler(this.gridViewPivotData_CellValueChanged);
            this.gridViewPivotData.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.gridViewPivotData_CustomColumnSort);
            this.gridViewPivotData.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridViewPivotData_ValidatingEditor);
            this.gridViewPivotData.InvalidValueException += new DevExpress.XtraEditors.Controls.InvalidValueExceptionEventHandler(this.gridViewPivotData_InvalidValueException);
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // gridView6
            // 
            this.gridView6.GridControl = this.gridControlPivotData;
            this.gridView6.Name = "gridView6";
            // 
            // labelControl4
            // 
            this.labelControl4.Location = new System.Drawing.Point(487, 7);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(2, 13);
            this.labelControl4.StyleController = this.layoutControl5;
            this.labelControl4.TabIndex = 17;
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(307, 7);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(0, 13);
            this.labelControl2.StyleController = this.layoutControl5;
            this.labelControl2.TabIndex = 16;
            // 
            // comboBoxEditAlgorthim
            // 
            this.comboBoxEditAlgorthim.EditValue = "K-means++";
            this.comboBoxEditAlgorthim.Location = new System.Drawing.Point(128, 7);
            this.comboBoxEditAlgorthim.Name = "comboBoxEditAlgorthim";
            this.comboBoxEditAlgorthim.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.comboBoxEditAlgorthim.Properties.Items.AddRange(new object[] {
            "K-means++",
            "Lloyd\'s"});
            this.comboBoxEditAlgorthim.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.comboBoxEditAlgorthim.Size = new System.Drawing.Size(175, 20);
            this.comboBoxEditAlgorthim.StyleController = this.layoutControl5;
            this.comboBoxEditAlgorthim.TabIndex = 15;
            // 
            // spinEditNumIterations
            // 
            this.spinEditNumIterations.EditValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEditNumIterations.Location = new System.Drawing.Point(614, 7);
            this.spinEditNumIterations.Name = "spinEditNumIterations";
            this.spinEditNumIterations.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditNumIterations.Properties.IsFloatValue = false;
            this.spinEditNumIterations.Properties.Mask.EditMask = "N00";
            this.spinEditNumIterations.Properties.MaxValue = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.spinEditNumIterations.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditNumIterations.Size = new System.Drawing.Size(75, 20);
            this.spinEditNumIterations.StyleController = this.layoutControl5;
            this.spinEditNumIterations.TabIndex = 14;
            // 
            // spinEditClusters
            // 
            this.spinEditClusters.EditValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditClusters.Location = new System.Drawing.Point(432, 7);
            this.spinEditClusters.Name = "spinEditClusters";
            this.spinEditClusters.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton()});
            this.spinEditClusters.Properties.IsFloatValue = false;
            this.spinEditClusters.Properties.Mask.EditMask = "N00";
            this.spinEditClusters.Properties.MaxValue = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.spinEditClusters.Properties.MinValue = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.spinEditClusters.Size = new System.Drawing.Size(51, 20);
            this.spinEditClusters.StyleController = this.layoutControl5;
            this.spinEditClusters.TabIndex = 13;
            // 
            // btnGenerateClusters
            // 
            this.btnGenerateClusters.Location = new System.Drawing.Point(751, 7);
            this.btnGenerateClusters.Name = "btnGenerateClusters";
            this.btnGenerateClusters.Size = new System.Drawing.Size(98, 22);
            this.btnGenerateClusters.StyleController = this.layoutControl5;
            this.btnGenerateClusters.TabIndex = 9;
            this.btnGenerateClusters.Text = "Generate Clusters";
            this.btnGenerateClusters.Click += new System.EventHandler(this.btnGenerateClusters_Click);
            // 
            // layoutControlGroup7
            // 
            this.layoutControlGroup7.CustomizationFormText = "Root";
            this.layoutControlGroup7.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup7.GroupBordersVisible = false;
            this.layoutControlGroup7.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.emptySpaceItem1,
            this.layoutControlItem23,
            this.layoutControlItem27,
            this.layoutControlItem28,
            this.layoutControlItem22,
            this.layoutControlItem21,
            this.layoutControlItem29,
            this.layoutControlItem12});
            this.layoutControlGroup7.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup7.Name = "Root";
            this.layoutControlGroup7.Padding = new DevExpress.XtraLayout.Utils.Padding(5, 5, 5, 5);
            this.layoutControlGroup7.Size = new System.Drawing.Size(856, 494);
            this.layoutControlGroup7.Text = "Root";
            this.layoutControlGroup7.TextVisible = false;
            // 
            // emptySpaceItem1
            // 
            this.emptySpaceItem1.AllowHotTrack = false;
            this.emptySpaceItem1.CustomizationFormText = "emptySpaceItem1";
            this.emptySpaceItem1.Location = new System.Drawing.Point(686, 0);
            this.emptySpaceItem1.Name = "emptySpaceItem1";
            this.emptySpaceItem1.Size = new System.Drawing.Size(58, 26);
            this.emptySpaceItem1.Text = "emptySpaceItem1";
            this.emptySpaceItem1.TextSize = new System.Drawing.Size(0, 0);
            // 
            // layoutControlItem23
            // 
            this.layoutControlItem23.Control = this.btnGenerateClusters;
            this.layoutControlItem23.CustomizationFormText = "layoutControlItem23";
            this.layoutControlItem23.Location = new System.Drawing.Point(744, 0);
            this.layoutControlItem23.MaxSize = new System.Drawing.Size(115, 26);
            this.layoutControlItem23.MinSize = new System.Drawing.Size(102, 26);
            this.layoutControlItem23.Name = "layoutControlItem23";
            this.layoutControlItem23.Size = new System.Drawing.Size(102, 26);
            this.layoutControlItem23.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem23.Text = "layoutControlItem23";
            this.layoutControlItem23.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem23.TextToControlDistance = 0;
            this.layoutControlItem23.TextVisible = false;
            // 
            // layoutControlItem27
            // 
            this.layoutControlItem27.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem27.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem27.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem27.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem27.Control = this.spinEditClusters;
            this.layoutControlItem27.CustomizationFormText = "Clusters";
            this.layoutControlItem27.Location = new System.Drawing.Point(304, 0);
            this.layoutControlItem27.MaxSize = new System.Drawing.Size(185, 26);
            this.layoutControlItem27.MinSize = new System.Drawing.Size(176, 26);
            this.layoutControlItem27.Name = "layoutControlItem27";
            this.layoutControlItem27.Size = new System.Drawing.Size(176, 26);
            this.layoutControlItem27.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem27.Text = "Number of Clusters";
            this.layoutControlItem27.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem28
            // 
            this.layoutControlItem28.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem28.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem28.AppearanceItemCaption.Options.UseTextOptions = true;
            this.layoutControlItem28.AppearanceItemCaption.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Far;
            this.layoutControlItem28.Control = this.spinEditNumIterations;
            this.layoutControlItem28.CustomizationFormText = "Number of Iterations";
            this.layoutControlItem28.Location = new System.Drawing.Point(486, 0);
            this.layoutControlItem28.MaxSize = new System.Drawing.Size(200, 26);
            this.layoutControlItem28.MinSize = new System.Drawing.Size(200, 26);
            this.layoutControlItem28.Name = "layoutControlItem28";
            this.layoutControlItem28.Size = new System.Drawing.Size(200, 26);
            this.layoutControlItem28.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem28.Text = "Number of Iterations";
            this.layoutControlItem28.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem22
            // 
            this.layoutControlItem22.AppearanceItemCaption.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.layoutControlItem22.AppearanceItemCaption.Options.UseFont = true;
            this.layoutControlItem22.Control = this.comboBoxEditAlgorthim;
            this.layoutControlItem22.CustomizationFormText = "Cluster Algorithm";
            this.layoutControlItem22.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem22.MaxSize = new System.Drawing.Size(300, 26);
            this.layoutControlItem22.MinSize = new System.Drawing.Size(300, 26);
            this.layoutControlItem22.Name = "layoutControlItem22";
            this.layoutControlItem22.Size = new System.Drawing.Size(300, 26);
            this.layoutControlItem22.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layoutControlItem22.Text = "Cluster Algorithm";
            this.layoutControlItem22.TextLocation = DevExpress.Utils.Locations.Default;
            this.layoutControlItem22.TextSize = new System.Drawing.Size(118, 13);
            // 
            // layoutControlItem21
            // 
            this.layoutControlItem21.Control = this.labelControl2;
            this.layoutControlItem21.CustomizationFormText = "0";
            this.layoutControlItem21.Location = new System.Drawing.Point(300, 0);
            this.layoutControlItem21.Name = "layoutControlItem21";
            this.layoutControlItem21.Size = new System.Drawing.Size(4, 26);
            this.layoutControlItem21.Text = "0";
            this.layoutControlItem21.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem21.TextToControlDistance = 0;
            this.layoutControlItem21.TextVisible = false;
            // 
            // layoutControlItem29
            // 
            this.layoutControlItem29.Control = this.labelControl4;
            this.layoutControlItem29.CustomizationFormText = "layoutControlItem29";
            this.layoutControlItem29.Location = new System.Drawing.Point(480, 0);
            this.layoutControlItem29.Name = "layoutControlItem29";
            this.layoutControlItem29.Size = new System.Drawing.Size(6, 26);
            this.layoutControlItem29.Text = "layoutControlItem29";
            this.layoutControlItem29.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem29.TextToControlDistance = 0;
            this.layoutControlItem29.TextVisible = false;
            // 
            // layoutControlItem12
            // 
            this.layoutControlItem12.Control = this.xtraTabControlCluster;
            this.layoutControlItem12.CustomizationFormText = "layoutControlItem12";
            this.layoutControlItem12.Location = new System.Drawing.Point(0, 26);
            this.layoutControlItem12.Name = "layoutControlItem12";
            this.layoutControlItem12.Size = new System.Drawing.Size(846, 458);
            this.layoutControlItem12.Text = "layoutControlItem12";
            this.layoutControlItem12.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem12.TextToControlDistance = 0;
            this.layoutControlItem12.TextVisible = false;
            // 
            // simpleButton2
            // 
            this.simpleButton2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.simpleButton2.Location = new System.Drawing.Point(735, 12);
            this.simpleButton2.Name = "simpleButton2";
            this.simpleButton2.Size = new System.Drawing.Size(186, 22);
            this.simpleButton2.StyleController = this.layoutControl1;
            this.simpleButton2.TabIndex = 10;
            this.simpleButton2.Text = "Cancel";
            // 
            // panelControl1
            // 
            this.panelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl1.Controls.Add(this.labelControl1);
            this.panelControl1.Controls.Add(this.txtClusterLabel);
            this.panelControl1.Location = new System.Drawing.Point(12, 12);
            this.panelControl1.Name = "panelControl1";
            this.panelControl1.Size = new System.Drawing.Size(841, 80);
            this.panelControl1.TabIndex = 4;
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.labelControl1.Location = new System.Drawing.Point(12, 20);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(100, 13);
            this.labelControl1.TabIndex = 8;
            this.labelControl1.Text = "Assortment Label";
            // 
            // txtClusterLabel
            // 
            this.txtClusterLabel.Location = new System.Drawing.Point(118, 17);
            this.txtClusterLabel.Name = "txtClusterLabel";
            this.txtClusterLabel.Size = new System.Drawing.Size(253, 20);
            this.txtClusterLabel.TabIndex = 7;
            conditionValidationRule1.ConditionOperator = DevExpress.XtraEditors.DXErrorProvider.ConditionOperator.IsNotBlank;
            conditionValidationRule1.ErrorText = "The label text is not valid";
            conditionValidationRule1.ErrorType = DevExpress.XtraEditors.DXErrorProvider.ErrorType.Warning;
            this.dxValidationProvider1.SetValidationRule(this.txtClusterLabel, conditionValidationRule1);
            this.txtClusterLabel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtClusterLabel_KeyPress);
            // 
            // panelControl3
            // 
            this.panelControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder;
            this.panelControl3.Controls.Add(this.btnCancel);
            this.panelControl3.Controls.Add(this.btnSave);
            this.panelControl3.Location = new System.Drawing.Point(857, 12);
            this.panelControl3.Name = "panelControl3";
            this.panelControl3.Size = new System.Drawing.Size(113, 80);
            this.panelControl3.TabIndex = 11;
            // 
            // btnCancel
            // 
            this.btnCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnCancel.Location = new System.Drawing.Point(12, 40);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(79, 22);
            this.btnCancel.TabIndex = 15;
            this.btnCancel.Text = "Cancel";
            // 
            // btnSave
            // 
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(12, 3);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(79, 22);
            this.btnSave.TabIndex = 14;
            this.btnSave.Text = "Save";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton2;
            this.layoutControlItem3.CustomizationFormText = "layoutControlItem3";
            this.layoutControlItem3.Location = new System.Drawing.Point(723, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(190, 48);
            this.layoutControlItem3.Text = "layoutControlItem3";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextToControlDistance = 0;
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlGroup1
            // 
            this.layoutControlGroup1.CustomizationFormText = "Root";
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layWizButtons});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "Root";
            this.layoutControlGroup1.Size = new System.Drawing.Size(982, 591);
            this.layoutControlGroup1.Text = "Root";
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.panelControl1;
            this.layoutControlItem1.CustomizationFormText = "layoutControlItem1";
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(845, 84);
            this.layoutControlItem1.Text = "layoutControlItem1";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem1.TextToControlDistance = 0;
            this.layoutControlItem1.TextVisible = false;
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.panelControl2;
            this.layoutControlItem2.CustomizationFormText = "layoutControlItem2";
            this.layoutControlItem2.Location = new System.Drawing.Point(0, 84);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(962, 487);
            this.layoutControlItem2.Text = "layoutControlItem2";
            this.layoutControlItem2.TextLocation = DevExpress.Utils.Locations.Right;
            this.layoutControlItem2.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem2.TextToControlDistance = 0;
            this.layoutControlItem2.TextVisible = false;
            // 
            // layWizButtons
            // 
            this.layWizButtons.Control = this.panelControl3;
            this.layWizButtons.CustomizationFormText = "layWizButtons";
            this.layWizButtons.Location = new System.Drawing.Point(845, 0);
            this.layWizButtons.MaxSize = new System.Drawing.Size(117, 84);
            this.layWizButtons.MinSize = new System.Drawing.Size(117, 84);
            this.layWizButtons.Name = "layWizButtons";
            this.layWizButtons.Size = new System.Drawing.Size(117, 84);
            this.layWizButtons.SizeConstraintsType = DevExpress.XtraLayout.SizeConstraintsType.Custom;
            this.layWizButtons.Text = "layWizButtons";
            this.layWizButtons.TextSize = new System.Drawing.Size(0, 0);
            this.layWizButtons.TextToControlDistance = 0;
            this.layWizButtons.TextVisible = false;
            // 
            // layoutControlGroup5
            // 
            this.layoutControlGroup5.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup5.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutControlGroup5.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup5.Name = "layoutControlGroup2";
            this.layoutControlGroup5.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup5.Size = new System.Drawing.Size(583, 50);
            this.layoutControlGroup5.Text = "layoutControlGroup2";
            this.layoutControlGroup5.TextLocation = DevExpress.Utils.Locations.Left;
            this.layoutControlGroup5.TextVisible = false;
            // 
            // layoutControlGroup6
            // 
            this.layoutControlGroup6.CustomizationFormText = "layoutControlGroup2";
            this.layoutControlGroup6.DefaultLayoutType = DevExpress.XtraLayout.Utils.LayoutType.Horizontal;
            this.layoutControlGroup6.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup6.Name = "layoutControlGroup6";
            this.layoutControlGroup6.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup6.Size = new System.Drawing.Size(173, 55);
            this.layoutControlGroup6.Text = "layoutControlGroup6";
            // 
            // layoutControlGroup9
            // 
            this.layoutControlGroup9.CustomizationFormText = "layoutControlGroup9";
            this.layoutControlGroup9.Location = new System.Drawing.Point(300, 0);
            this.layoutControlGroup9.Name = "layoutControlGroup9";
            this.layoutControlGroup9.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup9.Size = new System.Drawing.Size(486, 457);
            this.layoutControlGroup9.Text = "layoutControlGroup9";
            // 
            // layoutControlGroup10
            // 
            this.layoutControlGroup10.CustomizationFormText = "layoutControlGroup10";
            this.layoutControlGroup10.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup10.Name = "layoutControlGroup10";
            this.layoutControlGroup10.OptionsItemText.TextToControlDistance = 5;
            this.layoutControlGroup10.Size = new System.Drawing.Size(933, 591);
            this.layoutControlGroup10.Text = "layoutControlGroup10";
            // 
            // popupMenu
            // 
            this.popupMenu.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(DevExpress.XtraBars.BarLinkUserDefines.PaintStyle, this.barItemMoveTo, DevExpress.XtraBars.BarItemPaintStyle.Caption),
            new DevExpress.XtraBars.LinkPersistInfo(this.barRenameCluster)});
            this.popupMenu.Manager = this.barManager;
            this.popupMenu.Name = "popupMenu";
            // 
            // popupMenuNodes
            // 
            this.popupMenuNodes.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemNumeric),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemNumeric4),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemPercent, true),
            new DevExpress.XtraBars.LinkPersistInfo(this.barCheckItemPercent4)});
            this.popupMenuNodes.Manager = this.barManager;
            this.popupMenuNodes.Name = "popupMenuNodes";
            this.popupMenuNodes.BeforePopup += new System.ComponentModel.CancelEventHandler(this.popupMenuNodes_BeforePopup);
            // 
            // emptySpaceItem3
            // 
            this.emptySpaceItem3.AllowHotTrack = false;
            this.emptySpaceItem3.CustomizationFormText = "emptySpaceItem3";
            this.emptySpaceItem3.Location = new System.Drawing.Point(0, 412);
            this.emptySpaceItem3.Name = "emptySpaceItem3";
            this.emptySpaceItem3.Size = new System.Drawing.Size(783, 41);
            this.emptySpaceItem3.Text = "emptySpaceItem3";
            this.emptySpaceItem3.TextSize = new System.Drawing.Size(0, 0);
            // 
            // dxValidationProvider1
            // 
            this.dxValidationProvider1.ValidateHiddenControls = false;
            // 
            // frmCreateAsst
            // 
            this.AcceptButton = this.btnSave;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnCancel;
            this.ClientSize = new System.Drawing.Size(982, 591);
            this.Controls.Add(this.layoutControl1);
            this.Controls.Add(this.barDockControlLeft);
            this.Controls.Add(this.barDockControlRight);
            this.Controls.Add(this.barDockControlBottom);
            this.Controls.Add(this.barDockControlTop);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.MinimizeBox = false;
            this.Name = "frmCreateAsst";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Create Clustering Assortment";
            ((System.ComponentModel.ISupportInitialize)(this.gridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridProductResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProductResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.panelControl2)).EndInit();
            this.panelControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlClusterAlgorthim)).EndInit();
            this.xtraTabControlClusterAlgorthim.ResumeLayout(false);
            this.tabTime.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl6)).EndInit();
            this.layoutControl6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditTime.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.timeTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlSavedTime)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem26)).EndInit();
            this.tabProducts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl2)).EndInit();
            this.layoutControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlProducts)).EndInit();
            this.xtraTabControlProducts.ResumeLayout(false);
            this.tabProdProducts.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prodTreeList)).EndInit();
            this.tabProdAttr.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.prodAttTreeList)).EndInit();
            this.tabProdResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditProductLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditProducts.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemProducts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            this.tabLocations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlLocations)).EndInit();
            this.layoutControlLocations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinEditLocationLevel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlLocations)).EndInit();
            this.xtraTabControlLocations.ResumeLayout(false);
            this.tabLocLocations.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.locTreeList)).EndInit();
            this.tabLocAttr.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.locAttTreeList)).EndInit();
            this.tabLocResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLocationResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLocationResults)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditLocations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.lcgLocations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLocationBlank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLocations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemLocationLabel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemSavedLocations)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem15)).EndInit();
            this.tabMeasures.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl4)).EndInit();
            this.layoutControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditRuleSet.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.barManager)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditCustomCluster)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEditClusterName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.measuresTreeList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditMeasures.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemRuleSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItemMeasuresSpace)).EndInit();
            this.tabCluster.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl5)).EndInit();
            this.layoutControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlCluster)).EndInit();
            this.xtraTabControlCluster.ResumeLayout(false);
            this.xtraTabPagePivot.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridCluster)).EndInit();
            this.xtraTabPageAdHoc.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlPivotData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewPivotData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.comboBoxEditAlgorthim.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditNumIterations.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinEditClusters.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl1)).EndInit();
            this.panelControl1.ResumeLayout(false);
            this.panelControl1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtClusterLabel.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.panelControl3)).EndInit();
            this.panelControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layWizButtons)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.popupMenuNodes)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.emptySpaceItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dxValidationProvider1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraEditors.PanelControl panelControl1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.PanelControl panelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlClusterAlgorthim;
        private DevExpress.XtraTab.XtraTabPage tabProducts;
        private DevExpress.XtraEditors.SimpleButton simpleButton2;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.TextEdit txtClusterLabel;
        private DevExpress.XtraTab.XtraTabPage tabCluster;
        private DevExpress.XtraTab.XtraTabPage tabMeasures;
        private DevExpress.XtraEditors.SimpleButton btnGenerateClusters;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraEditors.PanelControl panelControl3;
        private DevExpress.XtraEditors.SimpleButton btnCancel;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraLayout.LayoutControlItem layWizButtons;
        private DevExpress.XtraLayout.LayoutControl layoutControl2;
        private DevExpress.XtraLayout.LayoutControlGroup lcgProducts;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlProducts;
        private DevExpress.XtraTab.XtraTabPage tabProdAttr;
        private DevExpress.XtraTreeList.TreeList prodAttTreeList;
        private DevExpress.XtraTab.XtraTabPage tabProdProducts;
        private DevExpress.XtraTreeList.TreeList prodTreeList;
        private DevExpress.XtraTab.XtraTabPage tabProdResults;
        private DevExpress.XtraGrid.GridControl gridProductResults;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.SimpleButton btnProductsSave;
        private DevExpress.XtraEditors.LabelControl labelControlSavedProducts;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditProducts;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem9;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem8;
        private DevExpress.XtraTab.XtraTabPage tabLocations;
        private DevExpress.XtraLayout.LayoutControl layoutControlLocations;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlLocations;
        private DevExpress.XtraTab.XtraTabPage tabLocAttr;
        private DevExpress.XtraTreeList.TreeList locAttTreeList;
        private DevExpress.XtraTab.XtraTabPage tabLocLocations;
        private DevExpress.XtraTreeList.TreeList locTreeList;
        private DevExpress.XtraTab.XtraTabPage tabLocResults;
        private DevExpress.XtraGrid.GridControl gridLocationResults;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLocationResults;
        private DevExpress.XtraLayout.LayoutControlGroup lcgLocations;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem10;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup3;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLocationBlank;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup2;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup6;
        private DevExpress.XtraEditors.SimpleButton btnLocationsSave;
        private DevExpress.XtraEditors.LabelControl labelSavedLocations;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditLocations;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLocationLabel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemSavedLocations;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem15;
        private DevExpress.XtraLayout.LayoutControl layoutControl4;
        private DevExpress.XtraTreeList.TreeList measuresTreeList;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup4;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem16;
        private DevExpress.XtraEditors.SimpleButton btnMeasuresSave;
        private DevExpress.XtraEditors.LabelControl labelSavedMeasures;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditMeasures;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem17;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem18;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem19;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView3;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView4;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn7;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn8;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn9;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProductResults;
        private DevExpress.XtraLayout.LayoutControl layoutControl5;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup7;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem23;
        private DevExpress.XtraTab.XtraTabPage tabTime;
        private DevExpress.XtraLayout.LayoutControl layoutControl6;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup8;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditTime;
        private DevExpress.XtraTreeList.TreeList timeTreeList;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlSavedTime;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem25;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup9;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup10;
        private DevExpress.XtraEditors.SimpleButton btnTimeSave;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem26;
        private DevExpress.XtraEditors.SpinEdit spinEditClusters;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem27;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditAlgorthim;
        private DevExpress.XtraEditors.SpinEdit spinEditNumIterations;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem28;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem22;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem21;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem29;
        private DevExpress.XtraEditors.LabelControl labelControlProdBlank;
        private DevExpress.XtraEditors.SpinEdit spinEditProductLevel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemProducts;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.SpinEdit spinEditLocationLevel;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemLocations;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlCluster;
        private DevExpress.XtraTab.XtraTabPage xtraTabPagePivot;
        private DevExpress.XtraPivotGrid.PivotGridControl pivotGridCluster;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageAdHoc;
        private DevExpress.XtraGrid.GridControl gridControlPivotData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewPivotData;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView6;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem12;
        private DevExpress.XtraBars.BarManager barManager;
        private DevExpress.XtraBars.BarDockControl barDockControlTop;
        private DevExpress.XtraBars.BarDockControl barDockControlBottom;
        private DevExpress.XtraBars.BarDockControl barDockControlLeft;
        private DevExpress.XtraBars.BarDockControl barDockControlRight;
        private DevExpress.XtraBars.PopupMenu popupMenu;
        private DevExpress.XtraBars.BarSubItem barItemMoveTo;
        private DevExpress.XtraBars.BarCheckItem barCheckItem1;
        private DevExpress.XtraBars.BarEditItem barEditItemCustomCluster;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditCustomCluster;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.Utils.ToolTipController memberTooltipController;
        private DevExpress.XtraBars.BarSubItem barRenameCluster;
        private DevExpress.XtraBars.BarEditItem barEditClusterName;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEditClusterName;
        private DevExpress.XtraBars.PopupMenu popupMenuNodes;
        private DevExpress.XtraBars.BarCheckItem barCheckItemPercent;
        private DevExpress.XtraBars.BarCheckItem barCheckItemNumeric;
        private DevExpress.XtraEditors.ComboBoxEdit comboBoxEditRuleSet;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemRuleSet;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem3;
        private DevExpress.XtraLayout.EmptySpaceItem emptySpaceItem2;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItemMeasuresSpace;
        private DevExpress.XtraEditors.DXErrorProvider.DXValidationProvider dxValidationProvider1;
        private DevExpress.XtraBars.BarCheckItem barCheckItemNumeric4;
        private DevExpress.XtraBars.BarCheckItem barCheckItemPercent4;

    }
}