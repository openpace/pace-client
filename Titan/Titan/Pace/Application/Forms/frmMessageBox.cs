#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Diagnostics;
using System.Drawing;
using System.Media;
using System.Threading;
using System.Windows.Forms;
using Titan.Properties;

namespace Titan.Pace.Application.Forms
{
    internal partial class frmMessageBox : Form
    {
        private const int PAD = 1;
        private const int PANEL_PAD = 5;
        private readonly int origHeight;
        private readonly int origWidth;

        public enum frmMessageBoxButtons
        {
            OK,
            OK_CANCEL,
            YES_NO_CANCEL
        }

        public frmMessageBox()
        {
            InitializeComponent();
            origWidth = Width;
            origHeight = Height;
        }
        private void MessageBox_Load(object sender, EventArgs e)
        {
            subpane.Collapse();
            Thread.CurrentThread.CurrentUICulture = PafApp.GetLocalization().GetCurrentCulture();
            cmdOk.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
            cmdOk2.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.OK");
            cmdCancel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
            cmdCancel2.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Cancel");
            cmdNo.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.No");
            cmdYes.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Button.Yes");
            Icon = (Icon) Resources.PIcon;
        }

        /// <summary>
        /// Shows the message box.
        /// </summary>
        /// <param name="ex">An Exception</param>
        public DialogResult Show(Exception ex)
        {
            return Show(ex, null, frmMessageBoxButtons.OK);    
        }

        /// <summary>
        /// Shows the message box.
        /// </summary>
        /// <param name="message">
        /// The message to display in the message section of the box.
        /// </param>
        /// <param name="source">
        /// The source of the error message, displayed on the caption of the messagebox.
        /// </param>
        public DialogResult Show(string message, string source)
        {
            return Show(message, source, null, null, frmMessageBoxButtons.OK);
        }

        /// <summary>
        /// Shows the message box.
        /// </summary>
        /// <param name="message">
        /// The message to display in the message section of the box.
        /// </param>
        /// <param name="source">
        /// The source of the error message, displayed on the caption of the messagebox.
        /// </param>
        /// <param name="stackTrace">String representation of the stack trace.</param>
        public DialogResult Show(string message, string source, string stackTrace)
        {
            return Show(message, source, stackTrace, null, frmMessageBoxButtons.OK);
        }

        /// <summary>
        /// Shows the message box.
        /// </summary>
        /// <param name="message">
        /// The message to display in the message section of the box.
        /// </param>
        /// <param name="caption">
        /// The caption to display on the heading of the message box.
        /// </param>
        /// <param name="displayIcon">
        /// The icon to be displayed on the left hand side of the messagebox.
        /// </param>
        /// <param name="showAdvanced">
        /// Show the advanced tab expanded or collapsed.
        /// </param>
        /// <param name="userControl">
        /// The user control to display in the collapsable box.
        /// </param>
        /// <param name="enableYesOrOkButtons">
        /// true to enable the ok, or yes buttons.
        /// </param>
        /// <param name="enableNoButton">
        /// true to enable the no button.
        /// </param>
        /// <param name="buttons">
        /// The message box buttons to display.
        /// </param>
        public DialogResult Show(string message, string caption, Icon displayIcon,
            bool showAdvanced, Control userControl, bool enableYesOrOkButtons,
            bool enableNoButton, frmMessageBoxButtons buttons)
        {
            NativeWindow mainWindow = new NativeWindow();
            try
            {
                mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

                lblMessage.Text = message;
                Text = caption;
                subpane.Visible = true;
                txtStackTrace.Visible = false;

                if (userControl != null)
                {
                    ConfigureSize(userControl.Width, origHeight);
                    userControl.Dock = DockStyle.Fill;
                    subpane.Controls.Add(userControl);
                    ConfigureSize(userControl.Width, origHeight);
                    ConfigureButtons(buttons, enableYesOrOkButtons, enableNoButton);
                }

                if (displayIcon == null)
                {
                    picIcon.Image = SystemIcons.Error.ToBitmap();
                }
                else
                {
                    picIcon.Image = displayIcon.ToBitmap();
                }

                if (showAdvanced)
                {
                    subpane.Expand();
                }
                else
                {
                    subpane.Collapse();
                }


                SystemSounds.Exclamation.Play();

                //Added for automated regression testing.
                if (!PafApp.TestHarnessRunning)
                {
                    return ShowDialog();
                }
                else
                {
                    return DialogResult.None;
                }
            }
            finally
            {
                subpane.Controls.Remove(userControl);
                mainWindow.ReleaseHandle();
            }
        }


        /// <summary>
        /// Shows the message box.
        /// </summary>
        /// <param name="message">
        /// The message to display in the message section of the box.
        /// </param>
        /// <param name="caption">
        /// The caption to display on the heading of the message box.
        /// </param>
        /// <param name="stackTrace">
        /// The stack trace of the error, displayed in the collapsable box.
        /// </param>
        /// <param name="displayIcon">
        /// The icon to be displayed on the left hand side of the messagebox.
        /// </param>
        /// <param name="buttons">
        /// The message box buttons to display.
        /// </param>
        public DialogResult Show(string message, string caption, string stackTrace, 
            Icon displayIcon, frmMessageBoxButtons buttons)
        {
            NativeWindow mainWindow = new NativeWindow();
            try
            {
                mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

                ResetButtons();

                lblMessage.Text = message;
                Text = caption;
                
                if (String.IsNullOrEmpty(stackTrace))
                {
                    subpane.Visible = false;
                    txtStackTrace.Visible = false;
                }
                else
                {
                    subpane.Visible = true;
                    txtStackTrace.Visible = true;
                }

                if (txtStackTrace != null)
                {
                    txtStackTrace.Text = stackTrace;
                }

                if (displayIcon == null)
                {
                    picIcon.Image = SystemIcons.Error.ToBitmap();
                }
                else
                {
                    picIcon.Image = displayIcon.ToBitmap();
                }

                ConfigureSize(origWidth, origHeight);
                ConfigureButtons(buttons, true, true);

                SystemSounds.Exclamation.Play();

                subpane.Expand();
                subpane.Collapse();

                PafApp.GetLogger().Error(message, new Exception(message, new Exception(stackTrace)));

                //Added for automated regression testing.
                if (!PafApp.TestHarnessRunning)
                {
                    return ShowDialog();
                }
                else
                {
                    return DialogResult.None;
                }
            }
            finally
            {
                mainWindow.ReleaseHandle();
            }
        }


        /// <summary>
        /// Shows the message box.
        /// </summary>
        /// <param name="ex">
        /// An System.Exception
        /// </param>
        /// <param name="displayIcon">
        /// The icon to be displayed on the left hand side of the messagebox.
        /// </param>
        /// <param name="buttons">
        /// The message box buttons to display.
        /// </param>
        public DialogResult Show(Exception ex, Icon displayIcon, frmMessageBoxButtons buttons)
        {
            NativeWindow mainWindow = new NativeWindow();
            try
            {
                mainWindow.AssignHandle(Process.GetCurrentProcess().MainWindowHandle);

                lblMessage.Text = ex.Message;
                
                ResetButtons();

                if (ex == null)
                {
                    subpane.Visible = false;
                    txtStackTrace.Visible = false;
                }
                else
                {
                    subpane.Visible = true;
                    Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");
                    txtStackTrace.Visible = true;
                    txtStackTrace.Text = ex.StackTrace;
                }

                if (displayIcon == null)
                {
                    picIcon.Image = SystemIcons.Error.ToBitmap();
                }
                else
                {
                    picIcon.Image = displayIcon.ToBitmap();
                }

                ConfigureSize(origWidth, origHeight);
                ConfigureButtons(buttons, true, true);

                SystemSounds.Exclamation.Play();

                subpane.Expand();
                subpane.Collapse();

                SystemSounds.Exclamation.Play();

                PafApp.GetLogger().Error(ex.Message, ex);

                //Added for automated regression testing.
                if (!PafApp.TestHarnessRunning)
                {
                    return ShowDialog(mainWindow);
                }
                else
                {
                    return DialogResult.None;
                }
            }
            finally
            {
                mainWindow.ReleaseHandle();
            }
        }

        /// <summary>
        /// Configures the size of the form.
        /// </summary>
        /// <param name="width">width of the form</param>
        /// <param name="height">height of the form</param>
        private void ConfigureSize(int width, int height)
        {
            ClientSize = new Size(width, height);

            txtStackTrace.Width = width - PANEL_PAD;
            subpane.Width = width - PANEL_PAD;
            ClientSize = new Size(width, height);

            Graphics graphics = CreateGraphics();
           float dpiX = graphics.DpiX;
           float dpiY = graphics.DpiY;
             if (dpiX >= 144 && dpiY >= 144)
             {
                 subpane.Height = 200;
                 subpane.DefaultExpandedHeight = 250;
             }
             else if (dpiX >= 120 && dpiY >= 120)
             {
                 subpane.Height = 138;
                 subpane.DefaultExpandedHeight = 140;
             }
             else
             {
                 subpane.Height = 115;
                 subpane.DefaultExpandedHeight = 120;
             }
        }

        ///// <summary>
        ///// Configure the position and visibility of the ok and cancel buttons.
        ///// </summary>
        ///// <param name="hideCancelButton">Show or hide the cancel button.</param>
        //private void ConfigureButtons(bool hideCancelButton)
        //{
        //    if (hideCancelButton)
        //    {
        //        //show the cancel button
        //        cmdCancel.Visible = true;
        //        //set the tops of the ok and cancel buttons to be ==
        //        cmdCancel.Top = cmdOk.Top;
        //        cmdCancel.Left = cmdOk.Right + PAD;
        //        //get the center of the message box.
        //        int center = ClientSize.Width  / 2;
        //        //center the cancel and ok buttons.
        //        cmdCancel.Left = center + (PAD / 2);
        //        cmdOk.Left = center - cmdOk.Width - (PAD / 2);
        //    }
        //    else
        //    {
        //        //hide the cancel button
        //        cmdCancel.Visible = false;
        //        //get the center of the message box
        //        int center = ClientSize.Width / 2;
        //        //center the ok button.
        //        cmdOk.Left = center - (cmdOk.Width / 2);
        //    }
        //}

        /// <summary>
        /// Reposition the buttons when the advance tab expands or contracts.
        /// </summary>
        private void RepositionButtons()
        {
            int center = ClientSize.Width / 2;
            int top = subpane.Top - panelOk.Height - PAD;

            panelOk.Left = center - (panelOk.Width / 2);
            panelOk.Top = top;

            panelOkCancel.Left = center - (panelOkCancel.Width / 2);
            panelOkCancel.Top = top;

            panelYesNo.Left = center - (panelYesNo.Width / 2);
            panelYesNo.Top = top;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="messageBoxButtons"></param>
        /// <param name="enableOkYesButton"></param>
        /// <param name="enableNoButton"></param>
        private void ConfigureButtons(frmMessageBoxButtons messageBoxButtons, bool enableOkYesButton,
            bool enableNoButton)
        {
            int center = ClientSize.Width / 2;
            int top = subpane.Top - panelOk.Height - PAD;
            
            cmdNo.Enabled = enableNoButton;
            cmdYes.Enabled = enableOkYesButton;
            cmdOk.Enabled = enableOkYesButton;
            cmdOk2.Enabled = enableOkYesButton;


            switch(messageBoxButtons)
            {
                case frmMessageBoxButtons.OK:
                    panelOk.Visible = true;
                    panelOk.Left = center - (panelOk.Width / 2);
                    panelOk.Top = top;
                    panelOkCancel.Visible = false;
                    panelYesNo.Visible = false;
                    break;
                case frmMessageBoxButtons.OK_CANCEL:
                    panelOk.Visible = false;
                    panelOkCancel.Left = center - (panelOkCancel.Width / 2);
                    panelOkCancel.Top = top;
                    panelOkCancel.Visible = true;
                    panelYesNo.Visible = false;
                    break;
                case frmMessageBoxButtons.YES_NO_CANCEL:
                    panelOk.Visible = false;
                    panelOkCancel.Visible = false;
                    panelYesNo.Visible = true;
                    panelYesNo.Left = center - (panelYesNo.Width / 2);
                    panelYesNo.Top = top;
                    break;
            }
        }

        private void ResetButtons()
        {
            cmdNo.Enabled = true;
            cmdYes.Enabled = true;
            cmdOk.Enabled = true;
            cmdOk2.Enabled = true;
        }

        /// <summary>
        /// Event handler for the click event of the "OK" button.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdOk_Click(object sender, EventArgs e)
        {
            Hide();
        }

        /// <summary>
        /// Event handler for the AfterCollapse event of the subpane.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane1_AfterCollapse(object sender, EventArgs e)
        {
            if (txtStackTrace.Visible)
            {
                subpane.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ShowDetails");
            }
            else
            {
                subpane.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ShowAdvanced");
            }
            RepositionButtons();
        }

        /// <summary>
        /// Event handler for the AfterExpand event of the subpane.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void subpane1_AfterExpand(object sender, EventArgs e)
        {
            if (txtStackTrace.Visible)
            {
                subpane.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.HideDetails");
            }
            else
            {
                subpane.Text =
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.HideAdvanced");
            }
            RepositionButtons();
        }
    }
}