#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.Pace.ExcelGridView.Utility;
using Titan.Palladium.GridView;

namespace Titan.Pace.Application.Core
{
    /// <remarks/>
    public interface IPrintStyle 
    {
        /// <remarks/>
        bool AdjustTo { get; set; }
        /// <remarks/>
        bool AlignWithPageMargin { get; set; }
        /// <remarks/>
        bool BlackAndWhite { get; set; }
        /// <remarks/>
        float Bottom { get; set; }
        /// <remarks/>
        string CellErrorsAs { get; set; }
        /// <remarks/>
        bool CenterHorizontally { get; set; }
        /// <remarks/>
        bool CenterVertically { get; set; }
        /// <remarks/>
        string ColsToRepeatAtLeft { get; set; }
        /// <remarks/>
        string Comment { get; set; }
        /// <remarks/>
        bool DefaultStyle { get; set; }
        /// <remarks/>
        bool DiffFirstPage { get; set; }
        /// <remarks/>
        bool DiffOddAndEvenPages { get; set; }
        /// <remarks/>
        bool DownThenOver { get; set; }
        /// <remarks/>
        bool DraftQuality { get; set; }
        /// <remarks/>
        bool EntireView { get; set; }
        /// <remarks/>
        string FirstPageNumber { get; set; }
        /// <remarks/>
        bool FitTo { get; set; }
        /// <remarks/>
        float Footer { get; set; }
        /// <remarks/>
        string FooterText { get; set; }
        /// <remarks/>
        string GUID { get; set; }
        /// <remarks/>
        bool Gridlines { get; set; }
        /// <remarks/>
        float Header { get; set; }
        /// <remarks/>
        string HeaderText { get; set; }
        /// <remarks/>
        bool Landscape { get; set; }
        /// <remarks/>
        float Left { get; set; }
        /// <remarks/>
        string Name { get; set; }
        /// <remarks/>
        bool OverThenDown { get; set; }
        /// <remarks/>
        int PageTall { get; set; }
        /// <remarks/>
        int PageWide { get; set; }
        /// <remarks/>
        string PaperSize { get; set; }
        /// <remarks/>
        int PercentNormalSize { get; set; }
        /// <remarks/>
        bool Portrait { get; set; }
        /// <remarks/>
        float Right { get; set; }
        /// <remarks/>
        bool RowAndColHeadings { get; set; }
        /// <remarks/>
        string RowsToRepeatAtTop { get; set; }
        /// <remarks/>
        bool ScaleWithDocument { get; set; }
        /// <remarks/>
        float Top { get; set; }
        /// <remarks/>
        bool UserSelection { get; set; }
        /// <remarks/>
        string UserSelectionText { get; set; }
        /// <remarks/>
        ContiguousRange RowRange { get; set; }
        /// <remarks/>
        ContiguousRange ColumnRange { get; set; }
        
        /// <summary>
        /// Prints the print style to the .log4net debug log (if the log level is set).
        /// </summary>
        void DebugPrintPrintStyle();

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns></returns>
        IPrintStyle Clone();

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        bool Equals(IPrintStyle other);
    }
}