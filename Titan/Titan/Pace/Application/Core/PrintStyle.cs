﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using Titan.Pace.ExcelGridView.Utility;
using Titan.PafService;
using Titan.Palladium.GridView;

namespace Titan.Pace.Application.Core
{
    /// <remarks/>
    public class PrintStyle : IPrintStyle, IEquatable<IPrintStyle>, ICloneable
    {
        /// <remarks/>
        public bool AdjustTo { get; set; }
        /// <remarks/>
        public bool AlignWithPageMargin { get; set; }
        /// <remarks/>
        public bool BlackAndWhite { get; set; }
        /// <remarks/>
        public float Bottom { get; set; }
        /// <remarks/>
        public string CellErrorsAs { get; set; }
        /// <remarks/>
        public bool CenterHorizontally { get; set; }
        /// <remarks/>
        public bool CenterVertically { get; set; }
        /// <remarks/>
        public string ColsToRepeatAtLeft { get; set; }
        /// <remarks/>
        public string Comment { get; set; }
        /// <remarks/>
        public bool DefaultStyle { get; set; }
        /// <remarks/>
        public bool DiffFirstPage { get; set; }
        /// <remarks/>
        public bool DiffOddAndEvenPages { get; set; }
        /// <remarks/>
        public bool DownThenOver { get; set; }
        /// <remarks/>
        public bool DraftQuality { get; set; }
        /// <remarks/>
        public bool EntireView { get; set; }
        /// <remarks/>
        public string FirstPageNumber { get; set; }
        /// <remarks/>
        public bool FitTo { get; set; }
        /// <remarks/>
        public float Footer { get; set; }
        /// <remarks/>
        public string FooterText { get; set; }
        /// <remarks/>
        public string GUID { get; set; }
        /// <remarks/>
        public bool Gridlines { get; set; }
        /// <remarks/>
        public float Header { get; set; }
        /// <remarks/>
        public string HeaderText { get; set; }
        /// <remarks/>
        public bool Landscape { get; set; }
        /// <remarks/>
        public float Left { get; set; }
        /// <remarks/>
        public string Name { get; set; }
        /// <remarks/>
        public bool OverThenDown { get; set; }
        /// <remarks/>
        public int PageTall { get; set; }
        /// <remarks/>
        public int PageWide { get; set; }
        /// <remarks/>
        public string PaperSize { get; set; }
        /// <remarks/>
        public int PercentNormalSize { get; set; }
        /// <remarks/>
        public bool Portrait { get; set; }
        /// <remarks/>
        public float Right { get; set; }
        /// <remarks/>
        public bool RowAndColHeadings { get; set; }
        /// <remarks/>
        public string RowsToRepeatAtTop { get; set; }
        /// <remarks/>
        public bool ScaleWithDocument { get; set; }
        /// <remarks/>
        public float Top { get; set; }
        /// <remarks/>
        public bool UserSelection { get; set; }
        /// <remarks/>
        public string UserSelectionText { get; set; }
        /// <remarks/>
        public ContiguousRange RowRange { get; set; }
        /// <remarks/>
        public ContiguousRange ColumnRange { get; set; }


        /// <summary>
        /// Creates a PrintStyle from a Paf printStyle.
        /// </summary>
        /// <param name="style">a paf printStyle</param>
        /// <param name="rowRange">Row range to set in the print area.</param>
        /// <param name="columnRange">Column range to set in the print area.</param>
        public PrintStyle(printStyle style, ContiguousRange rowRange, ContiguousRange columnRange)
            : this(style)
        {
            RowRange = rowRange;
            ColumnRange = columnRange;
        }

        /// <summary>
        /// Creates a PrintStyle from a Paf printStyle.
        /// </summary>
        /// <param name="style">a paf printStyle</param>
        public PrintStyle(printStyle style)
        {
            AdjustTo = style.adjustTo;
            AlignWithPageMargin = style.alignWithPageMargin;
            BlackAndWhite = style.blackAndWhite;
            
            Bottom = style.bottom;
            CellErrorsAs = style.cellErrorsAs;
            CenterHorizontally = style.centerHorizontally;
            
            CenterVertically = style.centerVertically;
            ColsToRepeatAtLeft = style.colsToRepeatAtLeft;
            Comment = style.comment;
            
            DefaultStyle = style.defaultStyle;
            DiffFirstPage = style.diffFirstPage;
            DiffOddAndEvenPages = style.diffOddAndEvenPages;
            
            DownThenOver = style.downThenOver;
            DraftQuality = style.draftQuality;
            EntireView = style.entireView;
            
            FirstPageNumber = style.firstPageNumber;
            FitTo = style.fitTo;
            Footer = style.footer;
            
            FooterText = style.footerText;
            GUID = style.GUID;
            Gridlines = style.gridlines;
            
            Header = style.header;
            HeaderText = style.headerText;
            Landscape = style.landscape;
            
            Left = style.left;
            Name = style.name;
            OverThenDown = style.overThenDown;
            
            PageTall = style.pageTall;
            PageWide = style.pageWide;
            PaperSize = style.paperSize;
            
            PercentNormalSize = style.percentNormalSize;
            Portrait = style.portrait;
            Right = style.right;
            
            RowAndColHeadings = style.rowAndColHeadings;
            RowsToRepeatAtTop = style.rowsToRepeatAtTop;
            ScaleWithDocument = style.scaleWithDocument;
            
            Top = style.top;
            UserSelection = style.userSelection;
            UserSelectionText = style.userSelectionText;

        }

        /// <remarks/>
        public void DebugPrintPrintStyle()
        {
            if(!PafApp.GetLogger().IsDebugEnabled) return;

            try
            {
                PafApp.GetLogger().DebugFormat("Print Style (received from server) Name: {0}", new[] { Name });

                PafApp.GetLogger().DebugFormat("Print Style adjust to: {0}", new[] { AdjustTo.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style Align With Page Margin: {0}", new[] { AlignWithPageMargin.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style Black And White: {0}", new[] { BlackAndWhite.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style Bottom: {0}", new[] { Bottom.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style CellErrorsAs: {0}", new[] { CellErrorsAs });
                PafApp.GetLogger().DebugFormat("Print Style CenterHorizontally: {0}", new[] { CenterHorizontally.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style CenterVertically: {0}", new[] { CenterVertically.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style ColsToRepeatAtLeft: {0}", new[] { ColsToRepeatAtLeft });
                PafApp.GetLogger().DebugFormat("Print Style Comment: {0}", new[] { Comment });

                PafApp.GetLogger().DebugFormat("Print Style DefaultStyle: {0}", new[] { DefaultStyle.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style DiffFirstPage: {0}", new[] { DiffFirstPage.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style DiffOddAndEvenPages: {0}", new[] { DiffOddAndEvenPages.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style DownThenOver: {0}", new[] { DownThenOver.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style DraftQuality: {0}", new[] { DraftQuality.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style EntireView: {0}", new[] { EntireView.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style FirstPageNumber: {0}", new[] { FirstPageNumber });
                PafApp.GetLogger().DebugFormat("Print Style FitTo: {0}", new[] { FitTo.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style Footer: {0}", new[] { Footer.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style FooterText: {0}", new[] { FooterText });
                PafApp.GetLogger().DebugFormat("Print Style GUID: {0}", new[] { GUID });
                PafApp.GetLogger().DebugFormat("Print Style Gridlines: {0}", new[] { Gridlines.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style Header: {0}", new[] { Header.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style HeaderText: {0}", new[] { HeaderText });
                PafApp.GetLogger().DebugFormat("Print Style Landscape: {0}", new[] { Landscape.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style Left: {0}", new[] { Left.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style OverThenDown: {0}", new[] { OverThenDown.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style PageTall: {0}", new[] { PageTall.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style PageWide: {0}", new[] { PageWide.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style PaperSize: {0}", new[] { PaperSize });

                PafApp.GetLogger().DebugFormat("Print Style PercenNormalSize: {0}", new[] { PercentNormalSize.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style Protrait: {0}", new[] { Portrait.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style Right: {0}", new[] { Right.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style RowAndColHeadings : {0}", new[] { RowAndColHeadings.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style RowsToRepeatAtTop : {0}", new[] { RowsToRepeatAtTop });
                PafApp.GetLogger().DebugFormat("Print Style ScaleWithDocument : {0}", new[] { ScaleWithDocument.ToString() });

                PafApp.GetLogger().DebugFormat("Print Style Top: {0}", new[] { Top.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style UserSelection: {0}", new[] { UserSelection.ToString() });
                PafApp.GetLogger().DebugFormat("Print Style UserSelectionText: {0}", new[] { UserSelectionText });

            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns>
        /// A new object that is a copy of this instance.
        /// </returns>
        /// <filterpriority>2</filterpriority>
        object ICloneable.Clone()
        {
            return Clone();
        }

        /// <summary>
        /// Creates a new object that is a copy of the current instance.
        /// </summary>
        /// <returns></returns>
        public IPrintStyle Clone()
        {
            return (IPrintStyle)MemberwiseClone();
        }

        /// <summary>
        /// Indicates whether the current object is equal to another object of the same type.
        /// </summary>
        /// <returns>
        /// true if the current object is equal to the <paramref name="other"/> parameter; otherwise, false.
        /// </returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(IPrintStyle other)
        {
            Guid oG = new Guid(GUID);

            Guid otherGuid = new Guid(other.GUID);

            return oG.Equals(otherGuid);
        }
    }
}
