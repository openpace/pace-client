#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security;
using System.Threading;
using System.Web.Services.Protocols;
using System.Windows.Forms;
using System.Xml;
using DevExpress.XtraTreeList.Nodes;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Core;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Exceptions;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Application.Forms;
using Titan.Pace.Application.Utilities;
using Titan.Pace.Application.Win32;
using Titan.Pace.Base.Data;
using Titan.Pace.ExcelGridView;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.Rules;
using Titan.Pace.TestHarness.Startup;
using Titan.Pace.TestHarness.Xml;
using Titan.PafService;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Palladium.Rules;
using Titan.Properties;
using Range=Microsoft.Office.Interop.Excel.Range;
using TestHarnessStartupInformation=Titan.Pace.TestHarness.Startup.TestHarnessStartupInformation;

namespace Titan.Pace.Application
{
    /// <summary>
    /// 
    /// </summary>
    internal class ExcelEventMgr
    {
        #region Private Variables

        /// <summary>
        /// SHIFT + F9 Listner.
        /// </summary>
        private NativeWindowListener _hotKeyListener;

        /// <summary>
        /// Dictionary to hold the charts.
        /// </summary>
        private readonly Dictionary<string, Chart> _charts = new Dictionary<string, Chart>(1);

        /// <summary>
        /// Variable to hold reference to icon so it does not get GC'd.
        /// </summary>
        private System.Drawing.Icon _titleBarIcon;

        /// <summary>
        /// Class level cells change event delegate.
        /// </summary>
        private DocEvents_ChangeEventHandler _eventDelCellsChange;

        /// <summary>
        /// Last cell that was clicked on.
        /// </summary>
        private Range _lastCell;

        /// <summary>
        /// List to hold reference to buttons so they don't get GC'd.
        /// These are the buttons that will be disabled when the sheet is activated to "our" sheet.
        /// </summary>
        private readonly List<CommandBarControl> _buttonsToDisable = new List<CommandBarControl>();

        /// <summary>
        /// List to hold reference to cell note buttons so they don't get GC'd.
        /// These are the buttons that will be disabled when the sheet is activated to "our" sheet.
        /// </summary>
        private readonly List<CommandBarControl> _cellNoteButtonsToDisable = new List<CommandBarControl>();

        /// <summary>
        /// List to hold reference to so they don't get GC'd.
        /// These are the buttons that will be enable when the sheet is activated to "our" sheet.
        /// </summary>
        private readonly List<CommandBarControl> _hyperlinkMemberTagButtonsToEnable = new List<CommandBarControl>();

        /// <summary>
        /// List to hold reference to so they don't get GC'd.
        /// These are the buttons that will be enable when the sheet is activated to "our" sheet.
        /// </summary>
        private readonly List<CommandBarControl> _formulaMemberTagButtonsToEnable = new List<CommandBarControl>();

        /// <summary>
        /// List to hold reference to buttons so they don't get GC'd.
        /// These buttons are always disabled.
        /// </summary>
        private readonly List<CommandBarControl> _disabledButtons = new List<CommandBarControl>();

        /// <summary>
        /// Task pane on the menu bar.
        /// </summary>
        private CommandBar _taskPane;

        #endregion Private Variables

        #region Public Properties

        /// <summary>
        /// Gets the default system password.
        /// </summary>
        public string SheetPassword
        {
            get { return PafAppConstants.SHEET_PASSWORD; }
        }

        /// <summary>
        /// Task pane on the menu bar.
        /// </summary>
        public CommandBar TaskPaneCommandBar
        {
            get { return _taskPane; }
        }

        #endregion Public Properties

        #region Public Events

        public event ProtectionMngr.ProtectionMngr_UserLockedCell UserLockedCell;
        public event ProtectionMngr.ProtectionMngr_SessionLockedCell SessionLockedCell;
        public event ProtectionMngr.ProtectionMngr_AddChangedCell AddChangedCell;
        public event ProtectionMngr.ProtectionMngr_AddChangedCells AddChangedCells;
        

        #endregion Public Events

        #region Public Event Handlers

        /// <summary>
        /// Eventhandler for the toolbar, menubar, and rightclick combo boxes
        /// </summary>
        /// <param name="target"></param>
        public void DropDownEventHandler(CommandBarComboBox target)
        {
            DropDownEventHandler(target.Parameter, target.Text);
        }

        /// <summary>
        /// Eventhandler for the toolbar, menubar, and rightclick combo boxes
        /// </summary>
        /// <param name="parameter"></param>
        /// <param name="target"></param>
        public void DropDownEventHandler(string parameter, string target)
        {
            try
            {
                // get active sheet
                Worksheet sheet =
                    (Worksheet)
                    Globals.ThisWorkbook.Application.ActiveWorkbook.ActiveSheet;

                string viewName = "";

                //See if the sheet has a view on it.
                if (PafApp.GetGridApp().DoesSheetContainCustomProperty(
                    sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    //Get the name of the view associated with the sheet.
                    viewName = PafApp.GetGridApp().GetCustomProperty(
                        sheet, PafAppConstants.OUR_VIEW_PROP_VALUE).ToString();
                }

                //Get the status of the "New" command bar button.
                bool excelInEditMode = PafApp.GetGridApp().InEditMode();

                //If the button is disabled, then Excel is in edit mode, so exit out.
                if (excelInEditMode)
                {
                    PafApp.MessageBox().Show(
                        PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.MessageBox.InExcelEditMode"),
                        PafApp.GetLocalization().GetResourceManager().
                                    GetString("Application.Title"));
                    return;
                }

                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;

                //Save the current task pane state (per Carolyn)
                PafApp.GetGridApp().SaveTaskPaneCurrentStatus();

                switch (parameter)
                {
                    case "m_ChangeRuleSet":

                        //doen't change the ruleset if the user re-selects the existing ruleset
                        if (PafApp.RuleSetName == target)
                        {
                            break;
                        }

                        PafApp.RuleSetName = target;
                        PafApp.GetLogger().InfoFormat("Changed RuleSet to: {0}", PafApp.RuleSetName);
                        if (viewName != "")
                        {
                            RefreshView(viewName, sheet, false);
                        }
                        break;
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                //Reset menus
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;
            }
        }

        /// <summary>
        /// Eventhandler for the toolbar, menubar, and rightclick menu.
        /// </summary>
        /// <param name="ctrl">The CommandBarButton that was clicked.</param>
        /// <param name="cancelDefault">Boolean to cancel event.</param>
        public void CommandBarEventHandler(CommandBarButton ctrl, ref bool cancelDefault)
        {
            CommandBarEventHandler(ctrl.Parameter, ctrl.DescriptionText);
        }

        /// <summary>
        /// Eventhandler for the toolbar, menubar, and rightclick menu.
        /// </summary>
        /// <param name="parameter">The CommandBarButton parameter.</param>
        /// <param name="descriptionText">The CommandBarButton description text.</param>
        public void CommandBarEventHandler(string parameter, string descriptionText)
        {
            try
            {
                //Save the task pane status
                //PafApp.GetGridApp().SaveTaskPaneCurrentStatus();
                {
                    // get active sheet
                    Worksheet sheet =
                                (Worksheet)
                                Globals.ThisWorkbook.Application.ActiveWorkbook.ActiveSheet;

                    //make sure the user has not selected some embedded object!
                    if (!IsSelectionARange(Globals.ThisWorkbook.Application.Selection))
                    {
                        throw new Exception(PafApp.GetLocalization().GetResourceManager().
                                        GetString("Application.Exception.InvalidObjectSelected"));
                    }
                    //create a view name variable.
                    string viewName = String.Empty;
                    object obj = PafApp.GetGridApp().GetCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);
                    //Get the status of the "New" command bar button.
                    bool excelInEditMode = PafApp.GetGridApp().InEditMode();
                    //set the sheet hash.
                    string sheetHash = Convert.ToString(sheet.GetHashCode());
                    //if the worksheet is not ours then reuturn.
                    if (obj != null)
                    {
                        viewName = obj.ToString();
                    }
                    //If the button is disabled, then Excel is in edit mode, so exit out.
                    if (excelInEditMode)
                    {
                        PafApp.MessageBox().Show(
                            PafApp.GetLocalization().GetResourceManager().
                                GetString("Application.MessageBox.InExcelEditMode"),
                            PafApp.GetLocalization().GetResourceManager().
                                        GetString("Application.Title"));
                        return;
                    }
                    //Improves the behavior of the native Excel Group Sheets functionality.
                    //Only the active sheet is left selected.
                    if (Globals.ThisWorkbook.Worksheets.Count > 1)
                    {
                        if (sheet.Equals(Globals.ThisWorkbook.ActiveSheet))
                        {
                            sheet.Select(Type.Missing);
                        }
                        else
                        {
                            return;
                        }
                    }
                    //set the wait cursor.
                    Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;
                    //get the active selection
                    // get selected range
                    Range selectedRange = (Range)Globals.ThisWorkbook.Application.Selection;
                    // get active cell
                    int row = Globals.ThisWorkbook.Application.ActiveCell.Row;
                    int col = Globals.ThisWorkbook.Application.ActiveCell.Column;
                    Cell activeCell = new Cell(row, col);

                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Fire toolbar click Event: " + parameter);
                    //now perform the action, depinding on the button clicked.
                    switch (parameter)
                    {
                        case "t_Login": //login.
                        case "m_Login":
                            #region Login
                            //Calc, break if user clicks cancel.
                            if (PafApp.GetGridApp().Calculate(sheet) == DialogResult.Cancel)
                            {
                                break;
                            }

                            //Save, break if user clicks cancel
                            if (PafApp.GetGridApp().SaveWork(false, false, false) == DialogResult.Cancel)
                            {
                                break;
                            }

                            if (PafApp.GetGridApp().ShowLogonDialogBox())
                            {
                                if (PafApp.GetGridApp().ShowRoleSelectionDialogBox())
                                {
                                    //TTN-1064
                                    PafApp.GetGridApp().SaveTaskPaneStatus(true);
                                    PafApp.GetGridApp().BuildActionsPane();
                                    PafApp.GetCommandBarMngr().EnableRolesMenuandToolbarButtons(true);
                                    PafApp.GetCommandBarMngr().EnableOptionsMenuButton(true);
                                    PafApp.GetCommandBarMngr().EnableChangePasswordMenuButton(true);
                                    PafApp.GetCommandBarMngr().EnableCreateAssortmentMenuButton(true);
                                }
                                else
                                {
                                    PafApp.GetCommandBarMngr().ShouldRuleSetComboBoxBeBuilt = false;
                                    //we must call this to get rid of the custom menu def drop down.
                                    PafApp.GetGridApp().RoleSelector.BuildCustomMenuDefs();
                                    PafApp.GetCommandBarMngr().EnableRolesMenuandToolbarButtons(true);
                                    PafApp.GetCommandBarMngr().ShowRoleFilterToolbarButton();
                                    PafApp.GetCommandBarMngr().EnableDisableRoleFilterToolbarButton();
                                    PafApp.GetCommandBarMngr().EnableOptionsMenuButton(true);
                                    PafApp.GetCommandBarMngr().EnableChangePasswordMenuButton(true);
                                    PafApp.GetCommandBarMngr().EnableCreateAssortmentMenuButton(true);
                                    PafApp.GetCommandBarMngr().NonViewSheetToolbarSetup();
                                    Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = false;
                                    throw new SecurityException(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.Class.PafApp.NoRoleSelected"));
                                }
                            }
                            #endregion Login
                            break;
                        case "t_SelectRole": //select role.
                        case "m_SelectRole":
                            #region Select Role
                            //Calc, break if user clicks cancel.
                            if (PafApp.GetGridApp().Calculate(sheet) == DialogResult.Cancel)
                                break;

                            //Save, break if user clicks cancel
                            if (PafApp.GetGridApp().SaveWork(false, false, false) == DialogResult.Cancel)
                            {
                                break;
                            }

                            if (PafApp.GetGridApp().ShowRoleSelectionDialogBox())
                            {
                                //TTN-1372
                                if (PafApp.GetUowCalculator().IsUowCalculated)
                                {
                                    PafApp.GetCommandBarMngr().EnableCreateAssortmentMenuButton(true);
                                    //TTN-1064
                                    PafApp.GetGridApp().SaveTaskPaneStatus(true);
                                    PafApp.GetGridApp().BuildActionsPane();
                                }
                                else
                                {
                                    //TTN-1857
                                    PafApp.GetGridApp().ActionsPane.ClearActionsPane();
                                }
                            }
                            #endregion Select Role
                            break;
                        case "t_SelectRoleFilter": //role filter.
                        case "m_SelectRoleFilter":
                            #region Role Filter
                            //Calc, break if user clicks cancel.
                            //TTN-1408
                            if (PafApp.GetGridApp().Calculate(sheet) == DialogResult.Cancel)
                            {
                                break;
                            }
                            //Save, break if user clicks cancel
                            //TTN-1408
                            if (PafApp.GetGridApp().SaveWork(false, false, false) == DialogResult.Cancel)
                            {
                                break;
                            }
                            //calculate uow

                            DialogResult result = DialogResult.None;
                            bool keepGoing = true;
                            do
                            {
                                result = PafApp.GetUowCalculator().CalculateUow(PafApp.GetGridApp().RoleFilterForm,
                                   PafApp.GetGridApp().RoleSelector.CurrentPafPlannerConfig,
                                   PafApp.GetGridApp().LogonInformation.UserName,
                                   PafApp.GetGridApp().RoleSelector.UserRole,
                                   PafApp.GetGridApp().RoleSelector.UserSelectedPlanTypeSpecString);

                                if(result == DialogResult.Cancel || result == DialogResult.OK ||
                                    result == DialogResult.Abort)
                                {
                                    keepGoing = false;
                                }


                            } while (keepGoing);


                            if (result == DialogResult.OK)
                            {
                                //TTN-1064
                                PafApp.GetGridApp().SaveTaskPaneStatus(true);
                                PafApp.GetGridApp().RoleSelector.BuildCustomMenuDefs();
                                PafApp.GetGridApp().BuildActionsPane();
                            }
                            else
                            {
                                //the user clicked cancel, if we don't have a 
                                //plan session, then make sure the task pane is hidden
                                //because a role filter has not been built yet.
                                if(PafApp.GetPafPlanSessionResponse() == null)
                                {
                                    PafApp.GetCommandBarMngr().SetTaskPaneStatus(false);
                                    //PafApp.GetGridApp().TemporaryLogTTN1064("ExcelEventMgr.CommandBarEventHandler", sheet, null, "PafPlanSessionResponse is Null (1046)");
                                }
                            }
                            #endregion Role Filter
                            break;
                        case "t_TaskPane": //show-hide task pane.
                        case "m_TaskPane":
                            #region Task Pane
                            PafApp.GetGridApp().ShowHideTaskPane();
                            #endregion Task Pane
                            break;
                        case "t_UndoChanges": //undo current changes
                        case "m_UndoChanges":
                            #region Refresh
                            //Save the current task pane state (per Carolyn)
                            PafApp.GetGridApp().SaveTaskPaneCurrentStatus();
                            if (PafApp.GetGridApp().RefreshView(sheet) == DialogResult.No)
                            {
                                break;
                            }
                            RefreshView(viewName, sheet, false);
                            #endregion Refresh
                            break;
                        case "t_Undo":
                            #region New Undo Button
                            if (PafApp.GetGridApp().UndoCalculation() == DialogResult.OK)
                            {
                                //Get the currently selected node.
                                var tn = (TreeListNode)PafApp.GetViewMngr().CurrentViewNode;

                                if (String.IsNullOrEmpty(viewName))
                                {
                                    return;
                                }

                                //Reset the dirty flag.
                                PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = false;

                                //clear the cell note caches.
                                PafApp.GetCellNoteMngr().ClearBuckets();
                                PafApp.GetCellNoteMngr().ClearCache();

                                //Make all the views plannable.
                                PafApp.GetViewMngr().MakeAllProtMngPlanable();

                                //Set all the open views to dirty.
                                PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(0);

                                //select the view...
                                PafApp.GetEventManager().SelectView(
                                    viewName,
                                    sheet,
                                    true,
                                    tn,
                                    true,
                                    true);

                                PafApp.UndoAvailable = false;
                            }
                            break;
                            #endregion New Undo Button
                        case "t_LockCell": //lock cell.
                        case "m_LockCell":
                        case "r_LockCell":
                            #region Lock Cell

                            //If the test harness is running then add the event handler.
                            if (PafApp.GetTestHarnessManager().IsRecording)
                            {
                                //Remove any old event handlers
                                PafApp.GetViewMngr().CurrentProtectionMngr.UserLockedCell -= ExcelEventMgr_UserLockedCell;
                                //Add the new one.
                                PafApp.GetViewMngr().CurrentProtectionMngr.UserLockedCell +=
                                    new ProtectionMngr.ProtectionMngr_UserLockedCell(ExcelEventMgr_UserLockedCell);
                            }
                            ContiguousRange contigRange = (selectedRange).ToContiguousRange();
                            //No add the locks.
                            PafApp.GetViewMngr().CurrentProtectionMngr.AddUserLock(contigRange, sheetHash, sheet.Name);
                            //Enable/disable the buttons.
                            PafApp.GetCommandBarMngr().EnableLockAndPasteShapeButtons();
                            #endregion Lock Cell
                            break;
                        case "m_SessionLock": //Session Lock
                        case "t_SessionLock":
                        case "r_SessionLock": 
                            #region Session Lock
                            //If the test harness is running then add the event handler.
                            if (PafApp.GetTestHarnessManager().IsRecording)
                            {
                                PafApp.GetViewMngr().CurrentProtectionMngr.SessionLockedCell -=
                                    new ProtectionMngr.ProtectionMngr_SessionLockedCell(ExcelEventMgr_SessionLockedCell);

                                //Add the new one.
                                PafApp.GetViewMngr().CurrentProtectionMngr.SessionLockedCell += 
                                    new ProtectionMngr.ProtectionMngr_SessionLockedCell(ExcelEventMgr_SessionLockedCell);
                            }
                            PafApp.GetGridApp().ShowSessionGridLockDialogBox();
                            
                            #endregion Session Lock
                            break;
                        case "t_PasteShape": //paste shape(deprecated) but DO NOT REMOVE
                        case "m_PasteShape": //This is going to turn into and enhancemnt.
                        case "r_PasteShape": //"paste without protection"
                            #region paste shape
                            try
                            {
                                Range range = (Range)Globals.ThisWorkbook.Application.Selection;

                                //Indicate that the change should be treated as a change, but not as a lock.
                                PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape = true;

                                foreach (Range cell in range)
                                {
                                    if ((bool)cell.Locked)
                                    {
                                        MessageBox.Show(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.PasteLockedCells"),
                                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                                        MessageBoxButtons.OK,
                                        MessageBoxIcon.Exclamation);
                                        return;
                                    }
                                }

                                range.PasteSpecial(XlPasteType.xlPasteValues,
                                    XlPasteSpecialOperation.xlPasteSpecialOperationNone,
                                    Type.Missing, Type.Missing);
                            }
                            //The selection might not be a range
                            catch
                            {
                            }
                            #endregion paste shape
                            break;
                        case "t_SaveChanges": //save changes.
                        case "m_SaveChanges":
                            #region Save Changes
                            //Save the current task pane state (per Carolyn)
                            PafApp.GetGridApp().SaveTaskPaneCurrentStatus();
                            //Calc, break if user clicks cancel.
                            //TTN-1052
                            if (PafApp.GetGridApp().Calculate(sheet) != DialogResult.Cancel)
                            {
                                SaveData(false);
                            }
                            #endregion Save Changes
                            break;
                        case "t_Refresh": //refresh from Essabase, refresh cell notes
                        case "m_Refresh": //refresh Pace DATA.
                            //Reload UOW
                            #region refresh Undo Changes
                            //Save the current task pane state (per Carolyn)
                            PafApp.GetGridApp().SaveTaskPaneCurrentStatus();

                            pafDataSlice dataSlice = null;
                            DialogResult rsp = PafApp.GetGridApp().ResetDataCache(ref dataSlice, viewName);

                            if (rsp == DialogResult.OK && dataSlice != null)
                            {
                                //Get the currently selected node.
                                var tn = (TreeListNode)PafApp.GetViewMngr().CurrentViewNode;

                                if (String.IsNullOrEmpty(viewName))
                                {
                                    return;
                                }

                                //Reset the dirty flag.
                                PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = false;

                                //clear the cell note caches.
                                PafApp.GetCellNoteMngr().ClearBuckets();
                                PafApp.GetCellNoteMngr().ClearCache();

                                //Make all the views plannable.
                                PafApp.GetViewMngr().MakeAllProtMngPlanable();

                                //Set all the open views to dirty.
                                PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(0);

                                //select the view...
                                PafApp.GetEventManager().SelectView(
                                    viewName,
                                    sheet,
                                    true,
                                    tn,
                                    true,
                                    true);

                                PafApp.UndoAvailable = false;
                            }
                            else if (rsp == DialogResult.OK && dataSlice == null)
                            {
                                //Reset the dirty flag.
                                PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = false;

                                //Make all the views plannable.
                                PafApp.GetViewMngr().MakeAllProtMngPlanable();

                                //Set all the open views to dirty.
                                PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(0);

                                PafApp.UndoAvailable = false;
                            }
                            #endregion refresh Undo Changes
                            break;
                        case "t_Calculate": //caclulate-evaluate.
                        case "m_Calculate":
                            #region Calculate
                            PafApp.GetEventManager().EvaluateView(sheet);
                            #endregion Calculate
                            break;
                        case "m_Sort": //sort - Duh!
                            #region Sort
                            //Save the current task pane state (per Carolyn)
                            PafApp.GetGridApp().SaveTaskPaneCurrentStatus();

                            frmSort sort = new frmSort();
                            sort.ShowDialogifDataRangeSelected();
                            //SortTest st = new SortTest();
                            #endregion Sort
                            break;
                        case "m_Custom": //Custom menu.
                            #region Custom
                            //PafApp.GetCommandBarMngr().LoadAutomationObject();
                            PafApp.GetCommandBarMngr().CustomMenuHandler(
                                descriptionText,
                                !String.IsNullOrEmpty(viewName) ? viewName : null);
                            #endregion Custom
                            break;
                        case "m_CreateAssortment": //Show the Cluster Dialog!
                            #region CreateAssortment
                            PafApp.GetGridApp().ShowClusterDialogBox();
                            #endregion CreateAssortment
                            break;
                        case "m_Options": //Show the options menu!
                            #region options
                            frmOptions frm = new frmOptions();
                            frm.CurrentGrid = sheetHash;
                            frm.ShowDialog();
                            #endregion options
                            break;
                        case "m_Help": //Opends the default browser to the wiki help.
                            #region Help
                            Win32Utils.OpenDefaultBrowser(PafApp.GetLocalization().GetResourceManager().GetString("Application.HelpFile.BaseUrl"));
                            break;
                        #endregion Help
                        case "m_Help1": //shows chum file.
                            #region Help1
                            string path = Environment.GetEnvironmentVariable(
                                PafApp.GetLocalization().GetResourceManager().GetString("Application.EnviormentVar"));

                            if (!String.IsNullOrEmpty(path))
                            {
                                try
                                {
                                    string fileName = PafApp.GetLocalization().GetResourceManager().GetString("Application.HelpFileName");
                                    string helpFileDir = PafApp.GetLocalization().GetResourceManager().GetString("Application.HelpFileName.Subdir");

                                    if (!File.Exists(String.Format("{0}{1}{2}{3}{4}",path,Path.DirectorySeparatorChar,helpFileDir,Path.DirectorySeparatorChar,fileName)))
                                    {
                                        fileName = PafApp.GetLocalization().GetResourceManager().GetString("Application.HelpFileName2");
                                    }

                                    if (!String.IsNullOrEmpty(fileName))
                                    {
                                        if (File.Exists(String.Format("{0}{1}{2}{3}{4}", path, Path.DirectorySeparatorChar, helpFileDir, Path.DirectorySeparatorChar, fileName)))
                                        {
                                            Help.ShowHelp(
                                                null,
                                                String.Format(
                                                    "{0}{1}{2}{3}{4}",
                                                    path,
                                                    Path.DirectorySeparatorChar,
                                                    helpFileDir,
                                                    Path.DirectorySeparatorChar,
                                                    fileName),
                                                PafApp.GetLocalization().GetResourceManager().GetString("Application.HelpFile.DefaultPage"));
                                        }
                                        else
                                        {
                                            throw new Exception();
                                        }
                                    }
                                }
                                catch (Exception)
                                {
                                    throw new Exception(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.ExcelEventMgr.NoHelpFile"));
                                }
                            }
                            #endregion Help1
                            break;
                        case "m_About": //Shows the about box.
                            #region About
                            frmAboutBox2 about = new frmAboutBox2();
                            about.ShowDialog();
                            #endregion About
                            break;
                        case "m_ChangePassword":
                            #region Change Password
                            //get current user name and password
                            string userName = PafApp.GetGridApp().LogonInformation.UserName;
                            string currentPassword = PafApp.GetGridApp().LogonInformation.UserPassword;

                            //create new form change password dialog and then open
                            frmChangePassword changePasswordForm = new frmChangePassword(userName, currentPassword, PafApp.MinNewPasswordLength, PafApp.MaxNewPasswordLength);
                            DialogResult dialogResult = changePasswordForm.ShowDialog();

                            //if user clicks ok
                            if (dialogResult == DialogResult.OK)
                            {

                                //get new password from form
                                string newPassword = changePasswordForm._changePassword.NewPassword;

                                //change password
                                PafApp.ChangePasswordRequest(userName, currentPassword, newPassword);

                                //set login info using new password
                                PafApp.GetGridApp().LogonInformation.UserPassword = newPassword;

                            }
                            #endregion Change Password
                            break;
                        case "r_ReplicateAll":
                            #region Replicate All
                            //do the replicaiton
                            PafApp.GetViewMngr().CurrentProtectionMngr.Replication.ReplicateCells(
                                (from Range c in selectedRange select new Cell(c.Row, c.Column)).ToList(),
                                true,
                                ReplicationType.ReplicateAll);
                            //TTN-1324
                            if(PafApp.GetTestHarnessManager().IsRecording)
                            {
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ChangeType = Change.ReplicateAll;
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ProtMngrListTracker.UpdateLists(PafApp.GetViewMngr().CurrentProtectionMngr);
                            }

                            #endregion Replicate All
                            break;
                        case "r_ReplicateExisting":
                            #region Replicate Existing
                            //do the replicaiton
                            PafApp.GetViewMngr().CurrentProtectionMngr.Replication.ReplicateCells(
                                (from Range c in selectedRange select new Cell(c.Row, c.Column)).ToList(),
                                true,
                                ReplicationType.ReplicateExisting);
                            //TTN-1324
                            if (PafApp.GetTestHarnessManager().IsRecording)
                            {
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ChangeType = Change.ReplicateExisting;
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ProtMngrListTracker.UpdateLists(PafApp.GetViewMngr().CurrentProtectionMngr);
                            }
                            #endregion Replicate Existing
                            break;
                        case "r_LiftAll":
                            #region Lift All
                            //do the lift
                            PafApp.GetViewMngr().CurrentProtectionMngr.Lift.LiftCells(
                                (from Range c in selectedRange select new Cell(c.Row, c.Column)).ToList(),
                                true,
                                LiftType.LiftAll);
                            //TTN-1324
                            if (PafApp.GetTestHarnessManager().IsRecording)
                            {
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ChangeType = Change.LiftAll;
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ProtMngrListTracker.UpdateLists(PafApp.GetViewMngr().CurrentProtectionMngr);
                            }

                            #endregion Lift All
                            break;
                        case "r_LiftExisting":
                            #region Lift Existing
                            //do the replicaiton
                            PafApp.GetViewMngr().CurrentProtectionMngr.Lift.LiftCells(
                                (from Range c in selectedRange select new Cell(c.Row, c.Column)).ToList(),
                                true,
                                LiftType.LiftExisting);
                            //TTN-1324
                            if (PafApp.GetTestHarnessManager().IsRecording)
                            {
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ChangeType = Change.LiftExisting;
                                PafApp.GetTestHarnessChangedCellMngr().PeekBack().ProtMngrListTracker.UpdateLists(PafApp.GetViewMngr().CurrentProtectionMngr);
                            }
                            #endregion Lift Existing
                            break;
                        case "r_Unreplicate":
                            #region Unreplicate
                            //do the unreplicaiton
                            PafApp.GetViewMngr().CurrentProtectionMngr.Replication.UnAllocateCells(
                                (from Range c in selectedRange select new Cell(c.Row, c.Column)).ToList(),
                                sheetHash,
                                sheet.Name);
                            #endregion Unreplicate
                            break;
                        case "r_Unlift":
                            #region UnLift
                            //do the unreplicaiton
                            PafApp.GetViewMngr().CurrentProtectionMngr.Lift.UnAllocateCells(
                                (from Range c in selectedRange select new Cell(c.Row, c.Column)).ToList(),
                                sheetHash,
                                sheet.Name);
                            #endregion Unreplicate
                            break;
                        case "m_RowGroup":
                            #region RowUngroup
                            Globals.ThisWorkbook.Application.ScreenUpdating = false;
                            OlapView olapView = PafApp.GetViewMngr().CurrentOlapView;
                            GroupingSpec rSpec = olapView.GetGroupingSpec(ViewAxis.Row);
                            if (rSpec == null) break;
                            if (!Convert.ToBoolean(descriptionText))
                            {
                                rSpec.ApplyGroups = false;
                                olapView.ClearMemberGroups(ViewAxis.Row);
                            }
                            else
                            {
                                rSpec.ApplyGroups = true;
                                olapView.BuildMemberGroups(ViewAxis.Row);
                                olapView.ExpandGroups(true, false);
                            }
                            #endregion RowUngroup
                            break;
                        case "m_ColumnGroup":
                            #region ColumnUngroup
                            Globals.ThisWorkbook.Application.ScreenUpdating = false;
                            OlapView olapView1 = PafApp.GetViewMngr().CurrentOlapView;
                            GroupingSpec cSpec = olapView1.GetGroupingSpec(ViewAxis.Col);
                            if (cSpec == null) break;
                            if (!Convert.ToBoolean(descriptionText))
                            {
                                cSpec.ApplyGroups = false;
                                olapView1.ClearMemberGroups(ViewAxis.Col);
                            }
                            else
                            {
                                cSpec.ApplyGroups = true;
                                olapView1.BuildMemberGroups(ViewAxis.Col);
                                olapView1.ExpandGroups(false, true);
                            }

                            #endregion ColumnUngroup
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;
                //Reset menus
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();
                if (PafApp.GetViewMngr().CurrentGrid != null)
                {
                    PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                }
                if (!Globals.ThisWorkbook.Application.ScreenUpdating)
                {
                    Globals.ThisWorkbook.Application.ScreenUpdating = true;
                }
            }
        }

        // ReSharper disable InconsistentNaming
        public void ExcelEventMgr_SessionLockedCell(object sender, SessionLockedCellEventArgs eventArgs)

        {
            if (SessionLockedCell != null)
            {
                SessionLockedCell(this, eventArgs);
            }
        }

        /// <summary>
        /// Event handler for when a user locks a cell.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void ExcelEventMgr_UserLockedCell(object sender, UserLockedCellEventArgs eventArgs)
        {
            if (UserLockedCell != null)
                UserLockedCell(this, eventArgs);
        }
        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Activates a view sheet.
        /// </summary>
        /// <param name="viewName">The name of the view.</param>
        /// <returns>true if the sheet was activated, false if not or was not found.</returns>
        public bool ActivateView(string viewName)
        {
            bool ret = true;
            try
            {
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;
                Globals.ThisWorkbook.Application.EnableEvents = false;
                Globals.ThisWorkbook.Application.ScreenUpdating = false;
                Worksheet sheet = null;


                foreach(KeyValuePair<int, ViewStateInfo> kvp in PafApp.GetViewMngr().Views)
                {
                    if(kvp.Value.ViewName.ToLower().Equals(viewName.ToLower()))
                    {
                        sheet = PafApp.GetGridApp().GetWorksheetFromHashCode(kvp.Key);
                        break;
                    }
                }

                if (sheet != null)
                {
                    //We are not on the current sheet, so get the existsing sheet, lock the 
                    Worksheet sht = PafApp.GetGridApp().GetWorksheetFromHashCode(sheet.GetHashCode());
                    //Turn the events back on so the activate/deactivate events fire.
                    Globals.ThisWorkbook.Application.EnableEvents = true;
                    PafApp.GetGridApp().ActivateSheet(sht.Name);

                    //Turn off select Essbase AddIn Global Options
                    //PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(false);

                    //Turn off Hyperion Smart View Global Options
                    //PafApp.GetGridApp().SetHSVAddInGlobalOptions(false, null);

                    //Hide Essbase AddIn
                    PafApp.GetGridApp().ShowHideEssbaseAddIn(false);

                    //Hide Hyprion Smart View AddIn
                    PafApp.GetGridApp().ShowHideHSVAddIn(false);
                }
                else
                {
                    ret = false;
                }

                return ret;

            }
            catch (Exception ex)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
                return false;
            }
            finally
            {
                //Reset the mouse pointer.
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                Globals.ThisWorkbook.Application.EnableEvents = true;
            }
        }

        /// <summary>
        /// Retrieve the view from the server and place it on a worksheet.
        /// </summary>
        /// <param name="viewName">The name of the view</param>
        /// <param name="workSheet">The worksheet object where the user was when then
        /// clicked the view in the view tree.</param>
        /// <param name="forceUpdate">Force the sheet to update, if this is false and the sheet 
        /// exists then the sheet will be selected and not updated.</param>
        /// <param name="treeNode">The tree node to select.</param>
        public void SelectView(string viewName, Worksheet workSheet, bool forceUpdate, object treeNode)
        {
            SelectView(viewName, workSheet, forceUpdate, treeNode, false, false);
        }

        /// <summary>
        /// Retrieve the view from the server and place it on a worksheet.
        /// </summary>
        /// <param name="viewName">The name of the view</param>
        /// <param name="workSheet">The worksheet object where the user was when then
        /// clicked the view in the view tree.</param>
        /// <param name="forceUpdate">Force the sheet to update, if this is false and the sheet 
        /// exists then the sheet will be selected and not updated.</param>
        /// <param name="treeNode">The tree node to select.</param>
        /// <param name="reset">Reset the current view.</param>
        /// <param name="resetMbrTagCache">Clear out all the cell in the member tag cache.</param>
        public void SelectView(string viewName, Worksheet workSheet, bool forceUpdate, object treeNode, bool reset, bool resetMbrTagCache)
        {
            PafApp.GetLogger().Info("SelectView: " + viewName + ", on sheet: " + workSheet.Name);
            Stopwatch startTime = Stopwatch.StartNew();

            //We look thru all the sheets in the workbook for a sheet with a cutsom view property.
            //When we create a view on a sheet, we add a custom property to the sheet with the 
            //view name.
            Worksheet sheet =
                PafApp.GetGridApp().DoesWorkbookContainCustomProperty(PafAppConstants.OUR_VIEW_PROP_VALUE, viewName);

            string sheetName = String.Empty;

            try
            {
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;
                Globals.ThisWorkbook.Application.EnableEvents = false;
                Globals.ThisWorkbook.Application.ScreenUpdating = false;


                //unprotect all our sheets...
                ////TTN-633
                //PafApp.GetGridApp().UnprotectOurWorksheets();
                //TTN-1042, TTN-2397
                bool unprotect = true;
                if (sheet != null && !forceUpdate && PafApp.GetGridApp().InCutCopyMode())
                {
                    unprotect = false;
                    PafApp.GetLogger().Info("SelectView: force update is false, Excel is in Cut/Copy mode, no unprotect will occur.");   
                }
                if (unprotect)
                {
                    PafApp.GetGridApp().UnprotectWorksheets();
                }

                //Hide Essbase AddIn
                PafApp.GetGridApp().ShowHideEssbaseAddIn(false);

                //Hide Hyprion Smart View AddIn
                PafApp.GetGridApp().ShowHideHSVAddIn(false);

                //Turn off Hyperion Smart View Global Options
                PafApp.GetGridApp().SetHSVAddInGlobalOptions(true);

                //Restore Essbase Global Options
                PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(true);

                //The worksheet/view combo does not exist, so we have to create it.
                if (sheet == null)
                {
                    //See if the sheet has a view on it.
                    //bool val = PafApp.GetGridApp().DoesSheetContainCustomProperty(
                    //    workSheet, PafAppConstants.OUR_VIEW_PROP_VALUE);

                    //If it's not our sheet...
                    //if (!val)
                    //{
                    //    //Store the global settings when leaving a non-Titan worksheet
                    //    PafApp.GetGridApp().GetEssbaseSecondaryButtonSetting(true);
                    //    PafApp.GetGridApp().GetEssbaseDoubleClicking(true);
                    //}

                    //No view sheet exists, so we have to create one.
                    sheet = PafApp.GetGridApp().BuildNewViewSheet(
                        workSheet,
                        PafAppConstants.OUR_VIEW_PROP_VALUE,
                        viewName,
                        PafAppConstants.SHEET_PASSWORD);

                    sheet.Change += _eventDelCellsChange;

                    //Some error occured, so we can't build the view.
                    if (sheet == null)
                        return;

                    //set the sheet name.
                    sheetName = sheet.Name;

                    //If the view is dynamic we must do some special setup, so no errors occur.
                    if (PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName))
                    {
                        //viewInfoResponse response = PafApp.GetServiceMngr().ViewInformation(viewName);
                        //PafApp.GetViewTreeView().SetGroupingSpec(viewName, response.viewInfo.viewSectionInfo[0].groupingSpecs);
                        Globals.ThisWorkbook.Ribbon.InvalidateViewOptionsGroup();
                        //If a grid does not already exist, then build one.
                        if (!PafApp.GetViewMngr().ViewExists(sheet.GetHashCode()))
                        {
                            //Fix issue TTN-230.  We have to set up all the necessary variables for
                            //dynamic views.  If not the dimension tree(s) will not appear/disappear properly.
                            BuildInitialDynamicView(sheet, viewName, treeNode);
                        }
                        else
                        {
                            PafApp.GetGridApp().UnhideSheet(sheet.Name);
                        }
                        //Make sure the protection manager is clean.
                        PafApp.GetViewMngr().CurrentView.GetProtectionMngr().ClearProtection();
                        //If we don't have any selections, then we can't get the view from the server - so return.
                        if (PafApp.GetGridApp().ActionsPane.GetCachedDyanmicPafUserSelections(viewName) == null)
                        {
                            return;
                        }
                    }
                }
                else
                {
                    //The sheet/view exists, so we must:
                    //Check to see if we are on the sheet, if so just continue, else lock the current sheet,
                    //and make the new sheet active.

                    PafApp.GetGridApp().UnhideSheet(sheet.Name);
                    if (!Convert.ToString(sheet.GetHashCode()).Equals(Convert.ToString(workSheet.GetHashCode())))
                    {
                        //We are not on the current sheet, so get the existsing sheet, lock the 
                        Worksheet sht = PafApp.GetGridApp().GetWorksheetFromHashCode(sheet.GetHashCode());
                        //Turn the events back on so the activate/deactivate events fire.
                        Globals.ThisWorkbook.Application.EnableEvents = true;
                        PafApp.GetGridApp().ActivateSheet(sht.Name);
                    }
                    //We don't want to force an update, so just return.
                    if (!forceUpdate)
                    {
                        return;
                    }
                }


                ExcelActionsPane actionsPane = PafApp.GetGridApp().ActionsPane;
                //Get user selections
                pafUserSelection[] pafUserSel = actionsPane.GetCachedDyanmicPafUserSelections(viewName);
                //get the suppress zero settings.
                bool[] suppressZero = actionsPane.GetCachedSuppressZeroSelections(viewName);
                //Get user selections parent first.
                bool? colParentFirst = actionsPane.GetParentFirst(ViewAxis.Col, pafUserSel);
                bool? rowParentFirst = actionsPane.GetParentFirst(ViewAxis.Row, pafUserSel);

                int sheetHash = sheet.GetHashCode();
                sheetName = sheet.Name;

                //Clear out sorting settings for dynamic views (every time the Get View button is selected)
                if (PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName) && reset == false)
                {
                    if (PafApp.GetViewMngr().GetOlapView(sheetHash) != null)
                    {
                        PafApp.GetViewMngr().GetOlapView(sheetHash).IsSorted = false;
                        PafApp.GetViewMngr().GetOlapView(sheetHash).ServerSortOverridden = false;
                        PafApp.GetViewMngr().GetOlapView(sheetHash).ServerSortSpecified = false;
                        PafApp.GetViewMngr().GetOlapView(sheetHash).CurrentSortOrder.Clear();
                        PafApp.GetViewMngr().GetOlapView(sheetHash).SortSelections.Clear();
                    }
                }

                //the view is being reselected, so clear out the cell notes.
                if (PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName))
                {
                    PafApp.GetCellNoteMngr().ClearCellNotes(PafApp.GetViewMngr().CurrentView);
                }

                //Create a view object.
                pafView pafView;
                List<CoordinateSet> parents = PafApp.GetViewMngr().GlobalLockMngr.AllUserSessionParentLocks();
                //Get the view...
                pafView = PafApp.GetServiceMngr().GetPafView(viewName, sheetHash, pafUserSel, suppressZero[0], suppressZero[1], parents);

                Globals.ThisWorkbook.Application.EnableEvents = false;
                Globals.ThisWorkbook.Application.ScreenUpdating = false;
                try
                {
                    if (resetMbrTagCache)
                    {
                        //reset the member tag cache.
                        PafApp.GetMbrTagMngr().MbrTagCache.Reset();
                    }

                    //TTN-804
                    bool applyRowGroups = true;
                    bool applyColGroups = true;
                    bool[] groupStatus = PafApp.GetViewTreeView().GetViewGroupingSpecStatus(viewName);
                    if (groupStatus != null && groupStatus.Length == 2)
                    {
                        applyRowGroups = groupStatus[0];
                        applyColGroups = groupStatus[1];
                    }

                    ExcelBuildView excelBuildView = new ExcelBuildView(sheet, viewName, true, pafView.HasAttributes());
                    excelBuildView.InitializeGrid(pafView, Settings.Default.AutoPrintRange, applyRowGroups, applyColGroups, rowParentFirst, colParentFirst);

                    ViewMngr viewMngr = PafApp.GetViewMngr();
                    IGridInterface currentGrid = viewMngr.CurrentGrid;
                    CommandBarMngr commandBarMngr = PafApp.GetCommandBarMngr();
                    OlapView olapView = viewMngr.CurrentOlapView;
                    ViewStateInfo currentView = viewMngr.CurrentView;
                    IAppInterface gridApp = PafApp.GetGridApp();

                    //Add/Update group specification
                    PafApp.GetViewTreeView().SetGroupingSpec(viewName, olapView.RowGroupSpec, olapView.ColGroupSpec);

                    //get the cell notes
                    PafApp.GetCellNoteMngr().UpdateClientCacheFromServerCache(PafApp.GetViewMngr().CurrentView.CellNotesAsArray);

                    //removed the cached intersecions.
                    PafApp.GetCellNoteMngr().ClearCachedIntersections(viewName);

                    //set the cell notes
                    PafApp.GetCellNoteMngr().RenderNotes(PafApp.GetViewMngr().CurrentView);

                    //Set the treenode
                    viewMngr.SetViewNode(sheetHash, treeNode);

                    //Save the task pane status
                    gridApp.SaveTaskPaneCurrentStatus();

                    //Set the view options.
                    gridApp.ActionsPane.SetViewOptions(viewName);

                    //TTN-804
                    olapView.ExpandGroups();

                    //Turn events on.
                    currentGrid.EnableEvents(true);

                    //Enable the Undo, & Calculate buttons
                    commandBarMngr.EnableMenuandToolbarButtons();

                    //enable the refresh button.
                    gridApp.EnableViewSheetOnlyButtons(true);

                    PafApp.StartUp = false;

                    //If their is a calc pending, then we must make this sheet not planable.
                    if (viewMngr.PlannableViewName.Equals(""))
                    {
                        currentView.Plannable = true;
                    }
                    else
                    {
                        currentView.Plannable = false;
                        //Since this is not plannable, then lock down the entire sheet.
                        currentGrid.Lockcells(olapView.DataRange);
                    }

                    //We are clean so set the flag.
                    currentView.Dirty = false;

                    //make sure the excel menus are disabled(ttn-765, 766, 778, 780).
                    //EnableDisableHostApplicationMenuOptions(false);

                    //Set the app bars.
                    commandBarMngr.ShowCustomApplicationBars(true, true, true);

                    //pmack
                    //Enable/Disable Custom menus
                    commandBarMngr.EnableCustomCommandMenus(viewName);

                    //Enabled/Disable the Sort Menu Button
                    commandBarMngr.EnableSortMenuButton(true);

                    //Enable the session lock buttons.
                    gridApp.EnableSessionLockButtons(true);

                    //set the last cell
                    _lastCell = GetActiveRange();

                    if (olapView.ServerSortSpecified)
                    {
                        viewMngr.Sort();
                    }

                    viewMngr.CurrentProtectionMngr.ProtectionMgnrStatus();

                    //Reset menus
                    Globals.ThisWorkbook.Ribbon.InvalidateAll();

                }
                catch (NoDataException)
                {
                    Globals.ThisWorkbook.Application.DisplayAlerts = false;
                    sheet.Delete();
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Equals(String.Empty))
                    {
                        PafApp.MessageBox().Show(ex);
                    }
                    Globals.ThisWorkbook.Application.DisplayAlerts = false;
                    PafApp.GetGridApp().ActionsPane.ResetViewTreeSelectedNode();
                    sheet.Delete();
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                //protect all our sheets...
                //TTN-633
                PafApp.GetGridApp().ProtectOurWorksheets();
                //TTN-1034
                PafApp.GetGridApp().HideUnbuiltWorksheet(sheet);
                //Reset the mouse pointer.
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                //Allow the screen to update.
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                Globals.ThisWorkbook.Application.EnableEvents = true;
                Globals.ThisWorkbook.Application.DisplayAlerts = true;
                //Clear out the changedcellmngr for this sheet.
                if (sheet != null)
                {
                    PafApp.GetChangedCellMngr().ClearChangedCells(sheetName);
                }
            }
            startTime.Stop();
            PafApp.GetLogger().Info("Memory Usage: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            PafApp.GetLogger().InfoFormat("Complete SelectView, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
            PafApp.GetLogger().Info("==========================================");
        }

        /// <summary>
        /// Refreshes the view on a worksheet.
        /// </summary>
        /// <param name="viewName">The name of the view</param>
        /// <param name="refreshFromServer">Refresh the view on the server.</param>
        public void RefreshView(string viewName, bool refreshFromServer)
        {
            // get active sheet
            Worksheet workSheet =
                (Worksheet)
                Globals.ThisWorkbook.Application.ActiveWorkbook.ActiveSheet;

            RefreshView(viewName, workSheet, refreshFromServer);
        }

        /// <summary>
        /// Refreshes the view on a worksheet.
        /// </summary>
        /// <param name="viewName">The name of the view</param>
        /// <param name="workSheet">The worksheet object where the user was when then
        /// clicked the view in the view tree.</param>
        /// <param name="refreshFromServer">Refresh the view on the server.</param>
        public void RefreshView(string viewName, Worksheet workSheet, bool refreshFromServer)
        {
            PafApp.GetLogger().Info("RefreshView: " + viewName + ", on sheet: " + workSheet.Name);
            Stopwatch startTime = Stopwatch.StartNew();

            object treeNode = PafApp.GetViewMngr().CurrentViewNode;

            //We look thru all the sheets in the workbook for a sheet with a cutsom view property.
            //When we create a view on a sheet, we add a custom property to the sheet with the 
            //view name.
            Worksheet sheet =
                PafApp.GetGridApp().DoesWorkbookContainCustomProperty(PafAppConstants.OUR_VIEW_PROP_VALUE, viewName);
            int sheetHash = 0;
            string sheetName = sheet.Name;
            //Get the currently plannable view (if one exists)
            int plannableHashCode = PafApp.GetViewMngr().PlannableViewHashCode;

            try
            {
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;
                Globals.ThisWorkbook.Application.EnableEvents = false;
                Globals.ThisWorkbook.Application.ScreenUpdating = false;

                //protect all our sheets...
                ////TTN-633
                //PafApp.GetGridApp().UnprotectOurWorksheets();
                //TTN-1042
                PafApp.GetGridApp().UnprotectWorksheets();

                sheetHash = sheet.GetHashCode();

                bool setPrintRange = (Settings.Default.AutoPrintRange && Utility.CheckForDefaultPrinter());

                //Is this a dynamic view.
                bool dynamicView = PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName);
                //Get actions pane.
                ExcelActionsPane actionsPane = PafApp.GetGridApp().ActionsPane;
                //Get the cached user selections
                pafUserSelection[] pafUserSel = actionsPane.GetCachedDyanmicPafUserSelections(viewName);
                //get the suppress zero settings.
                bool[] suppressZero = actionsPane.GetCachedSuppressZeroSelections(viewName);
                //If the view is dynamic and no selections have been made, then return - becuase we don't have enough selections to update from.
                if (dynamicView && pafUserSel == null)
                {
                    return;
                }
                bool? colParentFirst = actionsPane.GetParentFirst(ViewAxis.Col, pafUserSel);
                bool? rowParentFirst = actionsPane.GetParentFirst(ViewAxis.Row, pafUserSel);

                //Get the view...
                pafView pafView = null;
                PafApp.GetLogger().Info("Get View Response");

                if (refreshFromServer)
                {
                    pafView = PafApp.GetServiceMngr().GetPafView(viewName, sheetHash, pafUserSel, suppressZero[0], suppressZero[1], PafApp.GetViewMngr().GlobalLockMngr.AllUserSessionParentLocks());
                }
                else
                {
                    pafView = PafApp.GetServiceMngr().GetCurrentPafView(viewName);
                }
                PafApp.GetLogger().Info("Complete Get View Response");

                try
                {
                    //If the refresh is called as part of a sort
                    if (PafApp.GetViewMngr().Sorting)
                    {
                        //The protected intersections have already been captured - do not apply protection
                        ExcelBuildView excelBuildView = new ExcelBuildView(sheet, viewName, false, pafView.HasAttributes());
                        excelBuildView.InitializeGrid(pafView, Settings.Default.AutoPrintRange, rowParentFirst: rowParentFirst, colParentFirst: colParentFirst);

                        //Select the sort range
                        PafApp.GetViewMngr().CurrentGrid.Select(PafApp.GetViewMngr().CurrentOlapView.SortRange);
                        PafApp.GetViewMngr().CurrentGrid.SetWindowPosition(PafApp.GetViewMngr().CurrentOlapView.TopLeftWindowAnchor);
                    }
                    else
                    {
                        if (PafApp.GetMbrTagMngr().MbrTagCache.SavedMembers)
                        {
                            //we have to get the view to get all the new member tag data.
                            pafView = PafApp.GetServiceMngr().GetPafView(viewName, sheetHash, pafUserSel, suppressZero[0], suppressZero[1], PafApp.GetViewMngr().GlobalLockMngr.AllUserSessionParentLocks());
                        }

                        //reset the member tag cache.
                        PafApp.GetMbrTagMngr().MbrTagCache.Reset();

                        ExcelBuildView excelBuildView = new ExcelBuildView(sheet, viewName, true, pafView.HasAttributes());
                        excelBuildView.InitializeGrid(pafView, Settings.Default.AutoPrintRange, rowParentFirst: rowParentFirst, colParentFirst: colParentFirst);
                    }

                    ViewMngr viewMngr = PafApp.GetViewMngr();
                    CommandBarMngr commandBarMngr = PafApp.GetCommandBarMngr();
                    OlapView olapView = viewMngr.CurrentOlapView;
                    ViewStateInfo currentView = viewMngr.CurrentView;
                    IAppInterface gridApp = PafApp.GetGridApp();

                    //get the cell notes
                    PafApp.GetCellNoteMngr().UpdateClientCacheFromServerCache(currentView.CellNotesAsArray);

                    Debug.WriteLine("Current cell notes in client cache: " + PafApp.GetCellNoteMngr().ClientCache.Count);

                    //removed the cached intersecions.
                    PafApp.GetCellNoteMngr().ClearCachedIntersections(viewName);

                    //set the cell notes
                    PafApp.GetCellNoteMngr().RenderNotes(currentView);

                    //Set the treenode
                    viewMngr.SetViewNode(sheetHash, treeNode);

                    //Set the view options.
                    gridApp.ActionsPane.SetViewOptions(viewName);

                    //TTN-804
                    olapView.ExpandGroups();

                    //TTN-894
                    //Enable/Disable Custom menus
                    commandBarMngr.EnableCustomCommandMenus(viewName);

                    //Enable the Undo, & Calculate buttons
                    commandBarMngr.EnableMenuandToolbarButtons();

                    //enable the refresh button.
                    gridApp.EnableViewSheetOnlyButtons(true);

                    PafApp.StartUp = false;

                    //We must make this sheet not planable.
                    if (plannableHashCode.Equals(0))
                    {
                        //Do Nothing...
                        //Set the flags of all the sheets to be clean.
                        viewMngr.MakeAllProtMngPlanable();
                    }
                    else if (plannableHashCode.Equals(sheetHash))
                    {
                        //Set the flags of all the sheets to be clean.
                        viewMngr.MakeAllProtMngPlanable();

                        //Make all the other ProtectionMngr Dirty.
                        viewMngr.MakeAllProtMngrDirtyExcept(sheetHash);
                    }
                    else
                    {
                        currentView.Plannable = false;

                        //Since this is not plannable, then lock down the entire sheet.
                        viewMngr.CurrentGrid.Lockcells(olapView.DataRange);
                    }

                    //set the last cell
                    _lastCell = GetActiveRange();

                    //We are clean so set the flag.
                    currentView.Dirty = false;

                    //Set the app bars.
                    commandBarMngr.ShowCustomApplicationBars(true, true, true);

                    Globals.ThisWorkbook.Ribbon.InvalidateAll();
                }
                catch (Exception ex)
                {
                    if (!ex.Message.Equals(String.Empty))
                    {
                        PafApp.MessageBox().Show(ex);
                    }
                    Globals.ThisWorkbook.Application.DisplayAlerts = false;
                    sheet.Delete();
                }
            }
            catch (Exception ex)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                //protect all our sheets...
                //TTN-633
                PafApp.GetGridApp().ProtectOurWorksheets();
                //Reset the mouse pointer.
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                //Allow the screen to update.
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                Globals.ThisWorkbook.Application.EnableEvents = true;
                Globals.ThisWorkbook.Application.DisplayAlerts = true;
                //Clear out the changedcellmngr for this sheet.
                if (sheet != null)
                {
                    PafApp.GetChangedCellMngr().ClearChangedCells(sheetName);
                }
            }
            startTime.Stop();
            PafApp.GetLogger().Info("Memory Usage: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
            PafApp.GetLogger().InfoFormat("Complete RefreshView, runtime: {0}", startTime.ElapsedMilliseconds.ToString("N0"));
            PafApp.GetLogger().Info("==========================================");
        }

        /// <summary>
        /// Setup all the necessary variables for dynamic views.
        /// </summary>
        /// <param name="sheet">The worksheet object where the user was when then
        /// clicked the view in the view tree.</param>
        /// <param name="viewName">The name of the view.</param>
        /// <param name="treeNode">The tree node to select.</param>
        private void BuildInitialDynamicView(Worksheet sheet, string viewName, object treeNode)
        {
            try
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = false;

                ExcelBuildView _ExcelBuildView = new ExcelBuildView(sheet, viewName, true, false);

                //Set the treenode
                PafApp.GetViewMngr().SetViewNode(sheet.GetHashCode(), treeNode);

                //Turn events on.
                PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);

                //Enable the Save Work, Undo, & Calculate buttons
                PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();

                //enable the refresh button.
                PafApp.GetGridApp().EnableViewSheetOnlyButtons(true);

                PafApp.StartUp = false;

                //If their is a calc pending, then we must make this sheet not planable.
                if (String.IsNullOrEmpty(PafApp.GetViewMngr().PlannableViewName))
                {
                    PafApp.GetViewMngr().CurrentView.Plannable = true;
                }
                else
                {
                    PafApp.GetViewMngr().CurrentView.Plannable = false;
                }


                //We are clean so set the flag.
                PafApp.GetViewMngr().CurrentView.Dirty = false;

                //Select cell a1, per Jira issue.
                PafApp.GetViewMngr().CurrentGrid.Select(new Cell(1, 1));

                //Set the app bars.
                PafApp.GetCommandBarMngr().ShowCustomApplicationBars(true, true, true);
            }
            catch (Exception ex)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                //Turn screen updating on.
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// Saves the pace data back to the server.
        /// </summary>
        /// <param name="runSilent">
        /// Run the save silently, no save data message box will be shown to the user.
        /// </param>
        public void SaveData(bool runSilent)
        {
            try
            {
                try
                {
                    Range rng = GetActiveRange();

                    if (rng != null)
                    {
                        PafApp.GetCellNoteMngr().CopyAndDeleteCellNotes(
                            new Cell(rng.Row, rng.Column).CellAddress,
                            PafApp.GetViewMngr().CurrentView);
                    }
                }
                catch(Exception)
                {
                    //
                }

                PafApp.GetGridApp().SaveWork(true, true, runSilent);

                //pause the screen.
                Globals.ThisWorkbook.Application.ScreenUpdating = false;
                //render the notes.
                PafApp.GetCellNoteMngr().RenderNotes(PafApp.GetViewMngr().CurrentView);
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                //Turn screen updating on.
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
            }
        }

        /// <summary>
        /// Performs an evaluation.
        /// </summary>
        /// <param name="sheet">The sheet to perform the calculation.</param>
        public void EvaluateView(Worksheet sheet)
        {
            //string exception = String.Empty;
            string sheetName = String.Empty;
            Stopwatch startTime = Stopwatch.StartNew();

            try
            {
                //Set the mouse pointer to a hourglass.
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;

                //Get the hash code of the current sheet.
                int hashCode = sheet.GetHashCode();

                //Get the Current View Manager
                ViewMngr viewMngr = PafApp.GetViewMngr();

                //Get the existing Actions Pane
                ExcelActionsPane actionsPane = PafApp.GetGridApp().ActionsPane;

                //Set the current view.
                viewMngr.SetCurrentView(hashCode);

                //Get the current protection manager of the view.
                ProtectionMngr currentProtMngr = viewMngr.CurrentProtectionMngr;

                //Get the current view
                ViewStateInfo currentView = viewMngr.CurrentView;

                //Get the current ruleset used in protection processing
                RuleSet currentRuleSet = currentProtMngr.CurrentRuleset;
                if (currentRuleSet.HasMemberTagFormulas && PafApp.GetMbrTagMngr().IsMbrTagCacheDirty(currentRuleSet.ReferencedMbrTagDefs))
                {
                    //Ask the user to save the dirty member tags.
                    DialogResult re = MessageBox.Show(
                            String.Format(PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.DirtyMemberTags"), PafApp.RuleSetName),
                            PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                            MessageBoxButtons.YesNoCancel,
                            MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button1);

                    switch (re)
                    {
                        case DialogResult.Yes:
                            //User said yes, so save the tags
                            if (!PafApp.GetGridApp().SaveMemberTags(sheet, viewMngr))
                            {
                                return;
                            }
                            break;
                        case DialogResult.Cancel:
                            //User clicked cancel, so exit out of the Eval
                            return;
                    }
                }

                //Get the current olap view for the view.
                OlapView currentOlapView = viewMngr.CurrentOlapView;

                //Get the name of the view to be evaluated.
                string viewName = currentView.ViewName;

                //Log...
                PafApp.GetLogger().Info("Calculating view: " + viewName + " on Sheet: " + sheet.Name + "(" + hashCode + ").");

                sheetName = sheet.Name;

                //Get the changed and locked cell in a simple coord list.
                simpleCoordList changedCells = null;
                simpleCoordList lockedCells = null;
                simpleCoordList[] sessionLockedCells = null;
                currentProtMngr.GetChangedAndLockedCells(currentView.HasAttributes, out changedCells, out lockedCells, out sessionLockedCells);

                //get the simple coord list of replicated all cells
                simpleCoordList replicateAllCells =  currentProtMngr.Replication.AllocateAllCells.ToSimpleCoordList(false);

                //get the simple coord list of replicated existing cells
                simpleCoordList replicateExistingCells = currentProtMngr.Replication.AllocateExistingCells.ToSimpleCoordList(false);

                //get the simple coord list of replicated all cells
                simpleCoordList liftAllCells = currentProtMngr.Lift.AllocateAllCells.ToSimpleCoordList(false);

                //get the simple coord list of replicated existing cells
                simpleCoordList liftExistingCells = currentProtMngr.Lift.AllocateExistingCells.ToSimpleCoordList(false);

                //get the simple coord list of protected cells
                simpleCoordList protectedCells = currentProtMngr.ProtectedCellsSet.ToSimpleCoordList();

                //Get user selections
                pafUserSelection[] pafUserSel = actionsPane.GetCachedDyanmicPafUserSelections(viewName);
                //suppress zero settings.
                bool[] suppressZero = actionsPane.GetCachedSuppressZeroSelections(viewName);
                //Get user selections parent first.
                bool? colParentFirst = actionsPane.GetParentFirst(ViewAxis.Col, pafUserSel);
                bool? rowParentFirst = actionsPane.GetParentFirst(ViewAxis.Row, pafUserSel);


                //Get the data slice from the grid.
                int colCount = 0;
                pafDataSlice pafdslice = new pafDataSlice();
                pafdslice.compressed = true;
                pafdslice.data = null;


                //pafdslice.data.SetValue(new double(), 1);
                //TTN-1987 - added ServerSortSpecified
                if (currentOlapView.IsSorted || currentOlapView.ServerSortSpecified)
                {
                    pafdslice.compressedData = PafApp.GetViewMngr().UnSortedDataSlice(out colCount);
                }
                else
                {
                    string s = currentOlapView.GetGridDataBase64(out colCount);
                    pafdslice.compressedData = s;
                }
                pafdslice.columnCount = colCount;

                pafView outPafView;

                //Evaluate the view.
                pafDataSlice dataSlice = PafApp.GetServiceMngr().EvaluateView(
                    pafdslice,
                    changedCells,
                    lockedCells,
                    protectedCells,
                    replicateAllCells,
                    replicateExistingCells,
                    liftAllCells,
                    liftExistingCells,
                    sessionLockedCells,
                    null,//formulas
                    PafApp.RuleSetName,
                    PafApp.GetPafAuthResponse().securityToken.sessionToken,
                    PafApp.ClientId,
                    viewName,
                    suppressZero[0],
                    suppressZero[1],
                    out outPafView);

                //Populate data
                Globals.ThisWorkbook.Application.EnableEvents = false;
                Globals.ThisWorkbook.Application.ScreenUpdating = false;


                if (outPafView != null && !outPafView.dirtyFlag)
                {
                    Stopwatch startTime2 = Stopwatch.StartNew();

                    //Set the is suppressed setting on the view section.
                    currentOlapView.IsSuppressed = outPafView.viewSections[0].suppressed;

                    //if the view is not dirty, then we can sort the data slice if necessary.
                    //TTN-1987 - added ServerSortSpecified
                    if (currentOlapView.IsSorted || currentOlapView.ServerSortSpecified)
                    {
                        //resort the dataslice data
                        viewMngr.SortPafDataSlice(dataSlice, currentOlapView.CurrentSortOrder);
                    }

                    //Populate the data
                    currentOlapView.PopulateData(dataSlice);

                    //Make all the member tag managers grids dirty.
                    viewMngr.MakeAllMemberTagsDirtyExcept(0);

                    //populate, overlay the changed member tags, and set the bg fill color.
                    PafApp.GetMbrTagMngr().PopulateAndOverlayMemberTags(currentView);

                    //Apply Formatting
                    viewMngr.ResetFormatChanges();

                    viewMngr.ApplyBlankFormatting();

                    //Turn events and screen updating back on.
                    //TTN-1561 comment this out
                    //Globals.ThisWorkbook.Application.EnableEvents = true;
                    //Globals.ThisWorkbook.Application.ScreenUpdating = true;

                    //Clear Protection Manager
                    currentProtMngr.ClearProtection();

                    //Apply Non-Plannable & Forward Plannable Locks
                    currentProtMngr.AddSystemLocks();

                    //Add the locks for Singleton rules
                    currentProtMngr.AddSingletonRules();

                    //Add the locks for BOH rules
                    if (Settings.Default.BOHProtection)
                    {
                        currentProtMngr.AddBOHProtection();
                    }
                    //Add any existing session locks.
                    currentProtMngr.AddSessionLocks();

                    //viewMngr.ApplySessionLockProtectedFormats();

                    //Prune dependencies after formatting so protected cell show over locks.
                    currentProtMngr.PruneDependencies();

                    startTime2.Stop();
                    PafApp.GetLogger().InfoFormat("Apply Post-Evaluation formats, runtime: {0} (ms)", startTime2.ElapsedMilliseconds.ToString("N0"));
                }
                else if (outPafView != null && outPafView.dirtyFlag)
                {
                    PafApp.GetGridApp().UnprotectWorksheets();


                    //rebuild the view...
                    ExcelBuildView excelBuildView = new ExcelBuildView(sheet, viewName, true, outPafView.HasAttributes());
                    excelBuildView.InitializeGrid(outPafView, Settings.Default.AutoPrintRange, rowParentFirst: rowParentFirst, colParentFirst: colParentFirst);

                    //the view is dirty, so clear out any sort data.
                    viewMngr.CurrentOlapView.ClearSortData();

                    //get the cell notes
                    PafApp.GetCellNoteMngr().UpdateClientCacheFromServerCache(currentView.CellNotesAsArray);

                    //removed the cached intersecions.
                    PafApp.GetCellNoteMngr().ClearCachedIntersections(viewName);

                    //set the cell notes
                    PafApp.GetCellNoteMngr().RenderNotes(currentView);

                    //Set the treenode
                    //viewMngr.SetViewNode(hashCode, treeNode);

                    //Save the task pane status
                    PafApp.GetGridApp().SaveTaskPaneCurrentStatus();

                    //Protect our worksheets...
                    PafApp.GetGridApp().ProtectOurWorksheets();
                }

                //Set the view options.
                PafApp.GetGridApp().ActionsPane.RefreshDimensionControls(viewName, false);

                //Reapply the session lock protected formats
                viewMngr.ApplySessionLockProtectedFormats(true);

                //Set the database cache flag to dirty
                PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = true;

                //Set the flags of all the sheets to be clean.
                viewMngr.MakeAllProtMngPlanable();

                //Make all the other ProtectionMngr Dirty.
                viewMngr.MakeAllProtMngrDirtyExcept(hashCode);

                //Enable the Undo, & Calculate buttons
                PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();

                //TTN-791
                PafApp.GetGridApp().EnableLockButtons(true);

                //Enable the session lock buttons
                PafApp.GetGridApp().EnableSessionLockButtons(true);

                // Enable any custom menu buttons that we're disabled by pending calculations.
                PafApp.GetCommandBarMngr().EnableCustomCommandMenus(viewName);

                Globals.ThisWorkbook.Ribbon.InvalidateAll();

            }
            catch (SoapException exp)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;

                if (exp.Detail != null)
                {
                    XmlNode node = exp.Detail;
                    XmlDocument doc = new XmlDocument();

                    doc.LoadXml(node.InnerXml);
                    if (doc.DocumentElement.Equals(""))
                    {
                        PafApp.MessageBox().Show(new Exception(doc.InnerText));
                    }
                    else
                    {
                        PafApp.MessageBox().Show(exp);
                    }
                }
            }
            catch (NoDataException)
            {
                Globals.ThisWorkbook.Application.DisplayAlerts = false;
                sheet.Delete();
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                return;
            }
            finally
            {
                //Turn events and screen updating back on.
                startTime.Stop();
                Globals.ThisWorkbook.Application.EnableEvents = true;
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                Globals.ThisWorkbook.Application.DisplayAlerts = true;
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;
                if (sheet != null)
                {
                    PafApp.GetChangedCellMngr().ClearChangedCells(Convert.ToString(sheet.GetHashCode()));
                    
                }
                PafApp.GetLogger().Info("Memory Usage: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
                PafApp.GetLogger().Info("Completed view calculation on sheet: " + sheetName + ".");
                PafApp.GetLogger().InfoFormat("Total Evaluation runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));
                PafApp.GetLogger().Info("==========================================");
            }
        }

        /// <summary>
        /// Event handler for when a user authenicates to the server.
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="userId">User id that is logged onto the system.</param>
        public void userLogon_UserAuthenicated(object sender, string userId)
        {
            if (PafApp.GetPafAuthResponse() != null)
            {
                if (PafApp.GetPafAuthResponse().securityToken != null)
                {
                    //reset the user name.
                    PafApp.GetGridApp().RoleFilter.User = String.Empty;
                    PafApp.GetUowCalculator().UserId = String.Empty;
                    PafApp.GetGridApp().ShowTestHarnessToolbarButtons(PafApp.GetPafAuthResponse().securityToken.admin);
                }
            }
        }

        /// <summary>
        /// Event handler for when a user changes their role or Process/season,
        /// also handles when the user logins to the system.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Role Event Arguments.</param>
        /// <param name="Cancel">Cancel the impending role change.</param>
        public void LogonEvent_InfoChanged(object sender, LogonEventArgs e, ref bool Cancel)
        {
            bool doesViewSheetExist = false;

            //First loop thu the sheets to see if a view sheet exists.
            foreach (Worksheet sheet in Globals.ThisWorkbook.Worksheets)
            {
                if (PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    doesViewSheetExist = true;
                    break;
                }
            }

            //If no view sheets exist, then we don't need to warn the user - so exit.
            if (!doesViewSheetExist)
            {
                PafApp.GetGridApp().ActionsPane.ClearCachedPafUserSelections();
                PafApp.GetGridApp().ActionsPane.ClearCachedUserSelections();
                PafApp.GetGridApp().ActionsPane.ClearSuppressedZeroSelections();
                PafApp.GetGridApp().ActionsPane.ClearActionsPane();
                //Reset Simple Trees

                //clear out any cell notes...
                PafApp.GetCellNoteMngr().ClearBuckets();
                PafApp.GetCellNoteMngr().ClearCache();
                //clear member tags
                PafApp.GetMbrTagMngr().MbrTagCache.Reset();
                PafApp.GetMbrTagMngr().MakeMbrTagsClean();
                PafApp.GetViewMngr().Views.Clear();
                //Logging on to a new server, so update the custom test if necessary.
                if (e.type == LogonEventArgs.ChangeType.Userid || 
                    e.type == LogonEventArgs.ChangeType.Password ||
                    e.type == LogonEventArgs.ChangeType.Userid_Relogin ||
                    e.type == LogonEventArgs.ChangeType.Server)
                {
                    ChangeCustomTitleText(Globals.ThisWorkbook);
                    //ttn-981
                    PafApp.LogoffServer();
                    PafApp.GetGridApp().RoleSelector.UserSelectedRole = String.Empty;
                    PafApp.GetGridApp().RoleSelector.UserPlanTypeSpecString = String.Empty;
                }
                return;
            }

            DialogResult res;
            string message;

            switch (e.type)
            {
                case (LogonEventArgs.ChangeType.Role):
                    //Warn the user.
                    message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ChangingRoles");
                    PafApp_PafServiceCall(null, new PafWebserviceCallEventArgs(PafApp.ClientId, PafApp.SessionToken, ServiceCallType.Role));
                    break;
                case (LogonEventArgs.ChangeType.SeasonProcess):
                    //Warn the user.
                    message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ChangingSeasonProcess");
                    PafApp_PafServiceCall(null, new PafWebserviceCallEventArgs(PafApp.ClientId, PafApp.SessionToken, ServiceCallType.SeasonProcess));
                    break;
                case LogonEventArgs.ChangeType.Userid:
                case LogonEventArgs.ChangeType.Password:
                    //Warn the user.
                    message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ChangingUserid");
                    break;
                case LogonEventArgs.ChangeType.Userid_Relogin:
                    message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.UseridRelogin");
                    break;
                case LogonEventArgs.ChangeType.Server:
                    //Warn the user.
                    message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ChangingServer");
                    break;
                case LogonEventArgs.ChangeType.FilteredSubtotalsOrSupInvInx:        // TTN-2656   
                case LogonEventArgs.ChangeType.RoleFilter:
                    message = PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.ChangingFilteredRole");
                    break;
                default:
                    return;
            }

            if (PafApp.TestHarnessRunning)
            {
                res = DialogResult.OK;
            }
            else
            {
                //Show the message box.
                res = MessageBox.Show(
                        message,
                        PafApp.GetLocalization().GetResourceManager().
                            GetString("Application.Title"),
                        MessageBoxButtons.YesNo,
                        MessageBoxIcon.Question);
            }

            //If they say "NO" then cancel the role change.
            if (res == DialogResult.No)
                Cancel = true;
            else
            {
                //Remove the cached user selections.
                PafApp.GetGridApp().ActionsPane.ClearCachedPafUserSelections();
                PafApp.GetGridApp().ActionsPane.ClearCachedUserSelections();
                PafApp.GetGridApp().ActionsPane.ClearSuppressedZeroSelections();
                PafApp.GetGridApp().ActionsPane.ClearActionsPane();
                PafApp.GetViewMngr().Views.Clear();
                //clear out any cell notes...
                PafApp.GetCellNoteMngr().ClearBuckets();
                PafApp.GetCellNoteMngr().ClearCache();
                //clear member tags.
                PafApp.GetMbrTagMngr().MbrTagCache.Reset();
                PafApp.GetMbrTagMngr().MakeMbrTagsClean();
                //TTN-880
                PafApp.GetSaveWorkMngr().DataWaitingToBeSaved = false;

                //Logging on to a new server, so update the custom test if necessary.
                if (e.type == LogonEventArgs.ChangeType.Userid ||
                    e.type == LogonEventArgs.ChangeType.Password ||
                    e.type == LogonEventArgs.ChangeType.Userid_Relogin ||
                    e.type == LogonEventArgs.ChangeType.Server)
                {
                    ChangeCustomTitleText(Globals.ThisWorkbook);
                    //ttn-981
                    PafApp.LogoffServer();
                    PafApp.GetGridApp().RoleSelector.UserSelectedRole = String.Empty;
                    PafApp.GetGridApp().RoleSelector.UserPlanTypeSpecString = String.Empty;
                }

                //TTN-1756
                Globals.ThisWorkbook.Application.ScreenUpdating = false;
                //TTN-1936
                Globals.ThisWorkbook.Application.EnableEvents = false;
                try
                {
                    //Remove all the view sheets.
                    PafApp.GetGridApp().RemoveViewSheets();
                }
                catch (Exception)
                {
                    Globals.ThisWorkbook.Application.ScreenUpdating = true;
                    Globals.ThisWorkbook.Application.EnableEvents = true;
                    Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;
                }
                
            }
        }

        #endregion Public Event Handlers

        #region Workbook Events

        // ReSharper disable InconsistentNaming
        /// <summary>
        /// Event handler for the workbook startup
        /// </summary>
        /// <param name="sender">Sender.</param>
        /// <param name="e">Event args.</param>
        /// <param name="wbk">Current workbook.</param>

        public void ThisWorkbook_Startup(object sender, EventArgs e, ThisWorkbook wbk)
        {
            PafApp.GetLogger().InfoFormat("Starting Pace Client Version: {0}", Assembly.GetExecutingAssembly().GetName().Version);
            PafApp.GetLogger().InfoFormat("{0}", PafApp.GetGridApp().Version);

            var path = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.PerUserRoamingAndLocal).FilePath;
            PafApp.GetLogger().Warn("Reading properties from: " + path);

            var globalPath = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;
            PafApp.GetLogger().Warn("Using global path from: " + globalPath);
            TestHarnessStartupInformation thsi = null;

            try
            {
                //Shut down a second instance of the application started in the same process (same instance of Excel)
                string procId = Environment.GetEnvironmentVariable("TitanProcID", EnvironmentVariableTarget.Process);
                string id = Convert.ToString(Process.GetCurrentProcess().Id);

                if (procId == id)
                {
                    Environment.SetEnvironmentVariable("TitanDuctTape", "True", EnvironmentVariableTarget.Process);
                    wbk.ThisApplication.EnableEvents = false;
                    MessageBox.Show(String.Format(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.OnlyOneTitanWorkbook"), wbk.Name ),
                            PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return;
                }
                //Capture the proc id for the test above.  
                if (procId == null)
                {
                    Environment.SetEnvironmentVariable("TitanProcID", Process.GetCurrentProcess().Id.ToString(), EnvironmentVariableTarget.Process);
                }
                //Clean up any existing Application worksheets in the workbook
                CleanExistingWorkBooks();

                //Check for the existance of the test harness startup file.
                string fileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) +
                    Path.DirectorySeparatorChar + PafAppConstants.TEST_HARNESS_STARTUP_FILE_NAME;
                fileName = fileName.Replace("file:\\", "");
                //if the file exists, then deserialize it.
                if (File.Exists(fileName))
                {
                    thsi = (TestHarnessStartupInformation)XmlDeserializer.Deserialize(
                            Type.GetType("Titan.Pace.TestHarness.Startup.TestHarnessStartupInformation", true),
                            fileName);

                    //if (thsi.Version == null || thsi.Version.Equals("0"))
                    //{
                    //    thsi = new TestHarnessStartupInformation();
                    //    thsi = UpgradeTestHarnessStartupInformation_Original.Update(fileName);
                    //}
                }

                //Check to see if we have to run the harness files.
                if (thsi != null && thsi.RunHarnessTests)
                {
                    RunAppFromConfigFile(wbk, thsi);
                }
                else if (thsi != null && thsi.RunUnitTests)
                {
                    //Hook for automated unit testing.
                    //object missing = System.Reflection.Missing.Value;

                    //File.Delete(Properties.Settings.Default.PafPath);
                    //wbk.Close(false, missing, missing);
                    //wbk.ThisApplication.Quit();
                    SetUpExcelEventHandlers(wbk);
                   // UnitTestMngr.UnitTest();
                }
                else
                {

                    //TTN-1409
                    string excelBuild = wbk.ThisApplication.Version + "." + wbk.ThisApplication.Build;
                    if (Settings.Default.ExcelVersion != excelBuild)
                    {
                        Settings.Default.Upgrade();
                        Settings.Default.ExcelVersion = excelBuild;
                        Settings.Default.Save();
                        Settings.Default.Reload();
                    }

                    Settings.Default.ResetSessionLockStatus();

                    wbk.Windows[1].Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");
                    _titleBarIcon = (System.Drawing.Icon)Resources.PIcon;
                    ChangeExcelIcon.SetExcelWindowIcon(_titleBarIcon.Handle, "XLMAIN", Globals.ThisWorkbook.Application.Caption);
                    wbk.Application.ActiveWindow.DisplayHeadings = Settings.Default.AutoColumnHeadings;
                    wbk.Application.ActiveWindow.DisplayWorkbookTabs = true;

                    //Set up the necessary Excel event handlers.
                    SetUpExcelEventHandlers(wbk);

                    //Build the list of buttons to disable.
                    BuildListOfButtonsToDisable(wbk.Application);
                    //make sure the excel menus are disabled(ttn-766, 778, 780).
                    //EnableDisableHostApplicationMenuOptions(true);
                    Globals.ThisWorkbook.Ribbon.InvalidateAll();


                    //Set up evaluate hot key
                    _hotKeyListener = new NativeWindowListener(wbk.Application.Hwnd, wbk.Application.ActiveWindow.Caption.ToString());
                    _hotKeyListener.RegisterGlobalHotKey(Keys.F9, 4);
                    _hotKeyListener.onShiftF9 += this.EvaluateView;
                    _hotKeyListener.onShiftF9 += PafApp.GetTestHarnessManager().TestHarnessHotKeyListener_onShiftF9;


                    //_MouseHook = new MouseHook();
                    //_MouseHook._MouseClicked += new MouseHook.MouseClicked(_MouseHook__MouseClicked);

                    //Log Excel memory info
                    PafApp.GetGridApp().DisplayMemory();

                    //Fix Jira issue TTN-115, TTN-200, & TTN-1064.
                    //Always set the task pane status to TRUE.
                    PafApp.GetGridApp().SaveTaskPaneStatus(true);
                    //Settings.Default.TaskPaneStatus = true;
                    //Settings.Default.Save();
                    //Settings.Default.Reload();

                    if (PafApp.GetGridApp().ShowLogonDialogBox())
                    {
                        //change the caption to the custom caption if it exists.
                        ChangeCustomTitleText(wbk);
                        if (PafApp.GetGridApp().ShowRoleSelectionDialogBox())
                        {
                            PafApp.GetCommandBarMngr().ShouldRuleSetComboBoxBeBuilt = true;
                            PafApp.GetCommandBarMngr().CreateToolbars();
                            PafApp.GetCommandBarMngr().CreateMenu();
                            PafApp.GetCommandBarMngr().EnableRolesMenuandToolbarButtons(true);
                            PafApp.GetCommandBarMngr().ShowRoleFilterToolbarButton();
                            PafApp.GetCommandBarMngr().EnableOptionsMenuButton(true);
                            PafApp.GetCommandBarMngr().EnableCreateAssortmentMenuButton(true);
                            PafApp.GetCommandBarMngr().EnableChangePasswordMenuButton(true);
                            PafApp.GetCommandBarMngr().AddButtonsToRightClickMenu();
                            PafApp.GetCommandBarMngr().CreateTestHarnessToolbar();
                            PafApp.GetGridApp().BuildActionsPane();
                            PafApp.GetCommandBarMngr().SetTaskPaneStatus(true);
                            //Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = true;
                        }
                        else
                        {
                            PafApp.GetCommandBarMngr().ShouldRuleSetComboBoxBeBuilt = false;
                            PafApp.GetCommandBarMngr().CreateToolbars();
                            PafApp.GetCommandBarMngr().CreateMenu();
                            PafApp.GetCommandBarMngr().EnableRolesMenuandToolbarButtons(true);
                            PafApp.GetCommandBarMngr().ShowRoleFilterToolbarButton();
                            PafApp.GetCommandBarMngr().EnableOptionsMenuButton(true);
                            PafApp.GetCommandBarMngr().AddButtonsToRightClickMenu();
                            PafApp.GetCommandBarMngr().CreateTestHarnessToolbar();
                            PafApp.GetCommandBarMngr().EnableChangePasswordMenuButton(true);
                            throw new SecurityException(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.Class.PafApp.NoRoleSelected"));
                        }

                    }
                    else
                    {
                        PafApp.GetCommandBarMngr().ShouldRuleSetComboBoxBeBuilt = false;
                        PafApp.GetCommandBarMngr().CreateToolbars();
                        PafApp.GetCommandBarMngr().CreateMenu();
                        PafApp.GetCommandBarMngr().AddButtonsToRightClickMenu();
                        PafApp.GetCommandBarMngr().ShowRoleFilterToolbarButton();
                        throw new SecurityException(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.Class.PafApp.Login"));
                    }
                }
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                
            }
            catch (OutOfDateClientException e1)
            {
                PafApp.GetLogger().Error(e1);
                Globals.ThisWorkbook.Application.Quit();
            }
            catch (COMException ex)
            {
                PafApp.GetLogger().Error(ex);
            }
            catch (SecurityException ex)
            {
                MessageBox.Show(ex.Message, PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"), MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            catch (ThreadAbortException)
            {
                //Fix TTN-332
                wbk.Close(false, Missing.Value, Missing.Value);
                return;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex.Message,
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                    ex.StackTrace);
                return;
            }
            finally
            {
                //Fix Jira issue TTN-115, TTN-200, & TTN-1064.
                //Always set the task pane status to TRUE.
                PafApp.GetGridApp().SaveTaskPaneStatus(true);
                PafApp.GetGridApp().ShowHideTaskPane(true);
                //Settings.Default.TaskPaneStatus = true;
                //Settings.Default.Save();
                //Settings.Default.Reload();
            }
        }


        /// <summary>
        /// Shutdown event handler for the Excel workbook.
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="e">Event args.</param>
        public void ThisWorkbook_Shutdown(object sender, EventArgs e)
        {

            //Uncomment for clipboard monitoring...
            //clipboardRing.Dispose();

            //get rid of the mouse hook.

            //Logoff the Server
            PafApp.EndPlanningSession();

            //Fix Jira issues 95 + 154
            Environment.SetEnvironmentVariable("TitanProcID", null, EnvironmentVariableTarget.Process);
            Environment.SetEnvironmentVariable("TitanDuctTape", null, EnvironmentVariableTarget.Process);

            PafApp.GetCommandBarMngr().DeleteMenu();

            //Disable hooks for capturing hot keys
            _hotKeyListener.UnregisterGlobalHotKey();
            _hotKeyListener = null;

            //Remove the buttons from the right click menu.
            PafApp.GetCommandBarMngr().ResetRightClickMenu();

            //Force garbage collection
            GC.Collect();
            GC.WaitForPendingFinalizers();



            PafApp.GetGridApp().DisplayMemory();
            PafApp.GetLogger().Info("End Program");
        }

        /// <summary>
        /// Activate event handler for the Excel workbook.
        /// </summary>
        /// <param name="Wn">The current Excel window.</param>
        public void ThisWorkbook_WindowActivate(Window Wn)
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook window activate.");

            //See if the sheet has a view on it.
            bool val = PafApp.GetGridApp().DoesSheetContainCustomProperty((Worksheet)Wn.ActiveSheet, PafAppConstants.OUR_VIEW_PROP_VALUE);

            //Grab the HSV double click setting
            PafApp.GetGridApp().getHSVDoubleClickingButtonSetting(true);

            //Grab the Essbase Add-In settings
            PafApp.GetGridApp().getEssbaseDoubleClicking(true);
            PafApp.GetGridApp().getEssbaseSecondaryButtonSetting(true);

            //TTN-1064
            //PafApp.GetGridApp().SaveTaskPaneStatus(true);
            //PafApp.GetGridApp().ShowHideTaskPane(true);


            PafApp.GetGridApp().ShowHideTaskPane(Settings.Default.TaskPaneStatus);

            //If it's our sheet...
            if (val)
            {
                //Restore Hyperion Smart View Global Options
                PafApp.GetGridApp().SetHSVAddInGlobalOptions(true);

                //Restore Essbase Global Options
                PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(true);

                //Hide Essbase AddIn
                PafApp.GetGridApp().ShowHideEssbaseAddIn(false);

                //Hide Hyprion Smart View AddIn
                PafApp.GetGridApp().ShowHideHSVAddIn(false);
            }
            else
            {
                //Restore Hyperion Smart View Global Options
                PafApp.GetGridApp().SetHSVAddInGlobalOptions(false);

                //Restore Essbase Global Options
                PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(false);
            }

            Globals.ThisWorkbook.Application.DisplayDocumentActionTaskPane = Settings.Default.TaskPaneStatus;
            //Fix (TTN-317) - changed true values to val which tells us if this is our sheet or not.
            PafApp.GetCommandBarMngr().ShowCustomApplicationBars(true, true, true);
            Globals.ThisWorkbook.Ribbon.InvalidateAll();
            Globals.ThisWorkbook.Ribbon.SelectOurTab();
        }

        /// <summary>
        /// Deactivate event handler for the Excel workbook.
        /// </summary>
        /// <param name="Wn">The current Excel window.</param>
        public void ThisWorkbook_WindowDeactivate(Window Wn)
        {
            // The first time a view is selected after a second sheet has been opened by a user (and forced to close by the app),
            // this method is called in error.  The call is made from SelectView after EnableEvents is set to True.
            // The environment var flags the code to essentially ignore this method.
            if (Environment.GetEnvironmentVariable("TitanDuctTape", EnvironmentVariableTarget.Process) == "True")
            {
                Environment.SetEnvironmentVariable("TitanDuctTape", null, EnvironmentVariableTarget.Process);
            }
            else
            {
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook window deactivate");
                PafApp.GetGridApp().SaveTaskPaneCurrentStatus();
                PafApp.GetCommandBarMngr().ShowCustomApplicationBars(false, false, false);
                PafApp.GetCommandBarMngr().ShowRightClickMenuButtons(false);
            }

            //See if the sheet has a view on it.
            bool val = PafApp.GetGridApp().DoesSheetContainCustomProperty((Worksheet)Wn.ActiveSheet, PafAppConstants.OUR_VIEW_PROP_VALUE);

            //If it's our sheet...
            if (val)
            {

                //Restore Hyperion Smart View Global Options when the workbook closes
                PafApp.GetGridApp().SetHSVAddInGlobalOptions(false);

                //Restore Essbase Global Options
                PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(false);

                //Show Essbase AddIn
                PafApp.GetGridApp().ShowHideEssbaseAddIn(true);

                //Show Hyprion Smart View AddIn
                PafApp.GetGridApp().ShowHideHSVAddIn(true);
            }
        }

        /// <summary>
        /// Activate event handler for the Excel workbook.
        /// </summary>
        /// <param name="Cancel">Cancel the close event.</param>
        public void ThisWorkbook_BeforeClose(ref bool Cancel)
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook Before close.");
            DialogResult res = PafApp.GetGridApp().Calculate();
            if (res == DialogResult.Cancel)
            {
                Cancel = true;
                return;
            }

            res = PafApp.GetGridApp().SaveWork(false, false, false);
            if (res == DialogResult.Cancel)
            {
                Cancel = true;
                return;
            }

            //See if the sheet has a view on it.
            bool val = PafApp.GetGridApp().DoesSheetContainCustomProperty((Worksheet)Globals.ThisWorkbook.ActiveSheet, PafAppConstants.OUR_VIEW_PROP_VALUE);

            //If it's our sheet...
            if (val)
            {
                //Restore Essbase Global Options
                PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(false);
            }

            //Makes Excel think this workbook has already been saved(Fix TTN-423).
            Globals.ThisWorkbook.Saved = true;
        }

        /// <summary>
        /// Event handler that occurs before the user right clicks a cell.
        /// </summary>
        /// <param name="Sh">Active worksheet.</param>
        /// <param name="Target">Target range.</param>
        /// <param name="Cancel">Cancel the right click.</param>
        public void ThisWorkbook_SheetBeforeRightClick(object Sh, Range Target, ref bool Cancel)
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook before right click.");
            Stopwatch startTime = Stopwatch.StartNew();
            try
            {
                if (PafApp.StartUp == false)
                {
                    //added for replication.
                    //if the user selects more a range of cells then rights clicks on them,
                    //Excel does not return which cell the clicked on.  It returns the
                    //cell in the upper right hand corener, so we just disable the menu.
                    //the user must only select a single cell for replication to work.
                    Range rng = Target;
                    bool isMax = rng.IsLarge();
                    long cellCount = rng.CellCount();
                    long rowCount = rng.RowCount();

                    PafApp.GetLogger().WarnFormat("Total number of cells selected: {0}", new object[] { cellCount.ToString("N0") });
                    PafApp.GetLogger().WarnFormat("Total number of rows selected: {0}", new object[] { rowCount.ToString("N0") });

                    if (!isMax)
                    {
                        if (cellCount > 1)
                        {
                            PafApp.GetCommandBarMngr().EnableLockAndPasteShapeButtons();
                            PafApp.GetCommandBarMngr().EnableReplicateButtons();
                            PafApp.GetGridApp().EnableCellNotesButtons(false, _cellNoteButtonsToDisable);
                        }
                        else
                        {
                            PafApp.GetCommandBarMngr().EnableLockAndPasteShapeButtons();
                            PafApp.GetCommandBarMngr().EnableReplicateButtons();
                            PafApp.GetCommandBarMngr().EnableCellNotesButtons(new Cell(Target.Row, Target.Column), _cellNoteButtonsToDisable);
                        }
                        PafApp.GetCommandBarMngr().ContextMenuHandler(Sh.GetHashCode());
                    }

                    startTime.Stop();
                    PafApp.GetLogger().Info("Memory Usage: " + Win32Utils.GetMemoryUsageInMb().ToString("N0") + " MB");
                    PafApp.GetLogger().InfoFormat("Complete ThisWorkbook_SheetBeforeRightClick, runtime: {0}", startTime.ElapsedMilliseconds.ToString("N0"));
                }

            }
            catch (NullReferenceException)
            {
                //The range is nothing so leave it alone.
            }
            catch (ArgumentException)
            {
                //The grid is nothing, so do nothing.
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return;
            }
        }

        /// <summary>
        /// Event that occurs when a user selects a new cell.
        /// </summary>
        /// <param name="Sh">Active worksheet.</param>
        /// <param name="Target">Target range.</param>
        public void ThisWorkbook_SheetSelectionChange(object Sh, Range Target)
        {
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;

            if (debugEnabled) PafApp.GetLogger().Debug("Workbook sheet selection change.");
            Globals.ThisWorkbook.Ribbon.InvalidateCellGroup();
            try
            {
                Worksheet worksheet = (Worksheet)Sh;

                //Make sure this is our sheet, if not then exit!
                if (!PafApp.GetGridApp().DoesSheetContainCustomProperty(worksheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    return;
                }

                bool maxCell = Target.IsLarge(debugEnabled);

                try
                {
                    if (_lastCell != null)
                    {
                        ////copy/delete the cell note.
                        PafApp.GetCellNoteMngr().CopyAndDeleteCellNotes(new Cell(_lastCell.Row, _lastCell.Column).CellAddress, PafApp.GetViewMngr().CurrentView);
                        Debug.WriteLine("Last cell: " + _lastCell.Row + ":" + _lastCell.Column);
                    }
                }
                catch (Exception)
                {
                    _lastCell = null;
                }

                if (!maxCell)
                {
                    PafApp.GetCommandBarMngr().EnableLockAndPasteShapeButtons();
                    PafApp.GetCommandBarMngr().EnableReplicateButtons();
                }
                //TTN-1277
                PafApp.GetCommandBarMngr().ResetSuppressZeroButtons();
                
                SearchForEmbeddedCharts();
            }
            catch (NullReferenceException)
            {
                //The range is nothing so leave it alone.
            }
            catch (ArgumentException)
            {
                //The grid is nothing, so do nothing.
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                return;
            }
            finally
            {
                if (Target != null)
                {
                    _lastCell = Target;
                }
            }
        }

        /// <summary>
        /// Event hander for the workbook before print event.
        /// </summary>
        /// <param name="cancel"></param>
        public void ThisWorkbook_BeforePrint(ref bool cancel)
        {
            if (!Settings.Default.AutoPrintRange) //Set the print range.
            {
                return;
            }

            // get active sheet
            Worksheet sheet = (Worksheet)Globals.ThisWorkbook.Application.ActiveWorkbook.ActiveSheet;

            //See if the sheet has a view on it.
            if (!PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
            {
                return;
            }


            ViewStateInfo view = PafApp.GetViewMngr().GetView(sheet.GetHashCode());

            if (view == null) return;

            IPrintStyle printStyle = view.PrintStyle;
            if (printStyle == null) return;
            if (view.GetOlapView() == null) return;

            view.GetGrid().SetPrintRange(new ContiguousRange(view.GetOlapView().HeaderRange.TopLeft, view.GetOlapView().DataRange.BottomRight), printStyle);

        }

        /// <summary>
        /// Event handler for the workbook Before Save event.
        /// </summary>
        /// <param name="SaveAsUI">Show the UI box?</param>
        /// <param name="Cancel">Cancel the save.</param>
        public void ThisWorkbook_BeforeSave(bool SaveAsUI, ref bool Cancel)
        {
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook before save.");
            try
            {
                //Globals.ThisWorkbook.Application.DisplayAlerts = false;
                //Cancel = true;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
                Cancel = true;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Sh"></param>
        /// <param name="Target"></param>
        /// <param name="Cancel"></param>
        public void ThisWorkbook_SheetBeforeDoubleClick(object Sh, Range Target, ref bool Cancel)
        {
            try
            {
                Worksheet worksheet = (Worksheet)Sh;
                //Make sure this is our sheet, if not then exit!
                if (!PafApp.GetGridApp().DoesSheetContainCustomProperty(worksheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    return;
                }
                int sheetHash = Target.Worksheet.GetHashCode();
                OlapView olapView = PafApp.GetViewMngr().GetOlapView(sheetHash);

                var targetCells =
                    Target.Cast<Range>()
                        .Select(cell => new Cell(cell.Row, cell.Column))
                        .Where(targetCell => !olapView.DataRange.InContiguousRange(targetCell))
                        .ToList();
                targetCells.Reverse();

                foreach (Cell targetCell in targetCells)
                {
                    if (olapView.ColRange.InContiguousRange(targetCell))
                    {
                        if (olapView.ExpandCellGroup(ViewAxis.Col, targetCell.CellAddress))
                        {
                            Cancel = true;
                            break;
                        }
                    }
                    if (olapView.RowRange.InContiguousRange(targetCell))
                    {
                        if (olapView.ExpandCellGroup(ViewAxis.Row, targetCell.CellAddress))
                        {
                            Cancel = true;
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Warn(ex.Message, ex);
            }
        }

        /// <summary>
        /// Handles sheet cell onchange events.
        /// </summary>
        /// <param name="Target">Excel range that is being changed.</param>
        public void ThisWorkbook_ChangeRange2(Range Target)
        {
            bool debugEnabled = PafApp.GetLogger().IsDebugEnabled;

            if (debugEnabled) PafApp.GetLogger().Debug("Workbook change range.");

            Stopwatch sw = Stopwatch.StartNew();

            //Called when a cell or cells on a worksheet are changed.
            if (debugEnabled) Debug.WriteLine("Delegate: You Changed Cells " + Target.get_Address(Type.Missing, Type.Missing, XlReferenceStyle.xlA1, Type.Missing, Type.Missing) + " on " + Target.Worksheet.Name);

            Worksheet sheet = Target.Worksheet;
            int sheetHash = Target.Worksheet.GetHashCode();
            bool applicationSheet = true;
            bool inCopyMode = Globals.ThisWorkbook.Application.CutCopyMode == XlCutCopyMode.xlCopy ? true : false;
            bool isMbrTag = false;

            try
            {
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlWait;

                //Make sure this is our sheet, if not then exit!
                if (!PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    applicationSheet = false;
                    return;
                }

                //Improves the behavior of the native Excel Group Sheets functionality.
                //The data range of each selected sheet tab is still changed, but no application errors result.
                if (Globals.ThisWorkbook.ActiveChart == null && Globals.ThisWorkbook.Worksheets.Count > 1)
                {
                    int selShtCnt = 1;
                    if (Globals.ThisWorkbook.Windows[1] != null && Globals.ThisWorkbook.Windows[1].SelectedSheets != null)
                    {
                        selShtCnt = Globals.ThisWorkbook.Windows[1].SelectedSheets.Count;
                    }
                    if (sheet.Equals(Globals.ThisWorkbook.ActiveSheet))
                    {
                        sheet.Select(Type.Missing);
                    }
                    else if (!sheet.Equals(Globals.ThisWorkbook.ActiveSheet) && selShtCnt == 1)
                    {
                        //TTN-1154
                        //If only a single sheet is select, then don't select anything and just let the event handler fire.
                    }
                    else
                    {
                        return;
                    }
                }

                //If the view is readonly, then throw a message and undo the action.
                if (!PafApp.GetViewMngr().CurrentView.Plannable)
                {
                    if (debugEnabled) PafApp.GetLogger().Debug("Sheet: " + sheet.Name + "(" + sheetHash + ") is not planable.");

                    int plannableView = PafApp.GetViewMngr().PlannableViewHashCode;

                    if (plannableView != 0)
                    {

                        string[] message = new string[]
                        {
                            sheet.Name,
                            PafApp.GetGridApp().GetWorksheetFromHashCode(plannableView).Name
                        };

                        MessageBox.Show(
                            String.Format(PafApp.GetLocalization().GetResourceManager().
                                GetString("Application.MessageBox.ReadOnlyView"),
                                message),
                            PafApp.GetLocalization().GetResourceManager().
                                GetString("Application.Title"),
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation);

                        PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);
                        Globals.ThisWorkbook.Application.Undo();
                        PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                        return;
                    }
                }


                if (PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape)
                {
                    PasteWithoutProtection(Target, inCopyMode, sheetHash);
                    return;
                }

                Globals.ThisWorkbook.Application.ScreenUpdating = false;

                //create the Protection Mgr, and Olap variables.
                ProtectionMngr pMngr = PafApp.GetViewMngr().CurrentProtectionMngr;
                OlapView ov = PafApp.GetViewMngr().GetOlapView(sheetHash);

                //check for overlapping paste ranges.
                if (DoesPasteRangeHaveMemberTagsAndDataCells(Target, ov))
                {

                    MessageBox.Show(PafApp.GetLocalization().GetResourceManager().
                               GetString("Application.Exception.MemberTagInvalidPaste"),
                           PafApp.GetLocalization().GetResourceManager().
                               GetString("Application.Title"),
                           MessageBoxButtons.OK,
                           MessageBoxIcon.Exclamation);
                    PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);
                    Globals.ThisWorkbook.Application.Undo();
                    PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                    return;
                }

                //check for member tags.
                ExcelGridView.Utility.Range memberTagCells = null;
                foreach (Range cell in Target)
                {
                    Cell targetCell = new Cell(cell.Row, cell.Column);
                    memberTagDef tagDef;
                    if (ov.MemberTag.IsMemberTag(targetCell.CellAddress, out tagDef))
                    {
                        //memberTagCells = AddUpdateDeleteMemeberTag(Target, sheetHash, _PMngr);
                        isMbrTag = true;
                        break;
                        //PafApp.GetViewMngr().MakeAllMemberTagsDirtyExcept(sheetHash);
                    }
                }

                if (isMbrTag)
                {
                    memberTagCells = AddUpdateDeleteMemeberTag(Target, sheetHash, pMngr);
                    isMbrTag = true;
                    PafApp.GetViewMngr().MakeAllMemberTagsDirtyExcept(sheetHash);
                }

                //Replication
                Dictionary<CellAddress, ReplicationType> pastedCellToReplicate = new Dictionary<CellAddress, ReplicationType>();
                Dictionary<CellAddress, LiftType> pastedCellToLift = new Dictionary<CellAddress, LiftType>();
                //if there is a replication on the sheet, and we are in cut copy mode.
                if (pMngr.Replication.AllocationExists || pMngr.Lift.AllocationExists)
                {
                    //store the cell address, and replicaiton type, so we can rereplicate the cell after 
                    //the paste special value occurs.
                    foreach (Range cell in Target)
                    {
                        Cell targetCell = new Cell(cell.Row, cell.Column);
                        if (cell.Text != null)
                        {
                            double d;

                            string replicateCellText = pMngr.Replication.RemoveFormatInfoFromString(cell.Text.ToString(), cell.NumberFormat.ToString());
                            string liftCellText = pMngr.Lift.RemoveFormatInfoFromString(cell.Text.ToString(), cell.NumberFormat.ToString());

                            ReplicationType replicationType = pMngr.Replication.GetReplicationType(replicateCellText, out d);
                            LiftType liftType = pMngr.Lift.GetLiftType(liftCellText, out d);


                            if (replicationType != ReplicationType.None)
                            {
                                pastedCellToReplicate.Add(targetCell.CellAddress, replicationType);
                            }
                            else if (liftType != LiftType.None)
                            {
                                pastedCellToLift.Add(targetCell.CellAddress, liftType);
                            }
                        }
                    }
                }



                string rng = Target.get_Address(true, true, XlReferenceStyle.xlR1C1, Missing.Value, Missing.Value);
                ContiguousRange cr = new ContiguousRange(rng);
                if (cr.Cells.Count > 1)
                {
                    cr.PopulateData((object[,])Target.get_Value(XlRangeValueDataType.xlRangeValueDefault));
                }
                else
                {
                    object o = Target.get_Value(XlRangeValueDataType.xlRangeValueDefault);
                    cr.PopulateData(o);
                }
                ExcelGridView.Utility.Range range = new ExcelGridView.Utility.Range();
                range.AddContiguousRange(cr);

                foreach (ContiguousRange contiguousRange in range.ContiguousRanges)
                {
                    foreach (Cell cell in contiguousRange.Cells)
                    {
                        memberTagDef tagDef;

                        //cell.Value = valueArray[r,c];
                        if (!ov.MemberTag.IsMemberTag(cell.CellAddress, out tagDef))
                        {
                            //if the changed cell is in the data range on the view
                            if (PafApp.GetViewMngr().CurrentOlapView.DataRange.InContiguousRange(cell))
                            {
                                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Sheet: " + sheet.Name + "(" + sheetHash + ") adding cells to ChangedCellMngr.");

                                if (!cell.Value.IsDouble())
                                {
                                    double d;
                                    ReplicationType replicationType = pMngr.Replication.GetReplicationType(cell.Value, out d);
                                    if (replicationType != ReplicationType.None)
                                    {
                                        PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);
                                        PafApp.GetViewMngr().CurrentGrid.SetValueDouble(cell.CellAddress.Row, cell.CellAddress.Col, d);
                                        PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                                        pMngr.HandleChangedCells(cell.CellAddress, sheetHash.ToString(), sheet.Name, false);
                                        pMngr.Replication.ReplicateCell(cell.CellAddress, true, replicationType);
                                    }
                                    else
                                    {
                                        LiftType liftType = pMngr.Lift.GetLiftType(cell.Value, out d);
                                        if (liftType != LiftType.None)
                                        {
                                            PafApp.GetViewMngr().CurrentGrid.EnableEvents(false);
                                            PafApp.GetViewMngr().CurrentGrid.SetValueDouble(cell.CellAddress.Row, cell.CellAddress.Col, d);
                                            PafApp.GetViewMngr().CurrentGrid.EnableEvents(true);
                                            pMngr.HandleChangedCells(cell.CellAddress, sheetHash.ToString(), sheet.Name, false);
                                            pMngr.Lift.LiftCell(cell.CellAddress, true, liftType);
                                        }
                                    }
                                }
                                else if (!pMngr.Replication.IsCellAllocated(cell.CellAddress) || !pMngr.Lift.IsCellAllocated(cell.CellAddress))
                                {
                                    pMngr.HandleChangedCells(cell.CellAddress, Convert.ToString(sheetHash), sheet.Name, false);
                                    //if a copy has occured, then put the original format back into the changed cell mngr.
                                    //this will not capture AutoFills, Auto fills will be handled just as if a user
                                    //made a change to a cell, then changed the format of the cell.
                                    if (inCopyMode)
                                    {

                                        Format tempFormat = PafApp.GetFormattingMngr().GetDefaultCellFormat(cell.CellAddress);
                                        if (tempFormat != null)
                                        {
                                            ChangedCellInfo cc = new ChangedCellInfo(Convert.ToString(sheetHash), cell.CellAddress, Change.UserChanged, tempFormat);
                                            PafApp.GetChangedCellMngr().ChangeCell(cc, PafApp.GetChangedCellMngr().SheetCellTracker[Convert.ToString(sheetHash)], false);
                                        }
                                    }
                                }
                                else if (pMngr.Lift.IsCellAllocated(cell.CellAddress))
                                {
                                    //user changed the value in a Lift cell.
                                    pMngr.Lift.ReAllocateCell(cell.CellAddress);
                                }
                                else if (pMngr.Replication.IsCellAllocated(cell.CellAddress))
                                {
                                    //user changed the value in a replicated cell.
                                    pMngr.Replication.ReAllocateCell(cell.CellAddress);
                                }
                            }
                            //replicate the pasted cells.
                            if (pastedCellToReplicate.ContainsKey(cell.CellAddress) || pastedCellToLift.ContainsKey(cell.CellAddress))
                            {
                                //restore the original formatting, this is a workaround for a undo pastespecial which we removed.
                                PafApp.GetFormattingMngr().RestoreOriginalCellFormat(cell.CellAddress);
                                //Create a new changed cell info object, with the original format and push it onto the changed cell stack.
                                ChangedCellInfo cci = new ChangedCellInfo(sheet.GetHashCode().ToString(), cell.CellAddress, Change.ReplicateAll)
                                {
                                    CellAddress = cell.CellAddress,
                                    CellFormat = PafApp.GetViewMngr().CurrentView.DefaultFormats[cell.CellAddress.GetHashCode()]
                                };
                                PafApp.GetChangedCellMngr().ChangeCell(cci, PafApp.GetChangedCellMngr().SheetCellTracker[sheet.GetHashCode().ToString()], false);

                                if (pastedCellToReplicate.ContainsKey(cell.CellAddress))
                                {
                                    //replicate the cell.
                                    pMngr.Replication.ReplicateCell(cell.CellAddress, true, pastedCellToReplicate[cell.CellAddress]);
                                }
                                else if (pastedCellToLift.ContainsKey(cell.CellAddress))
                                {
                                    //Lift it.
                                    pMngr.Lift.LiftCell(cell.CellAddress, true, pastedCellToLift[cell.CellAddress]);
                                }

                            }
                        }
                    }
                }

                bool changedRanges = pMngr.ChangedRange.ContiguousRanges != null && pMngr.ChangedRange.ContiguousRanges.Count > 0;

                //Set the color for the changed Cells
                if (changedRanges)
                {
                    PafApp.GetViewMngr().CurrentGrid.SetBgFillColor(pMngr.ChangedRange, PafApp.GetPafAppSettingsHelper().GetSystemLockColor());
                }
                if (isMbrTag && memberTagCells != null && memberTagCells.CellCount > 0)
                {
                    PafApp.GetViewMngr().CurrentGrid.SetBgFillColor(memberTagCells, PafApp.GetPafAppSettingsHelper().GetMemberTagChangedColor());
                }

                //Next, clear the list of changed cell addresses
                pMngr.ChangedRange = null;

                if (changedRanges)
                {
                    pMngr.PruneDependencies();
                }

                Change changeType = PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape ? Change.PasteShape : Change.UserChanged;

                #region Test Harness Change Range
                //Fire the events, if the test harness is running.
                if (PafApp.GetTestHarnessManager().IsRecording)
                {
                    CellAddress ca = new CellAddress(Target.Row, Target.Column);
                    //check for a replciation
                    if (pMngr.Replication.IsCellAllocated(ca))
                    {
                        ReplicationType replicaitonType = pMngr.Replication.GetReplicationType(ca);

                        switch (replicaitonType)
                        {
                            case ReplicationType.ReplicateAll:
                                changeType = Change.ReplicateAll;
                                break;
                            case ReplicationType.ReplicateExisting:
                                changeType = Change.ReplicateExisting;
                                break;
                        }
                    }
                    else if (pMngr.Lift.IsCellAllocated(ca))
                    {
                        LiftType replicaitonType = pMngr.Lift.GetLiftType(ca);

                        switch (replicaitonType)
                        {
                            case LiftType.LiftAll:
                                changeType = Change.LiftAll;
                                break;
                            case LiftType.LiftExisting:
                                changeType = Change.LiftExisting;
                                break;
                        }
                    }

                    ProtectionMngr protectionMngr = PafApp.GetViewMngr().CurrentProtectionMngr;
                    List<Intersection> globalLocks = new List<Intersection>();
                    if (protectionMngr.GlobalLocks != null)
                    {
                        globalLocks = new List<Intersection>(protectionMngr.GlobalLocks.AllUserSessionParentLocks().ToIntersections());
                    }

                    if (Target.Count == 1)
                    {
                        if (AddChangedCell != null)
                        {
                            AddChangedCell(
                                this,
                                new AddChangedCellEventArgs(
                                    sheet.Name,
                                    Convert.ToString(sheetHash),
                                    changeType,
                                    new Cell(Target.Row, Target.Column, Target.Value2),
                                    new ProtectionMngrListTracker(
                                        new List<Intersection>(protectionMngr.ProtectedCellsSet.ToIntersections()),
                                        new List<Intersection>(protectionMngr.Changes),
                                        new List<Intersection>(protectionMngr.UserLocks),
                                        new List<Intersection>(protectionMngr.UnLockedChanges),
                                        new List<Intersection>(protectionMngr.Replication.AllocateAllCells),
                                        new List<Intersection>(protectionMngr.Replication.AllocateExistingCells),
                                        new List<Intersection>(protectionMngr.Lift.AllocateAllCells),
                                        new List<Intersection>(protectionMngr.Lift.AllocateExistingCells),
                                        globalLocks)));
                        }
                    }
                    else
                    {
                        if (AddChangedCells != null)
                        {
                            // get selected range
                            // ReSharper disable UseIndexedProperty
                            int row = Target.get_Offset(0, 0).Row;

                            int col = Target.get_Offset(0, 0).Column;
                            Cell topLeft = new Cell(row, col);

                            row = Target.get_Offset(Target.Rows.Count - 1, Target.Columns.Count - 1).Row;
                            col = Target.get_Offset(Target.Rows.Count - 1, Target.Columns.Count - 1).Column;
                            Cell bottomRight = new Cell(row, col);
                            // ReSharper restore UseIndexedProperty
                            ContiguousRange contigRange = new ContiguousRange(topLeft.CellAddress, bottomRight.CellAddress);

                            AddChangedCells(
                                this,
                                new AddChangedCellsEventArgs(
                                    sheet.Name,
                                    sheetHash.ToString(),
                                    changeType,
                                    contigRange,
                                    PafApp.GetViewMngr().CurrentGrid.GetValueObjectArray(contigRange),
                                    new ProtectionMngrListTracker(
                                        new List<Intersection>(protectionMngr.ProtectedCellsSet.ToIntersections()),
                                        new List<Intersection>(protectionMngr.Changes),
                                        new List<Intersection>(protectionMngr.UserLocks),
                                        new List<Intersection>(protectionMngr.UnLockedChanges),
                                        new List<Intersection>(protectionMngr.Replication.AllocateAllCells),
                                        new List<Intersection>(protectionMngr.Replication.AllocateExistingCells),
                                        new List<Intersection>(protectionMngr.Lift.AllocateAllCells),
                                        new List<Intersection>(protectionMngr.Lift.AllocateExistingCells),
                                        globalLocks)));

                        }
                    }
                }
                #endregion Test Harness Change Range

                //Make any other views read only.
                if (changedRanges)
                {
                    PafApp.GetViewMngr().MakeAllProtMngrNotPlanableExcept(sheetHash);
                }

                //Enable the toolbar buttons.
                PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();

                sw.Stop();
                if (debugEnabled) PafApp.GetLogger().Debug("ThisWorkbook_ChangeRange2, runtime: " + sw.ElapsedMilliseconds.ToString("N0") + " (ms)");

                if (debugEnabled) PafApp.GetLogger().Debug("Workbook change range complete.");
            }
            catch (Exception ex)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
                return;
            }
            finally
            {
                PafApp.GetGridApp().EnableEvents(true);
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;

                //Reset the PasteShape flag to false - it is only true when a user
                //uses the Paste Shape feature.
                if (applicationSheet)
                {
                    PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape = false;
                }
            }
        }

        // ReSharper restore InconsistentNaming

        /// <summary>
        /// Verifies that a sheet object is a worksheet or a chart sheet.
        /// </summary>
        /// <param name="sh">Sheet object(can be an worksheet, or a chart sheet object.</param>
        /// <returns>true if its a worksheet, or false if its a chart sheet.</returns>
        private static bool IsSheetAWorkSheet(object sh)
        {
            bool isWorkSheet = false;

            try
            {
                //Get the sheet from the com object.
                Worksheet sheet = (Worksheet)sh;
                isWorkSheet = true;
            }
            catch(Exception)
            {
            }

            return isWorkSheet;
        }

        /// <summary>
        /// Verifies selection is a range.
        /// </summary>
        /// <param name="selection">object(can be an chart sheet object, OLE object...</param>
        /// <returns>true if its a range, or false if it not(probably some embedded object).</returns>
        private static bool IsSelectionARange(object selection)
        {
            bool isRange = false;

            try
            {
                //Get the sheet from the com object.
                Range range = (Range)selection;
                isRange = true;
            }
            catch (Exception)
            {
            }

            return isRange;
        }

        /// <summary>
        /// Change the custom title text.
        /// </summary>
        /// <param name="wbk">The current workbook.</param>
        private void ChangeCustomTitleText(ThisWorkbook wbk)
        {
            if (PafApp.GetPafAppSettings() != null && PafApp.GetPafAppSettings().appTitle != null)
            {
                wbk.Windows[1].Caption =
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title") +
                        " - " + PafApp.GetPafAppSettings().appTitle;
            }
            else
            {
                wbk.Windows[1].Caption =
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");
            }
        }

        

        /// <summary>
        /// Add, update, delete member tags.
        /// </summary>
        /// <param name="range">range is being updated.</param>
        /// <param name="sheetHash">Hash code of the sheet that is being updated.</param>
        /// <param name="pManager">protection manager.</param>
        /// <returns></returns>
        private ExcelGridView.Utility.Range AddUpdateDeleteMemeberTag(Range range, int sheetHash, ProtectionMngr pManager)
        {
            //get the olap view.
            OlapView op = PafApp.GetViewMngr().GetOlapView(sheetHash);
            //create a new range.
            ExcelGridView.Utility.Range rng = new ExcelGridView.Utility.Range();
            foreach (Range cell in range)
            {
                //extrct a cell from the row/col
                Cell targetCell = new Cell(cell.Row, cell.Column);
                //create merged ranged.
                //GridView.Range mergeRng = new GridView.Range(targetCell.CellAddress);
                Cell topCell;
                //see if the range is merged.
                bool isMerged = op.Grid.IsMerged(new ExcelGridView.Utility.Range(targetCell.CellAddress), out topCell);

                //if the range is not merged, and this is the top left hand cell.
                if (isMerged && targetCell.CellAddress.Equals(topCell.CellAddress) ||
                    !isMerged)
                {
                    //get an intersection from a cell address.
                    Intersection inter = op.FindIntersection(targetCell.CellAddress);
                    //get the member tag def.
                    memberTagDef tagDef;
                    op.MemberTag.IsMemberTag(targetCell.CellAddress, out tagDef);
                    //if tagDef is != null
                    if (tagDef != null)
                    {
                        //fain a list of cell address from a intersection 
                        List<CellAddress> cellAdresses =
                            PafApp.GetViewMngr().CurrentOlapView.MemberTag.FindMemberTagIntersectionAddress(inter, tagDef.name);

                        bool memberTagMerged;
                        op.MemberTag.MemberTagMerged.TryGetValue(tagDef.name, out memberTagMerged);

                        //if the range is merged, becuase of a copy/move then unmerge it...
                        if(isMerged && !memberTagMerged)
                        {
                            op.Grid.UnMerge(new ContiguousRange(targetCell.CellAddress), PafAppConstants.SHEET_PASSWORD);
                        }


                        if (cellAdresses != null)
                        {
                            //for each cell address add then to a range.
                            foreach (CellAddress ca in cellAdresses)
                            {
                                rng.AddContiguousRange(ca);
                            }
                        }
                        else
                        {
                            //add the address to the range.
                            rng.AddContiguousRange(targetCell.CellAddress);
                        }

                        bool copyFormula = false;
                        switch (tagDef.type)
                        {
                            case memberTagType.FORMULA:
                                copyFormula = true;
                                break;
                        }

                        //handle the updates.
                        pManager.HandleChangedMemberTagCells(targetCell.CellAddress, sheetHash.ToString(),
                                                             range.Worksheet.Name, tagDef.name, copyFormula);
                        //add, update, delete the comment member tags.
                        op.MemberTag.AddUpdateDeleteCommentMemberTags(tagDef.name, inter, cell.Text.ToString().Trim());
                        //add the modified intersection to the member tag list.
                        ////op.MemberTag.MemberTagIntersections.Add(inter);
                        //make the changes to the expanded member tags.
                        switch (tagDef.type)
                        {
                            case memberTagType.TEXT:
                                PafApp.GetMbrTagMngr().MbrTagCache.AddUpdate(tagDef.name, inter, cell.Text.ToString().Trim());
                                break;
                            case memberTagType.FORMULA:
                                PafApp.GetMbrTagMngr().MbrTagCache.AddUpdate(tagDef.name, inter,
                                                                             cell.Formula.ToString().Trim());
                                break;
                            case memberTagType.HYPERLINK:
                                op.Grid.SetHyperlinks(rng, cell.Text.ToString().Trim(), String.Empty, String.Empty,
                                                      cell.Text.ToString().Trim());
                                PafApp.GetMbrTagMngr().MbrTagCache.AddUpdate(tagDef.name, inter, cell.Text.ToString().Trim());
                                //PafApp.GetMbrTagMngr().MbrTagCache.AddUpdate(tagDef.name, inter, cell.Hyperlinks[1].Address);
                                break;
                        }
                    }
                }
            }
            return rng;
        }


        

        private void PasteWithoutProtection(Range target, bool inCopyMode, int sheetHash)
        {
            Worksheet sheet = target.Worksheet;
            bool applicationSheet = true;
            //bool isMbrTag = false;

            DateTime startTime = DateTime.Now;

            try
            {
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Paste w/o Protection...");
                // ReSharper disable UseIndexedProperty
                //Called when a cell or cells on a worksheet are changed.
                Debug.WriteLine("Delegate: You Changed Cells " +
                   target.get_Address(Type.Missing, Type.Missing,
                   XlReferenceStyle.xlA1, Type.Missing, Type.Missing)
                   + " on " + target.Worksheet.Name);

                //create the Protection Mgr, and Olap variables.
                ProtectionMngr pMngr = PafApp.GetViewMngr().CurrentProtectionMngr;
                //OlapView ov = PafApp.GetViewMngr().GetOlapView(sheetHash);

                string rng = target.get_Address(true, true, XlReferenceStyle.xlR1C1, Missing.Value, Missing.Value);
                ContiguousRange cr = new ContiguousRange(rng);
                ExcelGridView.Utility.Range range = new ExcelGridView.Utility.Range();
                range.AddContiguousRange(cr);
                // ReSharper restore UseIndexedProperty

                foreach (ContiguousRange contiguousRange in range.ContiguousRanges)
                {
                    foreach (Cell cell in contiguousRange.Cells)
                    {
                        Cell targetCell = cell;

                        if (PafApp.GetViewMngr().CurrentOlapView.DataRange.InContiguousRange(targetCell))
                        {
                            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Sheet: " + sheet.Name + "(" + sheetHash + ") adding cells to ChangedCellMngr.");
                            pMngr.HandleChangedCells(targetCell.CellAddress, sheetHash.ToString(), sheet.Name, false);


                            ////if a copy has occured, then put the original format back into the changed cell mngr.
                            ////this will not capture AutoFills, Auto fills will be handled just as if a user
                            ////made a change to a cell, then changed the format of the cell.
                            //if (inCopyMode)
                            //{
                                
                            //    Format tempFormat = PafApp.GetFormattingMngr().GetDefaultCellFormat(targetCell.CellAddress);
                            //    if (tempFormat != null)
                            //    {
                            //        ChangedCellInfo cc = new ChangedCellInfo(sheetHash.ToString(), targetCell.CellAddress, Change.UserChanged, tempFormat);
                            //        PafApp.GetChangedCellMngr().ChangeCell(cc, PafApp.GetChangedCellMngr().SheetCellTracker[sheetHash.ToString()], false);
                            //    }
                            //}
                        }
                    }
                }


                bool changedRanges = false;
                if (pMngr.ChangedRange.ContiguousRanges != null && pMngr.ChangedRange.ContiguousRanges.Count > 0)
                {
                    changedRanges = true;
                }

                //Set the color for the changed Cells
                if (changedRanges)
                {
                    PafApp.GetViewMngr().CurrentGrid.SetBgFillColor(pMngr.ChangedRange, PafApp.GetPafAppSettingsHelper().GetSystemLockColor());
                }

                //Next, clear the list of changed cell addresses
                pMngr.ChangedRange = null;

                if (changedRanges)
                {
                    pMngr.PruneDependencies();
                }

              
                Change changeType;
                if (PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape)
                {
                    changeType = Change.PasteShape;
                }
                else
                {
                    changeType = Change.UserChanged;
                }

                #region Test Harness Change Range
                //Fire the events, if the test harness is running.
                if (PafApp.GetTestHarnessManager().IsRecording)
                {

                    if (target.Count == 1)
                    {
                        if (this.AddChangedCell != null)
                        {

                            AddChangedCell(
                                this,
                                new AddChangedCellEventArgs(
                                    sheet.Name,
                                    sheetHash.ToString(),
                                    changeType,
                                    new Cell(target.Row, target.Column, target.Value2),
                                    new ProtectionMngrListTracker(
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.ProtectedCellsSet.ToIntersections()),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Changes),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.UserLocks),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.UnLockedChanges),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Replication.AllocateAllCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Replication.AllocateExistingCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Lift.AllocateAllCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Lift.AllocateExistingCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.GlobalLocks.AllUserSessionParentLocks().ToIntersections()))));
                        }
                    }
                    else
                    {
                        if (AddChangedCells != null)
                        {
                            // get selected range
                            int row = target.get_Offset(0, 0).Row;
                            int col = target.get_Offset(0, 0).Column;
                            Cell topLeft = new Cell(row, col);

                            row = target.get_Offset(target.Rows.Count - 1, target.Columns.Count - 1).Row;
                            col = target.get_Offset(target.Rows.Count - 1, target.Columns.Count - 1).Column;
                            Cell bottomRight = new Cell(row, col);

                            ContiguousRange contigRange = new ContiguousRange(topLeft.CellAddress, bottomRight.CellAddress);

                            AddChangedCells(
                                this,
                                new AddChangedCellsEventArgs(
                                    sheet.Name,
                                    sheetHash.ToString(),
                                    changeType,
                                    contigRange,
                                    PafApp.GetViewMngr().CurrentGrid.GetValueObjectArray(contigRange),
                                    new ProtectionMngrListTracker(
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.ProtectedCellsSet.ToIntersections()),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Changes),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.UserLocks),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.UnLockedChanges),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Replication.AllocateAllCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Replication.AllocateExistingCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Lift.AllocateAllCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.Lift.AllocateExistingCells),
                                        new List<Intersection>(PafApp.GetViewMngr().CurrentProtectionMngr.GlobalLocks.AllUserSessionParentLocks().ToIntersections()))));

                        }
                    }
                }
                #endregion Test Harness Change Range

                //Make any other views read only.
                if (changedRanges)
                {
                    PafApp.GetViewMngr().MakeAllProtMngrNotPlanableExcept(sheetHash);
                }

                //Enable the toolbar buttons.
                PafApp.GetCommandBarMngr().EnableMenuandToolbarButtons();

                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Paste w/o Protection change range complete.");

                DateTime endTime = DateTime.Now;

                TimeSpan diff = endTime - startTime;

                Debug.WriteLine("Total Paste w/o Protection time: " + diff.TotalSeconds + " (s)");

            }
            catch (Exception ex)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
                return;
            }
            finally
            {
                PafApp.GetGridApp().EnableEvents(true);
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                Globals.ThisWorkbook.Application.Cursor = XlMousePointer.xlDefault;

                //Reset the PasteShape flag to false - it is only true when a user
                //uses the Paste Shape feature.
                if (applicationSheet)
                {
                    PafApp.GetViewMngr().CurrentProtectionMngr.PasteShape = false;
                }
            }


        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Target"></param>
        /// <param name="olapView"></param>
        /// <returns></returns>
        public bool DoesPasteRangeHaveMemberTagsAndDataCells(Range Target, OlapView olapView)
        {
            bool ret = false;

            try
            {
                //Loop over the range.
                foreach (Range cell in Target)
                {
                    //Get the cell from the range.
                    Cell targetCell = new Cell(cell.Row, cell.Column);
                    memberTagDef mbrTagDef;
                    //If the cell is not part of a member tag range
                    if (!olapView.MemberTag.IsMemberTag(targetCell.CellAddress, out mbrTagDef))
                    {
                        //If the cell contains a character that's not a replication or lift character
                        if (PafApp.GetGridApp().RangeHasReplicationCharacters(cell) &&
                            PafApp.GetGridApp().RangeHasLiftCharacters(cell))
                        {
                            //TTN-2391 - If the cell is not a replication cell or member tag, then if the value is null replace it with a zero.
                            if (cell.Value2 == null)
                            {
                                cell.Value2 = 0;
                            }
                            else
                            {
                                ret = true;
                                break;
                            }
                        }
                    }
                }
                return ret;
            }
            catch (Exception ex)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
                return ret;
            }
        }

        #endregion Workbook Events

        #region Chart Events

        /// <summary>
        /// Parse the series and return the worksheet name and range address that going to be updated.
        /// </summary>
        /// <param name="series">The series to be updated.</param>
        /// <param name="workSheetName">out parm of the work sheet</param>
        /// <param name="rangeAddress">out parm of the range address</param>
        /// <returns>A worksheet object, or null if one is not found.</returns>
        private static Worksheet ParseSeries(Series series, out string workSheetName, out string rangeAddress)
        {
            workSheetName = String.Empty;
            rangeAddress = String.Empty;
            Worksheet workSheet = null;

            char comma = ',';
            char expPoint = '!';
            char singleQuote = '\'';


            string formula = series.Formula;
            int expPointPos = formula.LastIndexOf(expPoint);

            if (expPointPos > 0)
            {
                //output the char.
                //Debug.WriteLine(formula.Substring(expPointPos - 1));
                //Debug.WriteLine(formula.Substring(expPointPos - 1, 1));

                //get the position of the last comma
                int lastCommaPos = formula.IndexOf(comma, expPointPos);
                //check, because it is an optional parameter on the series function...
                if(lastCommaPos == -1)
                {
                    //have to take one off the the trailing ")"
                    lastCommaPos = formula.Length -1;
                }


                //if the "!" has a single quote just b4 it, then the 
                //sheet name has a space or some strange character...
                if (formula.Substring(expPointPos - 1, 1).Equals(singleQuote.ToString()))
                {
                    //it has a space...
                    int singleQuotePos = formula.LastIndexOf(singleQuote, expPointPos - 2);
                    //range that is being updated.
                    rangeAddress = formula.Substring(singleQuotePos, lastCommaPos - singleQuotePos);
                    //set the worksheet name.
                    workSheetName = formula.Substring(singleQuotePos, expPointPos - singleQuotePos);
                    //remove the single quotes.
                    workSheetName = workSheetName.Replace(singleQuote.ToString(), String.Empty);
                }
                else
                {
                    //no spaces, parse back to comma.
                    int commaPos = formula.LastIndexOf(comma, expPointPos);
                    //range that is being updated.
                    rangeAddress = formula.Substring(commaPos + 1, lastCommaPos - commaPos - 1);
                    //find the ! point in the forumla...
                    int qp = rangeAddress.IndexOf(expPoint);
                    if (qp > 0)
                    {
                        //set the worksheet name.
                        workSheetName = rangeAddress.Substring(0, qp);
                    }
                    else
                    {
                        //set the worksheet name.
                        workSheetName = formula.Substring(commaPos + 1, lastCommaPos - expPointPos - 1);
                    }
                }
            }
            else
            {
                return null;
            }

            //make sure we don't have any extra spaces.
            rangeAddress.Trim();
            workSheetName.Trim();

            try
            {
               //get the worksheet object.
                workSheet = (Worksheet)Globals.ThisWorkbook.Worksheets.get_Item(workSheetName);
            }
            catch(Exception)
            {
                //can't get the worksheet return null...
            }
            return workSheet;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="SeriesIndex"></param>
        /// <param name="PointIndex"></param>
        private void ActiveChart_SeriesChange(int SeriesIndex, int PointIndex)
        {
            try
            {
                Chart chart = _charts[Globals.ThisWorkbook.ActiveChart.GetHashCode().ToString()];
                Series series = (Series)chart.SeriesCollection(SeriesIndex);
                string rangeAddress;
                string workSheetName;

                //get the worksheet object.
                Worksheet workSheet = ParseSeries(series, out workSheetName, out rangeAddress);
                
                //if the worksheet is null, the return.
                if(workSheet == null)
                {
                    return;
                }

                //See if the sheet has a view on it, if it's not our sheet then return.
                if (!PafApp.GetGridApp().DoesSheetContainCustomProperty(workSheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    return;
                }
                else
                {
                    Range target;
                    //get the range from the worksheet.
                    target = workSheet.get_Range(rangeAddress, Type.Missing);

                    if (target.Rows.Count > 1)
                    {
                        target = (Range)target.get_Item(PointIndex, Type.Missing);
                    }
                    else
                    {
                        target = (Range)target.get_Item(Type.Missing, PointIndex);
                    }

                    //loop thur the range and check the intersection to see if anything is protected.
                    //if so don't let it go thru to the changerange event handler.
                    foreach (Range cell in target)
                    {
                        Cell targetCell = new Cell(cell.Row, cell.Column);
                        Intersection inter = PafApp.GetViewMngr().GetOlapView(workSheet.GetHashCode()).FindIntersection(targetCell.CellAddress);
                        ProtectionMngr pm = PafApp.GetViewMngr().Views[workSheet.GetHashCode()].GetProtectionMngr();
                        //if (pm.ProtectedCellsSet.Contains(inter) || pm.SystemLocks.Contains(inter))
                        int uniqueId = inter.UniqueID;
                        if (pm.ProtectedCells.Contains(uniqueId) || pm.SystemLocks.Contains(uniqueId))
                        {
                            //don't allow anything to get updated...
                            return;
                        }
                    }

                    //call change range to fire protection, etc.
                    ThisWorkbook_ChangeRange2(target);
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        #endregion Chart Events


        #region Sheet Events

        /// <summary>
        /// Event handler for Sheet Activate event.  Fires after deactivate.
        /// </summary>
        /// <param name="Sh">The Excel.Worksheet that has been activated.</param>
        public void SheetsActivateEvent(object Sh)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook.sheet activate.");

            Worksheet sheet = null;
            bool isWorkSheet = false;
            object missing = Missing.Value;

            if(IsSheetAWorkSheet(Sh))
            {
                //Get the sheet from the com object.
                sheet = (Worksheet)Sh;

                isWorkSheet = true;
            }

            try
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = false;
                if (!isWorkSheet)
                {
                    Chart chart = (Chart)Sh;

                    if (!_charts.ContainsKey(chart.GetHashCode().ToString()))
                    {
                        chart.SeriesChange += new ChartEvents_SeriesChangeEventHandler(ActiveChart_SeriesChange);
                        _charts.Add(Globals.ThisWorkbook.Application.ActiveChart.GetHashCode().ToString(), chart);
                    }

                    //log it.
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Chart: " + chart.Name + "(" + chart.GetHashCode() + ") is not ours.");
                    //Disable the toolbar...
                    PafApp.GetCommandBarMngr().SetToolbarEnabledStatus(false);
                    //Make sure all the dim controls are hidden (TTN-230)
                    PafApp.GetGridApp().ActionsPane.HideAllDimensionControls();
                    //Reset the selection on the view action pane.
                    PafApp.GetGridApp().ActionsPane.ResetViewTreeSelectedNode();
                    //Show Essbase AddIn
                    PafApp.GetGridApp().ShowHideEssbaseAddIn(true);
                    //Show Hyprion Smart View AddIn
                    PafApp.GetGridApp().ShowHideHSVAddIn(true);
                    //make sure the excel menus are disabled(ttn-765, 766, 778, 780).
                    //EnableDisableHostApplicationMenuOptions(false);
                    //TTN-1214 set the hyperlink buttons.
                    PafApp.GetGridApp().EnableHyperlinkButton(false, _hyperlinkMemberTagButtonsToEnable);
                    //TTN-1214 set the forumla buttons.
                    PafApp.GetGridApp().EnableFormulaButton(false, _formulaMemberTagButtonsToEnable);
                    //TTN-1214 set the cell notes button.
                    PafApp.GetGridApp().EnableCellNotesButtons(false, _cellNoteButtonsToDisable);

                    return;
                }
                else
                {
                    //Get the sheets hash code.
                    int sheetHash = sheet.GetHashCode();

                    //Removed the deleted Grids, remove any user deleted grids from the view manager.
                    RemoveDeletedGrids();

                    //Set the current view in the view mngr.
                    PafApp.GetViewMngr().SetCurrentView(sheetHash);
                    //Get the parent workbook.
                    Workbook wbk = (Workbook)sheet.Parent;

                    wbk.Application.ScreenUpdating = false;

                    //Disable the toolbar...
                    PafApp.GetCommandBarMngr().SetToolbarEnabledStatus(true);

                    //See if the sheet has a view on it.
                    bool val = PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);

                    //If it's our sheet...
                    if (val)
                    {
                        if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook.sheet activate.");

                        //Get the name of the view associated with the sheet.
                        string viewName = PafApp.GetGridApp().GetCustomProperty(
                                sheet, PafAppConstants.OUR_VIEW_PROP_VALUE).ToString();


                        _lastCell = GetActiveRange();
                        
                        //TTN-633
                        //Protect the sheet.
                        //PafApp.GetGridApp().ProtectSheet(sheet, PafAppConstants.SHEET_PASSWORD);

                        //If the view is readonly, lock it.
                        if (PafApp.GetViewMngr().ViewExists(sheetHash) &&
                            !PafApp.GetViewMngr().CurrentView.Plannable)
                        {
                            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Sheet: " + sheet.Name + "(" + sheetHash + ") is not planable.");

                            if (PafApp.GetViewMngr().CurrentOlapView.StartCell != null)
                            {
                                //Since the view is read only, lock down the entire datarange.
                                PafApp.GetViewMngr().CurrentGrid.Lockcells(
                                    PafApp.GetViewMngr().CurrentOlapView.DataRange);
                            }
                        }

                        //If the view is dirty, and plannable then update the view.
                        if (PafApp.GetViewMngr().ViewExists(sheetHash) &&
                            PafApp.GetViewMngr().CurrentView.Plannable &&
                            PafApp.GetViewMngr().CurrentView.Dirty)
                        {
                            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Sheet: " + sheet.Name + "(" + sheetHash + ") is dirty and needs to be updated.");

                            //Check to see if the view has dynamic selections.
                            if (PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName))
                            {
                                //Refresh the dynamic view.
                                PafApp.GetGridApp().ActionsPane.SelectViewTreeSelectedNode((TreeListNode)PafApp.GetViewMngr().CurrentViewNode);
                                RefreshView(viewName, sheet, true);
                            }
                            else
                            {
                                //Update the range.
                                SelectView(viewName,
                                    sheet,
                                    true,
                                    PafApp.GetViewMngr().CurrentViewNode);
                            }
                            //Set the ProtectionMngr to clean.
                            PafApp.GetViewMngr().CurrentView.Dirty = false;
                        }
                        else if (PafApp.GetViewMngr().ViewExists(sheetHash) && 
                            PafApp.GetViewMngr().CurrentView.DirtyMbrTagFormatting)
                        {
                            PafApp.GetViewMngr().ResetMemberTagFormatChanges(PafApp.GetViewMngr().CurrentView);
                            PafApp.GetViewMngr().CurrentView.DirtyMbrTagFormatting = false;
                        }

                        //==============
                        Globals.ThisWorkbook.Application.EnableEvents = false;
                        PafApp.GetCellNoteMngr().RenderNotes(PafApp.GetViewMngr().CurrentView);
                        //PafApp.GetCellNoteMngr().UpdateNotes(PafApp.GetViewMngr().CurrentView);
                        //==========

                        //=====
                        //populate, overlay the changed member tags, and set the bg fill color.
                        PafApp.GetMbrTagMngr().PopulateAndOverlayMemberTags(PafApp.GetViewMngr().CurrentView);
                        //======

                        //Refresh the controls on the actions pane.
                        PafApp.GetGridApp().ActionsPane.RefreshDimensionControls(viewName, false);

                        //Enable/Disable Custom menus
                        PafApp.GetCommandBarMngr().EnableCustomCommandMenus(viewName);

                        //Enabled/Disable the Sort Menu Button
                        PafApp.GetCommandBarMngr().EnableSortMenuButton(true);

                        Cell targetCell = new Cell(
                            sheet.Application.ActiveCell.Row,
                            sheet.Application.ActiveCell.Column);

                        //Setup the toolbar for the view sheet.
                        PafApp.GetCommandBarMngr().ViewSheetToolbarSetup(targetCell, sheet.Name);

                        //make sure the excel menus are disabled(ttn-765, 766, 778, 780).
                        //EnableDisableHostApplicationMenuOptions(false);
                        //TTN-1214 set the hyperlink buttons.
                        //PafApp.GetCommandBarMngr().EnableHyperlinkButton(targetCell, _HyperlinkMemberTagButtonsToEnable);
                        //TTN-1214 set the forumla buttons.
                        //PafApp.GetCommandBarMngr().EnableFormulaButton(targetCell, _FormulaMemberTagButtonsToEnable);
                        //TTN-1214 set the cell notes button.
                        //PafApp.GetCommandBarMngr().EnableCellNotesButtons(targetCell, _CellNoteButtonsToDisable);

                        //Reset the HSV Global Options          
                        PafApp.GetGridApp().SetHSVAddInGlobalOptions(true);

                        //Restore Essbase Global Options
                        PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(true);
                    }
                    else
                    {
                        //log it.
                        if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Sheet: " + sheet.Name + "(" + sheetHash + ") is not ours.");
                        //Disable all the toolbar buttons.
                        PafApp.GetCommandBarMngr().NonViewSheetToolbarSetup();
                        //Enable/Disable Custom menus
                        PafApp.GetCommandBarMngr().DisableCustomCommandMenus();
                        //Enabled/Disable the Sort Menu Button
                        PafApp.GetCommandBarMngr().EnableSortMenuButton(false);
                        //Make sure all the dim controls are hidden (TTN-230)
                        PafApp.GetGridApp().ActionsPane.HideAllDimensionControls();
                        //Reset the selection on the view action pane.
                        PafApp.GetGridApp().ActionsPane.ResetViewTreeSelectedNode();
                        //make sure the excel menus are disabled(ttn-765, 766, 778, 780).
                        //EnableDisableHostApplicationMenuOptions(true);
                        //TTN-1214 set the hyperlink buttons.
                        PafApp.GetGridApp().EnableHyperlinkButton(true, _hyperlinkMemberTagButtonsToEnable);
                        //TTN-1214 set the forumla buttons.
                        PafApp.GetGridApp().EnableFormulaButton(true, _formulaMemberTagButtonsToEnable);
                        //TTN-1214 set the cell notes button.
                        PafApp.GetGridApp().EnableCellNotesButtons(true, _cellNoteButtonsToDisable);
                        //Show Essbase AddIn
                        PafApp.GetGridApp().ShowHideEssbaseAddIn(true);
                        //Show Hyprion Smart View AddIn
                        PafApp.GetGridApp().ShowHideHSVAddIn(true);
                        //Reset the HSV Global Options          
                        PafApp.GetGridApp().SetHSVAddInGlobalOptions(false);
                        //Restore Essbase Global Options
                        PafApp.GetGridApp().SetEssbaseAddInGlobalOptions(false);
                        //Disable the view options on non-Pace view sheets.
                        PafApp.GetGridApp().ActionsPane.SetViewOptions(sheet.Name);
                    }
                }
                stopwatch.Stop();
                PafApp.GetLogger().InfoFormat("SheetsActivateEvent runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
            }
            catch (ArgumentException)
            {
                return;
            }
            catch (Exception ex)
            {
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                Globals.ThisWorkbook.Application.ScreenUpdating = true;
                Globals.ThisWorkbook.Application.EnableEvents = true;
            }
        }

        /// <summary>
        /// Event handler for sheet Deactivate event.  Fires before activate.
        /// </summary>
        /// <param name="Sh">The Excel.Worksheet that has been deactivated.</param>
        public void SheetsDeactivateEvent(object Sh)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Workbook.sheet deactivate");

            PafApp.GetCacheBlockAsync();

            //Save the position of the task pane.
            PafApp.GetGridApp().SaveTaskPaneCurrentStatus();
            //Create a com object
            Worksheet sheet = null;
            //Create a chart object.
            Chart chart = null;
            //create a workbook object.
            Workbook wbk;
            //is worksheet flag.
            bool isWorkSheet = false;
            //missing object.
            //object missing = Missing.Value;


            if(IsSheetAWorkSheet(Sh))
            {
                //Get the sheet from the com object.
                sheet = (Worksheet)Sh;
                isWorkSheet = true;
                //Get the parent workbook, so we can freeze the screen.
                wbk = (Workbook)sheet.Parent;
            }
            else
            {
                //set the chart object, with as cast.
                chart = (Chart)Sh;
                //Get the parent workbook, so we can freeze the screen.
                wbk = (Workbook)chart.Parent;
            }

            try
            {
                //Pause the screen.
                wbk.Application.ScreenUpdating = false;

                if(isWorkSheet)
                {
                    //See if the sheet has a view on it.
                    bool val =
                        PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE);

                    //If the sheet has a view on it, then unprotect the sheet.  If we don't then we get errors.
                    if (val)
                    {
                        if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Leaving sheet: " + sheet.Name + "(" + sheet.GetHashCode() + ").");
                        //Prompt the user to calcuate, if any calculations are pending.
                        PafApp.GetGridApp().Calculate(sheet, false);

                        //===========copy and update cell notes=========
                        _lastCell = null;
                        PafApp.GetCellNoteMngr().CopyAndWriteAllUpdatesInsertsDeletsToClientCache(
                            PafApp.GetViewMngr().CurrentView);
                        //=====cell notes===============================

                        if (PafApp.GetViewMngr().CurrentView != null && !PafApp.GetViewMngr().CurrentView.ViewHasBeenBuilt)
                        {
                            PafApp.GetGridApp().HideSheet(sheet.Name);
                        }

                        //TTN-633 (removed)
                        //Unprotect the sheet before leaving.
                        //if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("Unprotecting sheet: " + sheet.Name + "(" + sheet.GetHashCode() + ").");
                        //PafApp.GetGridApp().UnprotectSheet(sheet, PafAppConstants.SHEET_PASSWORD);
                    }
                    else //It's not our sheet - make sure it's not protected.
                    {
                        //TTN-633 (removed)
                        PafApp.GetGridApp().UnprotectSheet(sheet, "");

                        //When you leave a non-Pace sheet grab the HSV double click setting
                        PafApp.GetGridApp().getHSVDoubleClickingButtonSetting(true);

                        //When you leave a non-Pace sheet grab the Essbase Add-In settings
                        PafApp.GetGridApp().getEssbaseDoubleClicking(true);
                        PafApp.GetGridApp().getEssbaseSecondaryButtonSetting(true);
                    }
                }
            }
            catch (Exception ex)
            {
                wbk.Application.ScreenUpdating = true;
                PafApp.MessageBox().Show(ex);
                return;
            }
            finally
            {
                //No matter what search 4 charts, and unfreeze the screen.
                SearchForEmbeddedCharts();
                Globals.ThisWorkbook.Ribbon.InvalidateAll();
                wbk.Application.ScreenUpdating = true;
            }
            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("SheetsDeactivateEvent runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
        }
        #endregion Sheet Events

        #region Private Methods

        /// <summary>
        /// Enable or disable the host application menu options.
        /// </summary>
        /// <param name="enable">true to enable, false to disable.</param>
        private void EnableDisableHostApplicationMenuOptions1(bool enable)
        {
            try
            {
                foreach (CommandBarControl c in _buttonsToDisable)
                {
                    //c.Enabled = enable;
                }
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Clean up any existing Application worksheets in the workbook
        /// </summary>
        private static void CleanExistingWorkBooks()
        {
            foreach (Worksheet workSheet in Globals.ThisWorkbook.Worksheets)
            {
                //See if the sheet has a view on it.
                if (PafApp.GetGridApp().DoesSheetContainCustomProperty(
                    workSheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                {
                    //Remove custom property flagging the sheet as an application sheet
                    PafApp.GetGridApp().RemoveCustomProperty(
                        workSheet, PafAppConstants.OUR_VIEW_PROP_VALUE);
                    //Unprotect the sheet
                    PafApp.GetGridApp().UnprotectSheet(workSheet, PafAppConstants.SHEET_PASSWORD);
                    string workSheetName = PafApp.GetGridApp().MakeSheetNameUnique(workSheet.Name, false, true);
                    workSheet.Name = workSheetName;
                    //Set clear out any non-plannable coloring on the worksheet tab
                    workSheet.Tab.Color = false;
                }
            }
        }

        /// <summary>
        /// Builds a list of menu buttons to disable when one of "our" sheets is active.
        /// </summary>
        /// <param name="application">An excel applicaiton object.</param>
        private void BuildListOfButtonsToDisable(Microsoft.Office.Interop.Excel.Application application)
        {
            try
            {
                object missing = Missing.Value;

                CommandBar wb = application.CommandBars[
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Excel.CommandBars.WorksheetMenuBar")];

                CommandBar cb = application.CommandBars[
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Excel.CommandBars.ChartMenuBar")];

                CommandBar reviewBar = application.CommandBars[
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Excel.CommandBars.Reviewing")];

                CommandBar cellBar = application.CommandBars[
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Excel.CommandBars.Cell")];

                CommandBar stdBar = application.CommandBars[
                    PafApp.GetLocalization().GetResourceManager().GetString(
                        "Application.Excel.CommandBars.Standard")];

                _taskPane  = application.CommandBars["Task Pane"];
                //====================================
                //starta
                //cell notes

                //Review bar->new comment
                CommandBarControl comment = reviewBar.FindControl(
                    missing,
                    1589,
                    missing,
                    false,
                    true);
                if(comment != null)
                {
                    comment.Enabled = true;
                    _cellNoteButtonsToDisable.Add(comment);
                }
            
                //Review bar->delete comment
                CommandBarControl deleteComment = cellBar.FindControl(
                    missing,
                    1592,
                    missing,
                    false,
                    true);
                if (deleteComment != null)
                {
                    deleteComment.Enabled = true;
                    _cellNoteButtonsToDisable.Add(deleteComment);
                }

                //cellbar->insert comment
                CommandBarControl insertComment2 = cellBar.FindControl(
                    missing,
                    2031,
                    missing,
                    false,
                    true);
                if (insertComment2 != null)
                {
                    insertComment2.Enabled = true;
                    _cellNoteButtonsToDisable.Add(insertComment2);
                }

                //cellbar->delete comment
                CommandBarControl deleteComment2 = cellBar.FindControl(
                    missing,
                    1592,
                    missing,
                    false,
                    true);
                if (deleteComment2 != null)
                {
                    deleteComment2.Enabled = true;
                    _cellNoteButtonsToDisable.Add(deleteComment2);
                }


                //insert->comment
                CommandBarControl insertComment = wb.FindControl(
                    missing,
                    1589,
                    missing,
                    false,
                    true);
                if (insertComment != null)
                {
                    insertComment.Enabled = true;
                    _cellNoteButtonsToDisable.Add(insertComment);
                }

                //end     
                //cell notes
                //====================================


                //member tags

                //insert->Hyperlink
                CommandBarControl insertHyperlink = wb.FindControl(
                    missing,
                    1576,
                    missing,
                    false,
                    true);
                if (insertHyperlink != null)
                {
                    insertHyperlink.Enabled = false;
                    _hyperlinkMemberTagButtonsToEnable.Add(insertHyperlink);
                }

                CommandBarControl insertHyperlink2 = stdBar.FindControl(
                    missing,
                    1576,
                    missing,
                    false,
                    true);
                if (insertHyperlink2 != null)
                {
                    insertHyperlink2.Enabled = false;
                    _hyperlinkMemberTagButtonsToEnable.Add(insertHyperlink2);
                }

                //insert->Formula
                CommandBarControl insertFormula = wb.FindControl(
                    missing,
                    385,
                    missing,
                    false,
                    true);
                if (insertFormula != null)
                {
                    insertFormula.Enabled = false;
                    _formulaMemberTagButtonsToEnable.Add(insertFormula);
                }

                CommandBarControl insertFormula2 = stdBar.FindControl(
                    missing,
                    1576,
                    missing,
                    false,
                    true);
                if (insertFormula2 != null)
                {
                    insertFormula2.Enabled = false;
                    _formulaMemberTagButtonsToEnable.Add(insertFormula2);
                }

                //end member tags

                //web page preview for the worksheet bar.
                CommandBarControl webPagePreview = wb.FindControl(
                    missing,
                    3655,
                    missing,
                    false,
                    true);
                if (webPagePreview != null)
                {
                    webPagePreview.Enabled = false;
                    _buttonsToDisable.Add(webPagePreview);
                }

                //web page preview for the chart bar.
                CommandBarControl webPagePreview2 = cb.FindControl(
                    missing,
                    3655,
                    missing,
                    false,
                    true);
                if (webPagePreview2 != null)
                {
                    webPagePreview2.Enabled = false;
                    _buttonsToDisable.Add(webPagePreview2);
                }


                //protect workbok, for the worksheet bar.
                CommandBarControl protectWorkbook = wb.FindControl(
                    missing,
                    894,
                    missing,
                    false,
                    true);
                if (protectWorkbook != null)
                {
                    protectWorkbook.Enabled = false;
                    _disabledButtons.Add(protectWorkbook);
                    //this must alway be off in our workbook.
                }

                //protect workbok, for the chart bar.
                CommandBarControl protectWorkbook2 = cb.FindControl(
                    missing,
                    894,
                    missing,
                    false,
                    true);
                if (protectWorkbook2 != null)
                {
                    protectWorkbook2.Enabled = false;
                    _disabledButtons.Add(protectWorkbook2);
                    //this must alway be off in our workbook.
                }

                //protect and share workbook, for the worksheet bar.
                CommandBarControl protectAndShareWbk = wb.FindControl(
                    missing,
                    3059,
                    missing,
                    false,
                    true);
                if (protectAndShareWbk != null)
                {
                    protectAndShareWbk.Enabled = false;
                    _disabledButtons.Add(protectAndShareWbk);
                    //this must alway be off in our workbook.
                }


                //protect and share workbook, for chart bar.
                CommandBarControl protectAndShareWbk2 = cb.FindControl(
                    missing,
                    3059,
                    missing,
                    false,
                    true);
                if (protectAndShareWbk2 != null)
                {
                    protectAndShareWbk2.Enabled = false;
                    _disabledButtons.Add(protectAndShareWbk2);
                    //this must alway be off in our workbook.
                }


                //insert->Picture
                CommandBarControl insertPicture = wb.FindControl(
                    missing,
                    30180,
                    missing,
                    false,
                    true);
                if (insertPicture != null)
                {
                    insertPicture.Enabled = false;
                    _buttonsToDisable.Add(insertPicture);
                }


                //insert->Object
                CommandBarControl insertObject = wb.FindControl(
                    missing,
                    546,
                    missing,
                    false,
                    true);
                if (insertObject != null)
                {
                    insertObject.Enabled = false;
                    _buttonsToDisable.Add(insertObject);
                }

                //edit->fill
                CommandBarControl fill = wb.FindControl(
                    missing,
                    30020,
                    missing,
                    false,
                    true);
                if (fill != null)
                {
                    fill.Enabled = false;
                    _buttonsToDisable.Add(fill);
                }

                //edit->fill->Across Worksheets
                CommandBarControl editFillAcrWorkSht = wb.FindControl(
                    missing,
                    869,
                    missing,
                    false,
                    true);
                if (editFillAcrWorkSht != null)
                {
                    editFillAcrWorkSht.Enabled = false;
                    _disabledButtons.Add(editFillAcrWorkSht);
                    //this must alway be off in our workbook.
                }

                //edit->clear->all, for worksheet bar
                CommandBarControl editClearAll = wb.FindControl(
                    missing,
                    1964,
                    missing,
                    false,
                    true);
                if (editClearAll != null)
                {
                    editClearAll.Enabled = false;
                    _buttonsToDisable.Add(editClearAll);
                }

                //edit->clear->comments, for worksheet bar
                CommandBarControl editClearComments = wb.FindControl(
                    missing,
                    874,
                    missing,
                    false,
                    true);
                if (editClearComments != null)
                {
                    editClearComments.Enabled = false;
                    _buttonsToDisable.Add(editClearComments);
                }

                //edit->clear->all, for chart bar.
                CommandBarControl editClearAll2 = cb.FindControl(
                    missing,
                    1964,
                    missing,
                    false,
                    true);
                if (editClearAll2 != null)
                {
                    editClearAll2.Enabled = false;
                    _buttonsToDisable.Add(editClearAll2);
                }


                //edit->clear->Formats, for worksheet bar.
                CommandBarControl editClearFormats = wb.FindControl(
                    missing,
                    872,
                    missing,
                    false,
                    true);
                if (editClearFormats != null)
                {
                    editClearFormats.Enabled = false;
                    _buttonsToDisable.Add(editClearFormats);
                }

                ////view->Task Pane, for worksheet bar.
                //_TaskPane = wb.FindControl(
                //    missing,
                //    5746,
                //    missing,
                //    false,
                //    true);


                //edit->clear->Formats, for chart bar.
                CommandBarControl editClearFormats2 = cb.FindControl(
                    missing,
                    872,
                    missing,
                    false,
                    true);
                if (editClearFormats2 != null)
                {
                    editClearFormats2.Enabled = false;
                    _buttonsToDisable.Add(editClearFormats2);
                }


                //Window->New Window
                CommandBarControl newWindow = wb.FindControl(
                    missing,
                    303,
                    missing,
                    false,
                    true);
                if (newWindow != null)
                {
                    newWindow.Enabled = false;
                    _buttonsToDisable.Add(newWindow);
                }

                //Window->Hide
                CommandBarControl windowHide = wb.FindControl(
                    missing,
                    865,
                    missing,
                    false,
                    true);
                if (windowHide != null)
                {
                    windowHide.Enabled = false;
                    _buttonsToDisable.Add(windowHide);
                }

                #region find menu id code
                //=======Leave this code it is usefull to find id tags for menu items.====================
                //use id tags becuase they are language independent.....

                //CommandBarControl file = wb.FindControl(
                //   missing,
                //   1,
                //   missing,
                //   false,
                //   true);

                //CommandBarPopup pop = (CommandBarPopup)file;

                //foreach (CommandBarControl c in pop.Controls)
                //{
                //    if (c != null)
                //    {
                //        Debug.WriteLine("caption: " + c.Caption);
                //        Debug.WriteLine("tag: " + c.Tag);
                //        Debug.WriteLine("id: " + c.Id);
                //    }
                //}
                //=======end leave code=====================
                #endregion find menu id code

            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        /// <summary>
        /// Setup all the necessary Excel event handlers for Titan.
        /// </summary>
        /// <param name="wbk">The host workbook.</param>
        private void SetUpExcelEventHandlers(ThisWorkbook wbk)
        {
            try
            {
                wbk.Windows[1].Caption = PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");
                wbk.Application.ActiveWindow.DisplayHeadings = Settings.Default.AutoColumnHeadings;

                //wbk.SheetChange += new Microsoft.Office.Interop.Excel.WorkbookEvents_SheetChangeEventHandler(PafApp.GetEventManager().ThisWorkbook_ChangeRange);
                wbk.SheetDeactivate += PafApp.GetEventManager().SheetsDeactivateEvent;
                wbk.SheetActivate += PafApp.GetEventManager().SheetsActivateEvent;
                wbk.Shutdown += PafApp.GetEventManager().ThisWorkbook_Shutdown;
                wbk.WindowDeactivate += PafApp.GetEventManager().ThisWorkbook_WindowDeactivate;
                wbk.WindowActivate += PafApp.GetEventManager().ThisWorkbook_WindowActivate;
                wbk.BeforeClose += PafApp.GetEventManager().ThisWorkbook_BeforeClose;
                wbk.SheetSelectionChange += PafApp.GetEventManager().ThisWorkbook_SheetSelectionChange;
                wbk.SheetBeforeRightClick += PafApp.GetEventManager().ThisWorkbook_SheetBeforeRightClick;
                wbk.SheetBeforeDoubleClick += PafApp.GetEventManager().ThisWorkbook_SheetBeforeDoubleClick;
                wbk.BeforeSave += PafApp.GetEventManager().ThisWorkbook_BeforeSave;
                wbk.BeforePrint += PafApp.GetEventManager().ThisWorkbook_BeforePrint;
                PafApp.GetViewMngr().ViewNotPlannable += ExcelEventMgr_ViewMngr_ViewNotPlannable;
                PafApp.GetViewMngr().ViewIsPlannable += ExcelEventMgr_ViewMngr_ViewPlannable;
                PafApp.PafServiceCall += PafApp_PafServiceCall;
                _eventDelCellsChange = PafApp.GetEventManager().ThisWorkbook_ChangeRange2;
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex.Message,
                    PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                    ex.StackTrace);
            }
        }

        private void PafApp_PafServiceCall(object sender, PafWebserviceCallEventArgs eventArgs)
        {
            switch (eventArgs.CallType)
            {
                case ServiceCallType.ClientAuth:
                    Settings.Default.ResetSessionLockStatus();
                    PafApp.GetViewMngr().GlobalLockMngr.Clear();
                    PafApp.GetGridApp().SetSessionLockButtonStatus(false);
                    PafApp.GetGridApp().EnableSessionLockButtons(false);
                    PafApp.UndoAvailable = false;
                    break;
                case ServiceCallType.PlanningSession:
                    Settings.Default.ResetSessionLockStatus();
                    PafApp.GetViewMngr().GlobalLockMngr.Clear();
                    PafApp.GetGridApp().SetSessionLockButtonStatus(false);
                    PafApp.GetGridApp().EnableSessionLockButtons(false);
                    PafApp.GetGridApp().SessionLockForm = null;
                    PafApp.GetGridApp().ClusterDialog = null;
                    PafApp.UndoAvailable = false;
                    break;
                case ServiceCallType.Role:
                case ServiceCallType.SeasonProcess:
                    PafApp.GetGridApp().SetSessionLockButtonStatus(false);
                    PafApp.GetGridApp().EnableSessionLockButtons(false);
                    PafApp.UndoAvailable = false;
                    break;
                default:
                    break;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="EventArgs">Event Args</param>
        private void ExcelEventMgr_ViewMngr_ViewPlannable(object sender, ViewPlannableStatusChangeEventArgs EventArgs)
        {
            Worksheet sheet = PafApp.GetGridApp().GetWorksheetFromHashCode(EventArgs.HashCode);
            if (sheet != null)
            {
                sheet.Tab.Color = false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender">Sender</param>
        /// <param name="EventArgs">Event Args</param>
        private void ExcelEventMgr_ViewMngr_ViewNotPlannable(object sender, ViewPlannableStatusChangeEventArgs EventArgs)
        {
            Worksheet sheet = PafApp.GetGridApp().GetWorksheetFromHashCode(EventArgs.HashCode);
            if (sheet != null)
            {
                sheet.Tab.Color = ColorTranslator.ToWin32(PafApp.GetPafAppSettingsHelper().GetNonPlannableProtectedColor());
            }
        }

        /// <summary>
        /// Execute test harness files from a command file.
        /// </summary>
        /// <param name="wbk">Host excel workbook.</param>
        /// <param name="thsi">Test Harness startup information object.</param>
        private void RunAppFromConfigFile(ThisWorkbook wbk, TestHarnessStartupInformation thsi)
        {
            try
            {
                PafApp.GetLogger().Info("RunAppFromConfigFile - Start");
                SetUpExcelEventHandlers(wbk);

                PafApp.GetCommandBarMngr().ShouldRuleSetComboBoxBeBuilt = false;
                PafApp.GetCommandBarMngr().CreateToolbars();
                PafApp.GetCommandBarMngr().CreateMenu();
                PafApp.GetCommandBarMngr().AddButtonsToRightClickMenu();

                StartupFileAutomationMngr startup = new StartupFileAutomationMngr(thsi);
                startup.run();


                //Log off the server.
                PafApp.GetLogger().Info("RunAppFromConfigFile - Pre EndPlanningSession");
                PafApp.EndPlanningSession();
                PafApp.GetLogger().Info("RunAppFromConfigFile - Post EndPlanningSession");

                //Turn off the Excel prompts/events.
                PafApp.GetLogger().Info("RunAppFromConfigFile - Pre Excel Call 1");
                PafApp.GetGridApp().DisplayAlerts = false;
                PafApp.GetLogger().Info("RunAppFromConfigFile - Pre Excel Call 2");
                PafApp.GetGridApp().EnableEvents(false);
                PafApp.GetLogger().Info("RunAppFromConfigFile - Pre Excel Calls Done");

                //Abort this thread.
                PafApp.GetLogger().Info("RunAppFromConfigFile - ThreadAbort");
                Thread.CurrentThread.Abort();
                PafApp.GetLogger().Info("RunAppFromConfigFile - End");
            }
            catch (CannotConnectToServerException ex)
            {
                PafApp.GetLogger().Error(ex);
                PafApp.GetTestHarnessManager().FailBtthFile(thsi.TestHarnessFiles);
            }
            catch (COMException ex)
            {
                PafApp.GetLogger().Error(ex);
            }
            catch (SecurityException ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (ThreadAbortException)
            {
                //Fix TTN-332
                wbk.Close(false, Missing.Value, Missing.Value);
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex.Message,
                                         PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                                         ex.StackTrace);
            }
            finally
            {
                Process.GetCurrentProcess().Kill();
            }
        }

        /// <summary>
        /// Searches the all the Pace worksheets for embedded charts and add them to the charts dictionary.
        /// </summary>
        private void SearchForEmbeddedCharts()
        {
            try
            {
                foreach (Worksheet sheet in Globals.ThisWorkbook.ThisApplication.Worksheets)
                {
                    if (sheet != null)
                    {
                        //See if the sheet has a view on it.
                        if (PafApp.GetGridApp().DoesSheetContainCustomProperty(sheet, PafAppConstants.OUR_VIEW_PROP_VALUE))
                        {
                            if (sheet.ChartObjects(Type.Missing) != null)
                            {
                                foreach (ChartObject co in (ChartObjects) sheet.ChartObjects(Type.Missing))
                                {
                                    Chart chart = co.Chart;
                                    if (!_charts.ContainsKey(chart.GetHashCode().ToString()))
                                    {
                                        chart.SeriesChange +=
                                            new ChartEvents_SeriesChangeEventHandler(ActiveChart_SeriesChange);
                                        _charts.Add(chart.GetHashCode().ToString(), chart);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Searches a sheet for an embedded charts and add them to the charts dictionary.
        /// </summary>
        /// <param name="sheet">Sheet to search for the chart.</param>
        private void SearchForEmbeddedCharts(Worksheet sheet)
        {
            try
            {
                if (sheet != null)
                {
                    if (sheet.ChartObjects(Type.Missing) != null)
                    {
                        foreach (ChartObject co in (ChartObjects)sheet.ChartObjects(Type.Missing))
                        {
                            Chart chart = co.Chart;
                            if (!_charts.ContainsKey(chart.GetHashCode().ToString()))
                            {
                                chart.SeriesChange +=
                                    new ChartEvents_SeriesChangeEventHandler(ActiveChart_SeriesChange);
                                _charts.Add(chart.GetHashCode().ToString(), chart);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
        }


        /// <summary>
        /// Gets the active range.
        /// </summary>
        /// <returns>returns the active range, or null.</returns>
        private static Range GetActiveRange()
        {
            Range selectedRange = null;
            try
            {
                if(Globals.ThisWorkbook.Application.Selection != null)
                {
                    selectedRange = (Range)Globals.ThisWorkbook.Application.Selection;
                }
            }
            catch(Exception)
            {
                selectedRange = null;
            }
            
            return selectedRange;
        }

        /// <summary>
        /// Remove any user deleted grids from the view manager.
        /// </summary>
        /// <remarks>enhanced to fix TTN-529</remarks>
        private static void RemoveDeletedGrids()
        {
            //Get the currently plannable view (if one exists)
            int plannableHashCode = PafApp.GetViewMngr().PlannableViewHashCode;
            //Now call method to remove any sheets in viewmngr that are not in workbook.
            List<int> deletedSheetResults = PafApp.GetViewMngr().RemoveDeletedGrids(PafApp.GetGridApp().Sheets);
            //Begin TTN-529
            //int currentViewHashCode = deletedSheetResults.Find(Utility.FindValueGreaterThanZero);
            int currentViewHashCode = deletedSheetResults.Find(x => x > 0);

            if (deletedSheetResults.Count > 0 && currentViewHashCode > 0)
            {
                if (plannableHashCode == currentViewHashCode)
                {
                    //Set the flags of all the sheets to be clean.
                    PafApp.GetViewMngr().MakeAllProtMngPlanable();
                    //TTN-1172
                    PafApp.GetViewMngr().MakeAllProtMngrDirtyExcept(0);
                }
            }
            //End TTN-529
        }

        /// <summary>
        /// Gets the active embedded chart.
        /// </summary>
        /// <returns>returns the active chart, or null.</returns>
        private static Chart GetActiveChart()
        {
            Chart selectedChart = null;
            try
            {
                if (Globals.ThisWorkbook.Application.Selection != null)
                {
                    selectedChart = (Chart)Globals.ThisWorkbook.Application.Selection;
                }
            }
            catch (Exception)
            {
                selectedChart = null;
            }

            return selectedChart;
        }


        #endregion Private Methods
    }
}
