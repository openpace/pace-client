#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
namespace Titan.Pace.Application.Compression
{
    internal interface IPafCompressedObj<T>
    {
        /// <summary>
        /// Returnts if the object if the compressed or not.
        /// </summary>
        /// <param name="obj">object to compress.</param>
        /// <returns>true if the object is compressed, flase if not.</returns>
        bool isCompressed(T obj);

        /// <summary>
        /// Sets if the object is compressed or not.
        /// </summary>
        /// <param name="obj">object to compress.</param>
        /// <param name="isCompressed">true if the object is compressed, flase if not.</param>
        void setCompressed(T obj, bool isCompressed);

        /// <summary>
        /// Compress a object.
        /// </summary>
        /// <param name="obj">object to compress.</param>
        void compressData(T obj);

        /// <summary>
        /// Uncompress a object.
        /// </summary>
        /// <param name="obj">object to compress.</param>
	    void uncompressData(T obj);
    }

    internal interface IPafCompressedObj
    {
        /// <summary>
        /// Returnts if the object if the compressed or not.
        /// </summary>
        /// <returns>true if the object is compressed, flase if not.</returns>
        bool isCompressed();

        /// <summary>
        /// Sets if the object is compressed or not.
        /// </summary>
        /// <param name="isCompressed">true if the object is compressed, flase if not.</param>
        void setCompressed(bool isCompressed);

        /// <summary>
        /// Compress an object.
        /// </summary>
        void compressData();

        /// <summary>
        /// Uncompress an object.
        /// </summary>
        void uncompressData();
    }
}
