#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.PafService;

namespace Titan.Pace.Application.Compression.PafSimpleCellNotes
{
    internal class PafSimpleCellNotes : IPafCompressedObj
    {
        private readonly simpleCellNote[] _SimpleCellNotes;

        public PafSimpleCellNotes(simpleCellNote[] SimpleCellNotes)
        {
            _SimpleCellNotes = SimpleCellNotes;
        }

        #region IPafCompressedObj Members

        public bool isCompressed()
        {
            throw new System.Exception("The method or operation is not implemented.");
        }

        public void setCompressed(bool isCompressed)
        {
            throw new System.Exception("The method or operation is not implemented.");
        }

        public void compressData()
        {
            if(_SimpleCellNotes != null && _SimpleCellNotes.Length > 0)
            {
                foreach(simpleCellNote scn in _SimpleCellNotes)
                {
                    if (! scn.compressed)
                    {
                        PafSimpleCellNote pscn = new PafSimpleCellNote(scn);
                        pscn.compressData();
                    }
                }
            }
        }

        public void uncompressData()
        {
            if (_SimpleCellNotes != null && _SimpleCellNotes.Length > 0)
            {
                foreach (simpleCellNote scn in _SimpleCellNotes)
                {
                    if (scn.compressed)
                    {
                        PafSimpleCellNote pscn = new PafSimpleCellNote(scn);
                        pscn.uncompressData();
                    }
                }
            }
        }

        #endregion
    }
}
