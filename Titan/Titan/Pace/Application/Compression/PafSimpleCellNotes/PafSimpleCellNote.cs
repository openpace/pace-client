#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.Pace.DataStructures;
using Titan.PafService;

namespace Titan.Pace.Application.Compression.PafSimpleCellNotes
{
    internal class PafSimpleCellNote : IPafCompressedObj
    {
        private readonly simpleCellNote _SimpleCellNote;

        public PafSimpleCellNote(simpleCellNote simpleCellNote)
        {
            _SimpleCellNote = simpleCellNote;
        }

        #region IPafCompressedObj Members

        public bool isCompressed()
        {
            return _SimpleCellNote.compressed;
        }

        public void setCompressed(bool isCompressed)
        {
            _SimpleCellNote.compressed = isCompressed;
        }

        public void compressData()
        {
            if(!_SimpleCellNote.simpleCoordList.compressed)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(_SimpleCellNote.simpleCoordList, true);
                _SimpleCellNote.simpleCoordList = pscl.GetSimpleCoordList;
                setCompressed(true);
            }
        }

        public void uncompressData()
        {
            if (_SimpleCellNote.simpleCoordList.compressed)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(_SimpleCellNote.simpleCoordList);
                pscl.uncompressData();
                _SimpleCellNote.simpleCoordList = pscl.GetSimpleCoordList;
                setCompressed(false);
            }
        }

        #endregion
    }
}
