#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.Pace.Application.Extensions;
using Titan.PafService;

namespace Titan.Pace.Application.Compression.PafViewSection
{
    class ViewTuple : IPafCompressedObj
    {
        #region IPafCompressedObj Members

        private readonly viewTuple _tuple;

        public ViewTuple(viewTuple tuple)
        {
            _tuple = tuple;
        }

        public bool isCompressed()
        {
            return _tuple.compressed;
        }

        public void setCompressed(bool isCompressed)
        {
            _tuple.compressed = isCompressed;
        }

        public void compressData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void uncompressData()
        {
            if (_tuple.compressed)
            {
                DateTime startTime = DateTime.Now;
                if (_tuple.compMemberDefs != null)
                {
                    _tuple.memberDefs = _tuple.compMemberDefs.SplitString(
                        PafAppConstants.ELEMENT_DELIM,
                        true);

                    _tuple.compMemberDefs = null;

                    string[] groupName = null;
                    List<string[]> groupValues = null;

                    if (_tuple.compressed)
                    {
                        if (!string.IsNullOrEmpty(_tuple.compMemberTagComments))
                        {
                            CompressionUtil.unCompressString(
                                _tuple.compMemberTagComments,
                                _tuple.groupDelimiter,
                                _tuple.elementDelimiter, 
                                false, 
                                out groupName,
                                out groupValues);
                        }

                        setCompressed(false);

                    }
                    if (groupName != null && groupName.Length > 0)
                    {
                        _tuple.memberTagCommentNames = new string[groupName.Length];
                        groupName.CopyTo(_tuple.memberTagCommentNames, 0);

                        if (groupValues != null)
                        {
                            _tuple.memberTagCommentValues = new string[groupValues.Count];

                            int i = 0;
                            foreach (string[] s in groupValues)
                            {
                                s.CopyTo(_tuple.memberTagCommentValues, i);
                                i++;
                            }
                        }
                        //_tuple.memberTagCommentValues = new string[groupValues.Length];
                        //groupValues.CopyTo(_tuple.memberTagCommentValues, 0);
                    }

                    _tuple.compMemberTagComments = null;
                }
                if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("ViewTuple.uncompressData:  runtime: " + (DateTime.Now - startTime).TotalSeconds);
            }
        }

        #endregion
    }
}
