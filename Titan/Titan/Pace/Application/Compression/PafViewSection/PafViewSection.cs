#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.PafService;

namespace Titan.Pace.Application.Compression.PafViewSection
{
    class PafViewSection : IPafCompressedObj
    {
        #region IPafCompressedObj Members

        private readonly pafViewSection _pvs;

        public PafViewSection(pafViewSection pafviewsection)
        {
            _pvs = pafviewsection;
        }

        public bool isCompressed()
        {
            return _pvs.compressed;
        }

        public void setCompressed(bool isCompressed)
        {
            _pvs.compressed = isCompressed;
        }

        public void compressData()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void uncompressData()
        {
            DateTime startTime = DateTime.Now;
            if (_pvs.compressed)
            {
                if (_pvs.compRowMemberTagData != null)
                {
                    DateTime startTime1 = DateTime.Now;
                    string[] groupName = null;
                    List<string[]> groupValues = null;

                    CompressionUtil.unCompressString(_pvs.compRowMemberTagData,
                        _pvs.groupDelimiterRowMemberTag,
                        _pvs.elementDelimiterRowMemberTag, 
                        false, 
                        out groupName,
                        out groupValues);

                    if (groupName != null && groupName.Length > 0)
                    {
                        _pvs.rowMemberTagData = new memberTagViewSectionData[groupName.Length];
                        for (int i = 0; i < groupName.Length; i++)
                        {
                            _pvs.rowMemberTagData[i] = new memberTagViewSectionData();
                            _pvs.rowMemberTagData[i].memberTagName = groupName[i];
                            _pvs.rowMemberTagData[i].memberTagValues = groupValues[i];
                        }
                    }

                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("PafViewSection.uncompressData.compColMemberTagData: " + _pvs.name + ", runtime: " + (DateTime.Now - startTime1).TotalSeconds);

                    _pvs.compRowMemberTagData = null;
                    setCompressed(false);
                }
                if (_pvs.compColMemberTagData != null)
                {
                    DateTime startTime2 = DateTime.Now;
                    string[] groupName = null;
                    List<string[]> groupValues = null;

                    CompressionUtil.unCompressString(_pvs.compColMemberTagData,
                        _pvs.groupDelimiterColMemberTag,
                        _pvs.elementDelimiterColMemberTag,
                        false,
                        out groupName,
                        out groupValues);
                    
                    if (groupName != null && groupName.Length > 0)
                    {
                        _pvs.colMemberTagData = new memberTagViewSectionData[groupName.Length];
                        for(int i = 0; i < groupName.Length; i++)
                        {
                            _pvs.colMemberTagData[i] = new memberTagViewSectionData();
                            _pvs.colMemberTagData[i].memberTagName = groupName[i];
                            _pvs.colMemberTagData[i].memberTagValues = groupValues[i];
                        }
                    }
                    if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("PafViewSection.uncompressData.compColMemberTagData: " + _pvs.name + ", runtime: " + (DateTime.Now - startTime2).TotalSeconds);

                    _pvs.compColMemberTagData = null;
                    setCompressed(false);
                }
            }
            if (PafApp.GetLogger().IsDebugEnabled) PafApp.GetLogger().Debug("PafViewSection.uncompressData(total process): " + _pvs.name + ", runtime: " + (DateTime.Now - startTime).TotalSeconds);
        }

        #endregion
    }
}
