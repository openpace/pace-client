#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using Titan.Pace.Application.Extensions;

namespace Titan.Pace.Application.Compression
{
    internal class CompressionUtil
    {

        /// <summary>
        /// Uncompresses a string.
        /// </summary>
        /// <param name="delimitedString">String to uncompress.</param>
        /// <param name="groupDelimiter">Group delimiter.</param>
        /// <param name="elementDelimiter">Elemnt delimiter.</param>
        /// <param name="removeBlanks">Remove blank values from the return string array.</param>
        /// <param name="groupNames"></param>
        /// <param name="groupValues"></param>
        /// <returns>An array of uncompressed strings.</returns>
        public static void unCompressString(string delimitedString, string groupDelimiter, 
            string elementDelimiter, bool removeBlanks, out string[] groupNames, out List<string[]> groupValues)
        {
            groupNames = null;
            groupValues = null;

            if (String.IsNullOrEmpty(delimitedString) || String.IsNullOrEmpty(groupDelimiter)
                || String.IsNullOrEmpty(elementDelimiter))
            {
                return;
            }

            List<string> names = new List<string>();
            List<string[]> values = new List<string[]>();
            List<string> item = new List<string>();

            string[] groups = delimitedString.SplitString(groupDelimiter, removeBlanks);

            if (groups != null)
            {
                foreach (string g in groups)
                {
                    string[] elements = g.SplitString(elementDelimiter, removeBlanks);
                    for (int i = 0; i < elements.Length; i++)
                    {
                        if (i == 0)
                        {
                            names.Add(elements[i]);
                            item = new List<string>();
                        }
                        else
                        {
                            item.Add(elements[i]);
                            //values.Add(elements[i]);
                        }
                    }
                    if (item.Count > 0)
                    {
                        values.Add(item.ToArray());
                    }
                }
            }


            if (names.Count > 0)
            {
                groupNames = new string[names.Count];
                groupNames = names.ToArray();

                //groupValues = new string[values.Count];
                groupValues = values;
            }

        }
    }
}
