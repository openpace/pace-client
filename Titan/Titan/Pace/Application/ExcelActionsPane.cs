#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraEditors.DXErrorProvider;
using DevExpress.XtraTreeList;
using DevExpress.XtraTreeList.Nodes;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;
using Titan.Pace.Application.Controls;
using Titan.Pace.Application.Controls.TreeView;
using Titan.Pace.Application.Events;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.Data;
using Titan.Pace.DataStructures;
using Titan.Pace.ExcelGridView;
using Titan.PafService;
using Titan.Properties;
using Button=System.Windows.Forms.Button;
using DataTable = System.Data.DataTable;
using Point = System.Drawing.Point;

namespace Titan.Pace.Application
{
    /// <summary>
    /// ExcelActionsPane class
    /// </summary>
    internal class ExcelActionsPane
    {
        #region Private Variables

        /// <summary>
        /// Name of the column that contains the flag that signals if the view can be selected.
        /// </summary>
        private const string CanSelect = "CanSelect";

        /// <summary>
        /// Name of the column that contains the flag that signals if the node is visible.
        /// </summary>
        private const string VisibleColumnName = "Visible";

        /// <summary>
        /// Name of the column that contains the has the view desc.
        /// </summary>
        private const string ViewDescription = "Description";

        /// <summary>
        /// Name of the column that contains the flag that signals if the view can be selected.
        /// </summary>
        private const string ViewDynamic = "Requires User Input";

        /// <summary>
        /// Name of the column that contains the name of the view.
        /// </summary>
        private const string ViewColumnName = "ViewName";

        /// <summary>
        /// The header control used in the actions pane.  Contains user information.
        /// </summary>
        //private readonly Header _header;

        /// <summary>
        /// User information flipper information box.
        /// </summary>
        private readonly UserInformation _userInformation = new UserInformation();

        /// <summary>
        /// The button used on the ActionsPane.
        /// </summary>
        private readonly Button _nextButton = new Button();

        /// <summary>
        /// An array of blank line controls.
        /// </summary>
        private readonly LineBreak[] _lineBreak = new LineBreak[4];

        /// <summary>
        /// Suppress zero flipper information box.
        /// </summary>
        private readonly ViewOptions _suppressZero;

        /// <summary>
        /// The ViewTree that contains the tree listing of views.
        /// </summary>
        private ViewTree _views;

        /// <summary>
        /// The list of ViewTree(s), one per dimension.
        /// </summary>
        private readonly List<DimensionTree> _viewTreeDimControls;

        /// <summary>
        /// Simple tree objects for the API.
        /// </summary>
        //private readonly SimpleTrees _SimpleTrees;

        /// <summary>
        /// Simple tree objects for the API.
        /// </summary>
        //private readonly PaceSimpleTrees _paceSimpleTrees;

        /// <summary>
        /// Image list for the view pane.
        /// </summary>
        private readonly ImageList _imageList;

        /// <summary>
        /// Dictionary to cache the ParentFirst checked status.
        /// </summary>
        private readonly Dictionary<string, bool> _parentFirst;
        
        /// <summary>
        /// Dictionary to chache the PafUserSelections in.
        /// </summary>
        private readonly Dictionary<string, pafUserSelection[]> _cachedPafUserSelections;

        /// <summary>
        /// Object to hold the selected tree nodes.  
        /// view, dimension, array of TreeNodes
        /// </summary>
        private readonly MultiKeyDictionary<string, string, string> _cachedTreeViews;

        /// <summary>
        /// Object to hold the status of the subpane expanded/collpased.  
        /// view, dimension, array of bool
        /// </summary>
        private readonly CacheSelectionsList<string, string, bool> _cachedSubpaneStatus;

        /// <summary>
        /// Object to hold the status of the checked/unchecked status of the column/row checkbox
        /// view, array of bool (row, column)
        /// </summary>
        private readonly Dictionary<string, bool[]> _cachedSuppredZeroSettings;

        /// <summary>
        /// Object to hold the status of the checked/unchecked status of the column/row grouping boxes
        /// view, array of bool (row, column)
        /// </summary>
        private readonly Dictionary<string, bool[]> _cachedGroupingSettings;

        /// <summary>
        /// The views that have been suppressed.
        /// </summary>
        private readonly Set<string> _cachedSuppressedViews;

        /// <summary>
        /// Hash set of base dimension names.
        /// </summary>
        private readonly HashSet<string> _base;

        /// <summary>
        /// Hash set of attribute dimension names.
        /// </summary>
        private readonly HashSet<string> _attr;

        /// <summary>
        /// 
        /// </summary>
        private SavedFilterSelections _savedSelections;


        #endregion Private Variables


        #region Public Properties

        //public SimpleDimTree[] ApiDimTrees { get; set; }

        /// <summary>
        /// Simple tree objects for the API.
        /// </summary>
        //public PaceSimpleTrees PaceSimpleTrees
        //{
        //    get { return _paceSimpleTrees; }
        //}

        private Dictionary<string, Dictionary<string, TreeListViewState>> viewDimTreeListStates = new Dictionary<string, Dictionary<string, TreeListViewState>>();

        /// <summary>
        /// Simple tree typed dataset.
        /// </summary>
        public SimpleDimTrees SimpleDimTrees
        {
            get { return PafApp.SimpleDimTrees; }
        }

        /// <summary>
        /// Name of the column in the datatable that contains the member key.
        /// </summary>
        private string MemberKeyColumnName
        {
            get
            {
                return SimpleDimTrees != null ? SimpleDimTrees.MemberKeyColumn : String.Empty;
            }
        }

        /// <summary>
        /// Name of the column in the datatable that contains the members parent key.
        /// </summary>
        private string ParentKeyColumnName
        {
            get
            {
                return SimpleDimTrees != null ? SimpleDimTrees.ParentKeyColum : String.Empty;
            }
        }

        /// <summary>
        /// Name of the column in the datatable that contains the members level.
        /// </summary>
        private string LevelColumnName
        {
            get
            {
                return SimpleDimTrees != null ? SimpleDimTrees.LevelColumName : String.Empty;
            }
        }

        /// <summary>
        /// Name of the column in the datatable that contains the members name.
        /// </summary>
        private string MemberColumnName
        {
            get
            {
                return SimpleDimTrees != null ? SimpleDimTrees.MemberColumName  : String.Empty;
            }
        }

        /// <summary>
        /// Name of the column in the datatable that contains the members generation.
        /// </summary>
        private string GenerationColumnName
        {
            get
            {
                return SimpleDimTrees != null ? SimpleDimTrees.GenerationColumName : String.Empty;
            }
        }


        #endregion Public Properties

        #region Constructor
        /// <summary>
        /// Constructor.
        /// </summary>
        public ExcelActionsPane()
        {
            //For Actions Pane
            //_header = new Header
            //{
            //    Name = "_Header",
            //    HeaderText =
            //        PafApp.GetLocalization()
            //            .GetResourceManager()
            //            .GetString("Application.Excel.ActionsPane.Header.Heading.Step1")
            //};

            _nextButton = new Button {Name = "_nextButton"};
            _nextButton.Click += _nextButton_Click;

            _viewTreeDimControls = new List<DimensionTree>();

            _suppressZero = new ViewOptions();

            for (int i = 0; i < _lineBreak.Length; i++)
            {
                _lineBreak[i] = new LineBreak {Name = "_LineBreak" + i};
            }

            _views = new ViewTree { Name = "_Views" };

            //_SimpleTrees = new SimpleTrees();

            //_paceSimpleTrees = new PaceSimpleTrees();

            _cachedPafUserSelections = new Dictionary<string, pafUserSelection[]>();

            _cachedTreeViews = new MultiKeyDictionary<string, string, string>();

            _cachedSubpaneStatus = new CacheSelectionsList<string, string, bool>();

            _cachedSuppredZeroSettings = new Dictionary<string, bool[]>();

            _cachedGroupingSettings = new Dictionary<string, bool[]>(10);

            _cachedSuppressedViews = new Set<string>();

            _parentFirst = new Dictionary<string, bool>(10);

            _base = new HashSet<string>();

            _attr = new HashSet<string>();

            _imageList = new ImageList();

            _imageList.Images.Add(Resources.open_folder);

            _imageList.Images.Add(Resources.closed_folder);


            _imageList.Images.Add(Resources.spreadsheet);
            
            _views.TreeList.StateImageList = _imageList;

            Globals.ThisWorkbook.Ribbon.ViewSectionGroupChanged += Ribbon_ViewSectionGroupChanged;
        }

        #endregion Constructor

        #region Public Methods
        /// <summary>
        /// Gets the SimpleTrees object.
        /// </summary>
        /// <returns>A SimpleTrees object.</returns>
        //[DebuggerHidden]
        //[Obsolete("Replaced by SimpleDimTrees", false)]
        //public SimpleTrees GetSimpleTrees()
        //{
        //    return _SimpleTrees;
        //}

        ///// <summary>
        ///// Gets the currently selected view in the tree control.
        ///// </summary>
        ///// <returns>Returns a string containing the currently selected view in the tree control.</returns>
        //public string GetSelectedView()
        //{
        //    if (_views.treeView1.GetSelectedNode() != null)
        //    {
        //        return _views.treeView1.GetSelectedNodeText(ViewColumnName).ToString();
        //    }
        //    return null;
        //}

        /// <summary>
        /// Reset the view tree control.  It unhighlights all the nodes.
        /// </summary>
        public void ResetViewTreeSelectedNode()
        {
            //_Views.treeView1.LowlightAllNodes(_Views.treeView1.Nodes);
            _views.TreeList.FocusedNode = null;
        }

        /// <summary>
        /// Select the tree node in the view tree.
        /// </summary>
        /// <param name="node">The view tree node to select/highlight.</param>
        public void SelectViewTreeSelectedNode(TreeListNode node)
        {
            if (node != null)
            {
                if (node != _views.TreeList.GetSelectedNode())
                {
                    //_Views.treeView1.LowlightAllNodes(_Views.treeView1.Nodes);
                    //_Views.treeView1.HighlightNode(node, false);
                    node.Selected = true;
                    //_Views.treeView1.selectn = node;
                }
            }
        }

        ///// <summary>
        ///// Performs a click of the "Get View" button.
        ///// </summary>
        //public void PerformGetViewClick()
        //{
        //    if (_nextButton.Visible)
        //        _nextButton.PerformClick();
        //}

        /// <summary>
        /// Set the view options (Suppress Zero) visible and enabled.
        /// </summary>
        /// <param name="viewName">Name of the currently selected view.</param>
        public void SetViewOptions(string viewName)
        {
            bool visible = false;
            bool enabled = false;
            bool dynamic = false;

            if (!String.IsNullOrEmpty(viewName))
            {
                visible = PafApp.GetViewTreeView().GetViewSuppressZeroVisible(viewName);
                dynamic = PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName);
                if (!dynamic)
                {
                    //get the state of the static view.
                    dynamic = _cachedSuppressedViews.Contains(viewName);
                }
                enabled = PafApp.GetViewTreeView().GetViewSuppressZeroEnabled(viewName);
            }

            _lineBreak[3].Visible = visible;
            _suppressZero.ControlIsVisible = visible;

            _lineBreak[3].Enabled = enabled;
            _suppressZero.Enabled = enabled;

            if (visible && !dynamic)
            {
                _lineBreak[2].Visible = false;
            }
            else if (dynamic)
            {
                _lineBreak[2].Visible = true;
                _lineBreak[3].Visible = true;
            }

            SetViewPlannableOptions(viewName);
        }

        /// <summary>
        /// Set the view options (Suppress Zero) plannable.  The Suppress zero filpper will be
        /// disabled when the view is being planned.  
        /// </summary>
        /// <param name="viewName">Name of the currently selected view.</param>
        public void SetViewPlannableOptions(string viewName)
        {
            try
            {
                string plannableViewName = PafApp.GetViewMngr().PlannableViewName;
                if (viewName.Equals(plannableViewName))
                {
                    _suppressZero.Enabled = false;
                    _nextButton.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                PafApp.GetLogger().Error(ex);
            }
        }

        /// <summary>
        /// Build the inital Actions Pane.
        /// </summary>
        public void BuildActionsPane()
        {
            try
            {
                //Globals.ThisWorkbook.ThisApplication.ScreenUpdating = false;
                PafApp.GetGridApp().ScreenUpdating = false;

                ClearActionPaneControls();

                //clear out any cached user selections.
                ClearCachedSelections();

                //Build the View Tree Dimension 
                BuildViewTree(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewTree.Subpane.ViewLabel"));

                //Add the text to the boxes
                //_header.HeaderText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.ActionsPane.Header.Heading.Step1");
                _userInformation.subpane.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserInformation.Title");
                _userInformation.lblUserIdLabel.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.UserInformation.UserIdLabel");
                _userInformation.UserId = PafApp.GetGridApp().LogonInformation.UserName;
                _userInformation.SeasonCaption = PafApp.GetGridApp().RoleSelector.UserSelectedPlanTypeSpecString;
                _userInformation.PlanType = PafApp.GetGridApp().RoleSelector.UserRole;
                _userInformation.subpane.Collapse();
                _userInformation.ControlIsVisible = true;
                _nextButton.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.ActionsPane.Button.Build");
                _nextButton.Visible = false;

                _suppressZero.subpane.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewOptions.Title");
                _suppressZero.chkRows.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewOptions.Row");
                _suppressZero.chkColumn.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewOptions.Column");
                _suppressZero.grpSuppressZero.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewOptions.GroupBoxTitle");
                _suppressZero.ControlIsVisible = false;
                _suppressZero.ChkBoxRowCheckChange += SuppressZero_ChkBoxCheckChange;
                _suppressZero.ChkBoxColumnCheckChange += SuppressZero_ChkBoxCheckChange;


                foreach (var s in SimpleDimTrees.Dimension)
                {
                    if (s.BaseDimension)
                    {
                        _base.Add(s.Name);
                    }
                    else
                    {
                        _attr.Add(s.Name);
                    }
                }

                _savedSelections = PafApp.GetPafAppSettingsHelper().UserSelectorSavedSelections();


                //if (PafApp.GetPafAuthResponse().attrDimInfo != null)
                //{
                //    foreach (attributeDimInfo str in PafApp.GetPafAuthResponse().attrDimInfo)
                //    {
                //        _Attr.Add(str.dimName);
                //    }
                //}


                ////Add the trees to the tree List
                ////var ds = new List<SimpleDimTree>();
                //SimpleDimTree[] ds = new SimpleDimTree[PafApp.GetPafPlanSessionResponse().dimTrees.Length];
                //int i = 0;
                //foreach (pafSimpleDimTree tree in PafApp.GetPafPlanSessionResponse().dimTrees)
                //{
                //    _SimpleTrees.Add(tree, tree.id, PafApp.GetViewTreeView().DoesViewDimHaveDynamicSelections("", tree.id));
                //    SimpleDimTree t = new SimpleDimTree(tree.aliasTableNames,
                //        tree.traversedMembers,
                //        tree.compAliasTableNames,
                //        tree.compMemberIndex,
                //        tree.compParentChild,
                //        tree.compressed,
                //        tree.elementDelim,
                //        tree.groupDelim,
                //        tree.id,
                //        tree.memberObjects.BuildArray(),
                //        tree.rootKey);
                //    PaceSimpleTrees.Add(t, t.Id, PafApp.GetViewTreeView().DoesViewDimHaveDynamicSelections("", tree.id));

                //    ds[i] = t;

                //    //add the base dims to the list.
                //    if (_Attr != null)
                //    {
                //        if (!_Attr.Contains(t.Id))
                //        {
                //            _Base.Add(t.Id);
                //        }
                //    }

                //    i++;
                //}

                //ApiDimTrees = ds;

                //Finally add the controls
                AddControlsToActionPane();
            }
            catch (NullReferenceException)
            {
                PafApp.GetLogger().Info("Cannot build tree, is user connected to server?");
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
            finally
            {
                PafApp.GetGridApp().ScreenUpdating = true;
            }
        }

        /// <summary>
        /// Gets the ParentFirst value for the array of user selections.
        /// </summary>
        /// <param name="viewAxis"></param>
        /// <param name="selections"></param>
        /// <returns></returns>
        public bool? GetParentFirst(ViewAxis viewAxis, pafUserSelection[] selections)
        {
            bool? parentFirst = null;
            //Get user selections parent first.
            if (selections != null && selections.Length > 0)
            {
                foreach (var userSel in from userSel in selections let userSelAxis = PafAxisExtension.ToAxis(userSel.pafAxis) where viewAxis == userSelAxis select userSel)
                {
                    if (parentFirst.HasValue && parentFirst.Value) break;
                    switch (userSel.pafAxis.ToAxis())
                    {
                        case ViewAxis.Col:
                            parentFirst = GetParentFirst(userSel.id);
                            break;
                        case ViewAxis.Row:
                            parentFirst = GetParentFirst(userSel.id);
                            break;
                    }
                }
            }
            return parentFirst;
        }


        /// <summary>
        /// Retrieve the cached PafUserSelections.
        /// </summary>
        /// <param name="viewName">The name of the view.</param>
        /// <returns>A list an array of PafUserSelections if they exist, or null if no selections have been made.</returns>
        public pafUserSelection[] GetCachedDyanmicPafUserSelections(string viewName)
        {
            pafUserSelection[] cachedUserSel;
            try
            {
                cachedUserSel = _cachedPafUserSelections.ValueOrDefault(viewName);
                var currentSettings = PafApp.GetViewTreeView().GetUserSelections(viewName).ToList();

                if (cachedUserSel == null) return null;
                if (cachedUserSel.Length != currentSettings.Count) return null;

                foreach (var sel in currentSettings)
                {
                    pafUserSelection sel1 = sel;
                    var temp = cachedUserSel.FirstOrDefault(x => x.id == sel1.id);
                    if (temp == null)
                    {
                        return null;
                    }
                }
            }
            catch(ArgumentException)
            {
                cachedUserSel = null;
            }
            return cachedUserSel;
        }

        /// <summary>
        /// Gets the cached suppress zero settings.
        /// </summary>
        /// <param name="viewName">The name of the view.</param>
        /// <returns>The row/column settings, or false if no values exist.</returns>
        public bool[] GetCachedSuppressZeroSelections(string viewName)
        {
            bool[] settings;
            try
            {
                settings = _cachedSuppredZeroSettings.ValueOrDefault(viewName);
                if (settings == null || settings.Length == 0)
                {
                    settings = new bool[2];
                }
            }
            catch (Exception)
            {
                settings = new bool[2];
            }
            return settings;
        }


        /// <summary>
        /// Looks at the view and displays any dynamic selection subpanes.
        /// </summary>
        /// <param name="view">Name of the view.</param>
        /// <param name="reselectUserSelections">Reselect the the users selections in the tree control.</param>
        public void RefreshDimensionControls(string view, bool reselectUserSelections)
        {

            //Get the current view node.
            object obj = PafApp.GetViewMngr().CurrentViewNode;
            if (obj != null)
            {
                TreeListNode tn = (TreeListNode)obj;
                SelectViewTreeSelectedNode(tn);
            }

            //Local variable
            if (reselectUserSelections)
            {
                AddDimensionControlsToPane(view);
            }

            //enable or disable the get view button.
            SetGetViewButtonStatus(view);

            //set the view options.
            SetViewOptions(view);

            //Check the check boxes
            SetSuppressZeroSettings(view);
        }

        /// <summary>
        /// Clear out the cached user selections.
        /// </summary>
        public void ClearCachedUserSelections()
        {
            if (_cachedTreeViews != null)
            {
                _cachedTreeViews.Clear();
            }
        }

        /// <summary>
        /// Remove the cached PafUserSelections that are made by the user.
        /// </summary>
        public void ClearCachedPafUserSelections()
        {
            if (_cachedPafUserSelections != null)
            {
                _cachedPafUserSelections.Clear();
            }
        }

        /// <summary>
        /// Remove the cached SuppressedZeroSelections.
        /// </summary>
        public void ClearSuppressedZeroSelections()
        {
            if (_cachedSuppredZeroSettings != null)
            {
                _cachedSuppredZeroSettings.Clear();
            }

            if (_cachedSuppressedViews != null)
            {
                _cachedSuppressedViews.Clear();
            }
        }

        /// <summary>
        /// Clear out the actions pane.
        /// </summary>
        public void ClearActionsPane()
        {
            try
            {
                PafApp.GetGridApp().ScreenUpdating = false;

                _viewTreeDimControls.Clear();

                //Create a new object...
                _views = new ViewTree { Name = "_Views" };
                _views.subpane.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewTree.Subpane.ViewLabel");

                _views.TreeList.StateImageList = _imageList;

                _views.Enabled = false;

                //_SimpleTrees.Clear();

                //PaceSimpleTrees.Clear();

                _attr.Clear();

                _base.Clear();

                Globals.ThisWorkbook.ActionsPane.Controls.Clear();

                _userInformation.SeasonCaption = String.Empty;
                _userInformation.PlanType = String.Empty;
                _userInformation.UserId = String.Empty;

                //Globals.ThisWorkbook.ActionsPane.Controls.Add(_header);

                //Globals.ThisWorkbook.ActionsPane.Controls.Add(_lineBreak[0]);

                Globals.ThisWorkbook.ActionsPane.Controls.Add(_views);

                Globals.ThisWorkbook.ActionsPane.Refresh();
            }
            finally
            {
                PafApp.GetGridApp().ScreenUpdating = true;
            }
        }

        /// <summary>
        /// Hides all the dimension tree controls on the actions pane.
        /// </summary>
        public void HideAllDimensionControls()
        {
            try
            {
                foreach (DimensionTree tree in _viewTreeDimControls)
                {
                    tree.ControlIsVisible = false;
                }
                Globals.ThisWorkbook.ActionsPane.Controls[_nextButton.Name].Visible = false;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Store the suppress zero settings for a view.
        /// </summary>
        /// <param name="viewName">Name of the view to store the settings.</param>
        /// <param name="settings">Setting to store.</param>
        public void StoreSuppressZeroSettings(string viewName, bool[] settings)
        {
            _cachedSuppredZeroSettings.AddOrUpdate(viewName, settings);
        }

        /// <summary>
        /// Set the suppress zero settings for a view.
        /// </summary>
        /// <param name="viewName">Name of the view to store the settings.</param>
        public void SetSuppressZeroSettings(string viewName)
        {
            bool[] settings = _cachedSuppredZeroSettings.ValueOrDefault(viewName);
            bool[] setting = new [] { false, false };

            if (settings != null && settings.Length > 0)
            {
                setting[0] = settings[0];
                setting[1] = settings[1];
            }
            else
            {
                StoreSuppressZeroSettings(viewName);
                setting = _cachedSuppredZeroSettings.ValueOrDefault(viewName);
            }

            try
            {
                _suppressZero.BeginUpdate();
                _suppressZero.chkRows.Checked = setting[0];
                _suppressZero.chkColumn.Checked = setting[1];
            }
            finally
            {
                _suppressZero.EndUpdate();
            }
        }

        /// <summary>
        /// Returns the cached suppressed zero settings as a string..
        /// </summary>
        /// <param name="viewName">The name of the view.</param>
        /// <returns>the cached suppressed zero settings as a string.</returns>
        public string GetCachedSuppressZeroSelectionsAsString(string viewName)
        {
            bool[] settings = GetCachedSuppressZeroSelections(viewName);

            //Always pratice safe coding :)
            if (settings == null || settings.Length == 0)
            {
                return String.Empty;
            }

            string equals = PafApp.GetLocalization().GetResourceManager().GetString("Application.General.Equals");
            StringBuilder sb = new StringBuilder();

            sb.Append(PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.SelectedSuppreddedOptions"));
            sb.Append(PafAppConstants.CRLF);

            sb.Append(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewOptions.Row"));
            sb.Append(equals);
            sb.Append(settings[0]);
            sb.Append(PafAppConstants.CRLF);

            sb.Append(PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.ViewOptions.Column"));
            sb.Append(equals);
            sb.Append(settings[1]);
            sb.Append(PafAppConstants.CRLF);

            return sb.ToString();
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Builds a view tree, that can be added to the actions pane.
        /// </summary>
        /// <param name="visible">Is the control visible.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="userSelection">User selection id, to use as an unique key.</param>
        /// <returns>A ViewTree.</returns>
        /// <remarks>Added to fix //TTN-1293</remarks>
        private DimensionTree AddSimpleDimTree(pafUserSelection userSelection, string viewName, bool visible)
        {

            _viewTreeDimControls.Add(new DimensionTree());
            string dimension = userSelection.dimension;
            int i = _viewTreeDimControls.Count - 1;
            _viewTreeDimControls[i].subpane.Text = dimension;
            _viewTreeDimControls[i].Name = userSelection.id;
            _viewTreeDimControls[i].Id = userSelection.id;
            _viewTreeDimControls[i].DimensionName = dimension;
            _viewTreeDimControls[i].ControlIsVisible = visible;
            _viewTreeDimControls[i].MemberNameColumn = MemberColumnName;
            _viewTreeDimControls[i].VisibleColumnName = VisibleColumnName;
            _viewTreeDimControls[i].LevelColumnName = LevelColumnName;
            _viewTreeDimControls[i].GenerationColumnName = GenerationColumnName;
            _viewTreeDimControls[i].ParentFirst = !userSelection.parentLast;
            _viewTreeDimControls[i].EnableLevelRightClick = userSelection.levelRightClick;
            _viewTreeDimControls[i].EnableGenerationRightClick = userSelection.generationRightClick;
            _viewTreeDimControls[i].AliasTableNames = SimpleDimTrees.GetAliasTables(dimension);
            _viewTreeDimControls[i].PromptString = userSelection.promptString;
            _viewTreeDimControls[i].GenInfo = SimpleDimTrees.GetDimensionInformation(dimension, DimensionInfoType.Generation);
            _viewTreeDimControls[i].LevelInfo = SimpleDimTrees.GetDimensionInformation(dimension, DimensionInfoType.Level);
            _viewTreeDimControls[i].TreeList.VisibleColumnName = VisibleColumnName;
            _viewTreeDimControls[i].TreeList.LevelColumnName = LevelColumnName;
            _viewTreeDimControls[i].TreeList.MemberColumnName = MemberColumnName;
            _viewTreeDimControls[i].TreeList.GenerationColumnName = GenerationColumnName;
            _viewTreeDimControls[i].TreeList.OptionsSelection.MultiSelect = true;
            _viewTreeDimControls[i].TreeList.FilterNode += _DimControl_FilterNode;
            _viewTreeDimControls[i].TreeList.NodeChanged += _DimControl_NodeChanged;
            _viewTreeDimControls[i].SavedSelectionsChangedEvent += DimensionTree_SavedSelectionsChangedEvent;
            _viewTreeDimControls[i].ErrorPopupIcon = ErrorType.Warning;
            _viewTreeDimControls[i].ErrorDisplayText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning");
            _viewTreeDimControls[i].ErrorPopupText = PafApp.GetLocalization().GetResourceManager().GetString("Application.Warning.UserSelectionMethodChanged");
            _viewTreeDimControls[i].ViewAxis = userSelection.pafAxis.ToAxis();
            _viewTreeDimControls[i].SavedSelections = _savedSelections;

            ViewAxis viewAxis = _viewTreeDimControls[i].ViewAxis;
            bool? group = GetViewSectionGroupings(viewAxis, viewName, dimension);
            if (viewAxis == ViewAxis.Page) group = null;
            if (group != null && group.Value)
            {
                _viewTreeDimControls[i].HasGroupings = true;
            }
            else
            {
                _viewTreeDimControls[i].HasGroupings = false;
            }
            

            if (_base.Contains(dimension))
            {
                _viewTreeDimControls[i].BaseDimension = true;
                //TTN-2424, allow any dim to have root selectable.
                //var row = SimpleDimTrees.GetMember(dimension, dimension);
                //if (row != null)
                //{
                //    _viewTreeDimControls[i].TreeList.UnselectableLevels.Add(row.Level);
                //}
            }
            else
            {
                _viewTreeDimControls[i].BaseDimension = false;
            }


            //List<string> leafOnlyDims = new List<string>(
            //    Settings.Default.LeafLevelOnlyDimensions.Split(
            //        ",".ToCharArray(),
            //        StringSplitOptions.RemoveEmptyEntries));
            //TTN-2424, allow any dim to have root selectable.
            List<string> leafOnlyDims = new List<string>(2);
            leafOnlyDims.Add(PafApp.PlanTypeDimension);
            leafOnlyDims.Add(PafApp.VersionDimension);


            //only allow leaf level selections.
            if (leafOnlyDims.Contains(dimension))
            {
                SimpleDimTrees.MembersRow firstMember = SimpleDimTrees.GetFirstMember(dimension, 0);
                if (firstMember != null)
                {
                    _viewTreeDimControls[i].TreeList.SelectableGenerations.Add(firstMember.Generation);
                }
                _viewTreeDimControls[i].ZeroLevelOnly = true;
                _viewTreeDimControls[i].TreeList.SelectableLevels.Add(0);
            }
            else
            {
                _viewTreeDimControls[i].ZeroLevelOnly = false;
            }

            return _viewTreeDimControls[i];
        }



        

        /// <summary>
        /// Store the suppress zero settings for a view.
        /// </summary>
        /// <param name="viewName">Name of the view to store the settings.</param>
        private void StoreSuppressZeroSettings(string viewName)
        {
            bool[] settings = _cachedSuppredZeroSettings.ValueOrDefault(viewName);
            if (settings == null || settings.Length == 0)
            {
                settings = PafApp.GetViewTreeView().GetViewRowColumnSupressZeroEnabled(viewName);
            }
            else
            {
                settings[0] = _suppressZero.chkRows.Checked;
                settings[1] = _suppressZero.chkColumn.Checked;
            }

            StoreSuppressZeroSettings(viewName, settings);
        }

        /// <summary>
        /// Builds a ViewTree object, with the views information
        /// </summary>
        /// <param name="caption">The caption of the subpane of the view tree</param>
        private void BuildViewTree(string caption)
        {
            //Set the caption.
            _views.subpane.Text = caption;
            _views.DynamicColumn = ViewDynamic;
            _views.DescriptionColumn = ViewDescription;
            _views.SelectableColumn = CanSelect;
            //Don't allow multiselect
            //_Views.treeView1.MultiSelect = TreeViewMultiSelect.NoMulti;
            //Set the image list.
            _views.TreeList.StateImageList = _imageList;

            try
            {
                _views.TreeList.BeginUpdate();

                //Get the first level of views
                pafViewTreeItem[] baseViews = PafApp.GetViewTreeView().GetBaseViews();
                _views.TreeList.GetStateImage += treeView1_GetStateImage;
                //Make sure we have some view.
                if (baseViews == null)
                {
                    //If we don't have any views, then throw an Exception.
                    //throw new NullReferenceException();
                }
                else
                {
                    //We have some views, so build the view tree.
                    //DataTable dt = baseViews.ToTreeListDataTable(ParentKeyColumnName, MemberKeyColumnName, MemberColumnName, Can_Select);
                    //_Views.treeView1.BindData(dt, MemberKeyColumnName, ParentKeyColumnName, Can_Select.ToSingleList());
                    _views.TreeList = baseViews.CreateUnboundTree(_views.TreeList, ViewColumnName, CanSelect, ViewDescription, ViewDynamic);
                    _views.TreeList.ForceInitialize();
                    _views.TreeList.CollapseAll();
                    _views.TreeList.Selection.Clear();
                    _views.TreeList.FocusedNode = null;
                }

                //Add the NodeMouseClickHandler
                _views.TreeList.BeforeFocusNode += _Views_BeforeFocusNode;
                _views.TreeList.FocusedNodeChanged += _Views_FocusedNodeChanged;

                
            }
            catch (NullReferenceException)
            {
                Exception exp = new Exception(PafApp.GetLocalization().GetResourceManager().GetString("Application.Exception.NoViewsFound"));
                exp.Source = PafApp.GetLocalization().GetResourceManager().GetString("Application.Title");

                MessageBox.Show(exp.Message,
                    exp.Source,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error);

                PafApp.GetLogger().Warn(exp);
            }
            finally 
            {
                _views.TreeList.EndUpdate();
            }
        }

        
        /// <summary>
        /// Refreshes the controls on the actions pane.
        /// </summary>
        /// <param name="selectedViewNode">The View Node that has been selected.</param>
        private void RefreshActionsPane(TreeListNode selectedViewNode)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            //Local variable
            if (selectedViewNode != null)
            {
                bool isDynamic = false;

                string viewName = _views.TreeList.GetSelectedNodeText(ViewColumnName).ToString();

                //Is the view dynamic.
                pafUserSelection[] pafUserSel = PafApp.GetViewTreeView().GetUserSelections(viewName);
                if (pafUserSel != null && pafUserSel.Length > 0)
                {
                    isDynamic = true;
                    viewInfoResponse response = PafApp.GetServiceMngr().ViewInformation(viewName);
                    PafApp.GetViewTreeView().SetGroupingSpec(viewName, response.viewInfo.viewSectionInfo[0].groupingSpecs);
                    PafApp.GetViewTreeView().SetUserSelection(viewName, response.viewInfo.userSelections);
                }

                //Local variable
                AddDimensionControlsToPane(viewName);

                //enable/disable the "GetView" button.
                SetGetViewButtonStatus(viewName);

                //set the view options.
                SetViewOptions(viewName);

                //Check the check boxes
                SetSuppressZeroSettings(viewName);

                //Check to see if the view is dynamic.
                if (isDynamic)
                {
                    //If the view is dynamic then show the button.
                    //button.Visible = isDynamic;
                    //02/24/2006 KRM
                    PafApp.GetEventManager().SelectView(
                        viewName,
                        (Worksheet)Globals.ThisWorkbook.ActiveSheet,
                        false,
                        selectedViewNode);
                }
                else
                {
                    //Since the view is static, then hide the button, and click it automatically for the user.
                    _nextButton_Click(null, null);
                }
            }

            stopwatch.Stop();
            PafApp.GetLogger().InfoFormat("RefreshActionsPane runtime: {0} (ms)",  stopwatch.ElapsedMilliseconds.ToString("N0"));
        }

        /// <summary>
        /// Cache the PafUserSelections that are made by the user.
        /// </summary>
        /// <param name="viewName">The name of the view.</param>
        /// <param name="userSelections">The PafUserSelections made by the user.</param>
        public void CacheDyanmicPafUserSelections(string viewName, pafUserSelection[] userSelections)
        {
            _cachedPafUserSelections.AddOrUpdate(viewName, userSelections);
        }

        /// <summary>
        /// Checks all the Dimension tree controls to verify that the user has made selections where necessary.
        /// </summary>
        /// <returns>true if all the necessary selections have been made, false if not.</returns>
        private bool CheckTreeCtlForASelection(string dimensionName)
        {
            //Get the control from the actions pane
            DimensionTree tmp = (DimensionTree)Globals.ThisWorkbook.ActionsPane.Controls[dimensionName];
            if (tmp != null)
            {
                if (tmp.ControlIsVisible)
                {
                    //Single select.  Make sure the selectedNode is not null.
                    List<string> nodes = tmp.TreeList.GetCheckedNodes();
                    if (nodes == null)
                    { 
                        return false; 
                    }

                    if (!tmp.TreeList.OptionsSelection.MultiSelect)
                    {
                        if (nodes.Count == 0)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        //Muliselect, make sure the user has selected at least one item.
                        if (nodes.Count == 0)
                        {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        /// <summary>
        /// Enables or disables the view button depending on weather or not the view has dynamic selections.
        /// If the view has dynamic selections, then it looks too see if the user has made a selection for
        /// each of the dynamic dimensions.
        /// </summary>
        /// <param name="viewName">The name of the current view.</param>
        private void SetGetViewButtonStatus(string viewName)
        {
            //the view is static, and the user made a change to the suppression setting.
            if (_cachedSuppressedViews.Contains(viewName))
            {
                _nextButton.Visible = true;
                _nextButton.Enabled = true;
                _nextButton.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.ActionsPane.Button.Refresh");
                SetViewOptions(viewName);
                return;
            }

            //First check the view to see if it is dynamic.
            if (!PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName))
            {
                //The view is dynamic, so make sure the button is hidden and exit.
                _nextButton.Visible = false;
                return;
            }

            //Loop thru all the ViewTree dim controls in the list.
            foreach (DimensionTree tree in _viewTreeDimControls)
            {
                //Get the control from the actions pane
                DimensionTree tmp = (DimensionTree)Globals.ThisWorkbook.ActionsPane.Controls[tree.Name];

                if (tmp != null && tmp.ControlIsVisible)
                {
                    //TTN-1293
                    if (!CheckTreeCtlForASelection(tmp.Id))
                    {
                        _nextButton.Visible = true;
                        _nextButton.Enabled = false;
                        _nextButton.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.ActionsPane.Button.Build");
                        return;
                    }
                }
            }
            _nextButton.Visible = true;
            _nextButton.Enabled = true;
            _nextButton.Text = PafApp.GetLocalization().GetResourceManager().GetString("Application.Excel.ActionsPane.Button.Build");
        }

        /// <summary>
        /// Add the controls to the actions pane.
        /// </summary>
        private void AddControlsToActionPane()
        {
            Globals.ThisWorkbook.ActionsPane.Controls.Clear();

            //_header.HeaderText = "Status";
            //_header.ErrorIcon = ErrorType.Warning;
            //_header.ErrorPopupText = "User selection method changed.";
            //SetStatusPanelVisible(false);

            //Globals.ThisWorkbook.ActionsPane.Controls.Add(_header);

            //Globals.ThisWorkbook.ActionsPane.Controls.Add(_lineBreak[0]);

            Globals.ThisWorkbook.ActionsPane.Controls.Add(_userInformation);

            Globals.ThisWorkbook.ActionsPane.Controls.Add(_lineBreak[1]);

            Globals.ThisWorkbook.ActionsPane.Controls.Add(_views);

            Globals.ThisWorkbook.ActionsPane.Controls.Add(_lineBreak[2]);

            Globals.ThisWorkbook.ActionsPane.Controls.Add(_nextButton);

            Globals.ThisWorkbook.ActionsPane.Controls.Add(_lineBreak[3]);

            _lineBreak[3].Visible = false;

            Globals.ThisWorkbook.ActionsPane.Controls.Add(_suppressZero);

            //Globals.ThisWorkbook.ActionsPane.Controls.Add(_BlankBreak);

            foreach (DimensionTree ctl in _viewTreeDimControls)
            {
                if (ctl.TreeListState != null)
                {
                    ctl.TreeListState.LoadState();
                }
                Globals.ThisWorkbook.ActionsPane.Controls.Add(ctl);
            }

            PafApp.GetGridApp().PositionTaskPane(MsoBarPosition.msoBarLeft);

            foreach (DimensionTree tree in _viewTreeDimControls)
                tree.subpane.Collapse();
        }

        /// <summary>
        /// Clears all the controls from the actions pane.
        /// </summary>
        private void ClearActionPaneControls()
        {
            PafApp.GetViewTreeView().ReloadViewDictionary(PafApp.GetPafPlanSessionResponse().viewTreeItems);

            _viewTreeDimControls.Clear();

            //Create a new object...
            _views = new ViewTree { Name = "_Views", TreeList = { StateImageList = _imageList } };

            //_SimpleTrees.Clear();

            //PaceSimpleTrees.Clear();

            Globals.ThisWorkbook.ActionsPane.Controls.Clear();
        }

        /// <summary>
        /// Clears out all the cached user selections.
        /// </summary>
        /// <remarks>Fix TTN-771.</remarks>
        private void ClearCachedSelections()
        {
            ClearCachedPafUserSelections();
            ClearCachedUserSelections();
        }

        /// <summary>
        /// Loop thru the dimension controls, and reselect the nodes for the user(if they exist).
        /// </summary>
        /// <param name="viewName">Name of the view.</param>
        /// <returns>true if the view has dynamic selections, false if not.</returns>
        /// <remarks>This was modified to fix TTN-1293 and allow multiple dimension selectors on a view.</remarks>
        private bool AddDimensionControlsToPane(string viewName)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            //Local variable
            bool isDynamic = false;
            List<DimensionTree> viewTrees = new List<DimensionTree>();
            pafUserSelection[] pafUserSel = PafApp.GetViewTreeView().GetUserSelections(viewName);

            //get the dynamic labels (in case they are needed) - we only want to have to retrieve them once, not every time.
            string rowLabel = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.DimensionTree.Subpane.RowLabel");
            string colLabel = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.DimensionTree.Subpane.ColumnLabel");
            string pageLabel = PafApp.GetLocalization().GetResourceManager().GetString("Application.Controls.DimensionTree.Subpane.PageLabel");

            try
            {
                PafApp.GetGridApp().ScreenUpdating = false;

                //Loop thru all the ViewTree and remove em'.
                foreach (DimensionTree tree in _viewTreeDimControls)
                {
                    //get the item.
                    DimensionTree tmp = (DimensionTree)Globals.ThisWorkbook.ActionsPane.Controls[tree.Name];

                    if (tree.TreeListState != null)
                    {
                        tree.TreeListState.SaveState();
                    }

                    //store it.
                    viewTrees.Add(tmp);

                    //Get the control from the actions pane
                    Globals.ThisWorkbook.ActionsPane.Controls.Remove(tree);
                }


                List<DimensionTree> treesToAdd = new List<DimensionTree>();
                //add the paf user selections controls first.
                if (pafUserSel != null)
                {
                    foreach (pafUserSelection pu in pafUserSel)
                    {
                        if (pu == null) continue;

                        string dimension = pu.dimension;
                        bool visible = SimpleDimTrees.GetDimensionIsHidden(dimension);

                        DimensionTree vt = AddSimpleDimTree(pu, viewName, visible);
                        //TTN-1300
                        if (vt != null)
                        {
                            treesToAdd.Add(vt);
                        }
                    }
                }

                viewTrees.Clear();
                viewTrees.AddRange(treesToAdd);

                if (viewTrees.Count > 0)
                {
                    //then add the remaining tree controls.
                    foreach (DimensionTree tree in viewTrees)
                    {
                        if (!Globals.ThisWorkbook.ActionsPane.Controls.Contains(tree))
                        {
                            Globals.ThisWorkbook.ActionsPane.Controls.Add(tree);
                        }
                    }
                }


                //clear out the old list, and readd the ViewTree objects.
                _viewTreeDimControls.Clear();
                _viewTreeDimControls.AddRange(viewTrees);

                //Loop thru all the ViewTree dim controls in the list.
                foreach (DimensionTree tree in _viewTreeDimControls)
                {
                    string treeId = tree.Id;
                    string dimensionName = tree.DimensionName;
                    //See if the dim has dynamic selections, if it does then set the control to be visible,
                    //if not then hide it.
                    tree.ControlIsVisible = PafApp.GetViewTreeView().DoesViewDimHaveDynamicSelections(viewName, dimensionName);

                    //If the view allows multi select, then turn it on for the tree control.
                    tree.TreeList.MultiCheck = PafApp.GetViewTreeView().DoesViewDimAllowMultipleSelections(viewName, dimensionName, treeId);

                    if (tree.ControlIsVisible)
                    {
                        //Get the prompt string from the server.
                        string displayString = PafApp.GetViewTreeView().GetUserSelectionDisplayString(viewName, dimensionName, treeId);
                        HashSet<string> filterList = PafApp.GetViewTreeView().GetUserSelectionSpecification(viewName, dimensionName, treeId);
                        HashSet<string> defaultSelections = PafApp.GetViewTreeView().GetUserSelectionDefaultSelections(viewName, dimensionName, treeId);
                        string primRowFormat = PafApp.GetViewTreeView().GetPrimaryRowColumnFormat(viewName, dimensionName);
                        string displayColumn = MemberColumnName;
                        switch (primRowFormat.ToLower())
                        {
                            case PafAppConstants.ALIAS_MAPPING_FORMAT_ALIAS:
                                displayColumn = PafApp.GetViewTreeView().GetAliasTableName(viewName, dimensionName);
                                break;
                        }

                        if (!String.IsNullOrWhiteSpace(displayString))
                        {
                            tree.subpane.Text = displayString.Trim();
                        }
                        else
                        {
                            //Get the axis that this dimension is on.
                            ViewAxis viewAxis = PafApp.GetViewTreeView().GetDimensionAxis(viewName, dimensionName);

                            if (viewAxis != ViewAxis.Unknown)
                            {
                                //Since the axis is not null, then add the text to the dimension name.
                                switch (viewAxis)
                                {
                                    case ViewAxis.Row:
                                        tree.subpane.Text = dimensionName + rowLabel;
                                        break;
                                    case ViewAxis.Col:
                                        tree.subpane.Text = dimensionName + colLabel;
                                        break;
                                    case ViewAxis.Page:
                                        tree.subpane.Text = dimensionName + pageLabel;
                                        break;
                                    default:
                                        tree.subpane.Text = dimensionName;
                                        break;
                                }
                            }
                            else
                            {
                                //No axis information, so just add the dimension name.
                                tree.subpane.Text = dimensionName;
                            }
                        }
                        //Restore the users parentFirst selecton.
                        bool parentFirst;
                        if (_parentFirst.TryGetValue(treeId, out parentFirst))
                        {
                            tree.ParentFirst = parentFirst;
                        }

                        //get the cached tree nodes from the cache structure.
                        List<string> tnc = _cachedTreeViews.GetValues(new Tuple<string, string>(viewName, treeId)) ?? new List<string>();
                        if (tnc == null || tnc.Count == 0)
                        {
                            tnc = _savedSelections.Values(treeId);
                            if (tnc == null || tnc.Count == 0)
                            {
                                tnc = new List<string>();
                            }
                        }

                        //build the tree.
                        bool wasSomethingHidden = false; //TTN-2385
                        DataTable dataTable = SimpleDimTrees.ToTreeListDataTable(dimensionName, VisibleColumnName, typeof(byte), 1);
                        if (filterList != null && filterList.Count > 0)
                        {
                            var result = dataTable.AsEnumerable().Where(x => !filterList.Contains(x.Field<string>(MemberColumnName)));

                            foreach (var row in result)
                            {
                                row[VisibleColumnName] = 0;
                                wasSomethingHidden = true;
                            }
                        }
                        tree.FilterTree = wasSomethingHidden;

                        List<string> aliasTables = SimpleDimTrees.GetAliasTables(dimensionName);
                        aliasTables.Add(LevelColumnName);
                        aliasTables.Add(GenerationColumnName);
                        aliasTables.Add(VisibleColumnName);
                        aliasTables.Add(MemberColumnName);
                        tree.TreeList.BindData(dataTable, MemberKeyColumnName, ParentKeyColumnName, dimensionName, aliasTables, enableFiltering: false);
                        tree.TreeList.Columns[displayColumn].Visible = true;
                        if (tree.TreeListState == null)
                        {
                            //tree.TreeList.ExpandAll();
                            //tree.TreeListState = new TreeListViewState(tree.TreeList);
                        }
                        else
                        {
                            //tree.TreeListState.LoadState();
                        }
                        tree.DisplayColumn = displayColumn;

                        //tree.TreeList.CollapseAll();

                        bool validate = false;
                        string prefix = "Saved";
                        //TTN-1526
                        if (tnc.Count == 0)
                        {
                            if (defaultSelections != null)
                            {
                                tree.TreeList.CheckNodes(defaultSelections, MemberColumnName.ToSingleList(), true);
                                prefix = "Default";
                                validate = true;
                            }
                            else
                            {
                                tree.TreeList.CheckNode(tree.TreeList.FirstSelectableNode);
                            }
                        }
                        else
                        {
                            //now reset the selections...
                            tree.TreeList.CheckNodes(tnc, MemberColumnName.ToSingleList(), true);
                            validate = true;
                        }


                        //If using saved (not cached selections) verify what was checked.
                        if (validate)
                        {
                            //Get the nodes that were checked
                            var nodes = tree.TreeList.GetCheckedNodes();
                            //If nothing was checked, then log it and default to legacy settings
                            if (nodes == null || nodes.Count == 0)
                            {
                                PafApp.GetLogger().WarnFormat("No {0} members can be rechecked on user selector {1} default selections will be applied", prefix, treeId);
                                tree.TreeList.CheckNode(tree.TreeList.FirstSelectableNode);
                            }
                            else
                            {
                                HashSet<string> tncSet = new HashSet<string>(tree.TreeList.GetCheckedNodes());
                                foreach (var node in tnc)
                                {
                                    if (!tncSet.Contains(node))
                                    {
                                        PafApp.GetLogger().WarnFormat("{0} user selection '{1}' cannot be found in tree.", prefix, node);
                                    }
                                }
                            }
                        }

                        tree.ValidateTree();

                        Dictionary<string, TreeListViewState> dict = null;
                        viewDimTreeListStates.TryGetValue(viewName, out dict);
                        if (dict == null)
                        {
                            dict = new Dictionary<string, TreeListViewState>();
                            dict.Add(dimensionName, new TreeListViewState(tree.TreeList));
                            viewDimTreeListStates.Add(viewName, dict);
                        }
                        else
                        {
                            TreeListViewState tlvs = null;
                            dict.TryGetValue(dimensionName, out tlvs);
                            if (tlvs == null)
                            {
                                dict.Add(dimensionName, new TreeListViewState(tree.TreeList));
                            }
                            else
                            {
                                tlvs.LoadState();
                            }
                        }

                        //get the previous saved state of the subpane
                        List<bool> sps = _cachedSubpaneStatus.GetCachedSelections(viewName, treeId);
                        if (sps != null)
                        {
                            if (sps[0])
                            {
                                tree.subpane.Expand();
                            }
                            else
                            {
                                tree.subpane.Collapse();
                            }
                        }
                        else //(no saved state)
                        {
                            tree.subpane.Collapse();
                        }
                        tree.ApplyFilter();
                    }

                    //If we have a control that is visible, then the view is dynamic, so set the flag to true.
                    if (tree.ControlIsVisible)
                    {
                        isDynamic = true;
                    }
                }
                stopwatch.Stop();
                PafApp.GetLogger().InfoFormat("AddDimensionControlsToPane runtime: {0} (ms)", stopwatch.ElapsedMilliseconds.ToString("N0"));
                return isDynamic;
            }
            finally
            {
                PafApp.GetGridApp().ScreenUpdating = true;
            }
        }

        private bool GetViewSectionGroupings(ViewAxis viewAxis, string viewName, string dimensionName)
        {
            if (viewAxis == ViewAxis.Page) return false;
            GroupingSpec spec = PafApp.GetViewTreeView().GetViewGroupingSpec(viewAxis, viewName);
            if (spec == null) return false;

            if (spec.DimensionName != dimensionName) return false;

            return spec.ApplyGroups;
        }

        /// <summary>
        /// Gets the ParentFirst selection of the user selector.
        /// </summary>
        /// <param name="userSelId"></param>
        /// <returns></returns>
        private bool GetParentFirst(string userSelId)
        {
            if (_parentFirst == null) return false;

            bool val;

            _parentFirst.TryGetValue(userSelId, out val);

            return val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <remarks>Removed becuase we don't support the API any longer.</remarks>
        //public void LoadAutomationObject()
        //{
        //    if (_addinUtilities == null)
        //    {
        //        object addInProgId = "Titan.Addin";
        //        _addinUtilities = (ICustomAction)Globals.ThisWorkbook.Application.COMAddIns.Item(ref addInProgId).Object;
        //    }
        //}

        #endregion Private Methods

        #region Event Handlers
        // ReSharper disable InconsistentNaming

        /// <summary>
        /// Event handler to populate the view tree icons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        [DebuggerHidden]
        private void treeView1_GetStateImage(object sender, GetStateImageEventArgs e)
        {

            if (e.Node is TreeListAutoFilterNode)
            {
                return;
            }
            object value = e.Node.GetValue("CanSelect");

            if (value == null)
            {
                return;
            }

            if ((bool)value)
            {
                e.NodeImageIndex = 2;
                return;
            }
            e.NodeImageIndex = e.Node.Expanded ? 0 : 1;
        }

        /// <summary>
        /// Event that fires before a node is given focus in the views tree.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _Views_BeforeFocusNode(object sender, BeforeFocusNodeEventArgs e)
        {
            //If the selected node is a folder, then don't allow it to be selected..
            //Make sure the node is not null.
            if (e.Node != null)
            {
                if (e.Node is TreeListAutoFilterNode)
                {
                    return;
                }
                object value = e.Node.GetValue("CanSelect");

                if (value == null)
                {
                    e.CanFocus = false;
                }
                else if (!(bool)value)
                {
                    e.CanFocus = false;

                }
                //if (e.Node.SelectedImageIndex == 1)
                //{
                //    //Ok, lowlight it.
                //    tree.LowlightNode(e.Node);
                //    //Cancel this event, since we lowlighted the node for the user.
                //    e.Cancel = true;
                //}
                else if (PafApp.GetGridApp().InEditMode())
                {
                    e.CanFocus = false;
                    //    //Ok, lowlight it.
                    //    tree.LowlightNode(e.Node);
                    //    //Cancel this event, since we lowlighted the node for the user.
                    //    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// Event that fires when the focus of a view node has changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _Views_FocusedNodeChanged(object sender, NodeEventArgs e)
        {
            try
            {
                if (e.Node == null)
                {
                    return;
                }
                if (e.Node is TreeListAutoFilterNode)
                {
                    return;
                }

                object value = e.Node.GetValue("CanSelect");

                if (value == null)
                {
                    return;
                }
                if (!(bool)value)
                {
                    return;
                }

                if (!e.Node.Visible)
                {
                    return;
                }

                SelectViewTreeSelectedNode(e.Node);

                Globals.ThisWorkbook.Application.EnableEvents = true;

                RefreshActionsPane(e.Node);
            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        /// <summary>
        /// Event that fires when the control is filtered.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DimControl_FilterNode(object sender, FilterNodeEventArgs e)
        {
            if (e.Node == null) return;

            DimensionTree host = (DimensionTree) ((TreeList) sender).Parent.Parent;
            if (host.FilterTree && !host.UserFilteredEnabled) return;

            PaceDimensionXtraTree treeList = (PaceDimensionXtraTree)e.Node.TreeList;
            
            if (treeList != null)
            {
                treeList.ExpandAll();
                e.Handled = false;
            }
        }

        /// <summary>
        /// Event that fires after an item is checked in the dimension tree.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DimControl_NodeChanged(object sender, NodeChangedEventArgs e)
        {
            //double check the get view button.
            if (_views.TreeList.GetSelectedNode() == null) return;
            SetGetViewButtonStatus(_views.TreeList.GetSelectedNodeText(ViewColumnName).ToString());
        }


        /// <summary>
        /// The method is unused but here as an example.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void _DimControl_MouseUp(object sender, MouseEventArgs e)
        {
            //Get the dimension tree
            TreeList tree = (TreeList)sender;
            if (e.Button == MouseButtons.Right)
            {
                Point clickPoint = new Point(e.X, e.Y);
                TreeListHitInfo ht = tree.CalcHitInfo(clickPoint);
                //TreeNode ClickNode = tree.GetNodeAt(ClickPoint);
                if (ht.Node == null) return;
                // Convert from Tree coordinates to Screen coordinates
                Point ScreenPoint = tree.PointToScreen(clickPoint);
                // Convert from Screen coordinates to Form coordinates
                Point FormPoint = tree.PointToClient(ScreenPoint);

            } 
        }

        /// <summary>
        /// Callback handler for when a Grouping status is changed.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void Ribbon_ViewSectionGroupChanged(object sender, ViewSectionGroupChangedEventArgs eventArgs)
        {
            if (eventArgs == null) return;
            var control = _viewTreeDimControls.FirstOrDefault(x => x.DimensionName.Equals(eventArgs.DimensionName));
            if (control == null) return;

            control.HasGroupings = eventArgs.GroupEnabled;
        }


        /// <summary>
        /// Event that fires when the user clicks a suppress zero checkbox.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="state"></param>
        private void SuppressZero_ChkBoxCheckChange(object sender, EventArgs e, bool state)
        {
            if (_views.TreeList.Selection != null)
            {
                string viewName = _views.TreeList.GetSelectedNodeText(ViewColumnName).ToString();
                _cachedSuppressedViews.Add(viewName);
                if (!PafApp.GetViewTreeView().DoesViewHaveDynamicSelections(viewName))
                {
                    SetGetViewButtonStatus(viewName);
                }
            }
        }

        /// <summary>
        /// Click the GetView Button.
        /// </summary>
        /// <param name="sender">Object sender.</param>
        /// <param name="e">EventArgs.</param>
        private void _nextButton_Click(object sender, EventArgs e)
        {
            try
            {
                //frmDimensionPopout dp = new frmDimensionPopout();

                Stopwatch startTime = Stopwatch.StartNew();
                //_Views.treeView1.SelectedNode = PafApp.GetViewMngr().GetViewNode();
                //TTN-1069
                if (PafApp.GetGridApp().InEditMode())
                {
                    MessageBox.Show(
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.InExcelEditMode"),
                        PafApp.GetLocalization().GetResourceManager().GetString("Application.Title"),
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Warning);

                    return;
                }

                var viewTreeView = PafApp.GetViewTreeView();


                //Get the view sels for the selected view
                pafUserSelection[] pafViewUserSel = viewTreeView.GetUserSelections(_views.TreeList.GetSelectedNodeText(ViewColumnName).ToString());
                bool updated = false;
                //is the suppress zero flipper visible on the actions pane.
                bool canSuppress = _suppressZero.Visible;
                //get the view name.
                string viewName = _views.TreeList.GetSelectedNodeText(ViewColumnName).ToString();
                //if the view can be suppress, the store the settings.
                if (canSuppress)
                {
                    StoreSuppressZeroSettings(viewName);
                }

                //If there are some selectable dimensions
                if (pafViewUserSel != null)
                {
                    foreach (pafUserSelection userSel in pafViewUserSel)
                    {
                        if (userSel == null) continue;
                        try
                        {
                            if (!CheckTreeCtlForASelection(userSel.dimension))
                            {
                                throw new Exception(PafApp.GetLocalization().GetResourceManager().GetString("Application.MessageBox.MissingSelection") + userSel.dimension);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }

                        //Get the control from the actions pane
                        //TTN-1293
                        DimensionTree tmp = (DimensionTree)Globals.ThisWorkbook.ActionsPane.Controls[userSel.id];
                        ViewAxis viewAxis = userSel.pafAxis.ToAxis();
                        //Make sure it's something...
                        if (tmp != null && tmp.TreeList != null)
                        {
                            bool parentFirst;
                            if (_parentFirst.TryGetValue(tmp.Id, out parentFirst))
                            {
                                _parentFirst[tmp.Id] = tmp.ParentFirst;
                            }
                            else
                            {
                                _parentFirst.Add(tmp.Id, tmp.ParentFirst);
                            }

                            //cache the subpane status.
                            bool subPaneStatus = tmp.subpane.IsExpanded;
                            //TTN-1293
                            _cachedSubpaneStatus.CacheSelection(viewName, tmp.Id, subPaneStatus);

                            try
                            {
                                TreeListViewState tlvs = viewDimTreeListStates[viewName][tmp.DimensionName];
                                if (tlvs != null)
                                {
                                    tlvs.SaveState();
                                }
                            }
                            catch (Exception) { }

                            Tuple<string, string> key = new Tuple<string, string>(viewName, tmp.Id);
                            //User can only sel one item, so just get it and set it.
                            if (!tmp.TreeList.MultiCheck)
                            {
                                userSel.values = new[] { tmp.TreeList.GetCheckedNode().GetSimpleTreeText(SimpleDimTrees) };
                                _cachedTreeViews.RemoveSelection(key);
                                _cachedTreeViews.AddSelections(key, tmp.TreeList.GetCheckedNodes().ToArray(), false);
                                updated = true;
                            }
                            else  //User can sel mul. items.
                            {
                                //Get the nodes into the hash - this is tree order.
                                //List<string> hash1 = tmp.TreeList.GetCheckedNodes(MemberColumnName);
                                
                                bool? group = GetViewSectionGroupings(viewAxis, viewName, tmp.DimensionName );
                                if (viewAxis == ViewAxis.Page) group = null;
                                List<string> hash;
                                if (group == null || !group.Value)
                                {
                                    //SetStatusPanelVisible(false);
                                    if (tmp.IsParentFirst)
                                    {
                                        hash = tmp.TreeList.GetCheckedNodes(true);
                                    }
                                    else
                                    {
                                        hash = tmp.TreeList.DeptFirstNodes(true).GetNodesText(MemberColumnName);
                                        hash.Reverse();

                                    }
                                }
                                else
                                {
                                    //SetStatusPanelVisible(true, tmp.subpane.Text, "User selection method changed from selection order to tree order.");
                                    hash = tmp.TreeList.DeptFirstNodes(true).GetNodesText(MemberColumnName);
                                    if (!tmp.IsParentFirst)
                                    {
                                        hash.Reverse();
                                    }
                                }


                                //Create and array to hold the hash values.
                                userSel.values = new string[hash.Count];
                                //Copy the MWTreeNodes into the string array.
                                for (int i = 0; i < hash.Count; i++)
                                {
                                    if (hash[i] != null)
                                    {
                                        userSel.values[i] = hash[i];
                                        updated = true;
                                    }
                                }
                                //Cache the nodes.
                                //TTN-1293
                                _cachedTreeViews.RemoveSelection(key);
                                _cachedTreeViews.AddSelections(key, hash.ToArray(), false);
                            }
                        }
                    }
                    if (updated)
                    {
                        //Now cache the PafUserSelections, so they can be used on refresh.
                        //create a new array to add to the dictionary.
                        pafUserSelection[] arr = new pafUserSelection[pafViewUserSel.Length];
                        //Copy to the temporary array.
                        pafViewUserSel.CopyTo(arr, 0);
                        //Now cache the temporary values.
                        _cachedPafUserSelections.AddOrUpdate(viewName, arr);
                    }
                }

                //TTN-2549
                //string dir = Path.GetDirectoryName(_saveToPath);
                //if (!String.IsNullOrEmpty(dir) && !Directory.Exists(dir))
                //{
                //    Directory.CreateDirectory(dir);
                //}

                ////Write the user selections to xml
                //using (var writer = XmlWriter.Create(_saveToPath))
                //{
                //    (new XmlSerializer(typeof(MultiKeyDictionary<string, string, string>))).Serialize(writer, _cachedTreeViews);
                //}

                bool forceUpdate = sender != null;

                Chart chart = Globals.ThisWorkbook.ActiveChart;
                if (chart != null)
                {
                    if (!PafApp.GetEventManager().ActivateView(_views.TreeList.GetSelectedNodeText(ViewColumnName).ToString()))
                    {
                        //the activte view request failed, so we must render the view.
                        //since we can't send the chart as a worksheet. 
                        //send the first worksheet, if it exists.
                        if(Globals.ThisWorkbook.Worksheets.Count > 0)
                        {
                            PafApp.GetEventManager().SelectView(
                                _views.TreeList.GetSelectedNodeText(ViewColumnName).ToString(),
                                (Worksheet)Globals.ThisWorkbook.Worksheets[1],
                                forceUpdate,
                                _views.TreeList.GetSelectedNode());
                        }
                    }
                }
                else
                {
                    //Get the current worksheet.
                    Worksheet sheet =
                        (Worksheet)Globals.ThisWorkbook.ActiveSheet;

                    bool val =
                       PafApp.GetGridApp().DoesSheetContainCustomProperty(
                           sheet, 
                           PafAppConstants.OUR_VIEW_PROP_VALUE);

                    //Make sure we have one.
                    if (sheet == null)
                        return;

                    if (PafApp.GetViewMngr().CurrentView != null &&
                        PafApp.GetViewMngr().CurrentView.GetOlapView().ColDims != null &&
                        PafApp.GetViewMngr().CurrentView.GetOlapView().RowDims != null &&
                        PafApp.GetViewMngr().CurrentView.GetOlapView().Headers != null &&
                        PafApp.GetViewMngr().CurrentView.GetOlapView().ColDims.Count > 0 &&
                        PafApp.GetViewMngr().CurrentView.GetOlapView().RowDims.Count > 0 &&
                        PafApp.GetViewMngr().CurrentView.GetOlapView().Headers.Count > 0 && val)
                    {
                        //update cell notes, before deactivate.
                        PafApp.GetCellNoteMngr().CopyAndWriteAllUpdatesInsertsDeletsToClientCache(
                            PafApp.GetViewMngr().CurrentView);
                    }

                    startTime.Stop();
                    PafApp.GetLogger().InfoFormat("GetViewButtonClick, runtime: {0} (ms)", startTime.ElapsedMilliseconds.ToString("N0"));

                    //Send the view request.
                    PafApp.GetEventManager().SelectView(
                        _views.TreeList.GetSelectedNodeText(ViewColumnName).ToString(),
                        sheet,
                        forceUpdate,
                        _views.TreeList.GetSelectedNode());
                }

            }
            catch (Exception ex)
            {
                PafApp.MessageBox().Show(ex);
            }
        }

        private void DimensionTree_SavedSelectionsChangedEvent(object sender, SavedSelectionsChangedEventArgs e)
        {
            if (sender == null) return;

            if (e.SavedCleared == SavedCleared.Saved)
            {
                //Save to disk
                _savedSelections.AddKeyValue(e.Id, e.Selections, true);
                PafApp.GetLogger().InfoFormat("Saving user selection for id: {0}", e.Id);
            }
            else if (e.SavedCleared == SavedCleared.Updated)
            {
                //Save to disk
                _savedSelections.AddKeyValue(e.Id, e.Selections, true);
                PafApp.GetLogger().InfoFormat("Saving user selection for id: {0}", e.Id);
                //Need to purge 
                foreach (var v in _cachedTreeViews.Dictionary)
                {
                    Tuple<string, string> key = new Tuple<string, string>(v.Item1, v.Item2);
                    if (key.Item2.Equals(e.Id))
                    {
                        _cachedTreeViews.RemoveSelection(key);
                        _cachedTreeViews.AddSelections(key, e.Selections, false);
                    }
                    string s = "";
                }
            }
            else
            {
                //Clear either all or current dimension
                if (e.AllDimensions)
                {
                    _savedSelections.ClearAllKeyValues();
                    PafApp.GetLogger().Info("Removing all saved user selections");
                }
                else
                {
                    _savedSelections.RemoveKeyValue(e.Id);
                    PafApp.GetLogger().InfoFormat("Removing saved user selection for id: {0}", e.Id);
                }
            }

            PafApp.GetPafAppSettingsHelper().SaveUserSelectorSavedSelections(_savedSelections);
        }

        // ReSharper restore InconsistentNaming
        #endregion Event Handlers
    }
}
