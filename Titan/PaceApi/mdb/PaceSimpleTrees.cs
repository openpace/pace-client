#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <summary>
    /// Class to hold multiple PaceSimpleTrees.  
    /// This allows you to access the tree by dimension name or index.
    /// </summary>
    [Serializable]
    [Guid("80B63C6E-4D03-4d00-97F4-231B88F105A8"),
     ProgId("Pace.SimpleTrees"),
     ClassInterface(ClassInterfaceType.None)]
    public class PaceSimpleTrees : ISimpleTrees
    {
        #region Private Variables
        /// <summary>
        /// Structure to track names of trees.
        /// </summary>
        private readonly Dictionary<string, int> _treeNameHash;

        /// <summary>
        /// List to store the selectability of the SimpleTrees.
        /// </summary>
        private readonly List<bool> _treeSelectable;

        /// <summary>
        /// List to store the PafSimpleTrees.
        /// </summary>
        private readonly List<SimpleDimTree> _trees;

        /// <summary>
        /// List to store the SimpleTreeMemebers (hashed PafSimpleMembers).
        /// </summary>
        private readonly List<PaceSimpleTreeMembers> _treeMembers;

        Dictionary<string,Dictionary<string, int>> _traversedMembers;

        #endregion Private Variables

        #region Constructor
        /// <summary>
        /// Constructor.
        /// </summary>
        public PaceSimpleTrees()
        {
            _trees = new List<SimpleDimTree>();
            _treeNameHash = new Dictionary<string, int>();
            _treeSelectable = new List<bool>();
            _treeMembers = new List<PaceSimpleTreeMembers>();
            _traversedMembers = new Dictionary<string, Dictionary<string, int>>();
        }

        #endregion Constructor

        #region Private Members

        /// <summary>
        /// Removes an item from the list and the hash.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <param name="index">Tree's hash index.</param>
        private void Remove(string treeName, int index)
        {
            _trees.RemoveAt(index);
            _treeNameHash.Remove(treeName);
            _treeSelectable.RemoveAt(index);
            _treeMembers.RemoveAt(index);
        }

        private PaceSimpleTreeMembers GetTreeMembers(string treeName)
        {
            if (String.IsNullOrEmpty(treeName)) return null;

            int i = _treeNameHash[treeName];

            if (i > -1)
            {
                return _treeMembers[i];

            }
            return null;
        }

        #endregion Private Members

        #region Public Members
        /// <summary>
        /// Add a PafSimpleTree to the objects internal list.
        /// </summary>
        /// <param name="item">PafSimpleTree object to insert.</param>
        /// <param name="treeName">Case sensitive name of tree.  Can be used to access tree.</param>
        /// <param name="isSelectable">Can the user select from this dimension.</param> 
        public void Add(SimpleDimTree item, string treeName, bool isSelectable)
        {
            _trees.Add(item);
            _treeNameHash.Add(treeName, _trees.Count - 1);
            _treeSelectable.Insert(_trees.Count - 1, isSelectable );
            _treeMembers.Add(new PaceSimpleTreeMembers(item));
        }

        /// <summary>
        /// Finds the children of a parent within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        public string[] GetChildren(string treeName, string parent)
        {
            PaceSimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if (simpleTree == null) return null;

            return simpleTree.GetChildren(parent);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="treeName"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        public string GetParent(string treeName, string child)
        {

            PaceSimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if (simpleTree != null)
            {
                SimpleDimMember simpleMember = simpleTree.GetPafSimpleMember(child);
                if (simpleMember != null)
                {
                    return simpleMember.ParentKey;
                }
            }
        
            return null;
        }

        /// <summary>
        /// Finds the children of a parent within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        public PaceSimpleTreeChildMembers[] GetChildrenAlias(string treeName, string parent, string aliasTable)
        {

            PaceSimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if(simpleTree == null) return null;

            return simpleTree.GetChildrenAliases(aliasTable, parent);
            
        }

        /// <summary>
        /// Finds the alias of a member within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member">The name of the member, for which you want the alias</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>The alias string</returns>
        public string GetAlias(string treeName, string member, string aliasTable)
        {
            PaceSimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            if(simpleTree == null) return null;

            return simpleTree.GetPafSimpleMemeberAliasValue(member, aliasTable);
 
        }

        /// <summary>
        /// Finds the a member within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member">The name of the member, for which you want the alias</param>
        /// <returns>A pafSimpleDimMember</returns>
        public SimpleDimMember GetMember(string treeName, string member)
        {
            PaceSimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            return simpleTree.GetPafSimpleMember(member);
        }


        /// <summary>
        /// Returns an array of aliases for an array of Paf Simple Members.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="members">The array of member names for which you want the aliases</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>The alias string</returns>
        public string[] GetAlias(string treeName, string[] members, string aliasTable)
        {
            PaceSimpleTreeMembers simpleTree = GetTreeMembers(treeName);

            return simpleTree.GetPafSimpleMemeberAliasValue(members, aliasTable);
        
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="treeName"></param>
        /// <returns></returns>
        public string GetFirstLevelZeroMember(string treeName)
        {
            PaceSimpleTreeMembers simpleTree = _treeMembers[_treeNameHash[treeName]];

            foreach (KeyValuePair<string, int> member in GetTraversedMembers(treeName))
            {
                if (simpleTree.GetPafSimpleMember(member.Key).SimpleDimMemberProps.LevelNumber == 0)
                {
                    string memberName = member.Key;
                    return memberName;
                }
            }

            return null;
        }

        /// <summary>
        /// Finds the Prev or Next member at the same level within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member"></param>
        /// <param name="offset"></param>
        /// <returns>A string member name</returns>
        public string GetOffset(string treeName, string member, int offset)
        {
            if (String.IsNullOrEmpty(treeName)) return null;

            int offsetCounter = 0;
            SimpleDimTree tree = GetTree(treeName);
            
            if (GetTraversedMembers(treeName).ContainsKey(member))
            {
                int memberIndex = GetTraversedMembers(treeName)[member];
                int memberCounter = memberIndex;

                //find the offset in the array
                if (offset == 0)
                {
                    return member;
                }
                if (offset < 0)
                {
                    while (memberCounter > 0)
                    {
                        memberCounter--;
                        PaceSimpleTreeMembers simpleTree = _treeMembers[_treeNameHash[treeName]];

                        if (simpleTree.GetPafSimpleMember(tree.TraversedMembers[memberIndex]).
                                SimpleDimMemberProps.LevelNumber == simpleTree.GetPafSimpleMember(
                                                                        tree.TraversedMembers[memberCounter]).
                                                                        SimpleDimMemberProps.LevelNumber)
                        {
                            offsetCounter--;
                        }

                        if (offsetCounter == offset)
                        {
                            return tree.TraversedMembers[memberCounter];
                        }
                    }
                }
                else if (offset > 0)
                    while (memberCounter < tree.TraversedMembers.Length - 1)
                    {
                        memberCounter++;
                        PaceSimpleTreeMembers simpleTree = _treeMembers[_treeNameHash[treeName]];

                        if (simpleTree.GetPafSimpleMember(tree.TraversedMembers[memberIndex]).
                                SimpleDimMemberProps.LevelNumber == simpleTree.GetPafSimpleMember(
                                                                        tree.TraversedMembers[memberCounter]).
                                                                        SimpleDimMemberProps.LevelNumber)
                        {
                            offsetCounter++;
                        }

                        if (offsetCounter == offset)
                        {
                            return tree.TraversedMembers[memberCounter];
                        }
                    }
            }

            //return "" if the member is not in the tree.  E.g. - the Member Time
            return String.Empty;
        }

        /// <summary>
        /// Gets an array of tree names.
        /// </summary>
        /// <returns>An array of dimension names</returns>
        public string[] GetTreeNames()
        {
            string []names = null;
            _treeNameHash.Keys.CopyTo(names, 0);

            return names;
        }

        /// <summary>
        /// Returns a PafSimpleTree, with a specific name.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <returns>A PafSimpleTree</returns>
        public SimpleDimTree GetTree(int index)
        {
            try
            {
                return _trees[index];
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Returns a PafSimpleTree, with a specific name.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to return.</param>
        /// <returns>A PafSimpleTree</returns>
        public SimpleDimTree GetTree(string treeName)
        {
            try
            {
                int i = _treeNameHash[treeName];
                return _trees[i];
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <remarks/>
        public Dictionary<string, int> GetTraversedMembers(string treeName)
        {
            try
            {
                if (_traversedMembers == null)
                {
                    _traversedMembers = new Dictionary<string, Dictionary<string, int>>();
                }

                if (! _traversedMembers.ContainsKey(treeName))
                {
                    Dictionary<string, int> members = new Dictionary<string, int>();

                    int i = 0;
                    foreach(string travMember in _trees[_treeNameHash[treeName]].TraversedMembers)
                    {
                        members.Add(travMember, i++);
                    }

                    _traversedMembers.Add(treeName, members);
                }
           
                return _traversedMembers[treeName];
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Method to return if the tree is selectable.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <returns>Returns a boolean if the tree is selectable.</returns>
        public bool IsTreeSelectable(string treeName)
        {
            try
            {
                int i = _treeNameHash[treeName];
                return _treeSelectable[i];
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Sets the selectability of the SimpleTree.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <param name="isSelectable">Select or deselect the tree.</param>
        public void TreeSelectable(int index, bool isSelectable)
        {
            try
            {
                _treeSelectable[index] = isSelectable;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Sets the selectability of the SimpleTree.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <param name="isSelectable">Select or deselect the tree.</param>
        public void TreeSelectable(string treeName, bool isSelectable)
        {
            try
            {
                int i = _treeNameHash[treeName];
                _treeSelectable[i] = isSelectable;
            }
            catch (Exception)
            {
            }
        }

        /// <summary>
        /// Method to return if the tree is selectable.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <returns>Returns a boolean if the tree is selectable.</returns>
        public bool IsTreeSelectable(int index)
        {
            try
            {
                return _treeSelectable[index];
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Enumerator accessor for the SImple Tree
        /// </summary>
        /// <returns>Enumerator for the SimpleTree</returns>
        public SimpleTreeEnumerator GetEnumerator() 
        {
            return new SimpleTreeEnumerator(_trees);
        }

        /// <summary>
        /// Removes an item at a specific location.
        /// </summary>
        /// <param name="index">The location to remove the item</param>
        public void RemoveAt(int index)
        {
            if (index > -1)
            {
                string treeName = _trees[index].Id;

                if (!String.IsNullOrEmpty(treeName))
                {
                    Remove(treeName, index);
                }
            }
        }

        /// <summary>
        /// Removes an item with a specific name.
        /// </summary>
        /// <param name="treeName">The name of the tree to remove.</param>
        public void Remove(string treeName)
        {
            if (String.IsNullOrEmpty(treeName)) return;

            int index = _treeNameHash[treeName];
            Remove(treeName, index);
        }

        /// <summary>
        /// Removes all elements from the SimpleTree structure.
        /// </summary>
        public void Clear()
        {
            if (_trees != null)
            {
                _trees.Clear();
            }

            if (_treeNameHash != null)
            {
                _treeNameHash.Clear();
            }

            if (_treeSelectable != null)
            {
                _treeSelectable.Clear();
            }

            if (_treeMembers != null)
            {
                _treeMembers.Clear();
            }

            if (_traversedMembers != null)
            {
                _traversedMembers.Clear();
            }
        }

        /// <summary>
        /// Returns the capacity of the internal list.
        /// </summary>
        /// <returns>int containg the capacity of the internal list.</returns>
        public int Capacity()
        {
            return _trees.Capacity;
        }

        /// <summary>
        /// Returns the number of items contained in the internal list.
        /// </summary>
        /// <returns>Integer with the count of the items in the list.</returns>
        public int Count()
        {
            return _trees.Count;
        }

        #endregion Public Members


        #region SimpleTreeEnumerator
        /// <summary>
        /// Enumerator for the SimpleTree class.
        /// </summary>
        public class SimpleTreeEnumerator
        {
            private int _nIndex;
            private readonly List<SimpleDimTree> _collection;
            /// <remarks/>
            public SimpleTreeEnumerator(List<SimpleDimTree> coll)
            {
                _collection = coll;
                _nIndex = -1;
            }
            /// <remarks/>
            public bool MoveNext()
            {
                _nIndex++;
                return (_nIndex < _collection.Count);
            }
            /// <remarks/>
            public SimpleDimTree Current
            {
                get
                {
                    return (_collection[_nIndex]);
                }
            }
        }
        #endregion SimpleTreeEnumerator

    }
}