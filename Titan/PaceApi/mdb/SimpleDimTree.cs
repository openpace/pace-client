﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <summary>
    /// This is a simplified version of the Paf Dim Member Tree
    /// </summary>
    [Serializable]
    [Guid("EC4EC4ED-2B05-4682-9BEF-B2DB961D941B"),
     ProgId("Pace.SimpleDimMember"),
     ClassInterface(ClassInterfaceType.None)]
    public class SimpleDimTree : IPafSimpleDimTree
    {
        private string[] _aliasTableNames;
        /// <summary>
        /// Get/set the alias table names
        /// </summary>
        public string[] AliasTableNames
        {
            get { return _aliasTableNames; }
            set { _aliasTableNames = value; }
        }

        /// <remarks/>
        public string CompAliasTableNames{ get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CompMemberIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CompParentChild { get; set; }

        /// <summary>
        /// Gets/Set if the tree is compressed
        /// </summary>
        public bool Compressed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ElementDelim { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string GroupDelim { get; set; }

        /// <summary>
        /// Return the SimpleDimTree id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// the memberObjects
        /// </summary>
        [ComVisible(false)]
        public IPafSimpleDimMember[] MemberObjects { get; set; }

        /// <summary>
        /// Get/Set the SimpleDimTree root key
        /// </summary>
        public string RootKey { get; set; }

        private string[] _traversedMembers;

        /// <summary>
        /// Get/Set the array of members in traversal order
        /// </summary>
        public string[] TraversedMembers
        {
            get { return _traversedMembers; }
            set { _traversedMembers = value; }
        }

        /// <remarks/>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (AliasTableNames != null && AliasTableNames.Length > 0)
            {
                sb.AppendLine("SimpleDimTree AliasTableNames: ");
                foreach (string s in AliasTableNames)
                {
                    sb.AppendLine(s);
                }
                sb.AppendLine(String.Empty);
            }


            sb.AppendLine("SimpleDimTree CompMemberIndex: ");
            sb.AppendLine(CompMemberIndex);
            sb.AppendLine(String.Empty);

            sb.AppendLine("SimpleDimTree CompParentChild: ");
            sb.AppendLine(CompParentChild);
            sb.AppendLine(String.Empty);

            sb.AppendLine("SimpleDimTree Compressed: ");
            sb.AppendLine(Compressed.ToString());
            sb.AppendLine(String.Empty);

            sb.AppendLine("SimpleDimTree ElementDelim: ");
            sb.AppendLine(ElementDelim);
            sb.AppendLine(String.Empty);

            sb.AppendLine("SimpleDimTree GroupDelim: ");
            sb.AppendLine(GroupDelim);
            sb.AppendLine(String.Empty);

            sb.AppendLine("SimpleDimTree Id: ");
            sb.AppendLine(Id);
            sb.AppendLine(String.Empty);

            if (MemberObjects != null && MemberObjects.Length > 0)
            {
                sb.AppendLine("SimpleDimTree MemberObjects: ");
                foreach (SimpleDimMember s in MemberObjects)
                {
                    sb.AppendLine(s.ToString());
                }
                sb.AppendLine(String.Empty);
            }

            sb.AppendLine("SimpleDimTree RootKey: ");
            sb.AppendLine(RootKey);
            sb.AppendLine(String.Empty);


            if (TraversedMembers != null && TraversedMembers.Length > 0)
            {
                sb.AppendLine("SimpleDimTree TraversedMembers: ");
                foreach (string s in TraversedMembers)
                {
                    sb.AppendLine(s);
                }
                sb.AppendLine(String.Empty);
            }

            return sb.ToString();
        }

        /// <summary>
        /// the memberObjects
        /// </summary>
        /// <returns>the memberObjects as an array</returns>
        public IPafSimpleDimMember[] GetMemberObjects()
        {
            return MemberObjects;
        }

        /// <remarks/>
        public SimpleDimTree(string[] aliasTableNames,
                             string[] traversedMembers,
                             string compAliasTableNames,
                             string compMemberIndex,
                             string compParentChild,
                             bool compressed,
                             string elementDelim,
                             string groupDelim,
                             string id,
                             SimpleDimMember[] memberObjects,
                             string rootKey)
        {
            utility.Array.Copy(aliasTableNames, ref _aliasTableNames, 0);

            utility.Array.Copy(traversedMembers, ref _traversedMembers, 0);

            CompAliasTableNames = compAliasTableNames;

            CompMemberIndex = compMemberIndex;

            CompParentChild = compParentChild;

            Compressed = compressed;

            ElementDelim = elementDelim;

            GroupDelim = groupDelim;

            Id = id;

            if (memberObjects != null && memberObjects.Length > 0)
            {
                var ds = new List<SimpleDimMember>();
                foreach (SimpleDimMember spec in memberObjects)
                {
                    ds.Add(new SimpleDimMember(
                        spec.ChildKeys, 
                        spec.Key,
                        spec.SimpleDimMemberProps, 
                        spec.ParentKey));

                }
                MemberObjects = ds.ToArray();
            }

            RootKey = rootKey;
        }
    }
}