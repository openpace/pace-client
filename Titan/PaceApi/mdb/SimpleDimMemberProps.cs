﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <remarks/>
    [Serializable]
    [Guid("4C55CFE1-CAB9-4f92-A0A2-47677FD7D5AF"),
     ProgId("Pace.SimpleDimMember"),
     ClassInterface(ClassInterfaceType.None)]
    public class SimpleDimMemberProps : IPafSimpleDimMemberProps
    {
        private string[] _aliasKeys;

        
        /// <remarks/>
        public string[] AliasKeys
        {
            get { return _aliasKeys; }
            set { _aliasKeys = value; }
        }

        private string[] _aliasValues;

        /// <remarks/>
        public string[] AliasValues
        {
            get { return _aliasValues; }
            set { _aliasValues = value; }
        }

        /// <remarks/>
        public int ConsolidationType { get; set; }

        /// <remarks/>
        public int GenerationNumber { get; set; }

        /// <remarks/>
        [ComVisible(false)]
        public long Id { get; set; }

        /// <remarks/>
        public int Id2 { get; set; }

        /// <remarks/>
        public int LevelNumber { get; set; }

        /// <remarks/>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (AliasKeys != null && AliasKeys.Length > 0)
            {
                sb.AppendLine("SimpleDimMemberProps AliasKeys: ");
                foreach (string s in AliasKeys)
                {
                    sb.AppendLine(s);
                }
                sb.AppendLine(String.Empty);
            }

            if (AliasValues != null && AliasValues.Length > 0)
            {
                sb.AppendLine("SimpleDimMemberProps AliasValues: ");
                foreach (string s in AliasValues)
                {
                    sb.AppendLine(s);
                }
                sb.AppendLine(String.Empty);
            }

            sb.AppendLine("SimpleDimMemberProps ConsolidationType: ");
            sb.AppendLine(ConsolidationType.ToString());
            sb.AppendLine(String.Empty);

            sb.AppendLine("SimpleDimMemberProps GenerationNumber: ");
            sb.AppendLine(GenerationNumber.ToString());
            sb.AppendLine(String.Empty);


            sb.AppendLine("SimpleDimMemberProps Id: ");
            sb.AppendLine(Id.ToString());
            sb.AppendLine(String.Empty);

            sb.AppendLine("SimpleDimMemberProps LevelNumber: ");
            sb.AppendLine(LevelNumber.ToString());
            sb.AppendLine(String.Empty);


            return sb.ToString();
        }

        /// <remarks/>
        public SimpleDimMemberProps(string[] aliasKeys,
                                    string[] aliasValues,
                                    int consolidationType,
                                    int generationNumber,
                                    long id,
                                    int levelNumber)
        {
            //_aliasKeys = aliasKeys;
            utility.Array.Copy(aliasKeys, ref _aliasKeys, 0);

            //_aliasValues = aliasValues;
            utility.Array.Copy(aliasValues, ref _aliasValues, 0);

            ConsolidationType = consolidationType;

            GenerationNumber = generationNumber;

            Id = id;

            LevelNumber = levelNumber;
        }

        /// <remarks/>
        public SimpleDimMemberProps(string[] aliasKeys,
                                    string[] aliasValues,
                                    int consolidationType,
                                    int generationNumber,
                                    int id,
                                    int levelNumber)
        {
            //_aliasKeys = aliasKeys;
            utility.Array.Copy(aliasKeys, ref _aliasKeys, 0);

            //_aliasValues = aliasValues;
            utility.Array.Copy(aliasValues, ref _aliasValues, 0);

            ConsolidationType = consolidationType;
            
            GenerationNumber = generationNumber;
            
            Id = id;
            
            Id2 = id;
            
            LevelNumber = levelNumber;
        }
    }
}