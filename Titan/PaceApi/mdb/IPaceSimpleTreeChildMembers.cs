#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.InteropServices;
namespace Pace.mdb
{
    /// <summary>
    /// 
    /// </summary>
    [Guid("F0C3CFE8-1AE8-4b45-AFB2-DCC402F98BD8")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IPaceSimpleTreeChildMembers
    {
        /// <summary>
        /// The name of the child.
        /// </summary>
        [DispId(1)]
        string ChildName { get; set; }

        /// <summary>
        /// The alias name of the child.
        /// </summary>
        [DispId(2)]
        string ChildAliasName { get; set; }
    }
}
