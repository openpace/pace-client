#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <summary>
    /// 
    /// </summary>
    [Guid("CF9DED1D-1C58-4e06-A1CF-CDC147EC4C86")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IPafSimpleDimMember
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElement("childKeys", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = true)]
        [DispId(1)]
        string[] ChildKeys { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("key", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(2)]
        string Key { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("pafSimpleDimMemberProps", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(3)]
        IPafSimpleDimMemberProps SimpleDimMemberProps { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("parentKey", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(4)]
        string ParentKey { get; set; }

        /// <remarks/>
        [DispId(5)]
        string ToString();
    }
}