﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Text;
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <remarks/>
    [Serializable]
    [Guid("2C50ACD0-D5CD-4994-B619-3F6B14A8E845"),
     ProgId("Pace.SimpleDimMember"),
     ClassInterface(ClassInterfaceType.None)]
    public class SimpleDimMember : IPafSimpleDimMember
    {
        private string[] _childKeys;
        /// <remarks/>
        public string[] ChildKeys
        {
            get { return _childKeys; }
            set { _childKeys = value; }
        }

        /// <remarks/>
        public string Key { get; set; }

        /// <remarks/>
        public IPafSimpleDimMemberProps SimpleDimMemberProps { get; set; }

        /// <remarks/>
        public string ParentKey { get; set; }

        /// <remarks/>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            if (ChildKeys != null && ChildKeys.Length > 0)
            {
                sb.AppendLine("SimpleDimMember ChildKeys: ");
                foreach (string s in ChildKeys)
                {
                    sb.AppendLine(s);
                }
                sb.AppendLine(String.Empty);
            }


            sb.AppendLine("SimpleDimMember Key: ");
            sb.AppendLine(Key);
            sb.AppendLine(String.Empty);

            sb.AppendLine(SimpleDimMemberProps.ToString());

            sb.AppendLine("SimpleDimMember ParentKey: ");
            sb.AppendLine(ParentKey);
            sb.AppendLine(String.Empty);

            return sb.ToString();
        }

        /// <remarks/>
        public SimpleDimMember(string[] childKeys, 
                               string key, IPafSimpleDimMemberProps simpleDimMemberProps,
                               string parentKey)
        {
            //_childKeys = childKeys;
            utility.Array.Copy(childKeys, ref _childKeys, 0);

            Key = key;

            SimpleDimMemberProps = new SimpleDimMemberProps(
                simpleDimMemberProps.AliasKeys,
                simpleDimMemberProps.AliasValues,
                simpleDimMemberProps.ConsolidationType,
                simpleDimMemberProps.GenerationNumber,
                simpleDimMemberProps.Id2,
                simpleDimMemberProps.LevelNumber);

            ParentKey = parentKey;
        }
    }
}