#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <summary>
    /// This is a simplified version of the Paf Dim Member Tree
    /// </summary>
    [Guid("2F0BCC68-BF42-4f60-945C-94B32DCD40AA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IPafSimpleDimTree
    {
        /// <summary>
        /// Get/set the alias table names
        /// </summary>
        [System.Xml.Serialization.XmlElement("aliasTableNames", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = true)]
        [DispId(1)]
        string[] AliasTableNames { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [System.Xml.Serialization.XmlElement("compAliasTableNames", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(2)]
        string CompAliasTableNames { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [System.Xml.Serialization.XmlElement("compParentChild", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(3)]
        string CompMemberIndex { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [System.Xml.Serialization.XmlElement("compParentChild", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(4)]
        string CompParentChild { get; set; }

        /// <summary>
        /// Gets/Set if the tree is compressed
        /// </summary>
        [System.Xml.Serialization.XmlElement("compressed", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(5)]
        bool Compressed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [System.Xml.Serialization.XmlElement("elementDelim", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(6)]
        string ElementDelim { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [System.Xml.Serialization.XmlElement("groupDelim", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(7)]
        string GroupDelim { get; set; }

        /// <summary>
        /// Return the SimpleDimTree id
        /// </summary>
        [System.Xml.Serialization.XmlElement("id",Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(8)]
        string Id { get; set; }

        /// <summary>
        /// the memberObjects
        /// </summary>
        [System.Xml.Serialization.XmlElement("memberObjects", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = true)]
        [DispId(9)]
        [ComVisible(false)]
        IPafSimpleDimMember[] MemberObjects { get; set; }

        /// <summary>
        /// Get/Set the SimpleDimTree root key
        /// </summary>
        [System.Xml.Serialization.XmlElement("rootKey", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(10)]
        string RootKey { get; set; }

        /// <summary>
        /// Get/Set the array of members in traversal order
        /// </summary>
        [System.Xml.Serialization.XmlElement("traversedMembers", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = true)]
        [DispId(11)]
        string[] TraversedMembers { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [DispId(12)]
        string ToString();

        /// <summary>
        /// the memberObjects
        /// </summary>
        /// <returns>the memberObjects as an array</returns>
        [DispId(13)]
        [return: MarshalAs(UnmanagedType.AsAny)]
        IPafSimpleDimMember[] GetMemberObjects();
    }
}