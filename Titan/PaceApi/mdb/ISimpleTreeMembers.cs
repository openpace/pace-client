#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <remarks/>
    [Guid("EC3EFA3F-39BE-44d8-9B5D-D973622F62CA")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ISimpleTreeMembers
    {
        /// <summary>
        /// Returns the children of a parent within the array of PafSimpleMembers.
        /// </summary>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        [DispId(1)]
        string[] GetChildren(string parent);

        /// <summary>
        /// Returns the children of a parent within the array of PafSimpleMembers.
        /// </summary>
        /// <param name="aliasTable"></param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        [DispId(2)]
        PaceSimpleTreeChildMembers[] GetChildrenAliases(string aliasTable, string parent);

        /// <summary>
        /// Returns the PafSimpleMember object for the string member
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <returns>a PafSimpleMember object</returns>
        [DispId(3)]
        SimpleDimMember GetPafSimpleMember(string member);

        /// <summary>
        /// Gets a string array of alias values for a paf simple member.
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <returns>An array of alias values for the member.</returns>
        [DispId(4)]
        string[] GetPafSimpleMemeberAliasValues(string member);

        /// <summary>
        /// Gets the alias value for a paf simple member.
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <param name="aliasIndex">The index of the alias table.</param>
        /// <returns>The alias value if it exists, null if nothing exists.</returns>
        [DispId(5)]
        string GetPafSimpleMemeberAliasValue(string member, int aliasIndex);

        /// <summary>
        /// Gets the alias value for a paf simple member.
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <param name="aliasTable">The name of the alias table.</param>
        /// <returns>The alias value if it exists, null if nothing exists.</returns>
        [DispId(6)]
        string GetPafSimpleMemeberAliasValue(string member, string aliasTable);

        /// <summary>
        /// Gets an array of alias values for an array of paf simple member.
        /// </summary>
        /// <param name="members">The array of member names.</param>
        /// <param name="aliasTable">The name of the alias table.</param>
        /// <returns>The array of alias values.</returns>
        [DispId(6)]
        string[] GetPafSimpleMemeberAliasValue(string[] members, string aliasTable);
    }
}