#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <remarks/>
    [Guid("3B1552FC-CF6A-45b0-8D27-F41C2FBED781")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ISimpleTrees
    {
        /// <summary>
        /// Add a PafSimpleTree to the objects internal list.
        /// </summary>
        /// <param name="item">PafSimpleTree object to insert.</param>
        /// <param name="treeName">Case sensitive name of tree.  Can be used to access tree.</param>
        /// <param name="isSelectable">Can the user select from this dimension.</param> 
        [DispId(1)]
        void Add(SimpleDimTree item, string treeName, bool isSelectable);

        /// <summary>
        /// Finds the children of a parent within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        [DispId(2)]
        string[] GetChildren(string treeName, string parent);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="treeName"></param>
        /// <param name="child"></param>
        /// <returns></returns>
        [DispId(3)]
        string GetParent(string treeName, string child);

        /// <summary>
        /// Finds the children of a parent within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        [DispId(4)]
        PaceSimpleTreeChildMembers[] GetChildrenAlias(string treeName, string parent, string aliasTable);

        /// <summary>
        /// Finds the alias of a member within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member">The name of the member, for which you want the alias</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>The alias string</returns>
        [DispId(5)]
        string GetAlias(string treeName, string member, string aliasTable);

        /// <summary>
        /// Finds the a member within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member">The name of the member, for which you want the alias</param>
        /// <returns>A pafSimpleDimMember</returns>
        [DispId(6)]
        SimpleDimMember GetMember(string treeName, string member);

        /// <summary>
        /// Returns an array of aliases for an array of Paf Simple Members.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="members">The array of member names for which you want the aliases</param>
        /// <param name="aliasTable">THe name of the aliasTable.</param>
        /// <returns>The alias string</returns>
        [DispId(7)]
        string[] GetAlias(string treeName, string[] members, string aliasTable);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="treeName"></param>
        /// <returns></returns>
        [DispId(8)]
        string GetFirstLevelZeroMember(string treeName);

        /// <summary>
        /// Finds the Prev or Next member at the same level within a specific PafSimpleTree.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to search.</param>
        /// <param name="member"></param>
        /// <param name="offset"></param>
        /// <returns>A string member name</returns>
        [DispId(9)]
        string GetOffset(string treeName, string member, int offset);

        /// <summary>
        /// Gets an array of tree names.
        /// </summary>
        /// <returns>An array of dimension names</returns>
        [DispId(10)]
        string[] GetTreeNames();

        /// <summary>
        /// Returns a PafSimpleTree, with a specific name.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <returns>A PafSimpleTree</returns>
        [DispId(11)]
        SimpleDimTree GetTree(int index);

        /// <summary>
        /// Returns a PafSimpleTree, with a specific name.
        /// </summary>
        /// <param name="treeName">The name of the PafSimpleTree to return.</param>
        /// <returns>A PafSimpleTree</returns>
        [DispId(12)]
        SimpleDimTree GetTree(string treeName);

        /// <remarks/>
        [DispId(13)]
        Dictionary<string, int> GetTraversedMembers(string treeName);

        /// <summary>
        /// Method to return if the tree is selectable.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <returns>Returns a boolean if the tree is selectable.</returns>
        [DispId(14)]
        bool IsTreeSelectable(string treeName);

        /// <summary>
        /// Sets the selectability of the SimpleTree.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <param name="isSelectable">Select or deselect the tree.</param>
        [DispId(15)]
        void TreeSelectable(int index, bool isSelectable);

        /// <summary>
        /// Sets the selectability of the SimpleTree.
        /// </summary>
        /// <param name="treeName">Name of the tree.</param>
        /// <param name="isSelectable">Select or deselect the tree.</param>
        [DispId(16)]
        void TreeSelectable(string treeName, bool isSelectable);

        /// <summary>
        /// Method to return if the tree is selectable.
        /// </summary>
        /// <param name="index">The int index of the tree.</param>
        /// <returns>Returns a boolean if the tree is selectable.</returns>
        [DispId(17)]
        bool IsTreeSelectable(int index);

        /// <summary>
        /// Enumerator accessor for the SImple Tree
        /// </summary>
        /// <returns>Enumerator for the SimpleTree</returns>
        [DispId(18)]
        PaceSimpleTrees.SimpleTreeEnumerator GetEnumerator();

        /// <summary>
        /// Removes an item at a specific location.
        /// </summary>
        /// <param name="index">The location to remove the item</param>
        [DispId(19)]
        void RemoveAt(int index);

        /// <summary>
        /// Removes an item with a specific name.
        /// </summary>
        /// <param name="treeName">The name of the tree to remove.</param>
        [DispId(20)]
        void Remove(string treeName);

        /// <summary>
        /// Removes all elements from the SimpleTree structure.
        /// </summary>
        [DispId(21)]
        void Clear();

        /// <summary>
        /// Returns the capacity of the internal list.
        /// </summary>
        /// <returns>int containg the capacity of the internal list.</returns>
        [DispId(22)]
        int Capacity();

        /// <summary>
        /// Returns the number of items contained in the internal list.
        /// </summary>
        /// <returns>Integer with the count of the items in the list.</returns>
        [DispId(23)]
        int Count();
    }
}