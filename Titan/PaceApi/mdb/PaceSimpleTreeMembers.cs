#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <remarks/>
    [Serializable]
    [Guid("A58BEE8F-D647-4cfd-8865-4FCE80762552"),
     ProgId("Pace.SimpleTreeMembers"),
     ClassInterface(ClassInterfaceType.None)]
    public class PaceSimpleTreeMembers : ISimpleTreeMembers
    {
        #region Private Variables
        /// <summary>
        /// Structure to hash the PafSimpleMembers.
        /// </summary>
        private readonly Dictionary<string, SimpleDimMember> _Members;

        /// <summary>
        /// Structure to hold the Child Members of a parent.
        /// </summary>
        private readonly Dictionary<string, List<PaceSimpleTreeChildMembers>> _ChildAliases;

        #endregion Private Variables

        #region Public Members

        /// <summary>
        /// Constructor (hashes the PafSimpleMembers by the key).
        /// </summary>
        /// <param name="memberObjects">An array of PafSimpleMember</param>
        public PaceSimpleTreeMembers(IPafSimpleDimTree memberObjects)
        {
            _Members = new Dictionary<string, SimpleDimMember>();
            _ChildAliases = new Dictionary<string, List<PaceSimpleTreeChildMembers>>();

            foreach (SimpleDimMember mem in memberObjects.MemberObjects)
            {
                _Members.Add(mem.Key, mem);
                if (mem.ParentKey != null)
                {
                    AddAliasesToDictionary(mem);
                }
            }
        }

        /// <summary>
        /// Returns the children of a parent within the array of PafSimpleMembers.
        /// </summary>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        public string[] GetChildren(string parent)
        {
            if (_Members.ContainsKey(parent))
                return _Members[parent].ChildKeys;
            else
                return null;
        }

        /// <summary>
        /// Returns the children of a parent within the array of PafSimpleMembers.
        /// </summary>
        /// <param name="aliasTable"></param>
        /// <param name="parent">The name of the parent, for which you want the children.</param>
        /// <returns>A string array with the children, null if no children exist.</returns>
        public PaceSimpleTreeChildMembers[] GetChildrenAliases(string aliasTable, string parent)
        {
            if (_ChildAliases.ContainsKey(aliasTable + "-" + parent))
            {
                List<PaceSimpleTreeChildMembers> lst = _ChildAliases[aliasTable + "-" + parent];
                PaceSimpleTreeChildMembers[] str = new PaceSimpleTreeChildMembers[lst.Count];
                lst.CopyTo(str);
                return str;
            }
            else
                return null;
        }

        /// <summary>
        /// Returns the PafSimpleMember object for the string member
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <returns>a PafSimpleMember object</returns>
        public SimpleDimMember GetPafSimpleMember(string member)
        {
            if (member != null)
            {
                if (_Members.ContainsKey(member))
                {
                    return _Members[member];
                }
            }

            return null;
        }

        /// <summary>
        /// Gets a string array of alias values for a paf simple member.
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <returns>An array of alias values for the member.</returns>
        public string[] GetPafSimpleMemeberAliasValues(string member)
        {
            if (_Members.ContainsKey(member))
                return _Members[member].SimpleDimMemberProps.AliasValues;
            else
                return null;
        }

        /// <summary>
        /// Gets the alias value for a paf simple member.
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <param name="aliasIndex">The index of the alias table.</param>
        /// <returns>The alias value if it exists, null if nothing exists.</returns>
        public string GetPafSimpleMemeberAliasValue(string member, int aliasIndex)
        {
            if (_Members.ContainsKey(member))
                return _Members[member].SimpleDimMemberProps.AliasValues[aliasIndex];
            else
                return null;
        }

        /// <summary>
        /// Gets the alias value for a paf simple member.
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <param name="aliasTable">The name of the alias table.</param>
        /// <returns>The alias value if it exists, null if nothing exists.</returns>
        public string GetPafSimpleMemeberAliasValue(string member, string aliasTable)
        {
            int index = GetAliasTableIndex(member, aliasTable);
            if (index >= 0)
            { 
                if (_Members.ContainsKey(member))
                    return _Members[member].SimpleDimMemberProps.AliasValues[index];
                else
                    return null;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Gets an array of alias values for an array of paf simple member.
        /// </summary>
        /// <param name="members">The array of member names.</param>
        /// <param name="aliasTable">The name of the alias table.</param>
        /// <returns>The array of alias values.</returns>
        public string[] GetPafSimpleMemeberAliasValue(string[] members, string aliasTable)
        {
            string[] alias = new string[members.Length];
            try
            {
                for (int i = 0; i < members.Length; i++)
                {
                    int index = GetAliasTableIndex(members[i], aliasTable);
                    if (index >= 0)
                    {
                        if (_Members.ContainsKey(members[i]))
                            alias[i] = _Members[members[i]].SimpleDimMemberProps.AliasValues[index];
                        else
                            alias[i] = "";
                    }
                    else
                    {
                        alias[i] = "";
                    }

                }
                return alias;
            }
            catch
            {
                return members;
            }
        }

        #endregion Public Members

        #region Private Members

        /// <summary>
        /// Build a dictionary of child alias keys.
        /// </summary>
        /// <param name="attr">The PafSimpleMember object to hash.</param>
        private void AddAliasesToDictionary(IPafSimpleDimMember attr)
        {
            for (int i = 0; i < attr.SimpleDimMemberProps.AliasKeys.Length; i++)
            {
                if (_ChildAliases.ContainsKey(attr.SimpleDimMemberProps.AliasKeys[i] + "-" + attr.ParentKey))
                {
                    //the key exists, so just add the items to the list
                    List<PaceSimpleTreeChildMembers> lst = 
                        _ChildAliases[attr.SimpleDimMemberProps.AliasKeys[i] + "-" + attr.ParentKey];
                    lst.Add(new PaceSimpleTreeChildMembers(attr.Key, attr.SimpleDimMemberProps.AliasValues[i]));
                    _ChildAliases[attr.SimpleDimMemberProps.AliasKeys[i] + "-" + attr.ParentKey] = lst;

                }
                else
                {
                    //Add the key and the list.
                    List<PaceSimpleTreeChildMembers> lst = new List<PaceSimpleTreeChildMembers>();
                    lst.Add(new PaceSimpleTreeChildMembers(attr.Key, attr.SimpleDimMemberProps.AliasValues[i]));
                    _ChildAliases.Add(attr.SimpleDimMemberProps.AliasKeys[i] + "-" + attr.ParentKey, lst);
                }
            }
        }

        /// <summary>
        /// Gets the index of the specified member and alias table name.
        /// </summary>
        /// <param name="member">The member name.</param>
        /// <param name="name">Name of the alias table.</param>
        /// <returns>The index if the table is found, -1 if not found.</returns>
        private int GetAliasTableIndex(string member, string name)
        {
            if (_Members.ContainsKey(member))
            {
                int index = Array.IndexOf(_Members[member].SimpleDimMemberProps.AliasKeys,name);
                if(index >= 0)
                    return index;
                else
                    return -1;
            }
            else
                return -1;
        }

        #endregion Private Members
    }

    /// <summary>
    /// Class to hold information about Simple Tree child members.
    /// </summary>
    [Serializable]
    [Guid("E72D6CAB-BB2E-4c19-AE69-6DF51589B6DD"),
     ProgId("Pace.SimpleTreeChildMembers"),
     ClassInterface(ClassInterfaceType.None)]
    public class PaceSimpleTreeChildMembers : IPaceSimpleTreeChildMembers
    {
        #region Private Variables
        /// <summary>
        /// Private variable to hold the name of the child.
        /// </summary>
        private string _ChildName;

        /// <summary>
        /// Private variable to hold the alias name of the child.
        /// </summary>
        private string _ChildAliasName;

        #endregion Private Variables

        #region Public Members

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PaceSimpleTreeChildMembers()
        { 
        }

        /// <summary>
        /// Constructor that takes the properties as parameters.
        /// </summary>
        /// <param name="childName">The name of the child.</param>
        /// <param name="childAliasName">The alias name of the child.</param>
        public PaceSimpleTreeChildMembers(string childName, string childAliasName)
        {
            _ChildName = childName;
            _ChildAliasName = childAliasName;
        }

        #endregion Public Members

        #region Public Properties

        /// <summary>
        /// The name of the child.
        /// </summary>
        public string ChildName
        {
            get { return _ChildName; }
            set { _ChildName = value; }
        }

        /// <summary>
        /// The alias name of the child.
        /// </summary>
        public string ChildAliasName
        {
            get { return _ChildAliasName; }
            set { _ChildAliasName = value; }
        }

        #endregion Public Properties
    }
}