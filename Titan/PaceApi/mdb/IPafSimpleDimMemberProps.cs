#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.InteropServices;

namespace Pace.mdb
{
    /// <remarks/>
    [Guid("2D22DECA-212A-4706-8409-39D44D5E64F4")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IPafSimpleDimMemberProps
    {
        /// <remarks/>
        [System.Xml.Serialization.XmlElement("aliasKeys", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = true)]
        [DispId(1)]
        string[] AliasKeys { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("aliasValues", Form = System.Xml.Schema.XmlSchemaForm.Unqualified, IsNullable = true)]
        [DispId(2)]
        string[] AliasValues { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("consolidationType", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(3)]
        int ConsolidationType { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("generationNumber", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(4)]
        int GenerationNumber { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("id", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [ComVisible(false)]
        long Id { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("id2", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(5)]
        int Id2 { get; set; }

        /// <remarks/>
        [System.Xml.Serialization.XmlElement("levelNumber", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        [DispId(6)]
        int LevelNumber { get; set; }

        /// <remarks/>
        [DispId(7)]
        string ToString();
    }
}