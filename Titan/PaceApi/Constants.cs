﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace Pace
{
    internal class Constants
    {
        /// <summary>
        /// Alias mapping format member for the defaul alias table.
        /// </summary>
        public const string ALIAS_MAPPING_FORMAT_ALIAS = "alias";

        /// <summary>
        /// Alias mapping format member, this is a server constant.
        /// </summary>
        public const string ALIAS_MAPPING_FORMAT_MEMBER = "member";
    }
}
