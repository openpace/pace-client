﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Net;

namespace Pace.Extensions
{
    /// <summary>
    /// Extension methods for the URI class.
    /// </summary>
    public static class UriExtension
    {
        /// <summary>
        /// Gets the active status of a URI
        /// </summary>
        /// <param name="x">URI to extend.</param>
        /// <param name="timeout">Timeout</param>
        /// <returns>Returns true if the URI is active, false if not.</returns>
        public static bool IsActive2(this Uri x, int timeout)
        {
            bool urlIsLive = false;

            //if url is not null
            if (x != null)
            {

                //create a webrequest
                WebRequest webRequest = WebRequest.Create(x);

                //set the time
                webRequest.Timeout = timeout;

                //Try to connect to url.  If url isn't live, a WebException will be thrown.
                try
                {
                    //try to get response
                    //webRequest.GetResponse();
                    using (WebResponse respon = webRequest.GetResponse())
                    {
                        urlIsLive = true;
                    }

                    //if we get this far, we can assume connected to url successfully
                    //urlIsLive = true;

                }
                catch (WebException)
                {
                    //do nothing, already false
                }
            }

            //return result
            return urlIsLive;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
        public static bool IsActive3(this Uri x, int timeout)
        {
            try
            {
                //Creating the HttpWebRequest
                HttpWebRequest request = WebRequest.Create(x) as HttpWebRequest;
                //Setting the Request method HEAD, you can also use GET too.
                request.Method = "HEAD";
                request.Timeout = timeout;
                //Getting the Web Response.
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                //Returns TURE if the Status code == 200
                return (response.StatusCode == HttpStatusCode.OK);
            }
            catch
            {
                //Any exception will returns false.
                return false;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="x"></param>
        /// <param name="timeout"></param>
        /// <returns></returns>
         public static bool IsActive(this Uri x, int timeout)
         {
             using (var client = new TitanWebClient())
             {
                 client.HeadOnly = true;
                 client.Timeout = timeout;
                 try
                 {
                    client.DownloadString(x);
                    return true;
                 }
                 catch (Exception)
                 {
                     return false;
                 }
             }
         }
    }
    class TitanWebClient : WebClient
    {
        public bool HeadOnly { get; set; }
        public int Timeout { get; set; }
        protected override WebRequest GetWebRequest(Uri address)
        {
            WebRequest req = base.GetWebRequest(address);
            req.Timeout = Timeout;
            if (HeadOnly && req.Method == "GET")
            {
                req.Method = "HEAD";
            }
            return req;
        }
    }
}
