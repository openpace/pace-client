﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Globalization;
using System.Text;
using System.Xml.Serialization;
using Pace.Extensions;

namespace Pace.connection
{
    /// <summary>
    /// Class to encapsulate the aliasing of Pace server URL's.
    /// </summary>
    [Serializable]
    public class PaceServerUrlAlias
    {
        private const string UrlAddOn = "?wsdl";

        /// <summary>
        /// Timeout to query the server.
        /// </summary>
        [XmlIgnore()]
        public int UriTimeout { get; set; }

        /// <summary>
        /// URL to the Pace Server.
        /// </summary>
        [XmlIgnore()]
        public Uri PaceServer { get; set; }

        ///<summary>
        ///  Unfortunately this has to be public to be xml serialized.
        ///</summary>
        [XmlElement("PaceServerUrl")]
        public string PaceServerUrlString
        {
            get { return PaceServer.ToString(); }
            set { PaceServer = new Uri(value); }
        }

        /// <summary>
        /// Gets the pace server url string with the "?wsdl" ending
        /// </summary>
        [XmlIgnore()]
        private Uri PaceServerWsdlUrlString
        {
            get { return new Uri(PaceServer + UrlAddOn) ; }
        }

        /// <summary>
        /// Alias to the Pace Server
        /// </summary>
        [XmlAttribute]
        public string PaceServerAlias { get; set; }

        /// <summary>
        /// Is the Pace Server URL active/online.
        /// </summary>
        [XmlIgnore]
        public bool IsActive
        {
            get { return PaceServerWsdlUrlString.IsActive(UriTimeout); }
        }

        /// <summary>
        /// Gets the concatentation of the Alias | URL
        /// </summary>
        public string UrlAliasString { 
            get
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(PaceServerAlias);
                sb.Append(" | ");
                sb.Append(PaceServer.AbsoluteUri);
                return sb.ToString();
            }
        }

        /// <summary>
        /// To alias string.
        /// </summary>
        /// <returns></returns>
        public string ToAliasString()
        {
            return UrlAliasString;
        }

        /// <summary>
        /// Server alias URL ToString
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return PaceServerUrlString;
        }

        /// <summary>
        /// Default constructor.
        /// </summary>
        public PaceServerUrlAlias()
        {
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paceServer">string url of the pace server</param>
        /// <param name="paceServerAlias"></param>
        /// <param name="timeout">Timeout to query the server.</param>
        public PaceServerUrlAlias(string paceServer, string paceServerAlias, int timeout)
        {
            PaceServer = new Uri(paceServer);
            PaceServerAlias = paceServerAlias;
            UriTimeout = timeout;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paceServer">string url of the pace server</param>
        /// <param name="paceServerAlias"></param>
        public PaceServerUrlAlias(string paceServer, string paceServerAlias)
        {
            PaceServer = new Uri(paceServer);
            PaceServerAlias = paceServerAlias;
            UriTimeout = 300;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paceServer">string url of the pace server</param>
        /// <param name="paceServerAlias">String representation of a URI</param>
        public PaceServerUrlAlias(Uri paceServer, string paceServerAlias)
        {
            PaceServer = paceServer;
            PaceServerAlias = paceServerAlias;
            UriTimeout = 300;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="paceServer">string url of the pace server</param>
        /// <param name="paceServerAlias">String representation of a URI</param>
        /// <param name="timeout">Timeout to query the server.</param>
        public PaceServerUrlAlias(Uri paceServer, string paceServerAlias, int timeout)
        {
            PaceServer = paceServer;
            PaceServerAlias = paceServerAlias;
            UriTimeout = timeout;
        }
    }
}
