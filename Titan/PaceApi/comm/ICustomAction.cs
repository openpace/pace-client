#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.InteropServices;
using Pace.mdb;

namespace Pace.comm
{
    /// <summary>
    /// 
    /// </summary>
    [Guid("568680FF-FBF8-4b84-A8D6-F430197870F9")]
    [InterfaceType(ComInterfaceType.InterfaceIsDual)]
    public interface ICustomAction
    {
        #region Properties
        /// <summary>
        /// Gets the unique menu key as defined in the custom menu xml.
        /// </summary>
        [DispId(1)]
        string MenuKey { get; }

        /// <summary>
        /// Action name paramaters - token keys
        /// </summary>
        [DispId(2)]
        string[] TokenKeys { get; set; }

        /// <summary>
        /// Action name parameters - token values
        /// </summary>
        [DispId(3)]
        string[] TokenValues { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DispId(4)]
        PaceSimpleTrees SimpleTrees { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DispId(5)]
        string[] RowColumnFormatDims { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DispId(6)]
        string[] RowColumnFormat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DispId(7)]
        string[] PrimaryRowColumnFormatDims { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DispId(8)]
        string[] PrimaryRowColumnFormat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DispId(9)]
        string[] AliasTableNamesDims { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DispId(10)]
        string[] AliasTableNames { get; set; }

        #endregion Properties


        #region Methods
        /// <summary>
        /// Fire the event.
        /// </summary>
        [DispId(11)]
        void Fire();


        /// <summary>
        /// 
        /// </summary>
        [DispId(12)]
        SimpleDimTree[] DimTrees { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimName"></param>
        /// <returns></returns>
        [DispId(13)]
        SimpleDimTree GetDimensionTree(string dimName);

        /// <summary>
        /// Sets the token keys.
        /// </summary>
        /// <param name="keys"></param>
        [DispId(14)]
        void SetTokenKeys(ref string[] keys);

        /// <summary>
        /// Sets the token values.
        /// </summary>
        /// <param name="values"></param>
        [DispId(15)]
        void SetTokenValues(ref string[] values);


        /// <summary>
        /// Sets the menu key.
        /// </summary>
        /// <param name="key"></param>
        [ComVisible(false)]
        void SetMenuKey(string key);

        /// <summary>
        /// Sets the dimension trees.
        /// </summary>
        /// <param name="dimensionTrees">Dimension trees.</param>
        [ComVisible(false)]
        void SetDimensionTrees(SimpleDimTree[] dimensionTrees);

        /// <summary>
        /// Sets the PaceSimpleTrees
        /// </summary>
        /// <param name="simpleTrees">PaceSimpleTrees</param>
        [DispId(16)]
        void SetSimpleTrees(PaceSimpleTrees simpleTrees);

        /// <summary>
        /// Gets the alias table name for a particular dimension.
        /// </summary>
        /// <param name="dimName">name of the dimension.</param>
        /// <returns></returns>
        [DispId(17)]
        string GetAliasTableName(string dimName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimName">name of the dimension.</param>
        /// <returns></returns>
        [ComVisible(false)]
        string GetRowColumnFormat(string dimName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimName">name of the dimension.</param>
        /// <returns></returns>
        [ComVisible(false)]
        string GetPrimaryRowColumnFormat(string dimName);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimName"></param>
        /// <param name="aliasTable"></param>
        /// <param name="mbrName"></param>
        /// <param name="primaryValue"></param>
        /// <param name="additionalValue"></param>
        [DispId(18)]
        void GetAliasesValues(string dimName,
            string aliasTable,
            string mbrName,
            out string primaryValue,
            out string additionalValue);

        #endregion Methods

    }
}