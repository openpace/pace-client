#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Runtime.InteropServices;
using Pace.mdb;

namespace Pace.comm
{
    /// <summary>
    /// 
    /// </summary>
    [Guid("D798A2FC-01A6-4c97-AC0F-703C3B2DBC81"),
     ProgId("Pace.CustomAction"),
     ComSourceInterfaces(typeof(ICustomActionEvents)), // Events interface implementation
     ClassInterface(ClassInterfaceType.None)]
    [Serializable]
    public class CustomAction : ICustomAction
    {
        private string _menuKey;

        private SimpleDimTree[] _dimTrees;

        private string[] _tokenValues;

        private string[] _tokenKeys;

        /// <summary>
        /// 
        /// </summary>
        public PaceSimpleTrees SimpleTrees { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] RowColumnFormatDims { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] RowColumnFormat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] PrimaryRowColumnFormatDims { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] PrimaryRowColumnFormat { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] AliasTableNamesDims { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string[] AliasTableNames { get; set; }


        /// <summary>
        /// Occurs when the custom action button is clicked in the Excel GUI.
        /// </summary>
        [ComVisible(false)]
        public delegate void CustomAction_CustomActionMenuButtonClicked(object sender);

        /// <summary>
        /// Occurs when the custom action button is clicked in the Excel GUI.
        /// </summary>
        public event CustomAction_CustomActionMenuButtonClicked CustomActionMenuButtonClicked;

        /// <summary>
        /// Gets the unique menu key as defined in the custom menu xml.
        /// </summary>
        public string MenuKey
        {
            get { return _menuKey; }
            set { _menuKey = value; }
        }

        /// <summary>
        /// Action name paramaters - token keys
        /// </summary>
        public string[] TokenKeys
        {
            get { return _tokenKeys; }
            set { _tokenKeys = value; }
        }

        /// <summary>
        /// Action name parameters - token values
        /// </summary>
        public string[] TokenValues
        {
            get { return _tokenValues; }
            set { _tokenValues = value; }
        }

        /// <summary>
        /// 
        /// </summary>
        public SimpleDimTree[] DimTrees
        {
            get { return _dimTrees; }
            set { _dimTrees = value; }
        }

        /// <summary>
        /// Fire the event.
        /// </summary>
        public void Fire()
        {
            if (CustomActionMenuButtonClicked != null)
            {
                CustomActionMenuButtonClicked(this);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dimName"></param>
        /// <returns></returns>
        public SimpleDimTree GetDimensionTree(string dimName)
        {
            foreach(SimpleDimTree tree in _dimTrees)
            {
                if(tree.Id.ToLower().Equals(dimName.ToLower()))
                {
                    return tree;
                }
            }
            return null;
        }

        /// <summary>
        /// Sets the simple tree object.
        /// </summary>
        /// <param name="simpleTrees"></param>
        public void SetSimpleTrees(PaceSimpleTrees simpleTrees)
        {
            if(simpleTrees != null && simpleTrees.Capacity() > 0)
            {
                SimpleTrees = simpleTrees;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keys"></param>
        public void SetTokenKeys(ref string[] keys)
        {
            if (keys != null && keys.Length > 0)
            {
                TokenKeys = keys;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="values"></param>
        public void SetTokenValues(ref string[] values)
        {
            if (values != null && values.Length > 0)
            {
                TokenValues = values;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        [ComVisible(false)]
        public void SetMenuKey(string key)
        {
            if (!String.IsNullOrEmpty(key))
            {
                MenuKey = key;
            }
        }

        /// <summary>
        /// Sets the dimension trees.
        /// </summary>
        /// <param name="dimensionTrees">Dimension trees.</param>
        [ComVisible(false)]
        public void SetDimensionTrees(SimpleDimTree[] dimensionTrees)
        {
            if (dimensionTrees != null && dimensionTrees.Length > 0)
            {
                DimTrees = dimensionTrees;
            }
        }

         /// <summary>
        /// 
        /// </summary>
        public string GetAliasTableName(string dimName)
        {
            string ret = String.Empty;
            int i = 0;
            foreach(string s in AliasTableNamesDims)
            {
                if (s.ToLower().Equals(dimName.ToLower()))
                {
                    if (AliasTableNames != null && AliasTableNames.Length >= i)
                    {

                        return AliasTableNames[i];
                    }
                }
                i++;
            }
            return ret;
        }

        /// <summary>
        /// Gets the row column format for the specified dimension.
        /// </summary>
        /// <param name="dimName"></param>
        /// <returns></returns>
        public string GetRowColumnFormat(string dimName)
        {
            string ret = String.Empty;
            int i = 0;
            foreach (string s in RowColumnFormatDims)
            {
                if (s.ToLower().Equals(dimName.ToLower()))
                {
                    if (RowColumnFormat != null && RowColumnFormat.Length >= i)
                    {

                        return RowColumnFormat[i];
                    }
                }
                i++;
            }
            return ret;
        }

        /// <summary>
        /// Gets the primary format for a specified dimension.
        /// </summary>
        /// <param name="dimName"></param>
        /// <returns></returns>
        public string GetPrimaryRowColumnFormat(string dimName)
        {
            string ret = String.Empty;
            int i = 0;
            foreach (string s in PrimaryRowColumnFormatDims)
            {
                if (s.ToLower().Equals(dimName.ToLower()))
                {
                    if (PrimaryRowColumnFormat != null && PrimaryRowColumnFormat.Length >= i)
                    {

                        return PrimaryRowColumnFormat[i];
                    }
                }
                i++;
            }
            return ret;
        }

        /// <summary>
        /// Gets the aliases values.
        /// </summary>
        /// <param name="dimName">Dimension name.</param>
        /// <param name="aliasTable">Alias table.</param>
        /// <param name="mbrName">Name of the memeber.</param>
        /// <param name="primaryValue">Variable to hold the primary value.</param>
        /// <param name="additionalValue">Variable to hold the additional value.</param>
        public void GetAliasesValues(string dimName, 
            string aliasTable, 
            string mbrName, 
            out string primaryValue, 
            out string additionalValue)
        {

            primaryValue = String.Empty;
            additionalValue = String.Empty;

            string primRowFormat = GetPrimaryRowColumnFormat(dimName);
            string addRowFormat = GetRowColumnFormat(dimName);

            switch (primRowFormat.ToLower())
            {
                case Constants.ALIAS_MAPPING_FORMAT_ALIAS:
                    primaryValue = SimpleTrees.GetAlias(dimName, mbrName, aliasTable);
                    break;
                case Constants.ALIAS_MAPPING_FORMAT_MEMBER:
                    //TTN-1185
                    SimpleDimMember psdm = SimpleTrees.GetMember(dimName, mbrName);
                    if (psdm != null)
                    {
                        primaryValue = psdm.Key;
                    }
                    else
                    {
                        primaryValue = null;
                    }
                    break;
            }

            switch (addRowFormat.ToLower())
            {
                case Constants.ALIAS_MAPPING_FORMAT_ALIAS:
                    additionalValue = SimpleTrees.GetAlias(dimName, mbrName, aliasTable);
                    break;
                case Constants.ALIAS_MAPPING_FORMAT_MEMBER:
                    //TTN-1185
                    SimpleDimMember psdm = SimpleTrees.GetMember(dimName, mbrName);
                    if (psdm != null)
                    {
                        additionalValue = psdm.Key;
                    }
                    else
                    {
                        additionalValue = null;
                    }
                    break;
            }
        }
    }
}