#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.InteropServices;

namespace Pace.comm
{
    /// <summary>
    /// 
    /// </summary>
    [Guid("866226CC-679E-4ce7-B527-5A80D6950278")]
    [InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface ICustomActionEvents
    {
        /// <summary>
        /// Occurs when the custom action button is clicked in the Excel GUI.
        /// </summary>
        /// <param name="sender"></param>
        [DispId(1)]
        void CustomActionMenuButtonClicked(object sender);
    }
}