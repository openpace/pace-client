﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
namespace Pace.utility
{
    /// <remarks/>
    public static class Array
    {
        /// <summary>
        /// Copies the source array to the destination array.  Initalizes the destination array to the proper size if it has not been.
        /// </summary>
        /// <param name="source">The one-dimensional Array that is the source of the elements copied from the current Array.</param>
        /// <param name="destination">The one-dimensional Array that is the destination of the elements copied from the current Array.</param>
        /// <param name="index">A 32-bit integer that represents the index in array at which copying begins.</param>
        /// <returns></returns>
        internal static void Copy(string[] source, ref string[] destination, int index)
        {
            if(source == null || index == -1)
            {
                return;
            }

            if (destination == null || source.GetLength(0) != destination.GetLength(0))
            {
                destination = new string[source.GetLength(0)];
            }

            source.CopyTo(destination, index);
        }

        /// <summary>
        /// Copies the source array to the destination array.  Initalizes the destination array to the proper size if it has not been.
        /// </summary>
        /// <param name="source">The one-dimensional Array that is the source of the elements copied from the current Array.</param>
        /// <param name="index">A 32-bit integer that represents the index in array at which copying begins.</param>
        /// <returns>he one-dimensional Array that is the destination of the elements copied from the current Array.</returns>
        internal static string[] Copy(string[] source, int index)
        {
            if (source == null || index == -1)
            {
                return null;
            }

            string[] destination = new string[source.GetLength(0)];

            source.CopyTo(destination, index);

            return destination;
        }
    }
}