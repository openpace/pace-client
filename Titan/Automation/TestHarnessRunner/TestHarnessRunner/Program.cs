﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace TestHarnessRunner
{
    class Program
    {
        private static int _returnCode = 0;
        
        static int Main(string[] args)
        {
            try
            {

                Console.WriteLine("Starting Open Pace TestHarnessRunner....");

                Console.WriteLine("Excel Program Path: " + args[0]);
                Console.WriteLine("Excel File: " + args[1]);

                var proc = new Process();
                proc.StartInfo.FileName = @"""" + args[0] + @"""";
                proc.StartInfo.Arguments = @"""" + args[1] + @"""";

                // set up output redirection

                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.EnableRaisingEvents = true;
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.CreateNoWindow = true;

                // see below for output handler
                proc.ErrorDataReceived += ProcErrorDataReceived;
                proc.OutputDataReceived += ProcOutputDataReceived;

                proc.Start();

                proc.BeginErrorReadLine();
                proc.BeginOutputReadLine();

                proc.WaitForExit();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.GetBaseException());
            }
            Environment.ExitCode = _returnCode;
            return _returnCode;
        }

        static void ProcErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Error.WriteLine(e.Data);
            _returnCode = -1;
        }

        static void ProcOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

    }

}
