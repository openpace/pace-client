﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using Microsoft.VisualStudio.CodeCoverage;

namespace CoverageConvert
{
    public static class Utility
    {

        public static void WriteToHtml(CoverageDS myCovDS, string path, int minRequiredClassCoveragePercent, int minRequiredMethodCoveragePercent)
        {


            try
            {

                myCovDS.ToHtml(
                    path,
                    "index.html",
                    minRequiredClassCoveragePercent,
                    minRequiredMethodCoveragePercent);

            }
            catch(Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }

        public static bool IsValidPolicy(int ActualPercent, int ExpectedPercent)
        {
            return (ActualPercent >= ExpectedPercent);
        }

        public static int GetPercentCoverage(uint dblCovered, uint dblNot)
        {
            uint dblTotal = dblCovered + dblNot;
            return Convert.ToInt32(100.0 * (double)dblCovered / (double)dblTotal);
        }

        public static int GetPercentCoverage(uint dblCovered, uint dblNot, uint dblPartial)
        {
            uint dblTotal = dblCovered + dblNot + dblPartial;
            return Convert.ToInt32(100.0 * (double)dblCovered / (double)dblTotal);
        }

        public static bool ShouldDisplay(bool blnDisplayOnlyErrors, bool isValid)
        {
            if (isValid)
            {
                //Is valid --> need to decide
                if (blnDisplayOnlyErrors)
                    return false;
                else
                    return true;
            }
            else
            {
                //Not valid --> always display
                return true;
            }
        }
    }
}
