﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Data;
using System.IO;
using Microsoft.VisualStudio.CodeCoverage;

namespace CoverageConvert
{
    public static class NamespaceTableRowExtension
    {

        public static CoverageDSPriv.ClassRow[] GetClassRows2(this CoverageDSPriv.NamespaceTableRow namespaceTableRow)
        {
            DataRelation relation = namespaceTableRow.Table.ChildRelations["Namespace_Class"];

            CoverageDSPriv.ClassRow[] childRows = (CoverageDSPriv.ClassRow[])namespaceTableRow.GetChildRows(relation);
            Array.Sort<CoverageDSPriv.ClassRow>(childRows, new ClassRowComparer());
            return childRows;
        }

        /// <summary>
        /// Saves a dataset to an html file.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="pathFileName">Full path file name to save the file.</param>
        public static string ToHtml(this CoverageDSPriv.NamespaceTableRow n, string path, int minRequiredClassCoveragePercent, int minRequiredMethodCoveragePercent)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append("<TR ALIGN='CENTER'>");

            string fileName = n.NamespaceName.CleanFileName() + ".html";

            sb.Append("<TD>");
            sb.Append("<a href=\"" + fileName + "\"><fontstyle=\"font-size:medium;\">" + n.NamespaceName + "</font></a><br>");
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", n.BlocksCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", n.BlocksNotCovered));
            sb.Append("</TD>");


            sb.Append("<TD>");
            sb.Append(string.Format("{0:P2}", n["PercentBlock"]));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:P2}", n["PercentBlockNot"]));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", n.LinesCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", n.LinesNotCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", n.LinesPartiallyCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:P2}", n["Percent"]));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:P2}", n["PercentNot"]));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:P2}", n["PercentPart"]));
            sb.Append("</TD>");

            sb.Append("</TR>");



            System.Text.StringBuilder sb2 = new System.Text.StringBuilder();
            sb2.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb2.Append("<head>");
            sb2.Append("<title>");
            sb2.Append("Page-");
            sb2.Append(Guid.NewGuid().ToString());
            sb2.Append("</title>");
            sb2.Append("</head>");
            sb2.Append("<body>");
            sb2.AppendFormat(@"<H2>");
            sb2.AppendFormat("Coverage Summary for: " + n.NamespaceName);
            sb2.AppendFormat(@"</H2>");

            sb2.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb2.Append("style='border: solid 1px Silver; font-size: medium;'>");
            sb2.Append("<TR ALIGN='CENTER'>");
            sb2.Append("<TH style=\"width:15%;\">Class Name</TH>");
            sb2.Append("<TH style=\"width:8;\">Blocks Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Blocks Not Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Covered (% Blk)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Not Covered (% Blk)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Lines Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Lines Not Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Lines Part Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Covered (% Ln)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Not Covered (% Ln)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Part Covered (% Ln)</TH>");
            sb2.Append("<TH style=\"width:5%;\">Coverage</TH>");

            sb2.Append("</TR>");


            foreach (CoverageDSPriv.ClassRow c in n.GetClassRows2())
            {
                sb2.Append(c.ToHtml(path, minRequiredClassCoveragePercent, minRequiredMethodCoveragePercent));
            }


            sb2.Append("</TABLE>");
            sb2.Append("&nbsp;");
            sb2.Append("&nbsp;");


            sb2.Append("<p style=\"font-size: x-small; font-weight: normal\">" + Constants.FOOTER + " : " + DateTime.Now.ToLongDateString() + ", " + DateTime.Now.ToLongTimeString() + "</p>");

            sb2.Append("</body>");
            sb2.Append("</html>");

            string pathFileName = Path.Combine(path, fileName);

            sb2.ToFile(pathFileName);


            return sb.ToString();

        }
    }
}
