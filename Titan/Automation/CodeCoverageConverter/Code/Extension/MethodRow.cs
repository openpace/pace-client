﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using Microsoft.VisualStudio.CodeCoverage;

namespace CoverageConvert
{
    public static class MethodRowExtension
    {
        public static string ToHtml(this CoverageDSPriv.MethodRow m, CoverageDSPriv.ClassRow c, int minRequiredMethodCoveragePercent)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool isValid = true;
            bool blnDisplayOnlyErrors = false;
            string strError = null;
            const string cErrorMsg = "[TOO LOW]";

            string strMethodName = m.MethodFullName;

            if (Utility.ShouldDisplay(blnDisplayOnlyErrors, isValid))
            {
                sb.Append("<TR ALIGN='CENTER'>");

                sb.Append("<TD>");
                sb.Append(strMethodName);
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", m.BlocksCovered));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", m.BlocksNotCovered));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", m["PercentBlock"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", m["PercentBlockNot"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", m.LinesCovered));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", m.LinesNotCovered));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", m.LinesPartiallyCovered));
                sb.Append("</TD>");

                double p = Convert.ToDouble(m["Percent"]);
                isValid = Utility.IsValidPolicy(Convert.ToInt32(p), minRequiredMethodCoveragePercent);
                if (!isValid)
                {
                    strError = cErrorMsg;
                }

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", p));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", m["PercentNot"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", m["PercentPart"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(strError);
                sb.Append("</TD>");

                sb.Append("</TR>");

            }

            return sb.ToString();
        }
    }
}
