﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Data;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.CodeCoverage;

namespace CoverageConvert
{
    public static class ClassRowExtension
    {

        public static CoverageDSPriv.MethodRow[] GetMethodRows2(this CoverageDSPriv.ClassRow classRow)
        {
            DataRelation relation = classRow.Table.ChildRelations["Class_Method"];

            CoverageDSPriv.MethodRow[] childRows = (CoverageDSPriv.MethodRow[])classRow.GetChildRows(relation);
            Array.Sort<CoverageDSPriv.MethodRow>(childRows, new MethodRowComparer());

            return childRows;
        }

        public static string ToHtml(this CoverageDSPriv.ClassRow c, string path, int minRequiredClassCoveragePercent, int minRequiredMethodCoveragePercent)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            bool isValid = true;
            bool blnDisplayOnlyErrors = false;
            string strError = null;
            const string cErrorMsg = "[TOO LOW]";


            string fileName = c.ClassName.CleanFileName() + "-methods.html";
            string pathFileName = Path.Combine(path, fileName);

            if (Utility.ShouldDisplay(blnDisplayOnlyErrors, isValid))
            {
                sb.Append("<TR ALIGN='CENTER'>");


                sb.Append("<TD>");
                //sb.Append(c.ClassName);
                sb.Append("<a href=\"" + fileName + "\"><fontstyle=\"font-size:medium;\">" + c.ClassName + "</font></a><br>");
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", c.BlocksCovered));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", c.BlocksNotCovered));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", c["PercentBlock"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", c["PercentBlockNot"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", c.LinesCovered));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", c.LinesNotCovered));
                sb.Append("</TD>");


                sb.Append("<TD>");
                sb.Append(string.Format("{0:N0}", c.LinesPartiallyCovered));
                sb.Append("</TD>");

                double p = Convert.ToDouble(c["Percent"]);
                isValid = Utility.IsValidPolicy(Convert.ToInt32(p), minRequiredClassCoveragePercent);
                if (!isValid)
                {
                    strError = cErrorMsg;
                }

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", p));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", c["PercentNot"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(string.Format("{0:P2}", c["PercentPart"]));
                sb.Append("</TD>");

                sb.Append("<TD>");
                sb.Append(strError);
                sb.Append("</TD>");

                sb.Append("</TR>");
            }
            
            StringBuilder sb2 = new StringBuilder();
            sb2.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb2.Append("<head>");
            sb2.Append("<title>");
            sb2.Append("Page-");
            sb2.Append(Guid.NewGuid().ToString());
            sb2.Append("</title>");
            sb2.Append("</head>");
            sb2.Append("<body>");
            sb2.AppendFormat(@"<H2>");
            sb2.AppendFormat(c.ClassName + " - Methods");
            sb2.AppendFormat(@"  </H2>");

            sb2.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb2.Append("style='border: solid 1px Silver; font-size: medium;'>");
            sb2.Append("<TH style=\"width:15%;\">Method Name</TH>");
            sb2.Append("<TH style=\"width:8;\">Blocks Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Blocks Not Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Covered (% Blk)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Not Covered (% Blk)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Lines Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Lines Not Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Lines Part Covered</TH>");
            sb2.Append("<TH style=\"width:8%;\">Covered (% Ln)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Not Covered (% Ln)</TH>");
            sb2.Append("<TH style=\"width:8%;\">Part Covered (% Ln)</TH>");
            sb2.Append("<TH style=\"width:5%;\">Coverage</TH>");

            sb2.Append("</TR>");

            foreach (CoverageDSPriv.MethodRow m in c.GetMethodRows2())
            {
                sb2.Append(m.ToHtml(c, minRequiredMethodCoveragePercent));
            }

            sb2.Append("</table>");

            sb2.Append("&nbsp;");
            sb2.Append("&nbsp;");
            sb2.Append("<p style=\"font-size: x-small; font-weight: normal\">" + Constants.FOOTER + " : " + DateTime.Now.ToLongDateString() + ", " + DateTime.Now.ToLongTimeString() + "</p>");

            sb2.Append("</body>");
            sb2.Append("</html>");

            sb2.ToFile(pathFileName);

            return sb.ToString();
        }
    }
}
