﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Data;
using System.IO;
using System.Text;
using Microsoft.VisualStudio.CodeCoverage;

namespace CoverageConvert
{
    public static class CoverageDataSetExtension
    {
        /// <summary>
        /// Saves a dataset to an html file.
        /// </summary>
        public static void ToHtml(this CoverageDS ds, string path, string file, 
            int minRequiredClassCoveragePercent, int minRequiredMethodCoveragePercent)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.Append("<head>");
            sb.Append("<title>");
            sb.Append("Page-");
            sb.Append(Guid.NewGuid().ToString());
            sb.Append("</title>");
            sb.Append("</head>");
            sb.Append("<body>");


            sb.AppendFormat(@"<H2>");
            sb.AppendFormat("Coverage Summary");
            sb.AppendFormat(@" </H2>");
            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");
            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TH style=\"width:9%;\">Namespace</TH>");
            sb.Append("<TH style=\"width:9%;\">Blocks Covered</TH>");
            sb.Append("<TH style=\"width:9%;\">Blocks Not Covered</TH>");
            sb.Append("<TH style=\"width:9%;\">Covered (% Blk)</TH>");
            sb.Append("<TH style=\"width:9%;\">Not Covered (% Blk)</TH>");
            sb.Append("<TH style=\"width:9%;\">Lines Covered</TH>");
            sb.Append("<TH style=\"width:9%;\">Lines Not Covered</TH>");
            sb.Append("<TH style=\"width:9%;\">Lines Part Covered</TH>");
            sb.Append("<TH style=\"width:9%;\">Covered (% Ln)</TH>");
            sb.Append("<TH style=\"width:9%;\">Not Covered (% Ln)</TH>");
            sb.Append("<TH style=\"width:9%;\">Part Covered (% Ln)</TH>");
            sb.Append("</TR>");


            ds.NamespaceTable.AddPercentColumn("Percent", ds.NamespaceTable.LinesCoveredColumn.ColumnName, ds.NamespaceTable.LinesNotCoveredColumn.ColumnName, ds.NamespaceTable.LinesPartiallyCoveredColumn.ColumnName);

            ds.Class.AddPercentColumn("Percent", ds.Class.LinesCoveredColumn.ColumnName, ds.Class.LinesNotCoveredColumn.ColumnName, ds.Class.LinesPartiallyCoveredColumn.ColumnName);

            ds.Method.AddPercentColumn("Percent", ds.Method.LinesCoveredColumn.ColumnName, ds.Method.LinesNotCoveredColumn.ColumnName, ds.Method.LinesPartiallyCoveredColumn.ColumnName);


            ds.NamespaceTable.AddPercentColumn("PercentNot", ds.NamespaceTable.LinesNotCoveredColumn.ColumnName, ds.NamespaceTable.LinesCoveredColumn.ColumnName, ds.NamespaceTable.LinesPartiallyCoveredColumn.ColumnName);

            ds.Class.AddPercentColumn("PercentNot", ds.Class.LinesNotCoveredColumn.ColumnName, ds.Class.LinesCoveredColumn.ColumnName, ds.Class.LinesPartiallyCoveredColumn.ColumnName);

            ds.Method.AddPercentColumn("PercentNot", ds.Method.LinesNotCoveredColumn.ColumnName, ds.Method.LinesCoveredColumn.ColumnName, ds.Method.LinesPartiallyCoveredColumn.ColumnName);


            ds.NamespaceTable.AddPercentColumn("PercentPart", ds.NamespaceTable.LinesPartiallyCoveredColumn.ColumnName, ds.NamespaceTable.LinesCoveredColumn.ColumnName, ds.NamespaceTable.LinesNotCoveredColumn.ColumnName);

            ds.Class.AddPercentColumn("PercentPart", ds.Class.LinesPartiallyCoveredColumn.ColumnName, ds.Class.LinesCoveredColumn.ColumnName, ds.Class.LinesNotCoveredColumn.ColumnName);

            ds.Method.AddPercentColumn("PercentPart", ds.Method.LinesPartiallyCoveredColumn.ColumnName, ds.Method.LinesCoveredColumn.ColumnName, ds.Method.LinesNotCoveredColumn.ColumnName);


            ds.NamespaceTable.AddPercentColumn("PercentBlock", ds.NamespaceTable.BlocksCoveredColumn.ColumnName, ds.NamespaceTable.BlocksNotCoveredColumn.ColumnName);

            ds.Class.AddPercentColumn("PercentBlock", ds.Class.BlocksCoveredColumn.ColumnName, ds.Class.BlocksNotCoveredColumn.ColumnName);

            ds.Method.AddPercentColumn("PercentBlock", ds.Method.BlocksCoveredColumn.ColumnName, ds.Method.BlocksNotCoveredColumn.ColumnName);

            ds.NamespaceTable.AddPercentColumn("PercentBlockNot", ds.NamespaceTable.BlocksNotCoveredColumn.ColumnName, ds.NamespaceTable.BlocksCoveredColumn.ColumnName);

            ds.Class.AddPercentColumn("PercentBlockNot", ds.Class.BlocksNotCoveredColumn.ColumnName,  ds.Class.BlocksCoveredColumn.ColumnName);

            ds.Method.AddPercentColumn("PercentBlockNot",ds.Method.BlocksNotCoveredColumn.ColumnName, ds.Method.BlocksCoveredColumn.ColumnName);



            foreach (CoverageDSPriv.NamespaceTableRow n in ds.NamespaceTable.Select("Percent >= 0", "Percent desc"))
            {
                sb.Append(n.ToHtml(path, minRequiredClassCoveragePercent, minRequiredMethodCoveragePercent));
            }



            sb.Append("</TABLE>");
            sb.Append("&nbsp;");


            uint sumBlocksCovered = 0;
            uint sumBlocksNotCovered = 0;
            uint sumLinesCovered = 0;
            uint sumLinesNotCovered = 0;
            uint sumLinesPartCovered = 0;
            foreach (DataRow dr in ds.NamespaceTable.Rows)
            {
                DataColumn dc = ds.NamespaceTable.BlocksCoveredColumn;
                DataColumn dc2 = ds.NamespaceTable.BlocksNotCoveredColumn;
                DataColumn dc3 = ds.NamespaceTable.LinesPartiallyCoveredColumn;
                DataColumn dc4 = ds.NamespaceTable.LinesCoveredColumn;
                DataColumn dc5 = ds.NamespaceTable.LinesNotCoveredColumn;
                sumBlocksCovered += (uint)dr[dc];
                sumBlocksNotCovered += (uint)dr[dc2];
                sumLinesPartCovered += (uint)dr[dc3];
                sumLinesCovered += (uint)dr[dc4];
                sumLinesNotCovered += (uint)dr[dc5];
            }

            //uint sumBlocksCoveredC = 0;
            //uint sumBlocksNotCoveredC = 0;
            //uint sumLinesCoveredC = 0;
            //uint sumLinesNotCoveredC = 0;
            //uint sumLinesPartCoveredC = 0;
            //foreach (DataRow dr in ds.Class.Rows)
            //{
            //    DataColumn dc = ds.Class.BlocksCoveredColumn;
            //    DataColumn dc2 = ds.Class.BlocksNotCoveredColumn;
            //    DataColumn dc3 = ds.Class.LinesPartiallyCoveredColumn;
            //    DataColumn dc4 = ds.Class.LinesCoveredColumn;
            //    DataColumn dc5 = ds.Class.LinesNotCoveredColumn;
            //    sumBlocksCoveredC += (uint)dr[dc];
            //    sumBlocksNotCoveredC += (uint)dr[dc2];
            //    sumLinesPartCoveredC += (uint)dr[dc3];
            //    sumLinesCoveredC += (uint)dr[dc4];
            //    sumLinesNotCoveredC += (uint)dr[dc5];

            //}

            //uint sumBlocksCoveredM = 0;
            //uint sumBlocksNotCoveredM = 0;
            //uint sumLinesCoveredM = 0;
            //uint sumLinesNotCoveredM = 0;
            //uint sumLinesPartCoveredM = 0;
            //foreach (DataRow dr in ds.Method.Rows)
            //{
            //    DataColumn dc = ds.Method.BlocksCoveredColumn;
            //    DataColumn dc2 = ds.Method.BlocksNotCoveredColumn;
            //    DataColumn dc3 = ds.Method.LinesPartiallyCoveredColumn;
            //    DataColumn dc4 = ds.Method.LinesCoveredColumn;
            //    DataColumn dc5 = ds.Method.LinesNotCoveredColumn;
            //    sumBlocksCoveredM += (uint)dr[dc];
            //    sumBlocksNotCoveredM += (uint)dr[dc2];
            //    sumLinesPartCoveredM += (uint)dr[dc3];
            //    sumLinesCoveredM += (uint)dr[dc4];
            //    sumLinesNotCoveredM += (uint)dr[dc5];

            //}

            sb.Append("<H3>Total Code Coverage</H3>");
            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");
            sb.Append("<TH style=\"width:7.5%;\">Blocks Covered</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Blocks Not Covered</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Covered (% Blk)</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Not Covered (% Blk)</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Lines Covered</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Lines Not Covered</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Lines Part Covered</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Covered (% Ln)</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Not Covered (% Ln)</TH>");
            sb.Append("<TH style=\"width:7.5%;\">Part Covered (% Ln)</TH>");
            sb.Append("<TH style=\"width:7.5%;\">TTL LOC</TH>");
            sb.Append("<TH style=\"width:7.5%;\">TTL Classes</TH>");
            sb.Append("<TH style=\"width:7.5%;\">TTL Methods</TH>");
            sb.Append("</TR>");


            sb.Append(CreateTotalRow( sumBlocksCovered, sumBlocksNotCovered, sumLinesCovered, sumLinesNotCovered,sumLinesPartCovered, ds.Lines.Count, ds.Class.Count, ds.Method.Count));

            sb.Append("</table>");


            sb.Append("<p style=\"font-size: medium; font-weight: normal\">" + string.Format("Class min required percent: {0}%", minRequiredClassCoveragePercent) + "</p>");

            sb.Append("<p style=\"font-size: medium; font-weight: normal\">" + string.Format("Method min required percent: {0}%", minRequiredMethodCoveragePercent) + "</p>");

            sb.Append("&nbsp;");


            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");
            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TH>Measurement</TH>");
            sb.Append("<TH>Description</TH>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Covered (Lines)</TD>");
            sb.Append("<TD>Displays the total number of lines of code that were exercised by your tests.</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Not Covered (Lines)</TD>");
            sb.Append("<TD>Displays the total number of lines of code that were not exercised by your tests.</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Covered (% Lines)</TD>");
            sb.Append("<TD>Displays the percentage of lines of code that were exercised by your tests.</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Not Covered (% Lines)</TD>");
            sb.Append("<TD>Displays the percentage of lines of code that were not exercised by your tests.</TD>");
            sb.Append("</TR>");


            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Covered (Blocks)</TD>");
            sb.Append("<TD>Displays the total number of code blocks that were exercised by your tests.</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Not Covered (Blocks)</TD>");
            sb.Append("<TD>Displays the total number of code blocks that were not exercised by your tests.</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Covered (% Blocks)</TD>");
            sb.Append("<TD>Displays the percentage of code blocks that were exercised by your tests.</TD>");
            sb.Append("</TR>");


            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Not Covered (% Blocks)</TD>");
            sb.Append("<TD>Displays the percentage of code blocks that were not exercised by your tests.</TD>");
            sb.Append("</TR>");


            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Partially Covered (Lines)</TD>");
            sb.Append("<TD>Displays the total number of lines of code where some of the code blocks within the line were not executed.</TD>");
            sb.Append("</TR>");


            sb.Append("<TR ALIGN='LEFT'>");
            sb.Append("<TD>Partially Covered (% Lines)</TD>");
            sb.Append("<TD>Displays the percentage of lines of code where some of the code blocks within the line were not executed.</TD>");
            sb.Append("</TR>");

            sb.Append("<p style=\"font-size: medium; font-weight: normal\">For code coverage, a line of code is an executable line of code that excludes white space, comments, type declarations, and namespace declarations.</p>");

            sb.Append("<p style=\"font-size: medium; font-weight: normal\">A line of code can contain multiple code blocks. If a line of code contains multiple code blocks and only a portion of those code blocks were exercised by a test run, then it is calculated as a partial line.</p>");

            sb.Append("</table>");

            sb.Append("&nbsp;");
            sb.Append("&nbsp;");
            sb.Append("<p style=\"font-size: x-small; font-weight: normal\">" + Constants.FOOTER + " : " + DateTime.Now.ToLongDateString() + ", " + DateTime.Now.ToLongTimeString() + "</p>");

            sb.Append("</body>");
            sb.Append("</html>");

            string indexFile = Path.Combine(path, "index.html");

            sb.ToFile(indexFile);
        }

        private static string CreateTotalRow( uint blocksCovered, uint blocksNotCovered, uint linesCovered, uint linesNotCovered, uint linesPartCovered,
            long totalLoc, long totalClasses, long totalMethods)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<TR ALIGN='CENTER'>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", blocksCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", blocksNotCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0}%", Utility.GetPercentCoverage(blocksCovered, blocksNotCovered)));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0}%", Utility.GetPercentCoverage(blocksNotCovered, blocksCovered)));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", linesCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", linesNotCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", linesPartCovered));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0}%", Utility.GetPercentCoverage(linesCovered, linesNotCovered, linesPartCovered)));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0}%", Utility.GetPercentCoverage(linesNotCovered, linesCovered, linesPartCovered)));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0}%", Utility.GetPercentCoverage(linesPartCovered, linesNotCovered, linesCovered)));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", totalLoc));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", totalClasses));
            sb.Append("</TD>");

            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", totalMethods));
            sb.Append("</TD>");

            sb.Append("</TR>");

            return sb.ToString();

        }
    }
}
