﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.IO;
using System.Text;

namespace CoverageConvert
{
    public static class StringExtension
    {

        public static string CleanFileName(this string filename)
        {
            string file = filename;
            file = string.Concat(file.Split(Path.GetInvalidFileNameChars(), StringSplitOptions.RemoveEmptyEntries));

            if (file.Length > 250)
            {
                file = file.Substring(0, 250);
            }
            return file;
        }

        /// <summary>
        /// Prints a string to a file.
        /// </summary>
        /// <param name="s">String to print to file.</param>
        /// <param name="pathFileName">Full path and file name to save the file.</param>
        public static void ToFile(this string s, string pathFileName)
        {
            StringBuilder sb = new StringBuilder(s);
            sb.ToFile(pathFileName);
        }

        /// <summary>
        /// Prints a stringbuilder to a file.
        /// </summary>
        /// <param name="sb">StringBuilder to print to file.</param>
        /// <param name="pathFileName">Full path and file name to save the file.</param>
        public static void ToFile(this StringBuilder sb, string pathFileName)
        {
          
            using (StreamWriter outfile = new StreamWriter(pathFileName))
            {
                outfile.Write(sb.ToString());
            }

        }
    }
}
