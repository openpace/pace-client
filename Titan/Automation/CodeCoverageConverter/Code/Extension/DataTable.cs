﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace CoverageConvert
{
    public static class DataTableExtension
    {

        public enum DataType
        {
            Boolean,
            Byte,
            Double,
            Int32,
            String
        } ;


        /// <summary>
        /// Converts a AxisPointKey to SeriesPointKey
        /// </summary>
        /// <param name="x">AxisPointKey to extend.</param>
        /// <returns>DevExpress.XtraCharts.SeriesPointKey</returns>
        public static Type GetDataType(this DataType x)
        {
            switch (x)
            {
                case DataType.Double:
                    return Type.GetType("System.Double");
                case DataType.Int32:
                    return Type.GetType("System.Int32");
                case DataType.Boolean:
                    return Type.GetType("System.Boolean");
                case DataType.Byte:
                    return Type.GetType("System.Byte");
                case DataType.String:
                    return Type.GetType("System.String");
            }

            return Type.GetType("System.Double");
        }
        

        /// <summary>
        /// Gets the ordinal (positon) of a data column in a data table.
        /// </summary>
        /// <param name="dt">Data Table to search.</param>
        /// <param name="columnName">Name of the column to find.</param>
        /// <returns>The ordianl of the column, or -1 if the column is not found.</returns>
        public static int GetColumnOrdinal(this DataTable dt, string columnName)
        {
            if (String.IsNullOrEmpty(columnName)) return -1;

            foreach (DataColumn dc in dt.Columns.Cast<DataColumn>().Where(dc => dc.ColumnName.Equals(columnName)))
            {
                return dc.Ordinal;
            }
            return -1;
        }

        /// <summary>
        /// Change the captions/column names in a data table.
        /// </summary>
        /// <param name="dt">Data table to modify.</param>
        /// <param name="newCaptions">List of new captions.</param>
        /// <param name="changeColumnNames">True to also change the column names, false to ignore.</param>
        public static void ChangeCaptions(this DataTable dt, List<string> newCaptions, bool changeColumnNames )
        {
            if(newCaptions.Count != dt.Columns.Count)
            {
                throw new ArgumentException("Invlid number of new captions.");
            }

            int i = 0;
            foreach(DataColumn dc in dt.Columns )
            {
                dc.Caption = newCaptions[i];
                if(changeColumnNames) dc.ColumnName = dc.Caption;
                i++;
            }
        }



        /// <summary>
        /// Will delete rows that contain ANY NULL values.
        /// </summary>
        public static void RemoveRowsWithAnyNulls(this DataTable dt, List<string> columnsToProcess)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            for (int a = 0; a < dt.Rows.Count; a++)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (columnsToProcess != null && !columnsToProcess.Contains(dt.Columns[i].ColumnName)) continue;
                    Type type = dt.Columns[i].DataType;
                    if (type == typeof(String))
                    {
                        if(dt.Rows[a][i] != DBNull.Value)
                        {
                            string value = (string) dt.Rows[a][i];
                            if (String.IsNullOrEmpty(value.Trim()))
                            {
                                dt.Rows[a].Delete();
                                break;
                            }
                        }
                        else
                        {
                            dt.Rows[a].Delete();
                            break;
                        }
                    }
                    else if (dt.Rows[a][i] == DBNull.Value || dt.Rows[a][i] == null)
                    {
                        dt.Rows[a].Delete();
                        break;
                    }
                }
            }
            dt.AcceptChanges();

        }

        /// <summary>
        /// Will replace null values with zero/String.Empty.
        /// </summary>
        /// <returns>True if something is fixed.</returns>
        public static bool ReplaceNullsWithZero(this DataTable dt, List<string> columnsToIgnore)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool ret = false;
            for (int a = 0; a < dt.Rows.Count; a++)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (columnsToIgnore != null && columnsToIgnore.Contains(dt.Columns[i].ColumnName)) continue;
                    Type type = dt.Columns[i].DataType;

                    if (type == typeof(String))
                    {
                        if (dt.Rows[a][i] == DBNull.Value)
                        {
                            dt.Rows[a][i] = String.Empty;
                            ret = true;
                        }
                    }
                    else if (dt.Rows[a][i] == DBNull.Value || dt.Rows[a][i] == null)
                    {
                        dt.Rows[a][i] = 0;
                        ret = true;
                    }
                }
            }
            dt.AcceptChanges();

            return ret;
        }

        /// <summary>
        /// Adds a calcualted column to the data table.
        /// </summary>
        /// <param name="x">DataTable</param>
        /// <param name="columnName"></param>
        /// <param name="columnType"></param>
        /// <param name="expression"></param>
        /// <param name="columnCaption"></param>
        /// <param name="displayFormat"></param>
        public static void AddCalculatedExpression(this DataTable x, string columnName, Type columnType, string expression, string columnCaption , string displayFormat)
        {

            x.AddColumn(columnName, columnType, expression, columnCaption, displayFormat);

            x.AcceptChanges();
        }



        /// <summary>
        /// Creates a new table 
        /// </summary>
        /// <param name="x">Table to filter.</param>
        /// <param name="fieldName">Field (column) to get the distinct values.</param>
        /// <returns></returns>
        public static List<string> FilterToDistinctList(this DataTable x, string fieldName)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var test = (from d in x.AsEnumerable()
                        select new
                        {
                            Name = d.Field<object>(fieldName)
                        }).Distinct().ToList();

            List<string> items = test.Select(id => id.Name).OfType<string>().ToList(); ;

            items.Sort();


            return items;
        }

        /// <summary>
        /// Removes duplicate rows from a data table (looks at all columns).
        /// </summary>
        /// <param name="x">DataTable to remove duplicates.</param>
        /// <returns>A data table with the duplicates removed.</returns>
        public static DataTable RemoveDuplicateRows(this DataTable x)
        {
            Stopwatch sw = Stopwatch.StartNew();

            Hashtable hTable = new Hashtable();
            List<int> duplicateList = new List<int>();

            int i = 0;
            foreach (DataRow drow in x.Rows)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DataColumn dc in x.Columns)
                {
                    sb.Append(drow[dc.ColumnName].ToString());
                    
                }
                if (hTable.Contains(sb.ToString()))
                {
                    duplicateList.Add(i);
                }
                else
                {
                    hTable.Add(sb.ToString(), i);
                }
                i++;
            }

            duplicateList.Reverse();

            foreach (int r in duplicateList)
            {
                x.Rows.RemoveAt(r);
            }
            x.AcceptChanges();

            return x;
        }

        public static DataTable RemoveDuplicates(this DataTable dt)
        {
            // Find the unique contacts in the table.
            IEnumerable<DataRow> query = dt.AsEnumerable().Distinct(DataRowComparer.Default);

            DataSet ds = new DataSet();
            ds.Tables.Add(query.CopyToDataTable());

            return ds.Tables[0];
        }

        



        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn to place where clause on.</param>
        /// <param name="value">Value to contains (Contains)</param>
        /// <returns>List of DataRow</returns>
        public static List<DataRow> SelectContains(this DataTable x, string columnName, string value)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var lst =  (from myRow in x.AsEnumerable()
                           where myRow.Field<string>(columnName).Contains(value)
                            select myRow).ToList();


            return lst;
        }
         //object d = new double();
         //           object s = String.Empty;
         //           object objectToPass = null;
         //           DataColumn dataColumn = table.GetDataColumn(measure);
         //           if (dataColumn.DataType == typeof(string))
         //           {
         //               objectToPass = s;
         //           }
         //           else if (dataColumn.DataType == typeof(double))
         //           {
         //               objectToPass = d;
         //           }



        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="orderBy">Column to orderby</param>
        /// <param name="ascending">Sort Assending (false to sort desending)</param>
        /// <param name="numRowToReturn">Filter the resulting DataTable</param>
        /// <returns>DataTable</returns>
        public static DataTable OrderBy(this DataTable x, string orderBy, bool ascending, int numRowToReturn)
        {
            DataColumn dataColumn = x.GetDataColumn(orderBy);
            object d = new double();
            object s = String.Empty;
            int i = new int();
            object objectToPass = null;
            if (dataColumn.DataType == typeof(string))
            {
                objectToPass = s;
            }
            else if (dataColumn.DataType == typeof(double))
            {
                objectToPass = d;
            }
            else if (dataColumn.DataType == typeof(Int32))
            {
                objectToPass = i;
            }

            DataTable dt = OrderBy(x, objectToPass, orderBy, ascending, numRowToReturn);

            x = dt;

            return dt;

        }

        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        ///<typeparam name="T">DataType of the DataColumn.</typeparam>
        /// <param name="value">Value type</param>
        /// <param name="orderBy">Column to orderby</param>
        /// <param name="ascending">Sort Assending (false to sort desending)</param>
        /// <param name="numRowToReturn">Filter the resulting DataTable</param>
        /// <returns>DataTable</returns>
        public static DataTable OrderBy<T>(this DataTable x, T value, string orderBy, bool ascending, int numRowToReturn)
        {
            OrderedEnumerableRowCollection<DataRow> query = null;
            if (ascending)
            {
                query = from c in x.AsEnumerable()
                        orderby c.Field<T>(orderBy)
                        select c;
            }
            else
            {
                query = from c in x.AsEnumerable()
                        orderby c.Field<T>(orderBy) descending
                        select c;
            }


            List<DataRow> rows = numRowToReturn > -1 ? query.Take(numRowToReturn).ToList() : query.ToList();

            DataTable dt =  rows.CopyToDataTable();

            return dt;
        }

        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <typeparam name="T">DataType of the DataColumn.</typeparam>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn to place where clause on.</param>
        /// <param name="value">Value must equal (where)</param>
        /// <returns>List of DataRow</returns>
        public static List<DataRow> SelectWhere<T>(this DataTable x, string columnName, T value)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var lst =  (from myRow in x.AsEnumerable()
                    where myRow.Field<T>(columnName).Equals(value)
                    select myRow).ToList();

            return lst;
        }

        

        /// <summary>
        /// Gets the data type of a DataColumn
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn</param>
        /// <returns>The data type.</returns>
        public static Type GetDataColumnType(this DataTable x, string columnName)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (String.IsNullOrEmpty(columnName)) throw new ArgumentException("Column name cannot be null or blank.");

            return !x.Columns.Contains(columnName) ? null : x.Columns[columnName].DataType;
        }

        /// <summary>
        /// Gets the DataColumn name
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnOrdinal">Ordinal (position) of the DataColumn</param>
        /// <returns>The data column.</returns>
        public static string GetDataColumnName(this DataTable x, int columnOrdinal)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (columnOrdinal == -1) throw new ArgumentException("Invalid Column Ordinal.");

            return x.Columns[columnOrdinal].ColumnName;
        }

        /// <summary>
        /// Gets the list of DataColumn names
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="stopperColumn">Column to stop at.</param>
        /// <returns>The data columns.</returns>
        public static List<string> GetDataColumnNames(this DataTable x, string stopperColumn)
        {
            if(stopperColumn == null)
            {
                stopperColumn = String.Empty;
            }

            return x.Columns.Cast<DataColumn>().TakeWhile(c => !c.ColumnName.Equals(stopperColumn)).Select(c => c.ColumnName).ToList();
        }


        /// <summary>
        /// Gets the DataColumn
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn</param>
        /// <returns>The data column.</returns>
        public static DataColumn GetDataColumn(this DataTable x, string columnName)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (String.IsNullOrEmpty(columnName)) throw new ArgumentException("Column name cannot be null or blank.");

            return !x.Columns.Contains(columnName) ? null : x.Columns[columnName];
        }

        /// <summary>
        /// Adds a DataColumn to a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the column to add.</param>
        /// <param name="columnType">Data type of the column.</param>
        /// <param name="expression">Optional.  Expression for the DataColumn.</param>
        /// <param name="columnCaption">Caption of the column</param>
        /// <param name="displayFormat">Numeric format of the column.</param>
        /// <returns>The DataColumn.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static DataColumn AddColumn(this DataTable x, string columnName, Type columnType, string expression , string columnCaption , string displayFormat)
        {
            Stopwatch sw = Stopwatch.StartNew();

            DataColumn dc = null;
            if (x == null)
            {
                throw new ArgumentException("DataTable argument is null.");
            }

            if (!x.Columns.Contains(columnName))
            {
                if (String.IsNullOrEmpty(expression))
                {
                    dc = x.Columns.Add(columnName, columnType);
                }
                else
                {
                    dc = x.Columns.Add(columnName, columnType, expression);
                }
            }
            else
            {
                dc = x.Columns[columnName];
                if(!dc.Expression.Equals(expression)) dc.Expression = expression;
            }

            if(!String.IsNullOrEmpty(columnCaption) && dc != null)
            {
                dc.Caption = columnCaption;
            }

            if (displayFormat != null)
            {
                if (dc.ExtendedProperties.ContainsKey("DisplayFormat"))
                {
                    dc.ExtendedProperties.Remove("DisplayFormat");
                }
                dc.ExtendedProperties.Add("DisplayFormat", displayFormat);
            }



            return x.Columns[columnName];
         
        }

    }
}
