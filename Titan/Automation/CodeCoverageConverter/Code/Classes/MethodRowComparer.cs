﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.CodeCoverage;

namespace CoverageConvert
{
    public class MethodRowComparer : IComparer<CoverageDSPriv.MethodRow>
    {
        public int Compare(CoverageDSPriv.MethodRow x, CoverageDSPriv.MethodRow y)
        {
            double xx = Convert.ToDouble(x["Percent"]);
            double yy = Convert.ToDouble(y["Percent"]);

            if (xx < yy) return 1;
            if (xx > yy) return -1;
            return 0;
        }
    }
}
