#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.CodeCoverage;

namespace CoverageConvert
{
	class Program
	{
		#region Constants
		private const string DEFAULT_OUTFILE_NAME = "CodeCoverageResults.xml";
	    private const int MinRequiredMethodCoveragePercent = 30;
	    private const int MinRequiredClassCoveragePercent = 30;
	    private static string _Path;
		#endregion Constants

		#region Private Fields
		private static bool DisplayHelp = false;
		private static string CoverageDataFile = null;
		private static string SymbolsPath = null;
		private static string ExePath = null;
		private static string OutFile = null;
		#endregion Private Fields

		#region Main
		static int Main(string[] args)
		{
			try
			{
                string filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                _Path = Path.GetDirectoryName(filePath);

				//if no arguments supplied show help
				if (args.Length == 0)
				{
					ShowHelp();
					return 0;
				}
                else
				{
				    foreach(string s in args)
				    {
                        Console.WriteLine(s);
				    }
				}

				//parse the supplied arguments
				ParseArguments(args);

				//if '/help' argument was supplied then show help
				if (DisplayHelp == true)
				{
					ShowHelp();
					return 0;
				}
				// '/help' argument not supplied so process normally
				else
				{
					//make sure a code coverage data file was specified
					if ((CoverageDataFile == null) || (CoverageDataFile.Trim().Length == 0))
					{
						System.Console.WriteLine("No code coverage data file was specified. Use the /infile parameter to specify it.");
						return 1;
					}

					//make sure output file is specified
					if ((OutFile == null) || (OutFile.Trim().Length == 0))
						OutFile = Path.GetFullPath(DEFAULT_OUTFILE_NAME);

					//set symbols/exe paths
					CoverageInfoManager.SymPath = SymbolsPath;
					CoverageInfoManager.ExePath = ExePath;

					//load the specified code coverage data file
					CoverageInfo cinfo = CoverageInfoManager.CreateInfoFromFile(CoverageDataFile);

					//Generate a dataset from the code coverage data file
					CoverageDS cdata = cinfo.BuildDataSet(null);

				    //WriteSomething(cdata, false);
                    Utility.WriteToHtml(cdata, _Path, MinRequiredClassCoveragePercent, MinRequiredMethodCoveragePercent);

					//write the dataset as XML
					cdata.WriteXml(OutFile);

					return 0;
				}
			}
			catch (Exception ex)
			{
				System.Console.WriteLine(ex.Message);
				return 1;
			}
		}

        public static int WriteSomething(CoverageDS myCovDS, bool blnDisplayOnlyErrors)
        {

            FileStream ostrm;
            StreamWriter writer;
            TextWriter oldOut = Console.Out;
            try
            {
                ostrm = new FileStream("./CoverageResults.txt", FileMode.OpenOrCreate, FileAccess.Write);
                writer = new StreamWriter(ostrm);
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot open Redirect.txt for writing");
                Console.WriteLine(e.Message);
                return 0;
            }
            Console.SetOut(writer);


            //loop through and display results
            Console.WriteLine("Code coverage results. All measurements in Blocks, not LinesOfCode.");

            int TotalClassCount = myCovDS.Class.Count;
            int TotalMethodCount = myCovDS.Method.Count;

            try
            {
                Console.WriteLine();

                Console.WriteLine("Coverage Policy:");
                Console.WriteLine(string.Format(" Class min required percent: {0}%", MinRequiredClassCoveragePercent));
                Console.WriteLine(string.Format(" Method min required percent: {0}%", MinRequiredMethodCoveragePercent));

                Console.WriteLine("Covered / Not Covered / Percent Coverage");
                Console.WriteLine();

                string strTab1 = new string(' ', 2);
                string strTab2 = strTab1 + strTab1;


                int intClassFailureCount = 0;
                int intMethodFailureCount = 0;

                int Percent = 0;
                bool isValid = true;
                string strError = null;
                const string cErrorMsg = "[FAILED: TOO LOW] ";

                //NAMESPACE
                foreach (CoverageDSPriv.NamespaceTableRow n in myCovDS.NamespaceTable)
                {
                    Console.WriteLine(string.Format("Namespace: {0}: {1} / {2} / {3}%",
                      n.NamespaceName, n.BlocksCovered, n.BlocksNotCovered, Utility.GetPercentCoverage(n.BlocksCovered, n.BlocksNotCovered)));

                    //CLASS
                    foreach (CoverageDSPriv.ClassRow c in n.GetClassRows())
                    {
                        Percent = Utility.GetPercentCoverage(c.BlocksCovered, c.BlocksNotCovered);
                        isValid = Utility.IsValidPolicy(Percent, MinRequiredClassCoveragePercent);
                        strError = null;
                        if (!isValid)
                        {
                            strError = cErrorMsg;
                            intClassFailureCount++;
                        }

                        if (Utility.ShouldDisplay(blnDisplayOnlyErrors, isValid))
                        {
                            Console.WriteLine(string.Format(strTab1 + "{4}Class: {0}: {1} / {2} / {3}%",
                              c.ClassName, c.BlocksCovered, c.BlocksNotCovered, Percent, strError));
                        }

                        //METHOD
                        foreach (CoverageDSPriv.MethodRow m in c.GetMethodRows())
                        {
                            Percent = Utility.GetPercentCoverage(m.BlocksCovered, m.BlocksNotCovered);
                            isValid = Utility.IsValidPolicy(Percent, MinRequiredMethodCoveragePercent);
                            strError = null;
                            if (!isValid)
                            {
                                strError = cErrorMsg;
                                intMethodFailureCount++;
                            }

                            string strMethodName = m.MethodFullName;
                            if (blnDisplayOnlyErrors)
                            {
                                //Need to print the full method name so we have full context
                                strMethodName = c.ClassName + "." + strMethodName;
                            }

                            if (Utility.ShouldDisplay(blnDisplayOnlyErrors, isValid))
                            {
                                Console.WriteLine(string.Format(strTab2 + "{4}Method: {0}: {1} / {2} / {3}%", strMethodName, m.BlocksCovered, m.BlocksNotCovered, Percent, strError));
                            }
                        }
                    }
                }

                Console.WriteLine();

                //Summary results
                Console.WriteLine(string.Format("Total Namespaces: {0}", myCovDS.NamespaceTable.Count));
                Console.WriteLine(string.Format("Total Classes: {0}", TotalClassCount));
                Console.WriteLine(string.Format("Total Methods: {0}", TotalMethodCount));
                Console.WriteLine();

                int intReturnCode = 0;
                if (intClassFailureCount > 0)
                {
                    Console.WriteLine(string.Format("Failed classes: {0} / {1}", intClassFailureCount, TotalClassCount));
                    intReturnCode = 1;
                }
                if (intMethodFailureCount > 0)
                {
                    Console.WriteLine(string.Format("Failed methods: {0} / {1}", intMethodFailureCount, TotalMethodCount));
                    intReturnCode = 1;
                }

                return intReturnCode;
            }
            finally
            {
                Console.SetOut(oldOut);
                writer.Close();
                ostrm.Close();
                Console.WriteLine("Done");
            }
        }

        
        

	    #endregion Main

		#region Private Methods
		private static void ParseArguments(string[] args)
		{
			foreach (string arg in args)
			{
				Arg argument = ParseArg(arg);
				switch (argument.Cmd.ToUpper())
				{
					case "/HELP":
						DisplayHelp = true;
						break;
					case "/INFILE":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							CoverageDataFile = Path.GetFullPath(argument.Value);
						else
							CoverageDataFile = argument.Value;
						break;
					case "/OUTFILE":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							OutFile = Path.GetFullPath(argument.Value);
						else
							OutFile = argument.Value;
						break;
					case "/SYMPATH":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							SymbolsPath = Path.GetFullPath(argument.Value);
						else
							SymbolsPath = argument.Value;
						break;
					case "/EXEPATH":
						if ((argument.Value != null) && (argument.Value.Trim().Length > 0))
							ExePath = Path.GetFullPath(argument.Value);
						else
							ExePath = argument.Value;
						break;
					default:
						DisplayHelp = true;
						break;
				}
			}
		}

		private static Arg ParseArg(string arg)
		{
			Arg argument = new Arg();

			int index = arg.IndexOf(':');
			if (index > 0)
			{
				argument.Cmd = arg.Substring(0, index);
				argument.Value = arg.Substring(index+1, arg.Length - index - 1);
			}
			else
			{
				argument.Cmd = arg;
			}

			return argument;
		}

		private static void ShowHelp()
		{
			System.Console.WriteLine("");
			System.Console.WriteLine("Usage:");
			System.Console.WriteLine("");
			System.Console.WriteLine("Description: \t Converts a Visual Studio 2005 code coverage data file to XML.");
			System.Console.WriteLine("");
			System.Console.WriteLine("Options:");
			System.Console.WriteLine("");
			System.Console.WriteLine("/help \t\t Display this usage message.");
			System.Console.WriteLine("");
			System.Console.WriteLine("/infile \t Load a code coverage data file.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /infile:data.coverage");
			System.Console.WriteLine("");
			System.Console.WriteLine("/outfile \t Save the conversion output to the specified file.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /outfile:c:\\temp\\coverageresults.xml");
			System.Console.WriteLine("");
			System.Console.WriteLine("/sympath \t The path to the debug symbol files.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /sympath:c:\\testresults\\out");
			System.Console.WriteLine("");
			System.Console.WriteLine("/exepath \t The path to the executable file.");
			System.Console.WriteLine("\t\t Example:");
			System.Console.WriteLine("\t\t   /exepath:c:\\testresults\\out");
		}
		#endregion Private Methods

		#region Types
		private struct Arg
		{
			public string Cmd;
			public string Value;
		}
		#endregion Structs
	}
}
