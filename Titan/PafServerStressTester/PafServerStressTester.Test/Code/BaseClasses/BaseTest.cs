﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Reflection;
using NUnit.Framework;

namespace PafServerStressTester.Test
{
    /// <summary>
    /// Abstract test that handles the setup and termination of the sample data.
    /// </summary>
    public abstract class BaseTest
    {
        protected string AppPath ;
        protected string TableName = "performancedata";

        [TestFixtureSetUp]
        public void Init()
        {
            string filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            AppPath = System.IO.Path.GetDirectoryName(filePath); 
            
        }

        [Test]
        public abstract void Test();
    }
}
