﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Data;
using System.IO;
using NUnit.Framework;
using Npgsql;
using NpgsqlTypes;
using PafServerStressTester.Test.Properties;
using ServerStressTesterBase.Pace.Data;
using ServerStressTesterBase.Pace.Extensions;
using ServerStressTesterDataSet.Pace.Data;

namespace PafServerStressTester.Test
{
     [TestFixture]
    public class GenerateStats : BaseTest
    {
         public const string File1 = "DataSet.xml";
         public const string File2 = "DataSet-1.xml";
         public const string FileName = "";

         [Test]
         public override void Test()
         {


             try
             {

                 var command = new NpgsqlCommand("select_filename_limit") {CommandType = CommandType.StoredProcedure};

                 command.Parameters.Add(new NpgsqlParameter("text", NpgsqlDbType.Text));
                 command.Parameters.Add(new NpgsqlParameter("int", NpgsqlDbType.Integer));
                 command.Parameters[0].Value = "Jim_Buyer_Fall_Plan_2006.btth";
                 command.Parameters[1].Value = 2;
                 
                 BaseDataAdapter select = new PostgresDataAdapter(Settings.Default.ConnectionString, command);
                 select.Fill();

                 TestResultsDataset testResultsDataset = new TestResultsDataset();
                 int i = 0;
                 foreach(DataRow dr in select.Data.Tables[0].Rows)
                 {

                     string xml = (string)dr["d_xml"];
                     if(i == 0)
                     {
                         testResultsDataset = TestResultsDatasetExtension.LoadFromXml(xml);
                     }
                     else
                     {
                         testResultsDataset.Merge(TestResultsDatasetExtension.LoadFromXml(xml));
                     }
                     i++;
                 }

                 Assert.IsNotNull(testResultsDataset);

                 testResultsDataset.CreateComparison();

                 //var command1 = new NpgsqlCommand("SELECT distinct(d_date) FROM performancedata;");
                 //select = new PostgresDataAdapter(Settings.Default.ConnectionString, command1);
                 //select.Fill();
                 //DataSet ds1 = select.Data;

                 //Assert.AreNotEqual(0, ds1.Tables[0].Rows.Count);



                 testResultsDataset.GetNonNormalizedTestResults().ToHtml(Path.Combine(AppPath, "testresults.html"));
                 testResultsDataset.GetNonNormalizedTestResultsComparison().ToHtml(Path.Combine(AppPath, "testresultscomparison.html"));
                 testResultsDataset.Settings.ToHtml(Path.Combine(AppPath, ".settings.html"));
                 testResultsDataset.GetNonNormalizedThreadTestResults().ToHtml(Path.Combine(AppPath, ".threadtestresults.html"));
                 testResultsDataset.GetNonNormalizedTestTimes().ToHtml(Path.Combine(AppPath, ".testtimes.html"));
                 testResultsDataset.Tests.ToHtml(Path.Combine(AppPath, ".tests.html"));
                 testResultsDataset.Dates.DatesDescending().ToHtml(Path.Combine(AppPath, ".dates.html"));
             }
             catch (ConstraintException ce)
             {
                 //DataRow[] rowsInError; 
                 //foreach (DataTable table in ds1.Tables)
                 //{
                 //    // Test if the table has errors. If not, skip it.
                 //    if (table.HasErrors)
                 //    {
                 //        // Get an array of all rows with errors.
                 //        rowsInError = table.GetErrors();
                 //        // Print the error of each column in each row.
                 //        for (int i = 0; i < rowsInError.Length; i++)
                 //        {
                 //            foreach (DataColumn column in table.Columns)
                 //            {
                 //                Debug.WriteLine(column.ColumnName + " " +
                 //                    rowsInError[i].GetColumnError(column));
                 //            }
                 //            // Clear the row errors
                 //            rowsInError[i].ClearErrors();
                 //        }
                 //    }
                 //}
             }
         }
    }
}
