﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using NUnit.Framework;
using Npgsql;
using NpgsqlTypes;
using PafServerStressTester.Test.Properties;
using ServerStressTesterBase.Pace.Data;
using ServerStressTesterBase.Pace.Extensions;
using ServerStressTesterDataSet.Pace.Data;

namespace PafServerStressTester.Test
{
     [TestFixture]
    public class Class1 : BaseTest
    {
         public const string File1 = "DataSet.xml";
         public const string File2 = "DataSet-1.xml";

         [Test]
         public override void Test()
         {

             TestResultsDataset ds1 = TestResultsDatasetExtension.Load(Path.Combine(AppPath, File1));
             TestResultsDataset ds2 = TestResultsDatasetExtension.Load(Path.Combine(AppPath, File2));

             try
             {
                 ds1.Merge(ds2);

                 List<TestResultsDataset.DatesRow> rows = ds1.GetDates(2);
                 List<TestResultsDataset.TestsRow> tests = ds1.GetTests();

                 Assert.AreEqual(rows.Count, 2);


                 foreach (TestResultsDataset.TestsRow testRow in tests)
                 {

                     TestResultsDataset.TestResultsComparisonRow newRow = ds1.TestResultsComparison.NewTestResultsComparisonRow();
                     int i = 1;
                     List<TestResultsDataset.TestResultsRow> results = ds1.FindTestResults(testRow.TestNumber, orderByTestNumber: false, orderByDate: true, ascending: false);
                     foreach (TestResultsDataset.TestResultsRow result in results)
                     {

                        if (i == 1)
                        {
                            newRow.TestNumber = result.TestNumber;
                            newRow.BestEvaluation = result.BestEvaluation;
                            newRow.WorstEvaluation = result.WorstEvaluation;
                            newRow.AverageEvaluation = result.AverageEvaluation;
                            newRow.TotalTestRuntime = result.TotalTestRuntime;
                        }
                        else
                        {
                            newRow.TestNumber = result.TestNumber;
                            newRow.BestEvaluationPrev = result.BestEvaluation;
                            newRow.WorstEvaluationPrev = result.WorstEvaluation;
                            newRow.AverageEvaluationPrev = result.AverageEvaluation;
                            newRow.TotalTestRuntimePrev = result.TotalTestRuntime;
                        }
                        i++;

                     }
                     ds1.TestResultsComparison.AddTestResultsComparisonRow(newRow);
                 }
                 ds1.RemotingFormat = SerializationFormat.Binary;

                 //BinaryFormatter format = new BinaryFormatter();
                 //using (FileStream fs = new FileStream(@"c:\sar1.bin", FileMode.CreateNew))
                 //{
                 //    ds1.RemotingFormat = SerializationFormat.Binary;

                 //    format.Serialize(fs, ds1);
                 //}
                 ds1.WriteXml(Path.Combine(AppPath, "merged.xml"));

                 var command = new NpgsqlCommand("addxml");
                 command.CommandType = CommandType.StoredProcedure;
                 
                 command.Parameters.Add(new NpgsqlParameter("date", NpgsqlDbType.Timestamp));
                 command.Parameters.Add(new NpgsqlParameter("xml", NpgsqlDbType.Xml));
                 command.Parameters.Add(new NpgsqlParameter("text", NpgsqlDbType.Text));

                 //command.Parameters.AddWithValue("@XML", NpgsqlDbType.Xml);
                 //command.Parameters.AddWithValue("@Date", NpgsqlDbType.Date);
                 command.Parameters[0].Value = System.DateTime.Now;
                 command.Parameters[1].Value = ds1.GetXml();
                 command.Parameters[2].Value = "myfilename";
                 
                 BaseDataAdapter insert = new PostgresDataAdapter(Settings.Default.ConnectionString, command, TableName);
                 insert.ExecuteNonQuery();

                 ds1.GetNonNormalizedTestResults().ToHtml(Path.Combine(AppPath, "testresults.html"));
                 ds1.GetNonNormalizedTestResultsComparison().ToHtml(Path.Combine(AppPath, "testresultscomparison.html"));
                 ds1.Settings.ToHtml(Path.Combine(AppPath,  ".settings.html"));
                 ds1.GetNonNormalizedThreadTestResults().ToHtml(Path.Combine(AppPath,  ".threadtestresults.html"));
                 ds1.GetNonNormalizedTestTimes().ToHtml(Path.Combine(AppPath,  ".testtimes.html"));
                 ds1.Tests.ToHtml(Path.Combine(AppPath, ".tests.html"));
                 ds1.Dates.DatesDescending().ToHtml(Path.Combine(AppPath, ".dates.html"));
             }
             catch (ConstraintException ce)
             {
                 DataRow[] rowsInError; 
                 foreach (DataTable table in ds1.Tables)
                 {
                     // Test if the table has errors. If not, skip it.
                     if (table.HasErrors)
                     {
                         // Get an array of all rows with errors.
                         rowsInError = table.GetErrors();
                         // Print the error of each column in each row.
                         for (int i = 0; i < rowsInError.Length; i++)
                         {
                             foreach (DataColumn column in table.Columns)
                             {
                                 Debug.WriteLine(column.ColumnName + " " +
                                     rowsInError[i].GetColumnError(column));
                             }
                             // Clear the row errors
                             rowsInError[i].ClearErrors();
                         }
                     }
                 }
             }
         }
    }
}
