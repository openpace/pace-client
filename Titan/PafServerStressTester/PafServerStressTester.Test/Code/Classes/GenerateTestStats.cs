﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using NUnit.Framework;
using Npgsql;
using NpgsqlTypes;
using PafServerStressTester.Test.Properties;
using ServerStressTesterBase.Pace.Chart.Classes;
using ServerStressTesterBase.Pace.Data;
using ServerStressTesterDataSet.Pace.Data;
using ServerStressTesterUiArtifacts;
using ServerStressTesterUiArtifacts.Code.Classes;

namespace PafServerStressTester.Test
{
     [TestFixture]
    public class GenerateTestStats : BaseTest
    {
         public const string FileName = "";

         [Test]
         public override void Test()
         {


             try
             {

                 var command = new NpgsqlCommand("get_xml");
                 command.CommandType = CommandType.StoredProcedure;
                 command.Parameters.Add(new NpgsqlParameter("text", NpgsqlDbType.Text));
                 command.Parameters[0].Value = "Jim_Testing_Spring_Forecast_2006.btth";
                 NpgsqlParameter returnColumn = new NpgsqlParameter("d_xml", NpgsqlDbType.Xml);
                 returnColumn.Direction = ParameterDirection.Output;
                 command.Parameters.Add(returnColumn);

                 BaseDataAdapter query = new PostgresDataAdapter(Settings.Default.ConnectionString, command);
                 query.Fill();

                TestResultsDataset testResultsDataset = new TestResultsDataset();
                 if(returnColumn.Value != null)
                 {
                     testResultsDataset = TestResultsDatasetExtension.LoadFromXml(returnColumn.Value.ToString());
                 }

                 Assert.IsNotNull(testResultsDataset);




                 List<TestResultsDataset.TestsRow> tests = testResultsDataset.GetTests();
                 //List<TestResultsDataset.TestResultsRow> rows = testResultsDataset.GetTestResultsList();
                 EnumerableRowCollection<TestResultsDataset.TestResultsRow> testResults = testResultsDataset.GetTestResults();

                 foreach(TestResultsDataset.TestsRow test in tests)
                 {
                     EnumerableRowCollection<TestResultsDataset.TestResultsRow> results = testResults.Where(y =>  y.TestNumber == test.TestNumber);
                     EnumerableRowCollection<TestResultsDataset.TestTimesRow> testTimes = testResultsDataset.GetTestTimeResults(test.TestNumber);   


                     TestResultsDataset.TestResultsDataTable tempTestResults = new TestResultsDataset.TestResultsDataTable();
                     results.CopyToDataTable(tempTestResults, LoadOption.OverwriteChanges);

                     TestResultsDataset.TestTimesDataTable tempTestTimes = new TestResultsDataset.TestTimesDataTable();
                     testTimes.CopyToDataTable(tempTestTimes, LoadOption.OverwriteChanges);

                     ChartSeriesUi<double, double> chart = new ChartSeriesUi<double, double>(
                         "series1",
                         xColumnName: "Date",
                         yColumnName: "AverageEvaluation");


                     XtraCharts xc = new XtraCharts(tempTestResults.GetNonNormalizedTestResults(testResultsDataset), new ChartTitleInfo("Average Evaluation - " + test.TestName), chart);

                     xc.GeneratePointChart();
                     xc.ExportToImage(@"D:\tmp\" + test.TestName + "-avgeval.png");

                     xc.DataSource = tempTestTimes.GetNonNormalizedTestTimes(testResultsDataset);
                     xc.ChartTitleInfo.Text = "All Evaluations - " + test.TestName;
                     xc.ChartSeries.DataTableYColumnName = "EvaluationRuntime";
                     xc.GeneratePointChart();
                     xc.ExportToImage(@"D:\tmp\" + test.TestName + "-alleval.png");

                     List<TestResultsDataset.TestResultsRow> resultsList = results.ToList();


                     tempTestResults.ToHtml(
                         test.TestName,
                         @"D:\tmp\" + test.TestName + ".html",
                         resultsList[0].TotalTestRuntime,
                         resultsList[0].WorstEvaluation,
                         resultsList[0].AverageEvaluation,
                         resultsList[0].TotalTestRuntime,
                         resultsList[0].AvgUowCreationTime,
                         resultsList[0].AvgViewCreationTime,
                         results.GetBestEvaluation(), 
                         results.GetWorstEvaluation(),
                         results.GetAvgEvaluationMin(),
                         results.GetAvgEvaluationMax(),
                         results.GetTotalRuntimeMin(),
                         results.GetTotalRuntimeMax(),
                         results.GetAvgUowCreationTimeMin(),
                         results.GetAvgUowCreationTimeMax(),
                         results.GetAvgViewCreationTimeMin(),
                         results.GetAvgViewCreationTimeMax(),
                         Uri.EscapeUriString(test.TestName) + "-avgeval.png",
                         Uri.EscapeUriString(test.TestName) + "-alleval.png");



                 }

                 //testResultsDataset.GetNonNormalizedTestResults().ToHtml(Path.Combine(AppPath, "testresults.html"));
                 //testResultsDataset.GetNonNormalizedTestResultsComparison().ToHtml(Path.Combine(AppPath, "testresultscomparison.html"));
                 //testResultsDataset.Settings.ToHtml(Path.Combine(AppPath, ".settings.html"));
                 //testResultsDataset.GetNonNormalizedThreadTestResults().ToHtml(Path.Combine(AppPath, ".threadtestresults.html"));
                 //testResultsDataset.GetNonNormalizedTestTimes().ToHtml(Path.Combine(AppPath, ".testtimes.html"));
                 testResultsDataset.Tests.ToHtml(Path.Combine(AppPath, ".tests.html"));
                 //testResultsDataset.Dates.DatesDescending().ToHtml(Path.Combine(AppPath, ".dates.html"));
             }
             catch (ConstraintException ce)
             {
                 //DataRow[] rowsInError; 
                 //foreach (DataTable table in ds1.Tables)
                 //{
                 //    // Test if the table has errors. If not, skip it.
                 //    if (table.HasErrors)
                 //    {
                 //        // Get an array of all rows with errors.
                 //        rowsInError = table.GetErrors();
                 //        // Print the error of each column in each row.
                 //        for (int i = 0; i < rowsInError.Length; i++)
                 //        {
                 //            foreach (DataColumn column in table.Columns)
                 //            {
                 //                Debug.WriteLine(column.ColumnName + " " +
                 //                    rowsInError[i].GetColumnError(column));
                 //            }
                 //            // Clear the row errors
                 //            rowsInError[i].ClearErrors();
                 //        }
                 //    }
                 //}
             }
         }

         public void MoveData()
         {
             var command = new NpgsqlCommand("select * FROM performancedata;") { CommandType = CommandType.Text };


             BaseDataAdapter select = new PostgresDataAdapter(Settings.Default.ConnectionString, command);
             select.Fill();

             TestResultsDataset testResultsDataset = new TestResultsDataset();
             int i = 0;
             foreach (DataRow dr in select.Data.Tables[0].Rows)
             {

                 string xml = (string)dr["d_xml"];
                 if (i == 0)
                 {
                     testResultsDataset = TestResultsDatasetExtension.LoadFromXml(xml);
                 }
                 else
                 {
                     testResultsDataset.Merge(TestResultsDatasetExtension.LoadFromXml(xml));
                 }
                 i++;
             }

             Assert.IsNotNull(testResultsDataset);



             command = new NpgsqlCommand("update_xml") { CommandType = CommandType.StoredProcedure };

             command.Parameters.Add(new NpgsqlParameter("date", NpgsqlDbType.Timestamp));
             command.Parameters.Add(new NpgsqlParameter("xml", NpgsqlDbType.Xml));

             command.Parameters[0].Value = System.DateTime.Now;
             command.Parameters[1].Value = testResultsDataset.GetXml();

             BaseDataAdapter insert = new PostgresDataAdapter(Settings.Default.ConnectionString, command);
             insert.ExecuteNonQuery();
         }

    }
}
