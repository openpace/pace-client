﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Linq;
using Titan.PafService;


namespace PafServerStressTester
{
    public static class PafUserSelectionExtension
    {
        /// <summary>
        /// Compares an array of pafUserSelections to see if they are equal.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <returns></returns>
        public static bool IsEqual(pafUserSelection[] x1, pafUserSelection[] x2)
        {
            if (x1.Length != x2.Length) return false;

            return !x1.Where((t, i) => !IsEqual(t, x2[i])).Any();
        }

        /// <summary>
        /// Compares a single pafUserSelection to see if it is equal.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <returns></returns>
        public static bool IsEqual(pafUserSelection x1, pafUserSelection x2)
        {

            if (!x1.id.Equals(x2.id))
            {
                return false;
            }
            if(!x1.dimension.Equals(x2.dimension))
            {
                return false;
            }
            if(x1.multiples != x2.multiples)
            {
                return false;
            }
            if (!pafAxisExtension.IsEqual(x1.pafAxis, x2.pafAxis))
            {
                return false;
            }
            if (!x1.promptString.Equals(x2.promptString))
            {
                return false;
            }

            return x1.values.All(t => t == x2.values[0]);
        }
    }
}
