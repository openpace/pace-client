﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using PafServerStressTester.PafServiceLocal;
using Titan.Palladium.GridView;

namespace PafServerStressTester
{
    public static class PafViewSectionExtension
    {
        /// <summary>
        /// Populates the data in the grid.
        /// </summary>
        /// <param name="viewSection">The source data to place in the grid.</param>
        /// <param name="rowBlanks">List of blank row ordinals</param>
        /// <param name="columnBlanks">List of blank column ordinals.</param>
        /// <param name="startCell">Calculated start cell.</param>
        public static double[,] PopulateData(this pafViewSection viewSection, List<int> rowBlanks, List<int> columnBlanks, out CellAddress startCell )
        {
            bool isBlankRow = false;
            bool isBlankCol = false;
            long rows = 0;
            long cols = 0;
            double[,] dataArray = null;
            int startRow = 4;

            if (viewSection.pafViewHeaders != null)
            {
                if (viewSection.pafViewHeaders.Length > 2)
                    startRow = startRow + (viewSection.pafViewHeaders.Length - 2);
            }

            startCell = new CellAddress(startRow, 1);

            int curRow = startCell.Row + viewSection.colAxisDims.Length + 1;
            int curCol = startCell.Col + viewSection.rowAxisDims.Length + 1;

            int blankRows = rowBlanks.Count;
            int blankCols = columnBlanks.Count;
            int i = 0;

            dataArray = new double[(viewSection.pafDataSlice.data.Length / viewSection.pafDataSlice.columnCount) + blankRows,
            viewSection.pafDataSlice.columnCount + blankCols];

            while (i < viewSection.pafDataSlice.data.Length)
            {
                if (rowBlanks.Contains((int)rows))
                {
                    for (int col = 0; col < viewSection.pafDataSlice.columnCount + blankCols; col++)
                    {
                        dataArray[rows, col] = 0;
                        cols++; //increment columns 
                    }
                    isBlankRow = true;
                }

                //the column blank has already been added if the row was also blank
                if (isBlankRow == false && columnBlanks.Contains((int)cols))
                {
                    dataArray[rows, cols] = 0;

                    isBlankCol = true;
                }

                //if no row/col blanks then add the data value to the data array
                if (isBlankRow == false && isBlankCol == false)
                {
                    dataArray[rows, cols] = (double)viewSection.pafDataSlice.data.GetValue(i);
                    //increment if not a blank row/col to pull next dataslice data value
                    i++;
                }

                if (cols == viewSection.pafDataSlice.columnCount - 1 + blankCols || isBlankRow)
                {
                    cols = 0;
                    curCol = 1 + viewSection.rowAxisDims.Length + 1;
                    rows++; //increment rows
                    curRow++;
                }
                else
                {
                    cols++; //increment columns
                    curCol++;
                }

                isBlankRow = false;
                isBlankCol = false;
            }
            return dataArray;
        }

    }
}
