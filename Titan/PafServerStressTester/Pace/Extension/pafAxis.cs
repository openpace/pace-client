﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using Titan.PafService;

namespace PafServerStressTester
{
    public static class pafAxisExtension
    {
        /// <summary>
        /// Compares a single pafAxis to see if it is equal.
        /// </summary>
        /// <param name="x1"></param>
        /// <param name="x2"></param>
        /// <returns></returns>
        public static bool IsEqual(pafAxis x1, pafAxis x2)
        {
            if(x1.colAxis != x2.colAxis)
            {
                return false;
            }
            if (x1.pageAxis != x2.pageAxis)
            {
                return false;
            }
            if (x1.rowAxis != x2.rowAxis)
            {
                return false;
            }
            if (x1.value != x2.value)
            {
                return false;
            }

            return true;
        }
    }
}
