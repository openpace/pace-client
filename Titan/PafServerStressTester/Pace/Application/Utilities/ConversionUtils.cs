#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using PafServerStressTester.PafServiceLocal;

namespace PafServerStressTester.Pace.Application.Utilities
{
    class ConversionUtils
    {
        public static pafUserSelection[] ConvertPafUserSels(Titan.PafService.pafUserSelection[] pafUserSels)
        {
            pafUserSelection[] ret = null;
            if (pafUserSels != null && pafUserSels.Length > 0)
            {
                ret = new pafUserSelection[pafUserSels.Length];
                for (int i = 0; i < pafUserSels.Length; i++)
                {
                    ret[i] = new pafUserSelection();

                    ret[i].dimension = pafUserSels[i].dimension;

                    ret[i].id = pafUserSels[i].id;

                    ret[i].pafAxis = new pafAxis
                     {
                         colAxis = pafUserSels[i].pafAxis.colAxis,
                         pageAxis = pafUserSels[i].pafAxis.pageAxis,
                         rowAxis = pafUserSels[i].pafAxis.rowAxis,
                         value = pafUserSels[i].pafAxis.value
                     };

                    ret[i].promptString = pafUserSels[i].promptString;

                    ret[i].values = new string[pafUserSels[i].values.Length];
                    for (int j = 0; j < pafUserSels[i].values.Length; j++)
                    {
                        ret[i].values[j] = pafUserSels[i].values[j];
                    }
                }
            }

            return ret;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="scl"></param>
        /// <returns></returns>
        public static simpleCoordList ConvertSimpleCoordList(Titan.PafService.simpleCoordList scl)
        {
            simpleCoordList ret = null;

            if (scl != null && scl.axis != null && scl.axis.Length > 0)
            {
                ret = new simpleCoordList();
                ret.axis = new string[scl.axis.Length];
                scl.axis.CopyTo(ret.axis, 0);

                ret.coordinates = new string[scl.coordinates.Length];
                scl.coordinates.CopyTo(ret.coordinates, 0);
            }
            if (scl != null && scl.compressed)
            {
                ret = new simpleCoordList();
                if (!string.IsNullOrEmpty(scl.compressedData))
                {
                    ret.compressedData = new string(scl.compressedData.ToCharArray());
                    ret.compressedStringTable = new string(scl.compressedStringTable.ToCharArray());
                    ret.compressed = true;
                }
            }

            return ret;
        }

        public static Titan.PafService.pafPlannerConfig ConvertPafPlannerConfig(pafPlannerConfig plannerConfig)
        {
            Titan.PafService.pafPlannerConfig ppc = new Titan.PafService.pafPlannerConfig();

            ppc.autoRunOnSaveMenuItemNames = plannerConfig.autoRunOnSaveMenuItemNames;
            ppc.calcElapsedPeriods = plannerConfig.calcElapsedPeriods;
            ppc.cycle = plannerConfig.cycle;
            
            ppc.defaultEvalEnabled = plannerConfig.defaultEvalEnabled;
            ppc.defaultEvalEnabledSpecified = plannerConfig.defaultEvalEnabledSpecified;
            
            ppc.defaultEvalEnabledWorkingVersion = plannerConfig.defaultEvalEnabledWorkingVersion;
            
            if(plannerConfig.defaultEvalRefVersions != null)
            {
                ppc.defaultEvalRefVersions = new string[plannerConfig.defaultEvalRefVersions.Length];
                plannerConfig.defaultEvalRefVersions.CopyTo(ppc.defaultEvalRefVersions, 0);
            }

            ppc.defaultRulesetName = plannerConfig.defaultRulesetName;
            
            ppc.isDataFilteredUow = plannerConfig.isDataFilteredUow;
            ppc.isDataFilteredUowSpecified = plannerConfig.isDataFilteredUowSpecified;
            ppc.isUserFilteredUow = plannerConfig.isUserFilteredUow;
            ppc.isUserFilteredUowSpecified = plannerConfig.isUserFilteredUowSpecified;

            ppc.mdbSaveWorkingVersionOnUowLoad = plannerConfig.mdbSaveWorkingVersionOnUowLoad;

            if(plannerConfig.menuItemNames != null)
            {
                ppc.menuItemNames = new string[plannerConfig.menuItemNames.Length];
                plannerConfig.menuItemNames.CopyTo(ppc.menuItemNames, 0);
            }

            ppc.replicateAllEnabled = plannerConfig.replicateAllEnabled;
            ppc.replicateAllEnabledSpecified = plannerConfig.replicateAllEnabledSpecified;
            ppc.replicateEnabled = plannerConfig.replicateEnabled;
            ppc.replicateEnabledSpecified = plannerConfig.replicateEnabledSpecified;

            ppc.role = plannerConfig.role;


            
            if(plannerConfig.ruleSetNames != null)
            {
                ppc.ruleSetNames = new string[plannerConfig.ruleSetNames.Length];
                plannerConfig.ruleSetNames.CopyTo(ppc.ruleSetNames, 0);
            }

            ppc.uowSizeLarge = plannerConfig.uowSizeLarge;
            ppc.uowSizeLargeSpecified = plannerConfig.uowSizeLargeSpecified;
            ppc.uowSizeMax = plannerConfig.uowSizeMax;
            ppc.uowSizeMaxSpecified = plannerConfig.uowSizeMaxSpecified;

            if (plannerConfig.userFilterSpec != null)
            {
                ppc.userFilterSpec = new string[plannerConfig.userFilterSpec.Length];
                plannerConfig.userFilterSpec.CopyTo(ppc.userFilterSpec, 0);
            }

            if (plannerConfig.versionFilter != null)
            {
                ppc.versionFilter = new string[plannerConfig.versionFilter.Length];
                plannerConfig.versionFilter.CopyTo(ppc.versionFilter, 0);
            }

            if (plannerConfig.viewTreeItemNames != null)
            {
                ppc.viewTreeItemNames = new string[plannerConfig.viewTreeItemNames.Length];
                plannerConfig.viewTreeItemNames.CopyTo(ppc.viewTreeItemNames, 0);
            }

            return ppc;
        }

        public static pafDimSpec[] ConvertPafDimSpec(Titan.PafService.pafDimSpec[] dimSpec)
        {
            pafDimSpec[] pds = null;
            if (dimSpec != null)
            {
                pds = new pafDimSpec[dimSpec.Length];
                int i = 0;
                foreach (Titan.PafService.pafDimSpec s in dimSpec)
                {
                    pds[i] = new pafDimSpec();

                    pds[i].dimension = s.dimension;

                    if (s.expressionList != null)
                    {
                        pds[i].expressionList = new string[s.expressionList.Length];
                        s.expressionList.CopyTo(pds[i].expressionList, 0);
                    }

                    //pds[i].selectable = s.selectable;

                    i++;
                }
            }
            return pds;
        }
    }
}