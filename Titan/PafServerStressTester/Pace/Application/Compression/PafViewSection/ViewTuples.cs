#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using PafServerStressTester.PafServiceLocal;
using Titan.Pace.Application.Compression;

namespace PafServerStressTester.Pace.Application.Compression.PafViewSection
{
    internal class ViewTuples : IPafCompressedObj
    {
        private readonly viewTuple[] _tuples;

        #region IPafCompressedObj Members

        public ViewTuples(viewTuple[] tuples)
        {
            _tuples = tuples;
        }

        public bool isCompressed()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void setCompressed(bool isCompressed)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        public void compressData()
        {
            if (_tuples != null && _tuples.Length > 0)
            {
                foreach (viewTuple scn in _tuples)
                {
                    if (!scn.compressed)
                    {
                        ViewTuple pscn = new ViewTuple(scn);
                        pscn.compressData();
                    }
                }
            }
        }

        public void uncompressData()
        {
            if (_tuples != null && _tuples.Length > 0)
            {
                foreach (viewTuple scn in _tuples)
                {
                    if (scn.compressed)
                    {
                        ViewTuple pscn = new ViewTuple(scn);
                        pscn.uncompressData();
                    }
                }
            }
        }

        #endregion
    }
}
