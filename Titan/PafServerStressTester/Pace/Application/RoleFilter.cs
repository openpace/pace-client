#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using PafServerStressTester.Pace.Application.Utilities;
using PafServerStressTester.PafServiceLocal;
using Titan.Palladium.TestHarness;

namespace PafServerStressTester.Pace.Application
{
    public class RoleFilter
    {
        private pafPlannerRole[] _roles;
        private pafPlannerConfig[] _plannerConfigs;
        private readonly Dictionary<string, List<pafPlannerConfig>> _pafPlannerConfigs = new Dictionary<string, List<pafPlannerConfig>>();

        /// <summary>
        /// Perform a UOW cell check and if it passes, start a planning session.
        /// </summary>
        /// <param name="pafPlannerConfig">pafPlannerConfig of the user.</param>
        /// <param name="userId">Current user id.</param>
        /// <param name="roleId">Role id.</param>
        /// <param name="seasonId">season id.</param>
        /// <param name="rt">The recorded test to calculate the uow for.</param>
        /// <param name="app">The Paf application object.</param>
        /// <returns>true or false</returns>
        private void CalculateUowTestHarness(pafPlannerConfig pafPlannerConfig,
            string userId, string roleId, string seasonId, RecordedTest rt, PafApp app)
        {
            bool isMaxUow;
            bool isLargeUow;

            //isDataFilteredUow is AKA: invlid intersections
            //isUserFilteredUow is AKA: Role Filter.
            if (rt.RoleFilterEnabled && rt.InvalidIntersection ||
                rt.RoleFilterEnabled && !rt.InvalidIntersection &&
                rt.RoleFilterSelections != null)
            {

                //get the data filtered UOW value
                bool dataFilUow = rt.InvalidIntersection;
                bool filterSub = rt.FilteredSubtotals;

                //Popululate the role filter
                app.PopulateRoleFilter(roleId, seasonId, dataFilUow, filterSub);

                //repopulate the Role Filter, to workaround a server bug.  
                //where it does not see to remember the role filter.
                app.PopulateRoleFilter(roleId, seasonId, dataFilUow, filterSub);

                //Get the Filtered UOW size.
                app.GetFilteredUowSize(
                    roleId,
                    seasonId,
                    dataFilUow,
                    filterSub,
                    ConversionUtils.ConvertPafDimSpec(rt.RoleFilterSelections));

                //Check the UOW size.
                DialogResult dr = performUowCellCheck(pafPlannerConfig, out isLargeUow, out isMaxUow, app);


                //if the result is ok, then create the planning session.
                if (dr == DialogResult.OK)
                {
                    app.CreatePlanningSession(roleId, seasonId, dataFilUow, filterSub);
                }
                else //if not reshow the previous screen.
                {
                    CalculateUowTestHarness(pafPlannerConfig, userId, roleId, seasonId, rt, app);
                }

            }
            else if (!rt.RoleFilterEnabled && rt.InvalidIntersection)
            {
                //create a dim spec.
                pafDimSpec[] pafDimSpec = new pafDimSpec[0];

                //get the data filtered UOW value
                bool dataFilUow = rt.InvalidIntersection;
                bool filteredSubs = rt.FilteredSubtotals;

                //Get the Filtered UOW size.
                app.GetFilteredUowSize(roleId, seasonId, dataFilUow, filteredSubs, pafDimSpec);

                //Check the UOW size.
                DialogResult dr = performUowCellCheck(pafPlannerConfig, out isLargeUow, out isMaxUow, app);

                //if the result is ok, then create the planning session.
                if (dr == DialogResult.OK)
                {
                    app.CreatePlanningSession(roleId, seasonId, filteredSubs, dataFilUow);
                }
            }
            else if (!rt.RoleFilterEnabled && !rt.InvalidIntersection)
            {
                int? large;
                int? max;
                //get the min and max values, from the planner conf and global values.
                GetCellMinMax(pafPlannerConfig, out large, out max, app);

                bool filteredSubtotals = rt.FilteredSubtotals;

                //check for null min and max values
                //if so, just start a planning session...
                if (large == null && max == null)
                {
                    app.CreatePlanningSession(roleId, seasonId, false, filteredSubtotals);
                }
                else
                {
                    //create a dim spec.
                    pafDimSpec[] pafDimSpec = new pafDimSpec[0];

                    //Get the Filtered UOW size.
                    app.GetFilteredUowSize(roleId, seasonId, false, filteredSubtotals, pafDimSpec);

                    //Check the UOW size.
                    DialogResult resp = performUowCellCheck(pafPlannerConfig, out isLargeUow, out isMaxUow, app);

                    //if the result is ok, then create the planning session.
                    if (resp == DialogResult.OK)
                    {
                        app.CreatePlanningSession(roleId, seasonId, false, filteredSubtotals);
                    }
                }
            }
        }

        /// <summary>
        /// Perform a UOW cell check.  No planning session is created.
        /// </summary>
        /// <param name="pafPlannerConfig">pafPlannerConfig of the user.</param>
        /// <param name="isLargeUow"></param>
        /// <param name="isMaxUow"></param>
        /// <returns>DialogResult.OK if cell count is within the criteria, DialogResult.No
        /// if the cell count is outside the criteria.</returns>
        private DialogResult performUowCellCheck(pafPlannerConfig pafPlannerConfig,
            out bool isLargeUow, out bool isMaxUow, PafApp app)
        {
            isLargeUow = false;
            isMaxUow = false;
            try
            {
                int? large;
                int? max;
                long? uowResSize = 0;
                DialogResult res = DialogResult.OK;

                //get the min and max values, from the planner conf and global values.
                GetCellMinMax(pafPlannerConfig, out large, out max, app);

                //check for null min and max values
                //if so, return without prompting...
                if (large == null && max == null)
                {
                    return res;
                }

                //check the UOW size response
                if (app.GetPafGetFilteredUOWSizeResponse() != null)
                {
                    if (app.GetPafGetFilteredUOWSizeResponse().uowCellCountSpecified)
                    {
                        uowResSize = app.GetPafGetFilteredUOWSizeResponse().uowCellCount;
                    }
                }

                //perform the "Zero" check.
                if (uowResSize == 0)
                {

                    //user can only choose cancel...
                    return DialogResult.No;
                }

                //perform the "Large" check.
                if (uowResSize >= large && uowResSize <= max)
                {
                    return DialogResult.OK;
                }

                //perform the "Maximum" check.
                if (uowResSize >= max)
                {
                    return DialogResult.No;
                }

                return res;
            }
            catch (Exception)
            {
                return DialogResult.No;
            }
        }

        /// <summary>
        /// Gets the cell large and max values.
        /// </summary>
        /// <param name="pafPlannerConfig">pafPlannerConfig of the user.</param>
        /// <param name="large">Large value.</param>
        /// <param name="max">Max value.</param>
        /// <param name="app">The Paf application object.</param>
        private void GetCellMinMax(pafPlannerConfig pafPlannerConfig, out int? large, out int? max, PafApp app)
        {
            try
            {
                large = null;
                max = null;
                int? glarge = null;
                int? gmax = null;

                int? rlarge = pafPlannerConfig.uowSizeLargeSpecified == false ? null : (int?)pafPlannerConfig.uowSizeLarge;
                int? rmax = pafPlannerConfig.uowSizeMaxSpecified == false ? null : (int?)pafPlannerConfig.uowSizeMax;

                if (app.GetPafAppSettings() != null)
                {
                    glarge = app.GetPafAppSettings().globalUowSizeLargeSpecified == false ? null : (int?)app.GetPafAppSettings().globalUowSizeLarge;
                    gmax = app.GetPafAppSettings().globalUowSizeMaxSpecified == false ? null : (int?)app.GetPafAppSettings().globalUowSizeMax;
                }

                if (rlarge != null)
                {
                    large = rlarge;
                }
                else
                {
                    if (glarge != null)
                    {
                        large = glarge;
                    }
                }

                if (rmax != null)
                {
                    max = rmax;
                }
                else
                {
                    if (gmax != null)
                    {
                        max = gmax;
                    }
                }
            }
            catch (Exception)
            {
                large = null;
                max = null;
            }
        }

        /// <summary>
        /// Gets a plan type if one does not exist.
        /// </summary>
        private void getClientAuth(PafApp app)
        {
            _roles = app.GetPafAuthResponse().plannerRoles;
            _plannerConfigs = app.GetPafAuthResponse().plannerConfigs;
        }

        /// <summary>
        /// Creates a role filter uow.
        /// </summary>
        /// <param name="userId">The user to logon to the server.</param>
        /// <param name="role">The current role.</param>
        /// <param name="process">THe current process.</param>
        /// <param name="rt">The recorded test to build the role filter from.</param>
        /// <param name="app">The Paf application object.</param>
        public void CreateUOW(string userId, string role, string process, RecordedTest rt, PafApp app)
        {
            //get the pafPlannerConfig
            pafPlannerConfig ppc = FindPlannerConfig(role, process);

            if (rt.RoleFilterEnabled)
            {
                //Remove the cached user selections.

                app.GetReinitClientState();
                ppc = FindPlannerConfig(role, process);

                CalculateUowTestHarness(
                       ppc,
                       userId,
                       role,
                       process,
                       rt,
                       app);

            }
            else
            {
                if (app.GetPafPlanSessionResponse() == null || app.GetPafPlanSessionResponse().dimTrees == null)
                {
                    CalculateUowTestHarness(
                       ppc,
                       userId,
                       role,
                       process,
                       rt,
                       app);

                }
            }
        }

        /// <summary>
        /// Procedure to add roles to the cboPlanType list box, or txtPlanType textbox.
        /// </summary>
        public void AddRoles(PafApp app)
        {
            getClientAuth(app);

            if (_pafPlannerConfigs != null)
            {
                _pafPlannerConfigs.Clear();
            }


            if (_roles == null)
                return;


            if (_pafPlannerConfigs != null && _plannerConfigs.Length > 0)
            {
                foreach (pafPlannerConfig str in _plannerConfigs)
                {
                    if (str != null)
                    {
                        if (_pafPlannerConfigs.ContainsKey(str.role))
                        {
                            List<pafPlannerConfig> tmp = _pafPlannerConfigs[str.role];
                            tmp.Add(str);
                            _pafPlannerConfigs[str.role] = tmp;
                        }
                        else
                        {
                            List<pafPlannerConfig> tmp = new List<pafPlannerConfig>();
                            tmp.Add(str);
                            _pafPlannerConfigs.Add(str.role, tmp);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Finds the current pafPlannerConfig to use.
        /// </summary>
        /// <param name="role">Current role selected by the user.</param>
        /// <param name="season">Current season/process selected by the user.</param>
        /// <returns>PafPlannerConf or null.</returns>
        private pafPlannerConfig FindPlannerConfig(string role, string season)
        {
            try
            {
                pafPlannerRole r = GetPafPlannerRole(role);
                season s = GetSeason(r, season);

                if (r != null && s != null)
                {
                    return FindPlannerConfig(r, s);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Finds the current pafPlannerConfig to use.
        /// </summary>
        /// <param name="role">Current role selected by the user.</param>
        /// <param name="season">Current season/process selected by the user.</param>
        /// <returns>PafPlannerConf or null.</returns>
        private pafPlannerConfig FindPlannerConfig(pafPlannerRole role, season season)
        {
            try
            {
                List<pafPlannerConfig> lst;
                if (_pafPlannerConfigs.TryGetValue(role.roleName, out lst))
                {
                    if (lst != null)
                    {
                        foreach (pafPlannerConfig ppc in lst)
                        {
                            if (role.roleName.Equals(ppc.role) && season.planCycle.Equals(ppc.cycle))
                            {
                                return ppc;
                            }
                        }
                        //in this case we are checking for the desfault instace, 
                        //the default instace will have an empty plancycle.
                        // if (lst.Count == 1)
                        foreach (pafPlannerConfig ppc in lst)
                        {
                            if (ppc.cycle == null || ppc.cycle.Equals(String.Empty))
                            {
                                return ppc;
                            }
                        }
                    }
                }
                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        private pafPlannerRole GetPafPlannerRole(string role)
        {
            return _roles.Where(r => !String.IsNullOrEmpty(r.roleName)).FirstOrDefault(r => role.ToLower().Equals(r.roleName.ToLower()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="season"></param>
        /// <returns></returns>
        private season GetSeason(pafPlannerRole role, string season)
        {
            return role.seasons.Where(s => !String.IsNullOrEmpty(s.id)).FirstOrDefault(s => season.ToLower().Equals(s.id.ToLower()));
        }
    }
}
