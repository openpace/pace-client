#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Net;
using System.Reflection;
using System.Security.Principal;
using System.Text;
using System.Web.Services.Protocols;
using PafServerStressTester.PafServiceLocal;
using PafServerStressTester.Properties;
using ServerStressTesterBase.Pace.LogUtils;

namespace PafServerStressTester.Pace.Application
{
    /// <summary>
    /// PafApp class, contains members for use by all classes.
    /// </summary>
    public class PafApp
    {
        private PafServiceProviderService _PafService;
        private pafAuthResponse _PafAuthResponse;
        private pafPlanSessionResponse _PafPlanSessionResponse;
        private PafServiceMngr _ServiceMngr;
        private pafGetFilteredUOWSizeResponse _PafGetFilteredUOWSizeResponse;
        private Logging _Logger;
        private string _ClientID = "";
        private string _Platform = "";
        private string _ServerVersion = "";


        public PafApp()
        {
        }

        /// <summary>
        /// Starts the communication between the client and the server.
        /// </summary>
        private void Init()
        {
            DateTime startTime = DateTime.Now;
            clientInitRequest initReq = null;

            initReq = new clientInitRequest();
            initReq.clientType = "PafServerStressTester";
            initReq.clientVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            initReq.ipAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();

            PafServiceProviderService pafsvc = GetPafService();

            pafServerAck ack = pafsvc.clientInit(initReq);

            _ClientID = ack.clientId;
            _Platform = ack.platform;

            GetLogger().InfoFormat("Client ID: {0}", ack.clientId);

            if(pafsvc.CookieContainer != null && Settings.Default.PrintCookieInformation)
            {
                PrintCookieDetailedInformation();
            }
            GetLogger().Debug("clientInit runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");

        }

        public void PrintServerInformation()
        {
            DateTime startTime = DateTime.Now;
            clientInitRequest initReq = null;

            initReq = new clientInitRequest();
            initReq.clientType = "PafServerStressTester";
            initReq.clientVersion = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            initReq.ipAddress = Dns.GetHostEntry(Dns.GetHostName()).AddressList[0].ToString();

            PafServiceProviderService pafsvc = GetPafService();

            pafServerAck ack = pafsvc.clientInit(initReq);

            _ClientID = ack.clientId;
            _Platform = ack.platform;

            GetLogger().InfoFormat("URL: {0}", Settings.Default.PafServerStressTester_PafServiceLocal_PafService);

            GetLogger().InfoFormat("Platform: {0}", ack.platform);

            GetLogger().InfoFormat("Server Version: {0}", ack.serverVersion);

            GetLogger().InfoFormat("Machine Name: {0}", Dns.GetHostName());

            GetLogger().InfoFormat("Host Name: {0}", Dns.GetHostEntry(Dns.GetHostName()).HostName);

            GetLogger().InfoFormat("User Name: {0}", WindowsIdentity.GetCurrent().Name);

            GetLogger().InfoFormat("Auth Mode: {0}", ack.authMode);
            

            if (pafsvc.CookieContainer != null && Settings.Default.PrintCookieInformation)
            {
                PrintCookieDetailedInformation();
            }

            GetLogger().Debug("clientInit runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
        }

        public void PrintCookieDetailedInformation()
        {
            PafServiceProviderService pafsvc = GetPafService();

            if (pafsvc.CookieContainer != null)
            {
                GetLogger().Info("Cookie(s) Found count: " + pafsvc.CookieContainer.Count);

                Uri u = new Uri(Settings.Default.PafServerStressTester_PafServiceLocal_PafService);

                CookieCollection cc = pafsvc.CookieContainer.GetCookies(u);

                if (cc != null && cc.Count > 0)
                {
                    foreach (Cookie cookie in cc)
                    {
                        //GetLogger().Info(c.GetCookieInformation());
                        StringBuilder sb = new StringBuilder();
                        if (cookie != null)
                        {
                            if (cookie.Name != null) sb.Append("Cookie Name: " + cookie.Name);
                            if (cookie.Value != null) sb.Append(", Cookie Value: " + cookie.Value);
                            if (cookie.Comment != null) sb.Append(", Cookie Comment: " + cookie.Comment);
                            if (cookie.CommentUri != null) sb.Append(", Cookie Comment URI: " + cookie.CommentUri);
                            sb.Append(", Cookie Discard: " + cookie.Discard);
                            if (cookie.Domain != null) sb.Append(", Cookie Domain: " + cookie.Domain);
                            sb.Append(", Cookie Expired: " + cookie.Expired);
                            sb.Append(", Cookie Expires: " + cookie.Expires);
                            sb.Append(", Cookie HttpOnly: " + cookie.HttpOnly);
                            if (cookie.Port != null) sb.Append(", Cookie Port: " + cookie.Port);
                            sb.Append(", Cookie Secure: " + cookie.Secure);
                            sb.Append(", Cookie TimeStamp: " + cookie.TimeStamp);
                            sb.Append(", Cookie Version: " + cookie.Version);
                        }
                        //return sb.ToString();
                        GetLogger().Info(sb.ToString());
                    }
                }
            }
        }

        public string GetCookieInfo(bool print)
        {
            if (!print) return String.Empty;
            PafServiceProviderService pafsvc = GetPafService();
            StringBuilder sb  = new StringBuilder();
            if (pafsvc.CookieContainer != null)
            {

                Uri u = new Uri(Settings.Default.PafServerStressTester_PafServiceLocal_PafService);

                CookieCollection cc = pafsvc.CookieContainer.GetCookies(u);

                if (cc != null && cc.Count > 0)
                {
                    foreach (Cookie cookie in cc)
                    {
                        //sb.AppendLine(c.GetCookieInformation());
                        //StringBuilder sb = new StringBuilder();
                        if (cookie != null)
                        {
                            if (cookie.Name != null) sb.AppendLine("Cookie Name: " + cookie.Name);
                            if (cookie.Value != null) sb.AppendLine(", Cookie Value: " + cookie.Value);
                            if (cookie.Comment != null) sb.AppendLine(", Cookie Comment: " + cookie.Comment);
                            if (cookie.CommentUri != null) sb.AppendLine(", Cookie Comment URI: " + cookie.CommentUri);
                            sb.AppendLine(", Cookie Discard: " + cookie.Discard);
                            if (cookie.Domain != null) sb.AppendLine(", Cookie Domain: " + cookie.Domain);
                            sb.AppendLine(", Cookie Expired: " + cookie.Expired);
                            sb.AppendLine(", Cookie Expires: " + cookie.Expires);
                            sb.AppendLine(", Cookie HttpOnly: " + cookie.HttpOnly);
                            if (cookie.Port != null) sb.AppendLine(", Cookie Port: " + cookie.Port);
                            sb.AppendLine(", Cookie Secure: " + cookie.Secure);
                            sb.AppendLine(", Cookie TimeStamp: " + cookie.TimeStamp);
                            sb.AppendLine(", Cookie Version: " + cookie.Version);
                        }
                    }
                }
            }
            return sb.ToString();
        }

        /// <summary>
        /// Authenticates the user with the PafServer
        /// </summary>
        /// <param name="username">The userid to be sent to the server.</param>
        /// <param name="passwordHash">The password to be sent to the server.</param>
        /// <param name="domain">The users domain (if LDAP is used)</param>
        /// <param name="IV">The IV (if LDAP is used)</param>
        /// <param name="password"></param>
        /// <param name="sid">The users unique sid (if LDAP is used)</param>
        public void SendClientAuth(string username, string passwordHash, string domain, string IV, string password, string sid)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                Init();

                PafServiceProviderService pafsvc = null;

                pafAuthRequest initAuth = new pafAuthRequest();

                GetLogger().Debug("Send Client Auth-Client Id: " + _ClientID);


                initAuth.username = username;
                initAuth.passwordHash = password;
                initAuth.clientId = _ClientID;
                initAuth.domain = domain;
                initAuth.IV = IV;
                initAuth.password = password;
                initAuth.sid = sid;


                pafsvc = GetPafService();

                pafAuthResponse auth = new pafAuthResponse();
                
                try
                {
                    auth = pafsvc.clientAuth(initAuth);
                }
                catch(System.Web.Services.Protocols.SoapException)
                {
                    throw;
                }
                catch (Exception)
                {
                    throw;
                }

                if (auth != null)
                {
                    if (auth.securityToken.valid)
                    {
                        _PafAuthResponse = auth;
                        //GetLogger().Info("Session Token: " + auth.securityToken.sessionToken);
                        GetLogger().Info("User Name: " + auth.securityToken.userName);
                    }
                    else
                    {
                        _PafAuthResponse = auth;
                        throw new System.Security.SecurityException(auth.securityToken.sessionToken); 
                    }
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
            finally
            {
                GetLogger().Debug("pafAuthRequest(SendClientAuth) runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }

        /// <summary>
        /// Get a Planning Session Response (which contain PafTrees)
        /// </summary>
        /// <returns>Nothing</returns>
        /// <param name="role">The users role.</param>
        /// <param name="seasonId">The season/process selected by the user.</param>
        /// <param name="invalidInxSuppSel"></param>
        /// <param name="filteredSubtotalsSelected"></param>
        public void CreatePlanningSession(string role, string seasonId, bool invalidInxSuppSel, bool filteredSubtotalsSelected)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                PafServiceProviderService pafsvc = null;

                GetLogger().Debug("Create Planning Session-Client Id: " + _ClientID);

                pafPlanSessionRequest planSession = new pafPlanSessionRequest();
                planSession.clientId = _ClientID;
                planSession.isInvalidIntersectionSuppressionSelected = invalidInxSuppSel;
                planSession.selectedRole = role;
                planSession.seasonId = seasonId;
                planSession.filteredSubtotalsSelected = filteredSubtotalsSelected;
                planSession.sessionToken = _PafAuthResponse.securityToken.sessionToken;
                planSession.compressResponse = true;

                pafsvc = GetPafService();

                Program.WriteLineToConsole("Starting planning session with cid: " + _ClientID,
                    Settings.Default.GenerateLogFile,
                    false);

                pafPlanSessionResponse resp = pafsvc.startPlanSession(planSession);

                if (resp != null)
                {
                    _PafPlanSessionResponse = resp;
                }
            }
            catch (System.Web.Services.Protocols.SoapException)
            {
                _PafPlanSessionResponse = null;
                throw;
            }
            catch (Exception)
            {
                _PafPlanSessionResponse = null;
                throw;
            }
            finally
            {
                GetLogger().Debug("startPlanSession runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }

        public void GetReinitClientState()
        {
            DateTime startTime = DateTime.Now;
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                GetLogger().Debug("Reinit Client State-Client Id: " + _ClientID);

                pafRequest reinitClientState = new pafRequest();
                reinitClientState.clientId = _ClientID;
                reinitClientState.sessionToken = _PafAuthResponse.securityToken.sessionToken;

                pafResponse resp = pafsvc.reinitializeClientState(reinitClientState);

            }
            catch (SoapException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                GetLogger().Debug("reinitializeClientState runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }

        /// <summary>
        /// Populate the role filter.
        /// </summary>
        /// <param name="role">The users role.</param>
        /// <param name="seasonId">The season/process selected by the user.</param>
        /// <param name="invalidInxSuppSel">Invalid Intersection Suppression Selected</param>
        /// <param name="filteredSubtotalsSelected"></param>
        public void PopulateRoleFilter(string role, string seasonId, bool invalidInxSuppSel, bool filteredSubtotalsSelected)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                GetLogger().Debug("Populate Role Filter-Client Id: " + _ClientID);

                pafPlanSessionRequest roleFilter = new pafPlanSessionRequest();
                roleFilter.clientId = _ClientID;
                roleFilter.selectedRole = role;
                roleFilter.seasonId = seasonId;
                roleFilter.isInvalidIntersectionSuppressionSelected = invalidInxSuppSel;
                roleFilter.filteredSubtotalsSelected = filteredSubtotalsSelected;
                roleFilter.sessionToken = _PafAuthResponse.securityToken.sessionToken;
                roleFilter.compressResponse = true;

                pafPopulateRoleFilterResponse resp = pafsvc.populateRoleFilters(roleFilter);

                if (resp != null)
                {
                    //Expand(resp.dimTrees);
                    //_PafPopulateRoleFilterResponse = resp;
                }
            }
            catch (SoapException)
            {
                throw;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                GetLogger().Debug("populateRoleFilters runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }

        /// <summary>
        /// Get the filtered Uow size.
        /// </summary>
        /// <param name="role">The users role.</param>
        /// <param name="seasonId">The season/process selected by the user.</param>
        /// <param name="invalidInxSuppSel">Invalid Intersection Suppression Selected</param>
        /// <param name="filteredSubtotalsSelected">Filtered subtotals selected</param>
        /// <param name="pafUserSelections">Paf user selections.</param>
        public void GetFilteredUowSize(string role, string seasonId,
            bool invalidInxSuppSel,  bool filteredSubtotalsSelected, pafDimSpec[] pafUserSelections)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                PafServiceProviderService pafsvc = GetPafService();

                pafGetFilteredUOWSizeRequest filteredUOWSize = new pafGetFilteredUOWSizeRequest();
                filteredUOWSize.clientId = _ClientID;
                filteredUOWSize.isInvalidIntersectionSuppressionSelected = invalidInxSuppSel;
                filteredUOWSize.pafUserSelections = pafUserSelections;
                filteredUOWSize.selectedRole = role;
                filteredUOWSize.seasonId = seasonId;
                filteredUOWSize.filteredSubtotalsSelected = filteredSubtotalsSelected;
                filteredUOWSize.sessionToken = _PafAuthResponse.securityToken.sessionToken;

                pafGetFilteredUOWSizeResponse resp = pafsvc.getFilteredUOWSize(filteredUOWSize);

                if (resp != null)
                {
                    _PafGetFilteredUOWSizeResponse = resp;
                }
            }
            catch (SoapException)
            {
                _PafGetFilteredUOWSizeResponse = null;
                throw;
            }
            catch (Exception)
            {
                _PafGetFilteredUOWSizeResponse = null;
                throw;
            }
            finally
            {
                GetLogger().Debug("getFilteredUOWSize runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public void EndPlanningSession(bool logMessages = true)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                // If a session exists
                if (_PafAuthResponse != null && _PafAuthResponse.securityToken != null &&
                    _PafAuthResponse.securityToken.sessionToken != null)
                {

                    PafServiceProviderService pafsvc = GetPafService();

                    if(logMessages) GetLogger().Info("End Planning Session-Client Id: " + _ClientID);

                    pafRequest planSession = new pafRequest();
                    planSession.clientId = ClientId;
                    planSession.sessionToken = _PafAuthResponse.securityToken.sessionToken;

                    try
                    {
                        pafsvc.endPlanningSessionAsync(planSession);
                    }
                    catch
                    {
                        //igrnore...
                    }
                    _PafPlanSessionResponse = null;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (logMessages) GetLogger().Debug("endPlanningSession runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }

        /// <summary>
        /// Gets the global PafAuthResponse, if one does not exist then it is created.
        /// </summary>
        /// <returns>PafAuthResponse</returns>
        public pafAuthResponse GetPafAuthResponse()
        {
            if (_PafAuthResponse == null)
            {
                _PafAuthResponse = new pafAuthResponse();
            }
            return _PafAuthResponse;
        }

        /// <summary>
        /// Gets the global pafGetFilteredUOWSizeResponse
        /// </summary>
        /// <returns>pafGetFilteredUOWSizeResponse</returns>
        public pafGetFilteredUOWSizeResponse GetPafGetFilteredUOWSizeResponse()
        {
            return _PafGetFilteredUOWSizeResponse;
        }

        /// <summary>
        /// Gets the global PafAppSettings
        /// </summary>
        /// <returns>PafAppSettings</returns>
        public appSettings GetPafAppSettings()
        {
            if (_PafAuthResponse != null)
            {
                return _PafAuthResponse.appSettings;
            }
            else
            {
                return null;
            }
        }


        /// <summary>
        /// Gets the global pafPlanSessionResponse.
        /// </summary>
        /// <returns>pafAuthResponse</returns>
        public pafPlanSessionResponse GetPafPlanSessionResponse()
        {
            return _PafPlanSessionResponse;
        }

        /// <summary>
        /// Gets a PafServiceMngr(which handles all the calls into the PafServiceLayer).
        /// </summary>
        /// <returns>A PafServiceMngr.</returns>
        public PafServiceMngr GetServiceMngr()
        {
            if (_ServiceMngr == null)
            {
                _ServiceMngr = new PafServiceMngr();
            }
            return _ServiceMngr;
        }


        /// <summary>
        /// Gets the PafService object.
        /// </summary>
        /// <returns>The PafService object created in the init(). </returns>
        public PafServiceProviderService GetPafService()
        {
            if (_PafService == null)
            {
                _PafService = new PafServiceProviderService();
                _PafService.Timeout = Settings.Default.ClientTimeout;
                //Create a cookie container, so the client is aware of any cookies.
                if (_PafService.CookieContainer == null)
                {
                    _PafService.CookieContainer = new CookieContainer();
                }




            }
            return _PafService;
        }

        /// <summary>
        /// Returns the ClientId set by the PafServer.
        /// </summary>
        public String ClientId
        {
            get { return _ClientID; }
            set { _ClientID = value; }
        }

        /// <summary>
        /// Returns the platform version.
        /// </summary>
        public String Platform
        {
            get { return _Platform; }
        }

        /// <summary>
        /// Returns the version of the PafServer.
        /// </summary>
        public String ServerVersion
        {
            get { return _ServerVersion; }
        }

        /// <summary>
        /// Get the logger object.
        /// </summary>
        /// <returns>A Logging object for logging purposes.</returns>
        public Logging GetLogger()
        {
            //Get the caller and pass it to the logger, so it shows up in the log.
            System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
            System.Diagnostics.StackFrame fr = st.GetFrame(1);
            System.Reflection.MethodBase m = fr.GetMethod();

            _Logger = new Logging(m.DeclaringType.FullName + "." + m.Name);

            if (_Logger == null)
            {
                _Logger = new Logging(MethodBase.GetCurrentMethod().DeclaringType.ToString());
            }

            return _Logger;
        }
    }
}