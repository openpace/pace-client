#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using PafServerStressTester.Pace.Application.Utilities;
using PafServerStressTester.PafServiceLocal;
using Titan.Pace;
using Titan.Pace.DataStructures;

namespace PafServerStressTester.Pace.Application
{
    public class PafServiceMngr
    {
        ////Store the PafView for each View - modified by sorting
        private readonly Dictionary<string, pafView> _PafViews  = new Dictionary<string, pafView>();

        /// <summary>
        /// Evaluate a view on the PafServer.
        /// </summary>
        /// <param name="dataSlice">The data slice to evaluate.</param>
        /// <param name="changedCells">List of cells that changed on the data slice.</param>
        /// <param name="lockedCells">List of locked cells on the data slice.</param>
        /// <param name="protectedCells">List of protected cells on the data slice.</param>
        /// <param name="replicateAllCells">List of cells to replicate "All" on the server.</param>
        /// <param name="replicateExistingCells">List of cells to replicate "Existing" on the server.</param>
        /// <param name="liftAllCells">List of cells to lift "All" on the server.</param>
        /// <param name="liftExistingCells">List of cells to lift "Existing" on the server.</param>
        /// <param name="sessionLockedCells"></param>
        /// <param name="protectedFormulas">String array of protected formulas on the data slice.</param>
        /// <param name="sessionToken">The session token provided by the server.</param>
        /// <param name="ruleSet">The session token provided by the server.</param>
        /// <param name="clientId">Client id provided by the server.</param>
        /// <param name="viewName">Name of the view to evaluate.</param>
        /// <param name="app">The Paf application object.</param>
        /// <returns>A pafDataSlice with the updated information.</returns>
        public pafDataSlice EvaluateView(pafDataSlice dataSlice, Titan.PafService.simpleCoordList changedCells,
            Titan.PafService.simpleCoordList lockedCells, Titan.PafService.simpleCoordList protectedCells, 
            Titan.PafService.simpleCoordList replicateAllCells, Titan.PafService.simpleCoordList replicateExistingCells,
            Titan.PafService.simpleCoordList liftAllCells, Titan.PafService.simpleCoordList liftExistingCells, Titan.PafService.simpleCoordList[] sessionLockedCells,
            string[] protectedFormulas, string ruleSet, string sessionToken, string clientId, string viewName, PafApp app)
        {
            DateTime startTime = DateTime.Now;
            //create the view request.
            evaluateViewRequest evalviewrequest = new evaluateViewRequest();
            //set the view request dataslice.
            evalviewrequest.dataSlice = dataSlice;
            //set the view request properties.
            evalviewrequest.sessionToken = sessionToken;
            //Set the client id.
            evalviewrequest.clientId = clientId;
            //Set the view name.
            evalviewrequest.viewName = viewName;
            //set the view request changed cells
            if (changedCells != null && changedCells.axis.Length > 0 && changedCells.coordinates.Length > 0)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(changedCells);
                pscl.compressData();
                evalviewrequest.changedCells = ConversionUtils.ConvertSimpleCoordList(pscl.GetSimpleCoordList);
                evalviewrequest.changedCells.axis = null;
                evalviewrequest.changedCells.coordinates = null;
            }
            else
            {
                evalviewrequest.changedCells = null;
            }

            //set the view request locked cells.
            if (lockedCells != null && lockedCells.axis.Length > 0 && lockedCells.coordinates.Length > 0)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(lockedCells);
                pscl.compressData();
                evalviewrequest.lockedCells = ConversionUtils.ConvertSimpleCoordList(pscl.GetSimpleCoordList);
                evalviewrequest.lockedCells.axis = null;
                evalviewrequest.lockedCells.coordinates = null;
            }
            else
            {
                evalviewrequest.lockedCells = null;
            }


            //set the view request protected cells.
            if (protectedCells != null && protectedCells.axis.Length > 0 && protectedCells.coordinates.Length > 0)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(protectedCells);
                pscl.compressData();
                evalviewrequest.protectedCells = ConversionUtils.ConvertSimpleCoordList(pscl.GetSimpleCoordList);
                evalviewrequest.protectedCells.axis = null;
                evalviewrequest.protectedCells.coordinates = null;
            }
            else
            {
                evalviewrequest.protectedCells = null;
            }

            //set the replicate all cells.
            if (replicateAllCells != null && replicateAllCells.axis.Length > 0 && replicateAllCells.coordinates.Length > 0)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(replicateAllCells);
                pscl.compressData();
                evalviewrequest.replicateAllCells = ConversionUtils.ConvertSimpleCoordList(pscl.GetSimpleCoordList);
                evalviewrequest.replicateAllCells.axis = null;
                evalviewrequest.replicateAllCells.coordinates = null;
            }
            else
            {
                evalviewrequest.replicateAllCells = null;
            }

            //set the replicate existing cells.
            if (replicateExistingCells != null && replicateExistingCells.axis.Length > 0 && replicateExistingCells.coordinates.Length > 0)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(replicateExistingCells);
                pscl.compressData();
                evalviewrequest.replicateExistingCells = ConversionUtils.ConvertSimpleCoordList(pscl.GetSimpleCoordList);
                evalviewrequest.replicateExistingCells.axis = null;
                evalviewrequest.replicateExistingCells.coordinates = null;
            }
            else
            {
                evalviewrequest.replicateExistingCells = null;
            }



            //set the lift all cells.
            if (liftAllCells != null && liftAllCells.axis.Length > 0 && liftAllCells.coordinates.Length > 0)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(liftAllCells);
                pscl.compressData();
                evalviewrequest.liftAllCells = ConversionUtils.ConvertSimpleCoordList(pscl.GetSimpleCoordList);
                evalviewrequest.liftAllCells.axis = null;
                evalviewrequest.liftAllCells.coordinates = null;
            }
            else
            {
                evalviewrequest.liftAllCells = null;
            }

            //set the lift existing cells.
            if (liftExistingCells != null && liftExistingCells.axis.Length > 0 && liftExistingCells.coordinates.Length > 0)
            {
                PafSimpleCoordList pscl = new PafSimpleCoordList(liftExistingCells);
                pscl.compressData();
                evalviewrequest.liftExistingCells = ConversionUtils.ConvertSimpleCoordList(pscl.GetSimpleCoordList);
                evalviewrequest.liftExistingCells.axis = null;
                evalviewrequest.liftExistingCells.coordinates = null;
            }
            else
            {
                evalviewrequest.liftExistingCells = null;
            }

            //set the view request protected formulas.
            evalviewrequest.protectedFormulas = protectedFormulas;
            //set the ruleset chosen  for the view
            evalviewrequest.ruleSetName = ruleSet;

            //////Pass evaluateView to the middle tier and get axis new data slice
            //////Get the data slice from the evaluateViewResponse.
            ////pafDataSlice newDataSlice =
            ////    app.GetPafService().evaluateView(evalviewrequest);

            //////Get the Base64 data string and convert it to the data slice
            ////if (newDataSlice.compressed)
            ////{
            ////    newDataSlice.data = GetUncompressedData(newDataSlice.compressedData);
            ////}

            //////Update the cached pafView object for the view with the new dataslice from the server.
            //////GetCurrentPafView(viewName).viewSections[0].pafDataSlice = newDataSlice;

            ////return newDataSlice;






            //Updated for suppress zero's
            //Pass evaluateView to the middle tier and get axis new data slice
            //Get the data slice from the evaluateViewResponse.
            pafView outPafView = app.GetPafService().evaluateView(evalviewrequest);

            pafDataSlice newDataSlice = null;

            if (outPafView != null)
            {
                newDataSlice = outPafView.viewSections[0].pafDataSlice;

                //Get the Base64 data string and convert it to the data slice
                if (outPafView.viewSections[0].pafDataSlice.compressed)
                {
                    newDataSlice.data = GetUncompressedData(newDataSlice.compressedData, app);
                }
            }

            app.GetLogger().Debug("evaluateView runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");


            return newDataSlice;

        }


        /// <summary>
        /// Get a PafView from the PafService.
        /// </summary>
        /// <param name="viewName">The name of the view to return from the server.</param>
        /// <param name="pafUserSel">User selections to be passed to the server(for dynamic view).
        /// This parameter can be null.</param>
        /// <param name="app">The Paf application object.</param>
        /// <param name="rowsSuppressed">Suppress the rows on the view.</param>
        /// <param name="columnsSuppressed">Suppress the columns on the view.</param>
        /// <returns>A PafView.</returns>
        public pafView GetPafView(string viewName, pafUserSelection[] pafUserSel, PafApp app, bool rowsSuppressed, bool columnsSuppressed)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                //Create the view object.
                viewRequest viewReq = new viewRequest();
                //Set the view request properties.
                viewReq.viewName = viewName;
                viewReq.sessionToken = app.GetPafAuthResponse().securityToken.sessionToken;
                viewReq.clientId = app.ClientId;
                viewReq.userSelections = pafUserSel;
                viewReq.compressResponse = true;
                viewReq.rowsSuppressed = rowsSuppressed;
                viewReq.columnsSuppressed = columnsSuppressed;

                //Cache the PafView - modified by sorting
                pafView pafView = app.GetPafService().getView(viewReq);

                if (_PafViews.ContainsKey(viewName))
                {
                    _PafViews[viewName] = pafView;
                }
                else
                {
                    _PafViews.Add(viewName, pafView);
                }


                //Get the Base64 data string and convert it to the data slice
                foreach (pafViewSection viewSection in pafView.viewSections)
                {
                    if (String.IsNullOrEmpty(PafAppConstants.ELEMENT_DELIM))
                    {
                        PafAppConstants.ELEMENT_DELIM = viewSection.elementDelim;
                    }

                    if (String.IsNullOrEmpty(PafAppConstants.GROUP_DELIM))
                    {
                        PafAppConstants.GROUP_DELIM = viewSection.groupDelim;
                    }

                    var col = new Compression.PafViewSection.ViewTuples(viewSection.colTuples);
                    col.uncompressData();

                    var row = new Compression.PafViewSection.ViewTuples(viewSection.rowTuples);
                    row.uncompressData();

                    var pvs = new Compression.PafViewSection.PafViewSection(viewSection);
                    pvs.uncompressData();

                    if (viewSection.pafDataSlice.compressed)
                    {
                        viewSection.pafDataSlice.data = GetUncompressedData(viewSection.pafDataSlice.compressedData, app);
                    }
                }

                return pafView;  
            }
            catch (System.Web.Services.Protocols.SoapException)
            {
                throw;
            }
            catch
            {
                throw;
            }
            finally
            {
                app.GetLogger().Debug("getView: " + viewName + ", runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }

        /// <summary>
        /// Convert a Base64 string to a double array of data
        /// </summary>
        /// <param name="compressedData">the Base64 string</param>
        /// <returns></returns>
        private static double?[] GetUncompressedData(string compressedData, PafApp app)
        {
            DateTime startTime = DateTime.Now;
            try
            {
                if (compressedData.Length > 0 && compressedData.Length%12 == 0)
                {
                    int arrayLength = compressedData.Length/12;
                    double?[] dataSlice = new double?[arrayLength];
                    int startIndex = 0;

                    for (int i = 0; i < arrayLength; i++)
                    {
                        string subString = compressedData.Substring(startIndex, 12);
                        byte[] bytes = Convert.FromBase64String(subString);

                        dataSlice[i] = BitConverter.ToDouble(bytes, 0);
                        startIndex += 12;
                    }
                    return dataSlice;
                }
                else
                {
                    return new double?[0];
                }
            }
            finally
            {
                app.GetLogger().Debug("UncompressData runtime: " + (DateTime.Now - startTime).TotalSeconds + " (s)");
            }
        }
    }
}