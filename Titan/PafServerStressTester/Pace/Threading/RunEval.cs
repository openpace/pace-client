#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using PafServerStressTester.PafServiceLocal;
using PafServerStressTester.Pace.Application;
using PafServerStressTester.Pace.Application.Utilities;
using PafServerStressTester.Properties;
using ServerStressTesterBase.Pace.Extensions;
using Titan.Pace;
using Titan.Pace.Application.Utilities;
using Titan.Pace.DataStructures;
using Titan.Pace.Application.Extensions;
using Titan.Pace.ExcelGridView.Utility;
using Titan.Pace.TestHarness.Binary;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Palladium.TestHarness;
using PafApp = PafServerStressTester.Pace.Application.PafApp;

namespace PafServerStressTester
{
    class RunEval
    {
        /// <summary>
        /// 
        /// </summary>
        public string ClientId { get; set; }
       
        /// <summary>
        /// 
        /// </summary>
        public TimeSpan RunTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //public string TThMainFile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string TThLogFile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<TimeSpan> EvaluationRunTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<long?> UowCreationTimes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<long?> ViewCreationTimes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public List<long?> LoginTimes { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan AvgEvaluationRunTime { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ThreadNum { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Status Status { get; set; }

        ///// <summary>
        ///// 
        ///// </summary>
        //public Dictionary<string, List<TimeSpan>> EvaluationRunTimes()
        //{
        //    return _EvaluationRunTime2.Values(TThMainFile);
        //}

        private PafApp _pafApp;
        private CellAddress _startCell;
        //private CacheSelectionsList<string, string, TimeSpan> _EvaluationRunTime2 = new CacheSelectionsList<string, string, TimeSpan>();

        private DateTime _startTime;
        private DateTime _endTime;

        private DateTime _evaluationStartTime;
        private DateTime _evaluationEndTime;

        private List<int> _rowBlanks = null;
        private List<int> _colBlanks = null;

        private readonly int _evalsToRun = Settings.Default.EvaluationPasses;
        private readonly int _evalSleep = Settings.Default.EvaluationSleep;
        private readonly bool _sessionDetail = Settings.Default.sessionDetail;
        private readonly bool _printCookieInfo = Settings.Default.PrintCookieInformation;

        private readonly RoleFilter _rf = new RoleFilter();

        public event ThreadCompleteEventHandler ThreadComplete;
        public delegate void ThreadCompleteEventHandler(object sender);

        public event ThreadTestCompleteEventHandler ThreadTestComplete;
        public delegate void ThreadTestCompleteEventHandler(object sender, ThreadTestCompleteArgs eventArgs);

        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="threadNumber">Number of threads.</param>
        public RunEval(int threadNumber)
        {
            
            Status = Status.Running;
            ThreadNum = threadNumber;
            Thread t = new Thread(new ThreadStart(run));
            if (_sessionDetail) ConsoleWrite("Forking off new client thread: " + ThreadNum);
            t.Start();
        }

        /// <summary>
        /// Kick off the thread.
        /// </summary>
        private void run()
        {
            _startTime = DateTime.Now;
            _pafApp = new PafApp();
            EvaluationRunTime = new List<TimeSpan>();
            var file = new FileInfo(Settings.Default.tthFile);
            //TThMainFile = file.Name;
            TThLogFile = file.Name + ".log";
            try
            {
                if (file.Extension.ToLower().Equals(".btth"))
                {
                    ProcessBtthFile(Settings.Default.tthFile);
                }
                else
                {
                    Program.WriteTestSuiteStart(file.FullName);
                    run2(file.FullName);
                    Program.WriteTestSuiteFinished(file.FullName);
                }

                Status = Status.Ok;
                _endTime = DateTime.Now;
                RunTime = _endTime - _startTime;
            }
            catch (Exception ex)
            {
                _endTime = DateTime.Now;
                RunTime = _endTime - _startTime;
                Status = Status.Error;
                Program.WriteTestSuiteFinished(file.FullName);
                Program.WriteLineToConsole(ex.Message, Settings.Default.GenerateLogFile, true, ex);
                Program.WriteErrorLineToLog(ex.Message, TThLogFile);
                Program.WriteErrorLineToLog(ex.Message, Settings.Default.ProgramErrorFileName);
                Program.WriteErrorLineToLog(ex.StackTrace, TThLogFile);
                Program.WriteErrorLineToLog(ex.StackTrace, Settings.Default.ProgramErrorFileName);
                Program.WriteLineToConsole("=======================================", Settings.Default.GenerateLogFile, true);
                Program.WriteErrorLineToLog("=======================================", TThLogFile);
                Program.WriteErrorLineToLog("=======================================", Settings.Default.ProgramErrorFileName);
            }
            finally
            {
                _pafApp.EndPlanningSession();
                ThreadComplete(this);
            }
        }

        /// <summary>
        /// Starts a .tth file
        /// </summary>
        /// <param name="fileName">tth file to launch.</param>
        private void run2(string fileName)
        {
            Stopwatch sw = new Stopwatch();
            List<double> evaluation = new List<double>();
            UowCreationTimes = new List<long?>();
            ViewCreationTimes = new List<long?>();
            LoginTimes = new List<long?>();
            FileInfo fi = new FileInfo(fileName);
            string testName = fi.Name;// +", thread: " + _ThreadNum;
            try
            {
                sw.Start();
                bool uowState = false;
                string previousViewName = String.Empty;
                pafView view = null;

                if (_sessionDetail) ConsoleWrite("Processing file: " + fileName);
                Program.WriteTestStart(testName);
                FileInfo fileObject = new FileInfo(fileName);

                TestHarness th = new TestHarness();
                th = BinaryDeserializer.Deserialize(fileName);

                Titan.PafService.pafUserSelection[] previousUserSelection = null;

                int tsNumber = 0;
                foreach (TestSequence ts in th[0].TestSequences)
                {
                    Titan.PafService.simpleCoordList changedCells;
                    Titan.PafService.simpleCoordList lockedCells;

                    var replicateAllIntersections = new List<Intersection>();
                    var repllicateExistingIntersections = new List<Intersection>();

                    var liftAllIntersections = new List<Intersection>();
                    var liftExistingIntersections = new List<Intersection>();

                    Titan.PafService.simpleCoordList replicateAll = null;
                    Titan.PafService.simpleCoordList replicateExisting = null;
                    Titan.PafService.simpleCoordList liftAll = null;
                    Titan.PafService.simpleCoordList liftExisting = null;
                    Titan.PafService.simpleCoordList protectedCells = null;

                    //get the replication all intersections.
                    if (ts.ChangedCellInfoList[0].ProtMngrListTracker.ReplicateAllCells != null &&
                        ts.ChangedCellInfoList[0].ProtMngrListTracker.ReplicateAllCells.Count > 0)
                    {
                        replicateAllIntersections.AddRange(ts.ChangedCellInfoList[0].ProtMngrListTracker.ReplicateAllCells);
                        //get the simple coord list of replicated all cells
                        replicateAll = replicateAllIntersections.ToSimpleCoordList(false);
                    }

                    //get the Replicate existing intersections.
                    if (ts.ChangedCellInfoList[0].ProtMngrListTracker.ReplicateExistingCells != null &&
                       ts.ChangedCellInfoList[0].ProtMngrListTracker.ReplicateExistingCells.Count > 0)
                    {
                        repllicateExistingIntersections.AddRange(ts.ChangedCellInfoList[0].ProtMngrListTracker.ReplicateExistingCells);
                        //get the simple coord list of replicated existing cells
                        replicateExisting = repllicateExistingIntersections.ToSimpleCoordList(false);
                    }


                    //get the lift all intersections.
                    if (ts.ChangedCellInfoList[0].ProtMngrListTracker.LiftAllCells != null &&
                        ts.ChangedCellInfoList[0].ProtMngrListTracker.LiftAllCells.Count > 0)
                    {
                        liftAllIntersections.AddRange(ts.ChangedCellInfoList[0].ProtMngrListTracker.LiftAllCells);
                        //get the simple coord list of replicated all cells
                        liftAll = replicateAllIntersections.ToSimpleCoordList(false);
                    }

                    //get the lift existing intersections.
                    if (ts.ChangedCellInfoList[0].ProtMngrListTracker.LiftExistingCells != null &&
                       ts.ChangedCellInfoList[0].ProtMngrListTracker.LiftExistingCells.Count > 0)
                    {
                        liftExistingIntersections.AddRange(ts.ChangedCellInfoList[0].ProtMngrListTracker.LiftExistingCells);
                        //get the simple coord list of replicated existing cells
                        liftExisting = repllicateExistingIntersections.ToSimpleCoordList(false);
                    }



                    //get the protected cells.
                    if (ts.ChangedCellInfoList[0].ProtMngrListTracker.ProtectedCells != null)
                    {
                        protectedCells = ts.ChangedCellInfoList[0].ProtMngrListTracker.ProtectedCells.ToSimpleCoordList(false);
                    }

                    //get the locked cells.
                    GetChangedAndLockedCells(
                        ts.ChangedCellInfoList[0].ProtMngrListTracker.Changes,
                        ts.ChangedCellInfoList[0].ProtMngrListTracker.ProtectedCells,
                        ts.ChangedCellInfoList[0].ProtMngrListTracker.UserLocks,
                        ts.ChangedCellInfoList[0].ProtMngrListTracker.UnLockedChanges,
                        out changedCells, out lockedCells);

                    string passwordHash;
                    PafLogonInfo user = GetLoginInfo(out passwordHash);


                    if (String.IsNullOrEmpty(ClientId))
                    {
                        Stopwatch caStopwatch = Stopwatch.StartNew();
                        //send an init and client auth.
                        _pafApp.SendClientAuth(
                            user.UserName,
                            passwordHash,
                            user.UserDomain,
                            user.IV,
                            user.UserPassword,
                            user.UserSID);

                        caStopwatch.Stop();
                        LoginTimes.Add(caStopwatch.ElapsedMilliseconds);
                    }
                    else
                    {
                        LoginTimes.Add(null);
                    }


                    if (!uowState || th[0].Reload)
                    {
                        //set the client id.
                        ClientId = _pafApp.ClientId;

                        TThLogFile = ClientId + "." + fileObject.Name + ".log";

                        _rf.AddRoles(_pafApp);

                        if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " returned client aut." + _pafApp.GetCookieInfo(_printCookieInfo));

                        if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " creating planning session." + _pafApp.GetCookieInfo(_printCookieInfo));

                        _pafApp.ClientId = ClientId;

                        Stopwatch uStopwatch = Stopwatch.StartNew();
                        _rf.CreateUOW(
                            user.UserName,
                            Settings.Default.role,
                            Settings.Default.process,
                            th[0],
                            _pafApp);
                        uStopwatch.Stop();
                        UowCreationTimes.Add(uStopwatch.ElapsedMilliseconds);

                        if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " created planning session." + _pafApp.GetCookieInfo(_printCookieInfo));
                        
                        uowState = true;
                    }
                    else
                    {
                        UowCreationTimes.Add(null);
                    }

                    //Added code to allow user to change view selector sections between view sequences.
                    bool buildView = false;
                    if (!ts.ViewName.Equals(previousViewName))
                    {
                        buildView = true;
                    }
                    else if (previousUserSelection != null && !PafUserSelectionExtension.IsEqual(previousUserSelection, ts.PafUserSelections))
                    {
                        buildView = true;
                    }

                    if(buildView)
                    {
                        Stopwatch vStopwatch = Stopwatch.StartNew();
                        view = _pafApp.GetServiceMngr().GetPafView(
                            ts.ViewName,
                            ConversionUtils.ConvertPafUserSels(ts.PafUserSelections),
                            _pafApp,
                            false,
                            false);
                        vStopwatch.Stop();
                        ViewCreationTimes.Add(vStopwatch.ElapsedMilliseconds);

                        previousUserSelection = ts.PafUserSelections;

                        previousViewName = view.name;

                        if (_sessionDetail)
                        {
                            ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " got view: " +
                                            ts.ViewName + " : " + _pafApp.GetCookieInfo(_printCookieInfo));
                        }
                    }
                    else
                    {
                        ViewCreationTimes.Add(null);
                    }


                    for (int i = 0; i < _evalsToRun; i++)
                    {
                        //if there is nothing to change then don't perform the evaluation...
                        if (ts.ChangedCellInfoList != null)
                        {
                            PerformEvaluationOperation(
                                _pafApp,
                                view,
                                th,
                                changedCells,
                                lockedCells,
                                protectedCells,
                                replicateAll,
                                replicateExisting,
                                liftAll,
                                liftExisting,
                                tsNumber,
                                fileObject.Name);
                            evaluation.Add(EvaluationRunTime[EvaluationRunTime.Count - 1].TotalMilliseconds);
                        }
                        // don't sleep on last operation 
                        if (i < _evalsToRun - 1 || Settings.Default.sleepOnLastEval)
                        {
                            OperationPause(_evalSleep);
                        }
                       
                        if (i < _evalsToRun - 1)
                        {
                            UowCreationTimes.Add(null);
                            ViewCreationTimes.Add(null);
                            LoginTimes.Add(null);
                        }
                    }
                    tsNumber++;
                }

                Status = Status.Ok;

                sw.Stop();

                if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " closing session." + _pafApp.GetCookieInfo(_printCookieInfo));

                Program.WriteTestEnd(testName, sw.ElapsedMilliseconds);

                //List<double> evaluation = EvaluationRunTime.Select(ts => ts.TotalMilliseconds).ToList();
                
                double avg = evaluation.Average();

                AvgEvaluationRunTime = new TimeSpan(0, 0, 0, 0, Convert.ToInt32(avg));

                ThreadTestComplete(this, new ThreadTestCompleteArgs(
                    ThreadNum, 
                    ClientId, 
                    testName, 
                    sw.ElapsedMilliseconds, 
                    Status.ToString(), 
                    evaluation, 
                    UowCreationTimes,
                    ViewCreationTimes,
                    LoginTimes));

            }
            catch (Exception ex)
            {
                sw.Stop();

                Program.WriteTestFailed("Performance", testName, ex.Message);

                if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " Failed. Exception" + ex.Message);


                if (_pafApp != null)
                {
                    Program.WriteErrorLineToLog(_pafApp.GetCookieInfo(_printCookieInfo), Settings.Default.ProgramErrorFileName);
                }
                Program.WriteLineToConsole(ex.Message, Settings.Default.GenerateLogFile, true, ex);
                Program.WriteErrorLineToLog(ex.Message, TThLogFile);
                Program.WriteErrorLineToLog(ex.Message, Settings.Default.ProgramErrorFileName);
                //Program.WriteLineToConsole(ex.StackTrace, Settings.Default.GenerateLogFile, true);
                Program.WriteErrorLineToLog(ex.StackTrace, TThLogFile);
                Program.WriteErrorLineToLog(ex.StackTrace, Settings.Default.ProgramErrorFileName);
                if (ex.InnerException != null)
                {
                    Program.WriteLineToConsole(ex.InnerException.Message, Settings.Default.GenerateLogFile, true, ex.InnerException);
                    Program.WriteErrorLineToLog(ex.InnerException.Message, TThLogFile);
                    Program.WriteErrorLineToLog(ex.InnerException.Message, Settings.Default.ProgramErrorFileName);

                    //Program.WriteLineToConsole(ex.InnerException.StackTrace, Settings.Default.GenerateLogFile, true);
                    Program.WriteErrorLineToLog(ex.InnerException.StackTrace, TThLogFile);
                    Program.WriteErrorLineToLog(ex.InnerException.StackTrace, Settings.Default.ProgramErrorFileName);
                }

                Program.WriteLineToConsole("=======================================", Settings.Default.GenerateLogFile, true);
                Program.WriteErrorLineToLog("=======================================", TThLogFile);
                Program.WriteErrorLineToLog("=======================================", Settings.Default.ProgramErrorFileName);
                
                
                throw;
            }
        }

        private void OperationPause(int evalSleep)
        {
            int wait = evalSleep;
            if (wait == 0 && !Settings.Default.RandomEvaluationSleep)
            {
                return;
            }
            if (Settings.Default.RandomEvaluationSleep)
            {
                Random random = new Random();
                wait = random.Next(Settings.Default.RandomEvaluationSleepMin, Settings.Default.RandomEvaluationSleepMax);
            }


            ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " Waiting: " + wait + " milliseconds before the next evaluation operation.");

            Thread.Sleep(wait);

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="passwordHash"></param>
        /// <returns></returns>
        private PafLogonInfo GetLoginInfo(out string passwordHash)
        {
            PafLogonInfo user = new PafLogonInfo(
                Settings.Default.userid,
                Settings.Default.password,
                null,
                null,
                null);

            string userid = user.UserName;
            passwordHash = user.UserPassword;

            //if the auth mode is mixed and the sid is empty.
            if (!Settings.Default.authMode.Equals(String.Empty) &&
                Settings.Default.authMode.Equals(authMode.mixedMode.ToString()) &&
                Settings.Default.useSingleSignOn)
            {
                if (userid.Contains(PafAppConstants.FOWARD_SLASH) || userid.Contains(PafAppConstants.ASTERISK))
                {
                    user.UserSID = Security.GetUserSID();
                    user.UserDomain = null;
                    passwordHash = null;
                }
            }
            else if (!Settings.Default.authMode.Equals(String.Empty) &&
                    Settings.Default.authMode.Equals(authMode.mixedMode.ToString()) &&
                    !Settings.Default.useSingleSignOn)
            {
                if (userid.Contains(PafAppConstants.FOWARD_SLASH) || userid.Contains(PafAppConstants.ASTERISK))
                {
                    user.UserSID = null;
                    user.UserDomain = Security.GetUsersDomain(userid, true);
                    passwordHash = null;
                    user = Security.EncryptUser(user);
                }
                else
                {
                    user.UserSID = null;
                    user.UserDomain = Security.GetUsersDomain(userid, true);
                    user = Security.EncryptUser(user);
                }
            }
            if (!Settings.Default.authMode.Equals(String.Empty) &&
                Settings.Default.authMode.Equals(authMode.nativeMode.ToString()))
            {
                user.UserSID = null;
                user.UserDomain = null;
                user = Security.EncryptUser(user);
            }

            return user;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        /// <param name="view"></param>
        /// <param name="th"></param>
        /// <param name="changedCells"></param>
        /// <param name="lockedCells"></param>
        /// <param name="protectedCells"></param>
        /// <param name="replicateAll"></param>
        /// <param name="replicateExisting"></param>
        /// <param name="liftAll"></param>
        /// <param name="liftExisting"></param>
        /// <param name="testSeqNumber"></param>
        private void PerformEvaluationOperation(PafApp app, pafView view, TestHarness th,
            Titan.PafService.simpleCoordList changedCells, Titan.PafService.simpleCoordList lockedCells, 
            Titan.PafService.simpleCoordList protectedCells,
            Titan.PafService.simpleCoordList replicateAll, 
            Titan.PafService.simpleCoordList replicateExisting,
            Titan.PafService.simpleCoordList liftAll,
            Titan.PafService.simpleCoordList liftExisting,
            int testSeqNumber,
            string fileName)
        {
            if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " modifying dataslice with changed intersections." + _pafApp.GetCookieInfo(_printCookieInfo));

            int startRow = 4;
            pafViewSection viewSection = view.viewSections[0];
            //If their are more than 2 headers, then adjust the start row accordingly.
            if (viewSection.pafViewHeaders != null)
            {
                if (viewSection.pafViewHeaders.Length > 2)
                {
                    startRow = startRow + (viewSection.pafViewHeaders.Length - 2);
                }
            }

            int rowOffset = 0;
            int colOffset = 0;
            List<string> rowDims = new List<string>(5);
            List<String> colDims = new List<string>(5);
            if (viewSection.rowAxisDims != null && viewSection.rowAxisDims.Length > 0)
            {
                rowDims.AddRange(viewSection.rowAxisDims);
            }
            if (viewSection.colAxisDims != null && viewSection.colAxisDims.Length > 0)
            {
                colDims.AddRange(viewSection.colAxisDims);
            }

            pafViewTreeItem[] viewTrees = _pafApp.GetPafPlanSessionResponse().viewTreeItems;
            var viewTree = viewTrees.FirstOrDefault(x => x.label == view.name);
            if (viewTree != null)
            {
                rowOffset += rowDims.Select(tmp => viewTree.aliasMappings.Where(x => x.dimName == tmp).ToList()).Where(mappings => mappings.Count > 0).Count(mappings => !String.IsNullOrWhiteSpace(mappings[0].additionalRowColumnFormat));
                colOffset += colDims.Select(tmp => viewTree.aliasMappings.Where(x => x.dimName == tmp).ToList()).Where(mappings => mappings.Count > 0).Count(mappings => !String.IsNullOrWhiteSpace(mappings[0].additionalRowColumnFormat));
            }

            _startCell = new CellAddress(startRow, 1);

            SetBlankColumns(viewSection);

            double[,] grid = viewSection.PopulateData(_rowBlanks, _colBlanks, out _startCell);//  PopulateData(view.viewSections[0]);

            foreach (ChangedCellInfo cell in th[0].TestSequences[testSeqNumber].ChangedCellInfoList)
            {
                if (cell.ChangeType == Change.UserChanged || 
                    cell.ChangeType == Change.ReplicateExisting ||
                    cell.ChangeType == Change.ReplicateAll)
                {
                    if (cell.CellAddress != null)
                    {
                        int row = cell.CellAddress.Row - viewSection.colAxisDims.Length - colOffset - _startCell.Row;
                        int col = cell.CellAddress.Col - viewSection.rowAxisDims.Length - rowOffset - _startCell.Col;
                        grid[row, col] = (Double)cell.CellValue;
                    }
                    else
                    {
                        List<Cell> cr = cell.CellAdressRange.Cells;
                        double[] d = cell.CellValue.ConvertTwoDimObjectArrayToDoubleArray();
                        int i = 0;
                        foreach (Cell rng in cr)
                        {
                            int row = rng.CellAddress.Row - viewSection.colAxisDims.Length - colOffset - _startCell.Row;
                            int col = rng.CellAddress.Col - viewSection.rowAxisDims.Length - rowOffset - _startCell.Col;
                            grid[row, col] = d[i];
                            i++;
                        }
                    }
                }
            }

            pafDataSlice pafdslice = new pafDataSlice();
            int colCount;
            pafdslice.compressed = true;
            pafdslice.data = null;
            pafdslice.compressedData = grid.ToBase64(_colBlanks.Count, out colCount); //GetGridDataBase64(grid, out colCount);
            pafdslice.columnCount = colCount;

            if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + app.ClientId + " sending evaluation request." + _pafApp.GetCookieInfo(_printCookieInfo));
            _evaluationStartTime = DateTime.Now;
            app.GetServiceMngr().EvaluateView(
                pafdslice,
                changedCells,
                lockedCells,
                protectedCells,
                replicateAll,
                replicateExisting,
                liftAll,
                liftExisting,
                null,
                null,
                th[0].TestSequences[testSeqNumber].RuleSetName,
                app.GetPafAuthResponse().securityToken.sessionToken,
                app.ClientId,
                th[0].TestSequences[testSeqNumber].ViewName,
                app);
            if (_sessionDetail) ConsoleWrite("Thread Num: " + ThreadNum + ", Client ID: " + ClientId + " evaluation returned from server." + _pafApp.GetCookieInfo(_printCookieInfo));

            _evaluationEndTime = DateTime.Now;
            EvaluationRunTime.Add(_evaluationEndTime - _evaluationStartTime);
            //_EvaluationRunTime2.CacheSelection(TThMainFile, fileName, _EvaluationEndTime - _EvaluationStartTime);

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="str"></param>
        private void ConsoleWrite(string str)
        {
            Program.WriteLineToConsole(str, Settings.Default.GenerateLogFile, false);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="Changes"></param>
        /// <param name="ProtectedCells"></param>
        /// <param name="UserLocks"></param>
        /// <param name="UnLockedChanges"></param>
        /// <param name="changedCells"></param>
        /// <param name="lockedCells"></param>
        public void GetChangedAndLockedCells(
            List<Intersection> Changes,
            List<Intersection> ProtectedCells,
            List<Intersection> UserLocks,
            List<Intersection> UnLockedChanges,
            out Titan.PafService.simpleCoordList changedCells,
            out Titan.PafService.simpleCoordList lockedCells)
        {
            //Build locks list from the list of changes and the list of user locks
            List<Intersection> locks = new List<Intersection>();
            //Build changes list from the list of changes and the list of unlocked changes
            List<Intersection> allChanges = new List<Intersection>();
            //List of coordinates.
            List<string> coordinates;// = new List<string>();
            //Cord list to hold the changed cells.
            changedCells = new Titan.PafService.simpleCoordList();
            //Cord list to hold the locked cells.
            lockedCells = new Titan.PafService.simpleCoordList();

            for (int i = 0; i < Changes.Count; i++)
            {
                if (!ProtectedCells.Contains(Changes[i]))
                {
                    //Locks list
                    locks.Add(Changes[i]);
                }

                //Changes List
                allChanges.Add(Changes[i]);
            }

            //Locks List
            for (int i = 0; i < UserLocks.Count; i++)
            {
                if (!locks.Contains(UserLocks[i]))
                {
                    locks.Add(UserLocks[i]);
                }
            }

            //Changes List
            for (int i = 0; i < UnLockedChanges.Count; i++)
            {
                if (!allChanges.Contains(UnLockedChanges[i]))
                {
                    allChanges.Add(UnLockedChanges[i]);
                }
            }


            //Locked Cells
            coordinates = new List<string>();
            Intersection viewLocks = null;

            for (int i = 0; i < locks.Count; i++)
            {
                viewLocks = locks[i];
                for (int j = 0; j < viewLocks.AxisSequence.Length; j++)
                {
                    coordinates.Add(viewLocks.Coordinates[j]);
                }
            }

            if (viewLocks != null)
            {
                lockedCells.axis = viewLocks.AxisSequence;
            }
            else
            {
                lockedCells.axis = new string[0];
            }

            if (locks != null)
            {
                lockedCells.coordinates = coordinates.ToArray();
            }
            else
            {
                lockedCells.coordinates = new string[0];
            }

            //Reset the coordinates list.
            coordinates = null;
            coordinates = new List<string>();





            //Changed Cells
            Intersection changes = null;
            for (int i = 0; i < allChanges.Count; i++)
            {
                changes = allChanges[i];
                for (int j = 0; j < changes.AxisSequence.Length; j++)
                {
                    coordinates.Add(changes.Coordinates[j]);
                }
            }

            if (changes != null)
            {
                changedCells.axis = changes.AxisSequence;
            }
            else
            {
                changedCells.axis = new string[0];
            }

            if (coordinates != null)
            {
                changedCells.coordinates = coordinates.ToArray();
            }
            else
            {
                changedCells.coordinates = new string[0];
            }

            coordinates = null;
        }


        /// <summary>
        /// Process all the .tth files in a .btth file.
        /// </summary>
        /// <param name="fileName">Name of the .btth file to process.</param>
        public void ProcessBtthFile(string fileName)
        {
            {
                FileInfo file;
                file = new FileInfo(fileName);
                string parentFileDir = file.DirectoryName;

                using (StreamReader sr = new StreamReader(fileName))
                {
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        line = line.Trim();
                        if (!line.Equals(""))
                        {
                            FileInfo fileInfo = new FileInfo(line);
                            //Does the file exist?
                            if (File.Exists(fileInfo.FullName))
                            {
                                ConsoleWrite("Processing file (in btth): " + line);

                                List<string> errorTests = null;

                                if (fileInfo.Extension.ToLower().Equals(".tth"))
                                {
                                    Program.WriteTestSuiteStart(fileInfo.FullName);
                                    run2(fileInfo.FullName);
                                    Program.WriteTestSuiteFinished(fileInfo.FullName);
                                }
                                else if (fileInfo.Extension.ToLower().Equals(".btth"))
                                {
                                    ProcessBtthFile(fileInfo.FullName);
                                }
                                else
                                {
                                    Program.WriteLineToConsole("File Does not exist: " + line, true, true);
                                }
                            }
                            else
                            {
                                string withParentDir = parentFileDir + Path.DirectorySeparatorChar + line;
                                if (File.Exists(withParentDir))
                                {
                                    Console.WriteLine("Processing file (in btth): " + withParentDir);
                                    List<string> errorTests = null;

                                    if (fileInfo.Extension.ToLower().Equals(".tth"))
                                    {
                                        Program.WriteTestSuiteStart(withParentDir);
                                        run2(withParentDir);
                                        Program.WriteTestSuiteFinished(withParentDir);
                                    }
                                    else if (fileInfo.Extension.ToLower().Equals(".btth"))
                                    {
                                        ProcessBtthFile(withParentDir);
                                    }
                                    else
                                    {
                                        Program.WriteLineToConsole("File Does not exist: " + withParentDir, true, true);
                                    }
                                }
                                else
                                {
                                    //It's neither so log an error.
                                    Program.WriteLineToConsole("File Does not exist: " + line, true, true);
                                    Program.WriteLineToConsole("File Does not exist: " + withParentDir, true, true);

                                }
                            }
                        }
                    }
                }
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewSection"></param>
        /// <returns></returns>
        public int GetColumnCount1(pafViewSection viewSection)
        {
            int count = 0;
            foreach (viewTuple tuple in viewSection.colTuples)
            {
                foreach (string memberDef in tuple.memberDefs)
                {
                    if (! memberDef.ToUpper().Equals("PAFBLANK"))
                    {
                        count++;
                    }
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewSection"></param>
        public void SetBlankColumns(pafViewSection viewSection)
        {
            _colBlanks = new List<int>();
            _rowBlanks = new List<int>();



            for (int i = 0; i < viewSection.rowTuples.Length; i++)
            {
                //Store locations for blank rows
                if (viewSection.rowTuples[i].memberDefs[viewSection.rowTuples[i].memberDefs.Length - 1].ToUpper().Equals("PAFBLANK") ||
                    viewSection.rowTuples[i].memberTag)
                {
                    _rowBlanks.Add(i);
                }
            }
            for (int i = 0; i < viewSection.colTuples.Length; i++)
            {
                //Store locations for blank rows
                if (viewSection.colTuples[i].memberDefs[viewSection.colTuples[i].memberDefs.Length - 1].ToUpper().Equals("PAFBLANK") ||
                    viewSection.colTuples[i].memberTag)
                {
                    _colBlanks.Add(i);
                }
            }
        }

        /// <summary>
        /// Checks to see if a value is numeric.
        /// </summary>
        /// <param name="ValueToCheck">Object value to check.</param>
        /// <returns>True if the value is numeric, false if the value is non numeric.</returns>
        public bool IsNumeric(object ValueToCheck)
        {
            try
            {
                double Dummy;
                return double.TryParse(ValueToCheck.ToString(), System.Globalization.NumberStyles.Any, null, out Dummy);
            }
            catch
            {
                return false;
            }
        }
    }
}