﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;

namespace PafServerStressTester
{
    public class ThreadTestCompleteArgs : EventArgs
    {
        public ThreadTestCompleteArgs(int threadNum, string clientId, string testName, double testRuntime,  string status = "Ok",
            List<double> evaluationTimes = null, List<long?> uowCreationTimes = null, List<long?> viewCreationTimes = null, List<long?> loginTimes = null)
        {
            ThreadNum = threadNum;
            ClientId = clientId;
            TestName = testName;
            UowCreationTimes = uowCreationTimes;
            ViewCreationTimes = viewCreationTimes;
            TestRuntime = testRuntime;
            EvaluationTimes = evaluationTimes;
            LoginTimes = loginTimes;
            Status = status;
        }

        public int ThreadNum { get; set; }
        public string ClientId { get; set; }
        public string TestName { get; set; }
        public string Status { get; set; }
        public double TestRuntime { get; set; }
        public List<long?> UowCreationTimes { get; set; }
        public List<long?> ViewCreationTimes { get; set; }
        public List<long?> LoginTimes { get; set; }
        public List<double> EvaluationTimes { get; set; }
    }
}
