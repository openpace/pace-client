﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Runtime.CompilerServices;
using ServerStressTesterBase.Pace.Extensions;

namespace PafServerStressTester.Pace.TeamCity
{
    public class TeamCityLogger
    {
        public bool Enabled { get; set; }

        public TeamCityLogger(bool enabled = true)
        {
            Enabled = enabled;
        }

        public enum TeamCityMessageStatus
        {
            NORMAL,
            WARNING,
            FAILURE,
            ERROR
        };

        /// <summary>
        /// Writes s testStarted block to the log.
        /// </summary>
        /// <param name="testName"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TestStarted(string testName)
        {
            if(!Enabled) return;

            Console.WriteLine("##teamcity[testStarted name='{0}']", new[] {testName.EscapeTeamCityChars()});
        }


        /// <summary>
        /// Writes a testFailed block to the log.
        /// </summary>
        /// <param name="testType"></param>
        /// <param name="testName"></param>
        /// <param name="errorMessage"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TestFailed(string testType, string testName, string errorMessage)
        {
            if(!Enabled) return;

            Console.WriteLine("##teamcity[testFailed type='{0}' name='{1}' message='{2}']", new[] { testType.EscapeTeamCityChars(), testName.EscapeTeamCityChars(), errorMessage.EscapeTeamCityChars() });
        }

        /// <summary>
        /// Writes a message about a test to the log.
        /// </summary>
        /// <param name="messageText"></param>
        /// <param name="messageStatus"></param>
        /// <param name="errorStackTrace"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TestMessage(string messageText, TeamCityMessageStatus messageStatus = TeamCityMessageStatus.NORMAL, string errorStackTrace = null)
        {
            if(!Enabled) return;

            if (!String.IsNullOrEmpty(errorStackTrace))
            {
                Console.WriteLine("##teamcity[message text='{0}' errorDetails='{1}' status='{2}']", new[] { messageText.EscapeTeamCityChars(), errorStackTrace.EscapeTeamCityChars(), messageStatus.ToString().EscapeTeamCityChars().ToUpper() });
            }
            else
            {
                Console.WriteLine("##teamcity[message text='{0}' status='{1}']", new[] { messageText.EscapeTeamCityChars(), messageStatus.ToString().ToUpper().EscapeTeamCityChars() });
            }
        }

        /// <summary>
        /// Writes a testFinished block to the log.
        /// </summary>
        /// <param name="testName"></param>
        /// <param name="elapsedMilliseconds"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TestFinished(string testName, long elapsedMilliseconds)
        {
            if(!Enabled) return;

            //##teamcity[testFinished name='testname' duration='test_duration_in_milliseconds']
            Console.WriteLine("##teamcity[testFinished name='{0}' duration='{1}']", new[] { testName.EscapeTeamCityChars(), elapsedMilliseconds.ToString().EscapeTeamCityChars() });
        }

        /// <summary>
        /// Writes a testSuiteStarted block to the log.
        /// </summary>
        /// <param name="testSuiteName"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TestSuiteStarted(string testSuiteName)
        {
            if(!Enabled) return;

            Console.WriteLine("##teamcity[testSuiteStarted name='{0}']", new[] { testSuiteName.EscapeTeamCityChars() });
        }

        /// <summary>
        /// Writes a testSuiteFinished block to the log.
        /// </summary>
        /// <param name="testSuiteName"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TestSuiteFinished(string testSuiteName)
        {
            if(!Enabled) return;

            Console.WriteLine("##teamcity[testSuiteFinished name='{0}']", new[] { testSuiteName.EscapeTeamCityChars() });
        }

        /// <summary>
        /// Writes a testSuiteFinished block to the log.
        /// </summary>
        /// <param name="statisticsKey"></param>
        /// <param name="statisticsValue"></param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public void TestSuiteStatistics(string statisticsKey, int statisticsValue)
        {
            if (!Enabled) return;

            Console.WriteLine("##teamcity[buildStatisticValue key='{0}' value='{1}']", new[] { statisticsKey.EscapeTeamCityChars(), statisticsValue.ToString().EscapeTeamCityChars() });
        }

        
    }
}
