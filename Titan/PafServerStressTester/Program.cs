#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using PafServerStressTester.Pace.Application;
using PafServerStressTester.Pace.TeamCity;
using PafServerStressTester.Properties;
using ServerStressTesterBase.Pace.Extensions;
using ServerStressTesterDataSet.Pace.Data;
using Logging = ServerStressTesterBase.Pace.LogUtils.Logging;

namespace PafServerStressTester
{
    class Program
    {
        private static Mutex _mutex;
        private static List<RunEval> _threads;
        private static int _threadsToRun;
        private static int _threadSleep;
        private static int _threadCount;
        private static int _evalPasses;
        private static int _evalSleep;
        private static TimeSpan _runTime;
        private static DateTime _startTime;
        private static Logging _logger;
        private static TeamCityLogger _teamCityLogger;
        private static readonly buildType TeamCityStats = new buildType();
        private static readonly TestResultsDataset ResultsDataset = new TestResultsDataset();
        private static string _path;

        private static void Main(string[] args)
        {
            try
            {
                string filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                _path = Path.GetDirectoryName(filePath);

                _mutex = new Mutex(false, "Program");
                _teamCityLogger = new TeamCityLogger(false);
                _threads = new List<RunEval>();
                _threadsToRun = Settings.Default.Threads;
                _threadSleep = Settings.Default.ThreadSleep;
                _evalPasses = Settings.Default.EvaluationPasses;
                _evalSleep = Settings.Default.EvaluationSleep;
                _logger = new Logging("PafServerStressTester");
                _threadCount = 0;
                _startTime = DateTime.Now;

                try
                {
                    WriteLineToConsole(String.Format("Starting Pace Server Stress Tester Version: {0}", Assembly.GetExecutingAssembly().GetName().Version), true);
                    WriteLineToConsole(String.Format("Using Titan.dll Version: {0}", Assembly.LoadFrom("Titan.dll").GetName().Version), true);
                    //WriteLineToConsole(String.Format("Using Pace.AssortmentService.Framework.dll Version: {0}", Assembly.LoadFrom("Pace.AssortmentService.Framework.dll").GetName().Version), true);
                    //WriteLineToConsole(String.Format("Using TestResultsDataSet.dll Version: {0}", Assembly.LoadFrom("TestResultsDataSet.dll").GetName().Version), true);
                    //WriteLineToConsole(String.Format("Using Pace.Global.dll Version: {0}", Assembly.LoadFrom("Pace.Global.dll").GetName().Version), true);
                }
                catch (Exception)
                {
                }

                if(args != null && args.Length > 0)
                {
                    if (args.Contains("-tc") || args.Contains("/tc"))
                    {
                        WriteLineToConsole("TeamCity logging is enabled.", true);
                        _teamCityLogger.Enabled = true;
                    }
                }

                PafApp tempServer = new PafApp();
                tempServer.PrintServerInformation();
                tempServer.EndPlanningSession(false);


                for (int i = 0; i < _threadsToRun; i++)
                {

                    var run = new RunEval(i);
                    run.ThreadTestComplete += RunThreadTestComplete;
                    run.ThreadComplete += RunThreadComplete;
                    _threads.Add(run);
                    if (i < _threadsToRun - 1) Thread.Sleep(_threadSleep);
                }
            }
            catch (Exception ex)
            {
                WriteLineToConsole(ex.Message, Settings.Default.GenerateLogFile, true, ex);
                WriteErrorLineToLog(ex.Message, Settings.Default.ProgramErrorFileName);
                WriteErrorLineToLog(ex.StackTrace, Settings.Default.ProgramErrorFileName);
            }
        }

        [MethodImpl(MethodImplOptions.Synchronized)]
        static void RunThreadTestComplete(object sender, ThreadTestCompleteArgs eventArgs)
        {
            if (!Settings.Default.createXmlDatasets) return;

            int testKey = ResultsDataset.GetTestKey(eventArgs.TestName, _startTime );
            ulong dateKey = ResultsDataset.GetDateKey(_startTime);
            TestResultsDataset.ThreadTestResultsRow testResultsRow = ResultsDataset.ThreadTestResults.NewThreadTestResultsRow();
            testResultsRow.TestNumber = testKey;
            testResultsRow.DateKey = dateKey;
            testResultsRow.ThreadNumber = eventArgs.ThreadNum;
            testResultsRow.ClientId = eventArgs.ClientId;
            testResultsRow.Status = eventArgs.Status;
            testResultsRow.TestRuntime = eventArgs.TestRuntime;
            ResultsDataset.ThreadTestResults.Rows.Add(testResultsRow);

            //foreach (double d in eventArgs.EvaluationTimes)
            for (int i = 0; i < eventArgs.EvaluationTimes.Count; i++ )
            {
                TestResultsDataset.TestTimesRow row = ResultsDataset.TestTimes.NewTestTimesRow();
                row.ThreadNumber = eventArgs.ThreadNum;
                row.DateKey = dateKey;
                row.TestNumber = testKey;
                row.EvaluationRuntime = eventArgs.EvaluationTimes[i];
                var uowCreationTime = eventArgs.UowCreationTimes[i];
                if (uowCreationTime != null)
                {
                    row.UowCreationTime = uowCreationTime.Value;
                }
                var viewCreationTime = eventArgs.ViewCreationTimes[i];
                if (viewCreationTime != null)
                {
                    row.ViewCreationTime = viewCreationTime.Value;
                }
                var loginTime = eventArgs.LoginTimes[i];
                if (loginTime != null)
                {
                    row.LoginTime = loginTime.Value;
                }

                ResultsDataset.TestTimes.Rows.Add(row);
            }

        }

        /// <summary>
        /// Callback for when a thread is done processing.
        /// </summary>
        /// <param name="sender"></param>
        private static void RunThreadComplete(object sender)
        {
            try
            {
                _mutex.WaitOne();

                _threadCount++;

                if (_threadCount == _threadsToRun)
                {
                    _runTime = DateTime.Now - _startTime;
                    OldPrint();
                    NewPrint();
                    PrintEvalTimes();
                }
            }
            catch(Exception ex)
            {
                WriteLineToConsole(ex.Message, Settings.Default.GenerateLogFile, true, ex);
                WriteErrorLineToLog(ex.Message, Settings.Default.ProgramErrorFileName);
                WriteErrorLineToLog(ex.StackTrace, Settings.Default.ProgramErrorFileName);
            }
            finally
            {
                _mutex.ReleaseMutex();
            }
        }
         /// <summary>
        /// 
        /// </summary>
        public static void PrintEvalTimes()
         {
             if (!Settings.Default.GenerateEvaluationTimeLogFile) return;

             DeleteErrorLog(Settings.Default.EvaluationTimeLogFileName);

             foreach (RunEval run in _threads)
             {
                 StringBuilder sb = new StringBuilder();
                 sb.Append(run.ClientId).Append(",");
                 if (run.EvaluationRunTime != null && run.EvaluationRunTime.Count > 0)
                 {
                     
                     foreach (TimeSpan t in run.EvaluationRunTime)
                     {
                         sb.Append(t.TotalSeconds.ToString("N8")).Append(",");
                     }

                     WriteToErrorLog(Settings.Default.EvaluationTimeLogFileName, sb.ToString().TrimEnd(','));
                 }
             }
        }

        public static void PrintSettings()
        {
            WriteLineToConsole("--------Settings--------", Settings.Default.GenerateLogFile);
            WriteLineToConsole(
                String.Format("Sessions (Threads): {0}," +
                              "\r\nEvaluation Script: {1}" +
                              "\r\nAuth Mode: {2}" +
                              "\r\nClient Timeout: {3}" +
                              "\r\nCreate XML Datasets: {4}" +
                              "\r\nEvaluation Time Detail: {5}" +
                              "\r\nEvaluation Passes: {6}" +
                              "\r\nEvaluation Sleep: {7}"+
                              "\r\nEvaluation Log File Name: {8}" +
                              "\r\nGenerate Error Log File: {9}" +
                              "\r\nGenerate Eval Time Log File: {10}" +
                              "\r\nGenerate Log File: {11}"+
                              "\r\nLDAP Domain: {12}"+
                              "\r\nServer Url: {13}"+
                              "\r\nPrint Cookie Info: {14}"+
                              "\r\nProcess: {15}"+ 
                              "\r\nProgram Error File Name: {16}" +
                              "\r\nRandom Eval Sleep: {17}" +
                              "\r\nRandom Eval Sleep Max: {18}" +
                              "\r\nRandom Eval Sleep Min: {19}" +
                              "\r\nRole: {20}" +
                              "\r\nSession Detail: {21}"+
                              "\r\nSleep on Last Eval: {22}" +
                              "\r\nThread Sleep: {23}" +
                              "\r\nUse SSO: {24}" +
                              "\r\nUser Id: {25}" ,

                new object[] 
                    {
                        _threadsToRun.ToString("N0"), 
                        Settings.Default.tthFile,
                        Settings.Default.authMode,
                        Settings.Default.ClientTimeout.ToString("N0"),
                        Settings.Default.createXmlDatasets,
                        Settings.Default.evalTimeDetail,
                        Settings.Default.EvaluationPasses,
                        Settings.Default.EvaluationSleep.ToString("N0"),
                        Settings.Default.EvaluationTimeLogFileName,
                        Settings.Default.GenerateErrorLogFile,
                        Settings.Default.GenerateEvaluationTimeLogFile,
                        Settings.Default.GenerateLogFile,
                        Settings.Default.domain,
                        Settings.Default.PafServerStressTester_PafServiceLocal_PafService,
                        Settings.Default.PrintCookieInformation,
                        Settings.Default.process,
                        Settings.Default.ProgramErrorFileName,
                        Settings.Default.RandomEvaluationSleep,
                        Settings.Default.RandomEvaluationSleepMax.ToString("N0"),
                        Settings.Default.RandomEvaluationSleepMin.ToString("N0"),
                        Settings.Default.role,
                        Settings.Default.sessionDetail,
                        Settings.Default.sleepOnLastEval,
                        Settings.Default.ThreadSleep.ToString("N0"),
                        Settings.Default.useSingleSignOn,
                        Settings.Default.userid


                    }
                    ), Settings.Default.GenerateLogFile);
        }

        /// <summary>
        /// 
        /// </summary>
        public static void OldPrint()
        {
            WriteLineToConsole("Stress Testing Pass Summary", Settings.Default.GenerateLogFile);
            WriteLineToConsole("---------------------------", Settings.Default.GenerateLogFile);
            PrintSettings();
            WriteLineToConsole("---------------------------", Settings.Default.GenerateLogFile);
            WriteLineToConsole("--------Thread Detail--------", Settings.Default.GenerateLogFile);

            double avgTtlRunTime = 0;
            foreach (RunEval run in _threads)
            {
                WriteLineToConsole(
                    String.Format("Cid: {0}, Tid: {1}, Avg Eval RT: {2}, TTL RT: {3}, Status: {4}",
                    new object[]
                    {
                        run.ClientId,
                        run.ThreadNum.ToString("N0"),
                        run.AvgEvaluationRunTime.TotalSeconds.ToString("N8"),
                        run.RunTime.TotalSeconds.ToString("N8"),
                        run.Status.ToString()
                    }),
                    Settings.Default.GenerateLogFile);

                statsValueType r = new statsValueType { key = "AvgEvaluationRunTime", valueSpecified = true, value = Convert.ToDecimal(run.AvgEvaluationRunTime.TotalMilliseconds) };
                statsValueType r1 = new statsValueType { key = "RunTime", valueSpecified = true, value = Convert.ToDecimal(run.RunTime.TotalMilliseconds) };

                TeamCityStats.statisticValue.Add(r);
                TeamCityStats.statisticValue.Add(r1);
                //_TeamCityStats.statusInfo.status = statusType.SUCCESS;
                //_TeamCityStats.statusInfo.statusSpecified = true;


                avgTtlRunTime += run.RunTime.TotalSeconds;
                if (Settings.Default.evalTimeDetail) 
                {
                    if (run.EvaluationRunTime != null && run.EvaluationRunTime.Count > 0)
                    {
                        StringBuilder sb = new StringBuilder();
                        foreach (TimeSpan t in run.EvaluationRunTime)
                        {
                            sb.Append(t.TotalSeconds + " ");
                        }

                        WriteLineToConsole("Evaluation times: " + sb, Settings.Default.GenerateLogFile);
                    }
                }
            }
            //calc the average total run time.
            avgTtlRunTime = avgTtlRunTime / _threads.Count;

            WriteLineToConsole("--------Process Overview--------", Settings.Default.GenerateLogFile);
            // Calculate boundary times
            // initialize best and worst to 1st available.
            double bestEval = 0.0;
            double worstEval = 0.0;

            if (_threads[0].EvaluationRunTime.Count > 0)
            {
                bestEval = _threads[0].EvaluationRunTime[0].TotalSeconds;
                worstEval = _threads[0].EvaluationRunTime[0].TotalSeconds;
            }


            double avgEval = 0;
            int cntr = 0;
            int failedSessions = 0;

            foreach (RunEval r in _threads)
            {
                if (r.Status != Status.Ok) failedSessions++;
                foreach (TimeSpan t in r.EvaluationRunTime)
                {
                    if (t.TotalSeconds < bestEval) bestEval = t.TotalSeconds;
                    if (t.TotalSeconds > worstEval) worstEval = t.TotalSeconds;
                    avgEval += t.TotalSeconds;
                    cntr++;
                }
            }

            if (cntr > 0)
            {
                avgEval = avgEval/cntr;
            }

            WriteLineToConsole(
                String.Format("Total Process Runtime: {0}, " +
                              "Best Eval: {1}, " +
                              "Worst Eval: {2}, " +
                              "Avg Eval: {3}, " +
                              "Avg TTL RT: {4}",
                new object[] 
                {
                    _runTime.TotalSeconds.ToString("N8"),
                    bestEval.ToString("N8"), 
                    worstEval.ToString("N8"), 
                    avgEval.ToString("N8"),
                    avgTtlRunTime.ToString("N8")
                }),
                Settings.Default.GenerateLogFile);


            statsValueType s = new statsValueType {key = "BestEvaluation", valueSpecified = true, value = Convert.ToDecimal(bestEval)};
            statsValueType s1 = new statsValueType { key = "WorstEvaluation", valueSpecified = true, value = Convert.ToDecimal(worstEval) };
            statsValueType s2 = new statsValueType { key = "AverageEvaluation", valueSpecified = true, value = Convert.ToDecimal(avgEval) };
            statsValueType s3 = new statsValueType { key = "AverageTtlRuntime", valueSpecified = true, value = Convert.ToDecimal(avgTtlRunTime) };

            TeamCityStats.statisticValue.Add(s);
            TeamCityStats.statisticValue.Add(s1);
            TeamCityStats.statisticValue.Add(s2);
            TeamCityStats.statisticValue.Add(s3);

             WriteLineToConsole(String.Format("Failed Sessions: {0}", failedSessions.ToString("N0")), Settings.Default.GenerateLogFile);

            if (_teamCityLogger.Enabled)
            {
                TeamCityStats.SaveToFile(Path.Combine(_path, "teamcity-info.xml"));
            }

            WriteLineToConsole("---------------------------", Settings.Default.GenerateLogFile);
        }



        /// <summary>
        /// 
        /// </summary>
        public static void NewPrint()
        {
            if (!Settings.Default.createXmlDatasets) return;

            ResultsDataset.Settings.AddSettingsRow(
                _threadsToRun,
                _threadSleep,
                _evalPasses,
                _evalSleep,
                Settings.Default.tthFile,
                Settings.Default.role,
                Settings.Default.process);

            
            //_resultsDataset.PrintToCsv(_Path, "TestHarnessStats");
            
            
            FileInfo fi = new FileInfo(Settings.Default.tthFile);
            ResultsDataset.DataSetName = fi.NameWithOutExtension();


            WriteLineToConsole("Writing dataset to xml: " + ResultsDataset.DataSetName, true);
            ResultsDataset.WriteXml(Path.Combine(_path, ResultsDataset.DataSetName + ".xml"));
            
            ResultsDataset.Settings.ToHtml(Path.Combine(_path, fi.Name + ".settings.html"));
            //_resultsDataset.Tests.ToHtml(Path.Combine(_Path, fi.Name + ".tests.html"));
            ResultsDataset.GetNonNormalizedTestResults().ToHtml(Path.Combine(_path, fi.Name + ".testresults.html"));
            ResultsDataset.GetNonNormalizedThreadTestResults().ToHtml(Path.Combine(_path, fi.Name + ".threadtestresults.html"));
            ResultsDataset.GetNonNormalizedTestTimes().ToHtml(Path.Combine(_path, fi.Name + ".testtimes.html"));
            ResultsDataset.GetNonNormalizedTestResultsComparison().ToHtml(Path.Combine(_path, fi.Name + ".testresultscomparison.html"));

        }


        /// <summary>
        /// Write a error line error log (Thread Safe).
        /// </summary>
        /// <param name="logFile">true to write the string to the log file/false to not.</param>
        /// <param name="str">String to write to the console window/log.</param>
        public static void WriteErrorLineToLog(string str, string logFile)
        {
            if (String.IsNullOrEmpty(str)) return;
            WriteToErrorLog(logFile, str);
        }


        public static void WriteTestStart(string str)
        {
            _teamCityLogger.TestStarted(str);
        }

        public static void WriteTestSuiteStatistics(string statisticsKey, int statisticsValue)
        {
            _teamCityLogger.TestSuiteStatistics(statisticsKey, statisticsValue);
        }

        public static void WriteTestEnd(string str, long elapsedMilliseconds)
        {
            _teamCityLogger.TestFinished(str, elapsedMilliseconds);
        }

        public static void WriteTestFailed(string testType, string testName,  string errorMessage)
        {
            _teamCityLogger.TestFailed(testType, testName,  errorMessage);
        }

        public static void WriteTestSuiteStart(string testSuiteName)
        {
            _teamCityLogger.TestSuiteStarted(testSuiteName);
        }

        public static void WriteTestSuiteFinished(string testSuiteName)
        {
            _teamCityLogger.TestSuiteFinished(testSuiteName);
        }

        /// <summary>
        /// Write a line to the console and log.
        /// </summary>
        /// <param name="str">String to write to the console window/log.</param>
        /// <param name="log">true to write the string to the log file/false to not.</param>
        /// <param name="isError">true if this is an error.</param>
        /// <param name="stackTrace"> </param>
        public static void WriteLineToConsole(string str, bool log, bool isError = false, Exception stackTrace= null)
        {
            if (String.IsNullOrEmpty(str)) return;
            if(isError && stackTrace == null)
            {
                stackTrace = new Exception();
            }
            
            if (log)
            {
                if (isError)
                {
                    _logger.Error(str, stackTrace);
                }
                else
                {
                    _logger.Info(str);
                }
            }

            if(!_teamCityLogger.Enabled)
            {
                if (isError)
                {
                    Console.Error.WriteLine(DateTime.Now.ToLongTimeString() + " : " + str);
                }
                else
                {
                    Console.WriteLine(DateTime.Now.ToLongTimeString() + " : " + str);
                }
            }
            else
            {
                if (isError)
                {
                    _teamCityLogger.TestMessage(str, TeamCityLogger.TeamCityMessageStatus.ERROR, stackTrace.StackTrace);
                }
                else
                {
                    _teamCityLogger.TestMessage(str);
                }
            }
        }

        /// <summary>
        /// Write output to the console and log.
        /// </summary>
        /// <param name="str">String to write to the console window/log.</param>
        /// <param name="log">true to write the string to the log file/false to not.</param>
        /// <param name="isError">true if this is an error.</param>
        public static void WriteToConsole(string str, bool log, bool isError)
        {
            Console.Write(str);
            if (log)
            {
                if (isError)
                {
                    _logger.Error(str);
                }
                else
                {
                    _logger.Info(str);
                }
            }
        }

        /// <summary>
        /// Deletes an error (Thread Safe).
        /// </summary>
        /// <param name="errorFilePath">Path to the error file.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void DeleteErrorLog(string errorFilePath)
        {
            try
            {
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + Path.DirectorySeparatorChar + errorFilePath;
                path = path.Replace("file:\\", "");
                File.Delete(path);
            }
            catch (Exception ex)
            {
                WriteLineToConsole(ex.Message, Settings.Default.GenerateLogFile, true, ex);
            }
        }


        /// <summary>
        /// Write a error line error log (Thread Safe).
        /// </summary>
        /// <param name="errorFilePath">Path to the error file.</param>
        /// <param name="str">String to write.</param>
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void WriteToErrorLog(string errorFilePath, string str)
        {
            //Monitor.Enter(errorFilePath);
            try
            {
                //string path = System.Reflection.Assembly.GetExecutingAssembly().Location + @"\" + errorFilePath;
                string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + Path.DirectorySeparatorChar + errorFilePath;
                path = path.Replace("file:\\", "");
                File.AppendAllText(path, str + Environment.NewLine, Encoding.UTF8);
            }
            catch (Exception ex)
            {
                WriteLineToConsole(ex.Message, Settings.Default.GenerateLogFile, true, ex);
                //Monitor.Exit(errorFilePath);
            }
        }
    }
}
