﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Concurrent;
using System.Data;
using ServerStressTesterBase.Pace.Chart.Classes;
using ServerStressTesterBase.Pace.Chart.Enums;
using ServerStressTesterBase.Pace.Chart.Interfaces;

namespace ServerStressTesterUiArtifacts.Code.Classes
{
    public class ChartSeriesUi<X, Y> : IChartSeries<X, Y>
    {

        private ConcurrentBag<ChartData<X, Y>> _data;

        public ChartSeriesUi(string name = "", bool showLegend = false, bool showPointLabels = false, bool showLineMarkers = true, 
            SeriesSortingMode seriesPointsSorting = SeriesSortingMode.None,  SeriesPointKey seriesPointsSortingKey = SeriesPointKey.Argument, 
            ChartType seriesViewType = ChartType.Default, string yColumnName = "", string xColumnName = "", string zColumnName = "", 
            string dataTableLabelColumnName = null, string filterExpression = "")
        {

            Name = name;
            ValueY = new SeriesAxis();
            ArgumentX = new SeriesAxis();
            ShowLegend = showLegend;
            ShowLineMarkers = showLineMarkers;
            ShowPointLabels = showPointLabels;
            SeriesPointsSorting = seriesPointsSorting;
            SeriesPointsSortingKey = seriesPointsSortingKey;
            DataTableXColumnName = xColumnName;
            DataTableYColumnName = yColumnName;
            DataTableZColumnName = zColumnName;
            DataTableLabelColumnName = dataTableLabelColumnName;
            FilterExpression = filterExpression;
            if(seriesViewType != ChartType.Default)
            {
                SeriesViewType = seriesViewType;
            }
            _data = new ConcurrentBag<ChartData<X, Y>>();

        }


        #region Implementation of IChartSeries

        public long GetTotalDataPoints()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Data set of data.
        /// </summary>
        public DataSet DataSet { get; protected set; }

        /// <summary>
        /// Name of the column in the data table that contains the label for the points
        /// </summary>
        public string DataTableLabelColumnName { get; set; }

        /// <summary>
        /// Name of the column in the data table that contains the data.
        /// </summary>
        public string DataTableXColumnName { get; set; }

        /// <summary>
        /// Name of the column in the data table that contains the data.
        /// </summary>
        public string DataTableYColumnName { get; set; }

        /// <summary>
        /// Name of the column in the data table that contains the data.
        /// </summary>
        public string DataTableZColumnName { get; set; }

        public string DataTableRowIdColumnName { get; set; }

        /// <summary>
        /// Expression to use to filter the dataset.
        /// </summary>
        public string FilterExpression { get; set; }

        /// <summary>
        /// View type to display on the series.
        /// </summary>
        public ChartType SeriesViewType { get; set; }

        /// <summary>
        /// Name of the series.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Show in chart legend.
        /// </summary>
        public bool ShowLegend { get; set; }

        /// <summary>
        /// Show point labels.
        /// </summary>
        public bool ShowPointLabels { get; set; }

        /// <summary>
        /// Displays the line markers (dots, boxes, etc) on the series.
        /// </summary>
        public bool ShowLineMarkers { get; set; }

        /// <summary>
        /// Sort order of the series.
        /// </summary>
        public SeriesSortingMode SeriesPointsSorting { get; set; }

        /// <summary>
        /// Gets or sets a value that specifies the values of the data points that the series should be sorted by.
        /// </summary>
        public SeriesPointKey SeriesPointsSortingKey { get; set; }

        /// <summary>
        ///// View type to display on the series.
        /// </summary>
        public ChartData<X, Y>[] Data
        {
            get { return _data.ToArray(); }
            protected set { _data = new ConcurrentBag<ChartData<X, Y>>(value); }
        }

        /// <summary>
        /// Max value for the x-axis.
        /// </summary>
        public X MaxArgument { get; set; }

        /// <summary>
        /// Min value for the x-axis.
        /// </summary>
        public X MinArgument { get; set; }

        /// <summary>
        /// Max value for the y-axis.
        /// </summary>
        public Y MaxValue { get; set; }

        /// <summary>
        /// Min value for the y-axis.
        /// </summary>
        public Y MinValue { get; set; }

        /// <summary>
        /// Y-Axis on the series.
        /// </summary>
        public SeriesAxis ValueY { get; set; }

        /// <summary>
        /// X-Axis on the series.
        /// </summary>
        public SeriesAxis ArgumentX { get; set; }

        #endregion
    }
}
