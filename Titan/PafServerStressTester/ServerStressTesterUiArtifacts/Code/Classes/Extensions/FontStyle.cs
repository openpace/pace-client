﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Drawing;

namespace ServerStressTesterUiArtifacts.Code.Classes.Extensions
{
    public static class FontStyleExtension
    {
        /// <summary>
        /// Converts a AxisScaleType to ScaleType
        /// </summary>
        /// <param name="x">AxisScaleType to extend.</param>
        /// <returns>DevExpress.XtraCharts.ScaleType</returns>
        public static FontStyle GetFontStyle(this string x)
        {
            return (FontStyle)Enum.Parse(typeof(FontStyle), x);
        }
    }
}