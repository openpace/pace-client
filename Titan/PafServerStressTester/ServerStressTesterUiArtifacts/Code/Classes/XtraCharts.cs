﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using DevExpress.XtraCharts;
using ServerStressTesterBase.Pace.Chart.Classes;
using ServerStressTesterBase.Pace.Chart.Enums;
using ServerStressTesterUiArtifacts.Code.Classes;
using ServerStressTesterUiArtifacts.Code.Classes.Extensions;

namespace ServerStressTesterUiArtifacts
{
    public class XtraCharts
    {
        public ChartControl ChartControl { get; set; }

        public object DataSource { get; set; }

        public  ChartTitleInfo ChartTitleInfo { get; set; }

        public ChartSeriesUi<double, double> ChartSeries { get; set; }


        private void Init(ChartTitleInfo chartTitleInfo)
        {

            XYDiagram xyDiagram1 = new XYDiagram();
            Series series1 = new Series();
            PointSeriesLabel pointSeriesLabel1 = new PointSeriesLabel();
            PointSeriesView pointSeriesView1 = new PointSeriesView();
            PointSeriesLabel pointSeriesLabel2 = new PointSeriesLabel();
            PointSeriesView pointSeriesView2 = new PointSeriesView();
            ChartControl = new ChartControl();


            ((System.ComponentModel.ISupportInitialize)(ChartControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView2)).BeginInit();

            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            ChartControl.Diagram = xyDiagram1;
            ChartControl.Legend.Visible = false;
            //this.chartControl1.Location = new System.Drawing.Point(2, 1);
            ChartControl.Name = "ChartControl";
            series1.ArgumentDataMember = "Date";
            series1.ArgumentScaleType = ScaleType.DateTime;
            pointSeriesLabel1.LineVisible = true;
            pointSeriesLabel1.Visible = false;
            series1.Label = pointSeriesLabel1;
            series1.Name = "Series 1";
            series1.ValueDataMembersSerializable = "AverageEvaluation";
            series1.View = pointSeriesView1;


            xyDiagram1.AxisX.DateTimeGridAlignment = DateTimeMeasurementUnit.Day;
            xyDiagram1.AxisX.DateTimeMeasureUnit = DateTimeMeasurementUnit.Day;
            xyDiagram1.AxisX.DateTimeOptions.Format = DateTimeFormat.Custom;
            xyDiagram1.AxisX.DateTimeOptions.FormatString = "MM/dd/yy";

            ChartControl.SeriesSerializable = new [] {series1};
            pointSeriesLabel2.LineVisible = true;
            ChartControl.SeriesTemplate.Label = pointSeriesLabel2;
            ChartControl.SeriesTemplate.View = pointSeriesView2;
            ChartControl.Size = new System.Drawing.Size(1024, 768);
            ChartControl.TabIndex = 2;

            
            if (!String.IsNullOrEmpty(chartTitleInfo.Text))
            {
                ChartControl.Titles.Clear();
                ChartTitle chartTitle1 = new ChartTitle
                {
                    Text = chartTitleInfo.Text,
                    Alignment = chartTitleInfo.Alignment.GetStringAlignment(),
                    Dock = chartTitleInfo.Dock.GetChartTitleDockStyle()
                };

                Font f = new Font(chartTitleInfo.FontName, chartTitleInfo.FontSize, chartTitleInfo.FontStyle.GetFontStyle());

                chartTitle1.Antialiasing = chartTitleInfo.Antialiasing;
                chartTitle1.Font = f;
                chartTitle1.Indent = chartTitleInfo.Indent;

                ChartControl.Titles.AddRange(new[] { chartTitle1 });
            }


            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(ChartControl)).EndInit();

        }

        private void CreatePointChart()
        {

            XYDiagram xyDiagram1 = new XYDiagram();

            ViewType type = ViewType.Point;
            if(ChartSeries.SeriesViewType != ChartType.Default)
            {
                type = ChartSeries.SeriesViewType.GetViewType();
            }

            Series series1 = new Series(ChartSeries.Name, type );
            PointSeriesLabel pointSeriesLabel1 = new PointSeriesLabel();
            PointSeriesView pointSeriesView1 = new PointSeriesView();
            PointSeriesLabel pointSeriesLabel2 = new PointSeriesLabel();
            PointSeriesView pointSeriesView2 = new PointSeriesView();
            ChartControl = new ChartControl();


            ((System.ComponentModel.ISupportInitialize)(ChartControl)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView2)).BeginInit();

            xyDiagram1.AxisX.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisX.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisX.VisibleInPanesSerializable = "-1";
            xyDiagram1.AxisY.Range.ScrollingRange.SideMarginsEnabled = true;
            xyDiagram1.AxisY.Range.SideMarginsEnabled = true;
            xyDiagram1.AxisY.VisibleInPanesSerializable = "-1";
            ChartControl.Diagram = xyDiagram1;
            ChartControl.Legend.Visible = false;
            //this.chartControl1.Location = new System.Drawing.Point(2, 1);
            ChartControl.Name = "ChartControl";
            series1.ArgumentDataMember = ChartSeries.DataTableXColumnName;// "Date";
            series1.ArgumentScaleType = ScaleType.DateTime;
            pointSeriesLabel1.LineVisible = true;
            pointSeriesLabel1.Visible = false;
            series1.Label = pointSeriesLabel1;
            series1.Name = "Series 1";
            series1.ValueDataMembersSerializable = ChartSeries.DataTableYColumnName;// "AverageEvaluation";
            series1.View = pointSeriesView1;


            xyDiagram1.AxisX.DateTimeGridAlignment = DateTimeMeasurementUnit.Day;
            xyDiagram1.AxisX.DateTimeMeasureUnit = DateTimeMeasurementUnit.Day;
            xyDiagram1.AxisX.DateTimeOptions.Format = DateTimeFormat.Custom;
            xyDiagram1.AxisX.DateTimeOptions.FormatString = "MM/dd/yy";

            ChartControl.SeriesSerializable = new[] { series1 };
            pointSeriesLabel2.LineVisible = true;
            ChartControl.SeriesTemplate.Label = pointSeriesLabel2;
            ChartControl.SeriesTemplate.View = pointSeriesView2;
            ChartControl.Size = new System.Drawing.Size(1024, 768);
            ChartControl.TabIndex = 2;


            if (!String.IsNullOrEmpty(ChartTitleInfo.Text))
            {
                ChartControl.Titles.Clear();
                ChartTitle chartTitle1 = new ChartTitle
                {
                    Text = ChartTitleInfo.Text,
                    Alignment = ChartTitleInfo.Alignment.GetStringAlignment(),
                    Dock = ChartTitleInfo.Dock.GetChartTitleDockStyle()
                };

                Font f = new Font(ChartTitleInfo.FontName, ChartTitleInfo.FontSize, ChartTitleInfo.FontStyle.GetFontStyle());

                chartTitle1.Antialiasing = ChartTitleInfo.Antialiasing;
                chartTitle1.Font = f;
                chartTitle1.Indent = ChartTitleInfo.Indent;

                ChartControl.Titles.AddRange(new[] { chartTitle1 });
            }

            ChartControl.DataSource = DataSource;

            ((System.ComponentModel.ISupportInitialize)(xyDiagram1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(series1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(pointSeriesView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(ChartControl)).EndInit();

        }

        public XtraCharts(DataTable dataTable, ChartTitleInfo chartTitleInfo, ChartSeriesUi<double, double> chartSeries)
        {
            DataSource = dataTable;
            ChartTitleInfo = chartTitleInfo;
            ChartSeries = chartSeries;
        }

        public void GeneratePointChart()
        {
            CreatePointChart();
        }


        public void ExportToImage(string path, ImageFormat format = null)
        {
            if(format == null)
            {
                format = ImageFormat.Png;
            }

            ChartControl.ExportToImage(path, format);
        }

    }
}
