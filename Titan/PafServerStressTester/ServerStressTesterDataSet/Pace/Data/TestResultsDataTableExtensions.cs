﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ServerStressTesterBase.Pace.Extensions;

namespace ServerStressTesterDataSet.Pace.Data
{
    public static class TestResultsDataTableExtensions
    {
        /// <summary>
        /// Gets the worst evaluation from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetWorstEvaluation(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Max(y => y.WorstEvaluation);
        }

        /// <summary>
        /// Gets the bets evaluation from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetBestEvaluation(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Min(y => y.BestEvaluation);
        }

        /// <summary>
        /// Gets the best average evaluation from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetAvgEvaluationMax(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Max(y => y.AverageEvaluation);
        }

        /// <summary>
        /// Gets the best average evaluation from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetAvgEvaluationMin(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Min(y => y.AverageEvaluation);
        }

        /// <summary>
        /// Gets the worst total runtime from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetTotalRuntimeMax(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Max(y => y.TotalTestRuntime);
        }

        /// <summary>
        /// Gets the best total runtime from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetTotalRuntimeMin(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Min(y => y.TotalTestRuntime);
        }

        /// <summary>
        /// Gets the worst uow runtime from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetAvgUowCreationTimeMax(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Max(y => y.AvgUowCreationTime);
        }

        /// <summary>
        /// Gets the best uow runtime from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetAvgUowCreationTimeMin(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Min(y => y.AvgUowCreationTime);
        }

        /// <summary>
        /// Gets the worst view runtime from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetAvgViewCreationTimeMax(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Max(y => y.AvgViewCreationTime);
        }

        /// <summary>
        /// Gets the best uow view from a IEnumerable
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static double GetAvgViewCreationTimeMin(this IEnumerable<TestResultsDataset.TestResultsRow> x)
        {
            return x.Min(y => y.AvgViewCreationTime);
        }


        /// <param name="avgEvalGraphPath"></param>
        public static void ToHtml(this TestResultsDataset.TestResultsDataTable dataTable, string testName, string pathFileName, 
            double evaluationMin, double evaluationMax, double avgEval, double totalRuntime, double uowRuntime, double viewRuntime,
            double evaluationMinOverall, double evaluationMaxOverall, double avgEvalMinOverall, double avgEvalMaxOverall, double totalRuntimeMin, double totalRuntimeMax, double uowMin, double uowMax, double viewMin, double viewMax,
            string avgEvalGraphPath, string allEvalGraphPath)
        {

            StringBuilder sb = new StringBuilder();

            sb.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.Append("<head>");
            sb.Append("<title>");
            sb.Append("Page-");
            sb.Append(Guid.NewGuid().ToString());
            sb.Append("</title>");
            sb.Append("</head>");
            sb.Append("<body>");

            sb.AppendFormat(@"<H1>");
            sb.AppendFormat(testName);
            sb.AppendFormat(@" </H1>");


            sb.Append("<p style=\"font-size: small; font-weight: normal\">" + string.Format("{0:N0} runs", dataTable.Rows.Count) + "</p>");


            sb.Append("&nbsp;");

            sb.Append(dataTable.CreateCurrentSummaryHtml("Current", evaluationMin, evaluationMax, avgEval, totalRuntime, uowRuntime, viewRuntime));


            sb.Append("<br />");

            sb.Append(dataTable.CreateOverallSummaryHtml("Overall", evaluationMinOverall, evaluationMaxOverall, avgEvalMinOverall, avgEvalMaxOverall, totalRuntimeMin, totalRuntimeMax, uowMin, uowMax, viewMin, viewMax));


            sb.Append("<br />");


            sb.Append("<img alt=\"\" src=\"" + avgEvalGraphPath + "\" style=\"height: 480px; width: 640px\" /></p>");

            sb.Append("<br />");


            sb.Append("<img alt=\"\" src=\"" + allEvalGraphPath + "\" style=\"height: 480px; width: 640px\" /></p>");
            
            sb.Append("</body>");
            sb.Append("</html>");


            sb.ToFile(pathFileName);
        }


        public static string CreateCurrentSummaryHtml(this TestResultsDataset.TestResultsDataTable dataTable, string timeFrame, 
            double evaluationMin, double evaluationMax, double avgEval, double totalRuntime, double uowRuntime, double viewRuntime)
        {

            StringBuilder sb = new StringBuilder();

            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TH>&nbsp;</TH>");
            sb.Append("<TH>");
            sb.Append(timeFrame);
            sb.Append("</TH>");
            sb.Append("</TR>");



            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>Avg. Evaluation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", avgEval));
            sb.Append("</TD>");
            sb.Append("</TR>");


            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>Best Evaluation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", evaluationMax));
            sb.Append("</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>Worst Evaluation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", evaluationMax));
            sb.Append("</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>Ttl Runtime</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", totalRuntime));
            sb.Append("</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>UOW Creation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", uowRuntime));
            sb.Append("</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>View Creation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", viewRuntime));
            sb.Append("</TD>");
            sb.Append("</TR>");


            sb.Append("</TABLE>");


            return sb.ToString();
        }

        public static string CreateOverallSummaryHtml(this TestResultsDataset.TestResultsDataTable dataTable, string timeFrame,
            double evaluationMin, double evaluationMax, double avgEvalMin, double avgEvalMax, double totalRuntimeMin, double totalRuntimeMax, double uowMin, double uowMax, double viewMin, double viewMax)
        {

            StringBuilder sb = new StringBuilder();

            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TH>");
            sb.Append(timeFrame);
            sb.Append("</TH>");
            sb.Append("<TH>Min</TH>");
            sb.Append("<TH>Max</TH>");
            sb.Append("</TR>");



            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>Avg. Evaluation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", avgEvalMin));
            sb.Append("</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", avgEvalMax));
            sb.Append("</TD>");
            sb.Append("</TR>");


            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>Evaluation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", evaluationMin));
            sb.Append("</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", evaluationMax));
            sb.Append("</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>Ttl Runtime</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", totalRuntimeMin));
            sb.Append("</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", totalRuntimeMax));
            sb.Append("</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>UOW Creation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", uowMin));
            sb.Append("</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", uowMax));
            sb.Append("</TD>");
            sb.Append("</TR>");

            sb.Append("<TR ALIGN='CENTER'>");
            sb.Append("<TD>View Creation</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", viewMin));
            sb.Append("</TD>");
            sb.Append("<TD>");
            sb.Append(string.Format("{0:N0}", viewMax));
            sb.Append("</TD>");
            sb.Append("</TR>");


            sb.Append("</TABLE>");


            return sb.ToString();
        }
    }
}
