﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Data;
using ServerStressTesterBase.Pace.Extensions;

namespace ServerStressTesterDataSet.Pace.Data
{
    public static class TestsDataTableExtension
    {
        /// <summary>
        /// Saves a dataset to an html file.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="pathFileName">Full path file name to save the file.</param>
        public static void ToHtml(this TestResultsDataset.TestsDataTable dataTable, string pathFileName)
        {

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.Append("<head>");
            sb.Append("<title>");
            sb.Append("Page-");
            sb.Append(Guid.NewGuid().ToString());
            sb.Append("</title>");
            sb.Append("</head>");
            sb.Append("<body>");


            sb.Append(dataTable.ToHtml());

            sb.Append("&nbsp;");
            sb.Append("&nbsp;");


            sb.Append("</body>");
            sb.Append("</html>");


            sb.ToFile(pathFileName);

        }

        public static string ToHtml(this TestResultsDataset.TestsDataTable dataTable)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.AppendFormat(@"<H2>");
            sb.AppendFormat(dataTable.TableName);
            sb.AppendFormat(@"</H2>");

            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");

            sb.Append("<TR ALIGN='CENTER'>");

            //first append the column names.
            foreach (DataColumn column in dataTable.Columns)
            {
                sb.Append("<TH>");
                sb.Append(column.ColumnName);
                sb.Append("</TH>");
            }

            sb.Append("</TR>");

            // next, the column values.
            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append("<TR ALIGN='CENTER'>");

                foreach (DataColumn column in dataTable.Columns)
                {
                    sb.Append("<TD>");
                    if (row[column].ToString().Trim().Length > 0)
                    {
                        if (column.ColumnName == "TestName")
                        {
                            sb.Append("<a href=\"" + Uri.EscapeUriString(row[column].ToString()) + ".html\"><fontstyle=\"font-size:medium;\">" + row[column].ToString()+ "</font></a><br>");
                        }
                        else
                        {
                            sb.Append(row[column]);
                        }
                    }
                    else
                    {
                        sb.Append("&nbsp;");
                    }
                    sb.Append("</TD>");
                }

                sb.Append("</TR>");
            }
            sb.Append("</TABLE>");



            return sb.ToString();
        }
    }
}
