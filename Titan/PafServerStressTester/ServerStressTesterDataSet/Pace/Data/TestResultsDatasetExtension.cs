﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ServerStressTesterBase.Pace.Extensions;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterDataSet.Pace.Data
{
    public static class TestResultsDatasetExtension
    {

        //OrderedEnumerableRowCollection<TestResultsDataset.TestsRow> query = null;

        /// <summary>
        /// Loads a TestResultsDataset from an xml file.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static TestResultsDataset Load(string path, string fileName)
        {
            string pathFileName = Path.Combine(path, fileName);
            return Load(pathFileName);
        }


        /// <summary>
        /// Loads a TestResultsDataset from an xml file.
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static TestResultsDataset Load(string fileName)
        {
            TestResultsDataset ds = new TestResultsDataset();
            if (File.Exists(fileName))
            {
                ds.ReadXml(fileName);
            }
            return ds;
        }

        /// <summary>
        /// Loads a TestResultsDataset from an xml string.
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static TestResultsDataset LoadFromXml(string xml)
        {
            Stopwatch sw = Stopwatch.StartNew();

            TestResultsDataset ds = new TestResultsDataset();
            if (!String.IsNullOrEmpty(xml))
            {
                using(StringReader sr = new StringReader(xml))
                {
                    ds.ReadXml(sr, XmlReadMode.ReadSchema);
                }
            }

            sw.Stop();
            Logger.Instance.DebugFormat("LoadFromXml runtime: {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });

            return ds;
        }

        /// <summary>
        /// Finds a test by name.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="testName"></param>
        /// <returns></returns>
        public static TestResultsDataset.TestsRow FindByTestName(this TestResultsDataset ds, string testName)
        {
            return ds.Tests.FirstOrDefault(x => x.TestName == testName);
        }

        /// <summary>
        /// Finds a test by name.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="testNumber"></param>
        /// <param name="dateKey"></param>
        /// <returns></returns>
        public static List<TestResultsDataset.TestResultsRow> FindTestResults(this TestResultsDataset ds, int testNumber,ulong dateKey)
        {
            return (from x in ds.TestResults.AsEnumerable() where x.TestNumber == testNumber && x.DateKey == dateKey select x).ToList();
        }

        /// <summary>
        /// Finds a test by name.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="testNumber"></param>
        /// <param name="orderByTestNumber"></param>
        /// <param name="orderByDate"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        public static List<TestResultsDataset.TestResultsRow> FindTestResults(this TestResultsDataset ds, int testNumber,  bool orderByTestNumber = false, bool orderByDate = false, bool ascending = true)
        {
            if(orderByTestNumber)
            {
                var lst =  (from x in ds.TestResults.AsEnumerable() where x.TestNumber == testNumber orderby x.TestNumber ascending select x).ToList();
                if(!ascending)
                {
                    lst.Reverse();
                }
                return lst;
            }
            if (orderByDate)
            {
                var lst = (from x in ds.TestResults.AsEnumerable() where x.TestNumber == testNumber orderby x.DateKey ascending select x).ToList();
                if (!ascending)
                {
                    lst.Reverse();
                }
                return lst;
            }


            return (from x in ds.TestResults.AsEnumerable() where x.TestNumber == testNumber  select x).ToList();
        }

        /// <summary>
        /// Gets all the test results.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="testNumber"></param>
        /// <returns></returns>
        public static EnumerableRowCollection<TestResultsDataset.TestTimesRow> GetTestTimeResults(this TestResultsDataset ds, int testNumber)
        {
            return (from x in ds.TestTimes.AsEnumerable() where x.TestNumber == testNumber select x);
        }

         /// <summary>
        /// Gets all the test results.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="orderByTestNumber"></param>
        /// <param name="orderByDate"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        public static EnumerableRowCollection<TestResultsDataset.TestResultsRow> GetTestResults(this TestResultsDataset ds, bool orderByTestNumber = false, bool orderByDate = true, bool ascending = false)
        {
            if (orderByTestNumber)
            {
                var lst = (from x in ds.TestResults.AsEnumerable() orderby x.TestNumber ascending select x);
                return lst;
            }
            if (orderByDate)
            {
                var lst = (from x in ds.TestResults.AsEnumerable() orderby x.DatesRow.Date ascending select x);
                return lst;
            }


            return (from x in ds.TestResults.AsEnumerable() select x);
        }

        /// <summary>
        /// Gets all the test results.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="orderByTestNumber"></param>
        /// <param name="orderByDate"></param>
        /// <param name="ascending"></param>
        /// <returns></returns>
        public static List<TestResultsDataset.TestResultsRow> GetTestResultsList(this TestResultsDataset ds, bool orderByTestNumber = false, bool orderByDate = true, bool ascending = false)
        {
            var query = GetTestResults(ds, orderByTestNumber, orderByDate, ascending);
            if (orderByTestNumber)
            {
                return !@ascending ? query.Reverse().ToList() : query.ToList();
            }
            if (orderByDate)
            {
                return !@ascending ? query.Reverse().ToList() : query.ToList();
            }

            return query.ToList();
        }

        /// <summary>
        /// Finds a test by name.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static TestResultsDataset.DatesRow FindByDate(this TestResultsDataset ds, DateTime dateTime)
        {
            return ds.Dates.FirstOrDefault(x => x.Date == dateTime);
        }


        /// <summary>
        /// Gets the dates datatable.
        /// </summary>
        /// <returns></returns>
        public static DataTable DatesDescending(this TestResultsDataset.DatesDataTable ds)
        {
            return (from x in ds.AsEnumerable() orderby x.Date descending select x).CopyToDataTable();
        }

        /// <summary>
        /// Finds a test by name.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static List<TestResultsDataset.DatesRow> GetDates(this TestResultsDataset ds, int? number = null)
        {
            if (number.HasValue)
            {
                return (from x in ds.Dates.AsEnumerable() orderby x.Date descending select x).Take(number.Value).ToList();
            }
            return (from x in ds.Dates.AsEnumerable() orderby x.Date descending select x).ToList();
        }

        /// <summary>
        /// Gets the TestsRows
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="number"></param>
        /// <param name="orderByTestNumber"></param>
        /// <returns></returns>
        public static List<TestResultsDataset.TestsRow> GetTests(this TestResultsDataset ds, int? number = null, bool orderByTestNumber = true)
        {

            //OrderedEnumerableRowCollection<TestResultsDataset.TestsRow>
            var  query = (from x in ds.Tests.AsEnumerable() orderby x.TestNumber ascending select x);

            return number.HasValue ? query.Take(number.Value).ToList() : query.ToList();
        }

        public static void AddTestReultsEntry(this TestResultsDataset ds, int testKey, ulong dateKey)
        {
            List<TestResultsDataset.TestResultsRow> tr = ds.FindTestResults(testKey, dateKey);

            if(tr == null || tr.Count == 0)
            {
                TestResultsDataset.TestResultsRow sr = ds.TestResults.NewTestResultsRow();
                sr.TestNumber = testKey;
                sr.DateKey = dateKey;
                ds.TestResults.AddTestResultsRow(sr);
            }
        }

        /// <summary>
        /// Gets a tests key.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="testName"></param>
        /// <param name="dateTime"></param>
        /// <param name="addTestIfNotFound">Add the test if the name is not found.</param>
        /// <returns></returns>
        public static int GetTestKey(this TestResultsDataset ds, string testName, DateTime dateTime, bool addTestIfNotFound = true)
        {
            TestResultsDataset.TestsRow tr = ds.FindByTestName(testName);
            if(tr == null)
            {
                if (addTestIfNotFound)
                {
                    TestResultsDataset.TestsRow newRow = ds.Tests.AddTestsRow(testName);

                    ulong dateKey = ds.GetDateKey(dateTime);
                    TestResultsDataset.TestResultsRow sr = ds.TestResults.NewTestResultsRow();
                    sr.TestNumber = newRow.TestNumber;
                    sr.DateKey = dateKey;
                    ds.TestResults.AddTestResultsRow(sr);

                    return newRow.TestNumber;
                }
                return -1;
            }

            return tr.TestNumber;
        }

        /// <summary>
        /// Gets a date key.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="dateTime"></param>
        /// <param name="addTestIfNotFound">Add the date if the name is not found.</param>
        /// <returns></returns>
        public static ulong GetDateKey(this TestResultsDataset ds, DateTime dateTime, bool addTestIfNotFound = true)
        {

            TestResultsDataset.DatesRow tr = ds.FindByDate(dateTime);
            if (tr == null)
            {
                if (addTestIfNotFound)
                {
                    TestResultsDataset.DatesRow newRow = ds.Dates.AddDatesRow(IntegerExtension.GenerateUniqueULong(), dateTime);

                    return newRow.DateKey;
                }
                return 0;
            }

            return tr.DateKey;
        }

        /// <summary>
        /// Add rows to the TestResultsComparison table.  This is called post Merge()
        /// </summary>
        /// <param name="x">TestResultsDataset</param>
        public static void CreateComparison(this TestResultsDataset x)
        {
            List<TestResultsDataset.TestsRow> tests = x.GetTests();

            foreach (TestResultsDataset.TestsRow testRow in tests)
            {

                TestResultsDataset.TestResultsComparisonRow newRow = x.TestResultsComparison.NewTestResultsComparisonRow();
                int i = 1;
                List<TestResultsDataset.TestResultsRow> results = x.FindTestResults(testRow.TestNumber, orderByTestNumber: false, orderByDate: true, ascending: false);
                foreach (TestResultsDataset.TestResultsRow result in results)
                {

                    if (i == 1)
                    {
                        newRow.TestNumber = result.TestNumber;
                        newRow.BestEvaluation = result.BestEvaluation;
                        newRow.WorstEvaluation = result.WorstEvaluation;
                        newRow.AverageEvaluation = result.AverageEvaluation;
                        newRow.TotalTestRuntime = result.TotalTestRuntime;
                    }
                    else
                    {
                        newRow.TestNumber = result.TestNumber;
                        newRow.BestEvaluationPrev = result.BestEvaluation;
                        newRow.WorstEvaluationPrev = result.WorstEvaluation;
                        newRow.AverageEvaluationPrev = result.AverageEvaluation;
                        newRow.TotalTestRuntimePrev = result.TotalTestRuntime;
                    }
                    i++;

                }
                x.TestResultsComparison.AddTestResultsComparisonRow(newRow);
            }
        }


        /// <summary>
        /// Gets the TestResults table joined with its parent(s) tables.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetNonNormalizedTestResults(this TestResultsDataset ds)
        {
            return ds.TestResults.GetNonNormalizedTestResults(null);

        }

        /// <summary>
        /// Gets the TestResults table joined with its parent(s) tables.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="parent">Parent dataset for joining </param>
        /// <returns>DataTable</returns>
        public static DataTable GetNonNormalizedTestResults(this TestResultsDataset.TestResultsDataTable ds, TestResultsDataset parent)
        {
            if (parent == null)
            {
                parent = (TestResultsDataset) ds.DataSet;
            }


            var query = from results in ds.AsEnumerable()
                        join tests in parent.Tests.AsEnumerable() on results.TestNumber equals tests.TestNumber
                        join dates in parent.Dates.AsEnumerable() on results.DateKey equals dates.DateKey
                        select new
                        {
                            tests.TestName,
                            dates.Date,
                            //results.TestsRow.TestName,
                            //results.DatesRow.Date,
                            results.BestEvaluation,
                            results.WorstEvaluation,
                            results.AverageEvaluation,
                            results.TotalTestRuntime,
                            results.AvgUowCreationTime,
                            results.AvgViewCreationTime
                        };


            DataTable dt = query.CopyToDataTableEx();
            dt.TableName = ds.TableName;

            return dt;

        }


        /// <summary>
        /// Gets the TestResults table joined with its parent(s) tables.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetNonNormalizedTestResultsComparison(this TestResultsDataset ds)
        {
            var query = from results in ds.TestResultsComparison.AsEnumerable()
                        //join tests in ds.Tests.AsEnumerable() on results.TestNumber equals tests.TestNumber
                        select new
                        {
                            //tests.TestName,
                            results.TestsRow.TestName,
                            results.BestEvaluation,
                            results.WorstEvaluation,
                            results.AverageEvaluation,
                            results.TotalTestRuntime,
                            results.BestEvaluationPrev,
                            results.WorstEvaluationPrev,
                            results.AverageEvaluationPrev,
                            results.TotalTestRuntimePrev,
                            results.BestEvaluationDiff,
                            results.WorstEvaluationDiff,
                            results.AverageEvaluationDiff,
                            results.TotalTestRuntimeDiff
                        };


            DataTable dt = query.CopyToDataTableEx();
            dt.TableName = ds.TestResults.TableName;

            return dt;

        }

        /// <summary>
        /// Gets the ThreadTestResults table joined with its parent(s) tables.
        /// </summary>
        /// <param name="ds"></param>
        /// <returns>DataTable</returns>
        public static DataTable GetNonNormalizedThreadTestResults(this TestResultsDataset ds)
        {
            var query =
                (from results in ds.ThreadTestResults.AsEnumerable()
                 //join tests in ds.Tests.AsEnumerable() on results.TestNumber equals tests.TestNumber
                 //join dates in ds.Dates.AsEnumerable() on results.DateKey equals dates.DateKey
                 select new
                 {
                     //tests.TestName,
                     //dates.Date,
                     results.TestsRow.TestName,
                     results.DatesRow.Date,
                     results.ThreadNumber,
                     results.ClientId,
                     results.BestEvaluation,
                     results.WorstEvaluation,
                     results.AverageEvaluation,
                     results.TestRuntime,
                     AvgUowCreationTime = results.Field<long?>("AvgUowCreationTime"),
                     AvgViewCreationTime = results.Field<long?>("AvgViewCreationTime"),
                     AvgLoginTime = results.Field<long?>("AvgLoginTime"),
                     results.Status
                 });

            DataTable clonedDt = new DataTable();
            foreach(DataColumn dc in ds.ThreadTestResults.Columns)
            {
                if (dc.ColumnName.Equals("TestNumber")) continue;
                if (dc.ColumnName.Equals("DateKey")) continue;

                DataColumn c = clonedDt.Columns.Add(dc.ColumnName, dc.DataType);
                c.AllowDBNull = true;
                c.Unique = false;

            }
            DataColumn testName = ds.Tests.GetDataColumn("TestName");
            if (testName != null)
            {
                DataColumn c = clonedDt.Columns.Add(testName.ColumnName, testName.DataType);
                c.SetOrdinal(0);
                c.AllowDBNull = true;
                c.Unique = false;
            }

            DataColumn dateCol = ds.Dates.GetDataColumn("Date");
            if (dateCol != null)
            {
                DataColumn c = clonedDt.Columns.Add(dateCol.ColumnName, dateCol.DataType);
                c.SetOrdinal(2);
                c.AllowDBNull = true;
                c.Unique = false;
            }

            return query.CopyToDataTableEx(table: clonedDt);

        }


         /// <summary>
        /// Gets the TestTimes table joined with its parent(s) tables.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="parent">Parent dataset for joining </param>
        /// <returns>DataTable</returns>
        public static DataTable GetNonNormalizedTestTimes(this TestResultsDataset.TestTimesDataTable ds, TestResultsDataset parent)
         {

             if (parent == null)
             {
                 parent = (TestResultsDataset)ds.DataSet;
             }

             var query =
                (from results in ds.AsEnumerable()
                 join tests in parent.Tests.AsEnumerable() on results.TestNumber equals tests.TestNumber
                 join dates in parent.Dates.AsEnumerable() on results.DateKey equals dates.DateKey
                 select new
                 {
                     tests.TestName,
                     dates.Date,
                     //results.TestsRow.TestName,
                     //results.DatesRow.Date,
                     results.ThreadNumber,
                     results.EvaluationRuntime,
                     UowCreationTime = results.Field<long?>("UowCreationTime"),
                     ViewCreationTime = results.Field<long?>("ViewCreationTime"),
                     LoginTime = results.Field<long?>("LoginTime")
                 });

            DataTable clonedDt = ds.Clone();
            clonedDt.Columns.Remove("TestNumber");
            clonedDt.Columns.Remove("DateKey");
            DataColumn testName = parent.Tests.GetDataColumn("TestName");
            if (testName != null)
            {
                DataColumn c = clonedDt.Columns.Add(testName.ColumnName, testName.DataType);
                c.SetOrdinal(1);
                c.AllowDBNull = true;
                c.Unique = false;
            }

            DataColumn dateCol = parent.Dates.GetDataColumn("Date");
            if (dateCol != null)
            {
                DataColumn c = clonedDt.Columns.Add(dateCol.ColumnName, dateCol.DataType);
                c.SetOrdinal(2);
                c.AllowDBNull = true;
                c.Unique = false;
            }

            return query.CopyToDataTableEx(table: clonedDt);
         }


        /// <summary>
        /// Gets the TestTimes table joined with its parent(s) tables.
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="parent">Parent dataset for joining.</param>
        /// <returns>DataTable</returns>
        public static DataTable GetNonNormalizedTestTimes(this TestResultsDataset ds, TestResultsDataset parent = null)
        {
            return ds.TestTimes.GetNonNormalizedTestTimes(null);
        }
    }
}
