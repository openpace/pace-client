﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using DevExpress.Utils;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraPrinting;
using Npgsql;
using NpgsqlTypes;
using ServerStressTesterBase.Pace.Data;
using ServerStressTesterDataSet.Pace.Data;
using ServerStressTesterStats.Properties;

namespace ServerStressTesterStats
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Console.WriteLine("got here");
        }

        private void button1_Click(object sender, EventArgs e)
        {

            TestResultsDataset tsd = null;
            FileInfo fi = new FileInfo(textBox1.Text);
            Console.WriteLine("Getting xml from database: " + fi);
            var command = new NpgsqlCommand("get_xml")
            {
                CommandType = CommandType.StoredProcedure
            };
            command.Parameters.Add(new NpgsqlParameter("text", NpgsqlDbType.Text));
            command.Parameters[0].Value = fi.Name;

            BaseDataAdapter query = new PostgresDataAdapter(Settings.Default.ConnectionString, command);
            var xml = query.ExecuteScalar();

            if (xml != null)
            {
                Console.WriteLine("XML returned loading into object");
                tsd =TestResultsDatasetExtension.LoadFromXml(xml.ToString());
            }



            gridControl1.DataSource = tsd.Dates;
            foreach (GridColumn dc in gridView1.Columns)
            {
                if (dc.FieldName == "Date")
                {
                    dc.DisplayFormat.FormatType = FormatType.DateTime;
                    dc.DisplayFormat.FormatString = "F";
                }
            }

        }


        private void gridControl1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveDialog = new SaveFileDialog())
            {
                saveDialog.Filter = "Excel (2003)(.xls)|*.xls|Excel (2010) (.xlsx)|*.xlsx |RichText File (.rtf)|*.rtf |Pdf File (.pdf)|*.pdf |Html File (.html)|*.html";
                if (saveDialog.ShowDialog() != DialogResult.Cancel)
                {
                    string exportFilePath = saveDialog.FileName;
                    string fileExtenstion = new FileInfo(exportFilePath).Extension;
                    switch (fileExtenstion)
                    {
                        case ".xls":

                            gridControl1.ExportToXls(exportFilePath);
                            break;
                        case ".xlsx":
                            XlsxExportOptions options = new XlsxExportOptions();
                            options.ExportMode = XlsxExportMode.SingleFile;
                            gridView1.ExportToXlsx(exportFilePath, options);
                            //gridControl1.ExportToXlsx(exportFilePath, options);
                            break;
                        case ".rtf":
                            gridControl1.ExportToRtf(exportFilePath);
                            break;
                        case ".pdf":
                            gridControl1.ExportToPdf(exportFilePath);
                            break;
                        case ".html":
                            gridControl1.ExportToHtml(exportFilePath);
                            break;
                        case ".mht":
                            gridControl1.ExportToMht(exportFilePath);
                            break;
                        default:
                            break;
                    }

                }
            }
        }
    }
}
