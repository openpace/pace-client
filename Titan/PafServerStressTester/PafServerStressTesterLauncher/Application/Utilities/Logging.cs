#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using log4net;
[assembly: log4net.Config.XmlConfigurator(ConfigFileExtension = "log4net.config", Watch = true)]

namespace PafServerStressTesterLauncher.Application.Utilities
{
    public class Logging : ILog
    {
        private readonly ILog log;// =

        #region ILoggerWrapper Members

        /// <summary>
        /// Gets the ILogger.
        /// </summary>
        public log4net.Core.ILogger Logger
        {
            get { return log.Logger; }
        }

        #endregion

        #region ILog Members

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="name">Name of the class.</param>
        public Logging(string name)
        {
            log = LogManager.GetLogger(name);
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        /// <param name="exception">Exception information to display.</param>
        public void Debug(object message, Exception exception)
        {
            if (IsDebugEnabled)
            {
                log.Debug(message, exception);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        public void Debug(object message)
        {
            if (IsDebugEnabled)
            {
                log.Debug(message);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        /// <param name="arg2">An object to format.</param>
        public void DebugFormat(string format, object arg0, object arg1, object arg2)
        {
            if (IsDebugEnabled)
            {
                log.DebugFormat(format, arg0, arg1, arg2);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="provider">Format provider</param>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsDebugEnabled)
            {
                log.DebugFormat(provider, format, args);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void DebugFormat(string format, params object[] args)
        {
            if (IsDebugEnabled)
            {
                log.DebugFormat(format, args);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        public void DebugFormat(string format, object arg0)
        {
            if (IsDebugEnabled)
            {
                log.DebugFormat(format, arg0);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        public void DebugFormat(string format, object arg0, object arg1)
        {
            if (IsDebugEnabled)
            {
                log.DebugFormat(format, arg0, arg1);
            }
        }

        /// <summary>
        /// Prints a error message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        /// <param name="exception">Exception information to display.</param>
        public void Error(object message, Exception exception)
        {
            if (IsErrorEnabled)
            {
                log.Error(message, exception);
            }
        }

        /// <summary>
        /// Prints a error message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        public void Error(object message)
        {
            if (IsErrorEnabled)
            {
                log.Error(message);
            }
        }

        /// <summary>
        /// Prints a error message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        /// <param name="arg2">An object to format.</param>
        public void ErrorFormat(string format, object arg0, object arg1, object arg2)
        {
            if (IsErrorEnabled)
            {
                log.ErrorFormat(format, arg0, arg1, arg2);
            }
        }

        /// <summary>
        /// Prints a error message to the log.
        /// </summary>
        /// <param name="provider">Format provider</param>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsErrorEnabled)
            {
                log.ErrorFormat(provider, format, args);
            }
        }

        /// <summary>
        /// Prints a error message to the log.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void ErrorFormat(string format, params object[] args)
        {
            if (IsErrorEnabled)
            {
                log.ErrorFormat(format, args);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        public void ErrorFormat(string format, object arg0)
        {
            if (IsErrorEnabled)
            {
                log.ErrorFormat(format, arg0);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        public void ErrorFormat(string format, object arg0, object arg1)
        {
            if (IsErrorEnabled)
            {
                log.ErrorFormat(format, arg0, arg1);
            }
        }

        /// <summary>
        /// Prints a fatal message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        /// <param name="exception">Exception information to display.</param>
        public void Fatal(object message, Exception exception)
        {
            if (IsFatalEnabled)
            {
                log.Fatal(message, exception);
            }
        }

        /// <summary>
        /// Prints a fatal message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        public void Fatal(object message)
        {
            if (IsFatalEnabled)
            {
                log.Fatal(message);
            }
        }

        /// <summary>
        /// Prints a fatal message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        /// <param name="arg2">An object to format.</param>
        public void FatalFormat(string format, object arg0, object arg1, object arg2)
        {
            if (IsFatalEnabled)
            {
                log.FatalFormat(format, arg0, arg1, arg2);
            }
        }

        /// <summary>
        /// Prints a fatal message to the log.
        /// </summary>
        /// <param name="provider">Format provider</param>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsFatalEnabled)
            {
                log.FatalFormat(provider, format, args);
            }
        }

        /// <summary>
        /// Prints a fatal message to the log.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void FatalFormat(string format, params object[] args)
        {
            if (IsFatalEnabled)
            {
                log.FatalFormat(format, args);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        public void FatalFormat(string format, object arg0)
        {
            if (IsFatalEnabled)
            {
                log.FatalFormat(format, arg0);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        public void FatalFormat(string format, object arg0, object arg1)
        {
            if (IsFatalEnabled)
            {
                log.FatalFormat(format, arg0, arg1);
            }
        }

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        /// <param name="exception">Exception information to display.</param>
        public void Info(object message, Exception exception)
        {
            if (IsInfoEnabled)
            {
                log.Info(message, exception);
            }
        }

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        public void Info(object message)
        {
            if (IsInfoEnabled)
            {
                log.Info(message);
            }
        }

        public void InfoFormat(string format, object arg0, object arg1, object arg2)
        {
            if (IsInfoEnabled)
            {
                log.InfoFormat(format, arg0, arg1, arg2);
            }
        }

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="provider">Format provider</param>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsInfoEnabled)
            {
                log.InfoFormat(provider, format, args);
            }
        }

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void InfoFormat(string format, params object[] args)
        {
            if (IsInfoEnabled)
            {
                log.InfoFormat(format, args);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        public void InfoFormat(string format, object arg0)
        {
            if (IsInfoEnabled)
            {
                log.InfoFormat(format, arg0);
            }
        }


        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        public void InfoFormat(string format, object arg0, object arg1)
        {
            if (IsInfoEnabled)
            {
                log.InfoFormat(format, arg0, arg1);
            }
        }

        /// <summary>
        /// Gets the debug enabled status.
        /// </summary>
        public bool IsDebugEnabled
        {
            get { return log.IsDebugEnabled; }
        }

        /// <summary>
        /// Gets the error enabled status.
        /// </summary>
        public bool IsErrorEnabled
        {
            get { return log.IsErrorEnabled; }
        }

        /// <summary>
        ///  Gets the error enabled status.
        /// </summary>
        public bool IsFatalEnabled
        {
            get { return log.IsFatalEnabled; }
        }

        /// <summary>
        /// Gets the error enabled status.
        /// </summary>
        public bool IsInfoEnabled
        {
            get { return log.IsInfoEnabled; }
        }

        /// <summary>
        /// Gets the error enabled status.
        /// </summary>
        public bool IsWarnEnabled
        {
            get { return log.IsWarnEnabled; }
        }

        /// <summary>
        /// Prints a warn message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        /// <param name="exception">Exception information to display.</param>
        public void Warn(object message, Exception exception)
        {
            if (IsWarnEnabled)
            {
                log.Warn(message, exception);
            }
        }

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="message">Message to display.</param>
        public void Warn(object message)
        {
            if (IsWarnEnabled)
            {
                log.Warn(message);
            }
        }

        /// <summary>
        /// Prints a warn message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        /// <param name="arg2">An object to format.</param>
        public void WarnFormat(string format, object arg0, object arg1, object arg2)
        {
            if (IsWarnEnabled)
            {
                log.WarnFormat(format, arg0, arg1, arg2);
            }
        }

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="provider">Format provider</param>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsWarnEnabled)
            {
                log.WarnFormat(provider, format, args);
            }
        }

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="format">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        public void WarnFormat(string format, params object[] args)
        {
            if (IsWarnEnabled)
            {
                log.WarnFormat(format, args);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        public void WarnFormat(string format, object arg0)
        {
            if (IsWarnEnabled)
            {
                log.WarnFormat(format, arg0);
            }
        }

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="format">Format</param>
        /// <param name="arg0">An object to format.</param>
        /// <param name="arg1">An object to format.</param>
        public void WarnFormat(string format, object arg0, object arg1)
        {
            if (IsWarnEnabled)
            {
                log.WarnFormat(format, arg0, arg1);
            }
        }

        #endregion
    }
}