﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Xml;
using System.Xml.XPath;
using Npgsql;
using NpgsqlTypes;
using PafServerStressTesterLauncher.Properties;
using ServerStressTesterBase.Pace.Chart.Classes;
using ServerStressTesterBase.Pace.Data;
using ServerStressTesterBase.Pace.Extensions;
using ServerStressTesterDataSet.Pace.Data;
using ServerStressTesterUiArtifacts;
using ServerStressTesterUiArtifacts.Code;
using ServerStressTesterUiArtifacts.Code.Classes;
using Logging = PafServerStressTesterLauncher.Application.Utilities.Logging;
using Titan.Pace.TestHarness.Startup;
using Titan.Pace.TestHarness.Xml;

namespace PafServerStressTesterLauncher
{
    class Program
    {
        private static Logging _Logger;
        private static int _returnCode = 0;
        private const int NumFiles = 4;
        private static DateTime _StartTime;
        static int Main(string[] args)
        {
            TestHarnessStartupInformation thsi = null;
            _Logger = new Logging("PafServerStressTesterLauncher");
            try
            {
                string filePath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
                string path = Path.GetDirectoryName(filePath); 

                List<string> htmlFiles = new List<string>();
                List<string> htmlLabels = new List<string>();

                _StartTime = DateTime.Now;

                //Check for the existence of the test harness startup file.
                string fileName = Path.Combine(path, args[0]);
                if (!File.Exists(fileName))
                {
                    Console.WriteLine("Cannot find file: " + fileName);
                }

                bool useTeamCity = !(args.Length > 1);

                //Check for the existance of the test harness startup file.
                //string outputFileName = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase) + Path.DirectorySeparatorChar + Settings.Default.outputxmlFile;
                string outputFileName = Path.Combine(path, Settings.Default.outputxmlFile);
                if (!File.Exists(outputFileName))
                {
                    Console.WriteLine("Cannot find file: " + outputFileName);
                }

                //if the file exists, then deserialize it.
                if (File.Exists(fileName))
                {
                    thsi = (TestHarnessStartupInformation)XmlDeserializer.Deserialize(Type.GetType("Titan.Pace.TestHarness.Startup.TestHarnessStartupInformation,Titan", true),
                            fileName);
                    if (thsi != null)
                    {
                        foreach (TestHarnessFileInfo t in thsi.TestHarnessFiles)
                        {

                            if (t.UseDefault)
                            {
                                ModifyXmlDocument(outputFileName, Settings.Default.userid_xpath, thsi.DefaultUserId);
                                ModifyXmlDocument(outputFileName, Settings.Default.password_xpath, thsi.DefaultPassword);
                                ModifyXmlDocument(outputFileName, Settings.Default.role_xpath, thsi.DefaultRole);
                                ModifyXmlDocument(outputFileName, Settings.Default.process_xpath, thsi.DefaultProcess);
                                ModifyXmlDocument(outputFileName, Settings.Default.tthFile_xpath, t.TestHarnessFile);
                                ModifyXmlDocument(outputFileName, Settings.Default.url_xpath, thsi.DefaultURL);
                            }
                            else
                            {
                                ModifyXmlDocument(outputFileName, Settings.Default.userid_xpath, t.UserId);
                                ModifyXmlDocument(outputFileName, Settings.Default.password_xpath, t.Password);
                                ModifyXmlDocument(outputFileName, Settings.Default.role_xpath, t.Role);
                                ModifyXmlDocument(outputFileName, Settings.Default.process_xpath, t.Process);
                                ModifyXmlDocument(outputFileName, Settings.Default.tthFile_xpath, t.TestHarnessFile);
                                ModifyXmlDocument(outputFileName, Settings.Default.url_xpath, t.Url);
                            }
                            try
                            {
                                Process proc = new Process();
                                proc.StartInfo.WorkingDirectory = path;
                                proc.StartInfo.FileName = @"PafServerStressTester.exe";
                                if(useTeamCity) proc.StartInfo.Arguments = "-tc";
                                // set up output redirection

                                proc.StartInfo.RedirectStandardOutput = true;
                                proc.StartInfo.RedirectStandardError = true;
                                proc.EnableRaisingEvents = true;
                                proc.StartInfo.UseShellExecute = false;
                                proc.StartInfo.CreateNoWindow = true;

                                // see below for output handler
                                proc.ErrorDataReceived += ProcErrorDataReceived;
                                proc.OutputDataReceived += ProcOutputDataReceived;

                                proc.Start();

                                proc.BeginErrorReadLine();
                                proc.BeginOutputReadLine();

                                proc.WaitForExit();

                                FileInfo fi = new FileInfo(t.TestHarnessFile);
                                if (useTeamCity)
                                {
                                    

                                    string pathFileName = Path.Combine(path, fi.NameWithOutExtension() + ".xml");
                                    Console.WriteLine("Processing dataset xml file: " + pathFileName);
                                    TestResultsDataset ds = TestResultsDatasetExtension.Load(pathFileName);

                                    Console.WriteLine("Getting xml from database: " + fi.Name);
                                    var command = new NpgsqlCommand("get_xml")
                                                      {
                                                          CommandType =
                                                              CommandType.StoredProcedure
                                                      };
                                    command.Parameters.Add(new NpgsqlParameter("text", NpgsqlDbType.Text));
                                    command.Parameters[0].Value = fi.Name;

                                    BaseDataAdapter query = new PostgresDataAdapter(Settings.Default.ConnectionString,
                                                                                    command);
                                    var xml = query.ExecuteScalar();

                                    if (xml != null)
                                    {
                                        Console.WriteLine("XML returned loading into object");
                                        ds.Merge(TestResultsDatasetExtension.LoadFromXml(xml.ToString()));
                                    }


                                    Console.WriteLine("Updating xml in database: " + fi.Name);
                                    command = new NpgsqlCommand("update_xml")
                                                  {
                                                      CommandType = CommandType.StoredProcedure
                                                  };
                                    command.Parameters.Add(new NpgsqlParameter("text", NpgsqlDbType.Text));
                                    command.Parameters.Add(new NpgsqlParameter("xml", NpgsqlDbType.Xml));
                                    command.Parameters[0].Value = fi.Name;
                                    command.Parameters[1].Value = ds.GetXml();

                                    BaseDataAdapter insert = new PostgresDataAdapter(Settings.Default.ConnectionString,
                                                                                     command);
                                    insert.ExecuteNonQuery();
                                }
                                WriteHtmlAndCharts(fi.Name, path);

                                //htmlFiles.Add(fi.Name + ".settings.html");
                                //htmlLabels.Add(fi.Name + " Settings");
                                htmlFiles.Add(fi.Name + ".tests.html");
                                htmlLabels.Add(fi.Name + " Tests");
                                //htmlFiles.Add( fi.Name + ".testresults.html");
                                //htmlLabels.Add(fi.Name + " Test Results");
                                //htmlFiles.Add(fi.Name + ".threadtestresults.html");
                                //htmlLabels.Add(fi.Name + " Thread Test Results");
                                //htmlFiles.Add(fi.Name + ".testtimes.html");
                                //htmlLabels.Add(fi.Name + " Test Times");

                                //if(File.Exists(pathFileName))
                                //{
                                //    File.Delete(pathFileName);
                                //}


                            }
                            catch (Exception ex)
                            {
                                WriteLineToConsole(ex.Message, true, true);
                            }
                        }
                        ToHtml(htmlFiles, htmlLabels, Path.Combine(path, "index.html"));
                    }
                }
                return _returnCode;
            }
            catch (Exception ex)
            {

                WriteLineToConsole(ex.Message, true, true);
                return 1;
            }
        }

        public static void WriteHtmlAndCharts(string testFile, string path)
        {


            try
            {

                var command = new NpgsqlCommand("get_xml");
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add(new NpgsqlParameter("text", NpgsqlDbType.Text));
                command.Parameters[0].Value = testFile;
                NpgsqlParameter returnColumn = new NpgsqlParameter("d_xml", NpgsqlDbType.Xml);
                returnColumn.Direction = ParameterDirection.Output;
                command.Parameters.Add(returnColumn);

                BaseDataAdapter query = new PostgresDataAdapter(Settings.Default.ConnectionString, command);
                query.Fill();

                TestResultsDataset testResultsDataset = new TestResultsDataset();
                if (returnColumn.Value != null)
                {
                    testResultsDataset = TestResultsDatasetExtension.LoadFromXml(returnColumn.Value.ToString());
                }


                List<TestResultsDataset.TestsRow> tests = testResultsDataset.GetTests();
                //List<TestResultsDataset.TestResultsRow> rows = testResultsDataset.GetTestResultsList();
                EnumerableRowCollection<TestResultsDataset.TestResultsRow> testResults = testResultsDataset.GetTestResults();

                foreach (TestResultsDataset.TestsRow test in tests)
                {
                    EnumerableRowCollection<TestResultsDataset.TestResultsRow> results = testResults.Where(y => y.TestNumber == test.TestNumber);
                    EnumerableRowCollection<TestResultsDataset.TestTimesRow> testTimes = testResultsDataset.GetTestTimeResults(test.TestNumber);


                    TestResultsDataset.TestResultsDataTable tempTestResults = new TestResultsDataset.TestResultsDataTable();
                    results.CopyToDataTable(tempTestResults, LoadOption.OverwriteChanges);

                    TestResultsDataset.TestTimesDataTable tempTestTimes = new TestResultsDataset.TestTimesDataTable();
                    testTimes.CopyToDataTable(tempTestTimes, LoadOption.OverwriteChanges);

                    ChartSeriesUi<double, double> chart = new ChartSeriesUi<double, double>(
                        "series1",
                        xColumnName: "Date",
                        yColumnName: "AverageEvaluation");


                    XtraCharts xc = new XtraCharts(tempTestResults.GetNonNormalizedTestResults(testResultsDataset), new ChartTitleInfo("Average Evaluation - " + test.TestName), chart);

                    xc.GeneratePointChart();
                    xc.ExportToImage(Path.Combine(path, test.TestName + "-avgeval.png"));

                    xc.DataSource = tempTestTimes.GetNonNormalizedTestTimes(testResultsDataset);
                    xc.ChartTitleInfo.Text = "All Evaluations - " + test.TestName;
                    xc.ChartSeries.DataTableYColumnName = "EvaluationRuntime";
                    xc.GeneratePointChart();
                    xc.ExportToImage(Path.Combine(path, test.TestName + "-alleval.png"));

                    List<TestResultsDataset.TestResultsRow> resultsList = results.ToList();


                    tempTestResults.ToHtml(
                        test.TestName,
                        Path.Combine(path,  test.TestName + ".html"),
                        resultsList[0].TotalTestRuntime,
                        resultsList[0].WorstEvaluation,
                        resultsList[0].AverageEvaluation,
                        resultsList[0].TotalTestRuntime,
                        resultsList[0].AvgUowCreationTime,
                        resultsList[0].AvgViewCreationTime,
                        results.GetBestEvaluation(),
                        results.GetWorstEvaluation(),
                        results.GetAvgEvaluationMin(),
                        results.GetAvgEvaluationMax(),
                        results.GetTotalRuntimeMin(),
                        results.GetTotalRuntimeMax(),
                        results.GetAvgUowCreationTimeMin(),
                        results.GetAvgUowCreationTimeMax(),
                        results.GetAvgViewCreationTimeMin(),
                        results.GetAvgViewCreationTimeMax(),
                        Uri.EscapeUriString(test.TestName) + "-avgeval.png",
                        Uri.EscapeUriString(test.TestName) + "-alleval.png");



                }

                //testResultsDataset.GetNonNormalizedTestResults().ToHtml(Path.Combine(AppPath, "testresults.html"));
                //testResultsDataset.GetNonNormalizedTestResultsComparison().ToHtml(Path.Combine(AppPath, "testresultscomparison.html"));
                //testResultsDataset.Settings.ToHtml(Path.Combine(AppPath, ".settings.html"));
                //testResultsDataset.GetNonNormalizedThreadTestResults().ToHtml(Path.Combine(AppPath, ".threadtestresults.html"));
                //testResultsDataset.GetNonNormalizedTestTimes().ToHtml(Path.Combine(AppPath, ".testtimes.html"));
                testResultsDataset.Tests.ToHtml(Path.Combine(path, testFile + ".tests.html"));
                //testResultsDataset.Dates.DatesDescending().ToHtml(Path.Combine(AppPath, ".dates.html"));
            }
            catch (ConstraintException ce)
            {
                //DataRow[] rowsInError; 
                //foreach (DataTable table in ds1.Tables)
                //{
                //    // Test if the table has errors. If not, skip it.
                //    if (table.HasErrors)
                //    {
                //        // Get an array of all rows with errors.
                //        rowsInError = table.GetErrors();
                //        // Print the error of each column in each row.
                //        for (int i = 0; i < rowsInError.Length; i++)
                //        {
                //            foreach (DataColumn column in table.Columns)
                //            {
                //                Debug.WriteLine(column.ColumnName + " " +
                //                    rowsInError[i].GetColumnError(column));
                //            }
                //            // Clear the row errors
                //            rowsInError[i].ClearErrors();
                //        }
                //    }
                //}
            }
        }

        /// <summary>
        /// Saves a dataset to an html file.
        /// </summary>
        /// <param name="htmlFiles"></param>
        /// <param name="pathFileName">Full path file name to save the file.</param>
        public static void ToHtml(List<string> htmlFiles, List<string> htmlLabels,  string pathFileName)
        {
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.Append("<head>");
            sb.Append("<title>");
            sb.Append("Page-");
            sb.Append(Guid.NewGuid().ToString());
            sb.Append("</title>");
            sb.Append("</head>");
            sb.Append("<body>");

            int c = 1;
            for(int i = 0; i < htmlFiles.Count; i++)
            {
                if (c == 1)
                {
                    sb.Append("<div>");
                }

                sb.Append("<a href=\"" + htmlFiles[i] + "\"><fontstyle=\"font-size:medium;\">" + htmlLabels[i] + "</font></a><br>");

                c++;

                if (c == NumFiles)
                {
                    sb.Append("</div><br>");
                    c = 1;
                }

            }

            sb.Append("&nbsp;");
            sb.Append("&nbsp;");


            sb.Append("</body>");
            sb.Append("</html>");


            sb.ToFile(pathFileName);

        }

        static void ProcErrorDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.Error.WriteLine(e.Data);
            _returnCode = -1;
        }

        static void ProcOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            Console.WriteLine(e.Data);
        }

        private static void ModifyXmlDocument(string xmlFileName, string xPath, string newValue)
        {

            XmlDocument document = new XmlDocument();
            document.Load(xmlFileName);
            XPathNavigator navigator = document.CreateNavigator();

            XmlNamespaceManager manager = new XmlNamespaceManager(navigator.NameTable);
            manager.AddNamespace(String.Empty, String.Empty);

            foreach (XPathNavigator nav in navigator.Select(@xPath, manager))
            {
                //Get the value of the XPath query result by using the iterator's Current.Value property
                //string imageSize = iterImageSize.Current.Value;
                if (!String.IsNullOrEmpty(nav.Value))
                {
                    nav.SetValue(newValue);
                }
            }

            document.Save(xmlFileName);
        }

        /// <summary>
        /// Write a line to the console and log.
        /// </summary>
        /// <param name="str">String to write to the console window/log.</param>
        /// <param name="log">true to write the string to the log file/false to not.</param>
        /// <param name="isError">true if this is an error.</param>
        public static void WriteLineToConsole(string str, bool log, bool isError)
        {
            Console.WriteLine(str);
            if (log)
            {
                if (isError)
                {
                    _Logger.Error(str);
                }
                else
                {
                    _Logger.Info(str);
                }
            }
        }
    }
}