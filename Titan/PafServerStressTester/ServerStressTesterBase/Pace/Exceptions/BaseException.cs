﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Text;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Exceptions
{
    /// <summary>
    /// All Framework exceptions inherit from BBaseException class
    /// </summary>
    public abstract class BaseException : ApplicationException
    {
        public BaseException() : base()
        {
            // Should be used for FrameworkFatalPopupMessageException only
            // Reason: Snapshot and PopupMessage dump has already occurred.
            // Exception used to expediate terminate.
        }

        public BaseException(string message)
            : base(message)
        {
            Log.Instance.ReportErrorWithSnapshot(message);
        }

        public BaseException(string message, System.Exception innerException)
            : base(message, innerException)
        {
            Log.Instance.ReportError(message);
        }

        internal static string FormatException(System.Exception exception)
        {
            System.Exception excp = exception;
            int nextedInterExceptions = 0;
            string crLf = Environment.NewLine;
            string excpName = excp.GetType().Name;

            StringBuilder sb = new StringBuilder(">>>>> " + excpName + " <<<<<");
            sb.Append(crLf + "\tMessage: " + excp.Message);
            sb.Append(crLf + "\tSource: " + excp.Source);
            sb.Append(crLf + "\tStackTrace: " + crLf + excp.StackTrace.ToString());

            while (null != excp.InnerException)
            {
                ++nextedInterExceptions;
                excp = excp.InnerException;
                excpName = excp.GetType().Name;
                sb.Append(crLf + "\tInnerException");

                for (int i = 1; i < nextedInterExceptions; ++i)
                    sb.Append("-InnerException");

                sb.Append(": " + ">>>>> " + excpName + " <<<<<" + crLf);
                sb.Append(crLf + "\tMessage: " + excp.Message);
                sb.Append(crLf + "\tSource: " + excp.Source);
                sb.Append(crLf + "\tStackTrace: ");

                string innerStackTrace = excp.StackTrace;
                if (!string.IsNullOrEmpty(innerStackTrace))
                    sb.Append(crLf + innerStackTrace) ;
            }
            return sb.ToString();
        }
    }
}