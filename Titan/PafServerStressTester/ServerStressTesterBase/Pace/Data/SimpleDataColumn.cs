﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Runtime.Serialization;
using ServerStressTesterBase.Pace.Base;

namespace ServerStressTesterBase.Pace.Data
{
    [DataContract(Namespace = GlobalConstants.NamespaceData)]
    public class SimpleDataColumn : IDataColumn
    {
        public SimpleDataColumn()
        {
        }

        public SimpleDataColumn(string columnName, string dataTypeName, string columnCaption = null, bool visible = true, string formatString = null)
        {
            ColumnName = columnName;
            DataTypeName = dataTypeName;
            Visible = visible;
            FormatString = formatString;
            ColumnCaption = columnCaption;
        }

        #region Implementation of IDataColumn


        /// <summary>
        /// Format string to use in the report (http://msdn.microsoft.com/en-us/library/427bttx3.aspx)
        /// </summary>
        [DataMember]
        public string FormatString { get; set; }

        /// <summary>
        /// By default is the column visible.
        /// </summary>
        [DataMember]
        public bool Visible { get; set; }

        /// <summary>
        /// Name of the data column.
        /// </summary>
        [DataMember]
        public string ColumnName { get; set; }

        /// <summary>
        /// Caption of the data column.
        /// </summary>
        [DataMember]
        public string ColumnCaption { get; set; }

        /// <summary>
        /// Name of the data type represented.  i.e. (string, double, int, bool).
        /// </summary>
        [DataMember]
        public string DataTypeName { get; set; }

        #endregion
    }
}
