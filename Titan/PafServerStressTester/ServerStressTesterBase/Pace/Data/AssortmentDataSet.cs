﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using ServerStressTesterBase.Pace.Base;

namespace ServerStressTesterBase.Pace.Data
{
    [DataContract(Namespace = GlobalConstants.NamespaceData)]
    public class AssortmentDataSet : IDataSet
    {
        private List<SimpleDataColumn> _columns;
        private List<SimpleDataRow> _rows;

        public AssortmentDataSet()
        {
            _columns = new List<SimpleDataColumn>();
            _rows = new List<SimpleDataRow>();
        }

        public AssortmentDataSet(List<SimpleDataColumn> columns, List<SimpleDataRow> rows)
        {
            _columns = new List<SimpleDataColumn>(columns);
            _rows = new List<SimpleDataRow>(rows);
        }

        public void AddColumn(SimpleDataColumn column)
        {
            _columns.Add(column);
        }

        public void AddRow(SimpleDataRow row)
        {
            _rows.Add(row);
        }

        #region Implementation of IReportData

        /// <summary>
        /// Data columns.
        /// </summary>
        [DataMember]
        public SimpleDataColumn[] DataColumns
        {
            get { return _columns.ToArray(); }
            set { _columns = value.ToList(); }
        }

        /// <summary>
        /// Data Rows.
        /// </summary>
        [DataMember]
        public SimpleDataRow[] DataRows
        {
            get { return _rows.ToArray(); }
            set { _rows = value.ToList(); }
        }

        #endregion
    }
}
