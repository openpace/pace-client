﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Collections.Generic;
using System.Data;
using System.Data.Common;

namespace ServerStressTesterBase.Pace.Data
{
    public interface IDataAdapter
    {
        DataSet Data { get; set; }

        DataTable GetDataTable(string columnName);

        void Fill();
        int ExecuteNonQuery();
        object ExecuteScalar();
        DbDataReader ExecuteReader();
        void Update<T>(string columnName, T valueToFind, T valueToUpdate, bool autoSave = true);
        List<DataRow> Select<T>(string columnName, T value);
        DataRow SelectFirst<T>(string columnName, T value);
        void Delete<T>(string columnName, T value, bool deleteAll = false, bool autoSave = true);
        void Insert(string[] columnNames, object[] values);

        void WriteUpdates(DataRow[] rows);
        void WriteInserts(DataRow[] rows);
        void WriteDeletes(DataRow[] rows);

        void Close();
    }
}
