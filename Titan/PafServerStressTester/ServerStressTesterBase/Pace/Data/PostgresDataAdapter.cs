﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Data;
using System.Data.Common;
using System.Linq;
using Npgsql;
using ServerStressTesterBase.Pace.Extensions;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Data
{
    public class PostgresDataAdapter : BaseDataAdapter
    {
        #region Privates

        private readonly string _connectionString;
        private readonly string _tableName;
        private readonly NpgsqlCommand _queryCommand;
        private NpgsqlDataAdapter _dataAdapter;
        private NpgsqlConnection _sqlConnection;
        private NpgsqlCommandBuilder _sqlCommandBuilder;

        #endregion Privates

        public PostgresDataAdapter(string connectionString, NpgsqlCommand queryCommand, string tableName = null)
        {
            _connectionString = connectionString;
            _queryCommand = queryCommand;
            _tableName = tableName;
        }



        /// <summary>
        /// Fill the DataSet from the underlying object.
        /// </summary>
        public override void Fill()
        {
            Stopwatch.Restart();


            using (_sqlConnection = new NpgsqlConnection(_connectionString))
            {
                Data = new DataSet();
                _queryCommand.Connection = _sqlConnection;
                _dataAdapter = new NpgsqlDataAdapter(_queryCommand);
                _sqlCommandBuilder = new NpgsqlCommandBuilder(_dataAdapter);

                if(string.IsNullOrEmpty(_tableName))
                {
                    _dataAdapter.Fill(Data);
                }
                else
                {
                    _dataAdapter.FillSchema(Data, SchemaType.Source, _tableName);
                    foreach (DataColumn dc in Data.Tables[0].Columns.Cast<DataColumn>().Where(dc => dc.DataType == typeof (Single)))
                    {
                        dc.DataType = typeof (Int32);
                        
                    }
                    bool createRowId = true;
                    foreach (DataColumn dc in Data.Tables[0].Columns.Cast<DataColumn>().Where(dc => dc.ColumnName.Equals(RowId)))
                    {
                        createRowId = false;

                    }
                    DataColumn rowIdCol = null;
                    if(createRowId)
                    {
                        rowIdCol = Data.Tables[0].AddAutoIncrementColumn(columnName: RowId);
                    }
                
                    _dataAdapter.Fill(Data, _tableName);
                    if (rowIdCol != null) Data.Tables[0].MoveDataColumn(rowIdCol.ColumnName, 0);
                }
            }


            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter dataset fill time: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Execute a scalar query.
        /// </summary>
        public override int ExecuteNonQuery()
        {
            Stopwatch.Restart();

            int scalar;

            using (_sqlConnection = new NpgsqlConnection(_connectionString))
            {
                _sqlConnection.Open();
                _queryCommand.Connection = _sqlConnection;
                scalar = _queryCommand.ExecuteNonQuery();
            }

            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter ExecuteNonQuery() runtime: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });

            return scalar;
        }

        /// <summary>
        /// Execute a scalar query.
        /// </summary>
        public override object ExecuteScalar()
        {
            Stopwatch.Restart();

            object scalar;

            using (_sqlConnection = new NpgsqlConnection(_connectionString))
            {
                _sqlConnection.Open();
                _queryCommand.Connection = _sqlConnection;
                scalar = _queryCommand.ExecuteScalar();
            }

            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter ExecuteScalar() runtime: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });

            return scalar;
        }

        /// <summary>
        /// Executes a query and returns a data reader.
        /// </summary>
        /// <returns>NpgsqlDataReader</returns>
        public override DbDataReader ExecuteReader()
        {
            Stopwatch.Restart();

            NpgsqlDataReader reader;

            using (_sqlConnection = new NpgsqlConnection(_connectionString))
            {
                _sqlConnection.Open();
                _queryCommand.Connection = _sqlConnection;
                reader = _queryCommand.ExecuteReader();
            }

            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter ExecuteReader() runtime: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });

            return reader;
        }

        /// <summary>
        /// Write all the updates to the datastore.
        /// </summary>
        /// <param name="rows">DataRows to update.</param>
        public override void WriteUpdates(DataRow[] rows)
        {
            Stopwatch.Restart();


            using (_sqlConnection = new NpgsqlConnection(_connectionString))
            {

                _queryCommand.Connection = _sqlConnection;
                _dataAdapter.SelectCommand = _queryCommand;
                _sqlCommandBuilder.DataAdapter = _dataAdapter;

                _sqlCommandBuilder.GetUpdateCommand();
                _dataAdapter.Update(rows);
            }

            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter WriteUpdates time: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Write all the inserts to the datastore.
        /// </summary>
        /// <param name="rows">DataRows to update.</param>
        public override void WriteInserts(DataRow[] rows)
        {
            Stopwatch.Restart();

            using (_sqlConnection = new NpgsqlConnection(_connectionString))
            {

                _queryCommand.Connection = _sqlConnection;
                _dataAdapter.SelectCommand = _queryCommand;
                _sqlCommandBuilder.DataAdapter = _dataAdapter;

                _sqlCommandBuilder.GetInsertCommand();
                _dataAdapter.Update(rows);
            }

            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter WriteInserts time: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Write all the deletes to the datastore.
        /// </summary>
        /// <param name="rows">DataRows to update.</param>
        public override void WriteDeletes(DataRow[] rows)
        {
            Stopwatch.Restart();
            using (_sqlConnection = new NpgsqlConnection(_connectionString))
            {

                _queryCommand.Connection = _sqlConnection;
                _dataAdapter.SelectCommand = _queryCommand;
                _sqlCommandBuilder.DataAdapter = _dataAdapter;

                _sqlCommandBuilder.GetDeleteCommand();
                _dataAdapter.Update(rows);
            }

            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter writedeletes time: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Closes the connection (if applicable).
        /// </summary>
        public override void Close()
        {
            Stopwatch.Restart();

            if (_sqlConnection.State != ConnectionState.Closed)
            {
                _sqlConnection.Close();
                _sqlConnection.Dispose();
            }

            Stopwatch.Stop();
            Logger.Instance.DebugFormat("PostgresDataAdapter dataset connection close time: {0} (ms)", new[] { Stopwatch.ElapsedMilliseconds.ToString() });
        }
    }
}
