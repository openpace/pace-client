﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Diagnostics;
using System.Linq;
using ServerStressTesterBase.Pace.Extensions;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Data
{
    public abstract class BaseDataAdapter : IDataAdapter, IDisposable
    {
        protected const string RowId = "RowId";

        protected BaseDataAdapter()
        {
            Stopwatch = new Stopwatch();
        }

        #region Implementation of IDataAdapter

        protected Stopwatch Stopwatch { get; set; }

        /// <summary>
        /// Gets the associated DataSet.
        /// </summary>
        public DataSet Data { get; set; }

        /// <summary>
        /// Fill the DataSet from the underlying object.
        /// </summary>
        public abstract void Fill();

        /// <summary>
        /// Execute a scalar query.
        /// </summary>
        public abstract int ExecuteNonQuery();

        /// <summary>
        /// Execute a scalar query.
        /// </summary>
        public abstract object ExecuteScalar();

        /// <summary>
        /// Executes a query and returns a data reader.
        /// </summary>
        /// <returns></returns>
        public abstract DbDataReader ExecuteReader();

        /// <summary>
        /// Write all the updates to the datastore.
        /// </summary>
        /// <param name="rows">DataRows to update.</param>
        public abstract void WriteUpdates(DataRow[] rows);

        /// <summary>
        /// Write all the inserts to the datastore.
        /// </summary>
        /// <param name="rows">DataRows to update.</param>
        public abstract void WriteInserts(DataRow[] rows);

        /// <summary>
        /// Write all the deletes to the datastore.
        /// </summary>
        /// <param name="rows">DataRows to update.</param>
        public abstract void WriteDeletes(DataRow[] rows);

        /// <summary>
        /// Closes the connection (if applicable).
        /// </summary>
        public abstract void Close();

        /// <summary>
        /// Gets the DataTable from the DataSet.
        /// </summary>
        /// <param name="columnName">ColumnName (text) to find in the table of the DataSet.</param>
        /// <returns>DataTable</returns>
        public DataTable GetDataTable(string columnName)
        {
            return Data.GetDataTable(columnName);
        }

        public void Insert(string[] columnNames, object[] values)
        {
            Logger.Instance.DebugFormat("Updating: {0} in column: {1}", new[] { values.ToString(), columnNames.ToString() });
            Stopwatch stopwatch = Stopwatch.StartNew();

            if(columnNames.Length != values.Length)
            {
                throw new ArgumentException("Array sizes don't match.");
            }
            if (columnNames.Length == 0 ||  values.Length == 0)
            {
                throw new ArgumentException("Invalid array sizes..");
            }

            DataTable table = GetDataTable(columnNames[0]);
            DataRow dr = table.NewRow();
            
            for(int i = 0; i < columnNames.Length; i++)
            {
                dr[columnNames[i]] = values[i];
            }

            table.Rows.Add(dr);

            WriteInserts(new [] {dr});

            stopwatch.Stop();
            Logger.Instance.DebugFormat("Insert statement runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

        }

        /// <summary>
        /// Selects all DataRow from a DataSet.
        /// </summary>
        /// <param name="columnName">ColumnName to query.</param>
        /// <param name="value">Value to search.</param>
        /// <returns></returns>
        public List<DataRow> Select<T>(string columnName, T value)
        {
            Logger.Instance.DebugFormat("Selecting: {0} in column: {1}", new[] { value.ToString(), columnName });
            Stopwatch stopwatch = Stopwatch.StartNew();

            DataTable table = GetDataTable(columnName);

            //List<DataRow> drs = (from myRow in myDataTable.AsEnumerable()
            //     where myRow.Field<T>(columnName).Equals(value)
            //     select myRow).ToList();

            List<DataRow> drs = table.SelectWhere(columnName, value);


            stopwatch.Stop();
            Logger.Instance.DebugFormat("Select statement runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return drs;
        }

        /// <summary>
        /// Selects a DataRow from a DataSet.
        /// </summary>
        /// <param name="columnName">ColumnName to query.</param>
        /// <param name="value">Value to search.</param>
        /// <returns></returns>
        public DataRow SelectFirst<T>(string columnName, T value)
        {
            Logger.Instance.DebugFormat("Selecting(single): {0} in column: {1}", new [] { value.ToString(), columnName });

            Stopwatch stopwatch = Stopwatch.StartNew();

            List<DataRow> rows = this.Select(columnName, value);

            DataRow row =  rows.FirstOrDefault();

            stopwatch.Stop();
            Logger.Instance.DebugFormat("SelectFirst statement runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return row;

        }

        /// <summary>
        /// Deletes a row(s) from a DataSet.
        /// </summary>
        /// <param name="columnName">ColumnName to query.</param>
        /// <param name="value">Value to search.</param>
        /// <param name="deleteAll">true to delete all records that match the query, false to just delete the first record.</param>
        /// <param name="autoSave">Auto save the dataset.</param>
        public void Delete<T>(string columnName, T value, bool deleteAll = false, bool autoSave = true)
        {
            Logger.Instance.DebugFormat("Deleting: {0} in column: {1}", new [] { value.ToString(), columnName });
            Stopwatch stopwatch = Stopwatch.StartNew();

            List<DataRow> rows = Select(columnName, value);

            if (deleteAll)
            {
                foreach (DataRow row in rows)
                {
                    row.Delete();
                }
            }
            else
            {
                DataRow row = rows.FirstOrDefault();
                if (row != null) row.Delete();
            }

            if (autoSave)
            {
                WriteDeletes(rows.ToArray());
            }

            stopwatch.Stop();
            Logger.Instance.DebugFormat("Delete statement runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

        }

        /// <summary>
        /// Updates the DataSet.
        /// </summary>
        /// <param name="columnName">ColumnName to query.</param>
        /// <param name="valueToFind">Value to search.</param>
        /// <param name="valueToUpdate">Value to update the found value to.</param>
        /// <param name="autoSave">Auto save the dataset.</param>
        public void Update<T>(string columnName, T valueToFind, T valueToUpdate, bool autoSave = true)
        {
            Logger.Instance.DebugFormat("Updating: {0} in column: {1}, to value: {2}", new [] {valueToFind.ToString() , columnName, valueToUpdate.ToString() } );
            Stopwatch stopwatch = Stopwatch.StartNew();

            DataRow dr = SelectFirst(columnName, valueToFind);

            dr[columnName] = valueToUpdate;
            
            if (dr[columnName].Equals(valueToFind))
            {
                throw new Exception("Value not set in data set.");
            }

            if (autoSave)
            {
                WriteUpdates(new [] {dr});
            }

            stopwatch.Stop();
            Logger.Instance.DebugFormat("Update statement runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }

        #endregion

        #region Implementation of IDisposable

        public void Dispose()
        {
            Logger.Instance.Debug("Closing BaseDataAdapter connection.");
            Close();
        }

        #endregion
    }
}
