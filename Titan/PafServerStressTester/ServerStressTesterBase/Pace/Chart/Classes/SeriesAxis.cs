﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.Serialization;
using ServerStressTesterBase.Pace.Base;
using ServerStressTesterBase.Pace.Chart.Enums;
using ServerStressTesterBase.Pace.Chart.Interfaces;
using ServerStressTesterBase.Pace.Data;

namespace ServerStressTesterBase.Pace.Chart.Classes
{
    /// <summary>
    /// Represents a series on a chart.
    /// </summary>
    
    [DataContract(Namespace = GlobalConstants.NamespaceChart)]
    public class SeriesAxis : IAxis
    {
        public SeriesAxis()
            :this(text: "")
        {
        }

        public SeriesAxis(string text = "", NumberFormat numericFormat = NumberFormat.General, AxisScaleType scale = AxisScaleType.Numerical, int numericPrecision = 0, 
            string dataNumericFormat = "N0")
        {
            Title = new AxisTitle(text);
            NumericFormat = new NumericOptions(numericFormat, numericPrecision);
            DataNumericFormat = dataNumericFormat;
            Scale = scale;
        }

        #region Implementation of IAxis

        /// <summary>
        ///  Format information for the axis.
        /// </summary>
        [DataMember]
        public AxisTitle Title { get; set; }

        /// <summary>
        /// Format for the numbers on the axis scale.
        /// </summary>
        [DataMember]
        public NumericOptions NumericFormat { get; set; }

        /// <summary>
        /// Format for the data in the chart.
        /// </summary>
        [DataMember]
        public string DataNumericFormat { get; set; }

        /// <summary>
        /// Scale for the value on the axis.
        /// </summary>
        [DataMember]
        public AxisScaleType Scale { get; set; }

        #endregion
    }
}
