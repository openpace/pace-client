﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.Serialization;
using ServerStressTesterBase.Pace.Base;
using ServerStressTesterBase.Pace.Chart.Enums;
using ServerStressTesterBase.Pace.Chart.Interfaces;

namespace ServerStressTesterBase.Pace.Chart.Classes
{
    
    [DataContract(Namespace = GlobalConstants.NamespaceChart)]
    public class ChartTitleInfo : ITextAppearance
    {
        public ChartTitleInfo()
            :this(text: "")
        {
        }

        public ChartTitleInfo(string text = "", bool antialiasing = true, string textColor = null, string fontName = "Tahoma", string fontStyle = "Bold", int fontSize = 14, int indent = 10,
            DockLocation dock = DockLocation.Top, bool visible = true, TextStringAlignment alignment = TextStringAlignment.Center)
        {
            Text = text;
            Antialiasing = antialiasing;
            TextColor = textColor;
            FontName = fontName;
            FontStyle = fontStyle;
            FontSize = fontSize;
            Visible = visible;
            Indent = indent;
            Dock = dock;
            Alignment = alignment;
        }

        #region Implementation of ITextAppearance

        /// <summary>
        ///  Text for the axis.
        /// </summary>
        [DataMember]
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets whether antialising is applied to the title's contents.
        /// </summary>
        [DataMember]
        public bool Antialiasing { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string TextColor { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int FontSize { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string FontName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public string FontStyle { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public bool Visible { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public TextStringAlignment Alignment { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public int Indent { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [DataMember]
        public DockLocation Dock { get; set; }

        #endregion
    }
}
