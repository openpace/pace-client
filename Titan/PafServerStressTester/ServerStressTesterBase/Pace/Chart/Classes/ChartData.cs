﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.Serialization;
using ServerStressTesterBase.Pace.Base;
using ServerStressTesterBase.Pace.Chart.Interfaces;

namespace ServerStressTesterBase.Pace.Chart.Classes
{
    [DataContract(Name="ChartData", Namespace = GlobalConstants.NamespaceChart)]
    public class ChartData<X, Y> : IChartData<X, Y>
    {
        public ChartData()
        {

        }

        public ChartData(X argument, Y value, string label = null, object zValue = null, int? rowId = null)
        {
            Argument = argument;
            Value = value;
            Label = label;
            ZValue = zValue;
            if(rowId.HasValue)
            {
                RowId = rowId.GetValueOrDefault();
            }
        }

        #region Implementation of IChartData<X,Y>

        /// <summary>
        /// Label for the point.
        /// </summary>
        [DataMember]
        public string Label { get; set; }

        /// <summary>
        /// Argument (X) data value
        /// </summary>
        [DataMember]
        public X Argument { get; set; }

        /// <summary>
        /// Value (Y) data value
        /// </summary>
        [DataMember]
        public Y Value { get; set; }

        /// <summary>
        /// Value (Z) data value
        /// </summary>
        [DataMember]
        public object ZValue { get; set; }

        /// <summary>
        /// Key that relates back to the datatable, grid, uploaded file.
        /// </summary>
        [DataMember]
        public int RowId { get; set; }

        #endregion
    }
}
