﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.Serialization;
using ServerStressTesterBase.Pace.Base;
using ServerStressTesterBase.Pace.Chart.Interfaces;
using ServerStressTesterBase.Pace.Data;

namespace ServerStressTesterBase.Pace.Chart.Classes
{

    
    [DataContract(Namespace = GlobalConstants.NamespaceChart)]
    public class NumericOptions : INumericOptions
    {
        public NumericOptions()
            :this(NumberFormat.General)
        {
        }

        public NumericOptions(NumberFormat format = NumberFormat.General, int precision = 0)
        {
            Format = format;
            Precision = precision;
        }

        #region Implementation of INumericOptions

        /// <summary>
        /// Gets or sets a value that specifies the formatting applied to numeric values.
        /// </summary>
        [DataMember]
        public NumberFormat Format { get; set; }

        /// <summary>
        /// Gets or sets the maximum number of digits displayed to the right of the decimal point.
        /// </summary>
        [DataMember]
        public int Precision { get; set; }

        #endregion
    }
}
