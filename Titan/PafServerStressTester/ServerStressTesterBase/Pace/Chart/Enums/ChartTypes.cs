﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Runtime.Serialization;
using ServerStressTesterBase.Pace.Base;

namespace ServerStressTesterBase.Pace.Chart.Enums
{
    [DataContract(Namespace = GlobalConstants.NamespaceChart)]
    public enum ChartType
    {
        [EnumMember]
        Default,
        [EnumMember]
        Bar,
        [EnumMember]
        StackedBar,
        [EnumMember]
        FullStackedBar,
        [EnumMember]
        SideBySideStackedBar,
        [EnumMember]
        SideBySideFullStackedBar,
        [EnumMember]
        Pie,
        [EnumMember]
        Doughnut,
        [EnumMember]
        Funnel,
        [EnumMember]
        Point,
        [EnumMember]
        Bubble,
        [EnumMember]
        Line,
        [EnumMember]
        StackedLine,
        [EnumMember]
        FullStackedLine,
        [EnumMember]
        StepLine,
        [EnumMember]
        Spline,
        [EnumMember]
        ScatterLine,
        [EnumMember]
        SwiftPlot,
        [EnumMember]
        Area,
        [EnumMember]
        StepArea,
        [EnumMember]
        SplineArea,
        [EnumMember]
        StackedArea,
        [EnumMember]
        StackedSplineArea,
        [EnumMember]
        FullStackedArea,
        [EnumMember]
        FullStackedSplineArea,
        [EnumMember]
        RangeArea,
        [EnumMember]
        Stock,
        [EnumMember]
        CandleStick,
        [EnumMember]
        SideBySideRangeBar,
        [EnumMember]
        RangeBar,
        [EnumMember]
        SideBySideGantt,
        [EnumMember]
        Gantt,
        [EnumMember]
        PolarPoint,
        [EnumMember]
        PolarLine,
        [EnumMember]
        PolarArea,
        [EnumMember]
        RadarPoint,
        [EnumMember]
        RadarLine,
        [EnumMember]
        RadarArea,
        [EnumMember]
        Bar3D,
        [EnumMember]
        StackedBar3D,
        [EnumMember]
        FullStackedBar3D,
        [EnumMember]
        ManhattanBar,
        [EnumMember]
        SideBySideStackedBar3D,
        [EnumMember]
        SideBySideFullStackedBar3D,
        [EnumMember]
        Pie3D,
        [EnumMember]
        Doughnut3D,
        [EnumMember]
        Funnel3D,
        [EnumMember]
        Line3D,
        [EnumMember]
        StackedLine3D,
        [EnumMember]
        FullStackedLine3D,
        [EnumMember]
        StepLine3D,
        [EnumMember]
        Area3D,
        [EnumMember]
        StackedArea3D,
        [EnumMember]
        FullStackedArea3D,
        [EnumMember]
        StepArea3D,
        [EnumMember]
        Spline3D,
        [EnumMember]
        SplineArea3D,
        [EnumMember]
        StackedSplineArea3D,
        [EnumMember]
        FullStackedSplineArea3D,
        [EnumMember]
        RangeArea3D,
    }
}
