﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Data;
using ServerStressTesterBase.Pace.Chart.Classes;
using ServerStressTesterBase.Pace.Chart.Enums;

namespace ServerStressTesterBase.Pace.Chart.Interfaces
{
    public interface IChartSeries<X, Y>
    {


        DataSet DataSet { get; }

        string FilterExpression { get; set; }

        string DataTableLabelColumnName { get; set; }

        string DataTableXColumnName { get; set; }

        string DataTableYColumnName { get; set; }

        string DataTableZColumnName { get; set; }

        string DataTableRowIdColumnName { get; set; }

        string Name { get; set; }

        bool ShowLegend { get; set; }

        bool ShowPointLabels { get; set; }

        SeriesSortingMode SeriesPointsSorting { get; set; }

        SeriesPointKey SeriesPointsSortingKey { get; set; }

        ChartData<X, Y>[] Data { get; }

        X MaxArgument { get; set; }

        X MinArgument { get; set; }

        Y MaxValue { get; set; }

        Y MinValue { get; set; }

        SeriesAxis ValueY { get; set; }

        SeriesAxis ArgumentX { get; set; }

        long GetTotalDataPoints();

    }
}
