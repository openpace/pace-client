﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
namespace ServerStressTesterBase.Pace.Base
{
    public static class GlobalConstants
    {
        public const string NamespaceData =  @"http://open-pace.com/data/";
        public const string NamespaceComm = @"http://open-pace.com/comm/";
        public const string NamespaceChart = @"http://open-pace.com/chart/";
        public const string NamespaceReport = @"http://open-pace.com/report/";
    }
}
