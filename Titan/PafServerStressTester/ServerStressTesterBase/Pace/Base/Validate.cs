﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using ServerStressTesterBase.Pace.Exceptions;

namespace ServerStressTesterBase.Pace.Base
{
    internal static class Validate
    {
        //private static readonly string ReportCategory = "Validation";
        internal static readonly string NullString = "(null)";
        //private static bool enableReport = true;
        public static readonly string DefaultMessage = (string)null;
        public static void IsTrue(bool condition)
        {
            Validate.IsTrue(condition, Validate.DefaultMessage);
        }

        public static void IsTrue(bool condition, string message)
        {
            Validate.IsTrue(condition, message, true);
        }

        public static bool IsTrue(bool condition, string message, bool exceptionOnFail)
        {
            message = Validate.IsTrueInternal(condition, message);
            if (exceptionOnFail && !condition)
                throw new ValidationException(message);
            else
                return condition;
        }

        internal static string IsTrueInternal(bool condition, string message)
        {
            if (message == Validate.DefaultMessage)
                message = "Condition is @ValidateNOT@true.";
            message = message.Replace("@ValidateNOT@", condition ? string.Empty : "not ");
            message = message.Replace("@ValidateTRUENOT@", !condition ? string.Empty : "not ");
            message = message.Replace("@ValidateRESULT@", condition ? "succeeded" : "failed");
            //if (Validate.enableReport)
            //    Report.Log(condition ? ReportLevel.Success : ReportLevel.Failure, Validate.ReportCategory, message);
            return message;
        }



        public static void IsFalse(bool condition)
        {
            Validate.IsFalse(condition, Validate.DefaultMessage);
        }

        public static void IsFalse(bool condition, string message)
        {
            Validate.IsFalse(condition, message, true);
        }

        public static bool IsFalse(bool condition, string message, bool exceptionOnFail)
        {
            if (message == Validate.DefaultMessage)
                message = "Condition is @ValidateNOT@false.";
            return Validate.IsTrue(!condition, message, exceptionOnFail);
        }


    }
}