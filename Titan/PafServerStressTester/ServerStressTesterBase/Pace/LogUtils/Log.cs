﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using log4net;
using ServerStressTesterBase.Pace.Base;
using ServerStressTesterBase.Pace.Exceptions;

namespace ServerStressTesterBase.Pace.LogUtils
{
    public sealed class Log: ILogger
    {
        public static ILogger Instance { get; private set; }
        private ILogger me { get; set; }
        private static ILog m_Log { get; set; }
        internal bool TestScriptExecuting { get; set; }

        public Log(string declaringType)
        {
            m_Log = LogManager.GetLogger(declaringType);
            Instance = new Log();
        }

        static Log()  
        {
            m_Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            Instance = new Log();
        }

        private Log()
        {
            me = this;
            string configFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Log4Net.Config");
            FileInfo configFileInfo = new FileInfo(configFile);
            log4net.Config.XmlConfigurator.Configure(configFileInfo);
        }

        /// <summary>
        /// Returns boolean indicating if the debug level is enabled.
        /// </summary>
        public bool IsDebugEnabled
        {
            get { return m_Log.IsDebugEnabled; }
        }


        /// <summary>
        /// Generates a debug log message to the log4net log file.
        /// </summary>
        /// <param name="message">The message to log</param>
        public void Debug(string message)
        {
            if (m_Log.IsDebugEnabled)
            {
                if(!String.IsNullOrEmpty(message))
                    m_Log.Debug(message);
            }
        }

        void ILogger.Debug(string message, List<string> messages)
        {
            if (m_Log.IsDebugEnabled)
            {
                StringBuilder sb = new StringBuilder(message);
                if (null != message)
                    messages.ForEach(x => sb.Append(Environment.NewLine + x));
                m_Log.Debug(sb.ToString());
            }
        }

        void ILogger.DebugFormat(string format, params string[] args)
        {
            if (m_Log.IsDebugEnabled)
                m_Log.DebugFormat(format, args);
        }

        /// <summary>
        /// Returns boolean indicating if the warn level is enabled.
        /// </summary>
        public bool IsWarnEnabled
        {
            get { return m_Log.IsWarnEnabled; }
        }


        void ILogger.Warn(string message)
        {
            if (m_Log.IsWarnEnabled)
                m_Log.Warn(message);
        }

        void ILogger.WarnFormat(string format, params object[] args)
        {
            if (m_Log.IsWarnEnabled)
                m_Log.WarnFormat(format, args);
        }

        /// <summary>
        /// Returns boolean indicating if the info level is enabled.
        /// </summary>
        public bool IsInfoEnabled
        {
            get { return m_Log.IsInfoEnabled; }
        }


        void ILogger.Info(string message)
        {
            if (m_Log.IsInfoEnabled)
                m_Log.Info(message);
        }

        void ILogger.InfoFormat(string format, params object[] args)
        {
            if (m_Log.IsInfoEnabled)
                m_Log.InfoFormat(format, args);
        }

        /// <summary>
        /// Returns boolean indicating if the error level is enabled.
        /// </summary>
        public bool IsErrorEnabled
        {
            get { return m_Log.IsErrorEnabled; }
        }


        void ILogger.Error(string message)
        {
            if (m_Log.IsErrorEnabled)
                m_Log.Error(message);
        }

        void ILogger.ErrorFormat(string format, params object[] args)
        {
            if (m_Log.IsErrorEnabled)
                m_Log.ErrorFormat(format, args);
        }

        void ILogger.Exception(Exception exception)
        {
            string exceptionMsg = BaseException.FormatException(exception);

            if (m_Log.IsErrorEnabled)
                m_Log.Error(exceptionMsg);
        }

        void ILogger.ReportDebug(string message)
        {
            me.Debug(message);
        }

        void ILogger.ReportInfo(string message)
        {
            me.Info(message);
        }

        void ILogger.ReportWarn(string message)
        {
            me.Warn(message);
        }

        void ILogger.ReportWarnWithSnapshot(string message)
        {
            me.Warn(message);
        }

        void ILogger.ReportError(string message)
        {
            me.Error(message);
        }

        void ILogger.ReportErrorWithSnapshot(string message)
        {
            me.Error(message);
        }

        void ILogger.ReportStep(string message)
        {
            if (TestScriptExecuting)
                me.ReportDebug(message);
            else
                me.ReportInfo(message);
        }

        void ILogger.ValidateIsTrue(bool condition, string message, string failureMessage)
        {
            if (!condition)
            {
                string failureMsg = (null == failureMessage) ? message : failureMessage;
                me.Debug("ValidateIsTrue: FAILED - " + failureMsg);
                Validate.IsTrue(condition, failureMsg, true);
            }

            Validate.IsTrue(condition, message);
            me.Debug("ValidateIsTrue: SUCCESSFUL - " + message);
        }

        void ILogger.ValidateIsFalse(bool condition, string message, string failureMessage)
        {
            if (condition)
            {
                string failureMsg = (null == failureMessage) ? message : failureMessage;
                me.Debug("ValidateIsFalse: FAILED - " + failureMsg);
                Validate.IsFalse(condition, failureMsg, true);
            }

            Validate.IsFalse(condition, message);
            me.Debug("ValidateIsFalse: SUCCESSFUL - " + message);
        }
    }
}