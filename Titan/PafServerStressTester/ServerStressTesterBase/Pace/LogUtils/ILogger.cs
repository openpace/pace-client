﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;

namespace ServerStressTesterBase.Pace.LogUtils
{
    /// <summary>
    /// This interface is for writing to a log4net log file.
    /// The AssemblyInfo file should contain the following entry:
    /// [assembly: log4net.Config.XmlConfigurator(ConfigFile = "Log4Net.config", Watch = true)]
    /// </summary>
    public interface ILogger
    {

        /// <summary>
        /// Returns boolean indicating if the debug level is enabled.
        /// </summary>
        bool IsDebugEnabled { get; }

        /// <summary>
        /// Generates a debug log message to the log4net log file.
        /// </summary>
        /// <param name="message">The message to log</param>
        void Debug(string message);

        /// <summary>
        /// Generates a debug log message to the log4net log file followed by a list of strings.
        /// </summary>
        /// <param name="message">Message to be logged before list is logged</param>
        /// <param name="messages">List to be logged</param>
        void Debug(string message, List<string> messages);

        /// <summary>
        /// Prints a debug message to the log.
        /// </summary>
        /// <param name="message">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        void DebugFormat(string message, params string[] args);


        /// <summary>
        /// Returns boolean indicating if the warn level is enabled.
        /// </summary>
        bool IsWarnEnabled { get; }

        /// <summary>
        /// Generates a warning log message to the log4net log file.
        /// </summary>
        /// <param name="message">The message to log</param>
        void Warn(string message);

        /// <summary>
        /// Prints a warning message to the log.
        /// </summary>
        /// <param name="message">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        void WarnFormat(string message, params object[] args);

        /// <summary>
        /// Returns boolean indicating if the info level is enabled.
        /// </summary>
        bool IsInfoEnabled { get; }

        /// <summary>
        /// Generates a info log message to the log4net log file.
        /// </summary>
        /// <param name="message">The message to log</param>
        void Info(string message);

        /// <summary>
        /// Prints a info message to the log.
        /// </summary>
        /// <param name="message">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        void InfoFormat(string message, params object[] args);

        /// <summary>
        /// Returns boolean indicating if the error level is enabled.
        /// </summary>
        bool IsErrorEnabled { get; }

        /// <summary>
        /// Generates a error log message to the log4net log file.
        /// </summary>
        /// <param name="message">The message to log</param>
        void Error(string message);

        /// <summary>
        /// Prints a error message to the log.
        /// </summary>
        /// <param name="message">Format string.</param>
        /// <param name="args">Format string arguments.</param>
        void ErrorFormat(string message, params object[] args);

        /// <summary>
        /// Generates a exception log message to the log4net log file.
        /// </summary>
        /// <param name="exception">Exception to log</param>
        void Exception(Exception exception);

        /// <summary>
        /// Method to generate a debug message to both the Ranorex Report and the Log4Net log file.
        /// </summary>
        /// <param name="message">The message to log</param>
        void ReportDebug(string message);

        /// <summary>
        /// Method to generate a info message to both the Ranorex Report and the Log4Net log file.
        /// </summary>
        /// <param name="message"></param>
        void ReportInfo(string message);

        /// <summary>
        /// Method to generate a warning message to both the Ranorex Report and the Log4Net log file.
        /// </summary>
        /// <param name="message">The message to log</param>
        void ReportWarn(string message);

        /// <summary>
        /// Method to generate a warning message to both the Ranorex Report and the Log4Net log file.
        /// A snapshot will also be generated to the Ranorex Report.
        /// </summary>
        /// <param name="message">The message to log</param>
        void ReportWarnWithSnapshot(string message);

        /// <summary>
        /// Method to generate a eror message to both the Ranorex Report and the Log4Net log file.
        /// </summary>
        /// <param name="message"></param>
        void ReportError(string message);

        /// <summary>
        /// Method to generate an error message to both the Ranorex Report and the Log4Net log file.
        /// A snapshot will also be generated to the Ranorex Report.
        /// </summary>
        /// <param name="message">The message to log</param>
        void ReportErrorWithSnapshot(string message);

        /// <summary>
        /// Method to generate either a debug message if the setup script is executing or an info message if the test script is executing.
        /// </summary>
        /// <param name="message">The message to log</param>
        void ReportStep(string message);

        /// <summary>
        /// Method to call Validation.IsTrue but supply an optional failure messages.
        /// </summary>
        /// <param name="condition">Condition to test</param>
        /// <param name="message">Message passed to Validation.IsTrue if condition is true or if failureMessage is not supplied</param>
        /// <param name="failureMessage">Failure message passed to Validation.IsTrue</param>
        void ValidateIsTrue(bool condition, string message, string failureMessage = null);

        /// <summary>
        /// Method to call Validation.IsFalse but supply an optional failure messages.
        /// </summary>
        /// <param name="condition">Condition to test</param>
        /// <param name="message">Message passed to Validation.IsFalse if condition is false or if failureMessage is not supplied</param>
        /// <param name="failureMessage">Failure message passed to Validation.IsFalse</param>
        void ValidateIsFalse(bool condition, string message, string failureMessage = null);
    }
}