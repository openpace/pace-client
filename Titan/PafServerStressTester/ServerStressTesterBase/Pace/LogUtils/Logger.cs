﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Diagnostics;
using System.Reflection;

namespace ServerStressTesterBase.Pace.LogUtils
{
    public static class Logger
    {
        public static ILogger Instance
        {
            get
            {
                //Get the caller and pass it to the logger, so it shows up in the log.
                StackTrace st = new StackTrace();
                StackFrame fr = st.GetFrame(1);
                MethodBase m = fr.GetMethod();


                ILogger logger = new Log(m.DeclaringType.FullName + "." + m.Name) ??
                                new Log(MethodBase.GetCurrentMethod().DeclaringType.ToString());

                return logger;
            }
        }
    }
}
