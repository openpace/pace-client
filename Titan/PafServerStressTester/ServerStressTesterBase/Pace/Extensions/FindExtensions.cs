﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class FindExtensions
    {
        /// <summary>
        /// Searches for an element that matches the conditions defined by the specified predicate, and returns the first occurrence within the entire <see cref="T:System.Collections.Generic.List`1"/>.
        /// </summary>
        /// <returns>
        /// The first element that matches the conditions defined by the specified predicate, if found; otherwise, the default value for type <paramref name="T"/>.
        /// </returns>
        /// <param name="x">List of client states</param>
        /// <param name="match">The <see cref="T:System.Predicate`1"/> delegate that defines the conditions of the element to search for.
        ///                 </param><exception cref="T:System.ArgumentNullException"><paramref name="match"/> is null.
        ///                 </exception>
        public static T Find<T>(this List<T> x, Predicate<T> match)
        {
            return x.Find(match);
        }

        /// <summary>
        /// Searches for an element that matches the conditions defined by the specified predicate, and returns the first occurrence within the entire <see cref="T:System.Collections.Generic.List`1"/>.
        /// </summary>
        /// <returns>
        /// The first element that matches the conditions defined by the specified predicate, if found; otherwise, the default value for type <paramref name="T"/>.
        /// </returns>
        /// <param name="x">List of client states</param>
        /// <param name="match">The <see cref="T:System.Predicate`1"/> delegate that defines the conditions of the element to search for.
        ///                 </param><exception cref="T:System.ArgumentNullException"><paramref name="match"/> is null.
        ///                 </exception>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static bool Delete<T>(this List<T> x, Predicate<T> match)
        {
            T cs = x.Find(match);

            if (cs == null)
            {
                return false;
            }
            
            x.Remove(cs);
            return true;

            
        }

        /// <summary>
        /// Retrieves all the elements that match the conditions defined by the specified predicate.
        /// </summary>
        /// <returns>
        /// A <see cref="T:System.Collections.Generic.List`1"/> containing all the elements that match the conditions defined by the specified predicate, if found; otherwise, an empty <see cref="T:System.Collections.Generic.List`1"/>.
        /// </returns>
        /// <param name="x">List of client states</param>
        /// <param name="match">The <see cref="T:System.Predicate`1"/> delegate that defines the conditions of the elements to search for.
        ///                 </param><exception cref="T:System.ArgumentNullException"><paramref name="match"/> is null.
        ///                 </exception>
        public static List<T> FindAll<T>(this List<T> x, Predicate<T> match)
        {
            return x.FindAll(match);
        }
    }
}
