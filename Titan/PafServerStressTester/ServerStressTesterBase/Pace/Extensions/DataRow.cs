﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class DataRowExtension
    {

        public static IList<T> ConvertTo<T>(this IList<DataRow> rows, Dictionary<string, string> columnNameMapping = null)
        {
            IList<T> list = null;

            if (rows != null)
            {
                list = rows.Select(row => CreateItem<T>(row, columnNameMapping)).ToList();
            }

            return list;
        }

        private static T CreateItem<T>(DataRow row, Dictionary<string, string> columnNameMapping = null)
        {
            T obj = default(T);
            if (row != null)
            {
                obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in row.Table.Columns)
                {

                    string columnName = column.ColumnName.Trim();
                    if (columnNameMapping != null)
                    {
                        columnNameMapping.TryGetValue(column.ColumnName.Trim(), out columnName);
                    }

                    PropertyInfo prop = obj.GetType().GetProperty(columnName);
                    try
                    {
                        object value = row[column.ColumnName];
                        if (value == DBNull.Value)
                        {
                            value = null;
                        }
                        prop.SetValue(obj, value, null);
                        
                    }
                    catch(Exception ex)
                    {
                        Logger.Instance.Error(ex.Message);
                        // You can log something here
                        throw;
                    }
                }
            }

            return obj;
        }
    }
}
