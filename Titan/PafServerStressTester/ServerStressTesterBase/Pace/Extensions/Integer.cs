﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class IntegerExtension
    {

        /// <summary>
        /// Generates a random number (integer) (string representation) with the given length
        /// </summary>
        /// <param name="x">String to extend.</param>
        /// <param name="max">Size of the string</param>
        /// <param name="min">Size of the string</param>
        /// <returns>Random string</returns>
        public static int RandomNumber(this int x, int max = Int32.MaxValue , int min= 0)
        {
            Random random = new Random();

            return random.Next(min, max);

        }

        /// <summary>
        /// Generates a unique string using a GUID.
        /// </summary>
        /// <returns></returns>
        public static ulong GenerateUniqueULong()
        {
            byte[] gb = Guid.NewGuid().ToByteArray();

            return BitConverter.ToUInt64(gb, 0);

        }
    }
}
