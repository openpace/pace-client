﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Text;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class DoubleExtension
    {
        /// <summary>
        /// Converts double array to Base64 string
        /// </summary>
        /// <param name="doubleArray">Double array</param>
        /// <param name="columnBlanksCount">Number of blank columns in the double array.</param>
        /// <param name="colCount">Number of columns in the array.</param>
        public static string ToBase64(this double[,] doubleArray, int columnBlanksCount, out int colCount)
        {
            StringBuilder getData = new StringBuilder();
            //int rowLength = 0;

            double[,] arrayS = doubleArray;

            int colLength = doubleArray.GetUpperBound(1) + 1;
            //rowLength = tsr.GetUpperBound(0) + 1 ;
            //colCount = colLength - _ColBlanks.Count;
            colCount = colLength - columnBlanksCount;

            for (int i = 0; i < arrayS.Length / colLength; i++)
            {
                ////Don't skip blank rows and columns, they have already been removed.
                for (int j = 0; j < colLength; j++)
                {
                    try
                    {
                        byte[] byteArray = BitConverter.GetBytes(arrayS[i, j]);
                        getData.Append(Convert.ToBase64String(byteArray));
                    }
                    catch
                    {
                        byte[] byteArray = BitConverter.GetBytes(0);
                        getData.Append(Convert.ToBase64String(byteArray));
                    }
                }
            }
            return getData.ToString();
        }

    }
}
