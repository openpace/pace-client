﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.Collections;
using System.Data;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class DataColumnExtension
    {
        /// <summary>
        /// Copy the extended properties from one DataColumn to another.
        /// </summary>
        /// <param name="source">Source Data Column</param>
        /// <param name="destination">Destination Data Column</param>
        public static void CopyExtendedProperties(this DataColumn source, DataColumn destination)
        {
            foreach (DictionaryEntry de in source.ExtendedProperties)
            {
                destination.ExtendedProperties.Add(de.Key, de.Value);
            }
        }

        /// <summary>
        /// Gets the display format (numeric format) for the data column.
        /// </summary>
        /// <param name="x">Data Column</param>
        /// <param name="key">(Optional) Key for the extended propery.</param>
        public static string GetDisplayFormat(this DataColumn x, string key = "DisplayFormat")
        {
            return (string) x.ExtendedProperties[key];
        }
    }
}
