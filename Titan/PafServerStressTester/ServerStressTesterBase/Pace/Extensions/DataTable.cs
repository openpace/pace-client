﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using ServerStressTesterBase.Pace.Data;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class DataTableExtension
    {

        /// <summary>
        /// Gets the ordinal (positon) of a data column in a data table.
        /// </summary>
        /// <param name="dt">Data Table to search.</param>
        /// <param name="columnName">Name of the column to find.</param>
        /// <returns>The ordianl of the column, or -1 if the column is not found.</returns>
        public static int GetColumnOrdinal(this DataTable dt, string columnName)
        {
            if (String.IsNullOrEmpty(columnName)) return -1;

            foreach (DataColumn dc in dt.Columns.Cast<DataColumn>().Where(dc => dc.ColumnName.Equals(columnName)))
            {
                return dc.Ordinal;
            }
            return -1;
        }

        /// <summary>
        /// Moves a datacolumhn to a new position.
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="columnName"></param>
        /// <param name="newPosition"></param>
        public static void MoveDataColumn(this DataTable dt, string columnName, int newPosition)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            DataColumn column = dt.GetDataColumn(columnName);
            if (column != null)
            {
                column.SetOrdinal(newPosition);
            }

            stopwatch.Stop();
            Logger.Instance.DebugFormat("MoveDataColumn Runtime {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Change the captions/column names in a data table.
        /// </summary>
        /// <param name="dt">Data table to modify.</param>
        /// <param name="newCaptions">List of new captions.</param>
        /// <param name="changeColumnNames">True to also change the column names, false to ignore.</param>
        public static void ChangeCaptions(this DataTable dt, List<string> newCaptions, bool changeColumnNames = false )
        {
            if(newCaptions.Count != dt.Columns.Count)
            {
                throw new ArgumentException("Invlid number of new captions.");
            }

            int i = 0;
            foreach(DataColumn dc in dt.Columns )
            {
                dc.Caption = newCaptions[i];
                if(changeColumnNames) dc.ColumnName = dc.Caption;
                i++;
            }
        }

        /// <summary>
        /// Will delete rows where all columns are null or optionally zero.
        /// </summary>
        /// <param name="dt">DataTable to modify.</param>
        /// <param name="purgeZeros">Remove zeros as well as nulls.</param>
        /// <param name="columnsToProcess">Process specific columns, null to process all.</param>
        /// <returns>True if something is fixed.</returns>
        public static bool RemoveRowsWithNull(this DataTable dt, bool purgeZeros = false, List<string> columnsToProcess = null)
        {
            bool ret = false;
            Stopwatch stopwatch = Stopwatch.StartNew();
            for (int a = 0; a < dt.Rows.Count; a++)
            {
                int c = 0;
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (columnsToProcess != null && !columnsToProcess.Contains(dt.Columns[i].ColumnName)) continue;
                    Type type = dt.Columns[i].DataType;
                    if (type == typeof(String))
                    {
                        if (dt.Rows[a][i] != DBNull.Value)
                        {
                            string value = (string)dt.Rows[a][i];
                            if (String.IsNullOrEmpty(value.Trim()))
                            {
                                c++;
                            }
                        }
                        else
                        {
                            c++;
                        }
                    }
                    else
                    {
                        if (dt.Rows[a][i] == DBNull.Value || dt.Rows[a][i] == null)
                        {
                            c++;
                        }
                        else if (purgeZeros && dt.Rows[a][i].EqualsObj(0))
                        {
                            c++;
                        }
                    }
                }
                if (columnsToProcess == null && c == dt.Columns.Count)
                {
                    ret = true;
                    dt.Rows[a].Delete();
                }
                if (columnsToProcess != null && c == columnsToProcess.Count)
                {
                    ret = true;
                    dt.Rows[a].Delete();
                }
            }
            dt.AcceptChanges();

            stopwatch.Stop();
            Logger.Instance.DebugFormat("RemoveRowsWithNull Runtime {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return ret;
        }

        /// <summary>
        /// Will delete rows that contain ANY NULL values.
        /// </summary>
        public static void RemoveRowsWithAnyNulls(this DataTable dt, List<string> columnsToProcess = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            for (int a = 0; a < dt.Rows.Count; a++)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (columnsToProcess != null && !columnsToProcess.Contains(dt.Columns[i].ColumnName)) continue;
                    Type type = dt.Columns[i].DataType;
                    if (type == typeof(String))
                    {
                        if(dt.Rows[a][i] != DBNull.Value)
                        {
                            string value = (string) dt.Rows[a][i];
                            if (String.IsNullOrEmpty(value.Trim()))
                            {
                                dt.Rows[a].Delete();
                                break;
                            }
                        }
                        else
                        {
                            dt.Rows[a].Delete();
                            break;
                        }
                    }
                    else if (dt.Rows[a][i] == DBNull.Value || dt.Rows[a][i] == null)
                    {
                        dt.Rows[a].Delete();
                        break;
                    }
                }
            }
            dt.AcceptChanges();

            stopwatch.Stop();
            Logger.Instance.DebugFormat("RemoveRowsWithAnyNulls Runtime {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Will replace null values with zero/String.Empty.
        /// </summary>
        /// <returns>True if something is fixed.</returns>
        public static bool ReplaceNullsWithZero(this DataTable dt, List<string> columnsToIgnore = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            bool ret = false;
            for (int a = 0; a < dt.Rows.Count; a++)
            {
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    if (columnsToIgnore != null && columnsToIgnore.Contains(dt.Columns[i].ColumnName)) continue;
                    Type type = dt.Columns[i].DataType;

                    if (type == typeof(String))
                    {
                        if (dt.Rows[a][i] == DBNull.Value)
                        {
                            dt.Rows[a][i] = String.Empty;
                            ret = true;
                        }
                    }
                    else if (dt.Rows[a][i] == DBNull.Value || dt.Rows[a][i] == null)
                    {
                        dt.Rows[a][i] = 0.0;
                        ret = true;
                    }
                }
            }
            dt.AcceptChanges();

            stopwatch.Stop();
            Logger.Instance.DebugFormat("ReplaceNullsWithZero Runtime {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return ret;
        }

        /// <summary>
        /// Adds a auto increment column to a data table.
        /// </summary>
        /// <param name="x">Data Table.</param>
        /// <param name="columnName">Name of the data column.</param>
        /// <param name="columnCaption">Caption of the data column.</param>
        /// <param name="autoIncrementSeed">Initial seed.</param>
        /// <param name="autoIncrementStep">Step values.</param>
        public static DataColumn AddAutoIncrementColumn(this DataTable x, string columnName = "[RowId]", string columnCaption = "Row Id", int autoIncrementSeed = 0, int autoIncrementStep = 1)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            DataColumn column = new DataColumn
            {
                ColumnName = columnName,
                Caption = columnCaption,
                DataType = Type.GetType("System.Int32"),
                AutoIncrement = true,
                AutoIncrementSeed = autoIncrementSeed,
                AutoIncrementStep = autoIncrementStep
            };


            x.Columns.Add(column);

            stopwatch.Stop();
            Logger.Instance.DebugFormat("AddAutoIncrementColumn:, Runtime {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return column;
        }

        /// <summary>
        /// Adds a calcualted column to the data table.
        /// </summary>
        /// <param name="x">DataTable</param>
        /// <param name="columnName"></param>
        /// <param name="columnType"></param>
        /// <param name="expression"></param>
        /// <param name="columnCaption"></param>
        /// <param name="displayFormat"></param>
        public static void AddCalculatedExpression(this DataTable x, string columnName, Type columnType, string expression, string columnCaption = null, string displayFormat = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            x.AddColumn(columnName, columnType, expression, columnCaption, displayFormat);

            x.AcceptChanges();

            stopwatch.Stop();
            Logger.Instance.DebugFormat("Add Calculated Expression Column: {0}, Runtime {1} (ms)", new[] { columnName, stopwatch.ElapsedMilliseconds.ToString() });
        }


        /// <summary>
        /// Copies all the extended propties of the data columns to another data table.
        /// </summary>
        /// <param name="source">Source data table.</param>
        /// <param name="destination">Destination data table.</param>
        public static void CopyColumnExtendedProperties(this DataTable source, DataTable destination)
        {
            foreach (DataColumn column in source.Columns.Cast<DataColumn>().Where(column => destination.Columns.Contains(column.ColumnName)))
            {
                column.CopyExtendedProperties(destination.Columns[column.ColumnName]);
                if (!destination.Columns[column.ColumnName].Caption.Equals(column.Caption))
                {
                    destination.Columns[column.ColumnName].Caption = column.Caption;
                }
            }
        }

        /// <summary>
        /// Loads the report data from the data set.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="dataColumnFilter">Data Columns to return in ReportData (null to return all)</param>
        /// <param name="dataColumnExclude">Data Columns to exclude from the ReportData (null to ignore)</param>
        /// <param name="columnsVisible">List of the column to be marked as visible (if null all columns will be marked visible)</param>
        public static AssortmentDataSet ToBaseData(this DataTable x, List<string> dataColumnFilter = null, List<string> dataColumnExclude = null, List<string> columnsVisible = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            AssortmentDataSet data = new AssortmentDataSet();

            List<DataColumn> columns = x.Columns.Cast<DataColumn>().ToList();

            foreach (DataColumn column in columns)
            {
                if (dataColumnFilter == null || dataColumnFilter.Contains(column.ColumnName))
                {
                    if (dataColumnExclude == null || !dataColumnExclude.Contains(column.ColumnName))
                    {
                        string format = (string)column.ExtendedProperties["DisplayFormat"];
                        SimpleDataColumn rdc = new SimpleDataColumn(column.ColumnName, column.DataType.ToString(),  formatString: format);
                        if(!String.IsNullOrEmpty(column.Caption )) rdc.ColumnCaption = column.Caption;
                        rdc.Visible = false;
                        if(columnsVisible == null)
                        {
                            rdc.Visible = true;
                        }
                        else if (columnsVisible.Contains(column.ColumnName))
                        {
                            rdc.Visible = true;
                        }
                        data.AddColumn(rdc);
                    }
                }
            }

            List<DataRow> rows = x.Rows.Cast<DataRow>().ToList();
            foreach (List<object> temp in rows.Select(row => Enumerable.ToList<object>(data.DataColumns.Select(rdc => row[(string) rdc.ColumnName]))))
            {
                for (int i = 0; i < temp.Count; i++)
                {
                    if (temp[i] == DBNull.Value)
                    {
                        temp[i] = null;
                    }
                }
                SimpleDataRow rdr = new SimpleDataRow(temp);
                data.AddRow(rdr);
            }


            stopwatch.Stop();
            Logger.Instance.DebugFormat("ToBaseData runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
            return data;
        }

        /// <summary>
        /// Creates a new table 
        /// </summary>
        /// <param name="x">Table to filter.</param>
        /// <param name="fieldName">Field (column) to get the distinct values.</param>
        /// <returns></returns>
        public static List<string> FilterToDistinctList(this DataTable x, string fieldName)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var test = (from d in x.AsEnumerable()
                        select new
                        {
                            Name = d.Field<object>(fieldName)
                        }).Distinct().ToList();

            List<string> items = test.Select(id => id.Name).OfType<string>().ToList(); ;

            items.Sort();

            sw.Stop();

            Logger.Instance.DebugFormat("FilterToDistinctList runtime: {0}", new[] { sw.ElapsedMilliseconds.ToString() });

            return items;
        }

        /// <summary>
        /// Removes duplicate rows from a data table (looks at all columns).
        /// </summary>
        /// <param name="x">DataTable to remove duplicates.</param>
        /// <returns>A data table with the duplicates removed.</returns>
        public static DataTable RemoveDuplicateRows(this DataTable x)
        {
            Stopwatch sw = Stopwatch.StartNew();

            Hashtable hTable = new Hashtable();
            List<int> duplicateList = new List<int>();

            int i = 0;
            foreach (DataRow drow in x.Rows)
            {
                StringBuilder sb = new StringBuilder();
                foreach (DataColumn dc in x.Columns)
                {
                    sb.Append(drow[dc.ColumnName].ToString());
                    
                }
                if (hTable.Contains(sb.ToString()))
                {
                    duplicateList.Add(i);
                }
                else
                {
                    hTable.Add(sb.ToString(), i);
                }
                i++;
            }

            duplicateList.Reverse();

            foreach (int r in duplicateList)
            {
                x.Rows.RemoveAt(r);
            }
            x.AcceptChanges();

            sw.Stop();

            Logger.Instance.DebugFormat("RemoveDuplicateRows runtime: {0}", new[] { sw.ElapsedMilliseconds.ToString() });

            return x;
        }

        public static DataTable RemoveDuplicates(this DataTable dt)
        {
            // Find the unique contacts in the table.
            IEnumerable<DataRow> query = dt.AsEnumerable().Distinct(DataRowComparer.Default);

            DataSet ds = new DataSet();
            ds.Tables.Add(query.CopyToDataTable());

            return ds.Tables[0];
        }

        /// <summary>
        /// Gets a dataset filterd on HierarchyLevel
        /// </summary>
        /// <returns></returns>
        public static DataTable Filter(this DataTable dataTable, string queryString)
        {
            Stopwatch sw = Stopwatch.StartNew();

            Logger.Instance.DebugFormat("FilteredDataSet using querystring: {0}", new[] { queryString });
            DataRow[] rows = dataTable.Select(queryString);

            IEnumerable<DataRow> query = rows.ToList();

            DataSet ds = new DataSet();

            if (query.Any())
            {
                ds.Tables.Add(query.CopyToDataTable<DataRow>());
                dataTable.CopyColumnExtendedProperties(ds.Tables[0]);
            }

            Logger.Instance.DebugFormat("Filter (DataTable) runtime: {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });


            if(ds.Tables.Count > 0)
            {
                DataTable tempTable = ds.Tables[0];
                ds.Tables.RemoveAt(0);
                return tempTable;
            }
            return null;

        }

        /// <summary>
        /// Creates a new table 
        /// </summary>
        /// <param name="x">Table to filter.</param>
        /// <param name="fieldName">Field (column) to get the distinct values.</param>
        /// <param name="dataColumnFilter">Column in the source table to include in the new table.  If null all columns are included.</param>
        /// <returns></returns>
        public static DataTable FilterToDistinct(this DataTable x, string fieldName, List<string> dataColumnFilter = null)
        {
            DataTable dt = new DataTable(x.TableName);
            

            HashSet<string> addedSet = new HashSet<string>();
            string[] filter = null;

            Stopwatch sw = Stopwatch.StartNew();

            if (dataColumnFilter != null)
            {
                foreach (string s in dataColumnFilter)
                {
                    dt.AddColumn(s, x.Columns[s].DataType);
                }
                filter = dataColumnFilter.ToArray();
            }
            else
            {
                filter = new string[x.Columns.Count];
                int i = 0;
                foreach (DataColumn column in x.Columns)
                {
                    dt.AddColumn(column.ColumnName, column.DataType);
                    filter[i] = column.ColumnName;
                    i++;
                }
            }

            DataRow[] rows = x.Select("", fieldName);

            dt.BeginLoadData();
            foreach (DataRow dr in rows)
            {
                object value = dr[fieldName];
                if (!addedSet.Contains(value) && dt.SelectWhere(fieldName, value).Count == 0)
                {
                    object[] values = new object[filter.Length];
                    for (int i = 0; i < filter.Length; i++)
                    {
                        values[i] = dr[filter[i]];
                    }
                    dt.Rows.Add(values);
                    addedSet.Add(value.ToString());
                }
            }
            dt.EndLoadData();

            x.CopyColumnExtendedProperties(dt);

            sw.Stop();

            Logger.Instance.DebugFormat("FilterToDistinct runtime: {0}", new[] { sw.ElapsedMilliseconds.ToString() });

            return dt;
        }

        /// <summary>
        /// Converts a DataTable to a Typed List of objects.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="table"></param>
        /// <param name="columnNameMapping"></param>
        /// <returns></returns>
        public static IList<T> ConvertTo<T>(this DataTable table, Dictionary<string, string> columnNameMapping = null)
        {
            Stopwatch sw = Stopwatch.StartNew();

            if (table == null)
                return null;

            List<DataRow> rows = table.Rows.Cast<DataRow>().ToList();

            IList<T> r = rows.ConvertTo<T>(columnNameMapping);

            sw.Stop();

            Logger.Instance.DebugFormat("DataTable conversion to typed list runtime: {0}", new[] { sw.ElapsedMilliseconds.ToString() });

            return r;

        }

        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn to place where clause on.</param>
        /// <param name="value">Value to contains (Contains)</param>
        /// <returns>List of DataRow</returns>
        public static List<DataRow> SelectContains(this DataTable x, string columnName, string value)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var lst =  (from myRow in x.AsEnumerable()
                           where myRow.Field<string>(columnName).Contains(value)
                            select myRow).ToList();

            sw.Stop();
            Logger.Instance.DebugFormat("SelectContains runtime: {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });

            return lst;
        }
         //object d = new double();
         //           object s = String.Empty;
         //           object objectToPass = null;
         //           DataColumn dataColumn = table.GetDataColumn(measure);
         //           if (dataColumn.DataType == typeof(string))
         //           {
         //               objectToPass = s;
         //           }
         //           else if (dataColumn.DataType == typeof(double))
         //           {
         //               objectToPass = d;
         //           }



        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="orderBy">Column to orderby</param>
        /// <param name="ascending">Sort Assending (false to sort desending)</param>
        /// <param name="numRowToReturn">Filter the resulting DataTable</param>
        /// <returns>DataTable</returns>
        public static DataTable OrderBy(this DataTable x, string orderBy, bool ascending = true, int numRowToReturn = -1)
        {
            Stopwatch sw = Stopwatch.StartNew();

            DataColumn dataColumn = x.GetDataColumn(orderBy);
            object d = new double();
            object s = String.Empty;
            int i = new int();
            object objectToPass = null;
            if (dataColumn.DataType == typeof(string))
            {
                objectToPass = s;
            }
            else if (dataColumn.DataType == typeof(double))
            {
                objectToPass = d;
            }
            else if (dataColumn.DataType == typeof(Int32))
            {
                objectToPass = i;
            }

            DataTable dt = OrderBy(x, objectToPass, orderBy, ascending, numRowToReturn);

            sw.Stop();
            Logger.Instance.DebugFormat("OrderBy Runtime {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });

            return dt;
        }

        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        ///<typeparam name="T">DataType of the DataColumn.</typeparam>
        /// <param name="value">Value type</param>
        /// <param name="orderBy">Column to orderby</param>
        /// <param name="ascending">Sort Assending (false to sort desending)</param>
        /// <param name="numRowToReturn">Filter the resulting DataTable</param>
        /// <returns>DataTable</returns>
        public static DataTable OrderBy<T>(this DataTable x, T value, string orderBy, bool ascending = true, int numRowToReturn = -1)
        {
            OrderedEnumerableRowCollection<DataRow> query = null;
            if (ascending)
            {
                query = from c in x.AsEnumerable()
                        orderby c.Field<T>(orderBy)
                        select c;
            }
            else
            {
                query = from c in x.AsEnumerable()
                        orderby c.Field<T>(orderBy) descending
                        select c;
            }


            List<DataRow> rows = numRowToReturn > -1 ? query.Take(numRowToReturn).ToList() : query.ToList();

            DataTable dt =  rows.CopyToDataTable();
            x.CopyColumnExtendedProperties(dt);
            return dt;
        }

        /// <summary>
        /// Selects an List DataRow from a DataTable.
        /// </summary>
        /// <typeparam name="T">DataType of the DataColumn.</typeparam>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn to place where clause on.</param>
        /// <param name="value">Value must equal (where)</param>
        /// <returns>List of DataRow</returns>
        public static List<DataRow> SelectWhere<T>(this DataTable x, string columnName, T value)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var lst =  (from myRow in x.AsEnumerable()
                    where myRow.Field<T>(columnName).Equals(value)
                    select myRow).ToList();

            sw.Stop();
            Logger.Instance.DebugFormat("SelectWhere<T> runtime: {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });

            return lst;
        }

        

        /// <summary>
        /// Gets the data type of a DataColumn
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn</param>
        /// <returns>The data type.</returns>
        public static Type GetDataColumnType(this DataTable x, string columnName)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (String.IsNullOrEmpty(columnName)) throw new ArgumentException("Column name cannot be null or blank.");

            return !x.Columns.Contains(columnName) ? null : x.Columns[columnName].DataType;
        }

        /// <summary>
        /// Gets the DataColumn name
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnOrdinal">Ordinal (position) of the DataColumn</param>
        /// <returns>The data column.</returns>
        public static string GetDataColumnName(this DataTable x, int columnOrdinal)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (columnOrdinal == -1) throw new ArgumentException("Invalid Column Ordinal.");

            return x.Columns[columnOrdinal].ColumnName;
        }

        /// <summary>
        /// Gets the list of DataColumn names
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="stopperColumn">Column to stop at.</param>
        /// <returns>The data columns.</returns>
        public static List<string> GetDataColumnNames(this DataTable x, string stopperColumn = null)
        {
            if(stopperColumn == null)
            {
                stopperColumn = String.Empty;
            }

            return x.Columns.Cast<DataColumn>().TakeWhile(c => !c.ColumnName.Equals(stopperColumn)).Select(c => c.ColumnName).ToList();
        }


        /// <summary>
        /// Gets the DataColumn
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the DataColumn</param>
        /// <returns>The data column.</returns>
        public static DataColumn GetDataColumn(this DataTable x, string columnName)
        {
            if (x == null) throw new ArgumentException("Data Table is null.");
            if (String.IsNullOrEmpty(columnName)) throw new ArgumentException("Column name cannot be null or blank.");

            return !x.Columns.Contains(columnName) ? null : x.Columns[columnName];
        }

        /// <summary>
        /// Adds a DataColumn to a DataTable.
        /// </summary>
        /// <param name="x">DataTable to extend.</param>
        /// <param name="columnName">Name of the column to add.</param>
        /// <param name="columnType">Data type of the column.</param>
        /// <param name="expression">Optional.  Expression for the DataColumn.</param>
        /// <param name="columnCaption">Caption of the column</param>
        /// <param name="displayFormat">Numeric format of the column.</param>
        /// <returns>The DataColumn.</returns>
        [MethodImpl(MethodImplOptions.Synchronized)]
        public static DataColumn AddColumn(this DataTable x, string columnName, Type columnType, string expression = null, string columnCaption = null, string displayFormat = null)
        {
            Stopwatch sw = Stopwatch.StartNew();

            DataColumn dc = null;
            if (x == null)
            {
                throw new ArgumentException("DataTable argument is null.");
            }

            if (!x.Columns.Contains(columnName))
            {
                //Logger.Instance.Debug("AddColumn.ColumnNotFound");
                //Stopwatch sw2 = Stopwatch.StartNew();
                if (String.IsNullOrEmpty(expression))
                {
                    dc = x.Columns.Add(columnName, columnType);
                }
                else
                {
                    dc = x.Columns.Add(columnName, columnType, expression);
                }
                //sw2.Stop();
                //Logger.Instance.DebugFormat("AddColumn.Columns.Add: {0} (ms)", new[] { sw2.ElapsedMilliseconds.ToString() });
            }
            else
            {
                dc = x.Columns[columnName];
                if(!dc.Expression.Equals(expression)) dc.Expression = expression;
            }

            if(!String.IsNullOrEmpty(columnCaption) && dc != null)
            {
                dc.Caption = columnCaption;
            }

            if (displayFormat != null)
            {
                if (dc.ExtendedProperties.ContainsKey("DisplayFormat"))
                {
                    dc.ExtendedProperties.Remove("DisplayFormat");
                }
                dc.ExtendedProperties.Add("DisplayFormat", displayFormat);
            }

            sw.Stop();
            Logger.Instance.DebugFormat("AddColumn runtime: {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });

            return x.Columns[columnName];
         
        }
        /// <summary>
        /// Custom CopyToDataTable
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="table"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public static DataTable CopyToDataTableEx<T>(this IEnumerable<T> source, DataTable table = null, LoadOption? options = null)
        {
            Stopwatch sw = Stopwatch.StartNew();

            var ret = new ObjectShredder<T>().Shred(source, table, options);

            sw.Stop();

            Logger.Instance.DebugFormat("CopyToDataTableEx runtime: {0}", new[] { sw.ElapsedMilliseconds.ToString() });

            return ret;
        }


        /// <summary>
        /// This method will convert the supplied DataTable 
        /// to XML string.
        /// </summary>
        /// <param name="x">DataTable to be converted.</param>
        /// <returns>XML string format of the DataTable.</returns>
        public  static string ToXml(this DataTable x)
        {
            Stopwatch sw = Stopwatch.StartNew();

            DataSet dsData = new DataSet();
            try
            {
                StringBuilder sbSql = new StringBuilder();
                StringWriter swSql = new StringWriter(sbSql);
                dsData.Merge(x, true, MissingSchemaAction.AddWithKey);
                dsData.Tables[0].TableName = "SampleDataTable";
                foreach (DataColumn col in dsData.Tables[0].Columns)
                {
                    col.ColumnMapping = MappingType.Attribute;
                }
                dsData.WriteXml(swSql, XmlWriteMode.WriteSchema);

                sw.Stop();

                Logger.Instance.DebugFormat("ToXml runtime: {0}", new[] { sw.ElapsedMilliseconds.ToString() });

                return sbSql.ToString();
            }
            catch (Exception sysException)
            {
                throw sysException;
            }
        }

        /// <summary>
        /// Prints a datatable to the log4net file (Note this is an expensive operation).
        /// </summary>
        /// <param name="dt"></param>
        public static void Print(this DataTable dt)
        {
            Stopwatch sw = Stopwatch.StartNew();
            Logger.Instance.DebugFormat("{0} Table.", dt.TableName);
            StringBuilder s = new StringBuilder("\t");
            for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            {
                s.Append(dt.Columns[curCol].ColumnName.Trim()).Append("\t");

            }
            Logger.Instance.Debug(s.ToString());
            s = new StringBuilder("\t");
            for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    string sTemp = dt.Rows[curRow][curCol].ToString().Trim();
                    if(sTemp.Contains("\r\n"))
                    {
                        sTemp = sTemp.Replace("\r\n", " ");
                    }
                    s.Append(sTemp).Append("\t");
                }
                Logger.Instance.Debug(s.ToString());
                s = new StringBuilder("\t");
            }
            sw.Stop();

            Logger.Instance.DebugFormat("DataTable Print runtime: {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Prints a datatable to the console window (Note this is an expensive operation).
        /// </summary>
        /// <param name="dt"></param>
        public static void PrintToConsole(this DataTable dt)
        {
            Stopwatch sw = Stopwatch.StartNew();
            Console.WriteLine("{0} Table.", dt.TableName);
            StringBuilder s = new StringBuilder("\t");
            for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
            {
                s.Append(dt.Columns[curCol].ColumnName.Trim()).Append("\t");

            }
            Console.Write(s.ToString());
            s = new StringBuilder("\t");
            for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    string sTemp = dt.Rows[curRow][curCol].ToString().Trim();
                    if (sTemp.Contains("\r\n"))
                    {
                        sTemp = sTemp.Replace("\r\n", " ");
                    }
                    s.Append(sTemp).Append("\t");
                }
                Console.Write(s.ToString());
                s = new StringBuilder("\t");
            }
            sw.Stop();

            Logger.Instance.DebugFormat("DataTable PrintToConsole runtime: {0} (ms)", new[] { sw.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Prints a datatable to a csv file.
        /// </summary>
        /// <param name="dt">DataTable to print to csv.</param>
        /// <param name="fileName">Name of the csv file.</param>
        /// <param name="columnsToExclude">List of DataColumns to exclude from the csv file.</param>
        public static void PrintToCsv(this DataTable dt, string fileName, List<string> columnsToExclude)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            StringBuilder s = new StringBuilder();
            List<int> colIdxToEx = new List<int>();
            if(columnsToExclude == null)
            {
                columnsToExclude = new List<string>();
            }
            using (StreamWriter sw = new StreamWriter(fileName))
            {
                for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                {
                    if (columnsToExclude.Contains(dt.Columns[curCol].Caption.Trim()))
                    {
                        colIdxToEx.Add(curCol);
                        continue;
                    }
                    s.Append(dt.Columns[curCol].Caption.Trim());
                
                    if (curCol + 1 != dt.Columns.Count)
                    {
                        s.Append(",");    
                    }
                }
                sw.WriteLine(s.ToString());
                s.Clear();

                for (int curRow = 0; curRow < dt.Rows.Count; curRow++)
                {
                    for (int curCol = 0; curCol < dt.Columns.Count; curCol++)
                    {
                        if (colIdxToEx.Contains(curCol)) continue;

                        string sTemp = dt.Rows[curRow][curCol].ToString().Trim().EscapeCsvCharacters(); 
                        if(dt.Columns[curCol].DataType == typeof(String))
                        {
                            sTemp = sTemp.AddCharacterToString("\"");
                        }
 
                        s.Append(sTemp);

                        if (curCol + 1 != dt.Columns.Count)
                        {
                            s.Append(",");
                        }
                    }
                    sw.WriteLine(s.ToString());
                    s.Clear();
                }

                stopwatch.Stop();
            }

            Logger.Instance.DebugFormat("DataTable PrintToCsv runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Saves a dataset to an html file.
        /// </summary>
        /// <param name="dataTable"></param>
        /// <param name="pathFileName">Full path file name to save the file.</param>
        public static void ToHtml(this DataTable dataTable, string pathFileName)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.Append("<head>");
            sb.Append("<title>");
            sb.Append("Page-");
            sb.Append(Guid.NewGuid().ToString());
            sb.Append("</title>");
            sb.Append("</head>");
            sb.Append("<body>");


            sb.Append(dataTable.ToHtml());

            sb.Append("&nbsp;");
            sb.Append("&nbsp;");


            sb.Append("</body>");
            sb.Append("</html>");


            sb.ToFile(pathFileName);

            stopwatch.Stop();

            Logger.Instance.DebugFormat("DataTable ToHtml runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }

        public static string ToHtml(this DataTable dataTable)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.AppendFormat(@"<H2>");
            sb.AppendFormat(dataTable.TableName);
            sb.AppendFormat(@"</H2>");

            sb.Append("<table border='1px' cellpadding='5' cellspacing='0' ");
            sb.Append("style='border: solid 1px Silver; font-size: medium;'>");

            sb.Append("<TR ALIGN='CENTER'>");

            //first append the column names.
            foreach (DataColumn column in dataTable.Columns)
            {
                sb.Append("<TH>");
                sb.Append(column.ColumnName);
                sb.Append("</TH>");
            }

            sb.Append("</TR>");

            // next, the column values.
            foreach (DataRow row in dataTable.Rows)
            {
                sb.Append("<TR ALIGN='CENTER'>");

                foreach (DataColumn column in dataTable.Columns)
                {
                    sb.Append("<TD>");
                    if (row[column].ToString().Trim().Length > 0)
                    {
                        sb.Append(row[column]);
                    }
                    else
                    {
                        sb.Append("&nbsp;");
                    }
                    sb.Append("</TD>");
                }

                sb.Append("</TR>");
            }
            sb.Append("</TABLE>");


            stopwatch.Stop();

            Logger.Instance.DebugFormat("DataTable ToHtml runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return sb.ToString();
        }

    }
}
