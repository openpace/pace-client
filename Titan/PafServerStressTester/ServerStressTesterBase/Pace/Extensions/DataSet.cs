﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class DataSetExtension
    {
        /// <summary>
        /// Creates a new DataSet and adds the specified DataTable.
        /// </summary>
        /// <param name="x">DataTable to add.</param>
        /// <returns>A DataSet with a DataTable added to the collection.</returns>
        public static DataSet CreateNewWithTable( DataTable x)
        {
            DataSet dataSet = new DataSet();
            dataSet.Tables.Add(x);
            return dataSet;
        }

        /// <summary>
        /// Gets the DataTable from the DataSet.
        /// </summary>
        /// <param name="x">Extended DataSet</param>
        /// <param name="columnName">ColumnName (text) to find in the table of the DataSet.</param>
        /// <returns>DataTable</returns>
        public static DataTable GetDataTable(this DataSet x, string columnName)
        {
            return (from DataTable tbl in x.Tables from DataColumn dr in tbl.Columns where dr.ColumnName.EqualsIgnoreCase(columnName) select tbl).FirstOrDefault();
        }

        /// <summary>
        /// Will delete rows where all columns are null or optionally zero.
        /// </summary>
        /// <param name="x">Dataset to modify.</param>
        /// <param name="purgeZeros">Remove zeros as well as nulls.</param>
        /// <param name="columnsToProcess">Process specific columns, null to process all.</param>
        /// <returns>True if something is fixed.</returns>
        public static bool PurgeNulls(this DataSet x, bool purgeZeros = false, List<string> columnsToProcess = null)
        {
            bool ret = false;
            foreach(DataTable t in x.Tables)
            {
                if(t.RemoveRowsWithNull(purgeZeros, columnsToProcess))
                {
                    ret = true;
                }
            }
            return ret;
        }

        /// <summary>
        /// Will replace nulls with zero or empty string.
        /// </summary>
        /// <param name="x">Dataset to modify.</param>
        /// <param name="columnsToIgnore">Ignore specific columns, null to process all.</param>
        /// <returns>True if something is fixed.</returns>
        public static bool ReplaceNullsWithZeroEmpty(this DataSet x, List<string> columnsToIgnore = null)
        {
            bool ret = false;
            foreach (DataTable t in x.Tables)
            {
                if (t.ReplaceNullsWithZero(columnsToIgnore))
                {
                    ret = true;
                }
            }
            return ret;
        }

        /// <summary>
        /// Prints a dataset to the log4net file window (Note this is an expensive operation).
        /// </summary>
        /// <param name="ds"></param>
        public static void Print(this DataSet ds)
        {

            //Console.WriteLine("Tables in '{0}' DataSet.\n", ds.DataSetName);
            Logger.Instance.DebugFormat("Tables in '{0}' DataSet.", ds.DataSetName);
            foreach (DataTable dt in ds.Tables)
            {
                dt.Print();
            }
        }

        /// <summary>
        /// Prints a dataset to the console window (Note this is an expensive operation).
        /// </summary>
        /// <param name="ds"></param>
        public static void PrintToConsole(this DataSet ds)
        {

            Console.WriteLine("Tables in '{0}' DataSet.\n", ds.DataSetName);
            foreach (DataTable dt in ds.Tables)
            {
                dt.PrintToConsole();
            }
        }

        /// <summary>
        /// Prints a dataset to a csv file (Note this is an expensive operation).
        /// </summary>
        /// <param name="ds"></param>
        /// <param name="directory">Directory to save the files</param>
        /// <param name="fileSuffix">Suffix to add to the file name</param>
        public static void PrintToCsv(this DataSet ds, string directory,  string fileSuffix)
        {
            foreach (DataTable dt in ds.Tables)
            {
                string filePath = Path.Combine(directory, fileSuffix + dt.TableName + ".csv");
                dt.PrintToCsv(filePath, null);
            }
        }

        /// <summary>
        /// Saves a dataset to an html file.
        /// </summary>
        /// <param name="dataSet"></param>
        /// <param name="pathFileName">Full path file name to save the file.</param>
        public static void ToHtml(this DataSet dataSet, string pathFileName)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            
            System.Text.StringBuilder sb = new System.Text.StringBuilder();

            sb.Append(@"<html xmlns='http://www.w3.org/1999/xhtml'>");
            sb.Append("<head>");
            sb.Append("<title>");
            sb.Append("Page-");
            sb.Append(Guid.NewGuid().ToString());
            sb.Append("</title>");
            sb.Append("</head>");
            sb.Append("<body>");

            foreach (DataTable thisTable in dataSet.Tables)
            {
                sb.Append(thisTable.ToHtml());

                sb.Append("&nbsp;");
                sb.Append("&nbsp;");
            }

            sb.Append("</body>");
            sb.Append("</html>");


            sb.ToFile(pathFileName);

            stopwatch.Stop();

            Logger.Instance.DebugFormat("Dataset ToHtml runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }
    }
}
