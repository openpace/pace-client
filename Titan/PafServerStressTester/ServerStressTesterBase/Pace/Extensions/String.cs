﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using ServerStressTesterBase.Pace.Data;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class StringExtensions
    {
        private const string QUOTE = "\"";
        private const string EscapedQuote = "\"\"";
        private static readonly char[] CharactersThatMustBeQuoted = { ',', '"' };

        /// <summary>
        /// Create an ColumnName IN (1,2,3,4) query string
        /// </summary>
        /// <param name="numbers">Array of numbers</param>
        /// <param name="columnName">Column Name</param>
        /// <param name="includeNot">Make the query an "Not IN"</param>
        /// <returns></returns>
        public static string ToIntQueryString(this int[] numbers, string columnName, bool includeNot = false)
        {
            if(numbers == null || numbers.Count() == 0)
            {
                return String.Empty;
            }

            string result = string.Join(",", numbers);

            string query = String.Empty;
            if(includeNot)
            {
                query = columnName + " NOT IN (" + result + ")";
            }
            else
            {
                query = columnName + " IN (" + result + ")";
            }

            return query;
        }

        /// <summary>
        /// Adds the proper escape characters to a string so it can be written to a csv file.
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string EscapeCsvCharacters(this string s)
        {
            if (s.Contains(QUOTE))
            {
                s = s.Replace(QUOTE, EscapedQuote);
            }

            if (s.IndexOfAny(CharactersThatMustBeQuoted) > -1)
            {
                s = QUOTE + s + QUOTE;
            }

            return s;
        }

        /// <summary>
        /// Concatentates a string to the begining and end of a string.
        /// </summary>
        /// <param name="s">Initial String</param>
        /// <param name="charToAdd">String to add to begining and end of string.</param>
        /// <param name="addIfExists">Add another character if the string already stats with the char.</param>
        /// <returns></returns>
        public static string AddCharacterToString(this string s, string charToAdd, bool addIfExists = false)
        {
            StringBuilder sb = new StringBuilder(s);

            if(!s.StartsWith(charToAdd) || addIfExists)
            {
                sb.Insert(0, charToAdd);
            }

            if (!s.EndsWith(charToAdd) || addIfExists)
            {
                sb.Append(charToAdd);
            }

            return sb.ToString();
        }

        /// <summary>
        /// Prints a string to a file.
        /// </summary>
        /// <param name="s">String to print to file.</param>
        /// <param name="pathFileName">Full path and file name to save the file.</param>
        public static void ToFile(this string s, string pathFileName)
        {
            StringBuilder sb = new StringBuilder(s);
            sb.ToFile(pathFileName);
        }

        /// <summary>
        /// Prints a stringbuilder to a file.
        /// </summary>
        /// <param name="sb">StringBuilder to print to file.</param>
        /// <param name="pathFileName">Full path and file name to save the file.</param>
        public static void ToFile(this StringBuilder sb, string pathFileName)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            using (StreamWriter outfile = new StreamWriter(pathFileName))
            {
                outfile.Write(sb.ToString());
            }
            stopwatch.Stop();
            Logger.Instance.DebugFormat("ToFile() runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }

        /// <summary>
        /// Gets the numeric format value from a simple numeric format string (F4, P5).
        /// </summary>
        /// <param name="s"></param>
        /// <returns>NumberFormat</returns>
        public static NumberFormat GetNumericFormat(this string s)
        {
            if (String.IsNullOrEmpty(s)) return 0;
            if (s.Length == 1) return 0;

            string n = s.Substring(0, 1);

            switch (n.ToUpper())
            {
                case "P": 
                    return NumberFormat.Percent;
                case "N":
                    return NumberFormat.Number;
                case "C":
                    return NumberFormat.Currency;
                case "E":
                    return NumberFormat.Scientific;
                case "F":
                    return NumberFormat.FixedPoint;
                case "G":
                    return NumberFormat.General;
                default:
                    return NumberFormat.General;
            }
        }


        /// <summary>
        /// Gets the percision value from a simple numeric format string (F4, P5).
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static int GetNumericFormatPercision(this string s)
        {
            if (String.IsNullOrEmpty(s)) return 0;
            if (s.Length == 1) return 0;

            int i;
            return int.TryParse(s.Substring(1, 1), out i) ? i : 0;
        }

        /// <summary>
        /// Removes invalid chars from the file name.
        /// </summary>
        /// <param name="pathFileName">Path/file name</param>
        /// <returns>True if the path is valid, false if not.</returns>
        public static string RemoveInvalidFileChars(this string pathFileName)
        {
            Char[] invalidChars = Path.GetInvalidFileNameChars();

            List<char> badChars = invalidChars.ToList();
            badChars.Add('-');
            badChars.Add(' ');


            foreach (char c in badChars)
            {
                if(pathFileName.Contains(c))
                {
                    pathFileName = pathFileName.Replace(c.ToString(), String.Empty);
                }
            }
            return pathFileName;
        }

        /// <summary>
        /// Removes the x chars off of a string.
        /// </summary>
        /// <param name="s">string to shorten</param>
        /// <param name="removeFromFront">Remove from the front of the string.</param>
        /// <param name="charsToRemove">Chars to remove</param>
        /// <returns></returns>
        public static string Shorten(this string s, bool removeFromFront = true, int charsToRemove = 1)
        {
            if(String.IsNullOrEmpty(s)) return String.Empty;
            if(s.Length <= charsToRemove)
            {
                return s;
            }
            if (!removeFromFront)
            {
               return s.Remove(s.Length - 1, charsToRemove);
            }
            else
            {
                return s.Remove(0, charsToRemove);
            }
        }

        /// <summary>
        /// Adds spaces in between capatial letters.
        /// </summary>
        /// <param name="x">string to convert.</param>
        /// <returns>Returns a string with spaces example:  (pass: FooBarMe, returns: Foo Bar Me)</returns>
        public static string AddSpaces(this string x)
        {
            if (string.IsNullOrEmpty(x))return "";

            StringBuilder newText = new StringBuilder(x.Length * 2);
            newText.Append(x[0]);
            for (int i = 1; i < x.Length; i++)
            {
                if (char.IsUpper(x[i]) && !char.IsUpper(x[i - 1]))
                {
                    newText.Append(' ');
                }
                newText.Append(x[i]);
            }
            return newText.ToString();
        }

        /// <summary>
        /// Converts a string to a Base64 encoded string.
        /// </summary>
        /// <param name="x">String to convert.</param>
        /// <returns>a base64 encoded string.</returns>
        static public string ToBase64(this string x)
        {
            x = Convert.ToBase64String(x.ToByteArrayAscii());
            return x;
        }


        /// <summary>
        /// Converts an Base64 encoded string to a string.
        /// </summary>
        /// <param name="x">String to convert.</param>
        /// <returns>a base64 encoded string.</returns>
        static public string FromBase64(this string x)
        {
            byte[] b = Convert.FromBase64String(x);
            x = b.FromByteArrayAscii();
            return x;
        }

        /// <summary>
        /// Converts a string to an ascii encoded byte array.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static byte[] ToByteArrayAscii(this string x)
        {
            return Encoding.ASCII.GetBytes(x);
        }

        /// <summary>
        /// Converts an encoded byte array to a string.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string FromByteArrayAscii(this byte[] x)
        {
            return Encoding.ASCII.GetString(x);
        }

        /// <summary>
        /// Gets the directory the assemby is running.
        /// </summary>
        /// <param name="x">String to extend.</param>
        /// <param name="addFileName">Filename to add at the end of the path.</param>
        /// <returns></returns>
        static public string AssemblyDirectory(this string x, string addFileName = null)
        {

            string codeBase = Assembly.GetExecutingAssembly().CodeBase;
            UriBuilder uri = new UriBuilder(codeBase);
            string path = Uri.UnescapeDataString(uri.Path);
            if (String.IsNullOrEmpty(addFileName))
            {
                return Path.GetDirectoryName(path);
            }
            else
            {
                return Path.GetDirectoryName(path) + "\\" + addFileName;
            }

        }

        /// <summary>
        /// Generates a unique string using a GUID.
        /// </summary>
        /// <returns></returns>
        public static string GenerateUniqueString()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int) b + 1);
            }
            return string.Format("{0:x}", i - DateTime.Now.Ticks);
        }

        /// <summary>
        /// Perform a string comparsion, but ignore the case of the strings.
        /// Trim() is called on the strings before the comparison is performed.
        /// </summary>
        /// <param name="x">string to extend</param>
        /// <param name="compareTo">String to perform the comparsion against.</param>
        /// <returns>true if the strings match, false if not or if either of the strings are null.</returns>
        public static bool EqualsIgnoreCase(this string x, string compareTo)
        {
            if (x == null || compareTo == null) return false;

            return String.Compare(x.Trim(), compareTo.Trim(), true) == 0;
        }


        /// <summary>
        /// Convert a delimited string into a list of strings
        /// </summary>
        /// <param name="x">Source string to parse.</param>
        /// <param name="delimiter">Delimiter to parse the string with.</param>
        /// <returns></returns>
        public static List<string> ConverttoList(this string x, string delimiter)
        {
            string[] stringArray = x.Split(new [] { delimiter }, StringSplitOptions.RemoveEmptyEntries);

            return stringArray.ToList();
        }


        /// <summary>
        /// Convert a delimited string into a Dictionary of strings
        /// </summary>
        /// <param name="x">Source string to parse.</param>
        /// <param name="delimiter1">First delimiter to parse.</param>
        /// <param name="delimiter2">Second delimiter to parse.</param>
        /// <returns></returns>
        public static List<List<String>> ConverttoListofLists(this string x, string delimiter1, string delimiter2)
        {
            List<String> longStrings = x.ConverttoList(delimiter1);

            return longStrings.Select(str => ConverttoList(str, delimiter2)).ToList();
        }

        /// <summary>
        /// Convert a delimited string into a Dictionary
        /// </summary>
        /// <param name="x">Source string to parse.</param>
        /// <param name="delimiter1">First delimiter to parse.</param>
        /// <param name="delimiter2">Second delimiter to parse.</param>
        /// <returns></returns>
        public static Dictionary<int, String> ConverttoDictionary(this string x, string delimiter1, string delimiter2)
        {
            List<String> longStrings = x.ConverttoList(delimiter1);

            return longStrings.Select(str => ConverttoList(str, delimiter2)).ToDictionary(shortStrings => int.Parse(shortStrings[1]), shortStrings => shortStrings[0]);
        }

        /// <summary>
        /// Creates a delimited string from a string list, and an element delimiter.
        /// </summary>
        /// <param name="x">String array to delimit</param>
        /// <param name="elementDelimiter">Delimiter to add between each array element.</param>
        /// <returns></returns>
        public static string CreateDelimitedString(this List<string> x, string elementDelimiter = ",")
        {

            return CreateDelimitedString(x.ToArray(), elementDelimiter);
        }

        /// <summary>
        /// Creates a delimited string from a string array, and an element delimiter.
        /// </summary>
        /// <param name="x">String array to delimit</param>
        /// <param name="elementDelimiter">Delimiter to add between each array element.</param>
        /// <returns></returns>
        public static string CreateDelimitedString(this string[] x, string elementDelimiter)
        {

            return String.Join(elementDelimiter, x);
        }


        /// <summary>
        /// Splits a string into an array.
        /// </summary>
        /// <param name="x">String to split.</param>
        /// <param name="delimiter">Delimiter (used to split string)</param>
        /// <param name="removeBlanks">Remove blank values from the return string array.</param>
        /// <returns>An array of strings.</returns>
        public static string[] SplitString(this string x, string delimiter, bool removeBlanks)
        {
            if (String.IsNullOrEmpty(x) || String.IsNullOrEmpty(delimiter))
            {
                return null;
            }

            //StringSplitOptions sso = removeBlanks ? StringSplitOptions.RemoveEmptyEntries : StringSplitOptions.None;

            StringSplitOptions sso;
            if(removeBlanks)
            {
                sso = StringSplitOptions.RemoveEmptyEntries;
            }
            else
            {
                sso = StringSplitOptions.None;
            }

            string[] stringArray = x.Split(new [] { delimiter }, sso);

            return stringArray;
        }

        /// <summary>
        /// Inserts a string inside of another string at the first found occurence of stringToFind.
        /// </summary>
        /// <param name="x">The string to search.</param>
        /// <param name="stringToFind">The string to find.</param>
        /// <param name="insertionString">The string to insert.</param>
        public static string InsertString(this string x, string stringToFind, string insertionString)
        {
            int pos = x.IndexOf(stringToFind);
            return pos > 0 ? x.Insert(pos, insertionString) : x;
        }

        /// <summary>
        /// Generates a random number (integer) (string representation) with the given length
        /// </summary>
        /// <param name="x">String to extend.</param>
        /// <param name="size">Size of the string</param>
        /// <returns>Random string</returns>
        public static string RandomNumber(this string x, int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            for (int i = 0; i < size; i++)
            {
                int num = random.Next(0, 10);
                builder.Append(num);
            }
            return builder.ToString();
        }

        /// <summary>
        /// Extended Equals method that does not throw a null reference exception, just return null.
        /// </summary>
        /// <param name="x">Base string.</param>
        /// <param name="compareTo">String to compare to.</param>
        /// <returns>True if the strings are equal or both are null, false if not or either are null.</returns>
        public static bool StringEquals(this String x, string compareTo)
        {
            if(x == null && compareTo == null)
            {
                return true;
            }

            if (x == null)
            {
                return false;
            }

            if (compareTo == null)
            {
                return false;
            }

            return x.Equals(compareTo);
        }

        ///// <summary>
        ///// Validates if the string is in a valid URI/URL format.
        ///// </summary>
        ///// <param name="x">String to extend/validate.</param>
        ///// <returns>true if the string is in a valid format, false if not.</returns>
        //public static bool IsUrlValid(this string x)
        //{
        //    try
        //    {
        //        Uri uri;
        //        if (Uri.TryCreate(x, UriKind.Absolute, out uri))
        //        {
        //            if(!UriScheme.SchemeIsValid(uri.Scheme))
        //            {
        //                return false;
        //            }
        //            if (uri.Port == -1)
        //            {
        //                return false;
        //            }
        //            if (String.IsNullOrEmpty(uri.Host))
        //            {
        //                return false;
        //            }

        //            return true;
        //        }
        //        return false;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
            
        //    }
        //}
    }
}
