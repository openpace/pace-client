﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System.IO;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class PathExtension
    {
        /// <summary>
        /// Gets a Temporary Directory.
        /// </summary>
        /// <returns></returns>
        public static string GetTempDirectory() 
        {
            string path;
            do 
            {
                path = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName()); 
            } while(Directory.Exists(path));

            Directory.CreateDirectory(path); 

            return path; 
        }
    }
}
