﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using ServerStressTesterBase.Pace.LogUtils;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class FileInfoExtension
    {
        public const string ZipFileSignature = "50-4B-03-04";

        /// <summary>
        /// Checks a file signature.
        /// </summary>
        /// <param name="fi"></param>
        /// <param name="signatureSize"></param>
        /// <param name="expectedSignature"></param>
        /// <returns></returns>
        public static bool CheckSignature(this FileInfo fi, int signatureSize, string expectedSignature)
        {
            if (String.IsNullOrEmpty(fi.FullName)) throw new ArgumentException("Must specify a filepath");
            if (String.IsNullOrEmpty(expectedSignature)) throw new ArgumentException("Must specify a value for the expected file signature");
            using (FileStream fs = new FileStream(fi.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                if (fs.Length < signatureSize) return false; byte[] signature = new byte[signatureSize];
                int bytesRequired = signatureSize; int index = 0;
                while (bytesRequired > 0)
                {
                    int bytesRead = fs.Read(signature, index, bytesRequired);
                    bytesRequired -= bytesRead; index += bytesRead;
                } string actualSignature = BitConverter.ToString(signature);
                if (actualSignature == expectedSignature) return true; else return false;
            }
        }

        /// <summary>
        /// Gets the file name without the extension.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public static string NameWithOutExtension(this FileInfo x)
        {
            return Path.GetFileNameWithoutExtension(x.Name);
        }

        /// <summary>
        /// Validates a file extension.
        /// </summary>
        /// <param name="x"></param>
        /// <param name="fileExtension"></param>
        /// <returns></returns>
        public static bool FileExtensionValid(this FileInfo x, string fileExtension)
        {
            FileInfo fi = new FileInfo(x.FullName);

            if (!fi.Extension.ToLower().Equals(fileExtension))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Compress a file or directory
        /// </summary>
        /// <param name="fi">FileInfo object that hold the reference to file or directory.</param>
        /// <param name="fileName">(Optional) Name of the compress file.</param>
        /// <returns>The name of the compressed file.</returns>
        public static string Compress(this FileInfo fi, string fileName = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            string pfileName = null;
            // Get the stream of the source file.
            using (FileStream inFile = fi.OpenRead())
            {
                // Prevent compressing hidden and 
                // already compressed files.
                if ((File.GetAttributes(fi.FullName)
                    & FileAttributes.Hidden)
                    != FileAttributes.Hidden & fi.Extension != ".gz")
                {
                    string compressFile = null;
                    if(String.IsNullOrEmpty(fileName))
                    {
                        compressFile = fi.FullName + ".gz";
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(new FileInfo(fileName).Extension))
                        {
                            compressFile = fileName + ".gz";
                        }
                        else
                        {
                            compressFile = fileName;
                        }
                        
                    }
                    // Create the compressed file.
                    using (FileStream outFile = File.Create(compressFile))
                    {
                        pfileName = outFile.Name;
                        using (GZipStream compress = new GZipStream(outFile, CompressionMode.Compress))
                        {
                            // Copy the source file into 
                            // the compression stream.
                            inFile.CopyTo(compress);

                            Logger.Instance.InfoFormat("Compressed {0} from {1} to {2} bytes.", fi.Name, fi.Length.ToString(), outFile.Length.ToString());
                        }
                    }
                }
            }

            stopwatch.Stop();
            Logger.Instance.DebugFormat("Compress runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return pfileName;
        }

        /// <summary>
        /// Writes the zip file.
        /// </summary>
        /// <param name="zipFileName">The destination path.</param>
        /// <param name="filesToZip">The files to zip.</param>
        /// <param name="compression">The compression level.</param>
        public static void WriteZipFile(this FileInfo zipFileName, List<string> filesToZip, int compression = 5)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            if (zipFileName == null)
                return;

            if (compression < 0 || compression > 9)
                throw new ArgumentException("Invalid compression rate.");

            if (zipFileName.Directory != null && !Directory.Exists(zipFileName.Directory.ToString()))
                throw new ArgumentException("The Path does not exist.");

            foreach (string c in filesToZip.Where(c => !File.Exists(c)))
                throw new ArgumentException(string.Format("The File{0}does not exist!", c));

            try
            {
                // Depending on the directory this could be very large and would require more attention
                // in a commercial package.
                string[] filenames = filesToZip.ToArray();

                // 'using' statements guarantee the stream is closed properly which is a big source
                // of problems otherwise.  Its exception safe as well which is great.
                using (ZipOutputStream s = new ZipOutputStream(File.Create(zipFileName.FullName)))
                {

                    s.SetLevel(compression); // 0 - store only to 9 - means best compression

                    byte[] buffer = new byte[4096];

                    foreach (string file in filenames)
                    {

                        // Using GetFileName makes the result compatible with XP
                        // as the resulting path is not absolute.
                        ZipEntry entry = new ZipEntry(Path.GetFileName(file));

                        // Setup the entry data as required.

                        // Crc and size are handled by the library for seakable streams
                        // so no need to do them here.

                        // Could also use the last write time or similar for the file.
                        entry.DateTime = DateTime.Now;
                        s.PutNextEntry(entry);

                        using (FileStream fs = File.OpenRead(file))
                        {

                            // Using a fixed size buffer here makes no noticeable difference for output
                            // but keeps a lid on memory usage.
                            int sourceBytes;
                            do
                            {
                                sourceBytes = fs.Read(buffer, 0, buffer.Length);
                                s.Write(buffer, 0, sourceBytes);
                            } while (sourceBytes > 0);
                        }
                    }

                    // Finish/Close arent needed strictly as the using statement does this automatically

                    // Finish is important to ensure trailing information for a Zip file is appended.  Without this
                    // the created file would be invalid.
                    s.Finish();

                    // Close is important to wrap things up and unlock the file.
                    s.Close();
                }
            }
            catch (Exception ex)
            {
                Logger.Instance.ErrorFormat("Exception during processing {0}", ex);
            }

            stopwatch.Stop();
            Logger.Instance.DebugFormat("WriteZipFile runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
        }


        /// <summary>
        /// Decompress a zip file
        /// </summary>
        /// <param name="zipFileName">Name of the zip file.</param>
        /// <param name="password">Password (if necessary) for the zip file.</param>
        /// <param name="outFolder">Location extract to (if necessary).</param>
        /// <returns></returns>
        public static List<string> DecompressZipFile(this FileInfo zipFileName, string password = null, string outFolder = null)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();
            List<string> files = new List<string>();

            if(outFolder == null)
            {
                outFolder = zipFileName.DirectoryName;
            }

            ZipFile zf = null;
            try
            {
                FileStream fs = File.OpenRead(zipFileName.FullName);
                zf = new ZipFile(fs);
                if (!String.IsNullOrEmpty(password))
                {
                    zf.Password = password;		// AES encrypted entries are handled automatically
                }
                foreach (ZipEntry zipEntry in zf)
                {
                    if (!zipEntry.IsFile)
                    {
                        continue;			// Ignore directories
                    }
                    String entryFileName = zipEntry.Name;
                    // to remove the folder from the entry:- entryFileName = Path.GetFileName(entryFileName);
                    // Optionally match entrynames against a selection list here to skip as desired.
                    // The unpacked length is available in the zipEntry.Size property.

                    byte[] buffer = new byte[4096];		// 4K is optimum
                    Stream zipStream = zf.GetInputStream(zipEntry);

                    // Manipulate the output filename here as desired.
                    String fullZipToPath = Path.Combine(outFolder, entryFileName);
                    files.Add(fullZipToPath);
                    string directoryName = Path.GetDirectoryName(fullZipToPath);
                    if (directoryName.Length > 0)
                        Directory.CreateDirectory(directoryName);

                    // Unzip file in buffered chunks. This is just as fast as unpacking to a buffer the full size
                    // of the file, but does not waste memory.
                    // The "using" will close the stream even if an exception occurs.
                    using (FileStream streamWriter = File.Create(fullZipToPath))
                    {
                        StreamUtils.Copy(zipStream, streamWriter, buffer);
                    }
                }
                return files;
            }
            finally
            {
                if (zf != null)
                {
                    zf.IsStreamOwner = true; // Makes close also shut the underlying stream
                    zf.Close(); // Ensure we release resources
                }

                stopwatch.Stop();
                Logger.Instance.DebugFormat("DecompressZipFile runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });
            }
        }

        /// <summary>
        /// Decompress a file or directory
        /// </summary>
        /// <param name="fi">FileInfo object that hold the reference to file or directory.</param>
        /// <returns>The list of files that were decompressed.</returns>
        public static string[] Decompress(this FileInfo fi)
        {
            Stopwatch stopwatch = Stopwatch.StartNew();

            List<string> files = new List<string>();
            // Get the stream of the source file.
            using (FileStream inFile = fi.OpenRead())
            {
                // Get original file extension, for example
                // "doc" from report.doc.gz.
                string curFile = fi.FullName;
                string origName = curFile.Remove(curFile.Length - fi.Extension.Length);

                //Create the decompressed file.
                using (FileStream outFile = File.Create(origName))
                {
                    using (GZipStream decompress = new GZipStream(inFile, CompressionMode.Decompress))
                    {
                        // Copy the decompression stream 
                        // into the output file.
                        decompress.CopyTo(outFile);
                        files.Add(outFile.Name);
                        Logger.Instance.InfoFormat("Decompressed: {0}", fi.Name);

                    }
                }
            }

            stopwatch.Stop();
            Logger.Instance.DebugFormat("Decompress runtime: {0} (ms)", new[] { stopwatch.ElapsedMilliseconds.ToString() });

            return files.ToArray();
        }
    }
}
