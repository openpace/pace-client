﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion

using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace ServerStressTesterBase.Pace.Extensions
{
    public static class ObjectExtension
    {

        /// <summary>
        /// Converts a two dimension object array to a single dimension double array.
        /// </summary>
        /// <param name="array">Object array to convert.</param>
        /// <returns>A one dimensional double array.</returns>
        public static double[] ConvertTwoDimObjectArrayToDoubleArray(this Object array)
        {
            object[,] obj = (object[,])array;
            double[] d = new double[obj.Length];
            try
            {
                //Copy the multidimension object array to a single dimension 
                //string array so it can be outputed to the log.
                int i = 0;
                for (int r = obj.GetLowerBound(0); r <= obj.GetUpperBound(0); r++)
                {
                    for (int c = obj.GetLowerBound(1); c <= obj.GetUpperBound(1); c++)
                    {
                        d[i] = Convert.ToDouble(obj[r, c]);
                        //str[i] = obj[r, c].ToString();
                        i++;
                    }
                }
                return d;
            }
            catch (Exception)
            {
                return new double[] { 0 };
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="o">Object to extend</param>
        /// <param name="compareTo">Object to compare to.</param>
        /// <returns></returns>
        public static bool EqualsObj(this object o, object compareTo)
        {
            Type typeX = o.GetType();
            if (typeX == typeof(String))
            {
                string x = (string) o;
                string to = (string) compareTo;
                return x.Equals(to);
            }
            if (typeX == typeof(Int32))
            {
                int x = Convert.ToInt32(o);
                int to = Convert.ToInt32(compareTo);
                return x.Equals(to);
            }
            if (typeX == typeof(Int64))
            {
                long x = Convert.ToInt64(o);
                long to = Convert.ToInt64(compareTo);
                return x.Equals(to);
            }
            if (typeX == typeof(Double))
            {
                double x = Convert.ToDouble(o);
                double to = Convert.ToDouble(compareTo);
                return x.Equals(to);
            }
            if (typeX == typeof(Boolean))
            {
                bool x = Convert.ToBoolean(o);
                bool to = Convert.ToBoolean(compareTo);
                return x.Equals(to);
            }
            if (typeX == typeof(Byte))
            {
                byte x = Convert.ToByte(o);
                byte to = Convert.ToByte(compareTo);
                return x.Equals(to);
            }
            if (typeX == typeof(Decimal))
            {
                decimal x = Convert.ToDecimal(o);
                decimal to = Convert.ToDecimal(compareTo);
                return x.Equals(to);
            }
            return false;
        }



        /// <summary>
        /// Serializes an object to an xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="x">Object to extend.</param>
        /// <param name="filePath">Path to serialize the file to.</param>
        /// <param name="fileName">Filename to serialize to.</param>
        /// <param name="typeInherited"></param>
        public static void Save<T>(this object x, string filePath, string fileName, Type typeInherited = null)
        {
            string pathFileName = Path.Combine(filePath, fileName);
            x.Save<T>(pathFileName, typeInherited);
        }

        /// <summary>
        /// Serializes an object to an xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="x">Object to extend.</param>
        /// <param name="fileName">Filename to serialize to.</param>
        /// <param name="typeInherited"></param>
        public static void Save<T>(this object x, string fileName, Type typeInherited = null)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                throw new NullReferenceException("Filename cannot be null.");
            }


            if (!Path.HasExtension(fileName))
            {
                fileName += ".xml";
            }

            using (var stream = File.Create(fileName))
            {
                if (typeInherited == null)
                {
                    new XmlSerializer(typeof(T)).Serialize(stream, x);
                }
                else
                {
                    Type[] types = new[] { typeInherited };
                    new XmlSerializer(typeof(T), types).Serialize(stream, x);
                }
            }
        }


        /// <summary>
        /// Serializes an object to an XML string.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="x">Object to extend.</param>
        /// <returns></returns>
        public static XElement ToXml<T>(this object x)
        {
            var xmlStream = new MemoryStream();
            new XmlSerializer(typeof(T)).Serialize(xmlStream, x);
            xmlStream.Flush();
            xmlStream.Seek(0, SeekOrigin.Begin);
            var xml = XDocument.Load(XmlReader.Create(xmlStream));
            return xml.Elements().First();
        }

        /// <summary>
        /// Loads an object from a serialized xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="filePath">Path to serialize the file to.</param>
        /// <param name="x">Object to extend.</param>
        /// <param name="fileName"></param>
        /// <param name="typeInherited"></param>
        public static T Load<T>(this object x, string filePath, string fileName, Type typeInherited = null)
        {
            string pathFileName = Path.Combine(filePath, fileName);
            return x.Load<T>(pathFileName, typeInherited);
        }

        /// <summary>
        /// Loads an object from a serialized xml file.
        /// </summary>
        /// <typeparam name="T">Object Type.</typeparam>
        /// <param name="x">Object to extend.</param>
        /// <param name="fileName"></param>
        /// <param name="typeInherited"></param>
        public static T Load<T>(this object x, string fileName, Type typeInherited = null)
        {
            if (String.IsNullOrEmpty(fileName))
            {
                throw new NullReferenceException("Filename cannot be null.");
            }

            if (!Path.HasExtension(fileName))
            {
                fileName += ".xml";
            }

            T obj;

            using (var stream = File.OpenRead(fileName))
            {
                if (typeInherited == null)
                {
                    obj =  (T)new XmlSerializer(typeof(T)).Deserialize(stream);
                    //return (T)new XmlSerializer(typeof(T)).Deserialize(stream);
                }
                else
                {
                    Type[] types = new[] { typeInherited };
                    obj =  (T)new XmlSerializer(typeof(T), types).Deserialize(stream);
                    //return (T)new XmlSerializer(typeof(T), types).Deserialize(stream);
                }
            }

            return obj;
        }
    }
}