#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System.Resources;
using System.Globalization;
using System.Threading;
namespace PaceTestHarness.Localization
{
    class AppLocalization
    {
        private ResourceManager _ResourceManager;

        /// <summary>
        /// Constructor.
        /// </summary>
        public AppLocalization()
        {
            _ResourceManager = new ResourceManager("PaceTestHarness.Localization.TestHarness", 
                System.Reflection.Assembly.GetExecutingAssembly());
        }

        /// <summary>
        /// Public method to get Resource Manager.
        /// </summary>
        /// <returns>The PafApp global Resource Manager</returns>
        public ResourceManager GetResourceManager()
        {
            if (_ResourceManager == null)
            {
                _ResourceManager =
                    new ResourceManager("PaceTestHarness.Localization.TestHarness",
                    System.Reflection.Assembly.GetExecutingAssembly());
            }
            Thread.CurrentThread.CurrentUICulture = GetCurrentCulture();

            return _ResourceManager;
        }

        /// <summary>
        /// Gets the culture information.
        /// </summary>
        /// <returns>The current culture setting of the machine.</returns>
        public CultureInfo GetCurrentCulture()
        {
            return _ResourceManager.GetResourceSet(new CultureInfo(Properties.Settings.Default.Language), true, true) == null ? new CultureInfo("en-US") : new CultureInfo(Properties.Settings.Default.Language);
        }
    }
}
