﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using PaceTestHarness.Localization;

namespace PaceTestHarness
{
    static class Program
    {
        private static AppLocalization _localization;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        /// <summary>
        /// Get the Localization object.
        /// </summary>
        /// <returns>An AppLocalization  object.</returns>
        public static AppLocalization GetLocalization()
        {
            return _localization ?? (_localization = new AppLocalization());
        }

        /// <summary>
        /// Gets the file open name.
        /// </summary>
        /// <returns>A string value with the path and file name.</returns>
        public static string GetOpenFileName(bool includeBtth)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = GetLocalization().GetResourceManager().GetString(!includeBtth ? "Application.Controls.TestHarnessEditorFilter" : "Application.Controls.TestHarnessEditorFilter2");
            open.Multiselect = false;
            open.CheckFileExists = true;
            open.CheckPathExists = true;
            open.ShowHelp = false;
            open.SupportMultiDottedExtensions = true;
            open.ShowReadOnly = false;
            open.Title = "";
            open.ValidateNames = true;
            try
            {
                if (open.ShowDialog() == DialogResult.OK)
                {
                    return open.FileName;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Gets a save as file name using the windows save as dialog box.
        /// </summary>
        /// <param name="suggestion">A default file name to enter in the box.  Use an empty string or null to indicate no default value.</param>
        /// <returns>A string file name, or "" if the user canceled out.</returns>
        public static string GetSaveAsFileName(string suggestion)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.AddExtension = true;
            save.CheckFileExists = false;
            save.CheckPathExists = true;
            save.DefaultExt = ".tth";
            save.Filter = GetLocalization().GetResourceManager().GetString("Application.Controls.TestHarnessEditorFilter");
            save.ShowHelp = false;
            save.SupportMultiDottedExtensions = true;
            save.Title = "";
            save.ValidateNames = true;

            if (!String.IsNullOrEmpty(suggestion))
                save.FileName = suggestion;
            else
                save.FileName = "";

            try
            {
                if (save.ShowDialog() == DialogResult.OK)
                {
                    return save.FileName;
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return "";
            }
        }

        /// <summary>
        /// Sets the Titan assembly version.
        /// </summary>
        public static Version GetTitanAssembly()
        {
            foreach (AssemblyName nm in Assembly.GetExecutingAssembly().GetReferencedAssemblies())
            {
                if (nm.Name.Equals("Titan"))
                {
                    return nm.Version;
                }
            }
            return null;
        }
    }
}
