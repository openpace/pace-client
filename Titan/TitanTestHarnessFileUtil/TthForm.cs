﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;
using DevExpress.Spreadsheet;
using DevExpress.Utils;
using DevExpress.XtraEditors;
using DevExpress.XtraGrid;
using DevExpress.XtraGrid.Columns;
using DevExpress.XtraGrid.Views.Base;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraTab;
using PaceTestHarness.Classes;
using Titan.Pace;
using Titan.Pace.Application.Extensions;
using Titan.Pace.Application.Extensions.PafService;
using Titan.Pace.TestHarness.Binary;
using Titan.Palladium.DataStructures;
using Titan.Palladium.GridView;
using Titan.Palladium.TestHarness;
using Range = DevExpress.Spreadsheet.Range;

namespace PaceTestHarness
{
    public partial class TthForm : XtraForm
    {
        public delegate void ModifiedEventHandler(object sender, bool modified);
        public event ModifiedEventHandler ModifiedEvent;

        private BindingSource _bs;
        private BindingList<RecordedTest> _list;
        private readonly string[] _masterColsToHide;
        private readonly string[] _masterColsReadOnly;
        private readonly string[] _cellInfoColsToHide;
        private readonly string[] _cellInfoColsReadOnly;
        private readonly string[] _intersectionColsToHide;
        private readonly string[] _userSelectionsColsToHide;
        private readonly string[] _userSelectionsColsReadOnly;
        private string _docName;
        private bool _modified = false;
        private int _rowCount = 1;
        private int _colCount = 1;
        private int _testSeq = 0;

        #region Properties

        public string DocName
        {
            get { return _docName; }
            set
            {
                _docName = value;
                Text = DocName;
            }
        }

        public bool Modified
        {
            get { return _modified; }
            set
            {
                if (value != _modified)
                {
                    _modified = value;
                    ModifiedEvent(this, value);
                    Text = DocName + (Modified ? "*" : "");
                    if (MdiParent != null)
                    {
                        //((TthForm) MdiParent).UpdateText();
                    }
                }
            }
        }

        public bool ResultsModified { get; private set; }

        public TestHarness TestHarness { get; private set; } 

        public string TestFileName { get; private set; }

        #endregion Properties

        public TthForm(string fileName, string docName)
        {
            _masterColsToHide = new[] { "PafUserSelections", "RoleFilterSelections", "TestSequenceResults", "TestSequenceBeginningState" };
            _masterColsReadOnly = new[] { "RoleFilterEnabled", "InvalidIntersection", "RowsSuppressed", "ColumnsSuppressed", "IsDynamic", "RuleSetName", "PafUserSelections", "Parent", "SequenceNumber"};

            _cellInfoColsToHide = new[] { "ProtMngrListTracker"};
            _cellInfoColsReadOnly = new[] { "Sheet"};

            _intersectionColsToHide = new[] {"AxisSequence", "Coordinates", "UniqueID", "UniqueID2"};
            _userSelectionsColsToHide = new[] { "generationRightClick", "levelRightClick", "parentLast" };
            _userSelectionsColsReadOnly = new[] { "generationRightClick", "levelRightClick", "specification", "multiples", "parentLast", "pafAxis", "id" };

            InitializeComponent();

            DocName = docName;
            TestFileName = fileName;
            LoadFile();
        }

        private void LoadFile()
        {
            TestHarness = BinaryDeserializer.Deserialize(TestFileName);
            _list = new BindingList<RecordedTest>();
            foreach (RecordedTest rt in TestHarness)
            {
                _list.Add(rt);
            }

            _bs = new BindingSource();
            _bs.DataSource = _list;


            gridControl1.BindData(_bs);
            FormatProtectionMngrGrid(gridView1, _masterColsToHide, _masterColsReadOnly);
        }

        #region Events
        // ReSharper disable InconsistentNaming

        private void gridControl1_Click(object sender, EventArgs e)
        {
            int rowHandle = ((ColumnView)gridControl1.FocusedView).FocusedRowHandle;

            if (gridControl1.FocusedView.LevelName == "")
            {
                xtraTabControlProtection.Visible = false;
                xtraTabControlSequence.Visible = false;

                RecordedTest rt = gridControl1.FocusedView.GetRow(rowHandle) as RecordedTest;

                if (rt == null) return;

                if (rt.RoleFilterEnabled && rt.RoleFilterSelections != null)
                {
                    xtraTabControlSequence.Visible = true;
                    xtraTabPageResults.PageVisible = false;
                    xtraTabPageUserSelections.Text = "Role Filter Selections";
                    xtraTabPageUserSelections.PageVisible = true;
                    gridViewUserSelections.Columns.Clear();
                    gridControlUserSelection.BindData(rt.RoleFilterSelections);
                    FormatProtectionMngrGrid(gridViewUserSelections, null, null);

                    var expCol = gridViewUserSelections.Columns.ColumnByFieldName("expressionList");
                    if (expCol != null)
                    {
                        expCol.DisplayFormat.FormatType = FormatType.Custom;
                        expCol.DisplayFormat.FormatString = ",";
                        expCol.DisplayFormat.Format = new StringArrayCustomFormatter();
                    }

                }
            }
            else if (gridControl1.FocusedView.LevelName == "TestSequences")
            {
                xtraTabControlProtection.Visible = false;
                xtraTabControlSequence.Visible = true;

                TestSequence ovb = gridControl1.FocusedView.GetRow(rowHandle) as TestSequence;
                _testSeq = rowHandle;

                Worksheet worksheet = spreadsheetControl1.Document.Worksheets[0];

                if (ovb == null) return;

                object[,] oneBased = ovb.TestSequenceResults.ResultSet;

                object[,] data = new object[oneBased.GetUpperBound(0), oneBased.GetUpperBound(1)];

                for (int r = 1; r < oneBased.GetUpperBound(0); ++r)
                {
                    for (int c = 1; c < oneBased.GetUpperBound(1); ++c)
                    {
                        data[r - 1, c - 1] = oneBased[r, c];
                    }
                }
                try
                {
                    spreadsheetControl1.Document.BeginUpdate();
                    Range range = worksheet.Range.FromLTRB(0, 0, _colCount, _rowCount);
                    worksheet.Clear(range);
                    worksheet.Import(data, 0, 0);
                    _rowCount = data.GetUpperBound(0);
                    _colCount = data.GetUpperBound(1);
                }
                finally
                {
                    spreadsheetControl1.Document.EndUpdate();
                }

                gridViewUserSelections.Columns.Clear();
                gridControlUserSelection.BindData(ovb.PafUserSelections);
                FormatProtectionMngrGrid(gridViewUserSelections, _userSelectionsColsReadOnly, null);
                var expCol = gridViewUserSelections.Columns.ColumnByFieldName("values");
                if (expCol != null)
                {
                    expCol.DisplayFormat.FormatType = FormatType.Custom;
                    expCol.DisplayFormat.FormatString = ",";
                    expCol.DisplayFormat.Format = new StringArrayCustomFormatter();
                }
                var pafAxis = gridViewUserSelections.Columns.ColumnByFieldName("pafAxis");
                if (expCol != null)
                {
                    pafAxis.DisplayFormat.FormatType = FormatType.Custom;
                    pafAxis.DisplayFormat.Format = new PafAxisCustomFormatter();
                }


                xtraTabPageUserSelections.Text = "User Selections";
                xtraTabPageUserSelections.PageVisible = ovb.PafUserSelections != null && ovb.PafUserSelections.Any();
                xtraTabPageResults.PageVisible = true;

            }
            else if (gridControl1.FocusedView.LevelName == "ChangedCellInfoList")
            {
                xtraTabControlSequence.Visible = false;
                xtraTabControlProtection.Visible = true;
                ChangedCellInfo cci = gridControl1.FocusedView.GetRow(rowHandle) as ChangedCellInfo;

                if (cci == null) return;

                FormatProtMngrListTracker(gridControlProt, gridViewProtected, xtraTabPageProt,
                    cci.ProtMngrListTracker.ProtectedCells, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlChanges, gridViewChanges, xtraTabPageChanges,
                    cci.ProtMngrListTracker.Changes, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlUserLocks, gridViewUserLocks, xtraTabPageUserLock,
                    cci.ProtMngrListTracker.UserLocks, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlUnlockedChanges, gridViewUnlockedChanges, xtraTabPageUnlockedChanges,
                    cci.ProtMngrListTracker.UnLockedChanges, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlRACells, gridViewRa, xtraTabPageReplicateAll,
                    cci.ProtMngrListTracker.ReplicateAllCells, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlRECells, gridViewRe, xtraTabPageReplicateExisting,
                    cci.ProtMngrListTracker.ReplicateExistingCells, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlLACells, gridViewLa, xtraTabPageLiftAll,
                    cci.ProtMngrListTracker.LiftAllCells, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlLECells, gridViewLe, xtraTabPageLiftExisting,
                    cci.ProtMngrListTracker.LiftExistingCells, _intersectionColsToHide, null);

                FormatProtMngrListTracker(gridControlSessionLocks, gridViewSessionLocks, xtraTabPageSessionLocks,
                    cci.ProtMngrListTracker.SessionLockCells, _intersectionColsToHide, null);
            }

        }

        private void gridView1_MasterRowCollapsing(object sender, MasterRowCanExpandEventArgs e)
        {
            GridView gridView = sender as GridView;

            if (gridView == null) return;

            GridView detailView = (GridView)gridView.GetDetailView(e.RowHandle, e.RelationIndex);

            detailView.MasterRowExpanded -= gridView1_MasterRowExpanded;
            detailView.MasterRowCollapsing -= gridView1_MasterRowCollapsing;
            detailView.CustomRowCellEdit -= gridView1_CustomRowCellEdit;
        }

        private void gridView1_MasterRowExpanded(object sender, CustomMasterRowEventArgs e)
        {
            GridView gridView = sender as GridView;

            if (gridView == null) return;

            GridView detailView = gridView.GetDetailView(e.RowHandle, e.RelationIndex) as GridView;

            if (detailView == null) return;

            if (detailView.LevelName == "TestSequences")
            {
                FormatProtectionMngrGrid(detailView, _masterColsToHide, _masterColsReadOnly);
                detailView.MasterRowExpanded += gridView1_MasterRowExpanded;
                detailView.MasterRowCollapsing += gridView1_MasterRowCollapsing;
                detailView.CustomRowCellEdit += gridView1_CustomRowCellEdit;
            }
            else if (detailView.LevelName == "ChangedCellInfoList")
            {
                FormatProtectionMngrGrid(detailView, _cellInfoColsToHide, _cellInfoColsReadOnly);
                detailView.CustomRowCellEdit += gridView1_CustomRowCellEdit;
            }
        }

        private void gridViewUserSelections_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            GridView gv = sender as GridView;
            if (gv == null) return;
            if (e.Column.FieldName == "expressionList" || e.Column.FieldName == "values")
            {
                repositoryItemTextEdit.DisplayFormat.FormatType = FormatType.Custom;
                repositoryItemTextEdit.DisplayFormat.FormatString = ",";
                repositoryItemTextEdit.DisplayFormat.Format = new StringArrayCustomFormatter();
                repositoryItemTextEdit.EditFormat.FormatType = FormatType.Custom;
                repositoryItemTextEdit.EditFormat.FormatString = ",";
                repositoryItemTextEdit.EditFormat.Format = new StringArrayCustomFormatter();
                e.RepositoryItem = repositoryItemTextEdit;
            }
            else if (e.Column.FieldName == "pafAxis")
            {

                repositoryItemPafAxisEdit.DisplayFormat.FormatType = FormatType.Custom;
                repositoryItemPafAxisEdit.DisplayFormat.FormatString = ",";
                repositoryItemPafAxisEdit.DisplayFormat.Format = new PafAxisCustomFormatter();
                repositoryItemPafAxisEdit.EditFormat.FormatType = FormatType.Custom;
                repositoryItemPafAxisEdit.EditFormat.FormatString = ",";
                repositoryItemPafAxisEdit.EditFormat.Format = new PafAxisCustomFormatter();
                e.RepositoryItem = repositoryItemPafAxisEdit;
            }
        }

        private void gridView1_CustomRowCellEdit(object sender, CustomRowCellEditEventArgs e)
        {
            GridView gv = sender as GridView;
            if (gv == null) return;
            if (e.Column.FieldName == "ChangeType")
            {
                var values = Enum.GetValues(typeof(Change));
                List<string> orderValues = new List<string>(10);
                orderValues.AddRange(from object v in values select v.ToString());
                repositoryItemComboChangeType.Items.Clear();
                repositoryItemComboChangeType.Items.AddRange(orderValues.OrderBy(x => x).ToList());
                e.RepositoryItem = repositoryItemComboChangeType;
            }
        }

        private void repositoryItemTextEdit_ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            if (e == null || e.Value == null) return;
            if (e.Value.ToString().Contains(","))
            {
                e.Value = e.Value.ToString().Split(new [] {","}, StringSplitOptions.RemoveEmptyEntries);
                e.Handled = true;
                Modified = true;
            }
        }

        private void repositoryItemPafAxisEdit_ParseEditValue(object sender, DevExpress.XtraEditors.Controls.ConvertEditValueEventArgs e)
        {
            ViewAxis axis = e.Value.ToString().ParseEnum<ViewAxis>();
            e.Value = axis.ToPafAxis();
        }



        private void gridView1_ValidatingEditor(object sender, DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventArgs e)
        {
            Modified = true;
        }

        private void TthForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            bool cancel = !SaveQuestion();

            if (cancel)
            {
                e.Cancel = true;
                return;
            }
            Unload();
            UnbindGrid(gridControlProt);
            UnbindGrid(gridControlChanges);
            UnbindGrid(gridControlUserLocks);
            UnbindGrid(gridControlUnlockedChanges);
            UnbindGrid(gridControlRACells);
            UnbindGrid(gridControlRECells);
            UnbindGrid(gridControlLACells);
            UnbindGrid(gridControlLECells);
            UnbindGrid(gridControlSessionLocks);
            UnbindGrid(gridControlUserSelection);
            UnbindGrid(gridControl1);
        }



        private void spreadsheetControl1_CellValueChanged(object sender, DevExpress.XtraSpreadsheet.SpreadsheetCellEventArgs e)
        {

            TestSequence ts = TestHarness[0].TestSequences[_testSeq];

            ts.TestSequenceResults.ResultSet[e.RowIndex + 1, e.ColumnIndex + 1] =
                spreadsheetControl1.Document.Worksheets[e.SheetName].Cells[e.RowIndex, e.ColumnIndex].Value.ToObject();

            ResultsModified = true;
            Modified = true;
        }

        // ReSharper enable InconsistentNaming
        #endregion Events


        #region Private

        private void FormatProtMngrListTracker(GridControl gridControl, GridView gridView, XtraTabPage page, IEnumerable<Intersection> tracker, IEnumerable<string> colsToHide, IEnumerable<string> readOnlyCols)
        {
            gridControl.BindData(tracker);
            FormatProtectionMngrGrid(gridView, colsToHide, readOnlyCols);
            page.PageVisible = tracker != null && tracker.Any();
        }

        private void FormatProtectionMngrGrid(GridView gridView, IEnumerable<string> columnsToHide, IEnumerable<string> readOnlyColumns)
        {
            gridView.BeginUpdate();
            gridView.OptionsView.ShowGroupPanel = false;

            if (columnsToHide != null)
            {
                foreach (var c in columnsToHide)
                {
                    GridColumn column = gridView.Columns.ColumnByFieldName(c);
                    if (column == null) continue;
                    column.Visible = false;

                }
            }

            if (readOnlyColumns != null)
            {
                foreach (var c in readOnlyColumns)
                {
                    GridColumn column = gridView.Columns.ColumnByFieldName(c);
                    if (column == null) continue;
                    column.OptionsColumn.AllowEdit = false;

                }
            }
            gridView.EndUpdate();
        }

        private void Unload()
        {
            spreadsheetControl1 = null;
        }

        private void UnbindGrid(GridControl x)
        {
            x.BeginUpdate();
            x.DataSource = null;
            x.EndUpdate();
        }

        private bool SaveQuestion()
        {
            if (Modified)
            {
                switch (XtraMessageBox.Show("Do you want to save the changes you made to " + DocName + "?", "Pace Test Harness", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Cancel:
                        return false;
                    case DialogResult.Yes:
                        SaveAs();
                        break;
                }
            }
            return true;
        }

        #endregion Private

        #region Public
        public bool SaveAs()
        {
            try
            {
                string fileName = Program.GetSaveAsFileName("");
                if (!String.IsNullOrEmpty(fileName))
                {
                    TestFileName = fileName;
                    return Save();
                }
                return false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        public bool Save()
        {
            try
            {
                TestHarness th = TestHarness;
                th.Version = Program.GetTitanAssembly();
                BinarySerializer.Serialize(th, TestFileName);
                Modified = false;
                ResultsModified = false;
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }
        #endregion Public

       

    }
}
