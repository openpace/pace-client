﻿namespace PaceTestHarness
{
    partial class TthForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.xtraTabControl1 = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPage2 = new DevExpress.XtraTab.XtraTabPage();
            this.splitContainerControl2 = new DevExpress.XtraEditors.SplitContainerControl();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabControlSequence = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageResults = new DevExpress.XtraTab.XtraTabPage();
            this.spreadsheetControl1 = new DevExpress.XtraSpreadsheet.SpreadsheetControl();
            this.ribbonControl1 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.xtraTabPageUserSelections = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlUserSelection = new DevExpress.XtraGrid.GridControl();
            this.gridViewUserSelections = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemTextEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.repositoryItemPafAxisEdit = new DevExpress.XtraEditors.Repository.RepositoryItemTextEdit();
            this.xtraTabControlProtection = new DevExpress.XtraTab.XtraTabControl();
            this.xtraTabPageProt = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlProt = new DevExpress.XtraGrid.GridControl();
            this.gridViewProtected = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageUserLock = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlUserLocks = new DevExpress.XtraGrid.GridControl();
            this.gridViewUserLocks = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageChanges = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlChanges = new DevExpress.XtraGrid.GridControl();
            this.gridViewChanges = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageUnlockedChanges = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlUnlockedChanges = new DevExpress.XtraGrid.GridControl();
            this.gridViewUnlockedChanges = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageReplicateAll = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlRACells = new DevExpress.XtraGrid.GridControl();
            this.gridViewRa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageReplicateExisting = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlRECells = new DevExpress.XtraGrid.GridControl();
            this.gridViewRe = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageLiftAll = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlLACells = new DevExpress.XtraGrid.GridControl();
            this.gridViewLa = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageLiftExisting = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlLECells = new DevExpress.XtraGrid.GridControl();
            this.gridViewLe = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.xtraTabPageSessionLocks = new DevExpress.XtraTab.XtraTabPage();
            this.gridControlSessionLocks = new DevExpress.XtraGrid.GridControl();
            this.gridViewSessionLocks = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.repositoryItemComboChangeType = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).BeginInit();
            this.xtraTabControl1.SuspendLayout();
            this.xtraTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).BeginInit();
            this.splitContainerControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlSequence)).BeginInit();
            this.xtraTabControlSequence.SuspendLayout();
            this.xtraTabPageResults.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).BeginInit();
            this.xtraTabPageUserSelections.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUserSelection)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUserSelections)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPafAxisEdit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlProtection)).BeginInit();
            this.xtraTabControlProtection.SuspendLayout();
            this.xtraTabPageProt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProtected)).BeginInit();
            this.xtraTabPageUserLock.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUserLocks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUserLocks)).BeginInit();
            this.xtraTabPageChanges.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewChanges)).BeginInit();
            this.xtraTabPageUnlockedChanges.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnlockedChanges)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnlockedChanges)).BeginInit();
            this.xtraTabPageReplicateAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRACells)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRa)).BeginInit();
            this.xtraTabPageReplicateExisting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRECells)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRe)).BeginInit();
            this.xtraTabPageLiftAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLACells)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLa)).BeginInit();
            this.xtraTabPageLiftExisting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLECells)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLe)).BeginInit();
            this.xtraTabPageSessionLocks.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSessionLocks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSessionLocks)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboChangeType)).BeginInit();
            this.SuspendLayout();
            // 
            // xtraTabControl1
            // 
            this.xtraTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControl1.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControl1.Name = "xtraTabControl1";
            this.xtraTabControl1.SelectedTabPage = this.xtraTabPage2;
            this.xtraTabControl1.ShowTabHeader = DevExpress.Utils.DefaultBoolean.False;
            this.xtraTabControl1.Size = new System.Drawing.Size(1064, 681);
            this.xtraTabControl1.TabIndex = 0;
            this.xtraTabControl1.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPage2});
            // 
            // xtraTabPage2
            // 
            this.xtraTabPage2.Controls.Add(this.splitContainerControl2);
            this.xtraTabPage2.Name = "xtraTabPage2";
            this.xtraTabPage2.Size = new System.Drawing.Size(1058, 675);
            this.xtraTabPage2.Text = "Main";
            // 
            // splitContainerControl2
            // 
            this.splitContainerControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerControl2.Horizontal = false;
            this.splitContainerControl2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerControl2.Name = "splitContainerControl2";
            this.splitContainerControl2.Panel1.Controls.Add(this.gridControl1);
            this.splitContainerControl2.Panel1.Text = "Panel1";
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControlSequence);
            this.splitContainerControl2.Panel2.Controls.Add(this.xtraTabControlProtection);
            this.splitContainerControl2.Panel2.Text = "Panel2";
            this.splitContainerControl2.Size = new System.Drawing.Size(1058, 675);
            this.splitContainerControl2.SplitterPosition = 302;
            this.splitContainerControl2.TabIndex = 0;
            this.splitContainerControl2.Text = "splitContainerControl2";
            // 
            // gridControl1
            // 
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemComboChangeType});
            this.gridControl1.Size = new System.Drawing.Size(1058, 302);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            this.gridControl1.Click += new System.EventHandler(this.gridControl1_Click);
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsView.ShowGroupPanel = false;
            this.gridView1.OptionsView.WaitAnimationOptions = DevExpress.XtraEditors.WaitAnimationOptions.Indicator;
            this.gridView1.MasterRowExpanded += new DevExpress.XtraGrid.Views.Grid.CustomMasterRowEventHandler(this.gridView1_MasterRowExpanded);
            this.gridView1.MasterRowCollapsing += new DevExpress.XtraGrid.Views.Grid.MasterRowCanExpandEventHandler(this.gridView1_MasterRowCollapsing);
            this.gridView1.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridView1_CustomRowCellEdit);
            this.gridView1.ValidatingEditor += new DevExpress.XtraEditors.Controls.BaseContainerValidateEditorEventHandler(this.gridView1_ValidatingEditor);
            // 
            // xtraTabControlSequence
            // 
            this.xtraTabControlSequence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlSequence.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtraTabControlSequence.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlSequence.Name = "xtraTabControlSequence";
            this.xtraTabControlSequence.SelectedTabPage = this.xtraTabPageResults;
            this.xtraTabControlSequence.Size = new System.Drawing.Size(1058, 368);
            this.xtraTabControlSequence.TabIndex = 5;
            this.xtraTabControlSequence.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageResults,
            this.xtraTabPageUserSelections});
            this.xtraTabControlSequence.Visible = false;
            // 
            // xtraTabPageResults
            // 
            this.xtraTabPageResults.Controls.Add(this.spreadsheetControl1);
            this.xtraTabPageResults.Name = "xtraTabPageResults";
            this.xtraTabPageResults.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageResults.Text = "Results";
            // 
            // spreadsheetControl1
            // 
            this.spreadsheetControl1.AllowDrop = true;
            this.spreadsheetControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.spreadsheetControl1.Location = new System.Drawing.Point(0, 0);
            this.spreadsheetControl1.MenuManager = this.ribbonControl1;
            this.spreadsheetControl1.Name = "spreadsheetControl1";
            this.spreadsheetControl1.Options.Export.Csv.Culture = new System.Globalization.CultureInfo("");
            this.spreadsheetControl1.Options.Export.Txt.Culture = new System.Globalization.CultureInfo("");
            this.spreadsheetControl1.Options.Export.Txt.ValueSeparator = ',';
            this.spreadsheetControl1.Options.Import.Csv.Culture = new System.Globalization.CultureInfo("");
            this.spreadsheetControl1.Options.Import.ThrowExceptionOnInvalidDocument = false;
            this.spreadsheetControl1.Options.Import.Txt.Culture = new System.Globalization.CultureInfo("");
            this.spreadsheetControl1.Size = new System.Drawing.Size(1052, 340);
            this.spreadsheetControl1.TabIndex = 4;
            this.spreadsheetControl1.Text = "spreadsheetControl1";
            this.spreadsheetControl1.CellValueChanged += new DevExpress.XtraSpreadsheet.CellValueChangedEventHandler(this.spreadsheetControl1_CellValueChanged);
            // 
            // ribbonControl1
            // 
            this.ribbonControl1.ExpandCollapseItem.Id = 0;
            this.ribbonControl1.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl1.ExpandCollapseItem});
            this.ribbonControl1.Location = new System.Drawing.Point(1, 1);
            this.ribbonControl1.MaxItemId = 1;
            this.ribbonControl1.Name = "ribbonControl1";
            this.ribbonControl1.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPage1});
            this.ribbonControl1.Size = new System.Drawing.Size(1052, 142);
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // xtraTabPageUserSelections
            // 
            this.xtraTabPageUserSelections.Controls.Add(this.gridControlUserSelection);
            this.xtraTabPageUserSelections.Name = "xtraTabPageUserSelections";
            this.xtraTabPageUserSelections.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageUserSelections.Text = "User Selections";
            // 
            // gridControlUserSelection
            // 
            this.gridControlUserSelection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlUserSelection.Location = new System.Drawing.Point(0, 0);
            this.gridControlUserSelection.MainView = this.gridViewUserSelections;
            this.gridControlUserSelection.Name = "gridControlUserSelection";
            this.gridControlUserSelection.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemTextEdit,
            this.repositoryItemPafAxisEdit});
            this.gridControlUserSelection.Size = new System.Drawing.Size(1052, 340);
            this.gridControlUserSelection.TabIndex = 0;
            this.gridControlUserSelection.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUserSelections});
            // 
            // gridViewUserSelections
            // 
            this.gridViewUserSelections.GridControl = this.gridControlUserSelection;
            this.gridViewUserSelections.Name = "gridViewUserSelections";
            this.gridViewUserSelections.CustomRowCellEdit += new DevExpress.XtraGrid.Views.Grid.CustomRowCellEditEventHandler(this.gridViewUserSelections_CustomRowCellEdit);
            // 
            // repositoryItemTextEdit
            // 
            this.repositoryItemTextEdit.AutoHeight = false;
            this.repositoryItemTextEdit.Name = "repositoryItemTextEdit";
            this.repositoryItemTextEdit.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemTextEdit_ParseEditValue);
            // 
            // repositoryItemPafAxisEdit
            // 
            this.repositoryItemPafAxisEdit.AutoHeight = false;
            this.repositoryItemPafAxisEdit.Name = "repositoryItemPafAxisEdit";
            this.repositoryItemPafAxisEdit.ParseEditValue += new DevExpress.XtraEditors.Controls.ConvertEditValueEventHandler(this.repositoryItemPafAxisEdit_ParseEditValue);
            // 
            // xtraTabControlProtection
            // 
            this.xtraTabControlProtection.Dock = System.Windows.Forms.DockStyle.Fill;
            this.xtraTabControlProtection.HeaderLocation = DevExpress.XtraTab.TabHeaderLocation.Bottom;
            this.xtraTabControlProtection.Location = new System.Drawing.Point(0, 0);
            this.xtraTabControlProtection.Name = "xtraTabControlProtection";
            this.xtraTabControlProtection.SelectedTabPage = this.xtraTabPageProt;
            this.xtraTabControlProtection.Size = new System.Drawing.Size(1058, 368);
            this.xtraTabControlProtection.TabIndex = 3;
            this.xtraTabControlProtection.TabPages.AddRange(new DevExpress.XtraTab.XtraTabPage[] {
            this.xtraTabPageProt,
            this.xtraTabPageUserLock,
            this.xtraTabPageChanges,
            this.xtraTabPageUnlockedChanges,
            this.xtraTabPageReplicateAll,
            this.xtraTabPageReplicateExisting,
            this.xtraTabPageLiftAll,
            this.xtraTabPageLiftExisting,
            this.xtraTabPageSessionLocks});
            this.xtraTabControlProtection.Visible = false;
            // 
            // xtraTabPageProt
            // 
            this.xtraTabPageProt.Controls.Add(this.gridControlProt);
            this.xtraTabPageProt.Name = "xtraTabPageProt";
            this.xtraTabPageProt.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageProt.Text = "Protected";
            // 
            // gridControlProt
            // 
            this.gridControlProt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlProt.Location = new System.Drawing.Point(0, 0);
            this.gridControlProt.MainView = this.gridViewProtected;
            this.gridControlProt.Name = "gridControlProt";
            this.gridControlProt.Size = new System.Drawing.Size(1052, 340);
            this.gridControlProt.TabIndex = 0;
            this.gridControlProt.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewProtected});
            // 
            // gridViewProtected
            // 
            this.gridViewProtected.GridControl = this.gridControlProt;
            this.gridViewProtected.Name = "gridViewProtected";
            // 
            // xtraTabPageUserLock
            // 
            this.xtraTabPageUserLock.Controls.Add(this.gridControlUserLocks);
            this.xtraTabPageUserLock.Name = "xtraTabPageUserLock";
            this.xtraTabPageUserLock.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageUserLock.Text = "User Locks";
            // 
            // gridControlUserLocks
            // 
            this.gridControlUserLocks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlUserLocks.Location = new System.Drawing.Point(0, 0);
            this.gridControlUserLocks.MainView = this.gridViewUserLocks;
            this.gridControlUserLocks.Name = "gridControlUserLocks";
            this.gridControlUserLocks.Size = new System.Drawing.Size(1052, 340);
            this.gridControlUserLocks.TabIndex = 0;
            this.gridControlUserLocks.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUserLocks});
            // 
            // gridViewUserLocks
            // 
            this.gridViewUserLocks.GridControl = this.gridControlUserLocks;
            this.gridViewUserLocks.Name = "gridViewUserLocks";
            // 
            // xtraTabPageChanges
            // 
            this.xtraTabPageChanges.Controls.Add(this.gridControlChanges);
            this.xtraTabPageChanges.Name = "xtraTabPageChanges";
            this.xtraTabPageChanges.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageChanges.Text = "Changes";
            // 
            // gridControlChanges
            // 
            this.gridControlChanges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlChanges.Location = new System.Drawing.Point(0, 0);
            this.gridControlChanges.MainView = this.gridViewChanges;
            this.gridControlChanges.Name = "gridControlChanges";
            this.gridControlChanges.Size = new System.Drawing.Size(1052, 340);
            this.gridControlChanges.TabIndex = 0;
            this.gridControlChanges.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewChanges});
            // 
            // gridViewChanges
            // 
            this.gridViewChanges.GridControl = this.gridControlChanges;
            this.gridViewChanges.Name = "gridViewChanges";
            // 
            // xtraTabPageUnlockedChanges
            // 
            this.xtraTabPageUnlockedChanges.Controls.Add(this.gridControlUnlockedChanges);
            this.xtraTabPageUnlockedChanges.Name = "xtraTabPageUnlockedChanges";
            this.xtraTabPageUnlockedChanges.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageUnlockedChanges.Text = "Unlocked Changes";
            // 
            // gridControlUnlockedChanges
            // 
            this.gridControlUnlockedChanges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlUnlockedChanges.Location = new System.Drawing.Point(0, 0);
            this.gridControlUnlockedChanges.MainView = this.gridViewUnlockedChanges;
            this.gridControlUnlockedChanges.Name = "gridControlUnlockedChanges";
            this.gridControlUnlockedChanges.Size = new System.Drawing.Size(1052, 340);
            this.gridControlUnlockedChanges.TabIndex = 0;
            this.gridControlUnlockedChanges.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewUnlockedChanges});
            // 
            // gridViewUnlockedChanges
            // 
            this.gridViewUnlockedChanges.GridControl = this.gridControlUnlockedChanges;
            this.gridViewUnlockedChanges.Name = "gridViewUnlockedChanges";
            // 
            // xtraTabPageReplicateAll
            // 
            this.xtraTabPageReplicateAll.Controls.Add(this.gridControlRACells);
            this.xtraTabPageReplicateAll.Name = "xtraTabPageReplicateAll";
            this.xtraTabPageReplicateAll.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageReplicateAll.Text = "RA Cells";
            // 
            // gridControlRACells
            // 
            this.gridControlRACells.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRACells.Location = new System.Drawing.Point(0, 0);
            this.gridControlRACells.MainView = this.gridViewRa;
            this.gridControlRACells.Name = "gridControlRACells";
            this.gridControlRACells.Size = new System.Drawing.Size(1052, 340);
            this.gridControlRACells.TabIndex = 0;
            this.gridControlRACells.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRa});
            // 
            // gridViewRa
            // 
            this.gridViewRa.GridControl = this.gridControlRACells;
            this.gridViewRa.Name = "gridViewRa";
            // 
            // xtraTabPageReplicateExisting
            // 
            this.xtraTabPageReplicateExisting.Controls.Add(this.gridControlRECells);
            this.xtraTabPageReplicateExisting.Name = "xtraTabPageReplicateExisting";
            this.xtraTabPageReplicateExisting.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageReplicateExisting.Text = "RE Cells";
            // 
            // gridControlRECells
            // 
            this.gridControlRECells.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlRECells.Location = new System.Drawing.Point(0, 0);
            this.gridControlRECells.MainView = this.gridViewRe;
            this.gridControlRECells.Name = "gridControlRECells";
            this.gridControlRECells.Size = new System.Drawing.Size(1052, 340);
            this.gridControlRECells.TabIndex = 0;
            this.gridControlRECells.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewRe});
            // 
            // gridViewRe
            // 
            this.gridViewRe.GridControl = this.gridControlRECells;
            this.gridViewRe.Name = "gridViewRe";
            // 
            // xtraTabPageLiftAll
            // 
            this.xtraTabPageLiftAll.Controls.Add(this.gridControlLACells);
            this.xtraTabPageLiftAll.Name = "xtraTabPageLiftAll";
            this.xtraTabPageLiftAll.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageLiftAll.Text = "LA Cells";
            // 
            // gridControlLACells
            // 
            this.gridControlLACells.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLACells.Location = new System.Drawing.Point(0, 0);
            this.gridControlLACells.MainView = this.gridViewLa;
            this.gridControlLACells.Name = "gridControlLACells";
            this.gridControlLACells.Size = new System.Drawing.Size(1052, 340);
            this.gridControlLACells.TabIndex = 0;
            this.gridControlLACells.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLa});
            // 
            // gridViewLa
            // 
            this.gridViewLa.GridControl = this.gridControlLACells;
            this.gridViewLa.Name = "gridViewLa";
            // 
            // xtraTabPageLiftExisting
            // 
            this.xtraTabPageLiftExisting.Controls.Add(this.gridControlLECells);
            this.xtraTabPageLiftExisting.Name = "xtraTabPageLiftExisting";
            this.xtraTabPageLiftExisting.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageLiftExisting.Text = "LE Cells";
            // 
            // gridControlLECells
            // 
            this.gridControlLECells.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlLECells.Location = new System.Drawing.Point(0, 0);
            this.gridControlLECells.MainView = this.gridViewLe;
            this.gridControlLECells.Name = "gridControlLECells";
            this.gridControlLECells.Size = new System.Drawing.Size(1052, 340);
            this.gridControlLECells.TabIndex = 0;
            this.gridControlLECells.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewLe});
            // 
            // gridViewLe
            // 
            this.gridViewLe.GridControl = this.gridControlLECells;
            this.gridViewLe.Name = "gridViewLe";
            // 
            // xtraTabPageSessionLocks
            // 
            this.xtraTabPageSessionLocks.Controls.Add(this.gridControlSessionLocks);
            this.xtraTabPageSessionLocks.Name = "xtraTabPageSessionLocks";
            this.xtraTabPageSessionLocks.Size = new System.Drawing.Size(1052, 340);
            this.xtraTabPageSessionLocks.Text = "Session Locks";
            // 
            // gridControlSessionLocks
            // 
            this.gridControlSessionLocks.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControlSessionLocks.Location = new System.Drawing.Point(0, 0);
            this.gridControlSessionLocks.MainView = this.gridViewSessionLocks;
            this.gridControlSessionLocks.Name = "gridControlSessionLocks";
            this.gridControlSessionLocks.Size = new System.Drawing.Size(1052, 340);
            this.gridControlSessionLocks.TabIndex = 0;
            this.gridControlSessionLocks.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridViewSessionLocks});
            // 
            // gridViewSessionLocks
            // 
            this.gridViewSessionLocks.GridControl = this.gridControlSessionLocks;
            this.gridViewSessionLocks.Name = "gridViewSessionLocks";
            // 
            // repositoryItemComboChangeType
            // 
            this.repositoryItemComboChangeType.AutoHeight = false;
            this.repositoryItemComboChangeType.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboChangeType.Name = "repositoryItemComboChangeType";
            // 
            // TthForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.xtraTabControl1);
            this.Name = "TthForm";
            this.Text = "Pace Test Harness Utility";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.TthForm_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControl1)).EndInit();
            this.xtraTabControl1.ResumeLayout(false);
            this.xtraTabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerControl2)).EndInit();
            this.splitContainerControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlSequence)).EndInit();
            this.xtraTabControlSequence.ResumeLayout(false);
            this.xtraTabPageResults.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl1)).EndInit();
            this.xtraTabPageUserSelections.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUserSelection)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUserSelections)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemTextEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemPafAxisEdit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabControlProtection)).EndInit();
            this.xtraTabControlProtection.ResumeLayout(false);
            this.xtraTabPageProt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlProt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewProtected)).EndInit();
            this.xtraTabPageUserLock.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUserLocks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUserLocks)).EndInit();
            this.xtraTabPageChanges.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewChanges)).EndInit();
            this.xtraTabPageUnlockedChanges.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlUnlockedChanges)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewUnlockedChanges)).EndInit();
            this.xtraTabPageReplicateAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRACells)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRa)).EndInit();
            this.xtraTabPageReplicateExisting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlRECells)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewRe)).EndInit();
            this.xtraTabPageLiftAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLACells)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLa)).EndInit();
            this.xtraTabPageLiftExisting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlLECells)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewLe)).EndInit();
            this.xtraTabPageSessionLocks.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridControlSessionLocks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridViewSessionLocks)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboChangeType)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraTab.XtraTabControl xtraTabControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPage2;
        private DevExpress.XtraEditors.SplitContainerControl splitContainerControl2;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlProtection;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageProt;
        private DevExpress.XtraGrid.GridControl gridControlProt;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewProtected;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageUserLock;
        private DevExpress.XtraGrid.GridControl gridControlUserLocks;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUserLocks;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageChanges;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageUnlockedChanges;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageReplicateAll;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageReplicateExisting;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLiftAll;
        private DevExpress.XtraSpreadsheet.SpreadsheetControl spreadsheetControl1;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageLiftExisting;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageSessionLocks;
        private DevExpress.XtraGrid.GridControl gridControlChanges;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewChanges;
        private DevExpress.XtraGrid.GridControl gridControlUnlockedChanges;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUnlockedChanges;
        private DevExpress.XtraGrid.GridControl gridControlRACells;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRa;
        private DevExpress.XtraGrid.GridControl gridControlRECells;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewRe;
        private DevExpress.XtraGrid.GridControl gridControlLACells;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLa;
        private DevExpress.XtraGrid.GridControl gridControlLECells;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewLe;
        private DevExpress.XtraGrid.GridControl gridControlSessionLocks;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewSessionLocks;
        private DevExpress.XtraTab.XtraTabControl xtraTabControlSequence;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageResults;
        private DevExpress.XtraTab.XtraTabPage xtraTabPageUserSelections;
        private DevExpress.XtraGrid.GridControl gridControlUserSelection;
        private DevExpress.XtraGrid.Views.Grid.GridView gridViewUserSelections;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl1;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemTextEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemTextEdit repositoryItemPafAxisEdit;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboChangeType;
    }
}

