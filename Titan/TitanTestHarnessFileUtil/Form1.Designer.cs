﻿namespace PaceTestHarness
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.ribbonPage1 = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroup1 = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.backstageViewControl1 = new DevExpress.XtraBars.Ribbon.BackstageViewControl();
            this.backstageViewButtonItemExit = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.ribbonControl2 = new DevExpress.XtraBars.Ribbon.RibbonControl();
            this.imageCollection2 = new DevExpress.Utils.ImageCollection(this.components);
            this.barButtonItemOpen = new DevExpress.XtraBars.BarButtonItem();
            this.barSubItemSave = new DevExpress.XtraBars.BarSubItem();
            this.barButtonItem1 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItem2 = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemClose = new DevExpress.XtraBars.BarButtonItem();
            this.barButtonItemExit = new DevExpress.XtraBars.BarButtonItem();
            this.ribbonGalleryBarItemSkins = new DevExpress.XtraBars.RibbonGalleryBarItem();
            this.imageCollection1 = new DevExpress.Utils.ImageCollection(this.components);
            this.ribbonPageHome = new DevExpress.XtraBars.Ribbon.RibbonPage();
            this.ribbonPageGroupFile = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupExit = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.ribbonPageGroupSkin = new DevExpress.XtraBars.Ribbon.RibbonPageGroup();
            this.xtraTabbedMdiManager1 = new DevExpress.XtraTabbedMdi.XtraTabbedMdiManager(this.components);
            this.bvItemExit = new DevExpress.XtraBars.Ribbon.BackstageViewButtonItem();
            this.applicationMenu1 = new DevExpress.XtraBars.Ribbon.ApplicationMenu(this.components);
            this.directoryEntry1 = new System.DirectoryServices.DirectoryEntry();
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).BeginInit();
            this.SuspendLayout();
            // 
            // ribbonPage1
            // 
            this.ribbonPage1.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroup1});
            this.ribbonPage1.Name = "ribbonPage1";
            this.ribbonPage1.Text = "ribbonPage1";
            // 
            // ribbonPageGroup1
            // 
            this.ribbonPageGroup1.Name = "ribbonPageGroup1";
            this.ribbonPageGroup1.Text = "ribbonPageGroup1";
            // 
            // backstageViewControl1
            // 
            this.backstageViewControl1.ColorScheme = DevExpress.XtraBars.Ribbon.RibbonControlColorScheme.Yellow;
            this.backstageViewControl1.Items.Add(this.backstageViewButtonItemExit);
            this.backstageViewControl1.LeftPaneMaxWidth = 135;
            this.backstageViewControl1.Location = new System.Drawing.Point(12, 155);
            this.backstageViewControl1.Name = "backstageViewControl1";
            this.backstageViewControl1.Office2013StyleOptions.RightPaneContentVerticalOffset = 70;
            this.backstageViewControl1.Ribbon = this.ribbonControl2;
            this.backstageViewControl1.SelectedTab = null;
            this.backstageViewControl1.Size = new System.Drawing.Size(555, 415);
            this.backstageViewControl1.TabIndex = 9;
            this.backstageViewControl1.Text = "backstageViewControl1";
            // 
            // backstageViewButtonItemExit
            // 
            this.backstageViewButtonItemExit.Caption = "Exit";
            this.backstageViewButtonItemExit.Glyph = ((System.Drawing.Image)(resources.GetObject("backstageViewButtonItemExit.Glyph")));
            this.backstageViewButtonItemExit.Name = "backstageViewButtonItemExit";
            this.backstageViewButtonItemExit.ItemClick += new DevExpress.XtraBars.Ribbon.BackstageViewItemEventHandler(this.backstageViewButtonItemExit_ItemClick);
            // 
            // ribbonControl2
            // 
            this.ribbonControl2.ApplicationButtonDropDownControl = this.backstageViewControl1;
            this.ribbonControl2.ExpandCollapseItem.Id = 0;
            this.ribbonControl2.Images = this.imageCollection2;
            this.ribbonControl2.Items.AddRange(new DevExpress.XtraBars.BarItem[] {
            this.ribbonControl2.ExpandCollapseItem,
            this.barButtonItemOpen,
            this.barSubItemSave,
            this.barButtonItem1,
            this.barButtonItem2,
            this.barButtonItemClose,
            this.barButtonItemExit,
            this.ribbonGalleryBarItemSkins});
            this.ribbonControl2.LargeImages = this.imageCollection1;
            this.ribbonControl2.Location = new System.Drawing.Point(0, 0);
            this.ribbonControl2.MaxItemId = 11;
            this.ribbonControl2.Name = "ribbonControl2";
            this.ribbonControl2.Pages.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPage[] {
            this.ribbonPageHome});
            this.ribbonControl2.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013;
            this.ribbonControl2.Size = new System.Drawing.Size(1064, 144);
            // 
            // imageCollection2
            // 
            this.imageCollection2.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection2.ImageStream")));
            this.imageCollection2.Images.SetKeyName(29, "SaveAs_16x16.png");
            // 
            // barButtonItemOpen
            // 
            this.barButtonItemOpen.Caption = "Open...";
            this.barButtonItemOpen.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpen.Glyph")));
            this.barButtonItemOpen.Id = 1;
            this.barButtonItemOpen.ImageIndex = 7;
            this.barButtonItemOpen.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemOpen.LargeGlyph")));
            this.barButtonItemOpen.Name = "barButtonItemOpen";
            this.barButtonItemOpen.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonItemStyles.SmallWithText;
            this.barButtonItemOpen.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemOpen_ItemClick);
            // 
            // barSubItemSave
            // 
            this.barSubItemSave.Caption = "Save";
            this.barSubItemSave.Enabled = false;
            this.barSubItemSave.Id = 5;
            this.barSubItemSave.LargeImageIndex = 2;
            this.barSubItemSave.LinksPersistInfo.AddRange(new DevExpress.XtraBars.LinkPersistInfo[] {
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem1),
            new DevExpress.XtraBars.LinkPersistInfo(this.barButtonItem2)});
            this.barSubItemSave.Name = "barSubItemSave";
            // 
            // barButtonItem1
            // 
            this.barButtonItem1.Caption = "Save";
            this.barButtonItem1.Id = 6;
            this.barButtonItem1.ImageIndex = 10;
            this.barButtonItem1.Name = "barButtonItem1";
            this.barButtonItem1.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem1_ItemClick);
            // 
            // barButtonItem2
            // 
            this.barButtonItem2.Caption = "Save As...";
            this.barButtonItem2.Id = 7;
            this.barButtonItem2.ImageIndex = 21;
            this.barButtonItem2.Name = "barButtonItem2";
            this.barButtonItem2.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItem2_ItemClick);
            // 
            // barButtonItemClose
            // 
            this.barButtonItemClose.Caption = "Close";
            this.barButtonItemClose.Id = 8;
            this.barButtonItemClose.ImageIndex = 12;
            this.barButtonItemClose.Name = "barButtonItemClose";
            this.barButtonItemClose.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemClose_ItemClick);
            // 
            // barButtonItemExit
            // 
            this.barButtonItemExit.Caption = "Exit";
            this.barButtonItemExit.Glyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExit.Glyph")));
            this.barButtonItemExit.Id = 9;
            this.barButtonItemExit.LargeGlyph = ((System.Drawing.Image)(resources.GetObject("barButtonItemExit.LargeGlyph")));
            this.barButtonItemExit.Name = "barButtonItemExit";
            this.barButtonItemExit.ItemClick += new DevExpress.XtraBars.ItemClickEventHandler(this.barButtonItemExit_ItemClick);
            // 
            // ribbonGalleryBarItemSkins
            // 
            this.ribbonGalleryBarItemSkins.Caption = "ribbonGalleryBarItem1";
            // 
            // 
            // 
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseFont = true;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.Options.UseTextOptions = true;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Hovered.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseFont = true;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.Options.UseTextOptions = true;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Normal.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseFont = true;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.Options.UseTextOptions = true;
            this.ribbonGalleryBarItemSkins.Gallery.Appearance.ItemCaptionAppearance.Pressed.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.ribbonGalleryBarItemSkins.Id = 10;
            this.ribbonGalleryBarItemSkins.Name = "ribbonGalleryBarItemSkins";
            // 
            // imageCollection1
            // 
            this.imageCollection1.ImageSize = new System.Drawing.Size(32, 32);
            this.imageCollection1.ImageStream = ((DevExpress.Utils.ImageCollectionStreamer)(resources.GetObject("imageCollection1.ImageStream")));
            // 
            // ribbonPageHome
            // 
            this.ribbonPageHome.Groups.AddRange(new DevExpress.XtraBars.Ribbon.RibbonPageGroup[] {
            this.ribbonPageGroupFile,
            this.ribbonPageGroupExit,
            this.ribbonPageGroupSkin});
            this.ribbonPageHome.Name = "ribbonPageHome";
            this.ribbonPageHome.Text = "Home";
            // 
            // ribbonPageGroupFile
            // 
            this.ribbonPageGroupFile.ItemLinks.Add(this.barButtonItemOpen);
            this.ribbonPageGroupFile.ItemLinks.Add(this.barButtonItemClose);
            this.ribbonPageGroupFile.ItemLinks.Add(this.barSubItemSave, true);
            this.ribbonPageGroupFile.Name = "ribbonPageGroupFile";
            this.ribbonPageGroupFile.Text = "File";
            // 
            // ribbonPageGroupExit
            // 
            this.ribbonPageGroupExit.ItemLinks.Add(this.barButtonItemExit);
            this.ribbonPageGroupExit.Name = "ribbonPageGroupExit";
            this.ribbonPageGroupExit.Text = "Exit";
            // 
            // ribbonPageGroupSkin
            // 
            this.ribbonPageGroupSkin.ItemLinks.Add(this.ribbonGalleryBarItemSkins);
            this.ribbonPageGroupSkin.Name = "ribbonPageGroupSkin";
            this.ribbonPageGroupSkin.Text = "Skin";
            // 
            // xtraTabbedMdiManager1
            // 
            this.xtraTabbedMdiManager1.MdiParent = this;
            this.xtraTabbedMdiManager1.FloatMDIChildActivated += new System.EventHandler(this.xtraTabbedMdiManager1_FloatMDIChildActivated);
            this.xtraTabbedMdiManager1.FloatMDIChildDeactivated += new System.EventHandler(this.xtraTabbedMdiManager1_FloatMDIChildDeactivated);
            // 
            // bvItemExit
            // 
            this.bvItemExit.Caption = "Exit";
            this.bvItemExit.ImageIndex = 22;
            this.bvItemExit.Name = "bvItemExit";
            // 
            // applicationMenu1
            // 
            this.applicationMenu1.Name = "applicationMenu1";
            this.applicationMenu1.Ribbon = this.ribbonControl2;
            // 
            // Form1
            // 
            this.AllowDrop = true;
            this.AllowFormGlass = DevExpress.Utils.DefaultBoolean.False;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1064, 681);
            this.Controls.Add(this.backstageViewControl1);
            this.Controls.Add(this.ribbonControl2);
            this.IsMdiContainer = true;
            this.Name = "Form1";
            this.Ribbon = this.ribbonControl2;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Pace Test Harness Utility";
            this.MdiChildActivate += new System.EventHandler(this.Form1_MdiChildActivate);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.Form1_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.Form1_DragEnter);
            ((System.ComponentModel.ISupportInitialize)(this.ribbonControl2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imageCollection1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.xtraTabbedMdiManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.applicationMenu1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPage1;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroup1;
        private DevExpress.XtraTabbedMdi.XtraTabbedMdiManager xtraTabbedMdiManager1;
        private DevExpress.XtraBars.Ribbon.BackstageViewControl backstageViewControl1;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem backstageViewButtonItemExit;
        private DevExpress.XtraBars.Ribbon.BackstageViewButtonItem bvItemExit;
        private DevExpress.XtraBars.Ribbon.ApplicationMenu applicationMenu1;
        private System.DirectoryServices.DirectoryEntry directoryEntry1;
        private DevExpress.XtraBars.Ribbon.RibbonControl ribbonControl2;
        private DevExpress.XtraBars.BarButtonItem barButtonItemOpen;
        private DevExpress.XtraBars.BarSubItem barSubItemSave;
        private DevExpress.XtraBars.Ribbon.RibbonPage ribbonPageHome;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupFile;
        private DevExpress.Utils.ImageCollection imageCollection2;
        private DevExpress.Utils.ImageCollection imageCollection1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem1;
        private DevExpress.XtraBars.BarButtonItem barButtonItem2;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupExit;
        private DevExpress.XtraBars.BarButtonItem barButtonItemClose;
        private DevExpress.XtraBars.BarButtonItem barButtonItemExit;
        private DevExpress.XtraBars.Ribbon.RibbonPageGroup ribbonPageGroupSkin;
        private DevExpress.XtraBars.RibbonGalleryBarItem ribbonGalleryBarItemSkins;
    }
}

