﻿#region copyright
// Copyright (c) 2017-2019 Contributors to Open Pace and others.
//
// The OPEN PACE product suite is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or (at your 
// option) any later version.
//
// This software is distributed in the hope that it will be useful, but WITHOUT 
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY 
// or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public 
// License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this  software. If not, see <http://www.gnu.org/licenses/>.
#endregion
using System;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DevExpress.LookAndFeel;
using DevExpress.XtraBars.Ribbon;
using PaceTestHarness.Properties;

namespace PaceTestHarness
{
    public partial class Form1 : RibbonForm
    {

        TthForm CurrentForm
        {
            get
            {
                if (this.ActiveMdiChild == null) return null;
                {
                    if (xtraTabbedMdiManager1.ActiveFloatForm != null)
                    {
                        return xtraTabbedMdiManager1.ActiveFloatForm as TthForm;
                    }
                }
                return this.ActiveMdiChild as TthForm;
            }
        }

        public Form1()
        {
            DevExpress.UserSkins.BonusSkins.Register();
            UserLookAndFeel.Default.UseDefaultLookAndFeel = true;
            UserLookAndFeel.Default.ParentLookAndFeel = null;
            UserLookAndFeel.Default.SetSkinStyle("Office 2013 Dark Gray");
            //UserLookAndFeel.Default.SkinName = "Visual Studio 2010";
            //UserLookAndFeel.Default.SkinName = "Metropolis";
            //UserLookAndFeel.Default.SkinName = "Office 2010 Silver";
            //UserLookAndFeel.Default.SkinName = "Metropolis";
            //UserLookAndFeel.Default.SkinName = "Seven Classic";
            //UserLookAndFeel.Default.SkinName = "Valentine";
            UserLookAndFeel.Default.Style = LookAndFeelStyle.Skin;
            UserLookAndFeel.Default.UseWindowsXPTheme = false;
            Icon = Resources.PIcon;
            
            InitializeComponent();
            InitSkinGallery();
        }

        private void barButtonItemOpen_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {

            string fileName = Program.GetOpenFileName(false);
            if (!String.IsNullOrEmpty(fileName))
            {
                OpenFile(fileName);
            }


           
        }

        private void OpenFile(string fileName)
        {
            if (!String.IsNullOrEmpty(fileName))
            {
                FileInfo file = new FileInfo(fileName);
                TthForm pad = new TthForm(fileName, file.Name);
                //if (fileName != null)
                //    pad.LoadDocument(fileName);
                //else
                //    pad.DocName = DocumentName;
                pad.ModifiedEvent += pad_ModifiedEvent;
                pad.MdiParent = this;
                pad.Show();
            }
        }

        private void pad_ModifiedEvent(object sender, bool modified)
        {
            barSubItemSave.Enabled = modified;
        }

        private void InitSkinGallery()
        {
            DevExpress.XtraBars.Helpers.SkinHelper.InitSkinGallery(ribbonGalleryBarItemSkins, true);
        }

        void ChangeActiveForm()
        {
            if (CurrentForm == null) return;
            pad_ModifiedEvent(this, CurrentForm.Modified);
        }

        private void barButtonItemClose_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            try
            {
                if (CurrentForm != null) CurrentForm.Hide();

                string s = "";
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }

        private void barButtonItemExit_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            Close();
        }

        private void backstageViewButtonItemExit_ItemClick(object sender, BackstageViewItemEventArgs e)
        {
            Close();
        }

        private void barButtonItem2_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CurrentForm.SaveAs();
        }

        private void barButtonItem1_ItemClick(object sender, DevExpress.XtraBars.ItemClickEventArgs e)
        {
            CurrentForm.Save();
        }

        private void xtraTabbedMdiManager1_FloatMDIChildActivated(object sender, EventArgs e)
        {
            ChangeActiveForm();
        }

        private void xtraTabbedMdiManager1_FloatMDIChildDeactivated(object sender, EventArgs e)
        {
            ChangeActiveForm();
        }

        private void Form1_MdiChildActivate(object sender, EventArgs e)
        {
            ChangeActiveForm();
        }

        private bool IsDropValid(string[] fileNames)
        {
            if (fileNames == null || fileNames.Length == 0) return false;
            return fileNames.Select(file => Path.GetExtension(file)).All(ext => ext != null && ext.ToLower().Equals(".tth"));
        }

        private void Form1_DragDrop(object sender, DragEventArgs e)
        {
            string[] str = e.Data.GetData("FileDrop") as string[];
            if (!IsDropValid(str)) return;
            foreach (string s in str)
            {
                OpenFile(s);
            }
        }

        private void Form1_DragEnter(object sender, DragEventArgs e)
        {
            string[] str = e.Data.GetData("FileDrop") as string[];
            e.Effect = IsDropValid(str) ? DragDropEffects.Copy : DragDropEffects.None;
        }
    }
}
