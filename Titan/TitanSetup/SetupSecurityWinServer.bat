@echo off

echo Building Titan .NET security settings...
PING 1.1.1.1 -n 1 -w 1000 >NUL

echo %PafClientHome%

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup All_Code -url "%PafClientHome%*" Execution -name Titan
echo added main program directory

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%PafClientHome%en-US\*" Execution -name en-US\Titan
echo added en-US language directory

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%PafClientHome%en-US\*" Execution -name es\Titan
echo added es language directory

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%PafClientHome%Titan.dll" FullTrust -name Titan_Dll
echo added titan.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%PafClientHome%Titan.Addin.dll" FullTrust -name Titan_Addin_Dll
echo added titan.addin.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%PafClientHome%Pace.CustomAction.dll" FullTrust -name Pace_CustomAction_Dll
echo added Pace.CustomAction.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%PafClientHome%log4net.dll" FullTrust -name log4net_Dll
echo added log4net.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup es\Titan -url "%PafClientHome%es\Titan.resources.dll" FullTrust -name titan.resources.dll
echo added es\Titan.resources.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup en-US\Titan -url "%PafClientHome%en-US\Titan.resources.dll" FullTrust -name titan.resources.dll
echo added en-US\Titan.resources.dll


rem start /w pkgmgr /iu:WAS-NetFxEnvironment;IIS-NetFxExtensibility;NetFx3;WCF-HTTP-Activation;WCF-NonHTTP-Activation
rem echo Enable WCF Http-Activation on Vista/Win7 machines

rem %SystemRoot%\Microsoft.Net\Framework\v3.0\Windows Communication Foundation\ServiceModelReg.exe /i /x
rem echo Enable WCF Http-Activation on XP/Win2000 machines