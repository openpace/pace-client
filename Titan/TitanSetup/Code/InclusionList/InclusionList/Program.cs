﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Xml;
using Microsoft.VisualStudio.Tools.Office.Runtime.Security;

namespace InclusionList
{
    class Program
    {
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        public const int SW_SHOWMINIMIZED = 2;
        public const int SW_HIDE = 0;

        private const string SOURCE = "Pace Planning Client";
        private const string EVENTLOG = "Setup";

        static void Main(string[] args)
        {
            IntPtr winHandle = Process.GetCurrentProcess().MainWindowHandle;
            ShowWindow(winHandle, SW_HIDE);

            //string key = "";// "<RSAKeyValue><Modulus></Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
            Uri solutionLocation = null; // new Uri(@"http://DeploymentServer/MySolution/MySolution.vsto");
            Uri solutionManfest = null; // new Uri(@"http://DeploymentServer/MySolution/MySolution.vsto");

            if (!EventLog.SourceExists(SOURCE))
            {
                EventLog.CreateEventSource(SOURCE, EVENTLOG);
            }

            for (int i = 0; i <= args.Count() - 1; i++)
            {
                Console.WriteLine(args[i]);
                string[] oArugment = args[i].Split('=');

                switch (oArugment[0])
                {
                    //case "/key":
                    //    key = oArugment[1];
                    //    Console.WriteLine("setting key = " + key);
                    //    EventLog.WriteEntry(SOURCE, "setting key = " + key, EventLogEntryType.Information);
                    //    break;
                    case "/location":
                        if (!Uri.TryCreate(oArugment[1], UriKind.Absolute, out solutionLocation))
                        {
                            Console.WriteLine("Error creating URI");
                            EventLog.WriteEntry(SOURCE, "Error creating URI", EventLogEntryType.Error);
                        }
                        Console.WriteLine("setting location = " + solutionLocation);
                        EventLog.WriteEntry(SOURCE, "setting location = " + solutionLocation, EventLogEntryType.Information);
                        break;
                    case "/solutionManifest":
                        if (!Uri.TryCreate(oArugment[1], UriKind.Absolute, out solutionManfest))
                        {
                            Console.WriteLine("Error creating URI");
                            EventLog.WriteEntry(SOURCE, "Error creating URI", EventLogEntryType.Error);
                        }
                        Console.WriteLine("setting solutionManifest = " + solutionManfest);
                        EventLog.WriteEntry(SOURCE, "setting solutionManifest = " + solutionManfest, EventLogEntryType.Information);
                        break;
                }
            }
            string key = GetKeyXml(solutionManfest);
            if (String.IsNullOrEmpty(key))
            {
                EventLog.WriteEntry(SOURCE, "Cannot find key", EventLogEntryType.Error);
            }
            else
            {
                try
                {
                    AddInSecurityEntry entry = new AddInSecurityEntry(solutionLocation, key);

                    try
                    {
                        UserInclusionList.Remove(solutionLocation);
                    }

                    catch (ArgumentNullException e)
                    {
                        Console.WriteLine("Exception: " + e.Message);
                        EventLog.WriteEntry(SOURCE, e.Message, EventLogEntryType.Error);
                    }

                    if (!UserInclusionList.Contains(entry))
                    {
                        UserInclusionList.Add(entry);

                        Console.WriteLine("Add User Inclusion was a success.");
                        EventLog.WriteEntry(SOURCE, "Add User Inclusion was a success", EventLogEntryType.Information);
                    }
                    else
                    {
                        Console.WriteLine("User Inclusion exists in list.");
                        EventLog.WriteEntry(SOURCE, "User Inclusion exists in list.", EventLogEntryType.Information);
                    }

                }
                catch (ArgumentNullException e)
                {
                    Console.WriteLine("Exception: " + e.Message);
                    EventLog.WriteEntry(SOURCE, e.Message, EventLogEntryType.Error);
                }
            }
        }

        private static string GetKeyXml(Uri solutionManfest) //, string xPath, string newValue)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(solutionManfest.AbsoluteUri);
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsMgr.AddNamespace("co.v1", "urn:schemas-microsoft-com:clickonce.v1");
                nsMgr.AddNamespace("xsi", "http://www.w3.org/2001/XMLSchema-instance");
                nsMgr.AddNamespace("as", "http://schemas.microsoft.com/windows/pki/2005/Authenticode");
                nsMgr.AddNamespace("msrel", "http://schemas.microsoft.com/windows/rel/2005/reldata");
                nsMgr.AddNamespace("vstav2", "urn:schemas-microsoft-com:vsta.v2");
                nsMgr.AddNamespace("asmv3", "urn:schemas-microsoft-com:asm.v3");
                nsMgr.AddNamespace("r", "urn:mpeg:mpeg21:2003:01-REL-R-NS");
                nsMgr.AddNamespace("dsig", "http://www.w3.org/2000/09/xmldsig#");
                nsMgr.AddNamespace("asmv2", "urn:schemas-microsoft-com:asm.v2");
                nsMgr.AddNamespace("vstov3", "urn:schemas-microsoft-com:vsto.v3");
                nsMgr.AddNamespace("asmv1", "urn:schemas-microsoft-com:asm.v1");
                nsMgr.AddNamespace("def", "http://www.w3.org/2000/09/xmldsig#");
                XmlNodeList nodes = xmlDoc.SelectNodes("/asmv1:assembly/def:Signature/def:KeyInfo/def:KeyValue/def:RSAKeyValue", nsMgr);
                if (nodes != null && nodes.Count == 1)
                {
                    return nodes[0].OuterXml;
                }
                return "";
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: " + e.Message);
                EventLog.WriteEntry(SOURCE, e.Message, EventLogEntryType.Error);
            }
            return "";
        }
    }
}
