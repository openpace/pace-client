﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using Microsoft.VisualStudio.Tools.Applications;
using Microsoft.VisualStudio.Tools.Applications.Runtime;

namespace SetExcelDocumentProperties
{
    class Program
    {
        [DllImport("user32.dll")]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        public const int SW_SHOWMINIMIZED = 2;
        public const int SW_HIDE = 0;

        private const string SOURCE = "Servicing";
        private const string EVENTLOG = "Setup";

        static void Main(string[] args)
        {
            IntPtr winHandle = Process.GetCurrentProcess().MainWindowHandle;
            ShowWindow(winHandle, SW_HIDE);

            try
            {
                if (!EventLog.SourceExists(SOURCE))
                {
                    EventLog.CreateEventSource(SOURCE, EVENTLOG);
                }
            }
            catch (Exception)
            {
            }
            


            string assemblyLocation = "";
            Guid solutionId = new Guid();
            Uri deploymentManifestLocation = null;
            string documentLocation = "";

            for (int i = 0; i <= args.Count() - 1; i++)
            {
                Console.WriteLine(args[i]);
                string[] oArugment = args[i].Split('=');

                switch (oArugment[0])
                {
                    case "/assemblyLocation":
                        assemblyLocation = oArugment[1];
                        LogIt("Pace Planning - setting assemblyLocation = " + assemblyLocation, SOURCE, EventLogEntryType.Information);
                        break;
                    case "/deploymentManifestLocation":
                        if (!Uri.TryCreate(oArugment[1], UriKind.Absolute, out deploymentManifestLocation))
                        {
                            LogIt("Error creating URI",SOURCE, EventLogEntryType.Error);
                        }
                        LogIt("Pace Planning - setting deploymentManifestLocation = " + deploymentManifestLocation, SOURCE, EventLogEntryType.Information);
                        break;
                    case "/documentLocation":
                        documentLocation = oArugment[1];
                        LogIt("Pace Planning - setting documentLocation = " + documentLocation, SOURCE,  EventLogEntryType.Information);
                        break;
                    case "/solutionID":
                        //solutionID = Guid.Parse(oArugment[1]);
                        solutionId = new Guid(oArugment[1]);
                        LogIt("Pace Planning - setting solutionId = " + solutionId, SOURCE, EventLogEntryType.Information);
                        //solutionID = Guid.NewGuid();
                        break;
                }
            }
            try
            {
                ServerDocument.RemoveCustomization(documentLocation);
                string[] nonpublicCachedDataMembers = null;
                ServerDocument.AddCustomization(documentLocation, assemblyLocation,
                                            solutionId, deploymentManifestLocation,
                                            true, out nonpublicCachedDataMembers);
                LogIt("Pace Planning Customization was a success.", SOURCE, EventLogEntryType.Error);

            }
            catch (System.IO.FileNotFoundException)
            {
                LogIt("The specified document does not exist.", SOURCE, EventLogEntryType.Error);
            }
            catch (System.IO.IOException)
            {
                LogIt("The specified document is read-only.", SOURCE, EventLogEntryType.Error);
            }
            catch (InvalidOperationException ex)
            {
                LogIt("The customization could not be removed.\n" + ex.Message, SOURCE, EventLogEntryType.Error);
            }
            catch (DocumentNotCustomizedException ex)
            {
                LogIt("The document could not be customized.\n" + ex.Message, SOURCE, EventLogEntryType.Error);
            }
        }

        private static void LogIt(string message, string eventLogSource, EventLogEntryType type)
        {
            Console.WriteLine(message);
            try
            {
                EventLog.WriteEntry(eventLogSource, message, type);
            }
            catch (Exception)
            {
            }
        }
    }
}
