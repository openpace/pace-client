
Choose one of the following methods to install the Pace Client in silent mode (THIS INFORMATION CAN ALSO BE IN THE OPEN PACE ONLINE DOCUMENTATION AT http://openpace.bitbucket.io ) :

* To install Pace in a silent mode with all of the pre-requisites, follow these steps:
	** For a full silent install run the following command (the installer must be in the directory of the setup.exe file or use a fully qualified path):  OpenPaceClientInstall-3.0.0.0.exe /s /v"/qn"
	** This checks for prerequisites and installs them silently if they are required; all default installer prompts are selected.
	
* If any issues arise while installing the Pace Client in silent mode the following install commands can be used to debug the msi installation:
	** Run the following command to install the Pace Client in silent mode with verbose logging:  OpenPaceClientInstall-3.0.0.0.exe /s /v"/qn /l* pace.log"
	** Run the following command to install the Pace Client in silent mode with debug level logging:  OpenPaceClientInstall-3.0.0.0.exe /s /v"/qn /lvx pace.log"

* To pass parameters to the msi engine use syntax like the following:  OpenPaceClientInstall-3.0.0.0.exe /s /v"/qn ALLUSERS=2"

* To install Pace in a silent mode with none of the pre-requisites, follow these steps:
	** The install path can be customized by calling the msiexec directly: msiexec /i "Open Pace.msi" /qn INSTALLDIR="C:\Program Files\Custom Directory\" 
	** Note: The application will not check for the required prerequisites.