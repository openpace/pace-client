@echo off

echo Building Titan .NET v2.0.50727 security settings...
PING 1.1.1.1 -n 1 -w 1000 >NUL


%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup All_Code -url "%~1*" Execution -name Titan
echo added main program directory

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1en-US\*" Execution -name en-US\Titan
echo added en-US language directory

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1en-US\*" Execution -name es\Titan
echo added es language directory

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1Titan.dll" FullTrust -name Titan_Dll
echo added titan.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1Titan.Addin.dll" FullTrust -name Titan_Addin_Dll
echo added titan.addin.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1Pace.CustomAction.dll" FullTrust -name Pace_CustomAction_Dll
echo added Pace.CustomAction.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1log4net.dll" FullTrust -name log4net_Dll
echo added log4net.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup es\Titan -url "%~1es\Titan.resources.dll" FullTrust -name titan.resources.dll
echo added es\Titan.resources.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup en-US\Titan -url "%~1en-US\Titan.resources.dll" FullTrust -name titan.resources.dll
echo added en-US\Titan.resources.dll

%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.Data.v12.1.dll" FullTrust -name DevExpress
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.Utils.v12.1.dll" FullTrust
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.XtraBars.v12.1.dll" FullTrust
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.XtraEditors.v12.1.dll" FullTrust
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.XtraGrid.v12.1.dll" FullTrust
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.XtraLayout.v12.1.dll" FullTrust
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.XtraTreeList.v12.1.dll" FullTrust
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -polchgprompt off -m -addgroup Titan -url "%~1DevExpress.XtraVerticalGrid.v12.1.dll" FullTrust
echo added DevExpress assemblies



