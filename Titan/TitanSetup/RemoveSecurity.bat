@echo off

echo Removing Titan .NET v2.0.50727 security settings...
PING 1.1.1.1 -n 1 -w 1000 >NUL

echo Remove group "Titan"
%windir%\Microsoft.NET\Framework\v2.0.50727\caspol.exe -rg Titan

echo ErrorLevel %ErrorLevel%

rem IF ERRORLEVEL -1 GOTO Label1

exit 0
